//
//  Utils.m
//
//  Created by AngusNi on 14/12/3.
//  Copyright (c) 2014年 AngusNi. All rights reserved.
//

#import "Utils.h"
#import "Reachability.h"
#import "constants.h"
@implementation Utils

/********************* File Utils **********************/

// 文件写入
+ (BOOL)getFileWriteImage:(NSURL*)url andFileName:(NSString *)fileName andFilePath:(NSString *)filePath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *libraryDirectory = [self getCurrentLibraryCachesDocument];
    if (![fileManager fileExistsAtPath:[libraryDirectory stringByAppendingPathComponent:filePath]]) {
        [fileManager createDirectoryAtPath:[libraryDirectory stringByAppendingPathComponent:filePath]withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *myDirectory = [libraryDirectory
                             stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"%@/%@", filePath, fileName]];
    if(url==nil) {
        return NO;
    }
    NSData*data = [NSData dataWithContentsOfURL:url];
    return [data writeToFile:myDirectory atomically:YES];
}

// 文件读取
+ (UIImage*)getFileReadImageWithFileName:(NSString *)fileName andFilePath:(NSString *)filePath {
    NSString *libraryDirectory = [self getCurrentLibraryCachesDocument];
    NSString *myDirectory = [libraryDirectory
                             stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"%@/%@", filePath, fileName]];
    if ([[NSFileManager defaultManager] fileExistsAtPath: myDirectory]) {
        NSData *data=[NSData dataWithContentsOfFile:myDirectory];
        return [UIImage imageWithData:data];
    }
    return PlaceHolderUserImage;
}

// 文件写入
+ (BOOL)getFileArrayWrite:(NSMutableArray*)array andFileName:(NSString *)fileName andFilePath:(NSString *)filePath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *libraryDirectory = [self getCurrentLibraryCachesDocument];
    if (![fileManager fileExistsAtPath:[libraryDirectory stringByAppendingPathComponent:filePath]]) {
        [fileManager createDirectoryAtPath:[libraryDirectory stringByAppendingPathComponent:filePath]withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *myDirectory = [libraryDirectory
                             stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"%@/%@", filePath, fileName]];
    NSData *arrayData = [NSKeyedArchiver archivedDataWithRootObject:array];
    return [arrayData writeToFile:myDirectory atomically:YES];
}

// 文件读取
+ (NSMutableArray*)getFileArrayReadWithFileName:(NSString *)fileName andFilePath:(NSString *)filePath {
    NSString *libraryDirectory = [self getCurrentLibraryCachesDocument];
    NSString *myDirectory = [libraryDirectory
                             stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"%@/%@", filePath, fileName]];
    if ([[NSFileManager defaultManager] fileExistsAtPath: myDirectory]) {
        NSData *arrayData=[NSData dataWithContentsOfFile:myDirectory];
        NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
        return array;
    }
    return nil;
}

// 文件写入
+ (BOOL)getFileWrite:(id)jsonObject
         andFileName:(NSString *)fileName
         andFilePath:(NSString *)filePath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *libraryDirectory = [self getCurrentLibraryCachesDocument];
    if (![fileManager fileExistsAtPath:[libraryDirectory stringByAppendingPathComponent:filePath]]) {
        [fileManager createDirectoryAtPath:[libraryDirectory stringByAppendingPathComponent:filePath]withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *myDirectory = [libraryDirectory
                             stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"%@/%@", filePath, fileName]];
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:jsonObject options:NSJSONWritingPrettyPrinted error:nil];
    return [jsonData writeToFile:myDirectory atomically:YES];
}
// 文件读取
+ (id)getFileReadWithFileName:(NSString *)fileName
                  andFilePath:(NSString *)filePath {
    NSString *libraryDirectory = [self getCurrentLibraryCachesDocument];
    NSString *myDirectory = [libraryDirectory
                             stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"%@/%@", filePath, fileName]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath: myDirectory]) {
        NSData *jsonData=[NSData dataWithContentsOfFile:myDirectory];
        id jsonObject=[NSJSONSerialization JSONObjectWithData:jsonData
                                                      options:NSJSONReadingAllowFragments
                                                        error:nil];
        return jsonObject;
    }
    return nil;
}
// 文件删除
+ (BOOL)getFileDeleteWithFileName:(NSString *)fileName
                      andFilePath:(NSString *)filePath {
    NSString *libraryDirectory = [self getCurrentLibraryCachesDocument];
    NSString *myDirectory = [libraryDirectory
                             stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"%@/%@", filePath, fileName]];
    return [[NSFileManager defaultManager] removeItemAtPath:myDirectory error:nil];
}

/********************* Current Utils **********************/
// 设备型号
+ (NSString *)getCurrentDeviceVersion {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithFormat:@"%s", systemInfo.machine];
    
    if ([platform isEqualToString:@"i386"]||[platform isEqualToString:@"x86_64"])
        return @"Simulator";
    if ([platform isEqualToString:@"iPhone1,1"])
        return @"iPhone 1";
    if ([platform isEqualToString:@"iPhone1,2"])
        return @"iPhone 3";
    if ([platform isEqualToString:@"iPhone2,1"])
        return @"iPhone 3s";
    if ([platform isEqualToString:@"iPhone3,1"] ||
        [platform isEqualToString:@"iPhone3,2"])
        return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])
        return @"iPhone 4s";
    if ([platform isEqualToString:@"iPhone5,1"] ||
        [platform isEqualToString:@"iPhone5,2"])
        return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone5,3"] ||
        [platform isEqualToString:@"iPhone5,4"])
        return @"iPhone 5c";
    if ([platform isEqualToString:@"iPhone6,1"] ||
        [platform isEqualToString:@"iPhone6,2"])
        return @"iPhone 5s";
    if ([platform isEqualToString:@"iPhone7,2"])
        return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])
        return @"iPhone 6Plus";
    if ([platform isEqualToString:@"iPhone8,2"])
        return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,1"])
        return @"iPhone 6sPlus";
    if ([platform isEqualToString:@"iPhone8,4"]||[platform isEqualToString:@"iPhone8,3"])
        return @"iPhone se";
    if ([platform isEqualToString:@"iPhone9,2"])
        return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,1"])
        return @"iPhone 7Plus";
    return platform;
}
// 设备系统号
+ (NSString *)getCurrentDeviceSystemVersion {
    return [[UIDevice currentDevice] systemVersion];
}
// 应用版本
+ (NSString *)getCurrentBundleShortVersion {
    return [[[NSBundle mainBundle] infoDictionary]
            objectForKey:@"CFBundleShortVersionString"];
}
// 应用名
+ (NSString *)getCurrentBundleName {
    return
    [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
}
// 应用Build版本
+ (NSString *)getCurrentBundleVersion {
    return
    [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}
// 应用临时目录
+ (NSString *)getCurrentLibraryCachesDocument {
    return [NSSearchPathForDirectoriesInDomains(
                                                NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}
// 应用Identifier
+ (NSString *)getCurrentBundleIdentifier {
    return [[[NSBundle mainBundle] infoDictionary]
            objectForKey:@"CFBundleIdentifier"];
}

/********************* UIViewController Utils **********************/
+ (void)getIOS7UINavigationController:(UIViewController *)viewContr {
    if ([viewContr respondsToSelector:@selector(edgesForExtendedLayout)]) {
        // UINavigationItem
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        backItem.title = @"";
        viewContr.navigationItem.backBarButtonItem = backItem;
        // UIStatusBar
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [[UIApplication sharedApplication]
         setStatusBarStyle:UIStatusBarStyleDefault];
        // UINavigationBar
        [[UINavigationBar appearance]
         setTitleTextAttributes:
         [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                     forKey:NSForegroundColorAttributeName]];
        [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
        [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
//
        // UITableView,Defaults to YES
//        viewContr.automaticallyAdjustsScrollViewInsets = NO;
//        // Defaults to UIRectEdgeAll
//        viewContr.edgesForExtendedLayout = UIRectEdgeAll;
//        // Defaults to NO
//        viewContr.extendedLayoutIncludesOpaqueBars = NO;
//        // UIStatusBar Defaults to NO
//        viewContr.modalPresentationCapturesStatusBarAppearance = NO;
//        // UINavigationBar,Defaults to YES
//        viewContr.navigationController.navigationBar.translucent = NO;
//        // UITabBar,Defaults to YES
//        viewContr.tabBarController.tabBar.translucent = NO;
    }
}

+ (float)getIOS7TabBarHeight {
    return 64.0;
}

/********************* NetWork Utils **********************/
+ (NSString *)getNetWorkError:(NSError *)error {
    switch (error.code) {
        case 100:
            return @"继续";
            break;
        case 101:
            return @"切换协议";
            break;
        case 200:
            return NSLocalizedString(@"R1020", @"确定");
            break;
        case 201:
            return @"已创建";
            break;
        case 202:
            return @"已接受";
            break;
        case 203:
            return @"非权威性信息";
            break;
        case 204:
            return @"无内容";
            break;
        case 205:
            return @"重置内容";
            break;
        case 206:
            return @"部分内容";
            break;
        case 302:
            return @"对象已移动";
            break;
        case 304:
            return @"未修改";
            break;
        case 307:
            return @"临时重定向";
            break;
        case 400:
            return @"错误的请求";
            break;
        case 401:
            return @"访问被拒绝";
            break;
        case 403:
            return @"禁止访问";
            break;
        case 404:
            return @"未找到请求的地址";
            break;
        case 405:
            return @"方法不被允许";
            break;
        case 406:
            return @"客户端浏览器不接受所请求页面的 MIME 类型";
            break;
        case 407:
            return @"要求进行代理身份验证";
            break;
        case 408:
            return @"服务器等候请求时发生超时";
            break;
        case 409:
            return @"服务器在完成请求时发生冲突";
            break;
        case 410:
            return @"请求的资源已删除";
            break;
        case 411:
            return @"服务器不接受不含有效内容长度标头字段的请求";
            break;
        case 412:
            return @"前提条件失败";
            break;
        case 413:
            return @"请求实体过大";
            break;
        case 414:
            return @"请求URI太长";
            break;
        case 415:
            return @"不支持的媒体类型";
            break;
        case 416:
            return @"请求的范围不符合要求";
            break;
        case 417:
            return @"执行失败";
            break;
        case 423:
            return @"锁定的错误";
            break;
        case 500:
            return @"服务器错误";
            break;
        case 501:
            return @"指定了未实现的配置";
            break;
        case 502:
            return @"错误网关";
            break;
        case 503:
            return @"服务不可用";
            break;
        case 504:
            return @"网关超时";
            break;
        case 505:
            return @"HTTP版本不受支持";
            break;
        default:
            return error.localizedDescription;
            break;
    }
}

+ (NSString *)getNetWorkName {
    NSString *wifiName = @"";
    CFArrayRef wifiInterfaces = CNCopySupportedInterfaces();
    
    if (!wifiInterfaces) {
        return @"";
    }
    
    NSArray *interfaces = (__bridge NSArray *)wifiInterfaces;
    
    for (NSString *interfaceName in interfaces) {
        CFDictionaryRef dictRef  =
        CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interfaceName));
        
        if (dictRef) {
            NSDictionary *networkInfo = (__bridge NSDictionary *)dictRef;
            //            NSLog(@"network info -> %@", networkInfo);
            wifiName  =
            [networkInfo objectForKey:(__bridge NSString *)kCNNetworkInfoKeySSID];
            CFRelease(dictRef);
        }
    }
    
    CFRelease(wifiInterfaces);
    return wifiName;
}

+ (NSString *)getNetWorkMac {
    NSString *wifiMac = @"";
    CFArrayRef wifiInterfaces = CNCopySupportedInterfaces();
    
    if (!wifiInterfaces) {
        return @"";
    }
    
    NSArray *interfaces = (__bridge NSArray *)wifiInterfaces;
    
    for (NSString *interfaceName in interfaces) {
        CFDictionaryRef dictRef  =
        CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interfaceName));
        
        if (dictRef) {
            NSDictionary *networkInfo = (__bridge NSDictionary *)dictRef;
            //            NSLog(@"network info -> %@", networkInfo);
            wifiMac = [networkInfo
                       objectForKey:(__bridge NSString *)kCNNetworkInfoKeyBSSID];
            CFRelease(dictRef);
        }
    }
    
    CFRelease(wifiInterfaces);
    return wifiMac;
}

+ (NSString *)getNetWorkType {
    Reachability *r = [Reachability reachabilityForInternetConnection];
    if ([r currentReachabilityStatus] == ReachableViaWiFi) {
        return @"Wifi";
    } else if ([r currentReachabilityStatus] == ReachableViaWWAN) {
        return @"3G/GPRS";
    } else {
        return @"NotReachable";
    }
}

+ (BOOL)getNetWorkConnected {
    
    Reachability *r = [Reachability reachabilityForInternetConnection];
    if ([r currentReachabilityStatus] == NotReachable) {
        return NO;
    }
    return YES;
}

/********************* UIColor+Category Utils **********************/
+ (UIColor *)getUIColorWithHexString:(NSString *)hexStr andAlpha:(float)alpha {
    NSString *cString = [[hexStr
                          stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    if ([cString length] < 6) {
        return [UIColor whiteColor];
    }
    
    if ([cString hasPrefix:@"#"]) {
        cString = [cString substringFromIndex:1];
    }
    
    if ([cString length] !=  6) {
        return [UIColor whiteColor];
    }
    
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    return [UIColor colorWithRed:((float)r / 255.0f)
                           green:((float)g / 255.0f)
                            blue:((float)b / 255.0f)
                           alpha:alpha];
}

+ (UIColor *)getUIColorWithHexString:(NSString *)hexStr {
    NSString *cString = [[hexStr
                          stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    if ([cString length] < 6) {
        return [UIColor whiteColor];
    }
    
    if ([cString hasPrefix:@"#"]) {
        cString = [cString substringFromIndex:1];
    }
    
    if ([cString length] !=  6) {
        return [UIColor whiteColor];
    }
    
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    return [UIColor colorWithRed:((float)r / 255.0f)
                           green:((float)g / 255.0f)
                            blue:((float)b / 255.0f)
                           alpha:1.0f];
}

+ (UIColor *)getUIColorRandom {
    static BOOL seeded = NO;
    
    if (!seeded) {
        seeded = YES;
        srandom((int)time(NULL));
    }
    
    CGFloat red = (CGFloat)random() / (CGFloat)RAND_MAX;
    CGFloat green = (CGFloat)random() / (CGFloat)RAND_MAX;
    CGFloat blue = (CGFloat)random() / (CGFloat)RAND_MAX;
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
}

+ (CGSize)getUIImageSizeWithURL:(NSString *)urlStr {
    if ([[[NSString stringWithFormat:@"%@", urlStr]
          componentsSeparatedByString:@"-w"] count] > 1&&[[[NSString stringWithFormat:@"%@", urlStr]
                                                           componentsSeparatedByString:@"-h"] count] > 1 && [[[NSString stringWithFormat:@"%@", urlStr]
                                                                                                              componentsSeparatedByString:@"@"] count] <= 1) {
        NSString *temp0 = [[[NSString stringWithFormat:@"%@", urlStr]
                            componentsSeparatedByString:@"-w"] objectAtIndex:1];
        NSString *tempW = [[[NSString stringWithFormat:@"%@", temp0]
                            componentsSeparatedByString:@"-h"] objectAtIndex:0];
        NSString *temp1 = [[[NSString stringWithFormat:@"%@", temp0]
                            componentsSeparatedByString:@"-h"] objectAtIndex:1];
        NSString *tempH = [[[NSString stringWithFormat:@"%@", temp1]
                            componentsSeparatedByString:@"."] objectAtIndex:0];
        return CGSizeMake(tempW.floatValue, tempH.floatValue);
    }
    return CGSizeMake(0, 0);
}

// 压缩图片
+ (UIImage *)narrowWithImage:(UIImage*)image {
    CGSize imgSize = CGSizeMake(image.size.width, image.size.height);
    int i = 10;
    while(i>0) {
        UIGraphicsBeginImageContext(imgSize);
        [image drawInRect:CGRectMake(0,0,imgSize.width,imgSize.height)];
        UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        if (newImage != nil) {
            NSData *imageData = UIImageJPEGRepresentation(newImage,0);
            image = [UIImage imageWithData:imageData];
            NSLog(@"图片品质:%lu",(unsigned long)[imageData length] / 1000);
            if (((unsigned long)[imageData length] / 1000)<=5) {
                break;
            }
            imgSize = CGSizeMake(imgSize.width/2.0, imgSize.height/2.0);
            i--;
        }
    }
    return image;
}

+ (UIImage *)getUIImageDefaultWithRect:(CGRect)rect {
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[self getUIColorWithHexString:@"f5f5f5"] CGColor]); // 默认颜色
    // CGContextSetFillColorWithColor(context,[[Service randomColor]
    // CGColor]);//随机色
    CGContextFillRect(context, rect);
    UIImage *imge = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return imge;
}

// 图片压缩问题
+ (UIImage *)getUIImageScalingFromSourceImage:(UIImage *)sourceImage
                                   targetSize:(CGSize)targetSize {
    CGSize tempSize = CGSizeZero;
    if(sourceImage.size.width/sourceImage.size.height>= scaleW/scaleH){
        if (sourceImage.size.width > scaleW) {
            tempSize =  CGSizeMake(scaleW, (scaleW / sourceImage.size.width) *
                                   sourceImage.size.height);
        }
    } else {
        if (sourceImage.size.height > scaleH) {
            tempSize =  CGSizeMake((scaleH *sourceImage.size.width)/
                                   sourceImage.size.height, scaleH);
        }
    }
    if(tempSize.width!= 0){
        targetSize = tempSize;
    }
    UIGraphicsBeginImageContext(targetSize);
    [sourceImage
     drawInRect:CGRectMake(0, 0, targetSize.width, targetSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if (newImage == nil) {
        NSLog(@"图片压缩失败");
        return sourceImage;
    }
    else {
        NSData *imageData = UIImageJPEGRepresentation(newImage,1.0);
        NSUInteger sizeOrigin = [imageData length];
        NSUInteger sizeOriginKB = sizeOrigin / 1000;
        NSLog(@"图片降低品质:%lu",(unsigned long)sizeOriginKB);
        if (sizeOriginKB > 100) {
            imageData = UIImageJPEGRepresentation(newImage,0.50);
        }
        newImage = [UIImage imageWithData:imageData];
        return newImage;
    }
}

static size_t getAssetBytesCallback(void *info, void *buffer, off_t position, size_t count) {
    ALAssetRepresentation *rep = (__bridge id)info;
    NSError *error = nil;
    size_t countRead = [rep getBytes:(uint8_t *)buffer fromOffset:position length:count error:&error];
    if (countRead == 0 && error) {
        // We have no way of passing this info back to the caller, so we log it, at least.
        NSLog(@"thumbnailForAsset:maxPixelSize: got an error reading an asset: %@", error);
    }
    return countRead;
}

static void releaseAssetCallback(void *info) {
    // The info here is an ALAssetRepresentation which we CFRetain in thumbnailForAsset:maxPixelSize:.
    // This release balances that retain.
    CFRelease(info);
}

// Returns a UIImage for the given asset, with size length at most the passed size.
// The resulting UIImage will be already rotated to UIImageOrientationUp, so its CGImageRef
// can be used directly without additional rotation handling.
// This is done synchronously, so you should call this method on a background queue/thread.
+ (UIImage *)thumbnailForAsset:(ALAsset *)asset maxPixelSize:(int)size {
    NSParameterAssert(asset !=  nil);
    NSParameterAssert(size > 0);
    ALAssetRepresentation *rep = [asset defaultRepresentation];
    CGDataProviderDirectCallbacks callbacks = {
        .version = 0,
        .getBytePointer = NULL,
        .releaseBytePointer = NULL,
        .getBytesAtPosition = getAssetBytesCallback,
        .releaseInfo = releaseAssetCallback,
    };
    CGDataProviderRef provider = CGDataProviderCreateDirect((void *)CFBridgingRetain(rep), [rep size], &callbacks);
    CGImageSourceRef source = CGImageSourceCreateWithDataProvider(provider, NULL);
    CGImageRef imageRef = CGImageSourceCreateThumbnailAtIndex(source, 0, (__bridge CFDictionaryRef) @{
                                                                                                      (NSString *)kCGImageSourceCreateThumbnailFromImageAlways : @YES,
                                                                                                      (NSString *)kCGImageSourceThumbnailMaxPixelSize : [NSNumber numberWithInt:size],
                                                                                                      (NSString *)kCGImageSourceCreateThumbnailWithTransform : @YES,
                                                                                                      });
    CFRelease(source);
    CFRelease(provider);
    if (!imageRef) {
        return nil;
    }
    UIImage *toReturn = [UIImage imageWithCGImage:imageRef];
    CFRelease(imageRef);
    return toReturn;
}

+ (UIImage *)getUIImageByScalingAndCroppingForSize:(CGSize)targetSize
                                       sourceImage:(UIImage *)sourceImage {
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if (widthFactor > heightFactor) {
            scaleFactor = widthFactor;
        } else {
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        if (widthFactor > heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        } else if (widthFactor < heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    UIGraphicsBeginImageContext(targetSize);
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if (newImage == nil) {
        NSLog(@"图片缩放失败");
        return sourceImage;
    }
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)getUIImageBlurry:(UIImage *)image withBlur:(CGFloat)blur {
    if ((blur < 0.0f) || (blur > 1.0f)) {
        blur = 0.5f;
    }
    
    int boxSize = (int)(blur * 100);
    boxSize -=  (boxSize % 2) + 1;
    
    CGImageRef img = image.CGImage;
    
    vImage_Buffer inBuffer, outBuffer;
    vImage_Error error;
    void *pixelBuffer;
    
    CGDataProviderRef inProvider = CGImageGetDataProvider(img);
    CFDataRef inBitmapData = CGDataProviderCopyData(inProvider);
    
    inBuffer.width = CGImageGetWidth(img);
    inBuffer.height = CGImageGetHeight(img);
    inBuffer.rowBytes = CGImageGetBytesPerRow(img);
    inBuffer.data = (void *)CFDataGetBytePtr(inBitmapData);
    
    pixelBuffer = malloc(CGImageGetBytesPerRow(img) * CGImageGetHeight(img));
    
    outBuffer.data = pixelBuffer;
    outBuffer.width = CGImageGetWidth(img);
    outBuffer.height = CGImageGetHeight(img);
    outBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    error = vImageBoxConvolve_ARGB8888(&inBuffer, &outBuffer, NULL, 0, 0, boxSize,
                                       boxSize, NULL, kvImageEdgeExtend);
    
    if (error) {
        NSLog(@"error from convolution %ld", error);
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(
                                             outBuffer.data, outBuffer.width, outBuffer.height, 8, outBuffer.rowBytes,
                                             colorSpace, CGImageGetBitmapInfo(image.CGImage));
    
    CGImageRef imageRef = CGBitmapContextCreateImage(ctx);
    UIImage *returnImage = [UIImage imageWithCGImage:imageRef];
    
    // clean up
    CGContextRelease(ctx);
    CGColorSpaceRelease(colorSpace);
    free(pixelBuffer);
    CFRelease(inBitmapData);
    CGImageRelease(imageRef);
    
    return returnImage;
}

// 图片文件保存到系统Lib。成功返回picName,否则返回空
+ (NSString *)getUIImageSave:(UIImage *)orgImg andPicName:(NSString *)picName {
    NSString *imgType = @"";
    NSData *imgData = nil;
    if (UIImagePNGRepresentation(orgImg) == nil) {
        imgType = @"jpg";
        imgData = UIImageJPEGRepresentation(orgImg, 1.0f);
    } else {
        imgType = @"png";
        imgData = UIImagePNGRepresentation(orgImg);
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyMdHmsS"];
    NSString *dateNow = [dateFormatter stringFromDate:[NSDate date]];
    
    if ([self getUIFileSave:imgData
                       Name:[NSString stringWithFormat:@"%@%@.%@", picName,
                             dateNow, imgType]
                       Path:@"FTB"]) {
        return picName;
    } else {
        return @"";
    }
}

// NSData文件保存到系统Lib。
+ (BOOL)getUIFileSave:(NSData *)tempData
                 Name:(NSString *)tempName
                 Path:(NSString *)filePath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *libraryDirectory = [self getCurrentLibraryCachesDocument];
    if (![fileManager fileExistsAtPath:
          [libraryDirectory
           stringByAppendingPathComponent:
           [NSString stringWithFormat:@"%@", filePath]]]) {
              [fileManager createDirectoryAtPath:
               [libraryDirectory
                stringByAppendingPathComponent:
                [NSString stringWithFormat:@"%@", filePath]]
                     withIntermediateDirectories:YES
                                      attributes:nil
                                           error:nil];
          }
    NSString *myDirectory = [libraryDirectory
                             stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"%@/%@", filePath, tempName]];
    if ([tempData writeToFile:myDirectory atomically:YES]) {
        return YES;
    } else {
        return NO;
    }
}

/********************* UIDate+Category Utils **********************/
+ (NSString *)getUIDateCompareNow:(NSString *)dateStr {
    if ([[dateStr componentsSeparatedByString:@"T"] count] == 1) {
        return @"";
    }
    NSString *inputDateStr  =
    [[dateStr componentsSeparatedByString:@"T"] objectAtIndex:0];
    NSString *inputTimeStr  =
    [[dateStr componentsSeparatedByString:@"T"] objectAtIndex:1];
    inputTimeStr =
    [[inputTimeStr componentsSeparatedByString:@"+"] objectAtIndex:0];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if ([[inputDateStr componentsSeparatedByString:@"-"] count] > 1) {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    } else {
        [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    }
    NSDate *inputDate = [formatter
                         dateFromString:[NSString stringWithFormat:@"%@ %@", inputDateStr,
                                         inputTimeStr]];
    inputDate = [self getUIDateFromatAnyDate:inputDate];
    //
    [formatter setDateFormat:@"HH:mm"];
    inputTimeStr = [formatter stringFromDate:inputDate];
    NSString *resultStr = @"";
    if (inputDate) {
        NSTimeInterval timeSinceLastUpdate = [inputDate timeIntervalSinceNow];
        int days=((int)timeSinceLastUpdate)/(3600*24);
        int hours=((int)timeSinceLastUpdate)%(3600*24)/3600;
        int minutes=((int)timeSinceLastUpdate)/60%60;
        if (timeSinceLastUpdate < 0) {
            resultStr = NSLocalizedString(@"R2056", @"活动已结束");
        } else {
            resultStr = [NSString stringWithFormat:NSLocalizedString(@"R2058", @"距结束%d天%d小时%d分钟"), days,hours,minutes];
        }
    }
    return resultStr;
}
//距离活动开始还有多长时间
+ (NSString *)getUIDateCompareNowFortrailer:(NSString *)dateStr{
    if ([[dateStr componentsSeparatedByString:@"T"] count] == 1) {
        return @"";
    }
    NSString *inputDateStr  =
    [[dateStr componentsSeparatedByString:@"T"] objectAtIndex:0];
    NSString *inputTimeStr  =
    [[dateStr componentsSeparatedByString:@"T"] objectAtIndex:1];
    inputTimeStr =
    [[inputTimeStr componentsSeparatedByString:@"+"] objectAtIndex:0];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if ([[inputDateStr componentsSeparatedByString:@"-"] count] > 1) {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    } else {
        [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    }
    NSDate *inputDate = [formatter
                         dateFromString:[NSString stringWithFormat:@"%@ %@", inputDateStr,
                                         inputTimeStr]];
    inputDate = [self getUIDateFromatAnyDate:inputDate];
    //
    [formatter setDateFormat:@"HH:mm"];
    inputTimeStr = [formatter stringFromDate:inputDate];
    NSString *resultStr = @"";
    if (inputDate) {
        NSTimeInterval timeSinceLastUpdate = [inputDate timeIntervalSinceNow];
//        int days=((int)timeSinceLastUpdate)/(3600*24);
//        int hours=((int)timeSinceLastUpdate)%(3600*24)/3600;
//        int minutes=((int)timeSinceLastUpdate)/60%60;
        int minutes = (int)timeSinceLastUpdate/60;
        int seconds = (int)timeSinceLastUpdate%60;
        if (timeSinceLastUpdate < 0) {
            resultStr = NSLocalizedString(@"R2518", @"活动已开始");
        }else {
          //  resultStr = [NSString stringWithFormat:NSLocalizedString(@"R2517", @"距开始%d天%d小时%d分钟"), days,hours,minutes];
            resultStr = [NSString stringWithFormat:@"距开始%d分%d秒",minutes,seconds];
        }
    }
    return resultStr;
}

//为活动界面做英文文字过长问题兼容
+ (NSString *)getUIDateCompareNowForEnglish:(NSString *)dateStr{
    if ([[dateStr componentsSeparatedByString:@"T"] count] == 1) {
        return @"";
    }
    NSString *inputDateStr  =
    [[dateStr componentsSeparatedByString:@"T"] objectAtIndex:0];
    NSString *inputTimeStr  =
    [[dateStr componentsSeparatedByString:@"T"] objectAtIndex:1];
    inputTimeStr =
    [[inputTimeStr componentsSeparatedByString:@"+"] objectAtIndex:0];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if ([[inputDateStr componentsSeparatedByString:@"-"] count] > 1) {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    } else {
        [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    }
    NSDate *inputDate = [formatter
                         dateFromString:[NSString stringWithFormat:@"%@ %@", inputDateStr,
                                         inputTimeStr]];
    inputDate = [self getUIDateFromatAnyDate:inputDate];
    //
    [formatter setDateFormat:@"HH:mm"];
    inputTimeStr = [formatter stringFromDate:inputDate];
    NSString *resultStr = @"";
    if (inputDate) {
        NSTimeInterval timeSinceLastUpdate = [inputDate timeIntervalSinceNow];
        int days=((int)timeSinceLastUpdate)/(3600*24);
        int hours=((int)timeSinceLastUpdate)%(3600*24)/3600;
        int minutes=((int)timeSinceLastUpdate)/60%60;
        if (timeSinceLastUpdate < 0) {
            resultStr = NSLocalizedString(@"R2056", @"活动已结束");
        } else {
            resultStr = [NSString stringWithFormat:@"%d,%d,%d", days,hours,minutes];
        }
    }
    return resultStr;
}


+ (NSString *)getUIDateCompare:(NSString *)dateStr {
    if ([[dateStr componentsSeparatedByString:@"T"] count] == 1) {
        return @"";
    }
    NSInteger aMinute = 60;
    NSInteger anHour = 3600;
    NSInteger aDay = 86400;
    NSInteger sevenDay = 86400 * 7;
    NSString *inputDateStr  =
    [[dateStr componentsSeparatedByString:@"T"] objectAtIndex:0];
    NSString *inputTimeStr  =
    [[dateStr componentsSeparatedByString:@"T"] objectAtIndex:1];
    inputTimeStr =
    [[inputTimeStr componentsSeparatedByString:@"+"] objectAtIndex:0];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if ([[inputDateStr componentsSeparatedByString:@"-"] count] > 1) {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    } else {
        [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    }
    NSDate *inputDate = [formatter
                         dateFromString:[NSString stringWithFormat:@"%@ %@", inputDateStr,
                                         inputTimeStr]];
    inputDate = [self getUIDateFromatAnyDate:inputDate];
    //
    [formatter setDateFormat:@"HH:mm"];
    inputTimeStr = [formatter stringFromDate:inputDate];
    NSString *resultStr = @"";
    if (inputDate) {
        NSTimeInterval timeSinceLastUpdate = [inputDate timeIntervalSinceNow];
        NSInteger timeToDisplay = 0;
        timeSinceLastUpdate *=  -1;
        if (timeSinceLastUpdate < anHour) {
            timeToDisplay = (NSInteger)(timeSinceLastUpdate
                                        / aMinute);
            if (timeToDisplay == 0) {
                resultStr = [NSString
                             stringWithFormat:NSLocalizedString(@"R2059", @"刚刚")];
            } else {
                resultStr = [NSString
                             stringWithFormat:NSLocalizedString(@"R2060", @"%ld分钟前"), (long)timeToDisplay];
            }
        } else if (timeSinceLastUpdate < aDay) {
            timeToDisplay = (NSInteger)(timeSinceLastUpdate
                                        / anHour);
            resultStr = [NSString stringWithFormat:NSLocalizedString(@"R2061", @"%ld小时前"), (long)timeToDisplay];
        } else if (timeSinceLastUpdate < sevenDay) {
            timeToDisplay = (NSInteger)(timeSinceLastUpdate / aDay);
            if (timeToDisplay == 1) {
                resultStr = NSLocalizedString(@"R2062", @"昨天");
            } else if (timeToDisplay == 2) {
                resultStr = NSLocalizedString(@"R2063", @"前天");
            } else {
                resultStr = [NSString stringWithFormat:NSLocalizedString(@"R2064", @"%ld天前"), (long)timeToDisplay];
            }
        } else {
            resultStr = inputDateStr;
        }
    }
    return resultStr;
}

+ (NSString *)getUIDateWeek:(NSDate *)in_date {
    NSString *retStr = @"";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDateComponents *componets  =
    [[NSCalendar autoupdatingCurrentCalendar] components:NSCalendarUnitWeekday
                                                fromDate:in_date];
    int weekday = (int)
    [componets weekday]; // a就是星期几,1代表星期日,2代表星期一,后面依次
    NSString *weekStr = @"";
    
    if (weekday == 1) {
        weekStr = @"星期日";
    }
    
    if (weekday == 2) {
        weekStr = @"星期一";
    }
    
    if (weekday == 3) {
        weekStr = @"星期二";
    }
    
    if (weekday == 4) {
        weekStr = @"星期三";
    }
    
    if (weekday == 5) {
        weekStr = @"星期四";
    }
    
    if (weekday == 6) {
        weekStr = @"星期五";
    }
    
    if (weekday == 7) {
        weekStr = @"星期六";
    }
    
    retStr  =
    [NSString stringWithFormat:@"%@", [formatter stringFromDate:in_date]];
    [formatter setDateFormat:@"HH:mm:ss"];
    retStr = [NSString stringWithFormat:@"%@   %@   %@", retStr, weekStr,
              [formatter stringFromDate:in_date]];
    return retStr;
}

+ (NSString *)getUIDateNow {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *str = [NSString
                     stringWithFormat:@"%@", [formatter stringFromDate:[NSDate date]]];
    return str;
}

+ (NSDate *)getUIDateFromatAnyDate:(NSDate *)anyDate {
    // 设置源日期时区
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT+0800"];
    // 设置转换后的目标日期时区
    NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];
    // 得到源日期与世界标准时间的偏移量
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:anyDate];
    // 目标日期与本地时区的偏移量
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:anyDate];
    // 得到时间偏移量的差值
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    // 转为现在时间
    NSDate* destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:anyDate];
    return destinationDateNow;
}

+ (NSString *)getUIDateXingZuo:(NSDate *)in_date {
    NSString *retStr = @"";
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"MM"];
    int i_month = 0;
    NSString *theMonth = [dateFormat stringFromDate:in_date];
    
    if ([[theMonth substringToIndex:0] isEqualToString:@"0"]) {
        i_month = [[theMonth substringFromIndex:1] intValue];
    } else {
        i_month = [theMonth intValue];
    }
    
    [dateFormat setDateFormat:@"dd"];
    int i_day = 0;
    NSString *theDay = [dateFormat stringFromDate:in_date];
    
    if ([[theDay substringToIndex:0] isEqualToString:@"0"]) {
        i_day = [[theDay substringFromIndex:1] intValue];
    } else {
        i_day = [theDay intValue];
    }
    
    /*
     *   摩羯座 12月22日------1月19日
     *   水瓶座 1月20日-------2月18日
     *   双鱼座 2月19日-------3月20日
     *   白羊座 3月21日-------4月19日
     *   金牛座 4月20日-------5月20日
     *   双子座 5月21日-------6月21日
     *   巨蟹座 6月22日-------7月22日
     *   狮子座 7月23日-------8月22日
     *   处女座 8月23日-------9月22日
     *   天秤座 9月23日------10月23日
     *   天蝎座 10月24日-----11月21日
     *   射手座 11月22日-----12月21日
     */
    
    switch (i_month) {
        case 1:
            if ((i_day >=  1) && (i_day <=  19)) {
                retStr = @"摩羯座";
            }
            
            if ((i_day >=  20) && (i_day <=  31)) {
                retStr = @"水瓶座";
            }
            
            break;
            
        case 2:
            
            if ((i_day >=  1) && (i_day <=  18)) {
                retStr = @"水瓶座";
            }
            
            if ((i_day >=  19) && (i_day <=  31)) {
                retStr = @"双鱼座";
            }
            
            break;
            
        case 3:
            
            if ((i_day >=  1) && (i_day <=  20)) {
                retStr = @"双鱼座";
            }
            
            if ((i_day >=  21) && (i_day <=  31)) {
                retStr = @"白羊座";
            }
            
            break;
            
        case 4:
            
            if ((i_day >=  1) && (i_day <=  19)) {
                retStr = @"白羊座";
            }
            
            if ((i_day >=  20) && (i_day <=  31)) {
                retStr = @"金牛座";
            }
            
            break;
            
        case 5:
            
            if ((i_day >=  1) && (i_day <=  20)) {
                retStr = @"金牛座";
            }
            
            if ((i_day >=  21) && (i_day <=  31)) {
                retStr = @"双子座";
            }
            
            break;
            
        case 6:
            
            if ((i_day >=  1) && (i_day <=  21)) {
                retStr = @"双子座";
            }
            
            if ((i_day >=  22) && (i_day <=  31)) {
                retStr = @"巨蟹座";
            }
            
            break;
            
        case 7:
            
            if ((i_day >=  1) && (i_day <=  22)) {
                retStr = @"巨蟹座";
            }
            
            if ((i_day >=  23) && (i_day <=  31)) {
                retStr = @"狮子座";
            }
            
            break;
            
        case 8:
            
            if ((i_day >=  1) && (i_day <=  22)) {
                retStr = @"狮子座";
            }
            
            if ((i_day >=  23) && (i_day <=  31)) {
                retStr = @"处女座";
            }
            
            break;
            
        case 9:
            
            if ((i_day >=  1) && (i_day <=  22)) {
                retStr = @"处女座";
            }
            
            if ((i_day >=  23) && (i_day <=  31)) {
                retStr = @"天秤座";
            }
            
            break;
            
        case 10:
            
            if ((i_day >=  1) && (i_day <=  23)) {
                retStr = @"天秤座";
            }
            
            if ((i_day >=  24) && (i_day <=  31)) {
                retStr = @"天蝎座";
            }
            
            break;
            
        case 11:
            
            if ((i_day >=  1) && (i_day <=  21)) {
                retStr = @"天蝎座";
            }
            
            if ((i_day >=  22) && (i_day <=  31)) {
                retStr = @"射手座";
            }
            
            break;
            
        case 12:
            
            if ((i_day >=  1) && (i_day <=  21)) {
                retStr = @"射手座";
            }
            
            if ((i_day >=  22) && (i_day <=  31)) {
                retStr = @"摩羯座";
            }
            
            break;
    }
    return retStr;
}

+ (NSString *)getUIDateAge:(NSDate *)birth_date {
    // 出生日期转换 年月日
    NSDateComponents *components1 = [[NSCalendar currentCalendar]
                                     components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                     fromDate:birth_date];
    NSInteger brithDateYear = [components1 year];
    NSInteger brithDateDay = [components1 day];
    NSInteger brithDateMonth = [components1 month];
    // 获取系统当前 年月日
    NSDateComponents *components2 = [[NSCalendar currentCalendar]
                                     components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                     fromDate:[NSDate date]];
    NSInteger currentDateYear = [components2 year];
    NSInteger currentDateDay = [components2 day];
    NSInteger currentDateMonth = [components2 month];
    // 计算年龄
    NSInteger iAge = currentDateYear - brithDateYear - 1;
    
    if ((currentDateMonth > brithDateMonth) ||
        ((currentDateMonth  =  brithDateMonth) &&
         (currentDateDay >=  brithDateDay))) {
            iAge++;
        }
    
    return [NSString stringWithFormat:@"%d", (int)iAge];
}

+ (BOOL)getIsNewVersion:(NSString *)version {
    BOOL isNewVersion = NO;
    if ([[[NSString stringWithFormat:@"%@",version]componentsSeparatedByString:@"."] count] == 3) {
        int newystr = [[NSString stringWithFormat:@"%@",version]componentsSeparatedByString:@"."][0].intValue;
        int newmstr = [[NSString stringWithFormat:@"%@",version]componentsSeparatedByString:@"."][1].intValue;
        int newdstr = [[NSString stringWithFormat:@"%@",version]componentsSeparatedByString:@"."][2].intValue;
        int ystr = [[NSString stringWithFormat:@"%@",[Utils getCurrentBundleShortVersion]]componentsSeparatedByString:@"."][0].intValue;
        int mstr = [[NSString stringWithFormat:@"%@",[Utils getCurrentBundleShortVersion]]componentsSeparatedByString:@"."][1].intValue;
        int dstr = [[NSString stringWithFormat:@"%@",[Utils getCurrentBundleShortVersion]]componentsSeparatedByString:@"."][2].intValue;
        if (newystr > ystr) {
            isNewVersion = YES;
        }
        if (newystr == ystr){
            if (newmstr > mstr) {
                isNewVersion = YES;
            }
            if (newmstr == mstr) {
                if (newdstr > dstr) {
                    isNewVersion = YES;
                }
            }
        }
    }
    return isNewVersion;
}


/********************* UIFont+Category Utils **********************/
+ (CGSize)getUIFontSizeFitW:(UILabel *)label {
    NSMutableParagraphStyle *paragraphStyle  =
    [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paragraphStyle.alignment = label.textAlignment;
    CGSize labelSize  =
    [label.text
     boundingRectWithSize:CGSizeMake(10000, label.frame.size.height)
     options:NSStringDrawingUsesLineFragmentOrigin |
     NSStringDrawingUsesFontLeading
     attributes:@{
                  NSFontAttributeName : label.font,
                  NSParagraphStyleAttributeName : paragraphStyle
                  } context:nil]
    .size;
    return labelSize;
}

+ (CGSize)getUIFontSizeFitH:(UILabel *)label {
    NSMutableParagraphStyle *paragraphStyle  =
    [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paragraphStyle.alignment = label.textAlignment;
    CGSize labelSize  =
    [label.text boundingRectWithSize:CGSizeMake(label.frame.size.width, 10000)
                             options:NSStringDrawingUsesLineFragmentOrigin |
     NSStringDrawingUsesFontLeading
                          attributes:@{
                                       NSFontAttributeName : label.font,
                                       NSParagraphStyleAttributeName : paragraphStyle
                                       } context:nil]
    .size;
    return labelSize;
}

+ (CGSize)getUIFontSizeFitH:(UILabel *)label withLineSpacing:(CGFloat)space {
    NSMutableParagraphStyle *paragraphStyle  =
    [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paragraphStyle.lineSpacing = space;
    paragraphStyle.alignment = label.textAlignment;
    CGSize labelSize  =
    [label.text boundingRectWithSize:CGSizeMake(label.frame.size.width, 10000)
                             options:NSStringDrawingUsesLineFragmentOrigin |
     NSStringDrawingUsesFontLeading
                          attributes:@{
                                       NSFontAttributeName : label.font,
                                       NSParagraphStyleAttributeName : paragraphStyle
                                       } context:nil]
    .size;
    return labelSize;
}

+ (void)getUILabel:(UILabel *)label withLineSpacing:(CGFloat)space {
    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] init];
    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
    style.alignment = label.textAlignment;
    style.lineSpacing = space;
    style.lineBreakMode = NSLineBreakByCharWrapping;
    NSDictionary *attributesDict = @{NSParagraphStyleAttributeName : style};
    NSAttributedString *as  =
    [[NSAttributedString alloc] initWithString:label.text
                                    attributes:attributesDict];
    [mas appendAttributedString:as];
    [label setAttributedText:mas];
}

+ (void)getUILabel:(UILabel *)label withParagraphSpacing:(CGFloat)space {
    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] init];
    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
    style.alignment = label.textAlignment;
    style.paragraphSpacing = space;
    style.lineBreakMode = NSLineBreakByCharWrapping;
    NSDictionary *attributesDict = @{NSParagraphStyleAttributeName : style};
    NSAttributedString *as  =
    [[NSAttributedString alloc] initWithString:label.text
                                    attributes:attributesDict];
    [mas appendAttributedString:as];
    [label setAttributedText:mas];
}

+ (void)getUILabel:(UILabel *)label withSpacing:(long)space {
    NSMutableAttributedString *mas =[[NSMutableAttributedString alloc]initWithString:label.text];
    CFNumberRef num = CFNumberCreate(kCFAllocatorDefault,kCFNumberSInt8Type,&space);
    [mas addAttribute:(id)kCTKernAttributeName value:(__bridge id)num range:NSMakeRange(0,[mas length])];
    CFRelease(num);
    [label setAttributedText:mas];
}

+ (NSUInteger)getUnicodeLengthOfString:(NSString *)text {
    NSUInteger asciiLength = 0;
    for (NSUInteger i = 0; i < text.length; i++) {
        unichar uc = [text characterAtIndex: i];
        asciiLength += isascii(uc) ? 1 : 2;
    }
    NSUInteger unicodeLength = asciiLength / 2;
    if(asciiLength % 2) {
        unicodeLength++;
    }
    return unicodeLength;
}


+ (CGSize)getUITextViewSizeFitH:(UITextView *)textview withLineSpacing:(CGFloat)space {
    NSMutableParagraphStyle *paragraphStyle  =
    [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.lineSpacing = space;
    paragraphStyle.alignment = textview.textAlignment;
    NSDictionary *attributesDic = @{NSFontAttributeName : textview.font,NSParagraphStyleAttributeName : paragraphStyle};
    CGSize labelSize  =
    [textview.text boundingRectWithSize:CGSizeMake(textview.frame.size.width, 10000)
                                options:NSStringDrawingUsesLineFragmentOrigin |
     NSStringDrawingUsesFontLeading
                             attributes:attributesDic context:nil]
    .size;
    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] init];
    NSAttributedString *as  =
    [[NSAttributedString alloc] initWithString:textview.text
                                    attributes:attributesDic];
    [mas appendAttributedString:as];
    [textview setAttributedText:mas];
    return labelSize;
}
//
//+ (void)getUITextView:(UITextView *)textview withLineSpacing:(CGFloat)space {
//    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] init];
//    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
//    style.alignment = textview.textAlignment;
//    style.lineSpacing = space;
//    style.lineBreakMode = NSLineBreakByWordWrapping;
//    NSDictionary *attributesDic = @{NSParagraphStyleAttributeName : style};
//    NSAttributedString *as  =
//    [[NSAttributedString alloc] initWithString:textview.text
//                                    attributes:attributesDic];
//    [mas appendAttributedString:as];
//    [textview setAttributedText:mas];
//}

/********************* Verification Utils **********************/
// 对输入的字符串验证邮箱地址
+ (BOOL)getIsValidateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest  =
    [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
}

// 对输入的字符串验证电话号码
+ (BOOL)getIsValidateNumber:(NSString *)number {
    NSString *numberRegex = @"(\\(\\d{3,4}\\)|\\d{3,4}-|\\s)?\\d{7,14}";
    NSPredicate *numberTest  =
    [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegex];
    return [numberTest evaluateWithObject:number];
}

// 对输入的字符串验证空值
+ (NSString *)getIsValidateObject:(id)object {
    if ((object == nil) || [object isEqual:[NSNull null]] ||
        ((NSNull *)object == [NSNull null])) {
        return @"";
    }
    
    return [NSString stringWithFormat:@"%@", object];
}

// 对输入的Unicode转化为汉字
+ (NSString *)getIsValidateUnicode:(NSString *)unicodeStr {
    NSString *tempStr1 = [unicodeStr stringByReplacingOccurrencesOfString:@"\\u" withString:@"\\U"];
    NSString *tempStr2 = [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 = [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *returnStr = [NSPropertyListSerialization propertyListWithData:tempData options:NSPropertyListImmutable format:NULL error:NULL];
    return [returnStr stringByReplacingOccurrencesOfString:@"\\r\\n" withString:@"\n"];
}

// 对输入的字符串验证系统表情
+ (NSString *)getIsValidateEmotion:(NSString *)emotion {
    NSData *data = [emotion dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *str  =
    [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSRegularExpression *regex  =
    [[NSRegularExpression alloc] initWithPattern:@"\\\\ud\\w{3}"
                                         options:0
                                           error:nil];
    NSArray *chunks  =
    [regex matchesInString:str options:0 range:NSMakeRange(0, [str length])];
    NSString *strTemp = str;
    
    for (int i = 0; i < [chunks count]; i++) {
        NSTextCheckingResult *b = [chunks objectAtIndex:i];
        NSString *str1 = [str substringWithRange:b.range];
        strTemp  =
        [strTemp stringByReplacingOccurrencesOfString:str1 withString:@""];
    }
    
    NSData *data1 = [strTemp dataUsingEncoding:NSUTF8StringEncoding];
    return [[NSString alloc] initWithData:data1
                                 encoding:NSNonLossyASCIIStringEncoding];
}

// 对输入的字符串验证 html字符串手机端自适应
+ (NSString *)getIsValidateHtml:(NSString *)html {
    html = [NSString
            stringWithFormat:
            @"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" "
            @"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html "
            @"xmlns = \"http://www.w3.org/1999/xhtml\"><head><meta "
            @"http-equiv = \"Content-Type\" content = \"text/html; charset = utf-8\" "
            @"/><meta content = \"width = device-width, initial-scale = 1.0, "
            @"minimum-scale = 1.0, maximum-scale = 1.0,user-scalable = no\" "
            @"name = \"viewport\" id = \"viewport\" /><style>body { text-align: "
            @"left; }a { color:#666666; text-decoration: none; }#content { "
            @"font-size:12.0pt; line-height:1.45em; font-family:'宋体'; "
            @"color:#666666; }#content p { padding:0; margin:15pt 0px; display: "
            @"block; }#content span, #content span[style], #content font, "
            @"#content font[style] { color:#666666 !important; "
            @"font-family:'宋体' !important; font-size:12.0pt "
            @"!important;}#content img, #content table { width:100%% "
            @"!important;}#content table, #content table td, #content table th "
            @"{ border:1px solid #cccccc; border-collapse:collapse; }   "
            @"</style></head><body><form><div id = \"content\" font-size:12.0pt; "
            @"font-family:'宋体'; color:#000000;>%@</div> </form></body></html>",
            html];
    return html;
}

// 对输入的字符串验证JSON
+ (NSString *)getIsValidJSONObject:(NSDictionary *)dic {
    NSString *json = @"";
    
    if ([NSJSONSerialization isValidJSONObject:dic]) {
        NSError *error;
        NSData *jsonData  =
        [NSJSONSerialization dataWithJSONObject:dic
                                        options:NSJSONWritingPrettyPrinted
                                          error:&error];
        json  =
        [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        // NSLog(@"json data:%@",json);
    }
    
    return json;
}

/********************* NSString Utils **********************/
// NSString的汉语拼音
+ (NSString *)getNSStringPinyin:(NSString *)chinese {
    NSMutableString *pinyin = [chinese mutableCopy];
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformMandarinLatin, NO);
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformStripCombiningMarks, NO);
    return [pinyin lowercaseString];
}

// NSString的手机号码判断
+ (NSString *)getNSStringPhoneNumber:(NSString *)mobile {
    mobile = [mobile stringByReplacingOccurrencesOfString:@" (" withString:@""];
    mobile = [mobile stringByReplacingOccurrencesOfString:@") " withString:@""];
    mobile = [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobile = [mobile stringByReplacingOccurrencesOfString:@"+86" withString:@""];
    return mobile;
}
// NSString的米判断
+ (NSString *)getNSStringKilometer:(NSString *)meter {
    if (meter.doubleValue < 100) {
        return [NSString stringWithFormat:@"%.2f", meter.doubleValue];
    } else {
        double result = meter.doubleValue / 1000.0;
        return [NSString stringWithFormat:@"%.2fk", result];
    }
}
// NSString的%.2f
+ (NSString *)getNSStringTwoFloat:(NSString *)value {
    if ([NSString stringWithFormat:@"%.2f", value.doubleValue].floatValue==[NSString stringWithFormat:@"%.0f", value.doubleValue].floatValue) {
        return [NSString stringWithFormat:@"%.0f", value.doubleValue];
    } else if ([NSString stringWithFormat:@"%.2f", value.doubleValue].floatValue==[NSString stringWithFormat:@"%.1f", value.doubleValue].floatValue) {
        return [NSString stringWithFormat:@"%.1f", value.doubleValue];
    } else {
        return [NSString stringWithFormat:@"%.2f", value.doubleValue];
    }
}
// NSString的千分位判断
+ (NSString *)getNSStringQian:(NSString *)coast {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSString *formattedNumberString = [formatter stringFromNumber:[NSNumber numberWithDouble:coast.doubleValue]];
    return formattedNumberString;
}
// NSString的万判断
+ (NSString *)getNSStringWan:(NSString *)coast {
    if (coast.intValue < 10000) {
        return [NSString stringWithFormat:@"%d", coast.intValue];
    } else {
        double result = coast.doubleValue / 10000;
        return [NSString stringWithFormat:@"%d万", (int)result];
    }
}
// NSString的类型判断
+ (BOOL)getNSStringTypeCheck:(NSString *)string andType:(NSString *)type {
    int alength = (int)[string length];
    for (int i = 0; i < alength; i++) {
        char commitChar = [string characterAtIndex:i];
        NSString *temp = [string substringWithRange:NSMakeRange(i, 1)];
        const char *u8Temp = [temp UTF8String];
        if (3 == strlen(u8Temp)) {
            if ([type isEqualToString:@"中文"]) {
                return YES;
            }
        } else if ((commitChar > 64) && (commitChar < 91)) {
            if ([type isEqualToString:@"字母"]) {
                return YES;
            }
        } else if ((commitChar > 96) && (commitChar < 123)) {
            if ([type isEqualToString:@"字母"]) {
                return YES;
            }
        } else if ((commitChar > 47) && (commitChar < 58)) {
            if ([type isEqualToString:@"数字"]) {
                return YES;
            }
        } else {
            if ([type isEqualToString:@"符号"]) {
                return YES;
            }
        }
    }
    return NO;
}
// NSString的类型判断
+ (BOOL)getNSStringTypeCheckNumber:(NSString *)string {
    int alength = (int)[string length];
    for (int i = 0; i < alength; i++) {
        char commitChar = [string characterAtIndex:i];
        NSString *temp = [string substringWithRange:NSMakeRange(i, 1)];
        const char *u8Temp = [temp UTF8String];
        if (3 == strlen(u8Temp)) {
            return NO;
        } else if ((commitChar > 64) && (commitChar < 91)) {
            return NO;
        } else if ((commitChar > 96) && (commitChar < 123)) {
            return NO;
        } else if ((commitChar > 47) && (commitChar < 58)) {
            // 是数字
        } else {
            return NO;
        }
    }
    return YES;
}
// 身份证号码校验
+ (BOOL)getNSStringTypeCheckIDCardNumber:(NSString *)value {
    BOOL flag;
    if (value.length <= 0) {
        flag = NO;
        return flag;
    }
    NSString *regex2 = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    return [identityCardPredicate evaluateWithObject:value];
//    
//    
//    value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    int length =0;
//    if (!value) {
//        return NO;
//    }else {
//        length = (int)value.length;
//        if (length !=15 && length !=18) {
//            return NO;
//        }
//    }
//    NSArray *areasArray =@[@"11",@"12", @"13",@"14", @"15",@"21", @"22",@"23", @"31",@"32", @"33",@"34", @"35",@"36", @"37",@"41", @"42",@"43", @"44",@"45", @"46",@"50", @"51",@"52", @"53",@"54", @"61",@"62", @"63",@"64", @"65",@"71", @"81",@"82", @"91"];
//    NSString *valueStart2 = [value substringToIndex:2];
//    BOOL areaFlag =NO;
//    for (NSString *areaCode in areasArray) {
//        if ([areaCode isEqualToString:valueStart2]) {
//            areaFlag =YES;
//            break;
//        }
//    }
//    if (!areaFlag) {
//        return false;
//    }
//    NSRegularExpression *regularExpression;
//    NSUInteger numberofMatch;
//    int year =0;
//    switch (length) {
//        case 15:
//            year = [value substringWithRange:NSMakeRange(6,2)].intValue +1900;
//            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
//                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$"
//                                                                       options:NSRegularExpressionCaseInsensitive
//                                                                         error:nil];//测试出生日期的合法性
//            }else {
//                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$"
//                                                                       options:NSRegularExpressionCaseInsensitive
//                                                                         error:nil];//测试出生日期的合法性
//            }
//            numberofMatch = [regularExpression numberOfMatchesInString:value
//                                                              options:NSMatchingReportProgress
//                                                                range:NSMakeRange(0, value.length)];
//            if(numberofMatch >0) {
//                return YES;
//            }else {
//                return NO;
//            }
//        case 18:
//            year = [value substringWithRange:NSMakeRange(6,4)].intValue;
//            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
//                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$"
//                                                                       options:NSRegularExpressionCaseInsensitive
//                                                                         error:nil];//测试出生日期的合法性
//            }else {
//                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$"
//                                                                       options:NSRegularExpressionCaseInsensitive
//                                                                         error:nil];//测试出生日期的合法性
//            }
//            numberofMatch = [regularExpression numberOfMatchesInString:value
//                                                              options:NSMatchingReportProgress
//                                                                range:NSMakeRange(0, value.length)];
//            if(numberofMatch >0) {
//                int S = ([value substringWithRange:NSMakeRange(0,1)].intValue + [value substringWithRange:NSMakeRange(10,1)].intValue) *7 + ([value substringWithRange:NSMakeRange(1,1)].intValue + [value substringWithRange:NSMakeRange(11,1)].intValue) *9 + ([value substringWithRange:NSMakeRange(2,1)].intValue + [value substringWithRange:NSMakeRange(12,1)].intValue) *10 + ([value substringWithRange:NSMakeRange(3,1)].intValue + [value substringWithRange:NSMakeRange(13,1)].intValue) *5 + ([value substringWithRange:NSMakeRange(4,1)].intValue + [value substringWithRange:NSMakeRange(14,1)].intValue) *8 + ([value substringWithRange:NSMakeRange(5,1)].intValue + [value substringWithRange:NSMakeRange(15,1)].intValue) *4 + ([value substringWithRange:NSMakeRange(6,1)].intValue + [value substringWithRange:NSMakeRange(16,1)].intValue) *2 + [value substringWithRange:NSMakeRange(7,1)].intValue *1 + [value substringWithRange:NSMakeRange(8,1)].intValue *6 + [value substringWithRange:NSMakeRange(9,1)].intValue *3;
//                int Y = S %11;
//                NSString *M = @"F";
//                NSString *JYM = @"10X98765432";
//                M = [JYM substringWithRange:NSMakeRange(Y,1)];// 判断校验位
//                if ([M isEqualToString:[value substringWithRange:NSMakeRange(17,1)]]) {
//                    return YES;// 检测ID的校验位
//                }else {
//                    return NO;
//                }
//            }else {
//                return NO;
//            }
//        default:
//            return NO;
//    }
}

/********************* Secret Utils **********************/
// 图片BASE64位编码
+ (NSString *)getBase64StringImage:(UIImage *)senderImage {
    NSData *dataObj = UIImagePNGRepresentation(senderImage);
    if (UIImagePNGRepresentation(senderImage) == nil) {
        dataObj = UIImageJPEGRepresentation(senderImage, 1.0);
    }
    NSString *dataStr = [GGTMBase64 stringByEncodingData:dataObj];
    return dataStr;
}

// 字符串中英文长度
+ (int)getSecretStringLength:(NSString *)string {
    int strlength = 0;
    char *p = (char *)[string cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i = 0;
         i < [string lengthOfBytesUsingEncoding:NSUnicodeStringEncoding]; i++) {
        if (*p) {
            p++;
            strlength++;
        } else {
            p++;
        }
    }
    return (strlength + 1) / 2;
}

// 字符串MD5处理32位
+ (NSString *)getSecretString32MD5:(NSString *)string {
    const char *cStr = [string UTF8String];
    unsigned char result[32];
    
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    return [[NSString
             stringWithFormat:
             @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
             result[0], result[1], result[2], result[3], result[4], result[5],
             result[6], result[7], result[8], result[9], result[10], result[11],
             result[12], result[13], result[14], result[15]] uppercaseString];
}

// 字符串MD5处理16位
+ (NSString *)getSecretString16MD5:(NSString *)string {
    const char *cStr = [string UTF8String];
    unsigned char result[16];
    
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    return [[NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X",
             result[4], result[5], result[6], result[7],
             result[8], result[9], result[10],
             result[11]] lowercaseString];
}

// 字符串SHA1处理
+ (NSString *)getSecretStringSHA1:(NSString *)string {
    const char *cstr = [string cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:string.length];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (CC_LONG)data.length, digest);
    NSMutableString *output  =
    [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

// 字符串SHA256处理
+ (NSString *)getSecretStringSHA256:(NSString *)string {
    const char *cstr = [string cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:string.length];
    uint8_t digest[CC_SHA256_DIGEST_LENGTH];
    
    CC_SHA256(data.bytes, (CC_LONG)data.length, digest);
    NSMutableString *output  =
    [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

// 字符串反转处理
+ (NSString *)getSecretStringReverse:(NSString *)string {
    int length = (int)string.length;
    NSMutableString *reversedString  =
    [[NSMutableString alloc] initWithCapacity:length];
    while (length > 0) {
        [reversedString
         appendString:[NSString
                       stringWithFormat:@"%C",
                       [string characterAtIndex:--length]]];
    }
    return reversedString;
}

// 字符串按位异或处理
+ (NSString *)getSecretStringXOR:(NSString *)string andKey:(NSString *)key {
    int keyLength = (int)key.length;
    int strLength = (int)string.length;
    int c = 0;
    NSMutableString *returnStr  =
    [[NSMutableString alloc] initWithCapacity:strLength];
    for (int i = 0; i < strLength; i++) {
        if (c == keyLength) {
            c = 0;
        }
        [returnStr
         appendString:[NSString stringWithFormat:@"%c%c",
                       [key characterAtIndex:c],
                       ([string characterAtIndex:i] ^
                        [key characterAtIndex:c])]];
        c++;
    }
    return returnStr;
}

// 字符串DES处理
+ (NSString *)getSecretStringDES:(NSString *)string andKey:(NSString *)key {
    return [self encrypt:string encryptOrDecrypt:kCCEncrypt key:key];
}

// 字符串十六进制处理
+ (NSString *)getSecretStringHex:(NSString *)string {
    NSData *myD = [string dataUsingEncoding:NSUTF8StringEncoding];
    Byte *bytes = (Byte *)[myD bytes];
    NSString *hexStr = @"";
    for (int i = 0; i < [myD length]; i++) {
        NSString *newHexStr  =
        [NSString stringWithFormat:@"%x", bytes[i] & 0xff]; /// 16进制数
        if ([newHexStr length] == 1) {
            hexStr = [NSString stringWithFormat:@"%@0%@", hexStr, newHexStr];
        } else {
            hexStr = [NSString stringWithFormat:@"%@%@", hexStr, newHexStr];
        }
    }
    return hexStr;
}

// 字符串Base64处理
+ (NSString *)getSecretStringBase64:(NSString *)string {
    NSData *nsdata = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [GGTMBase64 stringByEncodingData:nsdata];
    return base64Encoded;
}

// Des
+ (NSString *)encrypt:(NSString *)sText
     encryptOrDecrypt:(CCOperation)encryptOperation
                  key:(NSString *)key {
    const void *dataIn;
    size_t dataInLength;
    
    if (encryptOperation == kCCDecrypt) //传递过来的是decrypt 解码
    {
        //解码 base64
        NSData *decryptData = [GGTMBase64
                               decodeData:
                               [sText dataUsingEncoding:NSUTF8StringEncoding]]; //转成utf-8并decode
        dataInLength = [decryptData length];
        dataIn = [decryptData bytes];
    } else // encrypt
    {
        NSData *encryptData = [sText dataUsingEncoding:NSUTF8StringEncoding];
        dataInLength = [encryptData length];
        dataIn = (const void *)[encryptData bytes];
    }
    
    /*
     DES加密 ：用CCCrypt函数加密一下,然后用base64编码下,传过去
     DES解密
     ：把收到的数据根据base64,decode一下,然后再用CCCrypt函数解密,得到原本的数据
     */
    unsigned long *dataOut = NULL; //可以理解位type/typedef
    //的缩写（有效的维护了代码,比如：一个人用int,一个人用long。最好用typedef来定义）
    size_t dataOutAvailable = 0; // size_t  是操作符sizeof返回的结果类型
    size_t dataOutMoved = 0;
    
    dataOutAvailable  =
    (dataInLength + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    dataOut = malloc(dataOutAvailable * sizeof(unsigned long));
    memset((void *)dataOut, 0x0,
           dataOutAvailable); //将已开辟内存空间buffer的首 1 个字节的值设为值 0
    
    NSString *initIv = @"!= 830*clock#";
    const void *vkey = (const void *)[key UTF8String];
    const void *iv = (const void *)[initIv UTF8String];
    
    // CCCrypt函数 加密/解密
    CCCrypt(encryptOperation, //  加密/解密
            kCCAlgorithm3DES, //  加密根据哪个标准
            kCCOptionECBMode, //  选项分组密码算法
            vkey,             //密钥    加密和解密的密钥必须一致
            kCCKeySize3DES,   //   DES 密钥的大小（
            iv,               //  可选的初始矢量
            dataIn,           // 数据的存储单元
            dataInLength,     // 数据的大小
            (void *)dataOut,  // 用于返回数据
            dataOutAvailable, &dataOutMoved);
    NSString *result = nil;
    NSData *data = [NSData dataWithBytes:(const void *)dataOut
                                  length:(NSUInteger)dataOutMoved];
    free(dataOut);
    if (encryptOperation == kCCDecrypt) // encryptOperation = 1  解码
    {
        //得到解密出来的data数据,改变为utf-8的字符串
        result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    } else // encryptOperation = 0  （加密过程中,把加好密的数据转成base64的）
    {
        //编码 base64
        result = [GGTMBase64 stringByEncodingData:data];
    }
    return result;
}

@end
