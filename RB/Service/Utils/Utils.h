//
//  Utils.h
//
//  Created by AngusNi on 14/12/3.
//  Copyright (c) 2014年 AngusNi. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonHMAC.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <netdb.h>
#import <sys/utsname.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Security/Security.h>
#import "GGTMBase64.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import <Accelerate/Accelerate.h>
#import <CoreText/CoreText.h>
#import <AdSupport/ASIdentifierManager.h>
#import <AddressBook/AddressBook.h>
#import <QuartzCore/QuartzCore.h>


@interface Utils : NSObject

/********************* File Utils **********************/

// 文件写入
+ (BOOL)getFileWriteImage:(NSURL*)url andFileName:(NSString *)fileName andFilePath:(NSString *)filePath;
// 文件读取
+ (UIImage*)getFileReadImageWithFileName:(NSString *)fileName andFilePath:(NSString *)filePath;

// 文件写入
+ (BOOL)getFileArrayWrite:(NSMutableArray*)array andFileName:(NSString *)fileName andFilePath:(NSString *)filePath;
// 文件读取
+ (NSMutableArray*)getFileArrayReadWithFileName:(NSString *)fileName andFilePath:(NSString *)filePath;


// 文件写入
+ (BOOL)getFileWrite:(id)jsonObject
         andFileName:(NSString *)fileName
         andFilePath:(NSString *)filePath;
// 文件读取
+ (NSArray *)getFileReadWithFileName:(NSString *)fileName
                         andFilePath:(NSString *)filePath;
// 文件删除
+ (BOOL)getFileDeleteWithFileName:(NSString *)fileName
                      andFilePath:(NSString *)filePath;

/********************* Current Utils **********************/
// 设备型号
+ (NSString *)getCurrentDeviceVersion;
// 设备系统号
+ (NSString *)getCurrentDeviceSystemVersion;
// 应用版本
+ (NSString *)getCurrentBundleShortVersion;
// 应用名
+ (NSString *)getCurrentBundleName;
// 应用Build版本
+ (NSString *)getCurrentBundleVersion;
// 应用Identifier
+ (NSString *)getCurrentBundleIdentifier;
// 应用临时目录
+ (NSString *)getCurrentLibraryCachesDocument;

/********************* UIViewController Utils **********************/
+ (void)getIOS7UINavigationController:(UIViewController *)viewContr;
+ (float)getIOS7TabBarHeight;

/********************* NetWork Utils **********************/
+ (NSString *)getNetWorkError:(NSError *)error;
+ (NSString *)getNetWorkName;
+ (NSString *)getNetWorkType;
+ (NSString *)getNetWorkMac;
+ (BOOL)getNetWorkConnected;

/********************* Category Utils **********************/
+ (UIColor *)getUIColorWithHexString:(NSString *)hexStr andAlpha:(float)alpha;
+ (UIColor *)getUIColorWithHexString:(NSString *)hexStr;
+ (UIColor *)getUIColorRandom;
+ (UIImage *)narrowWithImage:(UIImage*)image;
+ (CGSize)getUIImageSizeWithURL:(NSString*)urlStr;
+ (UIImage *)getUIImageDefaultWithRect:(CGRect)rect;
+ (UIImage *)getUIImageScalingFromSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize;
+ (UIImage*)getUIImageByScalingAndCroppingForSize:(CGSize)targetSize sourceImage:(UIImage *)sourceImage;
+ (UIImage *)getUIImageBlurry:(UIImage *)image withBlur:(CGFloat)blur;
+ (NSString*)getUIImageSave:(UIImage*)orgImg andPicName:(NSString*)picName;
+ (BOOL)getUIFileSave:(NSData*)tempData Name:(NSString*)tempName Path:(NSString*)filePath;
+ (UIImage *)thumbnailForAsset:(ALAsset *)asset maxPixelSize:(int)size;

/********************* UIDate+Category Utils **********************/
+ (NSString *)getUIDateCompareNow:(NSString *)dateStr;
+ (NSString *)getUIDateCompareNowFortrailer:(NSString *)dateStr;
+ (NSString *)getUIDateCompareNowForEnglish:(NSString *)dateStr;
+ (NSString *)getUIDateCompare:(NSString *)dateStr;
+ (NSString *)getUIDateWeek:(NSDate *)in_date;
+ (NSString *)getUIDateNow;
+ (NSDate *)getUIDateFromatAnyDate:(NSDate *)anyDate;
+ (NSString *)getUIDateXingZuo:(NSDate *)in_date;
+ (NSString *)getUIDateAge:(NSDate *)birth_date;
+ (BOOL)getIsNewVersion:(NSString *)version;

/********************* UIFont+Category Utils **********************/
+ (CGSize)getUIFontSizeFitW:(UILabel *)label;
+ (CGSize)getUIFontSizeFitH:(UILabel *)label;
+ (CGSize)getUIFontSizeFitH:(UILabel *)label withLineSpacing:(CGFloat)space;
+ (void)getUILabel:(UILabel *)label withLineSpacing:(CGFloat)space;
+ (void)getUILabel:(UILabel *)label withParagraphSpacing:(CGFloat)space;
+ (void)getUILabel:(UILabel *)label withSpacing:(long)space;
+ (NSUInteger)getUnicodeLengthOfString:(NSString *)text;

+ (CGSize)getUITextViewSizeFitH:(UITextView *)textview withLineSpacing:(CGFloat)space;
//+ (void)getUITextView:(UITextView *)textview withLineSpacing:(CGFloat)space;

/********************* Verification Utils **********************/
// 对输入的字符串验证邮箱地址
+ (BOOL)getIsValidateEmail:(NSString *)email;
// 对输入的字符串验证电话号码
+ (BOOL)getIsValidateNumber:(NSString *)number;
// 对输入的字符串验证空值
+ (NSString *)getIsValidateObject:(id)object;
// 对输入的Unicode转化为汉字
+ (NSString *)getIsValidateUnicode:(NSString *)unicodeStr;
// 对输入的字符串验证系统表情
+ (NSString *)getIsValidateEmotion:(NSString *)emotion;
// 对输入的字符串验证 html字符串手机端自适应
+ (NSString *)getIsValidateHtml:(NSString *)html;
// 对输入的字符串验证JSON
+ (NSString *)getIsValidJSONObject:(NSDictionary *)dic;

/********************* NSString Utils **********************/
// NSString的汉语拼音
+ (NSString *)getNSStringPinyin:(NSString *)chinese;
// NSString的手机号码判断
+ (NSString *)getNSStringPhoneNumber:(NSString *)mobile;
// NSString的米判断
+ (NSString *)getNSStringKilometer:(NSString *)meter;
// NSString的%.2f
+ (NSString *)getNSStringTwoFloat:(NSString *)value;
// NSString的千分位判断
+ (NSString *)getNSStringQian:(NSString *)coast;
// NSString的万判断
+ (NSString *)getNSStringWan:(NSString *)coast;
// NSString的类型判断
+ (BOOL)getNSStringTypeCheck:(NSString *)string andType:(NSString*)type;
// NSString的类型判断-数字
+ (BOOL)getNSStringTypeCheckNumber:(NSString *)string;
// 身份证号码校验
+ (BOOL)getNSStringTypeCheckIDCardNumber:(NSString *)value;
/********************* Secret Utils **********************/
// 图片BASE64位编码
+ (NSString*)getBase64StringImage:(UIImage *)senderImage;
// 字符串中英文长度
+ (int)getSecretStringLength:(NSString *)string;
// 字符串MD5处理32位
+ (NSString *)getSecretString32MD5:(NSString *)string;
// 字符串MD5处理16位
+ (NSString *)getSecretString16MD5:(NSString *)string;
// 字符串SHA1处理
+ (NSString *)getSecretStringSHA1:(NSString *)string;
// 字符串SHA256处理
+ (NSString *)getSecretStringSHA256:(NSString *)string;
// 字符串反转处理
+ (NSString *)getSecretStringReverse:(NSString *)string;
// 字符串按位异或处理
+ (NSString *)getSecretStringXOR:(NSString *)string andKey:(NSString*)key;
// 字符串DES处理
+ (NSString *)getSecretStringDES:(NSString *)string andKey:(NSString*)key;
// 字符串十六进制处理
+ (NSString *)getSecretStringHex:(NSString *)string;
// 字符串Base64处理
+ (NSString *)getSecretStringBase64:(NSString *)string;

@end
