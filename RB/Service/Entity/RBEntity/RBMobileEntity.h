//
//  RBMobileEntity.h
//  RB
//
//  Created by RB8 on 2017/8/23.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBMobileEntity : NSObject
@property(nonatomic,strong)NSString * mobile;
@property(nonatomic,strong)NSString * name;
@property(nonatomic,strong)NSString * status;
@end
