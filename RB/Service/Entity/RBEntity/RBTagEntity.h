//
//  RBTagEntity.h
//  RB
//
//  Created by AngusNi on 16/2/17.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBTagEntity : NSObject
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *label;
@property(nonatomic,strong) NSString *cover_url;
@property(nonatomic,strong) NSString *freq;
@property(nonatomic,strong) NSString *probability;
@property(nonatomic,strong) NSString *ratio;
@end
