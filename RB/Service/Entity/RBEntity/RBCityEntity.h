//
//  RBCityEntity.h
//  RB
//
//  Created by AngusNi on 16/2/17.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBCityEntity : NSObject
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *name_en;
@end
