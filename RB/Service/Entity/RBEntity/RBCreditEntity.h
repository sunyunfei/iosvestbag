//
//  RBCreditEntity.h
//  RB
//
//  Created by RB8 on 2018/5/30.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBCreditEntity : NSObject
@property(nonatomic,strong)NSString * amount;
@property(nonatomic,strong)NSString * direct;
@property(nonatomic,strong)NSString * remark;
@property(nonatomic,strong)NSString * score;
@property(nonatomic,strong)NSString * show_time;
@property(nonatomic,strong)NSString * trade_no;
@end
