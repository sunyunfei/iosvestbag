//
//  RBPushEntity.h
//  RB
//
//  Created by AngusNi on 16/2/23.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBPushEntity : NSObject

@property(nonatomic,strong) NSString *action;
@property(nonatomic,strong) NSString *income;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *unread_message_count;
@property(nonatomic,strong) NSString *name;
@end
