//
//  RBAnalysisWechatEntity.h
//  RB
//
//  Created by AngusNi on 5/24/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBAnalysisWechatEntity : NSObject
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *nick_name;
@property(nonatomic,strong) NSString *logo_url;
@property(nonatomic,strong) NSString *user_name;
@property(nonatomic,strong) NSString *total_followers_count;
@property(nonatomic,strong) NSString *nnew_followers_count;
@property(nonatomic,strong) NSString *cancel_followers_count;
@property(nonatomic,strong) NSString *send_message_user_count;
@property(nonatomic,strong) NSString *send_message_count;
@property(nonatomic,strong) NSString *target_user_count;
@property(nonatomic,strong) NSString *read_user_count;
// WEIBO
@property(nonatomic,strong) NSString *incremental_follower_number;
@property(nonatomic,strong) NSString *decremental_follower_number;
@property(nonatomic,strong) NSString *verified_follower_ratio;
@property(nonatomic,strong) NSString *unverified_follower_ratio;
@property(nonatomic,strong) NSString *friend_number;
@property(nonatomic,strong) NSString *bilateral_number;
@property(nonatomic,strong) NSString *statuses_number;
@property(nonatomic,strong) NSString *follower_number;
//
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *unfollowed_at;
@property(nonatomic,strong) NSString *profile_image_url;
@end
