//
//  RBUserBigVEntity.h
//
//  Created by AngusNi on 15/7/13.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RBUserBigVEntity : NSObject
@property(nonatomic,strong) NSString *provider;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *follower_count;
@property(nonatomic,strong) NSString *belong_field;
@property(nonatomic,strong) NSString *headline_price;
@property(nonatomic,strong) NSString *second_price;
@property(nonatomic,strong) NSString *single_price;
@end