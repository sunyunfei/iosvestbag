//
//  RBKOLBannerEntity.h
//  RB
//
//  Created by AngusNi on 16/8/4.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBKOLBannerEntity : NSObject
@property(nonatomic,strong) NSString *category;
@property(nonatomic,strong) NSString *link;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *kol_id;
@property(nonatomic,strong) NSString *cover_url;

@end
