//
//  RBAnalysisListEntity.h
//
//  Created by AngusNi on 15/7/13.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RBAnalysisListEntity : NSObject
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *provider;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *user_name;
@property(nonatomic,strong) NSString *nick_name;
@property(nonatomic,strong) NSString *avatar_url;
@property(nonatomic,strong) NSString *location;
@property(nonatomic,strong) NSString *gender;
@property(nonatomic,strong) NSString *valid;
@end