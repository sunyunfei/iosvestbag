//
//  RBNlpEntity.h
//
//  Created by AngusNi on 15/7/13.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RBNlpEntity : NSObject
@property(nonatomic,strong) NSString *text;
@property(nonatomic,strong) NSMutableArray *keywords;
@property(nonatomic,strong) NSMutableArray *sentiment;
@property(nonatomic,strong) NSMutableArray *persons_brands;
@property(nonatomic,strong) NSMutableArray *products;
@property(nonatomic,strong) NSMutableArray *categories;
@property(nonatomic,strong) NSArray *cities;

@property(nonatomic,strong) NSMutableArray *gender_analysis;
@property(nonatomic,strong) NSMutableArray *age_analysis;
@property(nonatomic,strong) NSMutableArray *tag_analysis;
@property(nonatomic,strong) NSMutableArray *region_analysis;
@end
