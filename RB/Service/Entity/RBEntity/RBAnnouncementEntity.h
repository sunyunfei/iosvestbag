//
//  RBAnnouncementEntity.h
//
//  Created by AngusNi on 15/7/13.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RBAnnouncementEntity : NSObject
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *url;
@property(nonatomic,strong) NSString *detail_type;
@property(nonatomic,strong) NSString *banner_url;

@end