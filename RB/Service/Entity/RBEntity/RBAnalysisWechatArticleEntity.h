//
//  RBAnalysisWechatArticleEntity.h
//  RB
//
//  Created by AngusNi on 5/24/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBAnalysisWechatArticleEntity : NSObject
@property(nonatomic,strong) NSString *article_id;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *publish_date;
@property(nonatomic,strong) NSString *target_user_count;
@property(nonatomic,strong) NSString *read_user_count;
@property(nonatomic,strong) NSString *share_user_count;
@end
