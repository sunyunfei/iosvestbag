//
//  RBAmountEntity.h
//  RB
//
//  Created by AngusNi on 16/2/19.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBAmountEntity : NSObject
@property(nonatomic,strong) NSString *amount;
@property(nonatomic,strong) NSString *frozen_amount;
@property(nonatomic,strong) NSString *avail_amount;
@property(nonatomic,strong) NSString *total_income;
@property(nonatomic,strong) NSString *total_withdraw;
@property(nonatomic,strong) NSString *total_expense;
@property(nonatomic,strong) NSString *today_income;
@property(nonatomic,strong) NSString *verifying_income;
@property(nonatomic,strong) NSMutableArray *stats;
@property(nonatomic,strong) NSString *date;
@property(nonatomic,strong) NSString *total_amount;
@property(nonatomic,strong) NSString *count;
@end
