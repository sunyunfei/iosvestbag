//
//  RBIncomeEntity.h
//
//  Created by AngusNi on 15/7/13.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RBIncomeEntity : NSObject
@property(nonatomic,strong) NSString *credits;
@property(nonatomic,strong) NSString *amount;
@property(nonatomic,strong) NSString *avail_amount;
@property(nonatomic,strong) NSString *frozen_amount;
@property(nonatomic,strong) NSString *subject;
@property(nonatomic,strong) NSString *direct;
@property(nonatomic,strong) NSString *created_at;
@property(nonatomic,strong) NSString *pay_way;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *direct_text;

@end