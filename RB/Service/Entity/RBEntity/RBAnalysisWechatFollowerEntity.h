//
//  RBAnalysisWechatFollowerEntity.h
//  RB
//
//  Created by AngusNi on 5/24/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBAnalysisWechatFollowerEntity : NSObject
@property(nonatomic,strong) NSString *report_time;
@property(nonatomic,strong) NSString *nnew_followers_count;
@property(nonatomic,strong) NSString *cancel_followers_count;
@property(nonatomic,strong) NSString *growth_followers_count;
@property(nonatomic,strong) NSString *total_followers_count;
// WEIBO
@property(nonatomic,strong) NSString *r_date;
@property(nonatomic,strong) NSString *total_number;
@property(nonatomic,strong) NSString *verified_number;
@property(nonatomic,strong) NSString *unverified_number;
@end
