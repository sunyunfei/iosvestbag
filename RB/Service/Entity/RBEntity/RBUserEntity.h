//
//  RBUserEntity.h
//
//  Created by AngusNi on 15/7/13.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RBUserEntity : NSObject
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *mobile_number;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *gender;
@property(nonatomic,strong) NSString *date_of_birthday;
@property(nonatomic,strong) NSString *alipay_account;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *country;
@property(nonatomic,strong) NSString *app_city_label;
@property(nonatomic,strong) NSString *app_city;
@property(nonatomic,strong) NSMutableArray *tags;
@property(nonatomic,strong) NSString *issue_token;
@property(nonatomic,strong) NSString *kol_uuid;
@property(nonatomic,strong) NSString *influence_score;
@property(nonatomic,strong) NSString *influence_level;
@property(nonatomic,strong) NSString *rank_index;
@property(nonatomic,strong) NSString *selected_like_articles;
@property(nonatomic,strong) NSString *rongcloud_token;
@property(nonatomic,strong) NSString *age;
@property(nonatomic,strong) NSString *weixin_friend_count;

@property(nonatomic,strong) NSString *avatar_url;
@property(nonatomic,strong) NSString *kol_name;
@property(nonatomic,strong) NSString *avail_click;
@property(nonatomic,strong) NSString *total_click;

@property(nonatomic,strong) NSString *kol_role;
@property(nonatomic,strong) NSString *role_apply_status;
@property(nonatomic,strong) NSString *kol_check_remark;

@property(nonatomic,strong)NSMutableArray * socialArray;
@property(nonatomic,strong)NSString * is_new_member;

@end
