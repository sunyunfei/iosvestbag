//
//  RBPromoteEntity.h
//  RB
//
//  Created by RB8 on 2018/5/24.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBPromoteEntity : NSObject
@property(nonatomic,strong)NSString * created_at;
@property(nonatomic,strong)NSString * campaignDes;
@property(nonatomic,strong)NSString * end_at;
@property(nonatomic,strong)NSString * expired_at;
@property(nonatomic,strong)NSString * ProID;
@property(nonatomic,strong)NSString * min_credit;
@property(nonatomic,strong)NSString * rate;
@property(nonatomic,strong)NSString * start_at;
@property(nonatomic,strong)NSString * state;
@property(nonatomic,strong)NSString * title;
@property(nonatomic,strong)NSString * updated_at;
@property(nonatomic,strong)NSString * valid_days_count;
@end
