//
//  RBIndianaEntity.h
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBIndianaEntity : NSObject
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *code;
@property(nonatomic,strong) NSString *published_at;
@property(nonatomic,strong) NSString *total_number;
@property(nonatomic,strong) NSString *actual_number;
@property(nonatomic,strong) NSString *status;
@property(nonatomic,strong) NSString *poster_url;
@property(nonatomic,strong) NSString *kol_amount;
@property(nonatomic,strong) NSString *idescription;
@property(nonatomic,strong) NSString *lucky_number;
@property(nonatomic,strong) NSString *draw_at;
@property(nonatomic,strong) NSArray *pictures;
@property(nonatomic,strong) NSString *winner_name;
@property(nonatomic,strong) NSString *winner_avatar_url;
@property(nonatomic,strong) NSString *winner_token_number;

@property(nonatomic,strong) NSArray *tickets;
@property(nonatomic,strong) NSString *token_number;
@end