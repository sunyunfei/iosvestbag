//
//  RBAnalysisWechatLocationEntity.h
//  RB
//
//  Created by AngusNi on 5/24/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBAnalysisWechatLocationEntity : NSObject
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *code;
@property(nonatomic,strong) NSString *location;
@property(nonatomic,strong) NSString *number;
@property(nonatomic,strong) NSString *ratio;
@end
