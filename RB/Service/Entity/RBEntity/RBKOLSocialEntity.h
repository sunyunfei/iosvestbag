//
//  RBKOLSocialEntity.h
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBKOLSocialEntity : NSObject
@property(nonatomic,strong) NSString *provider;
@property(nonatomic,strong) NSString *provider_name;
@property(nonatomic,strong) NSString *uid;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *price;
@property(nonatomic,strong) NSString *homepage;
@property(nonatomic,strong) NSString *avatar_url;
@property(nonatomic,strong) NSString *brief;
@property(nonatomic,strong) NSString *followers_count;
@property(nonatomic,strong) NSString *reposts_count;
@property(nonatomic,strong) NSString *statuses_count;
@property(nonatomic,strong) NSString *search_kol_id;
@property(nonatomic,strong) NSString *kol_id;
@end
