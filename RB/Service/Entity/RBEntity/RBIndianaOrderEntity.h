//
//  RBIndianaOrderEntity.h
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBIndianaOrderEntity : NSObject
@property(nonatomic,strong) NSString *created_at;
@property(nonatomic,strong) NSString *number;
@property(nonatomic,strong) NSString *credits;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *avatar_url;
@end
