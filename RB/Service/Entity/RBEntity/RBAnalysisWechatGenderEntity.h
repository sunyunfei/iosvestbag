//
//  RBAnalysisWechatGenderEntity.h
//  RB
//
//  Created by AngusNi on 5/24/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBAnalysisWechatGenderEntity : NSObject
@property(nonatomic,strong) NSString *male_number;
@property(nonatomic,strong) NSString *female_number;
@property(nonatomic,strong) NSString *male_ratio;
@property(nonatomic,strong) NSString *female_ratio;

@end
