//
//  RBRankingTagsEntity.h
//  RB
//
//  Created by AngusNi on 5/10/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBRankingTagsEntity : NSObject
@property(nonatomic,strong) NSString *text;
@property(nonatomic,strong) NSString *weight;
@end
