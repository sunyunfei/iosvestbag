//
//  RBNotificationEntity.h
//  RB
//
//  Created by AngusNi on 16/2/19.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBNotificationEntity : NSObject
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *message_type;
@property(nonatomic,strong) NSString *is_read;
@property(nonatomic,strong) NSString *url;
@property(nonatomic,strong) NSString *logo_url;
@property(nonatomic,strong) NSString *sender;
@property(nonatomic,strong) NSString *item_id;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *read_at;
@property(nonatomic,strong) NSString *created_at;
@property(nonatomic,strong) NSString *sub_message_type;
@property(nonatomic,strong) NSString *sub_sub_message_type;
@end