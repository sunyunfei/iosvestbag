//
//  RBAdvertiserCampaignEntity.h
//
//  Created by AngusNi on 15/7/13.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RBAdvertiserCampaignEntity : NSObject
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *idescription;
@property(nonatomic,strong) NSString *status;
@property(nonatomic,strong) NSString *url;
@property(nonatomic,strong) NSString *img_url;
@property(nonatomic,strong) NSString *per_budget_type;
@property(nonatomic,strong) NSString *per_action_budget;
@property(nonatomic,strong) NSString *budget;
@property(nonatomic,strong) NSString *need_pay_amount;
@property(nonatomic,strong) NSString *voucher_amount;
@property(nonatomic,strong) NSString *used_voucher;
@property(nonatomic,strong) NSString *deadline;
@property(nonatomic,strong) NSString *start_time;
@property(nonatomic,strong) NSString *kol_amount;
@property(nonatomic,strong) NSArray  *invalid_reasons;
@property(nonatomic,strong) NSString *budget_editable;
@property(nonatomic,strong) NSString *avail_click;
@property(nonatomic,strong) NSString *total_click;
@property(nonatomic,strong) NSString *take_budget;
@property(nonatomic,strong) NSString *share_times;
@property(nonatomic,strong) NSString *cal_settle_time;

@property(nonatomic,strong) NSString *gender;
@property(nonatomic,strong) NSString *age;
@property(nonatomic,strong) NSString *region;
@property(nonatomic,strong) NSString *tag_labels;

@property(nonatomic,strong) NSArray  *stats_data;
@property(nonatomic,strong) NSString *sub_type;
@property(nonatomic,strong) NSString * kol_credit;
@property(nonatomic,strong) NSString * used_credits;
@end
