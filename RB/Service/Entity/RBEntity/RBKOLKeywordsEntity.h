//
//  RBKOLKeywordsEntity.h
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBKOLKeywordsEntity : NSObject
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *weight;
@end
