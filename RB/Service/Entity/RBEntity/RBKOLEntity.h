//
//  RBKOLEntity.h
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBKOLEntity : NSObject
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *age;
@property(nonatomic,strong) NSString *gender;
@property(nonatomic,strong) NSString *job_info;
@property(nonatomic,strong) NSString *avatar_url;
@property(nonatomic,strong) NSString *app_city_label;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *role_apply_status;
@property(nonatomic,strong) NSString *kol_role;
@property(nonatomic,strong) NSString *role_check_remark;
@property(nonatomic,strong) NSString * email;

@property(nonatomic,strong) NSMutableArray *tags;
@property(nonatomic,strong) NSMutableArray *kol_shows;
@property(nonatomic,strong) NSMutableArray *kol_keywords;
@property(nonatomic,strong) NSMutableArray *social_accounts;

@property(nonatomic,strong) NSString *share_forecast_commission;
@property(nonatomic,strong) NSString *share_settled_commission;

@end
