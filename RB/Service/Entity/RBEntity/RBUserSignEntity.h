//
//  RBUserSignEntity.h
//
//  Created by AngusNi on 15/7/13.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RBUserSignEntity : NSObject
@property(nonatomic,strong) NSString *continuous_checkin_count;
@property(nonatomic,strong) NSString *today_had_check_in;
@property(nonatomic,strong) NSArray *checkin_history;
@property(nonatomic,strong) NSString * total_check_in_amount;
@property(nonatomic,strong) NSString * today_already_amount;
@property(nonatomic,strong) NSString * tomorrow_can_amount;
@property(nonatomic,strong) NSString * total_check_in_days;
@property(nonatomic,strong) NSString * today_can_amount;
@property(nonatomic,strong) NSString * is_show_newbie;
@property(nonatomic,strong) NSString * red_money_count;
@property(nonatomic,strong) NSString * campaign_invites_count;
@property(nonatomic,strong) NSString * invite_friends;
@end
