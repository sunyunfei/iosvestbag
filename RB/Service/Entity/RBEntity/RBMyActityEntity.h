//
//  RBMyActityEntity.h
//  RB
//
//  Created by RB8 on 2017/11/7.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBMyActityEntity : NSObject
@property(nonatomic,strong)NSString * campaign_id;
@property(nonatomic,strong)NSString * earn_money;
@property(nonatomic,strong)NSString * iid;
@property(nonatomic,strong)NSString * img_status;
@property(nonatomic,strong)NSString * status;
@property(nonatomic,strong)NSString * reject_reason;
@property(nonatomic,strong)NSString * campaign_name;
@property(nonatomic,strong)NSString * per_action_type;
@property(nonatomic,strong)NSString * screenshot;
@property(nonatomic,strong)NSString * per_budget_type;

@end
