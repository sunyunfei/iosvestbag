//
//  RBAnalysisWechatMessageEntity.h
//  RB
//
//  Created by AngusNi on 5/24/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBAnalysisWechatMessageEntity : NSObject
@property(nonatomic,strong) NSString *report_time;
@property(nonatomic,strong) NSString *send_message_user_count;
@property(nonatomic,strong) NSString *send_message_count;
@property(nonatomic,strong) NSString *per_send_message_count;
@end
