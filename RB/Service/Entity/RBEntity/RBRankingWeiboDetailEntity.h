//
//  RBRankingWeiboDetailEntity.h
//  RB
//
//  Created by AngusNi on 5/10/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBRankingWeiboDetailEntity : NSObject
@property(nonatomic,strong) NSString *comments;
@property(nonatomic,strong) NSString *device;
@property(nonatomic,strong) NSString *kol_avatar_url;
@property(nonatomic,strong) NSString *kol_name;
@property(nonatomic,strong) NSString *likes;
@property(nonatomic,strong) NSString *retweets;
@property(nonatomic,strong) NSString *summary;
@property(nonatomic,strong) NSString *url;
@property(nonatomic,strong) NSString *user_type;
@property(nonatomic,strong) NSString *verify;
@property(nonatomic,strong) NSString *publish_date;
@property(nonatomic,strong) NSString *read_num;
@property(nonatomic,strong) NSString *like_num;
@property(nonatomic,strong) NSString *msg_cdn_url;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *publish_time;

@property(nonatomic,strong) NSString *content;
@property(nonatomic,strong) NSString *board_name;
@property(nonatomic,strong) NSString *pub_time;
@property(nonatomic,strong) NSString *sub_board_name;
@property(nonatomic,strong) NSString *ups;

@property(nonatomic,strong) NSString *ping_num;
@end
