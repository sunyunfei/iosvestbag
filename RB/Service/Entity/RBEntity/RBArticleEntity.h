//
//  RBArticleEntity.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBArticleEntity : NSObject
@property(nonatomic,strong) NSString *article_id;
@property(nonatomic,strong) NSString *article_title;
@property(nonatomic,strong) NSString *article_url;
@property(nonatomic,strong) NSString *article_avatar_url;
@property(nonatomic,strong) NSString *article_author;
@property(nonatomic,strong) NSString *article_read;
@property(nonatomic,strong) NSString *article_refresh;
@property(nonatomic,strong) NSString *article_publish_at;
@property(nonatomic,strong) NSString *read_count;
@end
