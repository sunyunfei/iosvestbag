//
//  RBActivityBannerEntity.h
//  RB
//
//  Created by RB8 on 2018/5/4.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBActivityBannerEntity : NSObject
@property(nonatomic,strong)NSString * banner_url;
@property(nonatomic,strong)NSString * desc;
@property(nonatomic,strong)NSString * detail_type;
@property(nonatomic,strong)NSString * title;
@property(nonatomic,strong)NSString * url;
@property(nonatomic,strong)NSString * bannerID;
@end
