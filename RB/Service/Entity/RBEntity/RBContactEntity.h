//
//  RBContactEntity.h
//  RB
//
//  Created by AngusNi on 3/18/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBContactEntity : NSObject
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *mobile;
@property(nonatomic,strong) NSString *influence_score;
@property(nonatomic,strong) NSString *joined;
@property(nonatomic,assign) int is_send;
@property(nonatomic,strong) NSString *invite_status;

@end
