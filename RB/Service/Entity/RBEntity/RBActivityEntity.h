//
//  RBActivityEntity.h
//  RB
//
//  Created by AngusNi on 16/2/16.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RBCampaignEntity.h"
@interface RBActivityEntity : NSObject
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *uuid;
@property(nonatomic,strong) NSString *status;
@property(nonatomic,strong) NSString *img_status;
@property(nonatomic,strong) NSString *share_url;
@property(nonatomic,strong) NSString *is_invited;
@property(nonatomic,strong) NSString *screenshot;
@property(nonatomic,strong) NSArray *screenshots;
@property(nonatomic,strong) NSString *reject_reason;
@property(nonatomic,strong) NSString *approved_at;
@property(nonatomic,strong) NSString *can_upload_screenshot;
@property(nonatomic,strong) NSArray  *upload_interval_time;
@property(nonatomic,strong) NSString *start_upload_screenshot;
@property(nonatomic,strong) NSString *avail_click;
@property(nonatomic,strong) NSString *total_click;
@property(nonatomic,strong) NSString *earn_money;
@property(nonatomic,strong) NSString *remain_budget;
@property(nonatomic,strong) NSString *tag;
@property(nonatomic,strong) NSString *ocr_status;
@property(nonatomic,strong) NSString *ocr_detail;
@property(nonatomic,strong) NSString *invitees_count;
@property(nonatomic,strong) NSMutableArray *invitees;
@property(nonatomic,strong) NSString *sub_type;
@property(nonatomic,strong) NSString *cpi_example_screenshot;
@property(nonatomic,strong) NSString * start_time;
@property(nonatomic,strong) NSArray *cpi_example_screenshots;
@property(nonatomic,strong) RBCampaignEntity *campaign;
@property(nonatomic,strong) NSString * leader_club;
@property(nonatomic,strong) NSArray * screenshot_comment;
@property(nonatomic,strong) NSString * alert;

@end
