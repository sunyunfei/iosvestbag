//
//  RBSimilarEntity.h
//  RB
//
//  Created by RB8 on 2017/8/23.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBSimilarEntity : NSObject
@property(nonatomic,copy)NSString * avatar_url;
@property(nonatomic,copy)NSString * iid;
@end
