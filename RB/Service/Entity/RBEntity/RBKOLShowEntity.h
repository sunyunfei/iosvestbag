//
//  RBKOLShowEntity.h
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBKOLShowEntity : NSObject
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *link;
@property(nonatomic,strong) NSString *provider;
@property(nonatomic,strong) NSString *cover_url;
@end
