//
//  RBInfluenceUpgradeEntity.h
//  RB
//
//  Created by AngusNi on 4/14/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBInfluenceUpgradeEntity : NSObject
@property(nonatomic,strong) NSString *kol_uuid;
@property(nonatomic,strong) NSString *influence_level;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *avatar_url;
@property(nonatomic,strong) NSString *influence_score;
@property(nonatomic,strong) NSString *rank_index;
@property(nonatomic,strong) NSString *cal_time;
@property(nonatomic,strong) NSString *campaign_count;
@property(nonatomic,strong) NSString *identity_count;
@property(nonatomic,strong) NSString *has_contacts;

@end
