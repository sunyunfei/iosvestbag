//
//  RBCampaignEntity.h
//  RB
//
//  Created by AngusNi on 16/2/16.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBCampaignEntity : NSObject
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *idescription;
@property(nonatomic,strong) NSString *img_url;
@property(nonatomic,strong) NSString *status;
@property(nonatomic,strong) NSString *message;
@property(nonatomic,strong) NSString *url;
@property(nonatomic,strong) NSString *per_budget_type;
@property(nonatomic,strong) NSString *per_action_budget;
@property(nonatomic,strong) NSString *per_action_type;
@property(nonatomic,strong) NSString *action_desc;
@property(nonatomic,strong) NSString *budget;
@property(nonatomic,strong) NSString *deadline;
@property(nonatomic,strong) NSString *start_time;
@property(nonatomic,strong) NSString *brand_name;
@property(nonatomic,strong) NSString *avail_click;
@property(nonatomic,strong) NSString *total_click;
@property(nonatomic,strong) NSString *take_budget;
@property(nonatomic,strong) NSString *remain_budget;
@property(nonatomic,strong) NSString *share_times;
@property(nonatomic,strong) NSMutableArray *interval_time;
@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *hide_brand_name;
@property(nonatomic,strong) NSString *task_description;
@property(nonatomic,strong) NSString *recruit_start_time;
@property(nonatomic,strong) NSString *recruit_end_time;
@property(nonatomic,strong) NSString *max_action;
@property(nonatomic,strong) NSString *sub_type;
@property(nonatomic,strong) NSString *wechat_auth_type;

@property(nonatomic,strong) NSString *cpi_example_screenshot;
@property(nonatomic,strong) NSArray *cpi_example_screenshots;
@property(nonatomic,strong) NSString *remark;

@property(nonatomic,strong) NSString *is_applying_note_required;
@property(nonatomic,strong) NSString *applying_note_description;
@property(nonatomic,strong) NSString *is_applying_picture_required;
@property(nonatomic,strong) NSString *applying_picture_description;

@end
