//
//  RBCpsCreateEntity.h
//  RB
//
//  Created by AngusNi on 16/9/7.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RBKOLEntity.h"
@interface RBCpsCreateEntity : NSObject
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *content;
@property(nonatomic,strong) NSString *cover;
@property(nonatomic,strong) NSString *show_url;
@property(nonatomic,strong) NSString *status;
@property(nonatomic,strong) NSString *check_remark;
@property(nonatomic,strong) NSString *end_date;
@property(nonatomic,strong) NSString *material_total_price;
@property(nonatomic,strong) NSString *writing_forecast_commission;
@property(nonatomic,strong) NSString *writing_settled_commission;
@property(nonatomic,strong) NSString *share_forecast_commission;
@property(nonatomic,strong) NSString *share_settled_commission;
@property(nonatomic,strong) NSString *author_iid;
@property(nonatomic,strong) NSString *author_name;
@property(nonatomic,strong) NSString *author_avatar_url;
@property(nonatomic,strong) NSString *cps_article_share_count;
@property(nonatomic,strong) NSMutableArray *cps_article_shares;
@end
