//
//  RBTudiEntity.h
//  RB
//
//  Created by RB8 on 2018/3/7.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBTudiEntity : NSObject
@property(nonatomic,copy) NSString * amount;
@property(nonatomic,copy) NSString * avatar_url;
@property(nonatomic,copy) NSString * campaign_invites_count;
@property(nonatomic,copy) NSString * kol_id;
@property(nonatomic,copy) NSString * kol_name;
@end
