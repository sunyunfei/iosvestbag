//
//  RBInfluenceReadEntity.h
//  RB
//
//  Created by AngusNi on 4/14/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RBInfluenceUpgradeEntity.h"

@interface RBInfluenceReadEntity : NSObject
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *avatar_url;
@property(nonatomic,strong) NSString *influence_level;
@property(nonatomic,strong) NSString *diff_score;
@property(nonatomic,strong) NSString *influence_score;

@property(nonatomic,strong) NSString *feature_score;
@property(nonatomic,strong) NSString *active_score;
@property(nonatomic,strong) NSString *campaign_score;
@property(nonatomic,strong) NSString *share_score;
@property(nonatomic,strong) NSString *contact_score;

@property(nonatomic,strong) NSString *feature_rate;
@property(nonatomic,strong) NSString *active_rate;
@property(nonatomic,strong) NSString *campaign_rate;
@property(nonatomic,strong) NSString *share_rate;
@property(nonatomic,strong) NSString *contact_rate;

@property(nonatomic,strong) NSString *contact_count;
@property(nonatomic,strong) NSString *rank_index;
@property(nonatomic,strong) NSString *foward_read_count;

@property(nonatomic,strong) RBInfluenceUpgradeEntity*influenceEntity;

@property(nonatomic,strong) NSMutableArray*history;
@property(nonatomic,strong) NSMutableArray*identities;
@property(nonatomic,strong) NSMutableArray*categories;
@property(nonatomic,strong) NSArray*wordclouds;
@property(nonatomic,strong) NSString*weibo_identity_status;
@end
