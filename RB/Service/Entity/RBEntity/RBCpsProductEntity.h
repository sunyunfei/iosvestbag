//
//  RBCpsProductEntity.h
//  RB
//
//  Created by AngusNi on 16/9/6.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBCpsProductEntity : NSObject
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *sku_id;
@property(nonatomic,strong) NSString *img_url;
@property(nonatomic,strong) NSString *material_url;
@property(nonatomic,strong) NSString *goods_name;
@property(nonatomic,strong) NSString *shop_id;
@property(nonatomic,strong) NSString *unit_price;
@property(nonatomic,strong) NSString *start_date;
@property(nonatomic,strong) NSString *end_date;
@property(nonatomic,strong) NSString *kol_commision_wl;
@property(nonatomic,strong) NSString *category;
@property(nonatomic,strong) NSString *category_label;
@end
