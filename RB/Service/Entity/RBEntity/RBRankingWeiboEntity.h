//
//  RBRankingWeiboEntity.h
//
//  Created by AngusNi on 15/7/13.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RBRankingWeiboEntity : NSObject
@property(nonatomic,strong) NSString *biz_code;
@property(nonatomic,strong) NSString *kol_id;
@property(nonatomic,strong) NSString *kol_name;
@property(nonatomic,strong) NSString *kol_avatar_url;
@property(nonatomic,strong) NSMutableArray *kol_category;
@property(nonatomic,strong) NSMutableArray *kol_keywords;
@property(nonatomic,strong) NSMutableArray *kol_brands;

@property(nonatomic,strong) NSString *kol_influence_score;

@property(nonatomic,strong) NSString *kol_info;
@property(nonatomic,strong) NSString *kol_relative;

@property(nonatomic,strong) NSString *fans;
@property(nonatomic,strong) NSString *gender;
@property(nonatomic,strong) NSString *posts;
@property(nonatomic,strong) NSString *verify;//企业,个人,团体

@property(nonatomic,strong) NSString *thanks;
@property(nonatomic,strong) NSString *ups;
@property(nonatomic,strong) NSString *url;


@property(nonatomic,strong) NSString *kol_sum_like_num_30;

@property(nonatomic,strong) NSString *kol_sum_read_num_30;
@end