//
//  RBArticleActionEntity.h
//  RB
//
//  Created by AngusNi on 3/18/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBArticleActionEntity : NSObject
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *article_id;
@property(nonatomic,strong) NSString *article_title;
@property(nonatomic,strong) NSString *article_url;
@property(nonatomic,strong) NSString *article_avatar_url;
@property(nonatomic,strong) NSString *article_author;
@property(nonatomic,strong) NSString *look;
@property(nonatomic,strong) NSString *forward;
@property(nonatomic,strong) NSString *collect;
@property(nonatomic,strong) NSString *like;
@property(nonatomic,strong) NSString *share_url;

@end
