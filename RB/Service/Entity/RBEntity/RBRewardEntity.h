//
//  RBRewardEntity.h
//
//  Created by AngusNi on 15/7/13.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RBRewardEntity : NSObject
@property(nonatomic,strong) NSString *task_type;
@property(nonatomic,strong) NSString *task_name;
@property(nonatomic,strong) NSString *reward_amount;
@property(nonatomic,strong) NSString *participate_status;

@end