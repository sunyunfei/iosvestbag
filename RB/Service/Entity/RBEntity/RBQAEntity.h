//
//  RBQAEntity.h
//  RB
//
//  Created by AngusNi on 4/7/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBQAEntity : NSObject
@property(nonatomic,strong) NSString *question;
@property(nonatomic,strong) NSString *answer;
@property(nonatomic,strong) NSString *logo;
@end
