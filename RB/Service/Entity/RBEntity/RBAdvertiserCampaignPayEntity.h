//
//  RBAdvertiserCampaignPayEntity.h
//
//  Created by AngusNi on 15/7/13.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface RBAdvertiserCampaignPayEntity : NSObject
@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *need_pay_amount;
@property(nonatomic,strong) NSString *brand_amount;
@property(nonatomic,strong) NSString *alipay_url;
@property(nonatomic,strong) NSString *status;
@end
