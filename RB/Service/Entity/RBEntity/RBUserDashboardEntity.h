//
//  RBUserDashboardEntity.h
//  RB
//
//  Created by AngusNi on 16/2/17.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBUserDashboardEntity : NSObject
@property(nonatomic,strong) NSString *influence_score;
@property(nonatomic,strong) NSString *today_income;
@property(nonatomic,strong) NSString *unread_count;
@property(nonatomic,strong) NSString *verifying_count;
@property(nonatomic,strong) NSString *waiting_upload_count;
@property(nonatomic,strong) NSString *settled_count;

@property(nonatomic,strong) NSString *iid;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *kol_role;
@property(nonatomic,strong) NSString *is_open_indiana;

@property(nonatomic,strong) NSString *role_apply_status;
@property(nonatomic,strong) NSString *role_check_remark;
@property(nonatomic,strong) NSString *max_campaign_click;
@property(nonatomic,strong) NSString *max_campaign_earn_money;
@property(nonatomic,strong) NSString *campaign_total_income;
@property(nonatomic,strong) NSString *avg_campaign_credit;
@property(nonatomic,strong) NSString *avatar_url;
@property(nonatomic,strong) NSMutableArray *tags;
//隐藏
@property(nonatomic,strong) NSString * hide;
@property(nonatomic,strong) NSString * detail;
@property(nonatomic,strong) NSArray * admintag;
//未读消息
@property(nonatomic,strong) NSString * has_any_unread_message;
//是否填写了公司名称
@property(nonatomic,strong) NSString * brand_campany_name;
@property(nonatomic,strong) NSString * is_show_invite_code;
@property(nonatomic,strong) NSString * logo;
@end
