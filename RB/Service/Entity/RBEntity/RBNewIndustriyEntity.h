//
//  RBNewIndustriyEntity.h
//  RB
//
//  Created by RB8 on 2017/8/16.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBNewIndustriyEntity : NSObject
@property(nonatomic,strong)NSString * avg_comments;
@property(nonatomic,strong)NSString * avg_likes;
@property(nonatomic,strong)NSString * avg_posts;
@property(nonatomic,strong)NSString * industry_name;
@property(nonatomic,strong)NSString * industry_score;
@end
