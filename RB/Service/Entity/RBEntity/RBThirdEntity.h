//
//  RBThirdEntity.h
//  RB
//
//  Created by AngusNi on 16/2/22.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBThirdEntity : NSObject
@property(nonatomic,strong) NSString *provider;
@property(nonatomic,strong) NSString *uid;
@property(nonatomic,strong) NSString *token;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *url;
@property(nonatomic,strong) NSString *avatar_url;
@property(nonatomic,strong) NSString *desc;
@end
