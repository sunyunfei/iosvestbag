//
//  RBNewInfluenceEntity.h
//  RB
//
//  Created by RB8 on 2017/8/16.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBNewInfluenceEntity : NSObject
@property(nonatomic,strong)NSString * avatar_url;
@property(nonatomic,strong)NSString * avg_comments;
@property(nonatomic,strong)NSString * avg_likes;
@property(nonatomic,strong)NSString * avg_posts;
@property(nonatomic,strong)NSString * calculated;
@property(nonatomic,strong)NSString * calculated_date;
@property(nonatomic,strong)NSString * Idescription;
@property(nonatomic,strong)NSMutableArray  * industries;
@property(nonatomic,strong)NSString * influence_level;
@property(nonatomic,strong)NSString * influence_score;
@property(nonatomic,strong)NSString * influence_score_percentile;
@property(nonatomic,strong)NSString * name;
@property(nonatomic,strong)NSString * provider;
@property(nonatomic,strong)NSMutableArray  * similar_kols;
@property(nonatomic,copy)  NSString * influence_score_visibility;


@end
