//
//  RBLoginSocialEntity.h
//  RB
//
//  Created by RB8 on 2017/8/17.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBLoginSocialEntity : NSObject
@property(nonatomic,strong)NSString * avatar_url;
@property(nonatomic,strong)NSString * name;
@property(nonatomic,strong)NSString * provider;
@property(nonatomic,strong)NSString * token;
@property(nonatomic,strong)NSString * uid;
@property(nonatomic,strong)NSString * desc;
@property(nonatomic,strong)NSString * url;

@end
