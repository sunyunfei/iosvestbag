//
//  RBInfluenceEntity.h
//  RB
//
//  Created by AngusNi on 3/18/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBInfluenceEntity : NSObject
@property(nonatomic,strong) NSString *influence_level;
@property(nonatomic,strong) NSString *influence_score;
@property(nonatomic,strong) NSString *joined_count;
@property(nonatomic,strong) NSString *rank_index;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *cal_time;
@property(nonatomic,strong) NSString *avatar_url;
@property(nonatomic,strong) NSString *last_influence_score;
@property(nonatomic,strong) NSMutableArray *contacts;

@property(nonatomic,strong) NSString *kol_uuid;
@property(nonatomic,strong) NSMutableArray *campaigns;
@end
