//
//  RBInfluenceHistoryEntity.h
//  RB
//
//  Created by AngusNi on 4/14/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBInfluenceHistoryEntity : NSObject
@property(nonatomic,strong) NSString *date;
@property(nonatomic,strong) NSString *score;
@end
