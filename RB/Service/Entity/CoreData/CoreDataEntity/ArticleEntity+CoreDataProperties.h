//
//  ArticleEntity+CoreDataProperties.h
//  RB
//
//  Created by AngusNi on 3/31/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ArticleEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface ArticleEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *article_author;
@property (nullable, nonatomic, retain) NSString *article_avatar_url;
@property (nullable, nonatomic, retain) NSString *article_id;
@property (nullable, nonatomic, retain) NSString *article_read;
@property (nullable, nonatomic, retain) NSString *article_title;
@property (nullable, nonatomic, retain) NSString *article_url;
@property (nullable, nonatomic, retain) NSString *article_refresh;

@end

NS_ASSUME_NONNULL_END
