//
//  ArticleEntity+CoreDataProperties.m
//  RB
//
//  Created by AngusNi on 3/31/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ArticleEntity+CoreDataProperties.h"

@implementation ArticleEntity (CoreDataProperties)

@dynamic article_author;
@dynamic article_avatar_url;
@dynamic article_id;
@dynamic article_read;
@dynamic article_title;
@dynamic article_url;
@dynamic article_refresh;

@end
