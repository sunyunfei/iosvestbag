//
//  ArticleEntityManager.m
//
//  Created by AngusNi on 15/6/16.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "ArticleEntityManager.h"
@implementation ArticleEntityManager
// 新增
+ (void)addCoreDataArticleEntity:(RBArticleEntity *)info {
    NSArray *resultArray = [ArticleEntity MR_findByAttribute:@"article_id" withValue:[NSString stringWithFormat:@"%@",info.article_id]];
    if ([resultArray count] == 0) {
        ArticleEntity *tmp = [ArticleEntity MR_createEntity];
        tmp.article_id = info.article_id;
        tmp.article_avatar_url = info.article_avatar_url;
        tmp.article_title = info.article_title;
        tmp.article_author = info.article_author;
        tmp.article_url = info.article_url;
        tmp.article_read = info.article_read;
        tmp.article_refresh = info.article_refresh;
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
}

// 查询 All分页
+ (NSArray *)selectCoreDataArticleEntity_AllByPageIndex:(int)index andPageSize:(int)size {
    NSArray *resultArray = [ArticleEntity MR_findAllByPageIndex:index andPageSize:size];
    return resultArray;
}

// 查询 All
+ (NSArray *)selectCoreDataArticleEntity_All {
    NSArray *resultArray = [ArticleEntity MR_findAll];
    return resultArray;
}

// 删除 ALL
+ (void)deleteCoreDataArticleEntity_All {
    NSArray *resultArray = [ArticleEntity MR_findAll];
    for (ArticleEntity *tmp in resultArray) {
        [tmp MR_deleteEntity];
    }
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

// 删除 By article_id
+ (void)deleteCoreDataArticleEntity_By:(NSString *)article_id {
    NSArray *resultArray  =
    [ArticleEntity MR_findByAttribute:@"article_id" withValue:article_id];
    for (ArticleEntity *tmp in resultArray) {
        [tmp MR_deleteEntity];
    }
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

// 更新 By article_id
+ (void)updateCoreDataArticleEntity_By:(NSString *)article_id andRBArticleEntity:(RBArticleEntity *)info {
    NSArray *resultArray  =
    [ArticleEntity MR_findByAttribute:@"article_id" withValue:article_id];
    for (ArticleEntity *tmp in resultArray) {
        tmp.article_id = info.article_id;
        tmp.article_avatar_url = info.article_avatar_url;
        tmp.article_title = info.article_title;
        tmp.article_author = info.article_author;
        tmp.article_url = info.article_url;
        tmp.article_read = info.article_read;
        tmp.article_refresh = info.article_refresh;
    }
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

@end
