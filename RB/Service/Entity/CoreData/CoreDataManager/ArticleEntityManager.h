//
//  ArticleEntityManager.h
//
//  Created by AngusNi on 15/6/16.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MagicalRecord.h"
#import <CoreData/CoreData.h>
//
#import "ArticleEntity.h"
#import "RBArticleEntity.h"
@interface ArticleEntityManager : NSObject

// 新增
+ (void)addCoreDataArticleEntity:(RBArticleEntity *)info;
// 查询 All分页
+ (NSArray *)selectCoreDataArticleEntity_AllByPageIndex:(int)index andPageSize:(int)size;
// 查询 All
+ (NSArray *)selectCoreDataArticleEntity_All;
// 删除 ALL
+ (void)deleteCoreDataArticleEntity_All;
// 删除 By article_id
+ (void)deleteCoreDataArticleEntity_By:(NSString *)article_id;
// 更新 By article_id
+ (void)updateCoreDataArticleEntity_By:(NSString *)article_id andRBArticleEntity:(RBArticleEntity *)info;

@end
