//
//  Handler.h
//
//  Created by AngusNi on 14/12/3.
//  Copyright (c) 2014年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"
@class Handler;
@protocol HandlerDelegate <NSObject>
@optional
- (void)handlerProgress:(float)progress Tag:(NSString *)sender;
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender;
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender;
- (void)handlerError:(NSError*)error Tag:(NSString *)sender;
@end


@interface Handler : NSObject
@property(nonatomic, weak) id <HandlerDelegate> delegate;
@property(nonatomic, strong) MKNetworkEngine*engine;
@property(nonatomic, strong) NSString*postUrl;
@property(nonatomic, strong) NSString*deviceToken;
@property(nonatomic, strong) NSString*authorization;
@property(nonatomic, assign) int authCount;

+ (id)shareHandler;
// 获取融云用户Token
- (void)getNetWorkRongCloudToken;


// RB-文章模块
// RB-文章历史列表
- (void)getRBArticleHistoryWithPage:(NSString*)page;
// RB-文章搜索列表
- (void)getRBArticleSearchWith:(NSString*)title andPage:(NSString*)page;
// RB-文章收藏列表
- (void)getRBArticleCollectWithPage:(NSString*)page;
// RB-文章分享列表
- (void)getRBArticleForwardWithPage:(NSString*)page;
// RB-文章列表
- (void)getRBArticleWithTitle:(NSString*)title andType:(NSString*)type;
// RB-文章阅读或喜欢
- (void)getRBArticleReadLikeWithAction:(NSString*)action andEntity:(RBArticleEntity*)entity;
// RB-文章转发、收藏、点赞
- (void)getRBArticleWithAction:(NSString*)action andIId:(NSString*)iid;


// RB-社交测试模块
// RB-社交影响力提升
- (void)getRBInfluenceIncrease;
// RB-社交影响力解读
- (void)getRBInfluenceRead;
// RB-社交影响力分享
- (void)getRBInfluenceShare;
// RB-社交进入测试影响力
- (void)getRBInfluence;
// RB-社交账号绑定
- (void)getRBInfluenceThirdAccountBindingWithProvider:(NSString*)provider andUid:(NSString*)uid andToken:(NSString*)token andName:(NSString*)name andUrl:(NSString*)url andAvatar_url:(NSString*)avatar_url andDesc:(NSString*)desc andSerial_params:(NSString*)serial_params andFollowers_count:(NSString*)followers_count andStatuses_count:(NSString*)statuses_count andRegistered_at:(NSString*)registered_at andVerified:(NSString*)verified andRefresh_token:(NSString*)refresh_token andUnionid:(NSString*)unionid andProvince:(NSString*)province andCity:(NSString*)city andGender:(NSString*)gender andIs_vip:(NSString*)is_vip andIs_yellow_vip:(NSString*)is_yellow_vip;
// RB-社交账号解除绑定
- (void)getRBInfluenceThirdAccountUnbind:(NSString*)provider andUid:(NSString*)uid;
//RB-社交账号绑定界面上传通讯录
- (void)getRBSocialInfluenceContactWithArray:(NSMutableArray*)array;
// RB-上传通讯录
- (void)getRBInfluenceContactWithArray:(NSMutableArray*)array;
// RB-短信邀请
- (void)getRBInfluenceSmsWithMobile:(NSString*)mobile;
// RB-社交账号排名
- (void)getRBInfluenceRank;
// RB-社交账号排名-分页
- (void)getRBInfluenceRankWithPage:(NSString*)page;
// RB-未允许获取通讯录测试结果
- (void)getRBInfluenceUnResult;
// RB-允许获取通讯录测试结果
- (void)getRBInfluenceInResultWithCity:(NSString*)city;
// RB-通过IP获取城市名
- (void)getRBInfluenceCityName;
// RB-设置城市
- (void)getRBInfluenceCitySet;
// RB-影响力详情
- (void)getRBInfluenceDetail;
// RB-影响力详情-标签云
- (void)getRBInfluenceDetailCloud;
//RB-影响力
-(void)getNewRBInfluence;
//RB-开始测试影响力
-(void)getRBbindSocialAccounts:(NSString*)kol_id andProvider:(NSString*)provider;
//RB—推荐的人测试影响力
-(void)getOtherRBbindSocialAccounts:(NSString*)kol_id andProvider:(NSString*)provider;
//RB-测试影响力
-(void)getRBTestInfluence;

// RB-注册登录模块
// RB-强制更新
- (void)getRBUserVersionUpdate;
// RB-是否开启第三方登录
- (void)getRBUserLoginThirdEnable;
// RB-获取短信验证码
- (void)getRBUserLoginPhoneCodeWith:(NSString*)phone;
// RB-获取邮箱验证码
- (void)getRBUserLoginEmailCodeWith:(NSString*)email Andtype:(NSString*)type;
// RB-邮箱验证码验证
- (void)validationRBEmailCode:(NSString*)email AndCode:(NSString*)emailCode;
// RB-邮箱注册名字加密码
- (void)getRBRegisterName:(NSString*)name AndPassword:(NSString*)password AndEmail:(NSString*)email AndVtoken:(NSString*)vtoken;
// RB-邮箱忘记密码
- (void)getRBUpdatePassword:(NSString*)email AndNewPassword:(NSString*)newpassword  AndpasswordConfirm:(NSString*)password_confirmation andVtoken:(NSString*)vtoken;
// RB-获取语音验证码
- (void)getRBUserLoginPhoneVoiceCodeWith:(NSString*)phone;
// RB-登录
- (void)getRBUserLoginWithPhone:(NSString*)phone andCode:(NSString*)code andInvteCode:(NSString*)inviteCode;
// RB-邮箱登录
- (void)getRBUserLoginWithEmail:(NSString*)email andCode:(NSString*)password;
// RB-第三方登录
- (void)getRBUserLoginThirdWithProvider:(NSString*)provider andUid:(NSString*)uid andToken:(NSString*)token andName:(NSString*)name andUrl:(NSString*)url andAvatar_url:(NSString*)avatar_url andDesc:(NSString*)desc andSerial_params:(NSString*)serial_params andFollowers_count:(NSString*)followers_count andStatuses_count:(NSString*)statuses_count andRegistered_at:(NSString*)registered_at andVerified:(NSString*)verified andRefresh_token:(NSString*)refresh_token andUnionid:(NSString*)unionid andProvince:(NSString*)province andCity:(NSString*)city andGender:(NSString*)gender andIs_vip:(NSString*)is_vip andIs_yellow_vip:(NSString*)is_yellow_vip ;
// RB-获取用户IM信息
- (void)getRBUserLoginIMWithId:(NSString*)iid;
// RB-对他人是否可见权限
- (void)getRBPermitToothers:(NSString*)action;



// RB-用户模块
// RB-获取用户信息
- (void)getRBUserInfo;
// RB-修改用户头像
- (void)getRBUserInfoAvatarUpdate:(UIImage*)avtar;
// RB-修改用户信息
- (void)getRBUserInfoUpdateWithEmail:(NSString*)email andName:(NSString*)name andWeixin_friend_count:(NSString*)weixin_friend_count andAge:(NSString*)age andGender:(NSString*)gender andBirthday:(NSString*)date_of_birthday andCountry:(NSString*)country andCity:(NSString*)city andDesc:(NSString*)desc andTags:(NSArray*)tags andAlipay:(NSString*)alipay_account;
// RB-获取城市列表
- (void)getRBUserInfoCitys;
// RB-获取用户首页信息
- (void)getRBUserInfoDashboard;
// RB-获取用户标签
- (void)getRBUserInfoTags;
// RB-绑定新手机号码
- (void)getRBUserInfoNewPhoneWithPhone:(NSString*)phone andCode:(NSString*)code;
// RB-第三方账号绑定
- (void)getRBUserInfoThirdAccountBindingWithProvider:(NSString*)provider andUid:(NSString*)uid andToken:(NSString*)token andName:(NSString*)name andUrl:(NSString*)url andAvatar_url:(NSString*)avatar_url andDesc:(NSString*)desc andSerial_params:(NSString*)serial_params andFollowers_count:(NSString*)followers_count andStatuses_count:(NSString*)statuses_count andRegistered_at:(NSString*)registered_at andVerified:(NSString*)verified andRefresh_token:(NSString*)refresh_token andUnionid:(NSString*)unionid  andProvince:(NSString*)province andCity:(NSString*)city andGender:(NSString*)gender andIs_vip:(NSString*)is_vip andIs_yellow_vip:(NSString*)is_yellow_vip;

// RB-第三方账号绑定V_2
- (void)getV2RBUserInfoThirdAccountBindingWithProvider:(NSString*)provider andUid:(NSString*)uid andToken:(NSString*)token andName:(NSString*)name andUrl:(NSString*)url andAvatar_url:(NSString*)avatar_url andDesc:(NSString*)desc andSerial_params:(NSString*)serial_params andFollowers_count:(NSString*)followers_count andStatuses_count:(NSString*)statuses_count andRegistered_at:(NSString*)registered_at andVerified:(NSString*)verified andRefresh_token:(NSString*)refresh_token andUnionid:(NSString*)unionid  andProvince:(NSString*)province andCity:(NSString*)city andGender:(NSString*)gender andIs_vip:(NSString*)is_vip andIs_yellow_vip:(NSString*)is_yellow_vip;

// RB-第三方登录绑定手机
- (void)getRBUserInfoThirdAccountBindingWithPhone:(NSString*)phone andCode:(NSString*)code;
// RB-第三方账号列表
- (void)getRBUserInfoThirdAccountList;
// RB-第三方账号解除绑定
- (void)getRBUserInfoThirdAccountUnbind:(NSString*)uid;
// RB-支付宝账号绑定
- (void)getRBUserInfoZhiFuBaoBindingWithName:(NSString*)name andAccout:(NSString*)account andIdcard:(NSString*)idcard;
// RB-我是大V-列表
- (void)getRBUserInfoBigV;
// RB-我是大V-填写
- (void)getRBUserInfoBigVWithProvider:(NSString*)provider andName:(NSString*)name andFollower_count:(NSString*)follower_count andBelong_field:(NSString*)belong_field andHeadline_price:(NSString*)headline_price andSecond_price:(NSString*)second_price andSingle_price:(NSString*)single_price;
// RB-我的页面-列表
- (void)getRBUserInfoProfile;
// RB-用户信息修改
- (void)getRBUserInfoUpdateLongitudeLatitude;
// RB-输入邀请码-提交
- (void)submitInviteCode:(NSString*)inviteCode;

//RB-文章模块
//RB-文章详情计时
- (void)getRBRecommendPost_id:(NSString*)post_id AndStayTime:(NSString*)stay_time;
//文章推荐接口
- (void)getRBRecommendTag:(NSString*)tag andPost_id:(NSString*)post_id;
//RB-获取文章列表
- (void)getRBArticleslist:(NSString*)type andPage:(int)page;
//文章添加或取消  点赞或收藏
- (void)getRBAddOrCancellike:(NSString*)type andpost_id:(NSString*)post_id andType:(NSString*)action;
//我的收藏列表
- (void)getRBMyCollectArticleList:(NSString*)action andPageIndex:(int)page;
// RB-浏览文章拆开红包
- (void)getRBApartRedBag:(NSString*)red_money;

// RB-活动模块
// RB-第一次做任务给奖励
- (void)getRBFirstDoCampaigngetTheAward;
// RB-点击banner调用，banner统计
- (void)getRBBannerClick:(NSString*)announcement_Id andparams_json:(NSString*)params_json;
// RB-获取活动列表
- (void)getRBActivityWithStatus:(NSString*)status andTitle:(NSString*)title andWith_message:(NSString*)with_message andPage:(NSString*)page;
// RB-我的活动页面获取活动列表
- (void)getRBActivityWithStatus:(NSString*)status andPage:(NSString*)page;
// RB-获取活动详情
- (void)getRBActivityDetailWithId:(NSString*)activityId;
// RB-获取活动参与
- (void)getRBActivityEnjoyWithId:(NSString*)activityId;
// RB-获取活动转发成功通知
- (void)getRBActivityShareWithId:(NSString*)activityId andSuntype:(NSString*)sub_type;
// RB-活动转发失败通知
-(void)getRBActivityCancelWithId:(NSString*)activityId;
// RB-招募活动上传截图
- (void)getRBActivityRecuritWithId:(NSString*)activityId andScreenShoot:(NSArray*)arr andCampaignLogo:(UIImage*)campaign_logo;
// RB-获取活动上传截图
- (void)getRBActivityUpaloadWithId:(NSString*)activityId andScreenShot:(UIImage*)screenshot andCampaignLogo:(UIImage*)campaign_logo;
// RB-获取活动参与
- (void)getRBCampaignApproveWithId:(NSString*)campaignId;
// RB-获取Campaign详情
- (void)getRBCampaignDetailWithId:(NSString*)campaignId;
// RB-获取Campaign参与
- (void)getRBCampaignEnjoyWithId:(NSString*)campaignId;
// RB-获取活动报名资格
- (void)getRBActivityCanApplyWithId:(NSString*)activityId;
// RB-获取活动报名
- (void)getRBActivityApplyWithId:(NSString*)activityId andNote:(NSString*)note andPicture:(UIImage*)img;
// RB-获取活动-上传图片
- (void)getRBActivityPicUpload:(UIImage*)img;
// RB-获取Campaign参与者详情
- (void)getRBCampaignKolDetailWithId:(NSString*)kolId;
// RB-获取Campaign的素材列表
- (void)getRBCampaignMaterialWithId:(NSString*)campaignId andCampaignInviteId:(NSString*)campaignInviteId;
// RB-获取Campaign拒绝活动
- (void)getRBCampaignInviteRejectWithId:(NSString*)campaignId;
// RB-获取Campaign参与人员分析
- (void)getRBCampaignAnalysisWithId:(NSString*)campaignId;
// RB-获取Campaign参与人员分析-分页
- (void)getRBCampaignAnalysisWithId:(NSString*)campaignId andPage:(NSString*)page;

// RB-发布活动
- (void)getRBActivityAddWithName:(NSString*)name andDescription:(NSString*)desc andUrl:(NSString*)url andImg:(UIImage*)img andStart_time:(NSString*)start_time andDeadline:(NSString*)deadline andPer_budget_type:(NSString*)per_budget_type andBudget:(NSString*)budget andPer_action_budget:(NSString*)per_action_budget andAge:(NSString*)age andGender:(NSString*)gender andRegion:(NSString*)region andTags:(NSString*)tags andSub_type:(NSString*)sub_type;
// RB-发布活动-使用积分抵扣
- (void)getRBActivityAddPayWithintegralID:(NSString*)iid andUse_credit:(NSString*)use_credit;
// RB-发布活动-使用钱包余额抵扣
- (void)getRBActivityAddPayByVoucherWithiid:(NSString*)iid andUsed_voucher:(NSString*)used_voucher andUseCredit:(NSString*)use_credit;
// RB-发布活动-使用广告账户余额抵扣
- (void)getRBActivityAddPayByAdvertiserWithiid:(NSString*)iid;
// RB-发布活动-使用支付宝付款是否成功
- (void)getRBActivityAddPayByAlipayWithiid:(NSString*)iid;
// RB-发布活动-列表
- (void)getRBActivityAddListWithCampaign_type:(NSString*)campaign_type andPage:(NSString*)page;
// RB-发布活动-活动详情页面
- (void)getRBActivityAddDetailWithId:(NSString*)iid;
// RB-发布活动-撤销
- (void)getRBActivityAddRevokeWithId:(NSString*)iid;
// RB-发布活动-修改
- (void)getRBActivityAddEditWithId:(NSString*)iid andName:(NSString*)name andDescription:(NSString*)desc andUrl:(NSString*)url andImg:(UIImage*)img andStart_time:(NSString*)start_time andDeadline:(NSString*)deadline andPer_budget_type:(NSString*)per_budget_type andBudget:(NSString*)budget andPer_action_budget:(NSString*)per_action_budget andAge:(NSString*)age andGender:(NSString*)gender andRegion:(NSString*)region andTags:(NSString*)tags andSubtype:(NSString*)sub_type;
// RB-发布活动-品牌主账户
- (void)getRBActivityAddAccount;
// RB-发布活动-品牌主账单
- (void)getRBActivityAddMyAccountWith:(NSString*)page;
// RB-发布多动-积分账单
- (void)getRBCreditListwithPage:(NSString*)page;
// RB- 修改品牌主资料
- (void)getRBActivityModifybrandwith:(NSMutableDictionary*)dicT andAvatar:(UIImage*)avatarImage;
// RB-发布活动-参与人员列表
- (void)getRBActivityAddMyListEnjoyWithId:(NSString*)iid andPage:(NSString*)page;
// RB-发布活动-广告账户充值
- (void)getRBActivityAddMyRechargeWithCredits:(NSString*)credits andCode:(NSString*)code;
// RB-发布活动-期望效果
- (void)getRBActivityAddResult;
// RB-发布活动-文章分析
- (void)getRBActivityAddAnalysisWithUrl:(NSString*)url andExpect:(NSString*)expect;




// RB-意见反馈模块
// RB-获取意见反馈
- (void)getRBUserHelpFeedBackWithContent:(NSString*)content andScreen:(UIImage*)screenshot;
// RB-获取QA
- (void)getRBUserHelpQA;
// RB-获取扫码登录网站
- (void)getRBUserHelpScanCode:(NSString*)code;
// RB-帮助中心-列表
- (void)getRBUserHelpListWithPage:(int)page;




// RB-消息模块
// RB-获取消息列表
- (void)getRBMessageListWithStatus:(NSString*)status andPage:(NSString*)page;
// RB-获取消息阅读
- (void)getRBMessageRead:(NSString*)msgId;
// RB-获取消息全部设置已读
- (void)getRBMessageAllRead;




// RB-收入模块
// RB-充值信息
- (void)getRBRecharge;
// RB-获取用户账户
- (void)getRBAccount;
// RB-获取用户账单列表
- (void)getRBAccountIncome;
// RB-获取用户提现申请
- (void)getRBAccountCashWithCredits:(NSString*)credits;
// RB-获取用户是否绑定支付宝
- (void)getRBAccountZhiFuBao;
// RB-账单历史列表
- (void)getRBAccountHistoryWithPage:(NSString*)page;




// RB-引擎
// RB-引擎-文章
- (void)getRBRankingListWithPageIndex:(int)pageIndex andPageSize:(int)pageSize;

// RB-引擎-微博
- (void)getRBRankingWeiboSearchWithTerm:(NSString*)term andOrder:(NSString*)order andCategory:(NSArray*)category andBrands:(NSArray*)brands andKeywords:(NSArray*)keywords andRelative:(NSString*)relative andPageIndex:(int)pageIndex andPageSize:(int)pageSize;
// RB-引擎-微博-列表
- (void)getRBRankingWeiboListWithKolId:(NSString*)kolId andPageIndex:(int)pageIndex andPageSize:(int)pageSize;
// RB-引擎-微博-详情
- (void)getRBRankingWeiboDetailWithId:(NSString*)iid;
// RB-引擎-微博-雷达图
- (void)getRBRankingWeiboCategoriesWithId:(NSString*)iid;
// RB-引擎-微博-标签云
- (void)getRBRankingWeiboTagsWithId:(NSString*)iid;

// RB-引擎-公众号-搜索
- (void)getRBRankingWechatSearchWithBiz_code:(NSString*)biz_code;
// RB-引擎-公众号-搜索
- (void)getRBRankingWechatSearchWithTerm:(NSString*)term andOrder:(NSString*)order andCategory:(NSArray*)category andBrands:(NSArray*)brands andKeywords:(NSArray*)keywords andPageIndex:(int)pageIndex andPageSize:(int)pageSize;
// RB-引擎-公众号-排行榜
- (void)getRBRankingWechatWithCategory:(NSArray*)category andKeywords:(NSArray*)keywords andTerm:(NSString*)term andRelative:(NSString*)relative andPageIndex:(int)pageIndex andPageSize:(int)pageSize;
// RB-引擎-公众号-列表
- (void)getRBRankingWechatListWithKolId:(NSString*)kolId andPageIndex:(int)pageIndex andPageSize:(int)pageSize;
// RB-引擎-公众号-详情
- (void)getRBRankingWechatDetailWithId:(NSString*)iid;
// RB-引擎-公众号-雷达图
- (void)getRBRankingWechatCategoriesWithId:(NSString*)iid;
// RB-引擎-公众号-标签云
- (void)getRBRankingWechatTagsWithId:(NSString*)iid;

// RB-引擎-知乎-搜索
- (void)getRBRankingZhihuSearchWithTerm:(NSString*)term andOrder:(NSString*)order andCategory:(NSArray*)category andBrands:(NSArray*)brands andKeywords:(NSArray*)keywords andPageIndex:(int)pageIndex andPageSize:(int)pageSize;
// RB-引擎-知乎-列表
- (void)getRBRankingZhihuListWithKolId:(NSString*)kolId andPageIndex:(int)pageIndex andPageSize:(int)pageSize;
// RB-引擎-知乎-详情
- (void)getRBRankingZhihuDetailWithId:(NSString*)iid;
// RB-引擎-知乎-雷达图
- (void)getRBRankingZhihuCategoriesWithId:(NSString*)iid;
// RB-引擎-知乎-标签云
- (void)getRBRankingZhihuTagsWithId:(NSString*)iid;

// RB-引擎-美拍-搜索
- (void)getRBRankingMeipaiSearchWithTerm:(NSString*)term andOrder:(NSString*)order andCategory:(NSArray*)category andBrands:(NSArray*)brands andKeywords:(NSArray*)keywords andPageIndex:(int)pageIndex andPageSize:(int)pageSize;
// RB-引擎-美拍-列表
- (void)getRBRankingMeipaiListWithKolId:(NSString*)kolId andPageIndex:(int)pageIndex andPageSize:(int)pageSize;
// RB-引擎-美拍-详情
- (void)getRBRankingMeipaiDetailWithId:(NSString*)iid;
// RB-引擎-美拍-雷达图
- (void)getRBRankingMeipaiCategoriesWithId:(NSString*)iid;
// RB-引擎-美拍-标签云
- (void)getRBRankingMeipaiTagsWithId:(NSString*)iid;

// RB-引擎-秒拍-搜索
- (void)getRBRankingMiaopaiSearchWithTerm:(NSString*)term andOrder:(NSString*)order andCategory:(NSArray*)category andBrands:(NSArray*)brands andKeywords:(NSArray*)keywords andPageIndex:(int)pageIndex andPageSize:(int)pageSize;
// RB-引擎-秒拍列表
- (void)getRBRankingMiaopaiListWithKolId:(NSString*)kolId andPageIndex:(int)pageIndex andPageSize:(int)pageSize;
// RB-引擎-秒拍-详情
- (void)getRBRankingMiaopaiDetailWithId:(NSString*)iid;
// RB-引擎-秒拍-雷达图
- (void)getRBRankingMiaopaiCategoriesWithId:(NSString*)iid;
// RB-引擎-秒拍-标签云
- (void)getRBRankingMiaopaiTagsWithId:(NSString*)iid;



// RB-奖励模块
// RB-奖励-签到
- (void)getRBUserRewardCheckIn;
// RB-奖励-签到历史
- (void)getRBUserRewardCheckInHistory;
// RB-新签到-签到历史
- (void)getRBUserRewardCheckInNewHistory;
// RB-新奖励-签到
- (void)getRBUserRewardNewCheckIn;
//七日签到历史
- (void)getRBWeekSignHistory;
// RB-奖励-完善个人资料
- (void)getRBUserRewardCompleteInfo;
// RB-奖励-获取邀请好友信息
- (void)getRBUserRewardInviteCode;
// RB-徒弟总收益
- (void)getRBTuDiAllReward:(NSString*)page;
// RB-今日收徒数
- (void)getRBTudiToday:(NSString*)page;

// RB-分析模块
// RB-分析-是否开启微博分析
- (void)getRBAnalysisWeiboEnable;
// RB-分析-绑定列表
- (void)getRBAnalysisList;
// RB-分析-微博-绑定
- (void)getRBAnalysisWeiboBundingWithProvider:(NSString*)provider andUid:(NSString*)uid andToken:(NSString*)token andRefresh_token:(NSString*)refresh_token andName:(NSString*)name andAvatar_url:(NSString*)avatar_url andLocation:(NSString*)location andGender:(NSString*)gender andSerial_params:(NSString*)serial_params andBind_type:(NSString*)bind_type;
// RB-分析-解除绑定微博
- (void)getRBAnalysisWeiboUnBundingWithUid:(NSString*)uid andLogin_id:(NSString*)login_id;
// RB-分析-微博详情
- (void)getRBAnalysisWeiboDetailWithUid:(NSString*)uid;
// RB-分析-微博详情-粉丝-新增
- (void)getRBAnalysisWeiboDetailFollowerNewWithUid:(NSString*)uid;
// RB-分析-微博详情-粉丝-认证
- (void)getRBAnalysisWeiboDetailFollowerVerifyWithUid:(NSString*)uid;
// RB-分析-微博详情-关注-认证
- (void)getRBAnalysisWeiboDetailFriendVerifyWithUid:(NSString*)uid;
// RB-分析-微博详情-粉丝-地域
- (void)getRBAnalysisWeiboDetailFriendProfileWithUid:(NSString*)uid;
// RB-分析-微博详情-列表
- (void)getRBAnalysisWeiboDetailListWithUid:(NSString*)uid;
// RB-分析-绑定公众号
- (void)getRBAnalysisWechatBundingWithIdentity_id:(NSString*)identity_id;
// RB-分析-绑定公众号
- (void)getRBAnalysisWechatBundingWithName:(NSString*)username andPassword:(NSString*)password andImgcode:(NSString*)imgcode andCookies:(NSString*)cookies;
// RB-分析-绑定公众号-二维码扫描登录
- (void)getRBAnalysisWechatBundingWithUid:(NSString*)login_id;
// RB-分析-公众号详情
- (void)getRBAnalysisWechatDetailWithLogin_id:(NSString*)login_id andIdentity_id:(NSString*)identity_id;
// RB-分析-公众号详情-图文
- (void)getRBAnalysisWechatDetailMessagesWithLogin_id:(NSString*)login_id andIdentity_id:(NSString*)identity_id;
// RB-分析-公众号详情-文章
- (void)getRBAnalysisWechatDetailArticlesWithLogin_id:(NSString*)login_id andIdentity_id:(NSString*)identity_id;
// RB-分析-公众号详情-粉丝
- (void)getRBAnalysisWechatDetailFollowerWithLogin_id:(NSString*)login_id andIdentity_id:(NSString*)identity_id;
// RB-分析-公众号-验证码图片
- (void)getRBAnalysisWechatLoginCode:(NSString*)picUrl and:(UIImageView*)urlIV;



// RB-一元夺宝
// RB-夺宝-列表
- (void)getRBIndianaListWithPage:(int)page;
// RB-夺宝-详细
- (void)getRBIndianaDetailWithCode:(NSString*)code;
// RB-夺宝-详细-购买人
- (void)getRBIndianaDetailListWithCode:(NSString*)code andPage:(NSString*)page;
// RB-夺宝-详细-图文
- (void)getRBIndianaDetailInfoWithCode:(NSString*)code;
// RB-夺宝-详细-计算
- (void)getRBIndianaDetailCalculationWithCode:(NSString*)code;
// RB-夺宝-详细-购买
- (void)getRBIndianaDetailBugWithCode:(NSString*)code andNum:(NSString*)num;
// RB-夺宝-详细-支付
- (void)getRBIndianaDetailBugPayWithCode:(NSString*)code;
// RB-夺宝-我参与的
- (void)getRBIndianaMyWithStatus:(NSString*)status andPage:(int)page;
// RB-夺宝-我参与的-详情
- (void)getRBIndianaMyDetailWithCode:(NSString*)code;
// RB-夺宝-获取收货地址
- (void)getRBIndianaMyAddress;
// RB-夺宝-获取收货地址-填写
- (void)getRBIndianaMyAddressEditWithName:(NSString*)name andPhone:(NSString*)phone andLocation:(NSString*)location;


// RB-KOL
// RB-KOL-列表
- (void)getRBKOLListWithPage:(int)page andBanner:(NSString*)banner andCategory:(NSString*)category andSearch:(NSString*)search andOrder:(NSString*)order;
// RB-KOL-详情
- (void)getRBKOLDetailWithKolId:(NSString*)kolId;
// RB——KOL-社交账号解除绑定
- (void)relieveRBKOLAccount:(NSString*)kol_id andProvider:(NSString*)provider andId:(NSString*)socialId;
// RB——KOL-查询绑定次数
- (void)seeaRBKOLrelieveTimes:(NSString*)kol_id AndProvider:(NSString*)provider;
// RB——KOL-查询解绑次数
-(void)searchRBKOLClearTimes:(NSString*)kol_id AndProvider:(NSString*)provider;
// RB-KOL-关注
- (void)getRBKOLCareWithKolId:(NSString*)kolId;
// RB-KOL申请-第一步
- (void)getRBKOLApplyWithAvatar:(UIImage*)avatar andName:(NSString*)name andAge:(NSString*)age andGender:(NSString*)gender andApp_city:(NSString*)app_city andJob:(NSString*)job_info andTags:(NSString*)tag_names andDesc:(NSString*)desc;
// RB-KOL申请-第二步V2
- (void)getV2RBKOLApplyWithProvider:(NSString*)provider andHomepage:(NSString*)homepage andUsername:(NSString*)username andPrice:(NSString*)price andFollowers:(NSString*)followers_count andScreenshot:(UIImage*)screenshot andUid:(NSString*)uid;
// RB-KOL申请-第二步
- (void)getRBKOLApplyWithProvider:(NSString*)provider andHomepage:(NSString*)homepage andUsername:(NSString*)username andPrice:(NSString*)price andFollowers:(NSString*)followers_count andScreenshot:(UIImage*)screenshot andUid:(NSString*)uid;
// RB-KOL申请-第三步
- (void)getRBKOLApplyWithShows:(NSString*)kol_shows;
// RB-KOL-关注列表
- (void)getRBKOLFollowersListWithPage:(int)page;


// RB-CPS模块
// RB-获取商品分类
- (void)getRBCpsProductCategoryWithPage:(NSString*)page;
// RB-获取商品列表
- (void)getRBCpsProductListWithGoods:(NSString*)goods andCategory:(NSString*)category andOrder:(NSString*)order andPage:(NSString*)page;
// RB-获取图片上传
- (void)getRBCpsUploadImg:(UIImage*)img;
// RB-获取创作详情
- (void)getRBCpsCreateDetailWithID:(NSString*)uid;
// RB-获取创作详情商品列表
- (void)getRBCpsCreateDetailProductListWithID:(NSString*)uid;
// RB-获取创作列表
- (void)getRBCpsCreateListWithPage:(NSString*)page;
// RB-获取我的创作列表
- (void)getRBCpsMyCreateListWithPage:(NSString*)page andStatus:(NSString*)status;
// RB-获取我的创作提交
- (void)getRBCpsCreateSubmitWithContent:(NSString*)content andTitle:(NSString*)title andCover:(NSString*)cover;
// RB-获取我的创作分享
- (void)getRBCpsCreateShareWithId:(NSString*)iid;
// RB-获取我的创作商品列表
- (void)getRBCpsCreateProductWithId:(NSString*)iid;


@end

