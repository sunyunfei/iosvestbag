//
//  Handler.m
//
//  Created by AngusNi on 14/12/3.
//  Copyright (c) 2014年 AngusNi. All rights reserved.
//

#import "Handler.h"
#import "RBAppDelegate.h"
static Handler *handler = nil;
@implementation Handler

+ (id)shareHandler {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        handler = [[Handler alloc] init];

        //测试环境
        // handler.postUrl = @"http://staging.robin8.net/";
        // handler.postUrl = @"http://test.robin8.net/";
        
       // handler.postUrl = @"https://qa.robin8.net/";
        //handler.postUrl = @"http://robin8.net/";
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:@"Session_ClientID"] == nil) {
            [defaults setObject:[[NSUUID UUID] UUIDString]
                         forKey:@"Session_ClientID"];
            [defaults synchronize];
        }
        handler.deviceToken = [defaults objectForKey:@"Session_ClientID"];
        handler.engine = [[MKNetworkEngine alloc] init];
        handler.authCount = 0;
    });
    //正式环境
    if ([LocalService getRBDevelopIPURL] != nil) {
        handler.postUrl = [LocalService getRBDevelopIPURL];
    }else{
     // handler.postUrl = @"https://robin8.net/";
        handler.postUrl = @"https://qa.robin8.net/";
    //  handler.postUrl = @"http://192.168.51.115:3001/";
     // handler.postUrl = @"http://eaa3dbb6.ap.ngrok.io/";
    }
    return handler;
}

- (NSDate *)getNowDateFromatAnDate:(NSDate *)anyDate{
    //设置源日期时区
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    //设置转换后的目标日期时区
    NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];
    //得到源日期与世界标准时间的偏移量
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:anyDate];
    //目标日期与本地时区的偏移量
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:anyDate];
    //得到时间偏移量的差值
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    //转为现在时间
    NSDate* destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:anyDate];
    return destinationDateNow;
}


// RB-JWT加密
- (NSString*)getRBSecret {
    NSDate*nowDate = [NSDate date];
    NSDate*utcDate = [self getNowDateFromatAnDate:nowDate];
    NSString*dateStr = [NSString stringWithFormat:@"%f",[utcDate timeIntervalSince1970]];
    NSString*tokenStr = [LocalService getRBLocalDataUserPrivateToken];
    if(tokenStr==nil) {
        tokenStr=@"";
    }
    JWTAlgorithmHS256*algorithm = [[JWTAlgorithmHS256 alloc]init];
    self.authorization = [JWT encodePayload:@{@"get_code":@"get_code",@"is_new":@"is_new",@"private_token":tokenStr,@"time":dateStr} withSecret:@"Robin888" algorithm:algorithm];
    return self.authorization;
}

// RB-返回登录页面
- (void)getRBBackToLogin {
   // [SVProgressHUD showErrorWithStatus:@"账户异常,请重新登录"];
    dispatch_after(
                   dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)),
                   dispatch_get_main_queue(), ^{
                       [[NSNotificationCenter defaultCenter] postNotificationName:NotificationShowLoginView
                                                                           object:self
                                                                         userInfo:nil];
                   });
}

// 融云模块
// 获取融云用户Token
- (void)getNetWorkRongCloudToken {
    NSString *appKey = RCIMAppKey;
    NSString *appSecret = RCIMServiceKey;
    NSString *nonce = [NSString stringWithFormat:@"%d", arc4random() % 5];
    NSString *timestamp = [NSString
                           stringWithFormat:@"%d", (int)[[NSDate date] timeIntervalSince1970]];
    NSString *signature = [Utils
                           getSecretStringSHA1:[NSString stringWithFormat:@"%@%@%@", appSecret,
                                                nonce, timestamp]];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:[LocalService getRBLocalDataUserLoginPhone] forKey:@"userId"];
    [dic setValue:[LocalService getRBLocalDataUserLoginName] forKey:@"name"];
    [dic setValue:[LocalService getRBLocalDataUserLoginAvatar] forKey:@"portraitUri"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:@"https://api.cn.rong.io/user/getToken.json"
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"App-Key" withValue:appKey];
    [op setHeader:@"Nonce" withValue:nonce];
    [op setHeader:@"Timestamp" withValue:timestamp];
    [op setHeader:@"Signature" withValue:signature];
    [op setHeader:@"Content-Type" withValue:@"application/x-www-form-urlencoded"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *tokenStr  =
        [NSString stringWithFormat:@"%@", [completedOperation.responseJSON objectForKey:@"token"]];
        [LocalService setRBLocalDataUserRongTokenWith:tokenStr];
        [[RCIM sharedRCIM] connectWithToken:tokenStr success:^(NSString*userId){
            NSLog(@"RCIM,success:%@",userId);
        } error:^(RCConnectErrorCode status) {
            NSLog(@"RCIM,error:%d",(int)status);
        } tokenIncorrect:^{
            NSLog(@"RCIM,tokenIncorrect");
        }];
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                }];
    [self.engine enqueueOperation:op];
}


// RB-文章模块
// RB-文章历史列表
- (void)getRBArticleHistoryWithPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/article_actions/history"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"history"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"history"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"history"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-文章搜索列表
- (void)getRBArticleSearchWith:(NSString*)title andPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:title forKey:@"title"];
    [dic setValue:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/articles/search"]
                              params:dic
                              httpMethod:@"GET"];
    NSLog(@"AuthorizationgetRBSecret%@", [self getRBSecret]);
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"search"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-文章收藏列表
- (void)getRBArticleCollectWithPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/article_actions/collect"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"collect"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"collect"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"collect"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-文章分享列表
- (void)getRBArticleForwardWithPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/article_actions/forward"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"forward"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"forward"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"forward"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-文章列表
- (void)getRBArticleWithTitle:(NSString*)title andType:(NSString*)type{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    //    ['discovery' ,'select'],分别代表 '发现文章'、'喜欢文章'
    if (title!=nil&&title.length!=0) {
        [dic setValue:title forKey:@"title"];
    }
    if (type!=nil) {
        [dic setValue:type forKey:@"type"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/articles"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"articles"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"articles"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"articles"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-文章阅读或喜欢
- (void)getRBArticleReadLikeWithAction:(NSString*)action andEntity:(RBArticleEntity*)entity {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    //  ['look','collect'] 分别代表 阅读、搜藏
    if (action!=nil) {
        [dic setValue:action forKey:@"action"];
    }
    [dic setValue:entity.article_id forKey:@"article_id"];
    [dic setValue:entity.article_title forKey:@"article_title"];
    [dic setValue:entity.article_url forKey:@"article_url"];
    [dic setValue:entity.article_avatar_url forKey:@"article_avatar_url"];
    [dic setValue:entity.article_author forKey:@"article_author"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/articles/action"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"articles/action"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"articles/action"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"articles/action"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-文章转发、收藏、点赞
- (void)getRBArticleWithAction:(NSString*)action andIId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    // ['forward','collect','like'] 分别代表、转发 搜藏(取消收藏)、点赞（取消点赞）
    [dic setObject:action forKey:@"action"];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v2/article_actions/%@/action", self.postUrl,iid]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"article_actions"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"article_actions"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"article_actions"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}




// RB-社交影响力模块
// RB-新的社交影响力
-(void)getNewRBInfluence{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@/api/v2_0/kols/influence_score", self.postUrl]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influence_score"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influence_score"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influence_score"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];

}
// RB-社交影响力提升
- (void)getRBInfluenceIncrease {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v2/influences/upgrade?kol_uuid=%@", self.postUrl,[LocalService getRBLocalDataUserKol]]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influences/upgrade"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influences/upgrade"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influences/upgrade"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-社交影响力解读
- (void)getRBInfluenceRead {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v2/influences/item_detail?kol_uuid=%@", self.postUrl,[LocalService getRBLocalDataUserKol]]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"item_detail"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"item_detail"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"item_detail"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-社交影响力分享
- (void)getRBInfluenceShare {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/influences/share"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influences/share"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influences/share"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influences/share"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-社交进入测试影响力
- (void)getRBInfluence {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/influences/start"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influences/start"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influences/start"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influences/start"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-社交账号绑定
- (void)getRBInfluenceThirdAccountBindingWithProvider:(NSString*)provider andUid:(NSString*)uid andToken:(NSString*)token andName:(NSString*)name andUrl:(NSString*)url andAvatar_url:(NSString*)avatar_url andDesc:(NSString*)desc andSerial_params:(NSString*)serial_params andFollowers_count:(NSString*)followers_count andStatuses_count:(NSString*)statuses_count andRegistered_at:(NSString*)registered_at andVerified:(NSString*)verified andRefresh_token:(NSString*)refresh_token andUnionid:(NSString*)unionid andProvince:(NSString*)province andCity:(NSString*)city andGender:(NSString*)gender andIs_vip:(NSString*)is_vip andIs_yellow_vip:(NSString*)is_yellow_vip {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:provider forKey:@"provider"];
    [dic setValue:uid forKey:@"uid"];
    [dic setValue:token forKey:@"token"];
    [dic setValue:name forKey:@"name"];
    [dic setValue:url forKey:@"url"];
    [dic setValue:avatar_url forKey:@"avatar_url"];
    [dic setValue:desc forKey:@"desc"];
    if (serial_params!=nil) {
        [dic setValue:serial_params forKey:@"serial_params"];
    }
    if (followers_count!=nil) {
        [dic setValue:followers_count forKey:@"followers_count"];
    }
    if (statuses_count!=nil) {
        [dic setValue:statuses_count forKey:@"statuses_count"];
    }
    if (registered_at!=nil) {
        [dic setValue:registered_at forKey:@"registered_at"];
    }
    if (verified!=nil) {
        [dic setValue:verified forKey:@"verified"];
    }
    if (refresh_token!=nil) {
        [dic setValue:refresh_token forKey:@"refresh_token"];
    }
    if (unionid!=nil) {
        [dic setValue:unionid forKey:@"unionid"];
    }
    if (province!=nil) {
        [dic setValue:province forKey:@"province"];
    }
    if (city!=nil) {
        [dic setValue:city forKey:@"city"];
    }
    if (gender!=nil) {
        [dic setValue:gender forKey:@"gender"];
    }
    if (is_vip!=nil) {
        [dic setValue:is_vip forKey:@"is_vip"];
    }
    if (is_yellow_vip!=nil) {
        [dic setValue:is_yellow_vip forKey:@"is_yellow_vip"];
    }
    [dic setValue:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/influences/bind_identity"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influences/identity_bind"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influences/identity_bind"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influences/identity_bind"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-社交账号解除绑定
- (void)getRBInfluenceThirdAccountUnbind:(NSString*)provider andUid:(NSString*)uid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    //    provider['weibo','wechat']
    [dic setValue:provider forKey:@"provider"];
    [dic setValue:uid forKey:@"uid"];
    [dic setValue:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/influences/unbind_identity"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influences/unbind_identity"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influences/unbind_identity"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influences/unbind_identity"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}


//api/v2/influences/bind_contacts

//RB-社交账号绑定界面上传通讯录
- (void)getRBSocialInfluenceContactWithArray:(NSMutableArray*)array {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    //    [dic setObject:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    [dic setObject:[array JSONRepresentation] forKey:@"contacts"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/influences/bind_contacts"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influences/bind_contacts"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influences/bind_contacts"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influences/bind_contacts"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-上传通讯录
- (void)getRBInfluenceContactWithArray:(NSMutableArray*)array {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    [dic setObject:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    [dic setObject:[array JSONRepresentation] forKey:@"contacts"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"/api/v2_0/contacts/kol_contacts"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_contacts"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_contacts"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_contacts"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-社交账号排名
- (void)getRBInfluenceRank {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/influences/rank"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influences/rank"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influences/rank"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influences/rank"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-社交账号排名-分页
- (void)getRBInfluenceRankWithPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:page forKey:@"page"];
    [dic setValue:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/influences/rank_with_page"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influences/rank"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influences/rank"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influences/rank"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
//api/v2/influences/send_invite
// RB-短信邀请
- (void)getRBInfluenceSmsWithMobile:(NSString*)mobile {
    mobile = [mobile stringByReplacingOccurrencesOfString:@" (" withString:@""];
    mobile = [mobile stringByReplacingOccurrencesOfString:@") " withString:@""];
    mobile = [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobile = [mobile stringByReplacingOccurrencesOfString:@"+86" withString:@""];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
  //  [dic setObject:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    [dic setObject:mobile forKey:@"mobile_number"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"/api/v2_0/contacts/send_invitation"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influences/send_invite"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influences/send_invite"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influences/send_invite"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-未允许获取通讯录测试结果
- (void)getRBInfluenceUnResult {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/influences/without_contacts_score"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"without_contacts_score"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"without_contacts_score"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"without_contacts_score"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-允许获取通讯录测试结果
- (void)getRBInfluenceInResultWithCity:(NSString*)city {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    [dic setObject:city forKey:@"kol_city"];
    [dic setObject:[Utils getCurrentDeviceVersion] forKey:@"kol_mobile_model"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/influences/cal_score"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"cal_score"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"cal_score"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"cal_score"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-通过IP获取城市名
- (void)getRBInfluenceCityName {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:@"http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json"
                              params:dic
                              httpMethod:@"GET"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        [LocalService setRBLocalDataUserCityWith:[completedOperation.responseJSON objectForKey:@"city"] andName:[completedOperation.responseJSON objectForKey:@"city"]];
        [LocalService setRBLocalDataUserKolCityWith:[completedOperation.responseJSON objectForKey:@"city"]];
        [self.delegate handlerSuccess:completedOperation.responseJSON
                                  Tag:@"iplookup"];
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    [self.delegate handlerError:error Tag:@""];
                }];
    [self.engine enqueueOperation:op];
}
// RB-设置城市
- (void)getRBInfluenceCitySet {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[LocalService getRBLocalDataUserCity] forKey:@"city_name"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/kols/set_city"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"set_city"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"set_city"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"set_city"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-影响力详情
- (void)getRBInfluenceDetail {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@%@", self.postUrl,
                                                      @"api/v1_5/influences?kol_uuid=",[LocalService getRBLocalDataUserKol]]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influences"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influences"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influences"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-影响力详情-标签云
- (void)getRBInfluenceDetailCloud {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@%@", self.postUrl,
                                                      @"api/v1_5/influences/my_analysis?kol_uuid=",[LocalService getRBLocalDataUserKol]]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"my_analysis"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"my_analysis"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"my_analysis"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
//RB-其他KOL测试影响力
-(void)getOtherRBbindSocialAccounts:(NSString*)kol_id andProvider:(NSString*)provider{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@/api/v2_0/kols/%@/similar_kol_details", self.postUrl,
                                                      kol_id]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"similar_kol_details"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"similar_kol_details"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    NSNumber *status = [errorOperation.responseJSON objectForKey:@"error"];
                    if ([status intValue] != 0) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"similar_kol_details"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

//RB-开始测试影响力
-(void)getRBbindSocialAccounts:(NSString*)kol_id andProvider:(NSString*)provider{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:provider forKey:@"provider"];
    if (kol_id.length>0) {
        [dic setValue:kol_id forKey:@"kol_id"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/kols/calculate_influence_score"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];

    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"calculate_influence_score"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"calculate_influence_score"];
        }
        
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"calculate_influence_score"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
-(void)getRBTestInfluence{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"/api/v2_0/kols/influence_score"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"influence_score"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"influence_score"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    NSNumber *status = [errorOperation.responseJSON objectForKey:@"error"];
                    if ([status intValue] != 0) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"influence_score"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-登录模块
// RB-强制更新
- (void)getRBUserVersionUpdate {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"IOS" forKey:@"app_platform"];
    [dic setValue:[Utils getCurrentBundleShortVersion] forKey:@"app_version"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/upgrades/check"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"check"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"check"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"check"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-点击banner调用，banner统计
- (void)getRBBannerClick:(NSString*)announcement_Id andparams_json:(NSString*)params_json{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:announcement_Id forKey:@"announcement_id"];
    [dic setValue:params_json forKey:@"params_json"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/announcements/click"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"announcements/click"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"announcements/click"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"announcements/click"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-是否开启第三方登录
- (void)getRBUserLoginThirdEnable {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/configs/identify_enabled"]
                              params:dic
                              httpMethod:@"GET"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"identify_enabled"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"identify_enabled"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    NSNumber *status = [errorOperation.responseJSON objectForKey:@"error"];
                    if ([status intValue] != 0) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"identify_enabled"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取短信验证码
- (void)getRBUserLoginPhoneCodeWith:(NSString*)phone {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    phone = [phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    [dic setValue:phone forKey:@"mobile_number"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/phones/get_code"]
                              params:dic
                              httpMethod:@"POST"];
  //  [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"phones/get_code"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"phones/get_code"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"phones/get_code"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
    
}
// RB-获取邮箱验证码
- (void)getRBUserLoginEmailCodeWith:(NSString*)email Andtype:(NSString*)type{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:email forKey:@"email"];
    if (type != nil) {
        [dic setValue:type forKey:@"type"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/registers/valid_code"]
                              params:dic
                              httpMethod:@"GET"];
    //  [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"registers/valid_code"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"registers/valid_code"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"detail"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"registers/valid_code"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-邮箱验证码验证
- (void)validationRBEmailCode:(NSString*)email AndCode:(NSString*)emailCode{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:email forKey:@"email"];
    [dic setValue:emailCode forKey:@"valid_code"];
    
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/registers/valid_email"]
                              params:dic
                              httpMethod:@"POST"];
    //  [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"registers/valid_email"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"registers/valid_email"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"detail"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"registers/valid_email"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-邮箱注册名字加密码
- (void)getRBRegisterName:(NSString*)name AndPassword:(NSString*)password AndEmail:(NSString*)email AndVtoken:(NSString*)vtoken{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:name forKey:@"name"];
    [dic setValue:email forKey:@"email"];
    [dic setValue:password forKey:@"password"];
    [dic setValue:vtoken forKey:@"vtoken"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/registers"]
                              params:dic
                              httpMethod:@"POST"];
    //  [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"registers"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"registers"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"detail"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"registers"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-邮箱忘记密码
- (void)getRBUpdatePassword:(NSString*)email AndNewPassword:(NSString*)newpassword  AndpasswordConfirm:(NSString*)password_confirmation andVtoken:(NSString*)vtoken{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:email forKey:@"email"];
    [dic setValue:newpassword forKey:@"new_password"];
    [dic setValue:password_confirmation forKey:@"new_password_confirmation"];
    [dic setValue:vtoken forKey:@"vtoken"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/sessions/update_password"]
                              params:dic
                              httpMethod:@"POST"];
    //  [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"sessions/update_password"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"sessions/update_password"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"detail"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"sessions/update_password"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取语音验证码
- (void)getRBUserLoginPhoneVoiceCodeWith:(NSString*)phone {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    phone = [phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    [dic setValue:phone forKey:@"mobile_number"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/phones/get_voice_code"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"phones/get_code"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"phones/get_code"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"phones/get_code"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-登录
- (void)getRBUserLoginWithPhone:(NSString*)phone andCode:(NSString*)code andInvteCode:(NSString*)inviteCode{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    phone = [phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    code = [code stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [dic setValue:phone forKey:@"mobile_number"];
    [dic setValue:code forKey:@"code"];
    if (inviteCode != nil) {
        [dic setValue:inviteCode forKey:@"invite_code"];
    }
    [dic setValue:@"IOS" forKey:@"app_platform"];
    [dic setValue:@"AppStore" forKey:@"utm_source"];
    [dic setValue:[Utils getCurrentDeviceSystemVersion] forKey:@"os_version"];
    [dic setValue:[Utils getCurrentDeviceVersion] forKey:@"device_model"];
    [dic setValue:[Utils getCurrentBundleShortVersion] forKey:@"app_version"];
    [dic setValue:[LocalService getRBLocalDataUserKolCity] forKey:@"city_name"];
    if ([LocalService getRBLocalDataUserClientId]==nil) {
        [dic setValue:self.deviceToken forKey:@"device_token"];
    } else {
        [dic setValue:[LocalService getRBLocalDataUserClientId] forKey:@"device_token"];
    }
    if ([LocalService getRBLocalDataUserIDFA]!=nil) {
        [dic setValue:[LocalService getRBLocalDataUserIDFA] forKey:@"IDFA"];
    }
    [dic setValue:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/kols/sign_in"]
                              params:dic
                              httpMethod:@"POST"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kols/sign_in"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kols/sign_in"];
        }
        // TalkingData Adtracking
        if([AppBundleIdentifier isEqualToString:@"com.robin8.rb"]) {
            [TalkingDataAppCpa onLogin:phone];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kols/sign_in"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-邮箱登录
- (void)getRBUserLoginWithEmail:(NSString*)email andCode:(NSString*)password{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:email forKey:@"login"];
    [dic setValue:password forKey:@"password"];
//    phone = [phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
//    code = [code stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//    [dic setValue:phone forKey:@"mobile_number"];
//    [dic setValue:code forKey:@"code"];
//    if (inviteCode != nil) {
//        [dic setValue:inviteCode forKey:@"invite_code"];
//    }
//    [dic setValue:@"IOS" forKey:@"app_platform"];
//    [dic setValue:@"AppStore" forKey:@"utm_source"];
//    [dic setValue:[Utils getCurrentDeviceSystemVersion] forKey:@"os_version"];
//    [dic setValue:[Utils getCurrentDeviceVersion] forKey:@"device_model"];
//    [dic setValue:[Utils getCurrentBundleShortVersion] forKey:@"app_version"];
//    [dic setValue:[LocalService getRBLocalDataUserKolCity] forKey:@"city_name"];
    
//    if ([LocalService getRBLocalDataUserClientId]==nil) {
//        [dic setValue:self.deviceToken forKey:@"device_token"];
//    } else {
//        [dic setValue:[LocalService getRBLocalDataUserClientId] forKey:@"device_token"];
//    }
//    if ([LocalService getRBLocalDataUserIDFA]!=nil) {
//        [dic setValue:[LocalService getRBLocalDataUserIDFA] forKey:@"IDFA"];
//    }
//    [dic setValue:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/sessions"]
                              params:dic
                              httpMethod:@"POST"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"sessions"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"sessions"];
        }
        // TalkingData Adtracking
        if([AppBundleIdentifier isEqualToString:@"com.robin8.rb"]) {
            [TalkingDataAppCpa onLogin:email];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"detail"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"sessions"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-第三方登录
- (void)getRBUserLoginThirdWithProvider:(NSString*)provider andUid:(NSString*)uid andToken:(NSString*)token andName:(NSString*)name andUrl:(NSString*)url andAvatar_url:(NSString*)avatar_url andDesc:(NSString*)desc andSerial_params:(NSString*)serial_params andFollowers_count:(NSString*)followers_count andStatuses_count:(NSString*)statuses_count andRegistered_at:(NSString*)registered_at andVerified:(NSString*)verified andRefresh_token:(NSString*)refresh_token andUnionid:(NSString*)unionid andProvince:(NSString*)province andCity:(NSString*)city andGender:(NSString*)gender andIs_vip:(NSString*)is_vip andIs_yellow_vip:(NSString*)is_yellow_vip {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"IOS" forKey:@"app_platform"];
    [dic setValue:[Utils getCurrentBundleShortVersion] forKey:@"app_version"];
    if ([LocalService getRBLocalDataUserClientId]==nil) {
        [dic setValue:self.deviceToken forKey:@"device_token"];
    } else {
        [dic setValue:[LocalService getRBLocalDataUserClientId] forKey:@"device_token"];
    }
    if ([LocalService getRBLocalDataUserIDFA]!=nil) {
        [dic setValue:[LocalService getRBLocalDataUserIDFA] forKey:@"IDFA"];
    }
    [dic setValue:[LocalService getRBLocalDataUserKolCity] forKey:@"city_name"];
    [dic setValue:[Utils getCurrentDeviceSystemVersion] forKey:@"os_version"];
    [dic setValue:[Utils getCurrentDeviceVersion] forKey:@"device_model"];
    [dic setValue:provider forKey:@"provider"];
    [dic setValue:uid forKey:@"uid"];
    [dic setValue:token forKey:@"token"];
    [dic setValue:name forKey:@"name"];
    [dic setValue:url forKey:@"url"];
    [dic setValue:avatar_url forKey:@"avatar_url"];
    [dic setValue:desc forKey:@"desc"];
    [dic setValue:@"AppStore" forKey:@"utm_source"];
    if (serial_params!=nil) {
        [dic setValue:serial_params forKey:@"serial_params"];
    }
    if (followers_count!=nil) {
        [dic setValue:followers_count forKey:@"followers_count"];
    }
    if (statuses_count!=nil) {
        [dic setValue:statuses_count forKey:@"statuses_count"];
    }
    if (registered_at!=nil) {
        [dic setValue:registered_at forKey:@"registered_at"];
    }
    if (verified!=nil) {
        [dic setValue:verified forKey:@"verified"];
    }
    if (refresh_token!=nil) {
        [dic setValue:refresh_token forKey:@"refresh_token"];
    }
    if (unionid!=nil) {
        [dic setValue:unionid forKey:@"unionid"];
    }
    if (province!=nil) {
        [dic setValue:province forKey:@"province"];
    }
    if (city!=nil) {
        [dic setValue:city forKey:@"city"];
    }
    if (gender!=nil) {
        [dic setValue:gender forKey:@"gender"];
    }
    if (is_vip!=nil) {
        [dic setValue:is_vip forKey:@"is_vip"];
    }
    if (is_yellow_vip!=nil) {
        [dic setValue:is_yellow_vip forKey:@"is_yellow_vip"];
    }
    [dic setValue:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/kols/oauth_login"]
                              params:dic
                              httpMethod:@"POST"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kols/oauth_login"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kols/oauth_login"];
        }
        
        // TalkingData Adtracking
        if([AppBundleIdentifier isEqualToString:@"com.robin8.rb"]) {
            [TalkingDataAppCpa onLogin:uid];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kols/oauth_login"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取用户IM信息
- (void)getRBUserLoginIMWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/common?kol_id=%@", self.postUrl, iid]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"common"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"common"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"common"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-对他人是否可见权限
- (void)getRBPermitToothers:(NSString*)action{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:action forKey:@"action"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"/api/v2_0/kols/manage_influence_visibility"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"manage_influence_visibility"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"manage_influence_visibility"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"manage_influence_visibility"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-用户模块
// RB-获取用户信息
- (void)getRBUserInfo {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/kols/profile"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kols/profile"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kols/profile"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kols/profile"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-修改用户头像
- (void)getRBUserInfoAvatarUpdate:(UIImage*)avtar {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/kols/upload_avatar"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addData:UIImageJPEGRepresentation(avtar, 1.0f)
         forKey:@"avatar"
       mimeType:@"image/jpeg"
       fileName:@"avatar.jpg"];
    [op onUploadProgressChanged:^(double progress) {
        if(progress >= 1){
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R5174",@"头像上传成功")];
        } else {
            [SVProgressHUD showProgress:progress status:NSLocalizedString(@"R5175",@"头像上传中...")];
        }
    }];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"upload_avatar"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"upload_avatar"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"upload_avatar"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-修改用户信息
- (void)getRBUserInfoUpdateWithEmail:(NSString*)email andName:(NSString*)name andWeixin_friend_count:(NSString*)weixin_friend_count andAge:(NSString*)age andGender:(NSString*)gender andBirthday:(NSString*)date_of_birthday andCountry:(NSString*)country andCity:(NSString*)city andDesc:(NSString*)desc andTags:(NSArray*)tags andAlipay:(NSString*)alipay_account {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (weixin_friend_count!=nil) {
        weixin_friend_count = [weixin_friend_count stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [dic setObject:weixin_friend_count forKey:@"weixin_friend_count"];
    }
    if (age!=nil) {
        age = [age stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [dic setObject:age forKey:@"age"];
    }
    if (email!=nil) {
        email = [email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [dic setObject:email forKey:@"email"];
    }
    if (name!=nil) {
        name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [dic setObject:name forKey:@"name"];
    }
    if (gender!=nil) {
        [dic setObject:gender forKey:@"gender"];
    }
    if (date_of_birthday!=nil) {
        [dic setObject:date_of_birthday forKey:@"date_of_birthday"];
    }
    if (country!=nil) {
        country = [country stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [dic setObject:country forKey:@"country"];
    }
    if (city!=nil) {
        city = [city stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [dic setObject:city forKey:@"app_city"];
    }
    if (desc!=nil) {
        desc = [desc stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [dic setObject:desc forKey:@"desc"];
    }
    if (tags!=nil) {
        NSString*srcdic=@"";
        for (int i=0;i<[tags count];i++) {
            if (i==0) {
                srcdic=[NSString stringWithFormat:@"%@",tags[i]];
            } else {
                srcdic=[NSString stringWithFormat:@"%@,%@",srcdic,tags[i]];
            }
        }
        [dic setObject:srcdic forKey:@"tags"];
    }
    if (alipay_account!=nil) {
        alipay_account = [alipay_account stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [dic setObject:alipay_account forKey:@"alipay_account"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/kols/update_profile"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"update_profile"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"update_profile"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    NSNumber *status = [errorOperation.responseJSON objectForKey:@"error"];
                    if ([status intValue] == 1) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"update_profile"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取城市列表
- (void)getRBUserInfoCitys {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/cities"]
                              params:dic
                              httpMethod:@"GET"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"cities"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"cities"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"cities"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取用户首页信息
- (void)getRBUserInfoDashboard {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_3/my/show"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"primary"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"primary"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"primary"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取用户标签
- (void)getRBUserInfoTags {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_5/tags/list"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"tags"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"tags"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"tags"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-绑定新手机号码
- (void)getRBUserInfoNewPhoneWithPhone:(NSString*)phone andCode:(NSString*)code {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    phone = [phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    [dic setValue:phone forKey:@"mobile_number"];
    [dic setValue:code forKey:@"code"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/kols/update_mobile"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"update_mobile"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"update_mobile"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"update_mobile"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
//第三方账号绑定V_2
- (void)getV2RBUserInfoThirdAccountBindingWithProvider:(NSString*)provider andUid:(NSString*)uid andToken:(NSString*)token andName:(NSString*)name andUrl:(NSString*)url andAvatar_url:(NSString*)avatar_url andDesc:(NSString*)desc andSerial_params:(NSString*)serial_params andFollowers_count:(NSString*)followers_count andStatuses_count:(NSString*)statuses_count andRegistered_at:(NSString*)registered_at andVerified:(NSString*)verified andRefresh_token:(NSString*)refresh_token andUnionid:(NSString*)unionid  andProvince:(NSString*)province andCity:(NSString*)city andGender:(NSString*)gender andIs_vip:(NSString*)is_vip andIs_yellow_vip:(NSString*)is_yellow_vip{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"IOS" forKey:@"app_platform"];
    [dic setValue:[Utils getCurrentBundleShortVersion] forKey:@"app_version"];
    if ([LocalService getRBLocalDataUserClientId]==nil) {
        [dic setValue:self.deviceToken forKey:@"device_token"];
    } else {
        [dic setValue:[LocalService getRBLocalDataUserClientId] forKey:@"device_token"];
    }
    if ([LocalService getRBLocalDataUserIDFA]!=nil) {
        [dic setValue:[LocalService getRBLocalDataUserIDFA] forKey:@"IDFA"];
    }
    [dic setValue:provider forKey:@"provider"];
    [dic setValue:uid forKey:@"uid"];
    [dic setValue:token forKey:@"token"];
    [dic setValue:name forKey:@"name"];
    [dic setValue:url forKey:@"url"];
    [dic setValue:avatar_url forKey:@"avatar_url"];
    [dic setValue:desc forKey:@"desc"];
    if (serial_params!=nil) {
        [dic setValue:serial_params forKey:@"serial_params"];
    }
    if (followers_count!=nil) {
        [dic setValue:followers_count forKey:@"followers_count"];
    }
    if (statuses_count!=nil) {
        [dic setValue:statuses_count forKey:@"statuses_count"];
    }
    if (registered_at!=nil) {
        [dic setValue:registered_at forKey:@"registered_at"];
    }
    if (verified!=nil) {
        [dic setValue:verified forKey:@"verified"];
    }
    if (refresh_token!=nil) {
        [dic setValue:refresh_token forKey:@"refresh_token"];
    }
    if (unionid!=nil) {
        [dic setValue:unionid forKey:@"unionid"];
    }
    if (province!=nil) {
        [dic setValue:province forKey:@"province"];
    }
    if (city!=nil) {
        [dic setValue:city forKey:@"city"];
    }
    if (gender!=nil) {
        [dic setValue:gender forKey:@"gender"];
    }
    if (is_vip!=nil) {
        [dic setValue:is_vip forKey:@"is_vip"];
    }
    if (is_yellow_vip!=nil) {
        [dic setValue:is_yellow_vip forKey:@"is_yellow_vip"];
    }
    [dic setValue:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/kols/identity_bind_v2"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kols/identity_bind_v2"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kols/identity_bind_v2"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kols/identity_bind_v2"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-第三方账号绑定
- (void)getRBUserInfoThirdAccountBindingWithProvider:(NSString*)provider andUid:(NSString*)uid andToken:(NSString*)token andName:(NSString*)name andUrl:(NSString*)url andAvatar_url:(NSString*)avatar_url andDesc:(NSString*)desc andSerial_params:(NSString*)serial_params andFollowers_count:(NSString*)followers_count andStatuses_count:(NSString*)statuses_count andRegistered_at:(NSString*)registered_at andVerified:(NSString*)verified andRefresh_token:(NSString*)refresh_token andUnionid:(NSString*)unionid  andProvince:(NSString*)province andCity:(NSString*)city andGender:(NSString*)gender andIs_vip:(NSString*)is_vip andIs_yellow_vip:(NSString*)is_yellow_vip {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"IOS" forKey:@"app_platform"];
    [dic setValue:[Utils getCurrentBundleShortVersion] forKey:@"app_version"];
    if ([LocalService getRBLocalDataUserClientId]==nil) {
        [dic setValue:self.deviceToken forKey:@"device_token"];
    } else {
        [dic setValue:[LocalService getRBLocalDataUserClientId] forKey:@"device_token"];
    }
    if ([LocalService getRBLocalDataUserIDFA]!=nil) {
        [dic setValue:[LocalService getRBLocalDataUserIDFA] forKey:@"IDFA"];
    }
    [dic setValue:provider forKey:@"provider"];
    [dic setValue:uid forKey:@"uid"];
    [dic setValue:token forKey:@"token"];
    [dic setValue:name forKey:@"name"];
    [dic setValue:url forKey:@"url"];
    [dic setValue:avatar_url forKey:@"avatar_url"];
    [dic setValue:desc forKey:@"desc"];
    if (serial_params!=nil) {
        [dic setValue:serial_params forKey:@"serial_params"];
    }
    if (followers_count!=nil) {
        [dic setValue:followers_count forKey:@"followers_count"];
    }
    if (statuses_count!=nil) {
        [dic setValue:statuses_count forKey:@"statuses_count"];
    }
    if (registered_at!=nil) {
        [dic setValue:registered_at forKey:@"registered_at"];
    }
    if (verified!=nil) {
        [dic setValue:verified forKey:@"verified"];
    }
    if (refresh_token!=nil) {
        [dic setValue:refresh_token forKey:@"refresh_token"];
    }
    if (unionid!=nil) {
        [dic setValue:unionid forKey:@"unionid"];
    }
    if (province!=nil) {
        [dic setValue:province forKey:@"province"];
    }
    if (city!=nil) {
        [dic setValue:city forKey:@"city"];
    }
    if (gender!=nil) {
        [dic setValue:gender forKey:@"gender"];
    }
    if (is_vip!=nil) {
        [dic setValue:is_vip forKey:@"is_vip"];
    }
    if (is_yellow_vip!=nil) {
        [dic setValue:is_yellow_vip forKey:@"is_yellow_vip"];
    }
    [dic setValue:[LocalService getRBLocalDataUserKol] forKey:@"kol_uuid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/kols/identity_bind"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kols/identity_bind"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kols/identity_bind"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                      //  [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kols/identity_bind"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-第三方登录绑定手机
- (void)getRBUserInfoThirdAccountBindingWithPhone:(NSString*)phone andCode:(NSString*)code {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    phone = [phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    [dic setValue:phone forKey:@"mobile_number"];
    [dic setValue:code forKey:@"code"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/kols/bind_mobile"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"bind_mobile"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"bind_mobile"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"bind_mobile"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-第三方账号列表
- (void)getRBUserInfoThirdAccountList {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/kols/identities"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"identities"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"identities"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"identities"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-第三方账号解除绑定
- (void)getRBUserInfoThirdAccountUnbind:(NSString*)uid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:uid forKey:@"uid"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/kols/identity_unbind"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"identity_unbind"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"identity_unbind"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"identity_unbind"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-支付宝账号绑定
- (void)getRBUserInfoZhiFuBaoBindingWithName:(NSString*)name andAccout:(NSString*)account andIdcard:(NSString*)idcard {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    account = [account stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    idcard = [idcard stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [dic setObject:account forKey:@"alipay_account"];
    [dic setObject:name forKey:@"alipay_name"];
    [dic setObject:idcard forKey:@"id_card"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_3/kols/bind_alipay"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"bind_alipay"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"bind_alipay"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"bind_alipay"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-我是大V-列表
- (void)getRBUserInfoBigV {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_3/kol_identity_prices/list"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_identity_prices/list"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_identity_prices/list"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_identity_prices/list"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-我是大V-填写
- (void)getRBUserInfoBigVWithProvider:(NSString*)provider andName:(NSString*)name andFollower_count:(NSString*)follower_count andBelong_field:(NSString*)belong_field andHeadline_price:(NSString*)headline_price andSecond_price:(NSString*)second_price andSingle_price:(NSString*)single_price {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    follower_count = [follower_count stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    belong_field = [belong_field stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    headline_price = [headline_price stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    second_price = [second_price stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    single_price = [single_price stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [dic setObject:provider forKey:@"provider"];
    [dic setObject:name forKey:@"name"];
    [dic setObject:follower_count forKey:@"follower_count"];
    [dic setObject:belong_field forKey:@"belong_field"];
    if(headline_price!=nil){
        [dic setObject:headline_price forKey:@"headline_price"];
    }
    if(second_price!=nil){
        [dic setObject:second_price forKey:@"second_price"];
    }
    if(single_price!=nil){
        [dic setObject:single_price forKey:@"single_price"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_3/kol_identity_prices/set_price"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_identity_prices/set_price"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_identity_prices/set_price"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_identity_prices/set_price"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-我的页面-列表
- (void)getRBUserInfoProfile {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_6/my/show"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"my/show"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"my/show"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"my/show"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-用户信息修改
- (void)getRBUserInfoUpdateLongitudeLatitude {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"IOS" forKey:@"app_platform"];
    [dic setValue:@"AppStore" forKey:@"utm_source"];
    [dic setValue:[LocalService getRBLocalDataUserLongitude] forKey:@"longitude"];
    [dic setValue:[LocalService getRBLocalDataUserLatitude] forKey:@"latitude"];
    [dic setValue:[Utils getCurrentDeviceSystemVersion] forKey:@"os_version"];
    [dic setValue:[Utils getCurrentDeviceVersion] forKey:@"device_model"];
    [dic setValue:[Utils getCurrentBundleShortVersion] forKey:@"app_version"];
    [dic setValue:[LocalService getRBLocalDataUserKolCity] forKey:@"city_name"];
    if ([LocalService getRBLocalDataUserClientId]==nil) {
        [dic setValue:self.deviceToken forKey:@"device_token"];
    } else {
        [dic setValue:[LocalService getRBLocalDataUserClientId] forKey:@"device_token"];
    }
    if ([LocalService getRBLocalDataUserIDFA]!=nil) {
        [dic setValue:[LocalService getRBLocalDataUserIDFA] forKey:@"IDFA"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_3/kols/update_profile"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kols/update_profile"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kols/update_profile"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kols/update_profile"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-输入邀请码-提交
- (void)submitInviteCode:(NSString*)inviteCode{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    [dic setValue:inviteCode forKey:@"invite_code"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/kols/invite_code"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kols/invite_code"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kols/invite_code"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        //  [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kols/invite_code"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
//RB-文章模块
//RB-文章推荐
- (void)getRBRecommendTag:(NSString*)tag andPost_id:(NSString*)post_id{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:tag forKey:@"tag"];
    [dic setObject:post_id forKey:@"post_id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/articles/recommends"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"articles/recommends"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"articles/recommends"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"articles/recommends"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
//RB-文章详情计时
- (void)getRBRecommendPost_id:(NSString*)post_id AndStayTime:(NSString*)stay_time{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    [dic setValue:post_id forKey:@"post_id"];
    [dic setValue:stay_time forKey:@"stay_time"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/articles/read"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"articles/read"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"articles/read"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        //  [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"articles/read"];
                    } else {
                        [self.delegate handlerError:error Tag:@"articles/read"];
                    }
                }];
    [self.engine enqueueOperation:op];
}
//RB-获取文章列表
- (void)getRBAddOrCancellike:(NSString*)type andpost_id:(NSString*)post_id andType:(NSString*)action{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    [dic setValue:action forKey:@"_action"];
    [dic setValue:post_id forKey:@"post_id"];
    [dic setValue:type forKey:@"_type"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/articles/set"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"articles/set"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"articles/set"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        //  [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kols/invite_code"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
//我的收藏
- (void)getRBMyCollectArticleList:(NSString*)action andPageIndex:(int)page{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    [dic setValue:action forKey:@"_action"];
    [dic setValue:[NSString stringWithFormat:@"%d",page] forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/my/article_lists"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"my/article_lists"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"my/article_lists"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        //  [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kols/invite_code"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

//文章列表
- (void)getRBArticleslist:(NSString*)type andPage:(int)page{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (type.length > 0) {
        [dic setObject:type forKey:@"_type"];
    }
    [dic setObject:[NSString stringWithFormat:@"%d",page] forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/articles"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"articles"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"articles"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"articles"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-浏览文章拆开红包
- (void)getRBApartRedBag:(NSString*)red_money{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:red_money forKey:@"red_money"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/articles/split_red"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"split_red"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"split_red"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"split_red"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-活动模块
// RB-第一次做任务给奖励
- (void)getRBFirstDoCampaigngetTheAward{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/tasks/finish_newbie"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"finish_newbie"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"finish_newbie"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"finish_newbie"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取活动列表
- (void)getRBActivityWithStatus:(NSString*)status andTitle:(NSString*)title andWith_message:(NSString*)with_message andPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    // status：['all','running','approved','verifying','settled','liked']
    // 所有活动、待接收活动、进行中活动、待审核活动、已完成活动、收藏的活动
    [dic setValue:status forKey:@"status"];
    if (title!=nil) {
        [dic setValue:title forKey:@"title"];
    }
    if (with_message!=nil) {
        [dic setValue:with_message forKey:@"with_message_stat"];
    }
    [dic setValue:@"y" forKey:@"with_announcements"];
    [dic setValue:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/campaign_invites"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"campaign_invites"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"campaign_invites"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"campaign_invites"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-我的活动界面获取活动列表
- (void)getRBActivityWithStatus:(NSString*)status andPage:(NSString*)page{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    // 所有活动、待接收活动、进行中活动、待审核活动、已完成活动、收藏的活动
    [dic setValue:status forKey:@"status"];
    [dic setValue:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/campaign_invites/my_campaigns"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"my_campaigns"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"my_campaigns"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"my_campaigns"];
                    } else {
                        [self.delegate handlerError:error Tag:@"my_campaigns"];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取活动详情
- (void)getRBActivityDetailWithId:(NSString*)activityId {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"1" forKey:@"invitee_page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaign_invites/%@", self.postUrl,activityId]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"campaign_invites_detail"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"campaign_invites_detail"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"campaign_invites_detail"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取活动参与
- (void)getRBActivityEnjoyWithId:(NSString*)activityId {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaign_invites/%@/approve", self.postUrl,activityId]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"campaign_invites_approve"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"campaign_invites_approve"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"campaign_invites_approve"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取活动参与
- (void)getRBCampaignApproveWithId:(NSString*)campaignId {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaigns/%@/approve", self.postUrl,campaignId]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"approve"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"approve"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"approve"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-获取Campaign详情
- (void)getRBCampaignDetailWithId:(NSString*)campaignId {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"1" forKey:@"invitee_page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaigns/%@", self.postUrl,campaignId]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"campaigns_detail"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"campaigns_detail"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"campaigns_detail"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取Campaign参与
- (void)getRBCampaignEnjoyWithId:(NSString*)campaignId  {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaigns/%@/receive", self.postUrl,campaignId]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"campaigns_receive"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"campaigns_receive"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"campaigns_receive"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取活动转发成功通知
- (void)getRBActivityShareWithId:(NSString*)activityId andSuntype:(NSString*)sub_type{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:sub_type forKey:@"sub_type"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaign_invites/%@/share", self.postUrl,activityId]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"campaign_invites_share"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"campaign_invites_share"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"campaign_invites_share"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-活动转发失败通知
-(void)getRBActivityCancelWithId:(NSString*)activityId{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaign_invites/%@/cancel_share", self.postUrl,activityId]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"campaign_invites_cancel_share"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"campaign_invites_cancel_share"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"campaign_invites_cancel_share"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];

}
// RB-招募活动上传截图
- (void)getRBActivityRecuritWithId:(NSString*)activityId andScreenShoot:(NSArray*)arr andCampaignLogo:(UIImage*)campaign_logo{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaign_invites/%@/upload_screenshot", self.postUrl, activityId]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    for (NSInteger i = 0; i<arr.count; i ++) {
        UIImage * image = arr[i];
        NSData * imageData = UIImageJPEGRepresentation(image, 1.0);
        NSString * str = [NSString stringWithFormat:@"screenshot%ld",(long)(i+1)];
        [op addData:imageData forKey:str mimeType:@"image/jpeg"fileName:@"screenshot.jpg"];
    }

    if(campaign_logo!=nil) {
        [op addData:UIImageJPEGRepresentation(campaign_logo, 1.0f)
             forKey:@"campaign_logo"
           mimeType:@"image/jpeg"
           fileName:@"campaign_logo.jpg"];
    }
    [op onUploadProgressChanged:^(double progress) {
        if(progress >= 1){
            
        } else {
            [SVProgressHUD showProgress:progress status:NSLocalizedString(@"R2099", @"上传中")];
        }
    }];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R2103", @"图片上传成功")];
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"upload_screenshot"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"upload_screenshot"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"upload_screenshot"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取活动上传截图
- (void)getRBActivityUpaloadWithId:(NSString*)activityId andScreenShot:(UIImage*)screenshot andCampaignLogo:(UIImage*)campaign_logo {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaign_invites/%@/upload_screenshot", self.postUrl, activityId]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    
    [op addData:UIImageJPEGRepresentation(screenshot, 1.0f)
         forKey:@"screenshot"
       mimeType:@"image/jpeg"
       fileName:@"screenshot.jpg"];
    if(campaign_logo!=nil) {
        [op addData:UIImageJPEGRepresentation(campaign_logo, 1.0f)
             forKey:@"campaign_logo"
           mimeType:@"image/jpeg"
           fileName:@"campaign_logo.jpg"];
    }
    [op onUploadProgressChanged:^(double progress) {
        if(progress >= 1){
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R2103", @"图片上传成功")];
        } else {
            [SVProgressHUD showProgress:progress status:NSLocalizedString(@"R2099", @"上传中")];
        }
    }];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"upload_screenshot"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"upload_screenshot"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"upload_screenshot"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取活动报名资格
- (void)getRBActivityCanApplyWithId:(NSString*)activityId {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:activityId forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaigns/can_apply", self.postUrl]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"can_apply"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"can_apply"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"can_apply"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-获取活动报名
- (void)getRBActivityApplyWithId:(NSString*)activityId andNote:(NSString*)note andPicture:(UIImage*)img {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:activityId forKey:@"id"];
    [dic setValue:@"" forKey:@"name"];
    [dic setValue:@"" forKey:@"phone"];
    [dic setValue:@"" forKey:@"weixin_no"];
    [dic setValue:@"" forKey:@"weixin_friend_count"];
    [dic setValue:@"" forKey:@"expect_price"];
    [dic setValue:@"" forKey:@"remark"];
    [dic setValue:@"" forKey:@"image_ids"];
    [dic setValue:note forKey:@"note"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaigns/apply", self.postUrl]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    if(img!=nil) { 
        [op addData:UIImageJPEGRepresentation(img, 1.0f)
             forKey:@"picture"
           mimeType:@"image/jpeg"
           fileName:@"picture.jpg"];
    }
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"apply"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"apply"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"apply"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取活动-上传图片
- (void)getRBActivityPicUpload:(UIImage*)img {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_3/images/upload"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addData:UIImageJPEGRepresentation(img, 1.0f)
         forKey:@"avatar"
       mimeType:@"image/jpeg"
       fileName:@"avatar.jpg"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"upload"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"upload"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"upload"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取Campaign参与者详情
- (void)getRBCampaignKolDetailWithId:(NSString*)kolId  {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_5/kols/%@/invitee_detail", self.postUrl,kolId]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"invitee_detail"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"invitee_detail"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"invitee_detail"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取Campaign的素材列表
- (void)getRBCampaignMaterialWithId:(NSString*)campaignId andCampaignInviteId:(NSString*)campaignInviteId  {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (campaignId!=nil) {
        [dic setObject:campaignId forKey:@"campaign_id"];
    }
    if (campaignInviteId!=nil) {
        [dic setObject:campaignInviteId forKey:@"campaign_invite_id"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/campaigns/materials", self.postUrl]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"materials"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"materials"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"materials"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取Campaign拒绝活动
- (void)getRBCampaignInviteRejectWithId:(NSString*)campaignId  {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/campaign_invites/%@/reject", self.postUrl,campaignId]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"reject"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"reject"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"reject"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取Campaign参与人员分析
- (void)getRBCampaignAnalysisWithId:(NSString*)campaignId {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:campaignId forKey:@"campaign_id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_8/campaign_analysis/invitee_analysis", self.postUrl]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"invitee_analysis"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"invitee_analysis"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"invitee_analysis"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-获取Campaign参与人员分析-分页
- (void)getRBCampaignAnalysisWithId:(NSString*)campaignId andPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:campaignId forKey:@"id"];
    [dic setValue:page forKey:@"invitee_page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/campaigns/%@/invitees", self.postUrl,campaignId]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"invitees"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"invitees"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"invitees"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}



//
// RB-发布活动
- (void)getRBActivityAddWithName:(NSString*)name andDescription:(NSString*)desc andUrl:(NSString*)url andImg:(UIImage*)img andStart_time:(NSString*)start_time andDeadline:(NSString*)deadline andPer_budget_type:(NSString*)per_budget_type andBudget:(NSString*)budget andPer_action_budget:(NSString*)per_action_budget andAge:(NSString*)age andGender:(NSString*)gender andRegion:(NSString*)region andTags:(NSString*)tags andSub_type:(NSString*)sub_type{
    if([age isEqualToString:@"All"]) {
        age = @"全部";
    }
    if([gender isEqualToString:@"All"]) {
        gender = @"全部";
    }
    if([region isEqualToString:@"All"]) {
        region = @"全部";
    }
    if([tags isEqualToString:@"All"]) {
        tags = @"全部";
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:name forKey:@"name"];
    [dic setValue:desc forKey:@"description"];
    [dic setValue:url forKey:@"url"];
    [dic setValue:start_time forKey:@"start_time"];
    [dic setValue:deadline forKey:@"deadline"];
    // 活动类型(click： 点击类型， post: 转发)
    [dic setValue:per_budget_type forKey:@"per_budget_type"];
    [dic setValue:budget forKey:@"budget"];
    [dic setValue:per_action_budget forKey:@"per_action_budget"];
    [dic setValue:age forKey:@"age"];
    [dic setValue:gender forKey:@"gender"];
    [dic setValue:region forKey:@"region"];
    [dic setValue:tags forKey:@"tags"];
    //
    [dic setValue:sub_type forKey:@"sub_type"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_campaigns"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addData:UIImageJPEGRepresentation(img, 1.0f)
         forKey:@"img"
       mimeType:@"image/jpeg"
       fileName:@"img.jpg"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_campaigns/add"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_campaigns/add"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"detail"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_campaigns/add"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-使用积分抵扣
- (void)getRBActivityAddPayWithintegralID:(NSString*)iid andUse_credit:(NSString*)use_credit{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:iid forKey:@"id"];
    // 是否使用钱包余额抵扣(1 表示 使用， 0 表示不使用
    [dic setValue:use_credit forKey:@"use_credit"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_campaigns/pay_by_credit"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"pay_by_credit"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"pay_by_credit"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"pay_by_credit"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-使用钱包余额抵扣
- (void)getRBActivityAddPayByVoucherWithiid:(NSString*)iid andUsed_voucher:(NSString*)used_voucher andUseCredit:(NSString*)use_credit{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:iid forKey:@"id"];
    // 是否使用钱包余额抵扣(1 表示 使用， 0 表示不使用
    [dic setValue:used_voucher forKey:@"used_voucher"];
    [dic setValue:use_credit forKey:@"use_credit"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_campaigns/pay_by_voucher"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"pay_by_voucher"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"pay_by_voucher"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"pay_by_voucher"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-使用广告账户余额抵扣
- (void)getRBActivityAddPayByAdvertiserWithiid:(NSString*)iid{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:iid forKey:@"id"];
    [dic setValue:@"balance" forKey:@"pay_way"];
    //use_credit
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_campaigns/pay"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_campaigns/pay"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_campaigns/pay"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_campaigns/pay"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-使用支付宝付款是否成功
- (void)getRBActivityAddPayByAlipayWithiid:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:iid forKey:@"id"];
    [dic setValue:@"1" forKey:@"check_pay"];
    [dic setValue:@"true" forKey:@"use_credit"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_campaigns/show"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_campaigns/show"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_campaigns/show"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_campaigns/show"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-列表
- (void)getRBActivityAddListWithCampaign_type:(NSString*)campaign_type andPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    //活动列表类型(unpay: 待付款列表, checking: 审核中列表, running: 运行中列表, completed: 已完成列表)
    [dic setValue:campaign_type forKey:@"campaign_type"];
    [dic setValue:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_campaigns"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_campaigns"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_campaigns"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_campaigns"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-活动详情页面
- (void)getRBActivityAddDetailWithId:(NSString*)iid{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_campaigns/detail"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_campaigns/detail"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_campaigns/detail"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_campaigns/detail"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-撤销
- (void)getRBActivityAddRevokeWithId:(NSString*)iid{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_campaigns/revoke"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_campaigns/revoke"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_campaigns/revoke"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_campaigns/revoke"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-修改
- (void)getRBActivityAddEditWithId:(NSString*)iid andName:(NSString*)name andDescription:(NSString*)desc andUrl:(NSString*)url andImg:(UIImage*)img andStart_time:(NSString*)start_time andDeadline:(NSString*)deadline andPer_budget_type:(NSString*)per_budget_type andBudget:(NSString*)budget andPer_action_budget:(NSString*)per_action_budget andAge:(NSString*)age andGender:(NSString*)gender andRegion:(NSString*)region andTags:(NSString*)tags andSubtype:(NSString*)sub_type{
    if([age isEqualToString:@"All"]) {
        age = @"全部";
    }
    if([gender isEqualToString:@"All"]) {
        gender = @"全部";
    }
    if([region isEqualToString:@"All"]) {
        region = @"全部";
    }
    if([tags isEqualToString:@"All"]) {
        tags = @"全部";
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:name forKey:@"name"];
    [dic setValue:iid forKey:@"id"];
    [dic setValue:desc forKey:@"description"];
    [dic setValue:url forKey:@"url"];
    [dic setValue:start_time forKey:@"start_time"];
    [dic setValue:deadline forKey:@"deadline"];
    [dic setValue:budget forKey:@"budget"];
    // 活动类型(click： 点击类型， post: 转发)
    [dic setValue:per_budget_type forKey:@"per_budget_type"];
    [dic setValue:per_action_budget forKey:@"per_action_budget"];
    [dic setValue:age forKey:@"age"];
    [dic setValue:gender forKey:@"gender"];
    [dic setValue:region forKey:@"region"];
    [dic setValue:tags forKey:@"tags"];
    //
    [dic setValue:sub_type forKey:@"sub_type"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_campaigns/update"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addData:UIImageJPEGRepresentation(img, 1.0f)
         forKey:@"img"
       mimeType:@"image/jpeg"
       fileName:@"img.jpg"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_campaigns/update"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_campaigns/update"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_campaigns/update"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-品牌主账户
- (void)getRBActivityAddAccount {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_brand"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_brand"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_brand"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_brand"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB- 修改品牌主资料
- (void)getRBActivityModifybrandwith:(NSMutableDictionary*)dicT andAvatar:(UIImage*)avatarImage{
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]initWithDictionary:dicT];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_brand/update_profile"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    if (avatarImage != nil) {
        [op addData:UIImageJPEGRepresentation(avatarImage, 1.0f)
             forKey:@"avatar"
           mimeType:@"image/jpeg"
           fileName:@"avatar.jpg"];
    }
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_brand/update_profile"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_brand/update_profile"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_brand/update_profile"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-品牌主账单
- (void)getRBActivityAddMyAccountWith:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_brand/bill"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"kol_brand/bill"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"kol_brand/bill"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"kol_brand/bill"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-积分账单
- (void)getRBCreditListwithPage:(NSString*)page{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/credits"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"credits"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"credits"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"credits"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-参与人员列表
- (void)getRBActivityAddMyListEnjoyWithId:(NSString*)iid andPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:iid forKey:@"id"];
    [dic setValue:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_campaigns/joined_kols"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"joined_kols"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"joined_kols"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"joined_kols"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-广告账户充值
- (void)getRBActivityAddMyRechargeWithCredits:(NSString*)credits andCode:(NSString*)code {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:credits forKey:@"credits"];
    [dic setValue:code forKey:@"invite_code"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_4/kol_brand/recharge"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"recharge"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"recharge"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"detail"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"recharge"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-充值信息
- (void)getRBRecharge{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
  
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2_0/promotions"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"promotions"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"promotions"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"detail"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"promotions"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-期望效果
- (void)getRBActivityAddResult {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_8/campaign_analysis/expect_effect_list"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"expect_effect_list"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"expect_effect_list"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"expect_effect_list"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-发布活动-文章分析
- (void)getRBActivityAddAnalysisWithUrl:(NSString*)url andExpect:(NSString*)expect {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:url forKey:@"url"];
    [dic setValue:expect forKey:@"expect_effect"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_8/campaign_analysis/content_analysis"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"analysis"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"analysis"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    NSLog(@"%@",error.description);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"analysis"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}




// RB-意见反馈模块
// RB-获取意见反馈
- (void)getRBUserHelpFeedBackWithContent:(NSString*)content andScreen:(UIImage*)screenshot {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:[Utils getCurrentBundleShortVersion] forKey:@"app_version"];
    [dic setValue:[Utils getCurrentDeviceSystemVersion] forKey:@"os_version"];
    [dic setValue:[Utils getCurrentDeviceVersion] forKey:@"device_model"];
    [dic setValue:@"IOS" forKey:@"app_platform"];
    [dic setValue:content forKey:@"content"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1/feedbacks/create"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    if(screenshot!=nil) {
        [op addData:UIImageJPEGRepresentation(screenshot, 1.0f) forKey:@"screenshot" mimeType:@"image/jpeg" fileName:@"screenshot.jpg"];
    }
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"feedbacks"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"feedbacks"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"feedbacks"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取QA
- (void)getRBUserHelpQA {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v2/system/account_notice"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"account_notice"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"account_notice"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"account_notice"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取扫码登录网站
- (void)getRBUserHelpScanCode:(NSString*)code {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:code forKey:@"login_token"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_5/scan_qr_code_and_login"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"scan_qr_code_and_login"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"scan_qr_code_and_login"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"scan_qr_code_and_login"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-帮助中心-列表
- (void)getRBUserHelpListWithPage:(int)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[NSString stringWithFormat:@"%d",page] forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/system/account_notice",self.postUrl]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"account_notice"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"account_notice"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"account_notice"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}



// RB-消息模块
// RB-获取消息列表
- (void)getRBMessageListWithStatus:(NSString*)status andPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    // status ['all','unread','read']
    [dic setObject:status forKey:@"status"];
    [dic setObject:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,@"api/v1/messages"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"messages"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"messages"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"messages"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取消息阅读
- (void)getRBMessageRead:(NSString*)msgId {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/messages/%@/read", self.postUrl,msgId]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"messages_read"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"messages_read"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"messages_read"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取消息全部设置已读
- (void)getRBMessageAllRead {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,@"api/v2/messages/read_all"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"messages_read_all"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"messages_read_all"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"messages_read_all"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}



// RB-收入模块
// RB-获取用户账户
- (void)getRBAccount {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,@"api/v1/kols/account"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"account"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"account"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"account"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取用户账单列表
- (void)getRBAccountIncome {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,@"api/v1/transactions/recent_income"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"recent_income"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"recent_income"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"recent_income"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取用户提现申请
- (void)getRBAccountCashWithCredits:(NSString*)credits {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    credits = [credits stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [credits stringByReplacingOccurrencesOfString:@" " withString:@""];
    [dic setObject:credits forKey:@"credits"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,@"api/v1_3/withdraws/apply"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"apply"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"apply"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"apply"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取用户是否绑定支付宝
- (void)getRBAccountZhiFuBao {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,@"api/v1_3/kols/alipay"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"alipay"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"alipay"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"alipay"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"alipay"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-账单历史列表
- (void)getRBAccountHistoryWithPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_3/transactions"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"transactions"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"transactions"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"transactions"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}


// RB-引擎
// RB-引擎-文章
- (void)getRBRankingListWithPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_5/hot_items"]
                              params:dic
                              httpMethod:@"GET"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"hot_items"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"hot_items"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"hot_items"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-微博
- (void)getRBRankingWeiboSearchWithTerm:(NSString*)term andOrder:(NSString*)order andCategory:(NSArray*)category andBrands:(NSArray*)brands andKeywords:(NSArray*)keywords andRelative:(NSString*)relative andPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(relative==nil){
        relative=@"0";
    }
    if(term==nil) {
        term = @"";
    }
    if(category==nil){
        category = @[];
    }
    if(brands==nil){
        brands = @[];
    }
    if(keywords==nil){
        keywords = @[];
    }
    if(order==nil){
        order = @"overall";
    }
    // overall  relative influence
    [dic setObject:order forKey:@"order_by"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    [dic setObject:@"" forKey:@"kol_id"];
    [dic setObject:relative forKey:@"min_relative"];
    [dic setObject:term forKey:@"term"];
    NSDictionary *filters = @{@"kol_category":category,@"kol_brands":brands,@"kol_product_models":@[],@"kol_keywords":keywords,@"kol_features":@[],@"kol_locations":@[],@"verify":@[],@"kol_type":@[@"biz"]};
    [dic setObject:filters forKey:@"filters"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/weibo"]
                              params:dic
                              httpMethod:@"POST"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/weibo"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/weibo"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/weibo"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-微博列表
- (void)getRBRankingWeiboListWithKolId:(NSString*)kolId andPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:kolId forKey:@"id"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/weibo/posts"]
                              params:dic
                              httpMethod:@"GET"];
    //    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/weibo/posts"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/weibo/posts"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/weibo/posts"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}

// RB-引擎-微博-详情
- (void)getRBRankingWeiboDetailWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/weibo/infos"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/weibo/infos"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/weibo/infos"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/weibo/infos"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}

// RB-引擎-雷达图-微博
- (void)getRBRankingWeiboCategoriesWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/weibo/categories"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/weibo/categories"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/weibo/categories"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/weibo/categories"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-标签云-微博
- (void)getRBRankingWeiboTagsWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/weibo/tags"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/weibo/tags"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/weibo/tags"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/weibo/tags"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}

// RB-引擎-公众号-搜索
- (void)getRBRankingWechatSearchWithBiz_code:(NSString*)biz_code  {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    // overall  relative influence
    [dic setObject:@"overall" forKey:@"order_by"];
    [dic setObject:@"0" forKey:@"from"];
    [dic setObject:@"10" forKey:@"size"];
    [dic setObject:@"" forKey:@"kol_id"];
    [dic setObject:@"" forKey:@"term"];
    NSDictionary *filters = @{@"biz_code":biz_code,@"kol_category":@[],@"kol_brands":@[],@"kol_product_models":@[],@"kol_keywords":@[],@"kol_features":@[],@"kol_locations":@[],@"verify":@[],@"kol_type":@[@"biz"]};
    [dic setObject:filters forKey:@"filters"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/wechat"]
                              params:dic
                              httpMethod:@"POST"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/wechat"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/wechat"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        NSString *message = [errorOperation.responseJSON objectForKey:@"message"];
        if (message != nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/wechat"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}

// RB-引擎-公众号-搜索
- (void)getRBRankingWechatSearchWithTerm:(NSString*)term andOrder:(NSString*)order andCategory:(NSArray*)category andBrands:(NSArray*)brands andKeywords:(NSArray*)keywords andPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(term==nil) {
        term = @"";
    }
    if(category==nil){
        category = @[];
    }
    if(brands==nil){
        brands = @[];
    }
    if(keywords==nil){
        keywords = @[];
    }
    if(order==nil){
        order = @"overall";
    }
    // overall  relative influence
    [dic setObject:order forKey:@"order_by"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    [dic setObject:@"" forKey:@"kol_id"];
    [dic setObject:term forKey:@"term"];
    NSDictionary *filters = @{@"kol_category":category,@"kol_brands":brands,@"kol_product_models":@[],@"kol_keywords":keywords,@"kol_features":@[],@"kol_locations":@[],@"verify":@[],@"kol_type":@[@"biz"]};
    [dic setObject:filters forKey:@"filters"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/wechat"]
                              params:dic
                              httpMethod:@"POST"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/wechat"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/wechat"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        NSString *message = [errorOperation.responseJSON objectForKey:@"message"];
        if (message != nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/wechat"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-公众号
- (void)getRBRankingWechatWithCategory:(NSArray*)category andKeywords:(NSArray*)keywords andTerm:(NSString*)term andRelative:(NSString*)relative andPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(term==nil) {
        [dic setObject:@"" forKey:@"term"];
    } else {
        [dic setObject:term forKey:@"term"];
    }
    if(relative!=nil) {
        [dic setObject:relative forKey:@"min_relative"];
    }
    [dic setObject:@"" forKey:@"kol_id"];
    [dic setObject:@"influence" forKey:@"order_by"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    NSDictionary *filters = @{@"kol_category":category,@"kol_brands":@[],@"kol_product_models":@[],@"kol_keywords":keywords,@"kol_features":@[],@"kol_locations":@[],@"verify":@[],@"kol_type":@[@"biz"]};
    [dic setObject:filters forKey:@"filters"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/wechat"]
                              params:dic
                              httpMethod:@"POST"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/wechat"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/wechat"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        NSString *message = [errorOperation.responseJSON objectForKey:@"message"];
        if (message != nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/wechat"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-公众号列表
- (void)getRBRankingWechatListWithKolId:(NSString*)kolId andPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:kolId forKey:@"id"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/wechat/articles"]
                              params:dic
                              httpMethod:@"GET"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/wechat/posts"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/wechat/posts"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/wechat/posts"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-公众号-详情
- (void)getRBRankingWechatDetailWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/wechat/infos"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/wechat/infos"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/wechat/infos"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/wechat/infos"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-雷达图-公众号
- (void)getRBRankingWechatCategoriesWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/wechat/categories"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/wechat/categories"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/wechat/categories"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/wechat/categories"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-标签云-公众号
- (void)getRBRankingWechatTagsWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/wechat/tags"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/wechat/tags"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/wechat/tags"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/wechat/tags"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}


// RB-引擎-知乎-搜索
- (void)getRBRankingZhihuSearchWithTerm:(NSString*)term andOrder:(NSString*)order andCategory:(NSArray*)category andBrands:(NSArray*)brands andKeywords:(NSArray*)keywords andPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(term==nil) {
        term = @"";
    }
    if(category==nil){
        category = @[];
    }
    if(brands==nil){
        brands = @[];
    }
    if(keywords==nil){
        keywords = @[];
    }
    if(order==nil){
        order = @"overall";
    }
    // overall  relative influence
    [dic setObject:order forKey:@"order_by"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    [dic setObject:@"" forKey:@"kol_id"];
    [dic setObject:term forKey:@"term"];
    NSDictionary *filters = @{@"kol_category":category,@"kol_brands":brands,@"kol_product_models":@[],@"kol_keywords":keywords,@"kol_features":@[],@"kol_locations":@[],@"verify":@[],@"kol_type":@[]};
    [dic setObject:filters forKey:@"filters"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/zhihu/kol"]
                              params:dic
                              httpMethod:@"POST"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/zhihu"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/zhihu"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        NSString *message = [errorOperation.responseJSON objectForKey:@"message"];
        if (message != nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/zhihu"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-知乎列表
- (void)getRBRankingZhihuListWithKolId:(NSString*)kolId andPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:kolId forKey:@"kol_id"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/zhihu/answers"]
                              params:dic
                              httpMethod:@"GET"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/zhihu/posts"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/zhihu/posts"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/zhihu/posts"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-知乎-详情
- (void)getRBRankingZhihuDetailWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"kol_id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/zhihu/infos"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/zhihu/infos"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/zhihu/infos"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/zhihu/infos"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-雷达图-知乎
- (void)getRBRankingZhihuCategoriesWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"kol_id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/zhihu/boards"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/zhihu/boards"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/zhihu/boards"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/zhihu/boards"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}

// RB-引擎-标签云-知乎
- (void)getRBRankingZhihuTagsWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"kol_id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/zhihu/tags"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/zhihu/tags"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/zhihu/tags"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/zhihu/tags"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}


// RB-引擎-美拍-搜索
- (void)getRBRankingMeipaiSearchWithTerm:(NSString*)term andOrder:(NSString*)order andCategory:(NSArray*)category andBrands:(NSArray*)brands andKeywords:(NSArray*)keywords andPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(term==nil) {
        term = @"";
    }
    if(category==nil){
        category = @[];
    }
    if(brands==nil){
        brands = @[];
    }
    if(keywords==nil){
        keywords = @[];
    }
    if(order==nil){
        order = @"overall";
    }
    // overall  relative influence
    [dic setObject:order forKey:@"order_by"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    [dic setObject:@"" forKey:@"kol_id"];
    [dic setObject:term forKey:@"term"];
    NSDictionary *filters = @{@"kol_category":category,@"kol_brands":brands,@"kol_product_models":@[],@"kol_keywords":keywords,@"kol_features":@[],@"kol_locations":@[],@"verify":@[],@"kol_type":@[]};
    [dic setObject:filters forKey:@"filters"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/meipai"]
                              params:dic
                              httpMethod:@"POST"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/meipai"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/meipai"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        NSString *message = [errorOperation.responseJSON objectForKey:@"message"];
        if (message != nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/meipai"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-美拍列表
- (void)getRBRankingMeipaiListWithKolId:(NSString*)kolId andPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:kolId forKey:@"id"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/meipai/videos"]
                              params:dic
                              httpMethod:@"GET"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/meipai/posts"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/meipai/posts"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/meipai/posts"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-美拍-详情
- (void)getRBRankingMeipaiDetailWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/meipai/infos"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/meipai/infos"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/meipai/infos"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/meipai/infos"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-雷达图-美拍
- (void)getRBRankingMeipaiCategoriesWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/meipai/categories"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/meipai/categories"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/meipai/categories"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/meipai/categories"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}

// RB-引擎-标签云-美拍
- (void)getRBRankingMeipaiTagsWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/meipai/tags"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/meipai/tags"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/meipai/tags"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/meipai/tags"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}

// RB-引擎-秒拍-搜索
- (void)getRBRankingMiaopaiSearchWithTerm:(NSString*)term andOrder:(NSString*)order andCategory:(NSArray*)category andBrands:(NSArray*)brands andKeywords:(NSArray*)keywords andPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(term==nil) {
        term = @"";
    }
    if(category==nil){
        category = @[];
    }
    if(brands==nil){
        brands = @[];
    }
    if(keywords==nil){
        keywords = @[];
    }
    if(order==nil){
        order = @"overall";
    }
    // overall  relative influence
    [dic setObject:order forKey:@"order_by"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    [dic setObject:@"" forKey:@"kol_id"];
    [dic setObject:term forKey:@"term"];
    NSDictionary *filters = @{@"kol_category":category,@"kol_brands":brands,@"kol_product_models":@[],@"kol_keywords":keywords,@"kol_features":@[],@"kol_locations":@[],@"verify":@[],@"kol_type":@[]};
    [dic setObject:filters forKey:@"filters"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/miaopai"]
                              params:dic
                              httpMethod:@"POST"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/miaopai"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/miaopai"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        NSString *message = [errorOperation.responseJSON objectForKey:@"message"];
        if (message != nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/miaopai"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}
// RB-引擎-秒拍列表
- (void)getRBRankingMiaopaiListWithKolId:(NSString*)kolId andPageIndex:(int)pageIndex andPageSize:(int)pageSize {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:kolId forKey:@"id"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageIndex*pageSize] forKey:@"from"];
    [dic setObject:[NSString stringWithFormat:@"%d",pageSize] forKey:@"size"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/miaopai/videos"]
                              params:dic
                              httpMethod:@"GET"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/miaopai/posts"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/miaopai/posts"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/miaopai/posts"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}

// RB-引擎-秒拍-详情
- (void)getRBRankingMiaopaiDetailWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/miaopai/infos"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/miaopai/infos"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/miaopai/infos"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/miaopai/infos"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}

// RB-引擎-雷达图-秒拍
- (void)getRBRankingMiaopaiCategoriesWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/miaopai/categories"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/miaopai/categories"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/miaopai/categories"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/miaopai/categories"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}

// RB-引擎-标签云-秒拍
- (void)getRBRankingMiaopaiTagsWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",SearchEngineUrl,@"search/v1.1/miaopai/tags"]
                              params:dic
                              httpMethod:@"GET"];
    [op setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSString *message = [completedOperation.responseJSON objectForKey:@"message"];
        if (message == nil) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"search/ranking/miaopai/tags"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"search/ranking/miaopai/tags"];
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
        if ([errorOperation.responseJSON objectForKey:@"message"]!=nil) {
            [self.delegate handlerErrorJson:errorOperation.responseJSON
                                        Tag:@"search/ranking/miaopai/tags"];
        } else {
            [self.delegate handlerError:error Tag:@""];
        }
    }];
    [self.engine enqueueOperation:op];
}





// RB-奖励模块
// RB-奖励-签到
- (void)getRBUserRewardCheckIn {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/tasks/check_in"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"check_in"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"check_in"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"check_in"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-新奖励-签到
- (void)getRBUserRewardNewCheckIn{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/check_tasks/check_in"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"check_in"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"check_in"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"check_in"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-奖励-签到历史
- (void)getRBUserRewardCheckInHistory {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/tasks/check_in_history"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"check_in_history"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"check_in_history"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"check_in_history"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-新签到-签到历史
- (void)getRBUserRewardCheckInNewHistory{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/check_tasks/check_in_history"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"check_in_history"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"check_in_history"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"check_in_history"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
//七日签到历史
- (void)getRBWeekSignHistory{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v2_0/tasks"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"v2_0/tasks"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"v2_0/tasks"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"v2_0/tasks"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-奖励-完善个人资料
- (void)getRBUserRewardCompleteInfo {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/tasks/complete_info"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"complete_info"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"complete_info"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"complete_info"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-奖励-获取邀请好友信息
- (void)getRBUserRewardInviteCode {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/tasks/invite_info"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"invite_info"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"invite_info"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"invite_info"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-徒弟总收益
- (void)getRBTuDiAllReward:(NSString*)page{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v2_0/kols/percentage_on_friend?page=%@",self.postUrl,page]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"percentage_on_friend"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"percentage_on_friend"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"percentage_on_friend"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-今日收徒数
- (void)getRBTudiToday:(NSString*)page{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v2_0/kols/today_friends?page=%@",self.postUrl,page]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"today_friends"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"today_friends"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"today_friends"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-分析模块
// RB-分析-是否开启微博分析
- (void)getRBAnalysisWeiboEnable {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_3/system/config"]
                              params:dic
                              httpMethod:@"GET"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"system/config"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"system/config"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    NSNumber *status = [errorOperation.responseJSON objectForKey:@"error"];
                    if ([status intValue] != 0) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"system/config"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-绑定列表
- (void)getRBAnalysisList {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/analysis_identities/list"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"analysis_identities/list"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"analysis_identities/list"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"analysis_identities/list"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-绑定微博
- (void)getRBAnalysisWeiboBundingWithProvider:(NSString*)provider andUid:(NSString*)uid andToken:(NSString*)token andRefresh_token:(NSString*)refresh_token andName:(NSString*)name andAvatar_url:(NSString*)avatar_url andLocation:(NSString*)location andGender:(NSString*)gender andSerial_params:(NSString*)serial_params andBind_type:(NSString*)bind_type {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:bind_type forKey:@"bind_type"];
    [dic setValue:provider forKey:@"provider"];
    [dic setValue:uid forKey:@"uid"];
    [dic setValue:token forKey:@"access_token"];
    [dic setValue:refresh_token forKey:@"refresh_token"];
    [dic setValue:name forKey:@"name"];
    [dic setValue:avatar_url forKey:@"avatar_url"];
    [dic setValue:location forKey:@"location"];
    [dic setValue:gender forKey:@"gender"];
    [dic setValue:serial_params forKey:@"serial_params"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/analysis_identities/identity_bind"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"analysis_identities/identity_bind"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"analysis_identities/identity_bind"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"analysis_identities/identity_bind"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-解除绑定微博
- (void)getRBAnalysisWeiboUnBundingWithUid:(NSString*)uid andLogin_id:(NSString*)login_id {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(uid!=nil){
        [dic setValue:uid forKey:@"identity_id"];
    }
    if(login_id!=nil){
        [dic setValue:login_id forKey:@"login_id"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/analysis_identities/identity_unbind"]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"analysis_identities/identity_unbind"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"analysis_identities/identity_unbind"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"analysis_identities/identity_unbind"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-分析-微博详情
- (void)getRBAnalysisWeiboDetailWithUid:(NSString*)uid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:uid forKey:@"identity_id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/weibo_report/primary"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"weibo_report/primary"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"weibo_report/primary"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"weibo_report/primary"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-分析-微博详情-粉丝-新增
- (void)getRBAnalysisWeiboDetailFollowerNewWithUid:(NSString*)uid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:uid forKey:@"identity_id"];
    [dic setValue:@"30" forKey:@"duration"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/weibo_report/follower_follow"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"weibo_report/follower_follow"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"weibo_report/follower_follow"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"weibo_report/follower_follow"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-微博详情-粉丝-认证
- (void)getRBAnalysisWeiboDetailFollowerVerifyWithUid:(NSString*)uid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:uid forKey:@"identity_id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/weibo_report/follower_verified"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"weibo_report/follower_verified"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"weibo_report/follower_verified"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"weibo_report/follower_verified"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-微博详情-关注-认证
- (void)getRBAnalysisWeiboDetailFriendVerifyWithUid:(NSString*)uid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:uid forKey:@"identity_id"];
    [dic setValue:@"30" forKey:@"duration"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/weibo_report/friend_verified"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"weibo_report/friend_verified"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"weibo_report/friend_verified"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"weibo_report/friend_verified"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-微博详情-粉丝-地域
- (void)getRBAnalysisWeiboDetailFriendProfileWithUid:(NSString*)uid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:uid forKey:@"identity_id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/weibo_report/follower_profile"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"weibo_report/follower_profile"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"weibo_report/follower_profile"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"weibo_report/follower_profile"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-微博详情-列表
- (void)getRBAnalysisWeiboDetailListWithUid:(NSString*)uid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:uid forKey:@"identity_id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/weibo_report/statuses"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"weibo_report/statuses"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"weibo_report/statuses"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"weibo_report/statuses"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-绑定公众号
- (void)getRBAnalysisWechatBundingWithIdentity_id:(NSString*)identity_id {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(identity_id!=nil&&identity_id.length!=0){
        [dic setValue:identity_id forKey:@"identity_id"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/public_login/login_with_account"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"public_login/login_with_account"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"public_login/login_with_account"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"public_login/login_with_account"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-绑定公众号
- (void)getRBAnalysisWechatBundingWithName:(NSString*)username andPassword:(NSString*)password andImgcode:(NSString*)imgcode andCookies:(NSString*)cookies  {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    username = [username stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    password = [password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    imgcode = [imgcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [dic setValue:password forKey:@"password"];
    [dic setValue:username forKey:@"username"];
    if(imgcode!=nil&&imgcode.length!=0){
        [dic setValue:imgcode forKey:@"imgcode"];
    }
    if(cookies!=nil&&cookies.length!=0){
        [dic setValue:cookies forKey:@"cookies"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/public_login/login_with_account"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"public_login/login_with_account"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"public_login/login_with_account"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"public_login/login_with_account"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-绑定公众号-二维码扫描登录
- (void)getRBAnalysisWechatBundingWithUid:(NSString*)login_id {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:login_id forKey:@"login_id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/public_login/check_status"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"public_login/check_status"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"public_login/check_status"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"public_login/check_status"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-分析-公众号详情
- (void)getRBAnalysisWechatDetailWithLogin_id:(NSString*)login_id andIdentity_id:(NSString*)identity_id {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (login_id!=nil) {
        [dic setValue:login_id forKey:@"login_id"];
    }
    if (identity_id!=nil) {
        [dic setValue:identity_id forKey:@"identity_id"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/weixin_report/primary"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"weixin_report/primary"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"weixin_report/primary"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"weixin_report/primary"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-公众号详情-图文
- (void)getRBAnalysisWechatDetailMessagesWithLogin_id:(NSString*)login_id andIdentity_id:(NSString*)identity_id {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (login_id!=nil) {
        [dic setValue:login_id forKey:@"login_id"];
    }
    if (identity_id!=nil) {
        [dic setValue:identity_id forKey:@"identity_id"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/weixin_report/messages"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"weixin_report/messages"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"weixin_report/messages"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"weixin_report/messages"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-公众号详情-文章
- (void)getRBAnalysisWechatDetailArticlesWithLogin_id:(NSString*)login_id andIdentity_id:(NSString*)identity_id {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (login_id!=nil) {
        [dic setValue:login_id forKey:@"login_id"];
    }
    if (identity_id!=nil) {
        [dic setValue:identity_id forKey:@"identity_id"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/weixin_report/articles"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"weixin_report/articles"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"weixin_report/articles"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"weixin_report/articles"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-分析-公众号详情-粉丝
- (void)getRBAnalysisWechatDetailFollowerWithLogin_id:(NSString*)login_id andIdentity_id:(NSString*)identity_id {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (login_id!=nil) {
        [dic setValue:login_id forKey:@"login_id"];
    }
    if (identity_id!=nil) {
        [dic setValue:identity_id forKey:@"identity_id"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/weixin_report/user_analysises"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"weixin_report/user_analysises"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"weixin_report/user_analysises"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"weixin_report/user_analysises"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-微信公众号-验证码图片
- (void)getRBAnalysisWechatLoginCode:(NSString*)picUrl and:(UIImageView*)urlIV {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@",picUrl]
                              params:dic
                              httpMethod:@"GET"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation.responseData);
        urlIV.image = [UIImage imageWithData:completedOperation.responseData];
        // cookies
        for (NSMutableDictionary *cookieDic in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
            if ([[NSString stringWithFormat:@"%@",[cookieDic valueForKey:NSHTTPCookieName]] isEqualToString:@"sig"]) {
                NSLog(@"%@=%@",[cookieDic valueForKey:NSHTTPCookieName],[cookieDic valueForKey:NSHTTPCookieValue]);
                [LocalService setRBLocalDataUserCookieStringWith:[NSString stringWithFormat:@" %@=%@",[cookieDic valueForKey:NSHTTPCookieName],[cookieDic valueForKey:NSHTTPCookieValue]]];
            }
        }
    } errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
        NSLog(@"errorOperation:\n%@", errorOperation);
    }];
    [self.engine enqueueOperation:op];
}





// RB-一元夺宝
// RB-夺宝-列表
- (void)getRBIndianaListWithPage:(int)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[NSString stringWithFormat:@"%d",page] forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@",self.postUrl,@"api/v1_3/lottery_activities"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"lottery_activities"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"lottery_activities"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"lottery_activities"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-夺宝-详细
- (void)getRBIndianaDetailWithCode:(NSString*)code {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_3/lottery_activities/%@",self.postUrl,code]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"lottery_activities/code"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"lottery_activities/code"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"lottery_activities/code"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-夺宝-详细-购买人
- (void)getRBIndianaDetailListWithCode:(NSString*)code andPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_3/lottery_activities/%@/orders",self.postUrl,code]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"lottery_activities/code/orders"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"lottery_activities/code/orders"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"lottery_activities/code/orders"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-夺宝-详细-图文
- (void)getRBIndianaDetailInfoWithCode:(NSString*)code {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_3/lottery_activities/%@/desc",self.postUrl,code]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"lottery_activities/code/desc"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"lottery_activities/code/desc"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"lottery_activities/code/desc"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-夺宝-详细-计算
- (void)getRBIndianaDetailCalculationWithCode:(NSString*)code {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_3/lottery_activities/%@/formula",self.postUrl,code]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"lottery_activities/code/formula"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"lottery_activities/code/formula"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"lottery_activities/code/formula"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-夺宝-详细-购买
- (void)getRBIndianaDetailBugWithCode:(NSString*)code andNum:(NSString*)num{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:code forKey:@"activity_code"];
    [dic setObject:num forKey:@"num"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_3/lottery_orders",self.postUrl]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"lottery_orders"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"lottery_orders"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"lottery_orders"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-夺宝-详细-支付
- (void)getRBIndianaDetailBugPayWithCode:(NSString*)code {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:code forKey:@"code"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_3/lottery_orders/%@/checkout",self.postUrl,code]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"lottery_orders/code/checkout"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"lottery_orders/code/checkout"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"lottery_orders/code/checkout"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-夺宝-我参与的
- (void)getRBIndianaMyWithStatus:(NSString*)status andPage:(int)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    //过滤夺宝活动状态：all 全部,executing 执行中的,finished 已完成（开奖）的,win 我中奖的
    [dic setObject:status forKey:@"status"];
    [dic setObject:[NSString stringWithFormat:@"%d",page] forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_3/my/lottery_activities",self.postUrl]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"my/lottery_activities"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"my/lottery_activities"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"my/lottery_activities"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-夺宝-我参与的-详情
- (void)getRBIndianaMyDetailWithCode:(NSString*)code {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:code forKey:@"code"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_3/my/lottery_activities/%@",self.postUrl,code]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"my/lottery_activities/code"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"my/lottery_activities/code"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"my/lottery_activities/code"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-夺宝-获取收货地址
- (void)getRBIndianaMyAddress {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_3/my/address",self.postUrl]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"my/address"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"my/address"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"my/address"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-夺宝-获取收货地址-填写
- (void)getRBIndianaMyAddressEditWithName:(NSString*)name andPhone:(NSString*)phone andLocation:(NSString*)location {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:name forKey:@"name"];
    [dic setObject:phone forKey:@"phone"];
    [dic setObject:location forKey:@"location"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_3/my/address",self.postUrl]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"my/address/put"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"my/address/put"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"my/address/put"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}


// RB-KOL
// RB-KOL-列表
- (void)getRBKOLListWithPage:(int)page andBanner:(NSString*)banner andCategory:(NSString*)category andSearch:(NSString*)search andOrder:(NSString*)order {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(banner!=nil) {
        [dic setObject:banner forKey:@"with_kol_announcement"];
    }
    if(category!=nil) {
        [dic setObject:category forKey:@"tag_name"];
    }
    if(search!=nil) {
        [dic setObject:search forKey:@"name"];
    }
    //'order_by_created','order_by_hot'
    if(order!=nil) {
        [dic setObject:order forKey:@"order"];
    }
    [dic setObject:[NSString stringWithFormat:@"%d",page] forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/big_v",self.postUrl]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:[NSString stringWithFormat:@"big_v/%@",order]];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:[NSString stringWithFormat:@"big_v/%@",order]];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:[NSString stringWithFormat:@"big_v/%@",order]];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB——KOL-社交账号解除绑定
- (void)relieveRBKOLAccount:(NSString*)kol_id andProvider:(NSString*)provider andId:(NSString*)socialId{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    [dic setObject:kol_id forKey:@"kol_id"];
    [dic setObject:provider forKey:@"provider"];
    [dic setObject:socialId forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/big_v/unbind_social_account",self.postUrl]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"unbind_social_account"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"unbind_social_account"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"unbind_social_account"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
    
}
// RB——KOL-查询绑定次数
- (void)seeaRBKOLrelieveTimes:(NSString*)kol_id AndProvider:(NSString*)provider{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    [dic setObject:kol_id forKey:@"kol_id"];
    [dic setObject:provider forKey:@"provider"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/kols/bind_count",self.postUrl]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"bind_count"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"bind_count"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"bind_count"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
-(void)searchRBKOLClearTimes:(NSString*)kol_id AndProvider:(NSString*)provider{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    [dic setObject:kol_id forKey:@"kol_id"];
    [dic setObject:provider forKey:@"provider"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1/kols/unbind_count",self.postUrl]
                              params:dic
                              httpMethod:@"PUT"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"unbind_count"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"unbind_count"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"unbind_count"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];

}
// RB-KOL-详情
- (void)getRBKOLDetailWithKolId:(NSString*)kolId {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:kolId forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/big_v/%@/detail",self.postUrl,kolId]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"big_v/detail"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"big_v/detail"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"big_v/detail"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-KOL-关注
- (void)getRBKOLCareWithKolId:(NSString*)kolId {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:kolId forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/big_v/%@/follow",self.postUrl,kolId]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"big_v/follow"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"big_v/follow"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"big_v/follow"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}


// RB-KOL申请-第一步
- (void)getRBKOLApplyWithAvatar:(UIImage*)avatar andName:(NSString*)name andAge:(NSString*)age andGender:(NSString*)gender andApp_city:(NSString*)app_city andJob:(NSString*)job_info andTags:(NSString*)tag_names andDesc:(NSString*)desc {
    //gender 0-未知 1-男 2-女
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:name forKey:@"name"];
    if([gender isEqualToString:NSLocalizedString(@"R5068",@"男")]) {
        gender = @"1";
    }
    if([gender isEqualToString:NSLocalizedString(@"R5069",@"女")]) {
        gender = @"2";
    }
    [dic setObject:age forKey:@"age"];
    [dic setObject:gender forKey:@"gender"];
    [dic setObject:app_city forKey:@"app_city"];
    [dic setObject:job_info forKey:@"job_info"];
    [dic setObject:tag_names forKey:@"tag_names"];
    [dic setObject:desc forKey:@"desc"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/big_v_applies/update_profile",self.postUrl]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addData:UIImageJPEGRepresentation(avatar, 1.0f)
         forKey:@"avatar"
       mimeType:@"image/jpeg"
       fileName:@"avatar.jpg"];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"update_profile"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"update_profile"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"update_profile"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-KOL申请-第二步V2
- (void)getV2RBKOLApplyWithProvider:(NSString*)provider andHomepage:(NSString*)homepage andUsername:(NSString*)username andPrice:(NSString*)price andFollowers:(NSString*)followers_count andScreenshot:(UIImage*)screenshot andUid:(NSString*)uid{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:provider forKey:@"provider_name"];
    [dic setObject:price forKey:@"price"];
    if(username!=nil) {
        [dic setObject:username forKey:@"username"];
    }
    if(homepage!=nil) {
        [dic setObject:homepage forKey:@"homepage"];
    }
    if(followers_count!=nil) {
        [dic setObject:followers_count forKey:@"followers_count"];
    }
    if(uid!=nil) {
        [dic setObject:uid forKey:@"uid"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/big_v_applies/update_social_v2",self.postUrl]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    if(screenshot!=nil) {
        [op addData:UIImageJPEGRepresentation(screenshot, 1.0f)
             forKey:@"screenshot"
           mimeType:@"image/jpeg"
           fileName:@"screenshot.jpg"];
    }
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"update_social_v2"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"update_social_v2"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"update_social_v2"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];

}
// RB-KOL申请-第二步
- (void)getRBKOLApplyWithProvider:(NSString*)provider andHomepage:(NSString*)homepage andUsername:(NSString*)username andPrice:(NSString*)price andFollowers:(NSString*)followers_count andScreenshot:(UIImage*)screenshot andUid:(NSString*)uid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:provider forKey:@"provider_name"];
    [dic setObject:price forKey:@"price"];
    if(username!=nil) {
        [dic setObject:username forKey:@"username"];
    }
    if(homepage!=nil) {
        [dic setObject:homepage forKey:@"homepage"];
    }
    if(followers_count!=nil) {
        [dic setObject:followers_count forKey:@"followers_count"];
    }
    if(uid!=nil) {
        [dic setObject:uid forKey:@"uid"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/big_v_applies/update_social",self.postUrl]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    if(screenshot!=nil) {
        [op addData:UIImageJPEGRepresentation(screenshot, 1.0f)
             forKey:@"screenshot"
           mimeType:@"image/jpeg"
           fileName:@"screenshot.jpg"];
    }
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"update_social"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"update_social"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"update_social"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-KOL申请-第三步
- (void)getRBKOLApplyWithShows:(NSString*)kol_shows {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(kol_shows!=nil) {
        [dic setObject:kol_shows forKey:@"kol_shows"];
    }
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/big_v_applies/submit_apply",self.postUrl]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"submit_apply"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"submit_apply"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"submit_apply"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-KOL-关注列表
- (void)getRBKOLFollowersListWithPage:(int)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[NSString stringWithFormat:@"%d",page] forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_6/my/friends",self.postUrl]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"friends"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"friends"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"friends"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-CPS模块
// RB-获取商品分类
- (void)getRBCpsProductCategoryWithPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_7/cps_materials/categories"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"categories"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"categories"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"categories"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取商品列表
- (void)getRBCpsProductListWithGoods:(NSString*)goods andCategory:(NSString*)category andOrder:(NSString*)order andPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if(goods!=nil) {
    [dic setObject:goods forKey:@"goods_name"];
    }
    if(category!=nil) {
    [dic setObject:category forKey:@"category_name"];
    }
        if(order!=nil) {
    [dic setObject:order forKey:@"order_by"];
        }
    [dic setObject:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_7/cps_materials"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"cps_materials"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"cps_materials"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"cps_materials"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取图片上传
- (void)getRBCpsUploadImg:(UIImage*)img {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_7/images/upload"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addData:UIImageJPEGRepresentation(img, 1.0f)
         forKey:@"file"
       mimeType:@"image/jpeg"
       fileName:@"file.jpg"];
//    [op onUploadProgressChanged:^(double progress) {
//        if(progress >= 1){
//            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R5174",@"头像上传成功")];
//        } else {
//            [SVProgressHUD showProgress:progress status:NSLocalizedString(@"R5175",@"头像上传中...")];
//        }
//    }];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"upload"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"upload"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"upload"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-获取创作详情
- (void)getRBCpsCreateDetailWithID:(NSString*)uid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:uid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_7/cps_articles/%@/show", self.postUrl,uid]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"show"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"show"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"show"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-获取创作详情商品列表
- (void)getRBCpsCreateDetailProductListWithID:(NSString*)uid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:uid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_7/cps_articles/%@/materials", self.postUrl,uid]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"materials"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"materials"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"materials"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-获取创作列表
- (void)getRBCpsCreateListWithPage:(NSString*)page {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:page forKey:@"page"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_7/cps_articles"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"cps_articles"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"cps_articles"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"cps_articles"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}
// RB-获取我的创作列表
- (void)getRBCpsMyCreateListWithPage:(NSString*)page andStatus:(NSString*)status {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:page forKey:@"page"];
    //文章状态 'pending' , 'passed','rejected' 分别代表待审核、审核通过、审核拒绝
    [dic setObject:status forKey:@"status"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_7/cps_articles/my_articles"]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"cps_articles/my"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"cps_articles/my"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"cps_articles/my"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-获取我的创作提交
- (void)getRBCpsCreateSubmitWithContent:(NSString*)content andTitle:(NSString*)title andCover:(NSString*)cover {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:content forKey:@"content"];
    [dic setObject:title forKey:@"title"];
    [dic setObject:cover forKey:@"cover"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_7/cps_articles/create"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"cps_articles/create"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"cps_articles/create"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"cps_articles/create"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-获取我的创作分享
- (void)getRBCpsCreateShareWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"cps_article_id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@%@", self.postUrl,
                                                      @"api/v1_7/cps_articles/share_article"]
                              params:dic
                              httpMethod:@"POST"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"cps_articles/share_article"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"cps_articles/share_article"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"cps_articles/share_article"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}

// RB-获取我的创作商品列表
- (void)getRBCpsCreateProductWithId:(NSString*)iid {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:iid forKey:@"id"];
    MKNetworkOperation *op = [self.engine
                              operationWithURLString:[NSString stringWithFormat:@"%@api/v1_7/cps_articles/%@/materials", self.postUrl,iid]
                              params:dic
                              httpMethod:@"GET"];
    [op setHeader:@"Authorization" withValue:[self getRBSecret]];
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSLog(@"completedOperation:\n%@", completedOperation);
        NSNumber *status = [completedOperation.responseJSON objectForKey:@"error"];
        if ([status intValue] == 0) {
            [self.delegate handlerSuccess:completedOperation.responseJSON
                                      Tag:@"cps_articles/materials"];
        } else {
            [self.delegate handlerErrorJson:completedOperation.responseJSON
                                        Tag:@"cps_articles/materials"];
        }
    }
                errorHandler:^(MKNetworkOperation *errorOperation, NSError *error) {
                    NSLog(@"errorOperation:\n%@", errorOperation);
                    if (error.code==401) {
                        [self getRBBackToLogin];
                        return;
                    }
                    if ([errorOperation.responseJSON objectForKey:@"error"]!=nil) {
                        [self.delegate handlerErrorJson:errorOperation.responseJSON
                                                    Tag:@"cps_articles/materials"];
                    } else {
                        [self.delegate handlerError:error Tag:@""];
                    }
                }];
    [self.engine enqueueOperation:op];
}


@end
