//
//  LocalService.m

//
//  Created by AngusNi on 15/6/10.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "LocalService.h"
#import "JWT.h"
@implementation LocalService
// 本地数据管理
// UserIndiana
// RB-保存用户UserIndiana
+(void)setRBLocalDataUserIndianaWith:(NSString*)str {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"UserIndiana"];
    [ud synchronize];
}
// RB-读取用户UserIndiana
+(NSString*)getRBLocalDataUserIndiana {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserIndiana"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserIndiana"]];
}
//RB-是否隐藏我的钱包
+(void)setRBLocalDataUserHideWith:(NSString*)str{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"hide"];
    [ud synchronize];
}
+(NSString*)getRBLocalDataUserHide{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"hide"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"hide"]];
}
//RB-保存绑定微信的UID
+ (void)setRBLocalWechatUID:(NSString*)str{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"UID"];
    [ud synchronize];
}
+ (NSString*)getRBLocalWechatUID{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UID"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UID"]];
}
// UserAgree
// RB-保存用户UserAgree
+(void)setRBLocalDataUserAgreeWith:(NSString*)str {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"UserAgree"];
    [ud synchronize];
}
// RB-读取用户UserAgree
+(NSString*)getRBLocalDataUserAgree {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserAgree"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserAgree"]];
}
// UserLoginId
// RB-保存用户UserLoginId
+(void)setRBLocalDataUserLoginIdWith:(NSString*)str {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"UserLoginId"];
    [ud synchronize];
}
// RB-读取用户UserLoginId
+(NSString*)getRBLocalDataUserLoginId {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserLoginId"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserLoginId"]];
}
// UserLoginName
// RB-保存用户UserLoginName
+(void)setRBLocalDataUserLoginNameWith:(NSString*)str {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"UserLoginName"];
    [ud synchronize];
}
// RB-读取用户UserLoginName
+(NSString*)getRBLocalDataUserLoginName {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserLoginName"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserLoginName"]];
}
// UserLoginAvatar
// RB-保存用户UserLoginAvatar
+(void)setRBLocalDataUserLoginAvatarWith:(NSString*)str {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"UserLoginAvatar"];
    [ud synchronize];
}
// RB-读取用户UserLoginAvatar
+(NSString*)getRBLocalDataUserLoginAvatar {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserLoginAvatar"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserLoginAvatar"]];
}
// UserLoginPhone
// RB-保存用户UserLoginPhone
+(void)setRBLocalDataUserLoginPhoneWith:(NSString*)str {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"UserLoginPhone"];
    [ud synchronize];
}
//RB-保存开发者设置的域名
+ (void)setRBDevelopIPURL: (NSString*)str{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"develop"];
    [ud synchronize];
}
//RB-获取开发者设置的域名
+ (NSString*)getRBDevelopIPURL{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"develop"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"develop"]];
}

// RB-读取用户UserLoginPhone
+(NSString*)getRBLocalDataUserLoginPhone {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserLoginPhone"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserLoginPhone"]];
}
// UserLoginKolId
// RB-保存用户UserLoginKolId
+(void)setRBLocalDataUserLoginKolIdWith:(NSString*)str {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"UserLoginKolId"];
    [ud synchronize];
}
// RB-保存用户Email
+(void)setRBLocalEmail:(NSString*)str{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"UserEmail"];
    [ud synchronize];
}
// RB-记录是否是新用户
+(void)setRBIsNewMember:(NSString*)str{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:str forKey:@"isNewMember"];
    [ud synchronize];
}
// RB-获取是否是新用户
+(BOOL)getRBIsNewMember{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    if ([[ud objectForKey:@"isNewMember"] isEqualToString:@"1"]) {
        return YES;
    }
    return NO;
}
// RB-读取用户Email
+(NSString*)getRBLocalEmail{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserEmail"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserEmail"]];
}
// RB-读取用户UserLoginKolId
+(NSString*)getRBLocalDataUserLoginKolId {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserLoginKolId"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserLoginKolId"]];
}
// RB-读取记录是否是在审核期间
+(BOOL)getRBIsIncheck{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"IsIncheck"] == nil) {
        return  NO;
    }
    return YES;
}
// RB-记录是否在审核期间
+(void)setRBIsInCheck:(NSString*)isInCheck{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:isInCheck forKey:@"IsIncheck"];
    [ud synchronize];
}


// UserPublicName
// RB-保存用户UserPublicName
+(void)setRBLocalDataUserPublicNameWith:(NSString*)publicName {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:publicName forKey:@"UserPublicName"];
    [ud synchronize];
}

// RB-读取用户UserPublicName
+(NSString*)getRBLocalDataUserPublicName {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserPublicName"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserPublicName"]];
}

// UserPublicPwd
// RB-保存用户UserPublicPwd
+(void)setRBLocalDataUserPublicPwdWith:(NSString*)publicPwd {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:publicPwd forKey:@"UserPublicPwd"];
    [ud synchronize];
}

// RB-读取用户UserPublicPwd
+(NSString*)getRBLocalDataUserPublicPwd {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserPublicPwd"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserPublicPwd"]];
}

// UserVersionShow
// RB-保存用户UserVersionShow
+(void)setRBLocalDataUserVersionShowWith:(NSString*)versionShow {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:versionShow forKey:@"UserVersionShow"];
    [ud synchronize];
}

// RB-读取用户UserVersionShow
+(NSString*)getRBLocalDataUserVersionShow {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserVersionShow"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserVersionShow"]];
}


// RB
// UserWechatPublicToken
// RB-保存用户UserWechatPublicToken
+(void)setRBLocalDataUserWechatPublicTokenWith:(NSString*)userWechatPublicToken {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:userWechatPublicToken forKey:@"UserWechatPublicToken"];
    [ud synchronize];
}

// RB-读取用户UserWechatPublicToken
+(NSString*)getRBLocalDataUserWechatPublicToken {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserWechatPublicToken"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserWechatPublicToken"]];
}


// UserCookieString
// RB-保存用户UserCookieString
+(void)setRBLocalDataUserCookieStringWith:(NSString*)userCookie {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:userCookie forKey:@"UserCookieString"];
    [ud synchronize];
}

// RB-读取用户UserCookieString
+(NSString*)getRBLocalDataUserCookieString {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserCookieString"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserCookieString"]];
}


// UserCookie
// RB-保存用户UserCookie
+(void)setRBLocalDataUserCookieWith:(NSData*)userCookie {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:userCookie forKey:@"UserCookie"];
    [ud synchronize];
}

// RB-读取用户UserCookie
+(NSData*)getRBLocalDataUserCookie {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserCookie"] == nil) {
        return nil;
    }
    return [ud objectForKey:@"UserCookie"];
}

// UserKolSearchList
// RB-保存用户UserKolSearchList
+(void)setRBLocalDataUserKolSearchListWith:(NSString*)searchList {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:searchList forKey:@"UserKolSearchList"];
    [ud synchronize];
}

// RB-读取用户UserKolSearchList
+(NSString*)getRBLocalDataUserKolSearchList {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserKolSearchList"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserKolSearchList"]];
}

// UserSearchList
// RB-保存用户UserSearchList
+(void)setRBLocalDataUserSearchListWith:(NSString*)searchList {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:searchList forKey:@"UserSearchList"];
    [ud synchronize];
}

// RB-读取用户UserSearchList
+(NSString*)getRBLocalDataUserSearchList {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserSearchList"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserSearchList"]];
}

// UserEngineSearchList
// RB-保存用户UserEngineSearchList
+(void)setRBLocalDataUserEngineSearchListWith:(NSString*)searchList {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:searchList forKey:@"UserEngineSearchList"];
    [ud synchronize];
}

// RB-读取用户UserEngineSearchList
+(NSString*)getRBLocalDataUserEngineSearchList {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserEngineSearchList"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserEngineSearchList"]];
}

// UserRankingSearchList
// RB-保存用户UserRankingSearchList
+(void)setRBLocalDataUserRankingSearchListWith:(NSString*)searchList {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:searchList forKey:@"UserRankingSearchList"];
    [ud synchronize];
}

// RB-读取用户UserRankingSearchList
+(NSString*)getRBLocalDataUserRankingSearchList {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserRankingSearchList"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserRankingSearchList"]];
}


// UserRongToken
// RB-保存用户UserRongToken
+(void)setRBLocalDataUserRongTokenWith:(NSString*)rongToken {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:rongToken forKey:@"UserRongToken"];
    [ud synchronize];
}

// RB-读取用户UserRongToken
+(NSString*)getRBLocalDataUserRongToken {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserRongToken"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserRongToken"]];
}


// UserShake
// RB-保存用户UserShake
+(void)setRBLocalDataUserShakeWith:(NSString*)shake {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:shake forKey:@"UserShake"];
    [ud synchronize];
}

// RB-读取用户UserShake
+(NSString*)getRBLocalDataUserShake {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserShake"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserShake"]];
}


// UserPush
// RB-保存用户UserPush
+(void)setRBLocalDataUserPushWith:(NSString*)push {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:push forKey:@"UserPush"];
    [ud synchronize];
}

// RB-读取用户UserPush
+(NSString*)getRBLocalDataUserPush {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserPush"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserPush"]];
}

// UserUpateDate
// RB-保存用户UserUpateDate
+(void)setRBLocalDataUserUpateDateWith:(NSString*)upateDate {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:upateDate forKey:@"UserUpateDate"];
    [ud synchronize];
}

// RB-读取用户UserUpateDate
+(NSString*)getRBLocalDataUserUpateDate {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserUpateDate"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserUpateDate"]];
}

// UserCity
// RB-保存用户UserCity
+(void)setRBLocalDataUserCityWith:(NSString*)city andName:(NSString*)cityname {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:city forKey:@"UserCity"];
    [ud setObject:cityname forKey:@"UserCityName"];
    [ud synchronize];
}

// RB-读取用户UserCity
+(NSString*)getRBLocalDataUserCity {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserCity"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserCity"]];
}

// RB-读取用户UserCityName
+(NSString*)getRBLocalDataUserCityName {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserCityName"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserCityName"]];
}

// UserKolCity
// RB-保存用户UserKolCity
+(void)setRBLocalDataUserKolCityWith:(NSString*)city {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:city forKey:@"UserKolCity"];
    [ud synchronize];
}

// RB-读取用户UserKolCity
+(NSString*)getRBLocalDataUserKolCity {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserKolCity"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserKolCity"]];
}

// UserGuide
// RB-保存用户UserGuide
+(void)setRBLocalDataUserGuideWith:(NSString*)userGuide {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:userGuide forKey:@"UserGuide"];
    [ud synchronize];
}

// RB-读取用户UserGuide
+(NSString*)getRBLocalDataUserGuide {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserGuide"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserGuide"]];
}

// UserKOL
// RB-保存用户UserKOL
+(void)setRBLocalDataUserKolWith:(NSString*)kol_uuid {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:kol_uuid forKey:@"UserKol"];
    [ud synchronize];
}

// RB-读取用户UserKOL
+(NSString*)getRBLocalDataUserKol {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserKol"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserKol"]];
}
// RB-获取用户第一次测试影响力标识
+(NSString*)getRBLocalTestInfluence{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"firstInfluence"]==nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"firstInfluence"]];
}
// RB-保存用户第一次测试影响力标识
+(void)setRBLocalTestInfluence{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:@"yes" forKey:@"firstInfluence"];
    [ud synchronize];
}
// RB-保存用户第一次测试影响力标识
+(void)setRBFirstInfluenceenter:(NSString*)enterMark{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    if (enterMark == nil) {
        [ud setObject:nil forKey:@"firstTestInfluence"];
    }else{
        [ud setObject:enterMark forKey:@"firstTestInfluence"];
    }
    [ud synchronize];
}
// RB-获取用户第一次测试影响力标识
+(NSString*)getRBFirstInfluenceenter{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"firstTestInfluence"]==nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"firstTestInfluence"]];
}
+(void)setRBfirstEnterAPP{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:@"YES" forKey:@"RBenter"];
}
+(BOOL)getRBfirstEnterApp{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"RBenter"] == nil) {
        return NO;
    }
    return YES;
}
+(void)setRBfirstEnterMyInterface{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:@"YES" forKey:@"RBMyenter"];
}
// RB-获取用户第一次进入我的界面
+(BOOL)getRBfirstEnterMyInterface{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"RBMyenter"] == nil) {
        return NO;
    }
    return YES;
}
// RB-第一次进入活动详情界面标识
+(void)setRBfirstEnterCampain{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:@"YES" forKey:@"RBCampain"];
}
// RB-获取第一次进入活动详情界面标识
+(BOOL)getRBfirstEnterCampain{
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"RBCampain"] == nil) {
        return NO;
    }
    return YES;
}



// PrivateToken
// RB-保存用户PrivateToken
+(void)setRBLocalDataUserPrivateTokenWith:(NSString*)private_token {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (private_token==nil) {
        [ud setObject:nil forKey:@"UserPrivateToken"];
    } else {
        NSDictionary*jwtStr=[JWT decodeMessage:private_token withSecret:@"Robin888"];
        NSArray*jwtArray = [jwtStr valueForKey:@"payload"];
        NSString*token = [NSString stringWithFormat:@"%@",[jwtArray valueForKey:@"private_token"]];
        [ud setObject:token forKey:@"UserPrivateToken"];
    }
    [ud synchronize];
}

// RB-读取用户PrivateToken
+(NSString*)getRBLocalDataUserPrivateToken {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserPrivateToken"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserPrivateToken"]];
}

// IDFA
// RB-保存用户IDFA
+(void)setRBLocalDataUserIDFAWith:(NSString*)idfa {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:idfa forKey:@"UserIDFA"];
    [ud synchronize];
}

// RB-读取用户IDFA
+(NSString*)getRBLocalDataUserIDFA {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserIDFA"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserIDFA"]];
}

// DeviceToken
// RB-保存用户DeviceToken
+(void)setRBLocalDataUserDeviceTokenWith:(NSString*)deviceToken {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:deviceToken forKey:@"UserDeviceToken"];
    [ud synchronize];
}

// RB-读取用户DeviceToken
+(NSString*)getRBLocalDataUserDeviceToken {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserDeviceToken"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserDeviceToken"]];
}

// ClientId
// RB-保存用户ClientId
+(void)setRBLocalDataUserClientIdWith:(NSString*)clientId {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:clientId forKey:@"UserClientId"];
    [ud synchronize];
}

// RB-读取用户ClientId
+(NSString*)getRBLocalDataUserClientId {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserClientId"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserClientId"]];
}

// Article
// RB-保存用户Article
+(void)setRBLocalDataUserArticleWith:(NSString*)article {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:article forKey:@"UserArticle"];
    [ud synchronize];
}

// RB-读取用户Article
+(NSString*)getRBLocalDataUserArticle {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserArticle"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserArticle"]];
}


// 经纬度
// RB-保存用户经纬度
+(void)setRBLocalDataUserLatitudeWith:(NSString*)latitude andLongitude:(NSString*)longitude {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:latitude forKey:@"UserLatitude"];
    [ud setObject:longitude forKey:@"UserLongitude"];
    [ud synchronize];
}

// RB-读取用户UserLatitude
+(NSString*)getRBLocalDataUserLatitude {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserLatitude"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserLatitude"]];
}

// RB-读取用户UserLongitude
+(NSString*)getRBLocalDataUserLongitude {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"UserLongitude"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@",[ud objectForKey:@"UserLongitude"]];
}
@end
