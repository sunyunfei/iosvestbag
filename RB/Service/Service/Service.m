//
//  Service.m
//
//  Created by AngusNi on 13-3-25.
//  Copyright (c) 2013年 AngusNi. All rights reserved.
//

#import "Service.h"

@implementation Service

+(void)clearImageCache {
    [UIImageView clearCache];
    [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] cleanDisk];
    [[SDImageCache sharedImageCache] clearDisk];
    YYImageCache *cache = [YYWebImageManager sharedManager].cache;
    [cache.memoryCache removeAllObjects];
    [cache.diskCache removeAllObjects];
}

@end
