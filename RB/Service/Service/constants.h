//
//  constants.h
//
//  Created by AngusNi on 15/4/27.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#ifndef RB_constants_h
#define RB_constants_h
#define RBAPPDELEGATE                        (RBAppDelegate *)[UIApplication sharedApplication].delegate

// 单例
#define DEFINE_SINGLETON_FOR_HEADER(className) \
+ (className *)shared##className;

#define DEFINE_SINGLETON_FOR_CLASS(className) \
static className *_shared##className = nil; \
+ (className *)shared##className { \
    static dispatch_once_t onceToken; \
    dispatch_once(&onceToken, ^{ \
        _shared##className = [[self alloc] init]; \
    }); \
    return _shared##className; \
}
//calendar
#define WeakSelf(weakSelf)  __weak __typeof(self) weakSelf = self;
#define NAVH (MAX(Device_Width, Device_Height)  == 812 ? 88 : 64)
#define TABBARH 49
#define Device_Width  [[UIScreen mainScreen] bounds].size.width//获取屏幕宽高
#define Device_Height [[UIScreen mainScreen] bounds].size.height

// 默认放大倍数
#define scaleW                               1.0*[UIScreen mainScreen].bounds.size.width
#define scaleH                               1.0*[UIScreen mainScreen].bounds.size.height
// 屏幕宽度
#define ScreenWidth                          [UIScreen mainScreen].bounds.size.width
// 屏幕高度
#define ScreenHeight                         [UIScreen mainScreen].bounds.size.height
//
#define ScaleWidth                           ScreenWidth/375
//
#define ScaleHeight                          ScreenHeight/667
//
#define NavHeight                           ([[UIApplication sharedApplication] statusBarFrame].size.height+44)
#define StatusHeight                         [[UIApplication sharedApplication] statusBarFrame].size.height

// Notification
#define NotificationRefreshHomeView           @"NotificationRefreshHomeView"
#define NotificationRefreshIndianaView        @"NotificationRefreshIndianaView"
#define NotificationShowMessageView           @"NotificationShowMessageView"
#define NotificationShowLoginView             @"NotificationShowLoginView"
#define NotificationRefreshArticleView        @"NotificationRefreshArticleView"
#define NotificationRefreshUserInfo           @"NotificationRefreshUserInfo"
#define NotificationStatusBarAction           @"NotificationStatusBarAction"
#define NotificationAlipayStatus              @"NotificationAlipayStatus"
#define NotificationDismissView               @"NotificationDismissView"
#define NotificationApplyInfo                 @"NotificationApplyInfo"
#define NotificationBackToHome                @"NotificationBackHome"
#define NotificationCampainAddCoverView       @"NotificationCampaignAddCover"


// BundleIdentifier
#define AppBundleIdentifier                   [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleIdentifier"]]
// ApiUrl
#define ApiUrl                                [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"http://robin8.net/" : @"http://robin8-staging.cn/"
// AppStoreUrl
#define AppStoreUrl                           @"itms-apps://itunes.apple.com/app/id1085946339"
// SearchEngineUrl
#define SearchEngineUrl                       @"http://139.196.240.227:5000/"


#ifdef  DEBUG
#define IsDebug      YES
#else
#define IsDebug      NO
#endif

// TalkingData
#ifdef  DEBUG
#define TalkingDataAppKey      @""
#else
#define TalkingDataAppKey      @"D48E64CF539E446EBAE757F5A918563D"
#endif
// TalkingDataCPA
#ifdef  DEBUG
#define TalkingDataCpaAppKey   @""
#else
#define TalkingDataCpaAppKey   @"688f581816cf419f8bd386ea2811c3e5"
#endif
// ShareSDK
#define ShareSDKAppKey         @"23aa599adfeda"
// SinaWeiboSDK

#define SinaWeiboAppKey        [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"817193667" : @"817193667"
#define SinaWeiboAppSecret     [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"4c8b3e75f44133d276fcdd115e9a8a66" : @"4c8b3e75f44133d276fcdd115e9a8a66"
//#define SinaWeiboAppKey        [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"365492318" : @"2851445977"
//#define SinaWeiboAppSecret     [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"1c7760271404c1f015f5d34c2aacb483" : @"1f1af22a642603dec93abeea475c1526"
#define SinaWeiboAppUrl        [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"https://www.baidu.com" : @"https://www.baidu.com"
// WeChatSDK
#define WeChatAppId            [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"wx042b2470edef4ddf" : @"wx042b2470edef4ddf"
#define WeChatAppSecret        [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"a0f38c82ccdf3d28591a7e5cf516250e" : @"a0f38c82ccdf3d28591a7e5cf516250e"
// QQSDK
#define QQAppId                [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"1106679870" : @"1106679870"
#define QQAppSecret            [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"Ec9F7XGyS9RJstVv" : @"Ec9F7XGyS9RJstVv"
// GtSDK
#ifdef  DEBUG
#define GtSDKAppId             @"yls9mphYux5qfRTyqOBp63"
#define GtSDKAppKey            @"ASHH4rcPi487MQhL0swwpA"
#define GtSDKAppSecret         @"SiVHO2IduV7U0nkXfPP5y8"
#else
#define GtSDKAppId             @"IZNLTEcKP29wbXiZVI8414"
#define GtSDKAppKey            @"q6R5PiHGxN8jzs4gLAmZd1"
#define GtSDKAppSecret         @"vp1baSRTI268tOTTKb5Tv"
#endif
// RCIMSDK
#ifdef  DEBUG
#define RCIMAppKey             @"pkfcgjstfbb48"
#define RCIMServiceKey         @"obOmr2EED148E"
#define RCIMServiceID          @"KEFU145881757665160"
#else
#define RCIMAppKey             @"c9kqb3rdkii8j"
#define RCIMServiceKey         @"3Qgy92wONUMc"
#define RCIMServiceID          @"KEFU145887621139590"
#endif


// 距离
// 默认列表左边距
#define CellLeft              12.5f
// 默认列表底距
#define CellBottom            7.5f


// 颜色
// 默认White
#define SysColorWhite         @"ffffff"
// 默认Black
#define SysColorBlack         @"181616"
// 默认SubBlack
#define SysColorSubBlack      @"424141"
// 默认Green
#define SysColorGreen         @"37bd64"
// 默认Blue
#define SysColorBlue          @"2dcad0"
// 默认Yellow
#define SysColorYellow        @"ecb200"
// 默认背景黄色
#define SysBackColorYellow    @"f1bc00"
// 默认背景蓝色
#define SysBackColorBlue      @"36cdd3"
// 默认Brown
#define SysColorBrown         @"ede8dc"
// 默认Red
#define SysColorRed           @"f26d45"
//默认橘黄
#define SysColorOrange        @"2bdbe2"
//默认弹框字的颜色
#define SysColorAlert         @"5a5a5a"
//字体颜色
#define SysColortextColor     @"2a2a2a"
//背景灰颜色
#define SysColorbackgray      @"ececec"
//灰色背景字体颜色
#define SysColorgrayText      @"686868"
//白灰字体颜色
#define SysWhiteGray          @"BBBBBB"
//深灰颜色
#define SysDeepGray           @"777777"
//
#define ccColorfebc3b         @"febc3b"
//
#define ccColorf5f5f5         @"f5f5f5"
//
#define ccColor1a1a1a         @"1a1a1a"
//
#define ccColor2e2e2e         @"2e2e2e"
//
#define ccColor5a5a5a         @"5a5a5a"
//
#define ccColor212121         @"212121"
//
#define ccColordadcdd         @"dadcdd"
//
#define ccColor969696         @"969696"
//
#define ccColoracacac         @"acacac"
//
#define ccColor2bdbe2         @"2bdbe2"
//
#define ccColord7d7d7         @"d7d7d7"
//
#define ccColor545454         @"545454"
//
#define ccColora4a4a4         @"a4a4a4"
//
#define ccColore4e4e4         @"e4e4e4"
//
#define ccColorffc701         @"ffc701"
//
#define ccColorffc700         @"ffc700"
//
#define ccColor797979         @"797979"
//
#define ccColor919191         @"919191"
//
#define ccColor444444         @"444444"
//
#define ccColorf3f3f3         @"f3f3f3"
//
#define ccColorff9b0d         @"ff9b0d"
//
#define ccColorff8004         @"ff8004"
//
#define ccColor565656         @"565656"
//
#define ccColor202020         @"202020"
//
#define ccColorff9b0d         @"ff9b0d"
//
#define ccColor414141         @"414141"
//
#define ccColor111111         @"111111"
//
#define ccColorff7901         @"ff7901"
// 默认Gray
#define SysColorGray          @"5f6060"
#define SysColorGray1         @"777777"
#define SysColorGray2         @"bbbbbb"
// 默认SubGray
#define SysColorSubGray       @"929292"
// 默认LightGray
#define SysColorLightGray     @"d6dbdb"
// 默认BkgGray
#define SysColorBkgGray       @"f8f8fa"


// 默认背景
#define SysColorBkg           @"f2f4f9"
// 默认导航线
#define SysColorBarLine       @"d4d1d2"
// 默认分割线
//#define SysColorLine          @"e0e0e0"

#define SysColorLine @"e9e9e9"

// 默认覆盖-深
#define SysColorCoverDeep     [Utils getUIColorWithHexString:@"000000" andAlpha:0.6f]
// 默认覆盖-浅
#define SysColorCover         [Utils getUIColorWithHexString:@"000000" andAlpha:0.3f]


// 图片
// 默认Place图片
#define PlaceHolderImage       [UIImage imageNamed:@"icon_default_bg.png"]
// 默认Place头像
#define PlaceHolderUserImage   [UIImage imageNamed:@"icon_user_default.png"]


// 字体图标
// 点击
#define Icon_btn_click         @"\U0000e61d"
// 更多
#define Icon_btn_more          @"\U0000e620"
// 播放
#define Icon_btn_play          @"\U0000e621"
// 关注
#define Icon_btn_care_h        @"\U0000e622"
// 不关注
#define Icon_btn_care_l        @"\U0000e623"
// 选中
#define Icon_btn_select_h      @"\U0000e61e"
// 未选中
#define Icon_btn_select_l      @"\U0000e61a"
// 选中失败
#define Icon_btn_select_e      @"\U0000e61b"
// +号
#define Icon_btn_add           @"\U0000e609"
// x号
#define Icon_btn_delete        @"\U0000e618"
// 向下30%
#define Icon_btn_down          @"\U0000e600"
// 向右
#define Icon_btn_right         @"\U0000e60e"

// 底部-点赞
#define Icon_btn_like          @"\U0000e617"
// 底部-收藏
#define Icon_btn_fav           @"\U0000e604"
// 底部-分享
#define Icon_btn_share         @"\U0000e608"

// 我的-钱包
#define Icon_btn_money         @"\U0000e60b"
// 我的-广告
#define Icon_btn_ad            @"\U0000e606"
// 我的-夺宝
#define Icon_btn_indiana       @"\U0000e612"
// 我的-签到
#define Icon_btn_checkin       @"\U0000e627"
// 我的-邀请
#define Icon_btn_invite        @"\U0000e60a"
// 我的-帮助
#define Icon_btn_help          @"\U0000e607"
// 我的-活动
#define Icon_btn_campaign      @"\U0000e601"

// 导航-返回
#define Icon_bar_back          @"\U0000e603"
// 导航-搜索
#define Icon_bar_search        @"\U0000e602"
// 导航-消息
#define Icon_bar_msg           @"\U0000e625"
// 导航-已读
#define Icon_bar_read          @"\U0000e616"
// 导航-设置
#define Icon_bar_setting       @"\U0000e61f"
// 导航-分享
#define Icon_bar_share         @"\U0000e608"
// 导航-筛选
#define Icon_bar_filter        @"\U0000e612"
// 导航-邀请
#define Icon_bar_invite        @"\U0000e61c"
// 导航-创作
#define Icon_btn_write         @"\U0000e610"
// 导航-ARK-更多
#define Icon_bar_more_ark      @"0000"
// 导航-ARK-发布
#define Icon_bar_add_ark       @"0001"
// 导航-ARK-筛选
#define Icon_bar_filter_ark    @"0002"
// 导航-ARK-搜索
#define Icon_bar_search_ark    @"0003"
// 导航-ARK-创作
#define Icon_bar_cps_ark       @"0004"
// 导航-ARK-写作
#define Icon_bar_write_ark     @"0005"

// 字体
// 默认细体
#define font_30               font_(30.0)
#define font_17               font_(17.0)
#define font_16               font_(16.0)
#define font_15               font_(15.0)
#define font_13               font_(13.0)
#define font_12               font_(12.0)
#define font_11               font_(11.0)
#define font_10               font_(10.0)
// 默认粗体
#define font_cu_30            font_cu_(30.0)
#define font_cu_17            font_cu_(17.0)
#define font_cu_15            font_cu_(15.0)
#define font_cu_14            font_cu_(14.0)
#define font_cu_13            font_cu_(13.0)
#define font_cu_12            font_cu_(12.0)
#define font_cu_11            font_cu_(11.0)
// 默认更粗体
#define font_cu_cu_30         font_cu_cu_(30.0)
#define font_cu_cu_17         font_cu_cu_(17.0)
#define font_cu_cu_15         font_cu_cu_(15.0)
#define font_cu_cu_13         font_cu_cu_(13.0)
#define font_cu_cu_11         font_cu_cu_(11.0)
// 默认字体名称
#define font_cu_cu_(s)        [UIFont systemFontOfSize:s weight:UIFontWeightMedium]
#define font_cu_(s)           [UIFont systemFontOfSize:s weight:UIFontWeightRegular]
#define font_(s)              [UIFont systemFontOfSize:s weight:UIFontWeightLight]
// 默认图标字体
#define font_icon_(s)         [UIFont fontWithName:@"iconfont" size:s]
//self
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self


#endif /* ifndef RB_constants_h */
