//
//  JsonService.h

//
//  Created by AngusNi on 15/6/10.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
// 自定义工具集函数
#import "Utils.h"
// 自定义常量
#import "constants.h"
#import "LocalService.h"
// 自定义数据结构
#import "RBUserEntity.h"
#import "RBActivityEntity.h"
#import "RBCampaignEntity.h"
#import "RBCityEntity.h"
#import "RBUserDashboardEntity.h"
#import "RBTagEntity.h"
#import "RBAmountEntity.h"
#import "RBNotificationEntity.h"
#import "RBThirdEntity.h"
#import "RBPushEntity.h"
#import "RBArticleEntity.h"
#import "RBInfluenceEntity.h"
#import "RBInfluenceEntity.h"
#import "RBContactEntity.h"
#import "RBArticleActionEntity.h"
#import "RBQAEntity.h"
#import "RBInfluenceReadEntity.h"
#import "RBInfluenceUpgradeEntity.h"
#import "RBInfluenceHistoryEntity.h"
#import "RBRankingWeiboEntity.h"
#import "RBRankingTagsEntity.h"
#import "RBRankingWeiboDetailEntity.h"
#import "RBIncomeEntity.h"
#import "RBRewardEntity.h"
#import "RBAnnouncementEntity.h"
#import "RBUserSignEntity.h"
#import "RBUserBigVEntity.h"
#import "RBAnalysisListEntity.h"
#import "RBAnalysisWechatEntity.h"
#import "RBAnalysisWechatFollowerEntity.h"
#import "RBAnalysisWechatMessageEntity.h"
#import "RBAnalysisWechatArticleEntity.h"
#import "RBAnalysisWechatGenderEntity.h"
#import "RBAnalysisWechatLocationEntity.h"
#import "RBIndianaEntity.h"
#import "RBIndianaOrderEntity.h"
#import "RBAdvertiserCampaignEntity.h"
#import "RBAdvertiserCampaignPayEntity.h"
#import "RBKOLEntity.h"
#import "RBKOLShowEntity.h"
#import "RBKOLKeywordsEntity.h"
#import "RBKOLSocialEntity.h"
#import "RBKOLBannerEntity.h"
#import "RBCpsProductEntity.h"
#import "RBCpsCreateEntity.h"
#import "RBNlpEntity.h"
#import "RBNewInfluenceEntity.h"
#import "RBNewIndustriyEntity.h"
#import "RBLoginSocialEntity.h"
#import "RBSimilarEntity.h"
#import "RBMobileEntity.h"
#import "RBMyActityEntity.h"
#import "RBTudiEntity.h"
#import "RBActivityBannerEntity.h"
#import "RBPromoteEntity.h"
#import "RBCreditEntity.h"
@interface JsonService : NSObject

// RB
// RB-判断是否是Visitor
+ (BOOL)isRBUserVisitor;

// RB-验证JSON有效性
+ (NSString*)checkJson:(id)jsonObject;

// RB-登录模块
+ (RBUserEntity *)getRBUserEntity:(id)jsonObject;


// RB-活动模块
+ (NSMutableArray *)getRBActivityListEntity:(id)jsonObject
                               andBackArray:(NSMutableArray *)backArray;
// RB-活动bannerlist
+ (NSMutableArray *)getRBActivityBannerEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray;
// RB-我的活动模块
+ (NSMutableArray *)getRBMyActivityListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray;
// RB-活动Banner模块
+ (NSMutableArray *)getRBAnnouncementsListEntity:(id)jsonObject
                                    andBackArray:(NSMutableArray *)backArray;
// RB-活动模块详情
+ (RBActivityEntity *)getRBActivityEntity:(id)jsonObject;
// RB-活动-素材
+ (NSMutableArray *)RBActivityMaterialListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray;


// RB-城市模块
+ (NSMutableArray *)getRBCityListEntity:(id)jsonObject
                           andBackArray:(NSMutableArray *)backArray;



// RB-个人首页模块
+ (RBUserDashboardEntity *)getRBUserInfoDashboardEntity:(id)jsonObject;
// RB-USER模块
+ (RBInfluenceReadEntity *)getRBUserSocailEntity:(id)jsonObject;
// RB-标签模块
+ (NSMutableArray *)getRBTagListEntity:(id)jsonObject
                          andBackArray:(NSMutableArray *)backArray;
// RB-大V模块
+ (NSMutableArray *)getRBUserInfoBigVListEntity:(id)jsonObject
                               andBackArray:(NSMutableArray *)backArray;



// RB-收入模块
+ (RBAmountEntity *)getRBAmountEntity:(id)jsonObject;
// RB-收入模块-账单列表
+ (NSMutableArray *)getRBIncomeListEntity:(id)jsonObject
                             andBackArray:(NSMutableArray *)backArray;
// RB-收入模块-积分列表
+ (NSMutableArray *)getRBCreditListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray;

// RB-通知模块
+ (NSMutableArray *)getRBNotificationListEntity:(id)jsonObject
                                   andBackArray:(NSMutableArray *)backArray;

// RB-第三方绑定模块
+ (NSMutableArray *)getRBThirdListEntity:(id)jsonObject
                            andBackArray:(NSMutableArray *)backArray;


// RB-用户信息
// RB-保存用户RBUserEntity
+(void)setRBUserEntityWith:(id)jsonObject;
// RB-读取用户RBUserEntity
+(RBUserEntity*)getRBUserEntity;



// RB-消息模块
+ (RBPushEntity *)getRBPushEntity:(id)jsonObject;



+ (void)getUserUpateDateCheck;
// RB-文章模块
+ (NSMutableArray *)getRBArticleListEntity:(id)jsonObject
                           insertBackArray:(NSMutableArray *)backArray;
+ (NSMutableArray *)getRBArticleActionsListEntity:(id)jsonObject
                                     andBackArray:(NSMutableArray *)backArray;
// RB-文章模块
+ (NSMutableArray *)getRBArticleListEntity:(id)jsonObject
                              andBackArray:(NSMutableArray *)backArray;
// RB-文章模块
+ (NSMutableArray *)getRBArticleActionsForwardListEntity:(id)jsonObject
                                            andBackArray:(NSMutableArray *)backArray;

+ (NSMutableArray *)getRBArticleListEntity:(id)jsonObject
                        andBackArrayNormal:(NSMutableArray *)backArray;
// RB-文章模块阅读
+ (RBArticleActionEntity *)getRBArticleActionEntity:(id)jsonObject;
// RB-引擎模块列表
+ (NSMutableArray *)getRBEngineListEntity:(id)jsonObject
                             andBackArray:(NSMutableArray *)backArray;

// RB-社交模块
// RB-社交模块-影响力
+ (RBInfluenceEntity *)getRBInfluenceEntity:(id)jsonObject;
// RB-社交模块-影响力
+ (NSMutableArray *)getRBInfluenceEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray;
// RB-社交模块-影响力解读
+ (RBInfluenceReadEntity *)getRBInfluenceReadEntity:(id)jsonObject;
// RB-社交模块-影响力提升
+ (RBInfluenceUpgradeEntity *)getRBInfluenceUpgradeEntity:(id)jsonObject;
// RB-社交模块-影响力解读-标签云
+ (RBInfluenceReadEntity *)getRBInfluenceReadEntity:(id)jsonObject and:(RBInfluenceReadEntity *)entity;


// RB-新的影响力模块
// RB-新的影响力我的模块
+ (RBNewInfluenceEntity*)getRBNewInfluenceEntity:(id)jsonObject;
// RB-QA模块
+ (NSMutableArray *)getRBQAListEntity:(id)jsonObject
                         andBackArray:(NSMutableArray *)backArray;

// RB-R榜模块-微博
+ (NSMutableArray *)getRBRankingListWeiboEntity:(id)jsonObject
                              andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-微博详情
+ (RBRankingWeiboEntity *)getRBRankingDetailWeiboEntity:(id)jsonObject;
// RB-R榜模块-微信
+ (NSMutableArray *)getRBRankingListWechatEntity:(id)jsonObject
                              andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-微信详情
+ (RBRankingWeiboEntity *)getRBRankingDetailWechatEntity:(id)jsonObject;
// RB-R榜模块-雷达图
+ (NSMutableArray *)getRBRankingCategoriesEntity:(id)jsonObject
                                    andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-话题图
+ (NSMutableArray *)getRBRankingBoardsEntity:(id)jsonObject
                                andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-标签云
+ (NSMutableArray *)getRBRankingTagsEntity:(id)jsonObject
                              andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-微博列表
+ (NSMutableArray *)getRBRankingWeiboListEntity:(id)jsonObject
                                   andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-微信列表
+ (NSMutableArray *)getRBRankingWechatListEntity:(id)jsonObject
                                    andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-知乎
+ (NSMutableArray *)getRBRankingListZhihuEntity:(id)jsonObject
                                   andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-知乎列表
+ (NSMutableArray *)getRBRankingZhihuListEntity:(id)jsonObject
                                   andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-美拍
+ (NSMutableArray *)getRBRankingListMeipaiEntity:(id)jsonObject
                                    andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-美拍列表
+ (NSMutableArray *)getRBRankingMeipaiListEntity:(id)jsonObject
                                    andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-秒拍
+ (NSMutableArray *)getRBRankingListMiaopaiEntity:(id)jsonObject
                                     andBackArray:(NSMutableArray *)backArray;
// RB-R榜模块-秒拍列表
+ (NSMutableArray *)getRBRankingMiaopaiListEntity:(id)jsonObject
                                     andBackArray:(NSMutableArray *)backArray;

// RB-奖励模块-签到
+ (RBUserSignEntity *)getRBUserSignEntity:(id)jsonObject;
// RB-充值界面-充值
+ (RBPromoteEntity *)getRBPromoteEntity:(id)jsonObject;


// RB-分析-列表
+ (NSMutableArray *)getRBAnalysisListEntity:(id)jsonObject
                               andBackArray:(NSMutableArray *)backArray;
// RB-分析-微信详情
+ (RBAnalysisWechatEntity *)getRBAnalysisWechatEntity:(id)jsonObject;
// RB-分析-微信-粉丝
+ (NSMutableArray *)getRBAnalysisWechatFollowerListEntity:(id)jsonObject
                                             andBackArray:(NSMutableArray *)backArray;
// RB-分析-微信-消息
+ (NSMutableArray *)getRBAnalysisWechatMessageListEntity:(id)jsonObject
                                            andBackArray:(NSMutableArray *)backArray;
// RB-分析-微信-图文
+ (NSMutableArray *)getRBAnalysisWechatArticleListEntity:(id)jsonObject
                                            andBackArray:(NSMutableArray *)backArray;

// RB-分析-微博-列表
+ (NSMutableArray *)getRBAnalysisWeiboArticleListEntity:(id)jsonObject
                                           andBackArray:(NSMutableArray *)backArray;
// RB-分析-微博-地区
+ (NSMutableArray *)getRBAnalysisWeiboLocationListEntity:(id)jsonObject
                                            andBackArray:(NSMutableArray *)backArray;
// RB-分析-微博-性别
+ (RBAnalysisWechatGenderEntity *)getRBAnalysisWeiboGenderListEntity:(id)jsonObject;
// RB-分析-微博-关注
+ (NSMutableArray *)getRBAnalysisWeiboFriendListEntity:(id)jsonObject
                                          andBackArray:(NSMutableArray *)backArray;

// RB-分析-微博-粉丝-新增
+ (NSMutableArray *)getRBAnalysisWeiboFollowerNewListEntity:(id)jsonObject
                                               andBackArray:(NSMutableArray *)backArray;
// RB-分析-微博-粉丝-取消
+ (NSMutableArray *)getRBAnalysisWeiboFollowerCancelListEntity:(id)jsonObject
                                                  andBackArray:(NSMutableArray *)backArray;
// RB-分析-微博-粉丝-取消-列表
+ (NSMutableArray *)getRBAnalysisWeiboFollowerCancelDetailListEntity:(id)jsonObject
                                                        andBackArray:(NSMutableArray *)backArray;



// RB-一元夺宝
// RB-一元夺宝-列表
+ (NSMutableArray *)getRBIndianaListEntity:(id)jsonObject
                              andBackArray:(NSMutableArray *)backArray;
// RB-一元夺宝-详情
+ (RBIndianaEntity *)getRBIndianaDetailEntity:(id)jsonObject;
// RB-一元夺宝-详情-购买者
+ (NSMutableArray *)getRBIndianaDetailOrderEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray;
// RB-一元夺宝-详情-我的
+ (NSMutableArray *)getRBIndianaDetailMyEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray;



// RB-发布活动-详情
+ (RBAdvertiserCampaignEntity *)getRBAdvertiserCampaignEntity:(id)jsonObject;
// RB-发布活动-付款详情
+ (RBAdvertiserCampaignPayEntity *)getRBAdvertiserCampaignPayEntity:(id)jsonObject;
// RB-发布活动-列表
+ (NSMutableArray *)getRBAdvertiserCampaignEntityList:(id)jsonObject andBackArray:(NSMutableArray *)backArray;
// RB-发布活动-参与列表
+ (NSMutableArray *)getRBAdvertiserUserEntityList:(id)jsonObject andBackArray:(NSMutableArray *)backArray;
// RB-发布活动-期望结果
+ (NSMutableArray *)getRBAdvertiserResultEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray;
// RB-发布活动-分析结果-活动信息
+ (RBAdvertiserCampaignEntity *)getRBAdvertiserResultCampaignEntity:(id)jsonObject;
// RB-发布活动-分析结果-NLP信息
+ (RBNlpEntity *)getRBAdvertiserResultNlpEntity:(id)jsonObject;
// RB-活动参与人员-分析结果
+ (RBNlpEntity *)getRBCampaignKolResultNlpEntity:(id)jsonObject;
// RB-活动-KOL列表
+ (NSMutableArray *)getRBCampaignKolList:(id)jsonObject andBackArray:(NSMutableArray *)backArray;

// RB-用户详情模块
+ (RBUserEntity *)getRBUserDetailEntity:(id)jsonObject;



// RB-KOL模块
// RB-KOL列表模块
+ (NSMutableArray *)getRBKolListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray;
// RB-KOL关注列表模块
+ (NSMutableArray *)getRBKolFollowersListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray;
// RB-KOL详情模块
+ (RBKOLEntity *)getRBKolDetailEntity:(id)jsonObject;
// RB-解除绑定成功以后返回的社交账号数组
+ (NSMutableArray *)getRBAllSocialAccounts:(id)jsonObject andBackArray:(NSMutableArray *)backArray;
// RB-KOL列表Banner模块
+ (NSMutableArray *)getRBKolBannerListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray;
// RB-KOL-我的
+ (RBUserDashboardEntity *)getRBKolDashboardDetailEntity:(id)jsonObject;


// RB-CPS模块
// RB-CPS产品列表
+ (NSMutableArray *)getRBCpsProductListEntity:(id)jsonObject
                                 andBackArray:(NSMutableArray *)backArray;
// RB-CPS创作列表
+ (NSMutableArray *)getRBCpsCreateListEntity:(id)jsonObject
                                andBackArray:(NSMutableArray *)backArray;
// RB-CPS创作详情
+ (RBCpsCreateEntity *)getRBCpsCreateDetailEntity:(id)jsonObject;
// RB-获取通讯录
+ (NSMutableArray *)getRBMobile:(id)jsonObject AndBackArray:(NSMutableArray *)backArray AndPhoneArray:(NSMutableArray*)PhoneArray;
// RB-徒弟总列表
+ (NSMutableArray *)getAllTudiList:(id)jsonObject AndBackArray:(NSMutableArray *)backArray;
@end
