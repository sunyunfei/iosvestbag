//
//  LocalService.h

//
//  Created by AngusNi on 15/6/10.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
// 自定义工具集函数
#import "Utils.h"
// 自定义常量
#import "constants.h"

@interface LocalService : NSObject
// 本地数据管理
// UserIndiana
// RB-保存用户UserIndiana
+(void)setRBLocalDataUserIndianaWith:(NSString*)str;
// RB-读取用户UserIndiana
+(NSString*)getRBLocalDataUserIndiana;
//RB-保存是否隐藏我的钱包标识
+(void)setRBLocalDataUserHideWith:(NSString*)str;
//RB-读取是否隐藏我的钱包标识
+(NSString*)getRBLocalDataUserHide;
//RB-保存用户绑定的wechatUID
+ (void)setRBLocalWechatUID:(NSString*)str;
//RB-保存开发者设置的域名
+ (void)setRBDevelopIPURL: (NSString*)str;
//RB-获取开发者设置的域名
+ (NSString*)getRBDevelopIPURL;
+ (NSString*)getRBLocalWechatUID;
// UserAgree
// RB-保存用户UserAgree
+(void)setRBLocalDataUserAgreeWith:(NSString*)str;
// RB-读取用户UserAgree
+(NSString*)getRBLocalDataUserAgree;
// UserLoginId
// RB-保存用户UserLoginId
+(void)setRBLocalDataUserLoginIdWith:(NSString*)str;
// RB-读取用户UserLoginId
+(NSString*)getRBLocalDataUserLoginId;
// RB-保存用户第一次进入引导影响力标识
+(void)setRBLocalTestInfluence;
// RB-获取用户第一次进入引导影响力标识
+(NSString*)getRBLocalTestInfluence;
// RB-保存用户第一次测试影响力标识
+(void)setRBFirstInfluenceenter:(NSString*)enterMark;
// RB-获取用户第一次测试影响力标识
+(NSString*)getRBFirstInfluenceenter;
// RB-用户第一次进入APP
+(void)setRBfirstEnterAPP;
// RB-获取用户第一次进入APP的标识
+(BOOL)getRBfirstEnterApp;
// RB-用户第一次进入我的界面
+(void)setRBfirstEnterMyInterface;
// RB-获取用户第一次进入我的界面
+(BOOL)getRBfirstEnterMyInterface;
// RB-第一次进入活动详情界面标识
+(void)setRBfirstEnterCampain;
// RB-获取第一次进入活动详情界面标识
+(BOOL)getRBfirstEnterCampain;
// RB-读取记录是否是在审核期间
+(BOOL)getRBIsIncheck;
// RB-记录是否在审核期间
+(void)setRBIsInCheck:(NSString*)isInCheck;
// RB-记录是否是新用户
+(void)setRBIsNewMember:(NSString*)str;
// RB-获取是否是新用户
+(BOOL)getRBIsNewMember;
// UserLoginName
// RB-保存用户UserLoginName
+(void)setRBLocalDataUserLoginNameWith:(NSString*)str;
// RB-读取用户UserLoginName
+(NSString*)getRBLocalDataUserLoginName;
// UserLoginAvatar
// RB-保存用户UserLoginAvatar
+(void)setRBLocalDataUserLoginAvatarWith:(NSString*)str;
// RB-读取用户UserLoginAvatar
+(NSString*)getRBLocalDataUserLoginAvatar;
// UserLoginPhone
// RB-保存用户UserLoginPhone
+(void)setRBLocalDataUserLoginPhoneWith:(NSString*)str;
// RB-读取用户UserLoginPhone
+(NSString*)getRBLocalDataUserLoginPhone;
// UserLoginKolId
// RB-保存用户UserLoginKolId
+(void)setRBLocalDataUserLoginKolIdWith:(NSString*)str;
// RB-保存用户Email
+(void)setRBLocalEmail:(NSString*)str;
// RB-读取用户Email
+(NSString*)getRBLocalEmail;
// RB-读取用户UserLoginKolId
+(NSString*)getRBLocalDataUserLoginKolId;




// UserPublicName
// RB-保存用户UserPublicName
+(void)setRBLocalDataUserPublicNameWith:(NSString*)publicName;
// RB-读取用户UserPublicName
+(NSString*)getRBLocalDataUserPublicName;


// UserPublicPwd
// RB-保存用户UserPublicPwd
+(void)setRBLocalDataUserPublicPwdWith:(NSString*)publicPwd;
// RB-读取用户UserPublicPwd
+(NSString*)getRBLocalDataUserPublicPwd;


// UserVersionShow
// RB-保存用户UserVersionShow
+(void)setRBLocalDataUserVersionShowWith:(NSString*)versionShow;
// RB-读取用户UserVersionShow
+(NSString*)getRBLocalDataUserVersionShow;


// UserWechatPublicToken
// RB-保存用户UserWechatPublicToken
+(void)setRBLocalDataUserWechatPublicTokenWith:(NSString*)userWechatPublicToken;
// RB-读取用户UserWechatPublicToken
+(NSString*)getRBLocalDataUserWechatPublicToken;


// UserCookieString
// RB-保存用户UserCookieString
+(void)setRBLocalDataUserCookieStringWith:(NSString*)userCookie;
// RB-读取用户UserCookieString
+(NSString*)getRBLocalDataUserCookieString;


// UserCookie
// RB-保存用户UserCookie
+(void)setRBLocalDataUserCookieWith:(NSData*)userCookie;
// RB-读取用户UserCookie
+(NSData*)getRBLocalDataUserCookie;

// UserKolSearchList
// RB-保存用户UserKolSearchList
+(void)setRBLocalDataUserKolSearchListWith:(NSString*)searchList;
// RB-读取用户UserKolSearchList
+(NSString*)getRBLocalDataUserKolSearchList;

// UserSearchList
// RB-保存用户UserSearchList
+(void)setRBLocalDataUserSearchListWith:(NSString*)searchList;
// RB-读取用户UserSearchList
+(NSString*)getRBLocalDataUserSearchList;


// UserRankingSearchList
// RB-保存用户UserRankingSearchList
+(void)setRBLocalDataUserRankingSearchListWith:(NSString*)searchList;
// RB-读取用户UserRankingSearchList
+(NSString*)getRBLocalDataUserRankingSearchList;

// UserEngineSearchList
// RB-保存用户UserEngineSearchList
+(void)setRBLocalDataUserEngineSearchListWith:(NSString*)searchList;
// RB-读取用户UserEngineSearchList
+(NSString*)getRBLocalDataUserEngineSearchList;

// UserRongToken
// RB-保存用户UserRongToken
+(void)setRBLocalDataUserRongTokenWith:(NSString*)rongToken;
// RB-读取用户UserRongToken
+(NSString*)getRBLocalDataUserRongToken;


/// UserShake
// RB-保存用户UserShake
+(void)setRBLocalDataUserShakeWith:(NSString*)shake;
// RB-读取用户UserShake
+(NSString*)getRBLocalDataUserShake;

// UserPush
// RB-保存用户UserPush
+(void)setRBLocalDataUserPushWith:(NSString*)push;
// RB-读取用户UserPush
+(NSString*)getRBLocalDataUserPush;


// UserUpateDate
// RB-保存用户UserUpateDate
+(void)setRBLocalDataUserUpateDateWith:(NSString*)upateDate;
// RB-读取用户UserUpateDate
+(NSString*)getRBLocalDataUserUpateDate;


// UserCity
// RB-保存用户UserCity
+(void)setRBLocalDataUserCityWith:(NSString*)city andName:(NSString*)cityname;
// RB-读取用户UserCity
+(NSString*)getRBLocalDataUserCity;
// RB-读取用户UserCityName
+(NSString*)getRBLocalDataUserCityName;

// UserKolCity
// RB-保存用户UserKolCity
+(void)setRBLocalDataUserKolCityWith:(NSString*)city;
// RB-读取用户UserKolCity
+(NSString*)getRBLocalDataUserKolCity;

// UserGuide
// RB-保存用户UserGuide
+(void)setRBLocalDataUserGuideWith:(NSString*)userGuide;
// RB-读取用户UserGuide
+(NSString*)getRBLocalDataUserGuide;

// UserKOL
// RB-保存用户UserKOL
+(void)setRBLocalDataUserKolWith:(NSString*)kol_uuid;
// RB-读取用户UserKOL
+(NSString*)getRBLocalDataUserKol;


// PrivateToken
// RB-保存用户PrivateToken
+(void)setRBLocalDataUserPrivateTokenWith:(NSString*)private_token;
// RB-读取用户PrivateToken
+(NSString*)getRBLocalDataUserPrivateToken;


// IDFA
// RB-保存用户IDFA
+(void)setRBLocalDataUserIDFAWith:(NSString*)idfa;
// RB-读取用户IDFA
+(NSString*)getRBLocalDataUserIDFA;

// DeviceToken
// RB-保存用户DeviceToken
+(void)setRBLocalDataUserDeviceTokenWith:(NSString*)deviceToken;
// RB-读取用户DeviceToken
+(NSString*)getRBLocalDataUserDeviceToken;


// ClientId
// RB-保存用户ClientId
+(void)setRBLocalDataUserClientIdWith:(NSString*)clientId;
// RB-读取用户ClientId
+(NSString*)getRBLocalDataUserClientId;


// Article
// RB-保存用户Article
+(void)setRBLocalDataUserArticleWith:(NSString*)article;
// RB-读取用户Article
+(NSString*)getRBLocalDataUserArticle;


// 经纬度
// RB-保存用户经纬度
+(void)setRBLocalDataUserLatitudeWith:(NSString*)latitude andLongitude:(NSString*)longitude;
// RB-读取用户UserLatitude
+(NSString*)getRBLocalDataUserLatitude;
// RB-读取用户UserLongitude
+(NSString*)getRBLocalDataUserLongitude;

@end
