//
//  JsonService.m
//
//  Created by AngusNi on 15/6/10.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "JsonService.h"
#import "JWT.h"
#import "ArticleEntityManager.h"

@implementation JsonService

// RB
// RB-判断是否是Visitor
+(BOOL)isRBUserVisitor {
    if([[LocalService getRBLocalDataUserLoginPhone] isEqualToString:@"13000000000"] && [LocalService getRBLocalEmail].length <= 0) {
        return YES;
    }
    return NO;
}

// RB-验证JSON有效性
+ (NSString *)checkJson:(id)jsonObject {
    if (jsonObject == nil || [jsonObject isEqual:[NSNull null]]) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", jsonObject];
}

// RB-登录模块
+ (RBUserEntity *)getRBUserEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"kol"];
    RBUserEntity *entity = [[RBUserEntity alloc] init];
    entity.iid = [self checkJson:[resultArray valueForKey:@"id"]];
    entity.is_new_member = [self checkJson:[jsonObject valueForKey:@"is_new_member"]];
    entity.email = [self checkJson:[resultArray valueForKey:@"email"]];
    entity.mobile_number = [self checkJson:[resultArray valueForKey:@"mobile_number"]];
    entity.name = [self checkJson:[resultArray valueForKey:@"name"]];
    entity.gender = [self checkJson:[resultArray valueForKey:@"gender"]];
    entity.date_of_birthday = [self checkJson:[resultArray valueForKey:@"date_of_birthday"]];
    entity.alipay_account = [self checkJson:[resultArray valueForKey:@"alipay_account"]];
    entity.desc = [self checkJson:[resultArray valueForKey:@"desc"]];
    entity.country = [self checkJson:[resultArray valueForKey:@"country"]];
    entity.app_city_label = [self checkJson:[resultArray valueForKey:@"app_city_label"]];
    entity.app_city = [self checkJson:[resultArray valueForKey:@"app_city"]];
    entity.avatar_url = [self checkJson:[resultArray valueForKey:@"avatar_url"]];
    entity.kol_uuid = [self checkJson:[resultArray valueForKey:@"kol_uuid"]];
    entity.age = [self checkJson:[resultArray valueForKey:@"age"]];
    entity.weixin_friend_count = [self checkJson:[resultArray valueForKey:@"weixin_friend_count"]];
    entity.kol_role = [self checkJson:[resultArray valueForKey:@"kol_role"]];
    entity.role_apply_status = [self checkJson:[resultArray valueForKey:@"role_apply_status"]];
    entity.kol_check_remark = [self checkJson:[resultArray valueForKey:@"kol_check_remark"]];
    entity.issue_token = [self checkJson:[resultArray valueForKey:@"issue_token"]];
    entity.rongcloud_token = [self checkJson:[resultArray valueForKey:@"rongcloud_token"]];
    entity.influence_score = [self checkJson:[resultArray valueForKey:@"influence_score"]];
    entity.selected_like_articles = [self checkJson:[resultArray valueForKey:@"selected_like_articles"]];
    
    entity.tags = [NSMutableArray new];
    NSArray *tagsArray = [resultArray valueForKey:@"tags"];
    for (int i=0; i<[tagsArray count]; i++) {
        RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
        tagEntity.name = [self checkJson:[tagsArray[i] valueForKey:@"name"]];
        tagEntity.label = [self checkJson:[tagsArray[i] valueForKey:@"label"]];
        [entity.tags addObject:tagEntity];
    }
    entity.socialArray = [NSMutableArray new];
    NSArray * socialArr = [jsonObject objectForKey:@"kol_identities"];
    for (NSInteger i = 0; i<socialArr.count; i++) {
        RBLoginSocialEntity * Ientity = [[RBLoginSocialEntity alloc]init];
        Ientity.avatar_url = [self checkJson:[socialArr[i] valueForKey:@"avatar_url"]];
        Ientity.name = [self checkJson:[socialArr[i] valueForKey:@"name"]];
        Ientity.provider = [self checkJson:[socialArr[i] valueForKey:@"provider"]];
        Ientity.token = [self checkJson:[socialArr[i] valueForKey:@"token"]];
        Ientity.uid = [self checkJson:[socialArr[i] valueForKey:@"uid"]];
        Ientity.desc = [self checkJson:[socialArr[i] valueForKey:@"desc"]];
        Ientity.url = [self checkJson:[socialArr[i] valueForKey:@"url"]];
        [entity.socialArray addObject:Ientity];
    }
    return entity;
}

// RB-活动模块
+ (NSMutableArray *)getRBActivityListEntity:(id)jsonObject
                               andBackArray:(NSMutableArray *)backArray {
    if ([jsonObject objectForKey:@"campaign_invites"] == nil || [[jsonObject objectForKey:@"campaign_invites"] isEqual:[NSNull null]]) {
        return backArray;
    }
    NSArray *resultArray = [jsonObject objectForKey:@"campaign_invites"];
    NSArray *idArray = [resultArray valueForKey:@"id"];
    
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBActivityEntity *entity = [[RBActivityEntity alloc] init];
            entity.iid = [self checkJson:[resultArray[i] valueForKey:@"id"]];
            entity.status = [self checkJson:[resultArray[i] valueForKey:@"status"]];
            entity.img_status = [self checkJson:[resultArray[i] valueForKey:@"img_status"]];
            entity.share_url = [self checkJson:[resultArray[i] valueForKey:@"share_url"]];
            entity.is_invited = [self checkJson:[resultArray[i] valueForKey:@"is_invited"]];
            entity.screenshot = [self checkJson:[resultArray[i] valueForKey:@"screenshot"]];
            entity.reject_reason = [self checkJson:[resultArray[i] valueForKey:@"reject_reason"]];
            entity.approved_at = [self checkJson:[resultArray[i] valueForKey:@"approved_at"]];
            entity.can_upload_screenshot = [self checkJson:[resultArray[i] valueForKey:@"can_upload_screenshot"]];
            entity.upload_interval_time = [resultArray[i] valueForKey:@"upload_interval_time"];
            entity.avail_click = [self checkJson:[resultArray[i] valueForKey:@"avail_click"]];
            entity.total_click = [self checkJson:[resultArray[i] valueForKey:@"total_click"]];
            entity.earn_money = [self checkJson:[resultArray[i] valueForKey:@"earn_money"]];
            entity.remain_budget = [self checkJson:[resultArray[i] valueForKey:@"remain_budget"]];
            entity.tag = [self checkJson:[resultArray[i] valueForKey:@"tag"]];
            entity.cpi_example_screenshot = [self checkJson:[resultArray[i] valueForKey:@"cpi_example_screenshot"]];
            //
            NSArray *campaignArray = [resultArray[i] valueForKey:@"campaign"];
            RBCampaignEntity *campaignEntity = [[RBCampaignEntity alloc] init];
            campaignEntity.iid = [self checkJson:[campaignArray valueForKey:@"id"]];
            campaignEntity.name = [self checkJson:[campaignArray valueForKey:@"name"]];
            campaignEntity.idescription = [self checkJson:[campaignArray valueForKey:@"description"]];
            campaignEntity.img_url = [self checkJson:[campaignArray valueForKey:@"img_url"]];
            campaignEntity.status = [self checkJson:[campaignArray valueForKey:@"status"]];
            campaignEntity.message = [self checkJson:[campaignArray valueForKey:@"message"]];
            campaignEntity.url = [self checkJson:[campaignArray valueForKey:@"url"]];
            campaignEntity.per_budget_type = [self checkJson:[campaignArray valueForKey:@"per_budget_type"]];
            campaignEntity.per_action_budget = [self checkJson:[campaignArray valueForKey:@"per_action_budget"]];
            campaignEntity.per_action_budget = [Utils getNSStringTwoFloat:campaignEntity.per_action_budget];
            campaignEntity.budget = [self checkJson:[campaignArray valueForKey:@"budget"]];
            campaignEntity.deadline = [self checkJson:[campaignArray valueForKey:@"deadline"]];
            campaignEntity.start_time = [self checkJson:[campaignArray valueForKey:@"start_time"]];
            campaignEntity.brand_name = [self checkJson:[campaignArray valueForKey:@"brand_name"]];
            campaignEntity.avail_click = [self checkJson:[campaignArray valueForKey:@"avail_click"]];
            campaignEntity.total_click = [self checkJson:[campaignArray valueForKey:@"total_click"]];
            campaignEntity.take_budget = [self checkJson:[campaignArray valueForKey:@"take_budget"]];
            campaignEntity.take_budget = [Utils getNSStringTwoFloat:campaignEntity.take_budget];
            campaignEntity.remain_budget = [self checkJson:[campaignArray valueForKey:@"remain_budget"]];
            campaignEntity.remain_budget = [Utils getNSStringTwoFloat:campaignEntity.remain_budget];
            campaignEntity.share_times = [self checkJson:[campaignArray valueForKey:@"share_times"]];
            campaignEntity.address = [self checkJson:[campaignArray valueForKey:@"address"]];
            campaignEntity.hide_brand_name = [self checkJson:[campaignArray valueForKey:@"hide_brand_name"]];
            campaignEntity.task_description = [self checkJson:[campaignArray valueForKey:@"task_description"]];
            campaignEntity.interval_time = [campaignArray valueForKey:@"interval_time"];
            campaignEntity.recruit_start_time = [campaignArray valueForKey:@"recruit_start_time"];
            campaignEntity.recruit_end_time = [campaignArray valueForKey:@"recruit_end_time"];
            campaignEntity.max_action = [self checkJson:[campaignArray valueForKey:@"max_action"]];
            campaignEntity.per_action_type = [self checkJson:[campaignArray valueForKey:@"per_action_type"]];
            campaignEntity.action_desc = [self checkJson:[campaignArray valueForKey:@"action_desc"]];
            campaignEntity.start_time = [self checkJson:[campaignArray valueForKey:@"start_time"]];
            entity.campaign=campaignEntity;
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-活动bannerlist
+ (NSMutableArray *)getRBActivityBannerEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray{
    if ([jsonObject objectForKey:@"announcements"] == nil || [[jsonObject objectForKey:@"announcements"] isEqual:[NSNull null]]) {
        return backArray;
    }
    NSArray * arr = [jsonObject objectForKey:@"announcements"];
    for (NSInteger i = 0; i < arr.count; i ++) {
        @autoreleasepool{
            RBActivityBannerEntity * entity = [[RBActivityBannerEntity alloc]init];
            entity.banner_url = [self checkJson:[arr[i] objectForKey:@"banner_url"]];
            entity.desc = [self checkJson:[arr[i] objectForKey:@"desc"]];
            entity.detail_type = [self checkJson:[arr[i] objectForKey:@"detail_type"]];
            entity.title = [self checkJson:[arr[i] objectForKey:@"title"]];
            entity.url = [self checkJson:[arr[i] objectForKey:@"url"]];
            entity.bannerID = [self checkJson:[arr[i] objectForKey:@"id"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-我的活动界面
+ (NSMutableArray *)getRBMyActivityListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray * campaingnArr = [jsonObject objectForKey:@"my_campaigns"];
    if (campaingnArr.count == 0) {
        return backArray;
    }
    for (NSInteger i = 0; i < campaingnArr.count; i ++) {
        @autoreleasepool{
            RBMyActityEntity * entity = [[RBMyActityEntity alloc]init];
            entity.campaign_id = [self checkJson:[campaingnArr[i] objectForKey:@"campaign_id"]];
            entity.earn_money = [self checkJson:[campaingnArr[i] objectForKey:@"earn_money"]];
            entity.iid = [self checkJson:[campaingnArr[i] objectForKey:@"id"]];
            entity.img_status = [self checkJson:[campaingnArr[i] objectForKey:@"img_status"]];
            entity.reject_reason = [self checkJson:[campaingnArr[i] objectForKey:@"reject_reason"]];
            entity.status = [self checkJson:[campaingnArr[i] objectForKey:@"status"]];
            entity.campaign_name = [self checkJson:[campaingnArr[i] objectForKey:@"campaign_name"]];
            entity.per_action_type = [self checkJson:[campaingnArr[i] objectForKey:@"per_action_type"]];
            entity.screenshot = [self checkJson:[campaingnArr[i] objectForKey:@"screenshot"]];
            entity.per_budget_type = [self checkJson:[campaingnArr[i] objectForKey:@"per_budget_type"]];

            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-活动Banner模块
+ (NSMutableArray *)getRBAnnouncementsListEntity:(id)jsonObject
                                    andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"announcements"];
    NSArray *detail_typeArray = [resultArray valueForKey:@"detail_type"];
    for (int i = 0; i < [detail_typeArray count]; i++) {
        @autoreleasepool {
            RBAnnouncementEntity *entity = [[RBAnnouncementEntity alloc] init];
            entity.title = [self checkJson:[resultArray[i] valueForKey:@"title"]];
            entity.desc = [self checkJson:[resultArray[i] valueForKey:@"desc"]];
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            entity.detail_type = [self checkJson:[resultArray[i] valueForKey:@"detail_type"]];
            entity.banner_url = [self checkJson:[resultArray[i] valueForKey:@"banner_url"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}


// RB-活动模块详情
+ (RBActivityEntity *)getRBActivityEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"campaign_invite"];
    RBActivityEntity *entity = [[RBActivityEntity alloc] init];
    entity.iid  =
    [self checkJson:[resultArray valueForKey:@"id"]];
    entity.uuid  =
    [self checkJson:[resultArray valueForKey:@"uuid"]];
    entity.status  =
    [self checkJson:[resultArray valueForKey:@"status"]];
    entity.img_status  =
    [self checkJson:[resultArray valueForKey:@"img_status"]];
    entity.share_url  =
    [self checkJson:[resultArray valueForKey:@"share_url"]];
    entity.is_invited  =
    [self checkJson:[resultArray valueForKey:@"is_invited"]];
    entity.screenshot  =
    [self checkJson:[resultArray valueForKey:@"screenshot"]];
    entity.reject_reason  =
    [self checkJson:[resultArray valueForKey:@"reject_reason"]];
    entity.approved_at  =
    [self checkJson:[resultArray valueForKey:@"approved_at"]];
    entity.can_upload_screenshot  =
    [self checkJson:[resultArray valueForKey:@"can_upload_screenshot"]];
    entity.start_upload_screenshot =
    [self checkJson:[resultArray valueForKey:@"start_upload_screenshot"]];
    entity.upload_interval_time  = [resultArray valueForKey:@"upload_interval_time"];
    entity.avail_click  =
    [self checkJson:[resultArray valueForKey:@"avail_click"]];
    entity.total_click  =
    [self checkJson:[resultArray valueForKey:@"total_click"]];
    entity.earn_money  =
    [self checkJson:[resultArray valueForKey:@"earn_money"]];
    entity.remain_budget  =
    [self checkJson:[resultArray valueForKey:@"remain_budget"]];
    entity.tag  =
    [self checkJson:[resultArray valueForKey:@"tag"]];
    entity.ocr_detail  =
    [self checkJson:[resultArray valueForKey:@"ocr_detail"]];
    entity.ocr_status  =
    [self checkJson:[resultArray valueForKey:@"ocr_status"]];
    entity.alert = [self checkJson:[jsonObject objectForKey:@"alert"]];
    entity.cpi_example_screenshot = [self checkJson:[resultArray valueForKey:@"cpi_example_screenshot"]];
    if ([resultArray valueForKey:@"cpi_example_screenshots"] != nil && ![[resultArray valueForKey:@"cpi_example_screenshots"] isEqual:[NSNull null]]) {
        entity.cpi_example_screenshots = [resultArray valueForKey:@"cpi_example_screenshots"];
    }
    if ([resultArray valueForKey:@"screenshots"] != nil && ![[resultArray valueForKey:@"screenshots"] isEqual:[NSNull null]]) {
        entity.screenshots = [resultArray valueForKey:@"screenshots"];
    }
    if ([resultArray valueForKey:@"screenshot_comment"] != nil && ![[resultArray valueForKey:@"screenshot_comment"] isEqual:[NSNull null]]) {
        entity.screenshot_comment = [resultArray valueForKey:@"screenshot_comment"];
    }
    entity.invitees_count  =
    [self checkJson:[jsonObject objectForKey:@"invitees_count"]];
    entity.sub_type = [self checkJson:[resultArray valueForKey:@"sub_type"]];
    entity.leader_club = [self checkJson:[jsonObject objectForKey:@"leader_club"]];
    //
    NSArray *inviteesArray = [jsonObject objectForKey:@"invitees"];
    NSArray *inviteesIdArray = [inviteesArray valueForKey:@"id"];
    entity.invitees = [NSMutableArray new];
    for (int i = 0; i < [inviteesIdArray count]; i++) {
        @autoreleasepool {
            RBUserEntity *uEntity = [[RBUserEntity alloc] init];
            uEntity.iid = [self checkJson:[inviteesArray[i] valueForKey:@"id"]];
            uEntity.name = [self checkJson:[inviteesArray[i] valueForKey:@"name"]];
            uEntity.avatar_url = [self checkJson:[inviteesArray[i] valueForKey:@"avatar_url"]];
            [entity.invitees addObject:uEntity];
        }
    }
    //
    NSArray *campaignArray = [resultArray valueForKey:@"campaign"];
    RBCampaignEntity *campaignEntity = [[RBCampaignEntity alloc] init];
    campaignEntity.iid = [self checkJson:[campaignArray valueForKey:@"id"]];
    campaignEntity.name = [self checkJson:[campaignArray valueForKey:@"name"]];
    campaignEntity.idescription = [self checkJson:[campaignArray valueForKey:@"description"]];
    campaignEntity.img_url = [self checkJson:[campaignArray valueForKey:@"img_url"]];
    campaignEntity.status = [self checkJson:[campaignArray valueForKey:@"status"]];
    campaignEntity.message = [self checkJson:[campaignArray valueForKey:@"message"]];
    campaignEntity.url = [self checkJson:[campaignArray valueForKey:@"url"]];
    campaignEntity.per_budget_type = [self checkJson:[campaignArray valueForKey:@"per_budget_type"]];
    campaignEntity.sub_type = [self checkJson:[campaignArray valueForKey:@"sub_type"]];
    campaignEntity.per_action_budget = [self checkJson:[campaignArray valueForKey:@"per_action_budget"]];
    campaignEntity.budget = [self checkJson:[campaignArray valueForKey:@"budget"]];
    campaignEntity.deadline = [self checkJson:[campaignArray valueForKey:@"deadline"]];
    campaignEntity.start_time = [self checkJson:[campaignArray valueForKey:@"start_time"]];
    campaignEntity.brand_name = [self checkJson:[campaignArray valueForKey:@"brand_name"]];
    campaignEntity.avail_click = [self checkJson:[campaignArray valueForKey:@"avail_click"]];
    campaignEntity.total_click = [self checkJson:[campaignArray valueForKey:@"total_click"]];
    campaignEntity.take_budget = [self checkJson:[campaignArray valueForKey:@"take_budget"]];
    campaignEntity.remain_budget = [self checkJson:[campaignArray valueForKey:@"remain_budget"]];
    campaignEntity.share_times = [self checkJson:[campaignArray valueForKey:@"share_times"]];
    campaignEntity.address = [self checkJson:[campaignArray valueForKey:@"address"]];
    campaignEntity.hide_brand_name = [self checkJson:[campaignArray valueForKey:@"hide_brand_name"]];
    campaignEntity.task_description = [self checkJson:[campaignArray valueForKey:@"task_description"]];
    campaignEntity.interval_time = [campaignArray valueForKey:@"interval_time"];
    campaignEntity.recruit_start_time = [campaignArray valueForKey:@"recruit_start_time"];
    campaignEntity.recruit_end_time = [campaignArray valueForKey:@"recruit_end_time"];
    campaignEntity.per_action_type = [self checkJson:[campaignArray valueForKey:@"per_action_type"]];
    campaignEntity.wechat_auth_type = [self checkJson:[campaignArray valueForKey:@"wechat_auth_type"]];
    campaignEntity.action_desc = [self checkJson:[campaignArray valueForKey:@"action_desc"]];
    campaignEntity.remark = [self checkJson:[campaignArray valueForKey:@"remark"]];
    campaignEntity.cpi_example_screenshot  = [self checkJson:[campaignArray valueForKey:@"cpi_example_screenshot"]];
    if ([campaignArray valueForKey:@"cpi_example_screenshots"] != nil && ![[campaignArray valueForKey:@"cpi_example_screenshots"] isEqual:[NSNull null]]) {
        campaignEntity.cpi_example_screenshots = [campaignArray valueForKey:@"cpi_example_screenshots"];
    }
    
    campaignEntity.is_applying_note_required  = [self checkJson:[campaignArray valueForKey:@"is_applying_note_required"]];
    campaignEntity.applying_note_description  = [self checkJson:[campaignArray valueForKey:@"applying_note_description"]];
    campaignEntity.is_applying_picture_required  = [self checkJson:[campaignArray valueForKey:@"is_applying_picture_required"]];
    campaignEntity.applying_picture_description  = [self checkJson:[campaignArray valueForKey:@"applying_picture_description"]];
    entity.campaign = campaignEntity;
    return entity;
}

// RB-活动-素材
+ (NSMutableArray *)RBActivityMaterialListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"campaign_materials"];
    NSArray *urlArray = [resultArray valueForKey:@"url"];
    for (int i = 0; i < [urlArray count]; i++) {
        @autoreleasepool {
            RBKOLBannerEntity *entity = [[RBKOLBannerEntity alloc] init];
            entity.category = [self checkJson:[resultArray[i] valueForKey:@"url_type"]];
            entity.cover_url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}


// RB-城市模块
+ (NSMutableArray *)getRBCityListEntity:(id)jsonObject
                           andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"cities"];
    NSArray *nameArray = [resultArray valueForKey:@"name"];
    for (int i = 0; i < [nameArray count]; i++) {
        @autoreleasepool {
            RBCityEntity *entity = [[RBCityEntity alloc] init];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.name_en = [self checkJson:[resultArray[i] valueForKey:@"name_en"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}



// RB-个人首页模块
+ (RBUserDashboardEntity *)getRBUserInfoDashboardEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"kol"];
    RBUserDashboardEntity *entity = [[RBUserDashboardEntity alloc] init];
    entity.influence_score = [self checkJson:[resultArray valueForKey:@"influence_score"]];
    entity.today_income = [self checkJson:[resultArray valueForKey:@"today_income"]];
    entity.unread_count = [self checkJson:[resultArray valueForKey:@"unread_count"]];
    entity.verifying_count = [self checkJson:[resultArray valueForKey:@"verifying_count"]];
    entity.settled_count = [self checkJson:[resultArray valueForKey:@"settled_count"]];
    entity.waiting_upload_count = [self checkJson:[resultArray valueForKey:@"waiting_upload_count"]];
    
    return entity;
}

// RB-USER模块
+ (RBInfluenceReadEntity *)getRBUserSocailEntity:(id)jsonObject {
    RBInfluenceReadEntity *entity = [[RBInfluenceReadEntity alloc] init];
    NSArray *kol_valueArray = [jsonObject objectForKey:@"kol_value"];
    entity.influence_score = [self checkJson:[kol_valueArray valueForKey:@"influence_score"]];
    entity.influence_level = [self checkJson:[kol_valueArray valueForKey:@"influence_level"]];
    entity.name = [self checkJson:[kol_valueArray valueForKey:@"name"]];
    entity.avatar_url = [self checkJson:[kol_valueArray valueForKey:@"avatar_url"]];
    //
    NSArray *resultArray = [jsonObject objectForKey:@"item_rate"];
    entity.feature_rate = [self checkJson:[resultArray valueForKey:@"feature_rate"]];
    entity.active_rate = [self checkJson:[resultArray valueForKey:@"active_rate"]];
    entity.campaign_rate = [self checkJson:[resultArray valueForKey:@"campaign_rate"]];
    entity.share_rate = [self checkJson:[resultArray valueForKey:@"share_rate"]];
    entity.contact_rate = [self checkJson:[resultArray valueForKey:@"contact_rate"]];
    //
    entity.history = [NSMutableArray new];
    NSArray *tasksArray = [jsonObject objectForKey:@"tasks"];
    NSArray *task_typeArray = [tasksArray valueForKey:@"task_type"];
    for (int i = 0; i < [task_typeArray count]; i++) {
        @autoreleasepool {
            RBRewardEntity *rewardEntity = [[RBRewardEntity alloc] init];
            rewardEntity.task_type = [self checkJson:[tasksArray[i] valueForKey:@"task_type"]];
            rewardEntity.task_name = [self checkJson:[tasksArray[i] valueForKey:@"task_name"]];
            rewardEntity.reward_amount = [self checkJson:[tasksArray[i] valueForKey:@"reward_amount"]];
            rewardEntity.participate_status = [self checkJson:[tasksArray[i] valueForKey:@"participate_status"]];
            [entity.history addObject:rewardEntity];
        }
    }
    return entity;
}




// RB-标签模块
+ (NSMutableArray *)getRBTagListEntity:(id)jsonObject
                          andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"tags"];
    NSArray *nameArray = [resultArray valueForKey:@"name"];
    for (int i = 0; i < [nameArray count]; i++) {
        @autoreleasepool {
            RBTagEntity *entity = [[RBTagEntity alloc] init];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.label = [self checkJson:[resultArray[i] valueForKey:@"label"]];
            entity.cover_url = [self checkJson:[resultArray[i] valueForKey:@"cover_url"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-大V模块
+ (NSMutableArray *)getRBUserInfoBigVListEntity:(id)jsonObject
                                   andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"kol_identity_prices"];
    NSArray *nameArray = [resultArray valueForKey:@"name"];
    for (int i = 0; i < [nameArray count]; i++) {
        @autoreleasepool {
            RBUserBigVEntity *entity = [[RBUserBigVEntity alloc] init];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.provider = [self checkJson:[resultArray[i] valueForKey:@"provider"]];
            entity.follower_count = [self checkJson:[resultArray[i] valueForKey:@"follower_count"]];
            entity.belong_field = [self checkJson:[resultArray[i] valueForKey:@"belong_field"]];
            entity.headline_price = [self checkJson:[resultArray[i] valueForKey:@"headline_price"]];
            entity.second_price = [self checkJson:[resultArray[i] valueForKey:@"second_price"]];
            entity.single_price = [self checkJson:[resultArray[i] valueForKey:@"single_price"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}


// RB-收入模块
+ (RBAmountEntity *)getRBAmountEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"kol"];
    RBAmountEntity *entity = [[RBAmountEntity alloc] init];
    entity.amount  =
    [self checkJson:[resultArray valueForKey:@"amount"]];
    entity.frozen_amount  =
    [self checkJson:[resultArray valueForKey:@"frozen_amount"]];
    entity.avail_amount  =
    [self checkJson:[resultArray valueForKey:@"avail_amount"]];
    entity.total_income  =
    [self checkJson:[resultArray valueForKey:@"total_income"]];
    entity.total_withdraw  =
    [self checkJson:[resultArray valueForKey:@"total_withdraw"]];
    entity.today_income  =
    [self checkJson:[resultArray valueForKey:@"today_income"]];
    entity.total_expense  =
    [self checkJson:[resultArray valueForKey:@"total_expense"]];
    entity.verifying_income  =
    [self checkJson:[resultArray valueForKey:@"verifying_income"]];
    //
    entity.stats = [NSMutableArray new];
    NSArray *statsArray = [jsonObject objectForKey:@"stats"];
    NSArray *dateArray = [statsArray valueForKey:@"date"];
    for (int i = 0; i < [dateArray count]; i++) {
        @autoreleasepool {
            RBAmountEntity *statsEntity = [[RBAmountEntity alloc] init];
            statsEntity.date = [self checkJson:[statsArray[i] valueForKey:@"date"]];
            statsEntity.total_amount = [self checkJson:[statsArray[i] valueForKey:@"total_amount"]];
            statsEntity.count = [self checkJson:[statsArray[i] valueForKey:@"count"]];
            [entity.stats addObject:statsEntity];
        }
    }
    return entity;
}

// RB-收入模块-账单列表
+ (NSMutableArray *)getRBIncomeListEntity:(id)jsonObject
                             andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"transactions"];
    NSArray *creditsArray = [resultArray valueForKey:@"credits"];
    for (int i = 0; i < [creditsArray count]; i++) {
        @autoreleasepool {
            RBIncomeEntity *entity = [[RBIncomeEntity alloc] init];
            entity.credits = [self checkJson:[resultArray[i] valueForKey:@"credits"]];
            entity.amount = [self checkJson:[resultArray[i] valueForKey:@"amount"]];
            entity.avail_amount = [self checkJson:[resultArray[i] valueForKey:@"avail_amount"]];
            entity.frozen_amount = [self checkJson:[resultArray[i] valueForKey:@"frozen_amount"]];
            entity.subject = [self checkJson:[resultArray[i] valueForKey:@"subject"]];
            entity.direct = [self checkJson:[resultArray[i] valueForKey:@"direct"]];
            entity.created_at = [self checkJson:[resultArray[i] valueForKey:@"created_at"]];
            entity.title = [self checkJson:[resultArray[i] valueForKey:@"title"]];
            entity.pay_way = [self checkJson:[resultArray[i] valueForKey:@"pay_way"]];
            entity.direct_text = [self checkJson:[resultArray[i] valueForKey:@"direct_text"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-收入模块-积分列表
+ (NSMutableArray *)getRBCreditListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray{
    NSArray * resultArray = [jsonObject objectForKey:@"credits"];
    for (int i = 0; i < [resultArray count]; i ++) {
        @autoreleasepool{
            RBCreditEntity * entity = [[RBCreditEntity alloc]init];
            entity.amount = [self checkJson:[resultArray[i] objectForKey:@"amount"]];
            entity.direct = [self checkJson:[resultArray[i] objectForKey:@"direct"]];
            entity.remark = [self checkJson:[resultArray[i] objectForKey:@"remark"]];
            entity.score = [self checkJson:[resultArray[i] objectForKey:@"score"]];
            entity.show_time = [self checkJson:[resultArray[i] objectForKey:@"show_time"]];
            entity.trade_no = [self checkJson:[resultArray[i] objectForKey:@"trade_no"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-通知模块
+ (NSMutableArray *)getRBNotificationListEntity:(id)jsonObject
                                   andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"messages"];
    NSArray *idArray = [resultArray valueForKey:@"id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBNotificationEntity *entity = [[RBNotificationEntity alloc] init];
            entity.iid = [self checkJson:[resultArray[i] valueForKey:@"id"]];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.title = [self checkJson:[resultArray[i] valueForKey:@"title"]];
            entity.message_type = [self checkJson:[resultArray[i] valueForKey:@"message_type"]];
            entity.is_read = [self checkJson:[resultArray[i] valueForKey:@"is_read"]];
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            entity.logo_url = [self checkJson:[resultArray[i] valueForKey:@"logo_url"]];
            entity.sender = [self checkJson:[resultArray[i] valueForKey:@"sender"]];
            entity.item_id = [self checkJson:[resultArray[i] valueForKey:@"item_id"]];
            entity.desc = [self checkJson:[resultArray[i] valueForKey:@"desc"]];
            entity.read_at = [self checkJson:[resultArray[i] valueForKey:@"read_at"]];
            entity.created_at = [self checkJson:[resultArray[i] valueForKey:@"created_at"]];
            entity.sub_message_type = [self checkJson:[resultArray[i] valueForKey:@"sub_message_type"]];
            entity.sub_sub_message_type = [self checkJson:[resultArray[i] valueForKey:@"sub_sub_message_type"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-第三方绑定模块
+ (NSMutableArray *)getRBThirdListEntity:(id)jsonObject
                            andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"identities"];
    if([resultArray count]==0) {
        return backArray;
    }
    NSArray *uidArray = [resultArray valueForKey:@"uid"];
    for (int i = 0; i < [uidArray count]; i++) {
        @autoreleasepool {
            RBThirdEntity *entity = [[RBThirdEntity alloc] init];
            entity.provider = [self checkJson:[resultArray[i] valueForKey:@"provider"]];
            entity.uid = [self checkJson:[resultArray[i] valueForKey:@"uid"]];
            entity.token = [self checkJson:[resultArray[i] valueForKey:@"token"]];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            entity.avatar_url = [self checkJson:[resultArray[i] valueForKey:@"avatar_url"]];
            entity.desc = [self checkJson:[resultArray[i] valueForKey:@"desc"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}


// RB-用户信息
// RB-保存用户RBUserEntity
+(void)setRBUserEntityWith:(id)jsonObject {
    [Utils getFileWrite:jsonObject andFileName:@"Login" andFilePath:@"RB"];
}

// RB-读取用户RBUserEntity
+(RBUserEntity*)getRBUserEntity {
    RBUserEntity*userEntity = [JsonService getRBUserEntity:[Utils getFileReadWithFileName:@"Login" andFilePath:@"RB"]];
    return userEntity;
}


// RB-消息模块
+ (RBPushEntity *)getRBPushEntity:(id)jsonObject {
    RBPushEntity *entity = [[RBPushEntity alloc] init];
    entity.action = [self checkJson:[jsonObject objectForKey:@"action"]];
    entity.income = [self checkJson:[jsonObject objectForKey:@"new_income"]];
    entity.title = [self checkJson:[jsonObject objectForKey:@"title"]];
    entity.unread_message_count = [self checkJson:[jsonObject objectForKey:@"unread_message_count"]];
    entity.name = [self checkJson:[jsonObject objectForKey:@"name"]];
    return entity;
}

// RB-文章模块
+ (NSMutableArray *)getRBArticleActionsListEntity:(id)jsonObject
                                     andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"article_actions"];
    NSArray *idArray = [resultArray valueForKey:@"article_id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBArticleEntity *entity = [[RBArticleEntity alloc] init];
            entity.article_id = [self checkJson:[resultArray[i] valueForKey:@"article_id"]];
            entity.article_title = [self checkJson:[resultArray[i] valueForKey:@"article_title"]];
            entity.article_url = [self checkJson:[resultArray[i] valueForKey:@"article_url"]];
            entity.article_avatar_url = [self checkJson:[resultArray[i] valueForKey:@"article_avatar_url"]];
            entity.article_author = [self checkJson:[resultArray[i] valueForKey:@"article_author"]];
            entity.article_read = [self checkJson:[resultArray[i] valueForKey:@"look"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-文章模块
+ (NSMutableArray *)getRBArticleActionsForwardListEntity:(id)jsonObject
                                            andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"article_actions"];
    NSArray *idArray = [resultArray valueForKey:@"article_id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBArticleEntity *entity = [[RBArticleEntity alloc] init];
            entity.article_id = [self checkJson:[resultArray[i] valueForKey:@"article_id"]];
            entity.article_title = [self checkJson:[resultArray[i] valueForKey:@"article_title"]];
            entity.article_url = [self checkJson:[resultArray[i] valueForKey:@"article_url"]];
            entity.article_avatar_url = [self checkJson:[resultArray[i] valueForKey:@"article_avatar_url"]];
            entity.article_author = [self checkJson:[resultArray[i] valueForKey:@"article_author"]];
            entity.article_read = [self checkJson:[resultArray[i] valueForKey:@"look"]];
            entity.read_count = [self checkJson:[resultArray[i] valueForKey:@"read_count"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

+ (void)getUserUpateDateCheck {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    if([LocalService getRBLocalDataUserUpateDate].length==0){
        [LocalService setRBLocalDataUserUpateDateWith:[dateFormatter stringFromDate:[NSDate date]]];
    }
    //
    NSString*upateStr = [LocalService getRBLocalDataUserUpateDate];
    NSDate *nowDate = [NSDate date];
    NSDate *upateDate = [dateFormatter dateFromString:upateStr];
    NSTimeInterval secondsInterval= [nowDate timeIntervalSinceDate:upateDate];
    if(secondsInterval>3*24*60*60){
        NSLog(@"清空数据库");
        [ArticleEntityManager deleteCoreDataArticleEntity_All];
        [LocalService setRBLocalDataUserUpateDateWith:[dateFormatter stringFromDate:[NSDate date]]];
    }
}

+ (NSMutableArray *)getRBArticleListEntity:(id)jsonObject
                        andBackArrayNormal:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"articles"];
    NSArray *idArray = [resultArray valueForKey:@"article_id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBArticleEntity *entity = [[RBArticleEntity alloc] init];
            entity.article_id = [self checkJson:[resultArray[i] valueForKey:@"article_id"]];
            entity.article_title = [self checkJson:[resultArray[i] valueForKey:@"article_title"]];
            entity.article_url = [self checkJson:[resultArray[i] valueForKey:@"article_url"]];
            entity.article_avatar_url = [self checkJson:[resultArray[i] valueForKey:@"article_avatar_url"]];
            entity.article_author = [self checkJson:[resultArray[i] valueForKey:@"article_author"]];
            entity.article_read =
            [self checkJson:[resultArray[i] valueForKey:@"article_read"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-文章模块
+ (NSMutableArray *)getRBArticleListEntity:(id)jsonObject
                           insertBackArray:(NSMutableArray *)backArray {
    for (int i = 0; i < [backArray count]; i++) {
        RBArticleEntity *entity = backArray[i];
        entity.article_refresh = @"0";
        [backArray replaceObjectAtIndex:i withObject:entity];
    }
    //
    NSArray *resultArray = [jsonObject objectForKey:@"articles"];
    NSArray *idArray = [resultArray valueForKey:@"article_id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBArticleEntity *entity = [[RBArticleEntity alloc] init];
            entity.article_id = [self checkJson:[resultArray[i] valueForKey:@"article_id"]];
            entity.article_title = [self checkJson:[resultArray[i] valueForKey:@"article_title"]];
            entity.article_url = [self checkJson:[resultArray[i] valueForKey:@"article_url"]];
            entity.article_avatar_url = [self checkJson:[resultArray[i] valueForKey:@"article_avatar_url"]];
            entity.article_author = [self checkJson:[resultArray[i] valueForKey:@"article_author"]];
            entity.article_read =
            [self checkJson:[resultArray[i] valueForKey:@"article_read"]];
            if([backArray count]>0) {
                if(i == 0) {
                    entity.article_refresh = @"1";
                }
                [backArray insertObject:entity atIndex:0];
            } else {
                [backArray addObject:entity];
            }
            // 离线数据
            [ArticleEntityManager addCoreDataArticleEntity:entity];
        }
    }
    // 防止数据过多,内存报警
    //    while ([backArray count]>50) {
    //        [backArray removeLastObject];
    //    }
    return backArray;
}

+ (NSMutableArray *)getRBArticleListEntity:(id)jsonObject
                              andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"articles"];
    NSArray *idArray = [resultArray valueForKey:@"article_id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBArticleEntity *entity = [[RBArticleEntity alloc] init];
            entity.article_id = [self checkJson:[resultArray[i] valueForKey:@"article_id"]];
            entity.article_title = [self checkJson:[resultArray[i] valueForKey:@"article_title"]];
            entity.article_url = [self checkJson:[resultArray[i] valueForKey:@"article_url"]];
            entity.article_avatar_url = [self checkJson:[resultArray[i] valueForKey:@"article_avatar_url"]];
            entity.article_author = [self checkJson:[resultArray[i] valueForKey:@"article_author"]];
            entity.article_read =
            [self checkJson:[resultArray[i] valueForKey:@"article_read"]];
            [backArray addObject:entity];
            // 离线数据
            [ArticleEntityManager addCoreDataArticleEntity:entity];
        }
    }
    // 防止数据过多,内存报警
    //    while ([backArray count]>50) {
    //        [backArray removeObjectAtIndex:0];
    //    }
    return backArray;
}

+ (NSMutableArray *)getRBEngineListEntity:(id)jsonObject
                             andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"hot_items"];
    NSArray *idArray = [resultArray valueForKey:@"id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBArticleEntity *entity = [[RBArticleEntity alloc] init];
            entity.article_id = [self checkJson:[resultArray[i] valueForKey:@"id"]];
            entity.article_title = [self checkJson:[resultArray[i] valueForKey:@"title"]];
            entity.article_url = [self checkJson:[resultArray[i] valueForKey:@"read_url"]];
            entity.article_publish_at = [self checkJson:[resultArray[i] valueForKey:@"publish_at"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

+ (RBNewInfluenceEntity*)getRBNewInfluenceEntity:(id)jsonObject{
    RBNewInfluenceEntity * entity = [[RBNewInfluenceEntity alloc]init];
    entity.avatar_url = [self checkJson:[jsonObject objectForKey:@"avatar_url"]];
    entity.avg_comments = [self checkJson:[jsonObject objectForKey:@"avg_comments"]];
    entity.avg_posts = [self checkJson:[jsonObject objectForKey:@"avg_posts"]];
    entity.avg_likes = [self checkJson:[jsonObject objectForKey:@"avg_likes"]];
    entity.calculated = [self checkJson:[jsonObject objectForKey:@"calculated"]];
    entity.calculated_date = [self checkJson:[jsonObject objectForKey:@"calculated_date"]];
    entity.Idescription = [self checkJson:[jsonObject objectForKey:@"description"]];
    entity.influence_level = [self checkJson:[jsonObject objectForKey:@"influence_level"]];
    entity.influence_score = [self checkJson:[jsonObject objectForKey:@"influence_score"]];
    entity.influence_score_percentile = [self checkJson:[jsonObject objectForKey:@"influence_score_percentile"]];
    entity.name = [self checkJson:[jsonObject objectForKey:@"name"]];
    entity.provider = [self checkJson:[jsonObject objectForKey:@"provider"]];
    entity.industries = [NSMutableArray new];
    entity.similar_kols = [NSMutableArray new];
    entity.influence_score_visibility = [self checkJson:[jsonObject objectForKey:@"influence_score_visibility"]];
    NSArray * arr = [jsonObject objectForKey:@"industries"];
    NSArray * arr1 = [jsonObject objectForKey:@"similar_kols"];
    for (NSInteger i = 0; i<arr.count; i++) {
        @autoreleasepool {
            RBNewIndustriyEntity * Ientity = [[RBNewIndustriyEntity alloc]init];
            Ientity.avg_comments = [self checkJson:[arr[i] objectForKey:@"avg_comments"]];
            Ientity.avg_likes = [self checkJson:[arr[i] objectForKey:@"avg_likes"]];
            Ientity.avg_posts = [self checkJson:[arr[i] objectForKey:@"avg_posts"]];
            Ientity.industry_name = [self checkJson:[arr[i] objectForKey:@"industry_name"]];
            Ientity.industry_score = [self checkJson:[arr[i] objectForKey:@"industry_score"]];
            [entity.industries addObject:Ientity];
        }
    }
    for (NSInteger i = 0; i<arr1.count; i++) {
        @autoreleasepool {
            RBSimilarEntity * SEntity = [[RBSimilarEntity alloc]init];
            SEntity.iid = [self checkJson:[arr1[i] objectForKey:@"id"]];
            SEntity.avatar_url = [self checkJson:[arr1[i] objectForKey:@"avatar_url"]];
            [entity.similar_kols addObject:SEntity];
        }
    }
    return entity;
}
// RB-文章模块阅读
+ (RBArticleActionEntity *)getRBArticleActionEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"article_action"];
    RBArticleActionEntity *entity = [[RBArticleActionEntity alloc] init];
    entity.iid  =
    [self checkJson:[resultArray valueForKey:@"id"]];
    entity.article_id  =
    [self checkJson:[resultArray valueForKey:@"article_id"]];
    entity.article_title  =
    [self checkJson:[resultArray valueForKey:@"article_title"]];
    entity.article_url  =
    [self checkJson:[resultArray valueForKey:@"article_url"]];
    entity.article_avatar_url  =
    [self checkJson:[resultArray valueForKey:@"article_avatar_url"]];
    entity.article_author  =
    [self checkJson:[resultArray valueForKey:@"article_author"]];
    entity.look  =
    [self checkJson:[resultArray valueForKey:@"look"]];
    entity.forward  =
    [self checkJson:[resultArray valueForKey:@"forward"]];
    entity.collect  =
    [self checkJson:[resultArray valueForKey:@"collect"]];
    entity.like  =
    [self checkJson:[resultArray valueForKey:@"like"]];
    entity.share_url  =
    [self checkJson:[resultArray valueForKey:@"share_url"]];
    return entity;
}


// RB-社交模块
// RB-社交模块-影响力
+ (RBInfluenceEntity *)getRBInfluenceEntity:(id)jsonObject {
    RBInfluenceEntity *entity = [[RBInfluenceEntity alloc] init];
    entity.rank_index = [self checkJson:[jsonObject objectForKey:@"rank_index"]];
    entity.joined_count = [self checkJson:[jsonObject objectForKey:@"joined_count"]];
    NSArray *kolArray = [jsonObject objectForKey:@"kol_value"];
    entity.cal_time = [self checkJson:[kolArray valueForKey:@"cal_time"]];
    entity.influence_level = [self checkJson:[kolArray valueForKey:@"influence_level"]];
    entity.influence_score = [self checkJson:[kolArray valueForKey:@"influence_score"]];
    entity.name = [self checkJson:[kolArray valueForKey:@"name"]];
    entity.avatar_url = [self checkJson:[kolArray valueForKey:@"avatar_url"]];
    entity.last_influence_score = [self checkJson:[kolArray valueForKey:@"last_influence_score"]];
    entity.contacts = [NSMutableArray new];
    NSArray *contactsArray = [jsonObject objectForKey:@"contacts"];
    NSArray *nameArray = [contactsArray valueForKey:@"name"];
    for (int i = 0; i < [nameArray count]; i++) {
        @autoreleasepool {
            RBContactEntity *contactEntity = [[RBContactEntity alloc] init];
            contactEntity.mobile = [self checkJson:[contactsArray[i] valueForKey:@"mobile"]];
            contactEntity.name = [self checkJson:[contactsArray[i] valueForKey:@"name"]];
            contactEntity.influence_score = [self checkJson:[contactsArray[i] valueForKey:@"influence_score"]];
            contactEntity.joined = [self checkJson:[contactsArray[i] valueForKey:@"joined"]];
            contactEntity.invite_status = [self checkJson:[contactsArray[i] valueForKey:@"invite_status"]];
            [entity.contacts addObject:contactEntity];
        }
    }
    return entity;
}

// RB-社交模块-影响力
+ (NSMutableArray *)getRBInfluenceEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray *contactsArray = [jsonObject objectForKey:@"contacts"];
    NSArray *nameArray = [contactsArray valueForKey:@"name"];
    for (int i = 0; i < [nameArray count]; i++) {
        @autoreleasepool {
            RBContactEntity *contactEntity = [[RBContactEntity alloc] init];
            contactEntity.mobile = [self checkJson:[contactsArray[i] valueForKey:@"mobile"]];
            contactEntity.name = [self checkJson:[contactsArray[i] valueForKey:@"name"]];
            contactEntity.influence_score = [self checkJson:[contactsArray[i] valueForKey:@"influence_score"]];
            contactEntity.joined = [self checkJson:[contactsArray[i] valueForKey:@"joined"]];
            contactEntity.invite_status = [self checkJson:[contactsArray[i] valueForKey:@"invite_status"]];
            [backArray addObject:contactEntity];
        }
    }
    return backArray;
}


// RB-社交模块-影响力解读-标签云
+ (RBInfluenceReadEntity *)getRBInfluenceReadEntity:(id)jsonObject and:(RBInfluenceReadEntity *)entity {
    entity.weibo_identity_status = [self checkJson:[jsonObject objectForKey:@"weibo_identity_status"]];
    if (![entity.weibo_identity_status isEqualToString:@"active"]) {
        entity.wordclouds = [NSMutableArray new];
        entity.categories = [NSMutableArray new];
        return entity;
    }
    entity.wordclouds = [jsonObject objectForKey:@"wordclouds"];
    entity.categories = [NSMutableArray new];
    NSArray *resultArray = [jsonObject objectForKey:@"categories"];
    NSArray *textArray = [resultArray valueForKey:@"text"];
    for (int i = 0; i < [textArray count]; i++) {
        @autoreleasepool {
            RBRankingTagsEntity *tEntity = [[RBRankingTagsEntity alloc] init];
            tEntity.text = [self checkJson:[resultArray[i] valueForKey:@"text"]];
            tEntity.weight = [self checkJson:[resultArray[i] valueForKey:@"weight"]];
            [entity.categories addObject:tEntity];
        }
    }
    return entity;
}

// RB-社交模块-影响力解读
+ (RBInfluenceReadEntity *)getRBInfluenceReadEntity:(id)jsonObject {
    RBInfluenceReadEntity *entity = [[RBInfluenceReadEntity alloc] init];
    NSArray *item_rate = [jsonObject objectForKey:@"item_rate"];
    entity.diff_score = [self checkJson:[jsonObject objectForKey:@"diff_score"]];
    entity.feature_rate = [self checkJson:[item_rate valueForKey:@"feature_rate"]];
    entity.active_rate = [self checkJson:[item_rate valueForKey:@"active_rate"]];
    entity.campaign_rate = [self checkJson:[item_rate valueForKey:@"campaign_rate"]];
    entity.contact_rate = [self checkJson:[item_rate valueForKey:@"contact_rate"]];
    entity.share_rate = [self checkJson:[item_rate valueForKey:@"share_rate"]];
    //
    NSArray *item_score = [jsonObject objectForKey:@"item_score"];
    entity.feature_score = [self checkJson:[item_score valueForKey:@"feature_score"]];
    entity.active_score = [self checkJson:[item_score valueForKey:@"active_score"]];
    entity.campaign_score = [self checkJson:[item_score valueForKey:@"campaign_score"]];
    entity.share_score = [self checkJson:[item_score valueForKey:@"share_score"]];
    entity.contact_score = [self checkJson:[item_score valueForKey:@"contact_score"]];
    //
    entity.contact_count = [self checkJson:[jsonObject objectForKey:@"contact_count"]];
    entity.rank_index = [self checkJson:[jsonObject objectForKey:@"rank_index"]];
    entity.foward_read_count = [self checkJson:[jsonObject objectForKey:@"foward_read_count"]];
    entity.history = [NSMutableArray new];
    NSArray *historyArray = [jsonObject objectForKey:@"history"];
    NSArray *dateArray = [historyArray valueForKey:@"date"];
    for (int i = 0; i < [dateArray count]; i++) {
        @autoreleasepool {
            RBInfluenceHistoryEntity *historyEntity = [[RBInfluenceHistoryEntity alloc] init];
            historyEntity.date = [self checkJson:[historyArray[i] valueForKey:@"date"]];
            historyEntity.score = [self checkJson:[historyArray[i] valueForKey:@"score"]];
            [entity.history addObject:historyEntity];
        }
    }
    
    NSArray *kol_valueArray = [jsonObject objectForKey:@"kol_value"];
    RBInfluenceUpgradeEntity *influenceEntity = [[RBInfluenceUpgradeEntity alloc] init];
    influenceEntity.kol_uuid = [self checkJson:[kol_valueArray valueForKey:@"kol_uuid"]];
    influenceEntity.influence_level = [self checkJson:[kol_valueArray valueForKey:@"influence_level"]];
    influenceEntity.name = [self checkJson:[kol_valueArray valueForKey:@"name"]];
    influenceEntity.avatar_url = [self checkJson:[kol_valueArray valueForKey:@"avatar_url"]];
    influenceEntity.influence_score = [self checkJson:[kol_valueArray valueForKey:@"influence_score"]];
    influenceEntity.cal_time = [self checkJson:[kol_valueArray valueForKey:@"cal_time"]];
    entity.influenceEntity = influenceEntity;
    
    entity.identities = [NSMutableArray new];
    NSArray *identitiesArray = [jsonObject objectForKey:@"identities"];
    NSArray *providerArray = [identitiesArray valueForKey:@"provider"];
    for (int i = 0; i < [providerArray count]; i++) {
        @autoreleasepool {
            RBThirdEntity *tEntity = [[RBThirdEntity alloc] init];
            tEntity.provider = [self checkJson:[identitiesArray[i] valueForKey:@"provider"]];
            tEntity.uid = [self checkJson:[identitiesArray[i] valueForKey:@"uid"]];
            tEntity.token = [self checkJson:[identitiesArray[i] valueForKey:@"token"]];
            tEntity.name = [self checkJson:[identitiesArray[i] valueForKey:@"name"]];
            tEntity.url = [self checkJson:[identitiesArray[i] valueForKey:@"url"]];
            tEntity.avatar_url = [self checkJson:[identitiesArray[i] valueForKey:@"avatar_url"]];
            tEntity.desc = [self checkJson:[identitiesArray[i] valueForKey:@"desc"]];
            [entity.identities addObject:tEntity];
        }
    }
    
    return entity;
}

// RB-社交模块-影响力提升
+ (RBInfluenceUpgradeEntity *)getRBInfluenceUpgradeEntity:(id)jsonObject {
    NSArray *kol_value = [jsonObject objectForKey:@"kol_value"];
    RBInfluenceUpgradeEntity *entity = [[RBInfluenceUpgradeEntity alloc] init];
    entity.kol_uuid = [self checkJson:[kol_value valueForKey:@"kol_uuid"]];
    entity.influence_level = [self checkJson:[kol_value valueForKey:@"influence_level"]];
    entity.name = [self checkJson:[kol_value valueForKey:@"name"]];
    entity.avatar_url = [self checkJson:[kol_value valueForKey:@"avatar_url"]];
    entity.influence_score = [self checkJson:[kol_value valueForKey:@"influence_score"]];
    entity.rank_index = [self checkJson:[jsonObject objectForKey:@"rank_index"]];
    entity.cal_time = [self checkJson:[kol_value valueForKey:@"cal_time"]];
    NSArray *upgrade_info = [jsonObject objectForKey:@"upgrade_info"];
    entity.campaign_count = [self checkJson:[upgrade_info valueForKey:@"campaign_count"]];
    entity.identity_count = [self checkJson:[upgrade_info valueForKey:@"identity_count"]];
    entity.has_contacts = [self checkJson:[upgrade_info valueForKey:@"has_contacts"]];
    return entity;
}


// RB-QA模块
+ (NSMutableArray *)getRBQAListEntity:(id)jsonObject
                         andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"notices"];
    NSArray *questionArray = [resultArray valueForKey:@"question"];
    for (int i = 0; i < [questionArray count]; i++) {
        @autoreleasepool {
            RBQAEntity *entity = [[RBQAEntity alloc] init];
            entity.question = [self checkJson:[resultArray[i] valueForKey:@"question"]];
            entity.answer = [self checkJson:[resultArray[i] valueForKey:@"answer"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-R榜模块-微博
+ (NSMutableArray *)getRBRankingListWeiboEntity:(id)jsonObject
                                   andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"weibo"];
    NSArray *kol_idArray = [resultArray valueForKey:@"kol_id"];
    for (int i = 0; i < [kol_idArray count]; i++) {
        @autoreleasepool {
            RBRankingWeiboEntity *entity = [[RBRankingWeiboEntity alloc] init];
            entity.biz_code = nil;
            entity.kol_id = [self checkJson:[resultArray[i] valueForKey:@"kol_id"]];
            entity.kol_name = [self checkJson:[resultArray[i] valueForKey:@"kol_name"]];
            entity.fans = [self checkJson:[resultArray[i] valueForKey:@"fans"]];
            entity.gender = [self checkJson:[resultArray[i] valueForKey:@"gender"]];
            entity.kol_avatar_url =
            [self checkJson:[resultArray[i] valueForKey:@"kol_avatar_url"]];
            entity.kol_category  = [resultArray[i] valueForKey:@"kol_category"];
            entity.kol_keywords  = [resultArray[i] valueForKey:@"kol_keywords"];
            entity.kol_brands  = [resultArray[i] valueForKey:@"kol_brands"];
            entity.kol_influence_score = [self checkJson:[resultArray[i] valueForKey:@"kol_influence_score"]];
            entity.kol_info = [self checkJson:[resultArray[i] valueForKey:@"kol_info"]];
            entity.posts = [self checkJson:[resultArray[i] valueForKey:@"posts"]];
            entity.verify = [self checkJson:[resultArray[i] valueForKey:@"verify"]];
            entity.kol_relative = [self checkJson:[resultArray[i] valueForKey:@"kol_relative"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-R榜模块-微博详情
+ (RBRankingWeiboEntity *)getRBRankingDetailWeiboEntity:(id)jsonObject {
    RBRankingWeiboEntity *entity = [[RBRankingWeiboEntity alloc] init];
    entity.biz_code = nil;
    entity.kol_id = [self checkJson:[jsonObject objectForKey:@"kol_id"]];
    entity.kol_name = [self checkJson:[jsonObject objectForKey:@"kol_name"]];
    entity.fans = [self checkJson:[jsonObject objectForKey:@"fans"]];
    entity.gender = [self checkJson:[jsonObject objectForKey:@"gender"]];
    entity.kol_avatar_url =
    [self checkJson:[jsonObject objectForKey:@"kol_avatar_url"]];
    
    entity.kol_category  = [jsonObject objectForKey:@"kol_category"];
    entity.kol_keywords  = [jsonObject objectForKey:@"kol_keywords"];
    entity.kol_brands  = [jsonObject objectForKey:@"kol_brands"];
    
    entity.kol_info = [self checkJson:[jsonObject objectForKey:@"kol_info"]];
    entity.posts = [self checkJson:[jsonObject objectForKey:@"posts"]];
    entity.verify = [self checkJson:[jsonObject objectForKey:@"verify"]];
    return entity;
}


// RB-R榜模块-微信
+ (NSMutableArray *)getRBRankingListWechatEntity:(id)jsonObject
                                    andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"wechat"];
    NSArray *kol_idArray = [resultArray valueForKey:@"kol_id"];
    for (int i = 0; i < [kol_idArray count]; i++) {
        @autoreleasepool {
            RBRankingWeiboEntity *entity = [[RBRankingWeiboEntity alloc] init];
            entity.biz_code = [self checkJson:[resultArray[i] valueForKey:@"biz_code"]];
            entity.kol_id = [self checkJson:[resultArray[i] valueForKey:@"kol_id"]];
            entity.kol_name = [self checkJson:[resultArray[i] valueForKey:@"kol_name"]];
            entity.fans = [self checkJson:[resultArray[i] valueForKey:@"fans"]];
            entity.gender = [self checkJson:[resultArray[i] valueForKey:@"gender"]];
            entity.kol_avatar_url =
            [self checkJson:[resultArray[i] valueForKey:@"kol_avatar_url"]];
            entity.kol_category  = [resultArray[i] valueForKey:@"kol_category"];
            entity.kol_keywords  = [resultArray[i] valueForKey:@"kol_keywords"];
            entity.kol_brands  = [resultArray[i] valueForKey:@"kol_brands"];
            entity.kol_influence_score = [self checkJson:[resultArray[i] valueForKey:@"kol_influence_score"]];
            entity.kol_info = [self checkJson:[resultArray[i] valueForKey:@"kol_info"]];
            entity.posts = [self checkJson:[resultArray[i] valueForKey:@"posts"]];
            entity.verify = [self checkJson:[resultArray[i] valueForKey:@"verify"]];
            entity.kol_relative = [self checkJson:[resultArray[i] valueForKey:@"kol_relative"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-R榜模块-微信详情
+ (RBRankingWeiboEntity *)getRBRankingDetailWechatEntity:(id)jsonObject {
    RBRankingWeiboEntity *entity = [[RBRankingWeiboEntity alloc] init];
    entity.biz_code = [self checkJson:[jsonObject objectForKey:@"biz_code"]];
    entity.kol_id = [self checkJson:[jsonObject objectForKey:@"kol_id"]];
    entity.kol_name = [self checkJson:[jsonObject objectForKey:@"kol_name"]];
    entity.fans = [self checkJson:[jsonObject objectForKey:@"fans"]];
    entity.gender = [self checkJson:[jsonObject objectForKey:@"gender"]];
    entity.kol_avatar_url =
    [self checkJson:[jsonObject objectForKey:@"kol_avatar_url"]];
    entity.kol_category  = [jsonObject objectForKey:@"kol_category"];
    entity.kol_keywords  = [jsonObject objectForKey:@"kol_keywords"];
    entity.kol_brands  = [jsonObject objectForKey:@"kol_brands"];
    entity.kol_info = [self checkJson:[jsonObject objectForKey:@"kol_info"]];
    entity.posts = [self checkJson:[jsonObject objectForKey:@"posts"]];
    entity.verify = [self checkJson:[jsonObject objectForKey:@"verify"]];
    return entity;
}

// RB-R榜模块-标签云
+ (NSMutableArray *)getRBRankingTagsEntity:(id)jsonObject
                              andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"tags"];
    NSArray *textArray = [resultArray valueForKey:@"text"];
    for (int i = 0; i < [textArray count]; i++) {
        @autoreleasepool {
            RBRankingTagsEntity *entity = [[RBRankingTagsEntity alloc] init];
            entity.text = [self checkJson:[resultArray[i] valueForKey:@"text"]];
            entity.weight = [self checkJson:[resultArray[i] valueForKey:@"weight"]];
            [backArray addObject:entity.text];
        }
    }
    return backArray;
}

// RB-R榜模块-雷达图
+ (NSMutableArray *)getRBRankingCategoriesEntity:(id)jsonObject
                                    andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"categories"];
    NSArray *textArray = [resultArray valueForKey:@"text"];
    for (int i = 0; i < [textArray count]; i++) {
        @autoreleasepool {
            RBRankingTagsEntity *entity = [[RBRankingTagsEntity alloc] init];
            entity.text = [self checkJson:[resultArray[i] valueForKey:@"text"]];
            entity.weight = [self checkJson:[resultArray[i] valueForKey:@"weight"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-R榜模块-话题图
+ (NSMutableArray *)getRBRankingBoardsEntity:(id)jsonObject
                                andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"result"];
    NSArray *textArray = [resultArray valueForKey:@"text"];
    for (int i = 0; i < [textArray count]; i++) {
        @autoreleasepool {
            RBRankingTagsEntity *entity = [[RBRankingTagsEntity alloc] init];
            entity.text = [self checkJson:[resultArray[i] valueForKey:@"text"]];
            entity.weight = [self checkJson:[resultArray[i] valueForKey:@"weight"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-R榜模块-微博列表
+ (NSMutableArray *)getRBRankingWeiboListEntity:(id)jsonObject
                                   andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"posts"];
    NSArray *kol_nameArray = [resultArray valueForKey:@"kol_name"];
    for (int i = 0; i < [kol_nameArray count]; i++) {
        @autoreleasepool {
            RBRankingWeiboDetailEntity *entity = [[RBRankingWeiboDetailEntity alloc] init];
            entity.comments = [self checkJson:[resultArray[i] valueForKey:@"comments"]];
            entity.device = [self checkJson:[resultArray[i] valueForKey:@"device"]];
            entity.kol_avatar_url = [self checkJson:[resultArray[i] valueForKey:@"kol_avatar_url"]];
            entity.kol_name = [self checkJson:[resultArray[i] valueForKey:@"kol_name"]];
            entity.likes = [self checkJson:[resultArray[i] valueForKey:@"likes"]];
            entity.retweets = [self checkJson:[resultArray[i] valueForKey:@"retweets"]];
            entity.summary = [self checkJson:[resultArray[i] valueForKey:@"summary"]];
            entity.summary = [[NSString stringWithFormat:@"%@",entity.summary]stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            entity.user_type = [self checkJson:[resultArray[i] valueForKey:@"user_type"]];
            entity.verify = [self checkJson:[resultArray[i] valueForKey:@"verify"]];
            entity.msg_cdn_url = nil;
            entity.publish_time = [self checkJson:[resultArray[i] valueForKey:@"publish_time"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-R榜模块-微信列表
+ (NSMutableArray *)getRBRankingWechatListEntity:(id)jsonObject
                                    andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"articles"];
    NSArray *urlArray = [resultArray valueForKey:@"url"];
    for (int i = 0; i < [urlArray count]; i++) {
        @autoreleasepool {
            RBRankingWeiboDetailEntity *entity = [[RBRankingWeiboDetailEntity alloc] init];
            entity.msg_cdn_url = [self checkJson:[resultArray[i] valueForKey:@"msg_cdn_url"]];
            entity.read_num = [self checkJson:[resultArray[i] valueForKey:@"read_num"]];
            entity.like_num = [self checkJson:[resultArray[i] valueForKey:@"like_num"]];
            entity.summary = [self checkJson:[resultArray[i] valueForKey:@"summary"]];
            entity.summary = [[NSString stringWithFormat:@"%@",entity.summary]stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            entity.publish_date = [self checkJson:[resultArray[i] valueForKey:@"publish_date"]];
            entity.title = [self checkJson:[resultArray[i] valueForKey:@"title"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}


// RB-R榜模块-知乎
+ (NSMutableArray *)getRBRankingListZhihuEntity:(id)jsonObject
                                   andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"zhihu"];
    NSArray *kol_idArray = [resultArray valueForKey:@"kol_id"];
    for (int i = 0; i < [kol_idArray count]; i++) {
        @autoreleasepool {
            RBRankingWeiboEntity *entity = [[RBRankingWeiboEntity alloc] init];
            entity.kol_id = [self checkJson:[resultArray[i] valueForKey:@"kol_id"]];
            entity.kol_name = [self checkJson:[resultArray[i] valueForKey:@"kol_name"]];
            entity.fans = [self checkJson:[resultArray[i] valueForKey:@"fans"]];
            entity.gender = [self checkJson:[resultArray[i] valueForKey:@"gender"]];
            entity.kol_avatar_url =
            [self checkJson:[resultArray[i] valueForKey:@"kol_avatar_url"]];
            entity.kol_category  = [resultArray[i] valueForKey:@"kol_category"];
            entity.kol_keywords  = [resultArray[i] valueForKey:@"kol_keywords"];
            entity.kol_brands  = [resultArray[i] valueForKey:@"kol_brands"];
            entity.kol_influence_score = [self checkJson:[resultArray[i] valueForKey:@"kol_influence_score"]];
            entity.kol_info = [self checkJson:[resultArray[i] valueForKey:@"kol_info"]];
            entity.posts = [self checkJson:[resultArray[i] valueForKey:@"posts"]];
            entity.verify = [self checkJson:[resultArray[i] valueForKey:@"verify"]];
            entity.kol_relative = [self checkJson:[resultArray[i] valueForKey:@"kol_relative"]];
            
            entity.ups = [self checkJson:[resultArray[i] valueForKey:@"ups"]];
            entity.thanks = [self checkJson:[resultArray[i] valueForKey:@"thanks"]];
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-R榜模块-知乎列表
+ (NSMutableArray *)getRBRankingZhihuListEntity:(id)jsonObject
                                   andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"result"];
    NSArray *urlArray = [resultArray valueForKey:@"url"];
    for (int i = 0; i < [urlArray count]; i++) {
        @autoreleasepool {
            RBRankingWeiboDetailEntity *entity = [[RBRankingWeiboDetailEntity alloc] init];
            entity.sub_board_name = [self checkJson:[resultArray[i] valueForKey:@"sub_board_name"]];
            entity.board_name = [self checkJson:[resultArray[i] valueForKey:@"board_name"]];
            entity.ups = [self checkJson:[resultArray[i] valueForKey:@"ups"]];
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            entity.pub_time = [self checkJson:[resultArray[i] valueForKey:@"pub_time"]];
            entity.title = [self checkJson:[resultArray[i] valueForKey:@"title"]];
            entity.content = [self checkJson:[resultArray[i] valueForKey:@"content"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}


// RB-R榜模块-美拍
+ (NSMutableArray *)getRBRankingListMeipaiEntity:(id)jsonObject
                                    andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"meipai"];
    NSArray *kol_idArray = [resultArray valueForKey:@"kol_id"];
    for (int i = 0; i < [kol_idArray count]; i++) {
        @autoreleasepool {
            RBRankingWeiboEntity *entity = [[RBRankingWeiboEntity alloc] init];
            entity.kol_id = [self checkJson:[resultArray[i] valueForKey:@"kol_id"]];
            entity.kol_name = [self checkJson:[resultArray[i] valueForKey:@"kol_name"]];
            entity.fans = [self checkJson:[resultArray[i] valueForKey:@"fans"]];
            entity.gender = [self checkJson:[resultArray[i] valueForKey:@"gender"]];
            entity.kol_avatar_url =
            [self checkJson:[resultArray[i] valueForKey:@"kol_avatar_url"]];
            entity.kol_category  = [resultArray[i] valueForKey:@"kol_category"];
            entity.kol_keywords  = [resultArray[i] valueForKey:@"kol_keywords"];
            entity.kol_brands  = [resultArray[i] valueForKey:@"kol_brands"];
            entity.kol_influence_score = [self checkJson:[resultArray[i] valueForKey:@"kol_influence_score"]];
            entity.kol_info = [self checkJson:[resultArray[i] valueForKey:@"kol_info"]];
            entity.posts = [self checkJson:[resultArray[i] valueForKey:@"posts"]];
            entity.verify = [self checkJson:[resultArray[i] valueForKey:@"verify"]];
            entity.kol_relative = [self checkJson:[resultArray[i] valueForKey:@"kol_relative"]];
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            
            entity.kol_sum_like_num_30 = [self checkJson:[resultArray[i] valueForKey:@"kol_sum_like_num_30"]];
            
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-R榜模块-美拍列表
+ (NSMutableArray *)getRBRankingMeipaiListEntity:(id)jsonObject
                                    andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"videos"];
    NSArray *urlArray = [resultArray valueForKey:@"url"];
    for (int i = 0; i < [urlArray count]; i++) {
        @autoreleasepool {
            RBRankingWeiboDetailEntity *entity = [[RBRankingWeiboDetailEntity alloc] init];
            entity.msg_cdn_url = [self checkJson:[resultArray[i] valueForKey:@"msg_cdn_url"]];
            entity.like_num = [self checkJson:[resultArray[i] valueForKey:@"like_num"]];
            entity.ping_num = [self checkJson:[resultArray[i] valueForKey:@"ping_num"]];
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            entity.publish_date = [self checkJson:[resultArray[i] valueForKey:@"publish_date"]];
            entity.title = [self checkJson:[resultArray[i] valueForKey:@"title"]];
            entity.content = [self checkJson:[resultArray[i] valueForKey:@"content"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-R榜模块-秒拍
+ (NSMutableArray *)getRBRankingListMiaopaiEntity:(id)jsonObject
                                     andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"miaopai"];
    NSArray *kol_idArray = [resultArray valueForKey:@"kol_id"];
    for (int i = 0; i < [kol_idArray count]; i++) {
        @autoreleasepool {
            RBRankingWeiboEntity *entity = [[RBRankingWeiboEntity alloc] init];
            entity.kol_id = [self checkJson:[resultArray[i] valueForKey:@"kol_id"]];
            entity.kol_name = [self checkJson:[resultArray[i] valueForKey:@"kol_name"]];
            entity.fans = [self checkJson:[resultArray[i] valueForKey:@"fans"]];
            entity.gender = [self checkJson:[resultArray[i] valueForKey:@"gender"]];
            entity.kol_avatar_url =
            [self checkJson:[resultArray[i] valueForKey:@"kol_avatar_url"]];
            entity.kol_category  = [resultArray[i] valueForKey:@"kol_category"];
            entity.kol_keywords  = [resultArray[i] valueForKey:@"kol_keywords"];
            entity.kol_brands  = [resultArray[i] valueForKey:@"kol_brands"];
            entity.kol_influence_score = [self checkJson:[resultArray[i] valueForKey:@"kol_influence_score"]];
            entity.kol_info = [self checkJson:[resultArray[i] valueForKey:@"kol_info"]];
            entity.posts = [self checkJson:[resultArray[i] valueForKey:@"posts"]];
            entity.verify = [self checkJson:[resultArray[i] valueForKey:@"verify"]];
            entity.kol_relative = [self checkJson:[resultArray[i] valueForKey:@"kol_relative"]];
            
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            
            entity.kol_sum_read_num_30 = [self checkJson:[resultArray[i] valueForKey:@"kol_sum_read_num_30"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-R榜模块-秒拍列表
+ (NSMutableArray *)getRBRankingMiaopaiListEntity:(id)jsonObject
                                     andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"videos"];
    NSArray *urlArray = [resultArray valueForKey:@"url"];
    for (int i = 0; i < [urlArray count]; i++) {
        @autoreleasepool {
            RBRankingWeiboDetailEntity *entity = [[RBRankingWeiboDetailEntity alloc] init];
            entity.msg_cdn_url = [self checkJson:[resultArray[i] valueForKey:@"msg_cdn_url"]];
            entity.like_num = [self checkJson:[resultArray[i] valueForKey:@"like_num"]];
            entity.ping_num = [self checkJson:[resultArray[i] valueForKey:@"ping_num"]];
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            entity.publish_date = [self checkJson:[resultArray[i] valueForKey:@"publish_date"]];
            entity.title = [self checkJson:[resultArray[i] valueForKey:@"title"]];
            entity.content = [self checkJson:[resultArray[i] valueForKey:@"content"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}



// RB-奖励模块-签到
+ (RBUserSignEntity *)getRBUserSignEntity:(id)jsonObject {
    RBUserSignEntity *entity = [[RBUserSignEntity alloc] init];
    entity.continuous_checkin_count = [self checkJson:[jsonObject valueForKey:@"continuous_checkin_count"]];
    entity.today_had_check_in = [self checkJson:[jsonObject valueForKey:@"today_had_check_in"]];
    entity.total_check_in_days = [self checkJson:[jsonObject valueForKey:@"total_check_in_days"]];
    entity.total_check_in_amount = [self checkJson:[jsonObject valueForKey:@"total_check_in_amount"]];
    entity.today_can_amount = [self checkJson:[jsonObject valueForKey:@"today_can_amount"]];
    entity.today_already_amount = [self checkJson:[jsonObject valueForKey:@"today_already_amount"]];
    entity.tomorrow_can_amount = [self checkJson:[jsonObject valueForKey:@"tomorrow_can_amount"]];
    entity.is_show_newbie = [self checkJson:[jsonObject valueForKey:@"is_show_newbie"]];
    entity.red_money_count = [self checkJson:[jsonObject valueForKey:@"red_money_count"]];
    entity.invite_friends = [self checkJson:[jsonObject valueForKey:@"invite_friends"]];
    entity.campaign_invites_count = [self checkJson:[jsonObject valueForKey:@"campaign_invites_count"]];
    NSArray *checkin_history = [jsonObject objectForKey:@"checkin_history"];
    
    entity.checkin_history = checkin_history;
    return entity;
}
// RB-充值界面-充值
+ (RBPromoteEntity *)getRBPromoteEntity:(id)jsonObject{
    
    RBPromoteEntity * entity = [[RBPromoteEntity alloc]init];
    entity.created_at = [self checkJson:[jsonObject valueForKey:@"created_at"]];
    entity.campaignDes = [self checkJson:[jsonObject valueForKey:@"description"]];
    entity.end_at = [self checkJson:[jsonObject valueForKey:@"end_at"]];
    entity.expired_at = [self checkJson:[jsonObject valueForKey:@"expired_at"]];
    entity.ProID = [self checkJson:[jsonObject valueForKey:@"id"]];
    entity.min_credit = [self checkJson:[jsonObject valueForKey:@"min_credit"]];
    entity.rate = [self checkJson:[jsonObject valueForKey:@"rate"]];
    entity.start_at = [self checkJson:[jsonObject valueForKey:@"start_at"]];
    entity.state = [self checkJson:[jsonObject valueForKey:@"state"]];
    entity.title = [self checkJson:[jsonObject valueForKey:@"title"]];
    entity.updated_at = [self checkJson:[jsonObject valueForKey:@"updated_at"]];
    entity.valid_days_count = [self checkJson:[jsonObject valueForKey:@"valid_days_count"]];
    return entity;
}

// RB-分析-列表
+ (NSMutableArray *)getRBAnalysisListEntity:(id)jsonObject
                               andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"identities"];
    NSArray *idArray = [resultArray valueForKey:@"id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBAnalysisListEntity *entity = [[RBAnalysisListEntity alloc] init];
            entity.iid = [self checkJson:[resultArray[i] valueForKey:@"id"]];
            entity.provider = [self checkJson:[resultArray[i] valueForKey:@"provider"]];
            entity.nick_name = [self checkJson:[resultArray[i] valueForKey:@"nick_name"]];
            entity.user_name = [self checkJson:[resultArray[i] valueForKey:@"user_name"]];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.avatar_url = [self checkJson:[resultArray[i] valueForKey:@"avatar_url"]];
            entity.location = [self checkJson:[resultArray[i] valueForKey:@"location"]];
            entity.gender = [self checkJson:[resultArray[i] valueForKey:@"gender"]];
            entity.valid = [self checkJson:[resultArray[i] valueForKey:@"valid"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-分析-微信详情
+ (RBAnalysisWechatEntity *)getRBAnalysisWechatEntity:(id)jsonObject {
    RBAnalysisWechatEntity *entity = [[RBAnalysisWechatEntity alloc] init];
    NSArray *resultArray = [jsonObject objectForKey:@"primary"];
    entity.email = [self checkJson:[resultArray valueForKey:@"email"]];
    entity.nick_name = [self checkJson:[resultArray valueForKey:@"nick_name"]];
    entity.logo_url = [self checkJson:[resultArray valueForKey:@"logo_url"]];
    entity.user_name = [self checkJson:[resultArray valueForKey:@"user_name"]];
    entity.total_followers_count = [self checkJson:[resultArray valueForKey:@"total_followers_count"]];
    entity.nnew_followers_count = [self checkJson:[resultArray valueForKey:@"new_followers_count"]];
    entity.cancel_followers_count = [self checkJson:[resultArray valueForKey:@"cancel_followers_count"]];
    entity.send_message_user_count = [self checkJson:[resultArray valueForKey:@"send_message_user_count"]];
    entity.send_message_count = [self checkJson:[resultArray valueForKey:@"send_message_count"]];
    entity.target_user_count = [self checkJson:[resultArray valueForKey:@"target_user_count"]];
    entity.read_user_count = [self checkJson:[resultArray valueForKey:@"read_user_count"]];
    // WEIBO
    entity.incremental_follower_number = [self checkJson:[resultArray valueForKey:@"incremental_follower_number"]];
    entity.decremental_follower_number = [self checkJson:[resultArray valueForKey:@"decremental_follower_number"]];
    entity.verified_follower_ratio = [self checkJson:[resultArray valueForKey:@"verified_follower_ratio"]];
    entity.unverified_follower_ratio = [self checkJson:[resultArray valueForKey:@"unverified_follower_ratio"]];
    entity.friend_number = [self checkJson:[resultArray valueForKey:@"friend_number"]];
    entity.bilateral_number = [self checkJson:[resultArray valueForKey:@"bilateral_number"]];
    entity.statuses_number = [self checkJson:[resultArray valueForKey:@"statuses_number"]];
    entity.follower_number = [self checkJson:[resultArray valueForKey:@"follower_number"]];
    
    return entity;
}
// RB-分析-微信-粉丝
+ (NSMutableArray *)getRBAnalysisWechatFollowerListEntity:(id)jsonObject
                                             andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"user_analysises"];
    NSArray *report_timeArray = [resultArray valueForKey:@"report_time"];
    for (int i = 0; i < [report_timeArray count]; i++) {
        @autoreleasepool {
            RBAnalysisWechatFollowerEntity *entity = [[RBAnalysisWechatFollowerEntity alloc] init];
            entity.report_time = [self checkJson:[resultArray[i] valueForKey:@"report_time"]];
            entity.nnew_followers_count = [self checkJson:[resultArray[i] valueForKey:@"new_followers_count"]];
            entity.cancel_followers_count = [self checkJson:[resultArray[i] valueForKey:@"cancel_followers_count"]];
            entity.growth_followers_count = [self checkJson:[resultArray[i] valueForKey:@"growth_followers_count"]];
            entity.total_followers_count = [self checkJson:[resultArray[i] valueForKey:@"total_followers_count"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-分析-微信-消息
+ (NSMutableArray *)getRBAnalysisWechatMessageListEntity:(id)jsonObject
                                            andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"messages"];
    NSArray *report_timeArray = [resultArray valueForKey:@"report_time"];
    for (int i = 0; i < [report_timeArray count]; i++) {
        @autoreleasepool {
            RBAnalysisWechatMessageEntity *entity = [[RBAnalysisWechatMessageEntity alloc] init];
            entity.report_time = [self checkJson:[resultArray[i] valueForKey:@"report_time"]];
            entity.send_message_user_count = [self checkJson:[resultArray[i] valueForKey:@"send_message_user_count"]];
            entity.send_message_count = [self checkJson:[resultArray[i] valueForKey:@"send_message_count"]];
            entity.per_send_message_count = [self checkJson:[resultArray[i] valueForKey:@"per_send_message_count"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-分析-微信-图文
+ (NSMutableArray *)getRBAnalysisWechatArticleListEntity:(id)jsonObject
                                            andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"articles"];
    NSArray *publish_dateArray = [resultArray valueForKey:@"publish_date"];
    for (int i = 0; i < [publish_dateArray count]; i++) {
        @autoreleasepool {
            RBAnalysisWechatArticleEntity *entity = [[RBAnalysisWechatArticleEntity alloc] init];
            entity.article_id = [self checkJson:[resultArray[i] valueForKey:@"article_id"]];
            entity.title = [self checkJson:[resultArray[i] valueForKey:@"title"]];
            entity.publish_date = [self checkJson:[resultArray[i] valueForKey:@"publish_date"]];
            entity.target_user_count = [self checkJson:[resultArray[i] valueForKey:@"target_user_count"]];
            entity.read_user_count = [self checkJson:[resultArray[i] valueForKey:@"read_user_count"]];
            entity.share_user_count = [self checkJson:[resultArray[i] valueForKey:@"share_user_count"]];
            
            
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-分析-微博-列表
+ (NSMutableArray *)getRBAnalysisWeiboArticleListEntity:(id)jsonObject
                                           andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"statuses"];
    NSArray *r_idArray = [resultArray valueForKey:@"r_id"];
    for (int i = 0; i < [r_idArray count]; i++) {
        @autoreleasepool {
            RBRankingWeiboDetailEntity *entity = [[RBRankingWeiboDetailEntity alloc] init];
            entity.summary = [self checkJson:[resultArray[i] valueForKey:@"text"]];
            entity.publish_date = [self checkJson:[resultArray[i] valueForKey:@"publised_at"]];
            entity.retweets = [self checkJson:[resultArray[i] valueForKey:@"reposts_count"]];
            entity.comments = [self checkJson:[resultArray[i] valueForKey:@"comments_count"]];
            entity.likes = [self checkJson:[resultArray[i] valueForKey:@"attitudes_count"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}


// RB-分析-微博-地区
+ (NSMutableArray *)getRBAnalysisWeiboLocationListEntity:(id)jsonObject
                                            andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"regions"];
    NSArray *nameArray = [resultArray valueForKey:@"name"];
    if ([resultArray valueForKey:@"name"] == nil || [[resultArray valueForKey:@"name"] isEqual:[NSNull null]]) {
        return nil;
    }
    for (int i = 0; i < [nameArray count]; i++) {
        @autoreleasepool {
            RBAnalysisWechatLocationEntity *entity = [[RBAnalysisWechatLocationEntity alloc] init];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.code = [self checkJson:[resultArray[i] valueForKey:@"code"]];
            entity.location = [self checkJson:[resultArray[i] valueForKey:@"location"]];
            entity.number = [self checkJson:[resultArray[i] valueForKey:@"number"]];
            entity.ratio = [self checkJson:[resultArray[i] valueForKey:@"ratio"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-分析-微博-性别
+ (RBAnalysisWechatGenderEntity *)getRBAnalysisWeiboGenderListEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"genders"];
    RBAnalysisWechatGenderEntity *entity = [[RBAnalysisWechatGenderEntity alloc] init];
    entity.male_number = [self checkJson:[resultArray valueForKey:@"male_number"]];
    entity.female_number = [self checkJson:[resultArray valueForKey:@"female_number"]];
    entity.male_ratio = [self checkJson:[resultArray valueForKey:@"male_ratio"]];
    entity.female_ratio = [self checkJson:[resultArray valueForKey:@"female_ratio"]];
    return entity;
}

// RB-分析-微博-粉丝-新增
+ (NSMutableArray *)getRBAnalysisWeiboFollowerNewListEntity:(id)jsonObject
                                               andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"incremental_followers"];
    NSArray *r_dateArray = [resultArray valueForKey:@"r_date"];
    for (int i = 0; i < [r_dateArray count]; i++) {
        @autoreleasepool {
            RBAnalysisWechatFollowerEntity *entity = [[RBAnalysisWechatFollowerEntity alloc] init];
            entity.r_date = [self checkJson:[resultArray[i] valueForKey:@"r_date"]];
            entity.total_number = [self checkJson:[resultArray[i] valueForKey:@"number"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-分析-微博-粉丝-取消
+ (NSMutableArray *)getRBAnalysisWeiboFollowerCancelListEntity:(id)jsonObject
                                                  andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"decremental_followers"];
    NSArray *r_dateArray = [resultArray valueForKey:@"r_date"];
    for (int i = 0; i < [r_dateArray count]; i++) {
        @autoreleasepool {
            RBAnalysisWechatFollowerEntity *entity = [[RBAnalysisWechatFollowerEntity alloc] init];
            entity.r_date = [self checkJson:[resultArray[i] valueForKey:@"r_date"]];
            entity.total_number = [self checkJson:[resultArray[i] valueForKey:@"number"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-分析-微博-粉丝-取消-列表
+ (NSMutableArray *)getRBAnalysisWeiboFollowerCancelDetailListEntity:(id)jsonObject
                                                        andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"decremental_follower_list"];
    NSArray *unfollowed_atArray = [resultArray valueForKey:@"unfollowed_at"];
    for (int i = 0; i < [unfollowed_atArray count]; i++) {
        @autoreleasepool {
            RBAnalysisWechatEntity *entity = [[RBAnalysisWechatEntity alloc] init];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.profile_image_url = [self checkJson:[resultArray[i] valueForKey:@"profile_image_url"]];
            entity.follower_number = [self checkJson:[resultArray[i] valueForKey:@"follower_number"]];
            entity.unfollowed_at = [self checkJson:[resultArray[i] valueForKey:@"unfollowed_at"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}


// RB-分析-微博-关注
+ (NSMutableArray *)getRBAnalysisWeiboFriendListEntity:(id)jsonObject
                                          andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"friend_verified"];
    NSArray *r_dateArray = [resultArray valueForKey:@"r_date"];
    for (int i = 0; i < [r_dateArray count]; i++) {
        @autoreleasepool {
            RBAnalysisWechatFollowerEntity *entity = [[RBAnalysisWechatFollowerEntity alloc] init];
            entity.r_date = [self checkJson:[resultArray[i] valueForKey:@"r_date"]];
            entity.total_number = [self checkJson:[resultArray[i] valueForKey:@"total_number"]];
            entity.unverified_number = [self checkJson:[resultArray[i] valueForKey:@"unverified_number"]];
            entity.verified_number = [self checkJson:[resultArray[i] valueForKey:@"verified_number"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}


// RB-一元夺宝
// RB-一元夺宝-列表
+ (NSMutableArray *)getRBIndianaListEntity:(id)jsonObject
                              andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"activities"];
    NSArray *iidArray = [resultArray valueForKey:@"id"];
    for (int i = 0; i < [iidArray count]; i++) {
        @autoreleasepool {
            RBIndianaEntity *entity = [[RBIndianaEntity alloc] init];
            entity.iid = [self checkJson:[resultArray[i] valueForKey:@"id"]];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.code = [self checkJson:[resultArray[i] valueForKey:@"code"]];
            entity.published_at = [self checkJson:[resultArray[i] valueForKey:@"published_at"]];
            entity.total_number = [self checkJson:[resultArray[i] valueForKey:@"total_number"]];
            entity.actual_number = [self checkJson:[resultArray[i] valueForKey:@"actual_number"]];
            entity.status = [self checkJson:[resultArray[i] valueForKey:@"status"]];
            entity.poster_url = [self checkJson:[resultArray[i] valueForKey:@"poster_url"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-一元夺宝-详情
+ (RBIndianaEntity *)getRBIndianaDetailEntity:(id)jsonObject {
    RBIndianaEntity *entity = [[RBIndianaEntity alloc] init];
    entity.kol_amount = [self checkJson:[jsonObject valueForKey:@"kol_amount"]];
    entity.token_number = [self checkJson:[jsonObject objectForKey:@"token_number"]];
    entity.tickets = [jsonObject objectForKey:@"tickets"];
    NSArray *resultArray = [jsonObject objectForKey:@"activity"];
    entity.iid = [self checkJson:[resultArray valueForKey:@"id"]];
    entity.name = [self checkJson:[resultArray valueForKey:@"name"]];
    entity.code = [self checkJson:[resultArray valueForKey:@"code"]];
    entity.published_at = [self checkJson:[resultArray valueForKey:@"published_at"]];
    entity.total_number = [self checkJson:[resultArray valueForKey:@"total_number"]];
    entity.actual_number = [self checkJson:[resultArray valueForKey:@"actual_number"]];
    entity.status = [self checkJson:[resultArray valueForKey:@"status"]];
    entity.poster_url = [self checkJson:[resultArray valueForKey:@"poster_url"]];
    entity.idescription = [self checkJson:[resultArray valueForKey:@"description"]];
    entity.lucky_number = [self checkJson:[resultArray valueForKey:@"lucky_number"]];
    entity.draw_at = [self checkJson:[resultArray valueForKey:@"draw_at"]];
    entity.pictures = [resultArray valueForKey:@"pictures"];
    entity.winner_name = [self checkJson:[resultArray valueForKey:@"winner_name"]];
    entity.winner_avatar_url = [self checkJson:[resultArray valueForKey:@"winner_avatar_url"]];
    entity.winner_token_number = [self checkJson:[resultArray valueForKey:@"winner_token_number"]];
    return entity;
}


// RB-一元夺宝-详情-购买者
+ (NSMutableArray *)getRBIndianaDetailOrderEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"orders"];
    NSArray *iidArray = [resultArray valueForKey:@"id"];
    for (int i = 0; i < [iidArray count]; i++) {
        @autoreleasepool {
            RBIndianaOrderEntity *entity = [[RBIndianaOrderEntity alloc] init];
            NSArray *kolArray = [resultArray[i] valueForKey:@"kol"];
            entity.name = [self checkJson:[kolArray valueForKey:@"name"]];
            entity.avatar_url = [self checkJson:[kolArray valueForKey:@"avatar_url"]];
            entity.created_at = [self checkJson:[resultArray[i] valueForKey:@"created_at"]];
            entity.number = [self checkJson:[resultArray[i] valueForKey:@"number"]];
            entity.credits = [self checkJson:[resultArray[i] valueForKey:@"credits"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-一元夺宝-详情-我的
+ (NSMutableArray *)getRBIndianaDetailMyEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"activities"];
    NSArray *iidArray = [resultArray valueForKey:@"id"];
    for (int i = 0; i < [iidArray count]; i++) {
        @autoreleasepool {
            RBIndianaEntity *entity = [[RBIndianaEntity alloc] init];
            entity.token_number = [self checkJson:[resultArray[i] objectForKey:@"token_number"]];
            entity.iid = [self checkJson:[resultArray[i] valueForKey:@"id"]];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.code = [self checkJson:[resultArray[i] valueForKey:@"code"]];
            entity.published_at = [self checkJson:[resultArray[i] valueForKey:@"published_at"]];
            entity.total_number = [self checkJson:[resultArray[i] valueForKey:@"total_number"]];
            entity.actual_number = [self checkJson:[resultArray[i] valueForKey:@"actual_number"]];
            entity.status = [self checkJson:[resultArray[i] valueForKey:@"status"]];
            entity.poster_url = [self checkJson:[resultArray[i] valueForKey:@"poster_url"]];
            entity.winner_name = [self checkJson:[resultArray[i] valueForKey:@"winner_name"]];
            entity.winner_token_number = [self checkJson:[resultArray[i] valueForKey:@"winner_token_number"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}



// RB-发布活动-详情
+ (RBAdvertiserCampaignEntity *)getRBAdvertiserCampaignEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"campaign"];
    RBAdvertiserCampaignEntity *entity = [[RBAdvertiserCampaignEntity alloc] init];
    entity.kol_credit = [self checkJson:[jsonObject objectForKey:@"kol_credit"]];
    entity.iid = [self checkJson:[resultArray valueForKey:@"id"]];
    entity.name = [self checkJson:[resultArray valueForKey:@"name"]];
    entity.idescription = [self checkJson:[resultArray valueForKey:@"description"]];
    entity.status = [self checkJson:[resultArray valueForKey:@"status"]];
    entity.url = [self checkJson:[resultArray valueForKey:@"url"]];
    entity.img_url = [self checkJson:[resultArray valueForKey:@"img_url"]];
    entity.status = [self checkJson:[resultArray valueForKey:@"status"]];
    entity.per_budget_type = [self checkJson:[resultArray valueForKey:@"per_budget_type"]];
    entity.per_action_budget = [self checkJson:[resultArray valueForKey:@"per_action_budget"]];
    entity.budget = [self checkJson:[resultArray valueForKey:@"budget"]];
    entity.need_pay_amount = [self checkJson:[resultArray valueForKey:@"need_pay_amount"]];
    entity.voucher_amount = [resultArray valueForKey:@"voucher_amount"];
    entity.used_voucher = [self checkJson:[resultArray valueForKey:@"used_voucher"]];
    entity.deadline = [self checkJson:[resultArray valueForKey:@"deadline"]];
    entity.start_time = [self checkJson:[resultArray valueForKey:@"start_time"]];
    entity.invalid_reasons = [resultArray valueForKey:@"invalid_reasons"];
    entity.kol_amount = [jsonObject objectForKey:@"kol_amount"];
    entity.avail_click = [self checkJson:[resultArray valueForKey:@"avail_click"]];
    entity.total_click = [self checkJson:[resultArray valueForKey:@"total_click"]];
    entity.take_budget = [self checkJson:[resultArray valueForKey:@"take_budget"]];
    entity.share_times = [self checkJson:[resultArray valueForKey:@"share_times"]];
    entity.budget_editable = [self checkJson:[resultArray valueForKey:@"budget_editable"]];
    entity.cal_settle_time = [self checkJson:[resultArray valueForKey:@"cal_settle_time"]];
    entity.stats_data = [resultArray valueForKey:@"stats_data"];
    entity.gender = [self checkJson:[resultArray valueForKey:@"gender"]];
    entity.age = [self checkJson:[resultArray valueForKey:@"age"]];
    entity.region = [self checkJson:[resultArray valueForKey:@"region"]];
    entity.tag_labels = [self checkJson:[resultArray valueForKey:@"tag_labels"]];
    entity.sub_type = [self checkJson:[resultArray valueForKey:@"sub_type"]];
    entity.used_credits = [self checkJson:[resultArray valueForKey:@"used_credits"]];
    return entity;
}
// RB-发布活动-付款详情
+ (RBAdvertiserCampaignPayEntity *)getRBAdvertiserCampaignPayEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"campaign"];
    RBAdvertiserCampaignPayEntity *entity = [[RBAdvertiserCampaignPayEntity alloc] init];
    entity.iid = [self checkJson:[resultArray valueForKey:@"id"]];
    entity.need_pay_amount = [self checkJson:[resultArray valueForKey:@"need_pay_amount"]];
    entity.status = [self checkJson:[resultArray valueForKey:@"status"]];
    entity.brand_amount = [self checkJson:[resultArray valueForKey:@"brand_amount"]];
    entity.alipay_url = [self checkJson:[resultArray valueForKey:@"alipay_url"]];
    return entity;
}

// RB-活动-KOL列表
+ (NSMutableArray *)getRBCampaignKolList:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray *inviteesArray = [jsonObject objectForKey:@"invitees"];
    NSArray *inviteesIdArray = [inviteesArray valueForKey:@"id"];
    for (int i = 0; i < [inviteesIdArray count]; i++) {
        @autoreleasepool {
            RBUserEntity *uEntity = [[RBUserEntity alloc] init];
            uEntity.iid = [self checkJson:[inviteesArray[i] valueForKey:@"id"]];
            uEntity.name = [self checkJson:[inviteesArray[i] valueForKey:@"name"]];
            uEntity.avatar_url = [self checkJson:[inviteesArray[i] valueForKey:@"avatar_url"]];
            [backArray addObject:uEntity];
        }
    }
    return backArray;
}

// RB-发布活动-列表
+ (NSMutableArray *)getRBAdvertiserCampaignEntityList:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"campaigns"];
    NSArray *iidArray = [resultArray valueForKey:@"id"];
    for (int i = 0; i < [iidArray count]; i++) {
        @autoreleasepool {
            RBAdvertiserCampaignEntity *entity = [[RBAdvertiserCampaignEntity alloc] init];
            entity.iid = [self checkJson:[resultArray[i] valueForKey:@"id"]];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.idescription = [self checkJson:[resultArray[i] valueForKey:@"description"]];
            entity.status = [self checkJson:[resultArray[i] valueForKey:@"status"]];
            entity.url = [self checkJson:[resultArray[i] valueForKey:@"url"]];
            entity.img_url = [self checkJson:[resultArray[i] valueForKey:@"img_url"]];
            entity.status = [self checkJson:[resultArray[i] valueForKey:@"status"]];
            entity.per_budget_type = [self checkJson:[resultArray[i] valueForKey:@"per_budget_type"]];
            entity.per_action_budget = [self checkJson:[resultArray[i] valueForKey:@"per_action_budget"]];
            entity.budget = [self checkJson:[resultArray[i] valueForKey:@"budget"]];
            entity.need_pay_amount = [self checkJson:[resultArray[i] valueForKey:@"need_pay_amount"]];
            entity.voucher_amount = [resultArray[i] valueForKey:@"voucher_amount"];
            entity.used_voucher = [self checkJson:[resultArray[i] valueForKey:@"used_voucher"]];
            entity.deadline = [self checkJson:[resultArray[i] valueForKey:@"deadline"]];
            entity.start_time = [self checkJson:[resultArray[i] valueForKey:@"start_time"]];
            entity.invalid_reasons = [resultArray[i] valueForKey:@"invalid_reasons"];
            entity.kol_amount = [resultArray[i] valueForKey:@"kol_amount"];
            entity.sub_type = [self checkJson:[resultArray[i] valueForKey:@"sub_type"]];
            entity.used_credits = [self checkJson:[resultArray[i] valueForKey:@"used_credits"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-发布活动-参与列表
+ (NSMutableArray *)getRBAdvertiserUserEntityList:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"campaign_invites"];
    NSArray *kol_nameArray = [resultArray valueForKey:@"kol_name"];
    for (int i = 0; i < [kol_nameArray count]; i++) {
        @autoreleasepool {
            RBUserEntity *entity = [[RBUserEntity alloc] init];
            entity.iid = [self checkJson:[resultArray[i] valueForKey:@"kol_id"]];
            entity.avatar_url = [self checkJson:[resultArray[i] valueForKey:@"avatar_url"]];
            entity.kol_name = [self checkJson:[resultArray[i] valueForKey:@"kol_name"]];
            entity.avail_click = [self checkJson:[resultArray[i] valueForKey:@"avail_click"]];
            entity.total_click = [self checkJson:[resultArray[i] valueForKey:@"total_click"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-发布活动-期望结果
+ (NSMutableArray *)getRBAdvertiserResultEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"expect_effect_list"];
    NSArray *nameArray = [resultArray valueForKey:@"name"];
    for (int i = 0; i < [nameArray count]; i++) {
        @autoreleasepool {
            RBTagEntity *entity = [[RBTagEntity alloc] init];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.label = [self checkJson:[resultArray[i] valueForKey:@"label"]];
            entity.cover_url = [self checkJson:[resultArray[i] valueForKey:@"budget_type"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-发布活动-分析结果-活动信息
+ (RBAdvertiserCampaignEntity *)getRBAdvertiserResultCampaignEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"campaign_input"];
    RBAdvertiserCampaignEntity *entity = [[RBAdvertiserCampaignEntity alloc] init];
    entity.iid = [self checkJson:[resultArray valueForKey:@"id"]];
    entity.name = [self checkJson:[resultArray valueForKey:@"name"]];
    entity.idescription = [self checkJson:[resultArray valueForKey:@"description"]];
    entity.url = [self checkJson:[resultArray valueForKey:@"url"]];
    entity.img_url = [self checkJson:[resultArray valueForKey:@"img_url"]];
    entity.per_budget_type = [self checkJson:[resultArray valueForKey:@"per_budget_type"]];
    entity.budget = [self checkJson:[resultArray valueForKey:@"budget"]];
    entity.per_action_budget = [self checkJson:[resultArray valueForKey:@"per_action_budget"]];
    entity.start_time = [self checkJson:[resultArray valueForKey:@"start_time"]];
    entity.deadline = [self checkJson:[resultArray valueForKey:@"deadline"]];
    entity.gender = [self checkJson:[resultArray valueForKey:@"gender"]];
    entity.age = [self checkJson:[resultArray valueForKey:@"age"]];
    entity.region = [self checkJson:[resultArray valueForKey:@"region"]];
    entity.tag_labels = [self checkJson:[resultArray valueForKey:@"tag_labels"]];
    return entity;
}
// RB-发布活动-分析结果-NLP信息
+ (RBNlpEntity *)getRBAdvertiserResultNlpEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"analysis_info"];
    RBNlpEntity *entity = [[RBNlpEntity alloc] init];
    entity.text = [self checkJson:[resultArray valueForKey:@"text"]];
    entity.cities = [resultArray valueForKey:@"cities"];
    entity.keywords = [NSMutableArray new];
    NSArray *keywordsArray = [resultArray valueForKey:@"keywords"];
    NSArray *labelArray = [keywordsArray valueForKey:@"label"];
    for (int j = 0; j < [labelArray count]; j++) {
        @autoreleasepool {
            RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
            tagEntity.freq = [self checkJson:[keywordsArray[j] valueForKey:@"freq"]];
            tagEntity.label = [self checkJson:[keywordsArray[j] valueForKey:@"label"]];
            [entity.keywords addObject:tagEntity];
        }
    }
    entity.sentiment = [NSMutableArray new];
    NSArray *sentimentArray = [resultArray valueForKey:@"sentiment"];
    NSArray *labelArray1 = [sentimentArray valueForKey:@"label"];
    for (int k = 0; k < [labelArray1 count]; k++) {
        @autoreleasepool {
            RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
            tagEntity.label = [self checkJson:[sentimentArray[k] valueForKey:@"label"]];
            tagEntity.probability = [self checkJson:[sentimentArray[k] valueForKey:@"probability"]];
            [entity.sentiment addObject:tagEntity];
        }
    }
    entity.categories = [NSMutableArray new];
    NSArray *categorysArray = [resultArray valueForKey:@"categories"];
    NSArray *labelArray2 = [categorysArray valueForKey:@"label"];
    for (int l = 0; l < [labelArray2 count]; l++) {
        @autoreleasepool {
            RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
            tagEntity.label = [self checkJson:[categorysArray[l] valueForKey:@"label"]];
            tagEntity.probability = [self checkJson:[categorysArray[l] valueForKey:@"probability"]];
            [entity.categories addObject:tagEntity];
        }
    }
    entity.persons_brands = [NSMutableArray new];
    NSArray *persons_brandsArray = [resultArray valueForKey:@"persons_brands"];
    NSArray *labelArray3 = [persons_brandsArray valueForKey:@"label"];
    for (int l = 0; l < [labelArray3 count]; l++) {
        @autoreleasepool {
            RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
            tagEntity.label = [self checkJson:[persons_brandsArray[l] valueForKey:@"label"]];
            tagEntity.freq = [self checkJson:[persons_brandsArray[l] valueForKey:@"freq"]];
            [entity.persons_brands addObject:tagEntity];
        }
    }
    entity.products = [NSMutableArray new];
    NSArray *productsArray = [resultArray valueForKey:@"products"];
    NSArray *labelArray4 = [productsArray valueForKey:@"label"];
    for (int l = 0; l < [labelArray4 count]; l++) {
        @autoreleasepool {
            RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
            tagEntity.label = [self checkJson:[productsArray[l] valueForKey:@"label"]];
            tagEntity.freq = [self checkJson:[productsArray[l] valueForKey:@"freq"]];
            [entity.products addObject:tagEntity];
        }
    }
    return entity;
}
// RB-活动参与人员-分析结果
+ (RBNlpEntity *)getRBCampaignKolResultNlpEntity:(id)jsonObject {
    RBNlpEntity *entity = [[RBNlpEntity alloc] init];
    entity.gender_analysis = [NSMutableArray new];
    NSArray *genderArray = [jsonObject objectForKey:@"gender_analysis"];
    NSArray *nameArray = [genderArray valueForKey:@"name"];
    for (int j = 0; j < [nameArray count]; j++) {
        @autoreleasepool {
            RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
            tagEntity.ratio = [self checkJson:[genderArray[j] valueForKey:@"ratio"]];
            tagEntity.name = [self checkJson:[genderArray[j] valueForKey:@"name"]];
            [entity.gender_analysis addObject:tagEntity];
        }
    }
    entity.age_analysis = [NSMutableArray new];
    NSArray *ageArray = [jsonObject objectForKey:@"age_analysis"];
    NSArray *nameArray1 = [ageArray valueForKey:@"name"];
    for (int k = 0; k < [nameArray1 count]; k++) {
        @autoreleasepool {
            RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
            tagEntity.ratio = [self checkJson:[ageArray[k] valueForKey:@"ratio"]];
            tagEntity.name = [self checkJson:[ageArray[k] valueForKey:@"name"]];
            [entity.age_analysis addObject:tagEntity];
        }
    }
    entity.tag_analysis = [NSMutableArray new];
    NSArray *tagArray = [jsonObject objectForKey:@"tag_analysis"];
    NSArray *nameArray2 = [tagArray valueForKey:@"name"];
    for (int k = 0; k < [nameArray2 count]; k++) {
        @autoreleasepool {
            RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
            tagEntity.ratio = [self checkJson:[tagArray[k] valueForKey:@"ratio"]];
            tagEntity.name = [self checkJson:[tagArray[k] valueForKey:@"name"]];
            [entity.tag_analysis addObject:tagEntity];
        }
    }
    entity.region_analysis = [NSMutableArray new];
    NSArray *regionArray = [jsonObject objectForKey:@"region_analysis"];
    NSArray *nameArray3 = [regionArray valueForKey:@"province_name"];
    for (int k = 0; k < [nameArray3 count]; k++) {
        @autoreleasepool {
            RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
            tagEntity.name = [self checkJson:[regionArray[k] valueForKey:@"province_name"]];
            [entity.region_analysis addObject:tagEntity];
        }
    }
    return entity;
}




// RB-用户详情模块
+ (RBUserEntity *)getRBUserDetailEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"invitee"];
    RBUserEntity *entity = [[RBUserEntity alloc] init];
    entity.iid = [self checkJson:[resultArray valueForKey:@"id"]];
    entity.name = [self checkJson:[resultArray valueForKey:@"name"]];
    entity.app_city_label = [self checkJson:[resultArray valueForKey:@"app_city_label"]];
    entity.avatar_url = [self checkJson:[resultArray valueForKey:@"avatar_url"]];
    entity.tags = [resultArray valueForKey:@"tags"];
    entity.influence_level = [self checkJson:[resultArray valueForKey:@"influence_level"]];
    entity.rank_index = [self checkJson:[resultArray valueForKey:@"rank_index"]];
    entity.influence_score = [self checkJson:[resultArray valueForKey:@"influence_score"]];
    return entity;
}




// RB-KOL列表模块
+ (NSMutableArray *)getRBKolListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"big_vs"];
    NSArray *idArray = [resultArray valueForKey:@"id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBKOLEntity *entity = [[RBKOLEntity alloc] init];
            entity.iid = [self checkJson:[resultArray[i] valueForKey:@"id"]];
            entity.avatar_url = [self checkJson:[resultArray[i] valueForKey:@"avatar_url"]];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.job_info = [self checkJson:[resultArray[i] valueForKey:@"job_info"]];
            //
            entity.tags = [NSMutableArray new];
            NSArray *tagsArray = [resultArray[i] valueForKey:@"tags"];
            NSArray *nameArray = [tagsArray valueForKey:@"name"];
            for (int j = 0; j < [nameArray count]; j++) {
                @autoreleasepool {
                    RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
                    tagEntity.name = [self checkJson:[tagsArray[j] valueForKey:@"name"]];
                    tagEntity.label = [self checkJson:[tagsArray[j] valueForKey:@"label"]];
                    [entity.tags addObject:tagEntity];
                }
            }
            //
            entity.social_accounts = [NSMutableArray new];
            NSArray *social_accountsArray = [resultArray[i] valueForKey:@"social_accounts"];
            NSArray *providerArray = [social_accountsArray valueForKey:@"provider"];
            for (int m = 0; m < [providerArray count]; m++) {
                @autoreleasepool {
                    RBKOLSocialEntity *socialEntity = [[RBKOLSocialEntity alloc] init];
                    socialEntity.provider = [self checkJson:[social_accountsArray[m] valueForKey:@"provider"]];
                    socialEntity.provider_name = [self checkJson:[social_accountsArray[m] valueForKey:@"provider_name"]];
                    socialEntity.uid = [self checkJson:[social_accountsArray[m] valueForKey:@"uid"]];
                    socialEntity.username = [self checkJson:[social_accountsArray[m] valueForKey:@"username"]];
                    socialEntity.homepage = [self checkJson:[social_accountsArray[m] valueForKey:@"homepage"]];
                    socialEntity.avatar_url = [self checkJson:[social_accountsArray[m] valueForKey:@"avatar_url"]];
                    socialEntity.brief = [self checkJson:[social_accountsArray[m] valueForKey:@"brief"]];
                    socialEntity.followers_count = [self checkJson:[social_accountsArray[m] valueForKey:@"followers_count"]];
                    socialEntity.reposts_count = [self checkJson:[social_accountsArray[m] valueForKey:@"reposts_count"]];
                    socialEntity.statuses_count = [self checkJson:[social_accountsArray[m] valueForKey:@"statuses_count"]];
                    [entity.social_accounts addObject:socialEntity];
                }
            }
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-KOL关注列表模块
+ (NSMutableArray *)getRBKolFollowersListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"big_vs"];
    NSArray *idArray = [resultArray valueForKey:@"id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBKOLEntity *entity = [[RBKOLEntity alloc] init];
            entity.iid = [self checkJson:[resultArray[i] valueForKey:@"id"]];
            entity.avatar_url = [self checkJson:[resultArray[i] valueForKey:@"avatar_url"]];
            entity.name = [self checkJson:[resultArray[i] valueForKey:@"name"]];
            entity.job_info = [self checkJson:[resultArray[i] valueForKey:@"job_info"]];
            //
            entity.tags = [NSMutableArray new];
            NSArray *tagsArray = [resultArray[i] valueForKey:@"tags"];
            NSArray *nameArray = [tagsArray valueForKey:@"name"];
            for (int j = 0; j < [nameArray count]; j++) {
                @autoreleasepool {
                    RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
                    tagEntity.name = [self checkJson:[tagsArray[j] valueForKey:@"name"]];
                    tagEntity.label = [self checkJson:[tagsArray[j] valueForKey:@"label"]];
                    [entity.tags addObject:tagEntity];
                }
            }
            //
            entity.social_accounts = [NSMutableArray new];
            NSArray *social_accountsArray = [resultArray[i] valueForKey:@"social_accounts"];
            NSArray *providerArray = [social_accountsArray valueForKey:@"provider"];
            for (int m = 0; m < [providerArray count]; m++) {
                @autoreleasepool {
                    RBKOLSocialEntity *socialEntity = [[RBKOLSocialEntity alloc] init];
                    socialEntity.provider = [self checkJson:[social_accountsArray[m] valueForKey:@"provider"]];
                    socialEntity.provider_name = [self checkJson:[social_accountsArray[m] valueForKey:@"provider_name"]];
                    socialEntity.uid = [self checkJson:[social_accountsArray[m] valueForKey:@"uid"]];
                    socialEntity.username = [self checkJson:[social_accountsArray[m] valueForKey:@"username"]];
                    socialEntity.homepage = [self checkJson:[social_accountsArray[m] valueForKey:@"homepage"]];
                    socialEntity.avatar_url = [self checkJson:[social_accountsArray[m] valueForKey:@"avatar_url"]];
                    socialEntity.brief = [self checkJson:[social_accountsArray[m] valueForKey:@"brief"]];
                    socialEntity.followers_count = [self checkJson:[social_accountsArray[m] valueForKey:@"followers_count"]];
                    socialEntity.reposts_count = [self checkJson:[social_accountsArray[m] valueForKey:@"reposts_count"]];
                    socialEntity.statuses_count = [self checkJson:[social_accountsArray[m] valueForKey:@"statuses_count"]];
                    [entity.social_accounts addObject:socialEntity];
                }
            }
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-KOL详情模块
+ (RBKOLEntity *)getRBKolDetailEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"big_v"];
    RBKOLEntity *entity = [[RBKOLEntity alloc] init];
    entity.iid = [self checkJson:[resultArray valueForKey:@"id"]];
    entity.age = [self checkJson:[resultArray valueForKey:@"age"]];
    entity.name = [self checkJson:[resultArray valueForKey:@"name"]];
    entity.gender = [self checkJson:[resultArray valueForKey:@"gender"]];
    entity.avatar_url = [self checkJson:[resultArray valueForKey:@"avatar_url"]];
    entity.job_info = [self checkJson:[resultArray valueForKey:@"job_info"]];
    entity.app_city_label = [self checkJson:[resultArray valueForKey:@"app_city_label"]];
    entity.desc = [self checkJson:[resultArray valueForKey:@"desc"]];
    entity.role_check_remark = [self checkJson:[resultArray valueForKey:@"role_check_remark"]];
    entity.role_apply_status = [self checkJson:[resultArray valueForKey:@"role_apply_status"]];
    entity.kol_role = [self checkJson:[resultArray valueForKey:@"kol_role"]];
    entity.email = [self checkJson:[resultArray valueForKey:@"email"]];
    //
    entity.tags = [NSMutableArray new];
    NSArray *tagsArray = [resultArray valueForKey:@"tags"];
    NSArray *nameArray = [tagsArray valueForKey:@"name"];
    for (int j = 0; j < [nameArray count]; j++) {
        @autoreleasepool {
            RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
            tagEntity.name = [self checkJson:[tagsArray[j] valueForKey:@"name"]];
            tagEntity.label = [self checkJson:[tagsArray[j] valueForKey:@"label"]];
            [entity.tags addObject:tagEntity];
        }
    }
    //
    entity.kol_shows = [NSMutableArray new];
    NSArray *kol_showsArray = [jsonObject objectForKey:@"kol_shows"];
    NSArray *titleArray = [kol_showsArray valueForKey:@"title"];
    for (int k = 0; k < [titleArray count]; k++) {
        @autoreleasepool {
            RBKOLShowEntity *showEntity = [[RBKOLShowEntity alloc] init];
            showEntity.title = [self checkJson:[kol_showsArray[k] valueForKey:@"title"]];
            showEntity.desc = [self checkJson:[kol_showsArray[k] valueForKey:@"desc"]];
            showEntity.link = [self checkJson:[kol_showsArray[k] valueForKey:@"link"]];
            showEntity.provider = [self checkJson:[kol_showsArray[k] valueForKey:@"provider"]];
            showEntity.cover_url = [self checkJson:[kol_showsArray[k] valueForKey:@"cover_url"]];
            [entity.kol_shows addObject:showEntity];
        }
    }
    //
    entity.kol_keywords = [NSMutableArray new];
    NSArray *kol_keywordsArray = [jsonObject objectForKey:@"kol_keywords"];
    NSArray *weightArray = [kol_keywordsArray valueForKey:@"weight"];
    for (int l = 0; l < [weightArray count]; l++) {
        @autoreleasepool {
            RBKOLKeywordsEntity *kwEntity = [[RBKOLKeywordsEntity alloc] init];
            kwEntity.name = [self checkJson:[kol_keywordsArray[l] valueForKey:@"keyword"]];
            kwEntity.weight = [self checkJson:[kol_keywordsArray[l] valueForKey:@"weight"]];
            [entity.kol_keywords addObject:kwEntity];
        }
    }
    //
    entity.social_accounts = [NSMutableArray new];
    NSArray *social_accountsArray = [jsonObject objectForKey:@"social_accounts"];
    NSArray *providerArray = [social_accountsArray valueForKey:@"provider"];
    for (int m = 0; m < [providerArray count]; m++) {
        @autoreleasepool {
            RBKOLSocialEntity *socialEntity = [[RBKOLSocialEntity alloc] init];
            socialEntity.provider = [self checkJson:[social_accountsArray[m] valueForKey:@"provider"]];
            socialEntity.provider_name = [self checkJson:[social_accountsArray[m] valueForKey:@"provider_name"]];
            socialEntity.uid = [self checkJson:[social_accountsArray[m] valueForKey:@"uid"]];
            socialEntity.price = [self checkJson:[social_accountsArray[m] valueForKey:@"price"]];
            socialEntity.username = [self checkJson:[social_accountsArray[m] valueForKey:@"username"]];
            socialEntity.homepage = [self checkJson:[social_accountsArray[m] valueForKey:@"homepage"]];
            socialEntity.avatar_url = [self checkJson:[social_accountsArray[m] valueForKey:@"avatar_url"]];
            socialEntity.search_kol_id = [self checkJson:[social_accountsArray[m] valueForKey:@"search_kol_id"]];
            socialEntity.brief = [self checkJson:[social_accountsArray[m] valueForKey:@"brief"]];
            socialEntity.followers_count = [self checkJson:[social_accountsArray[m] valueForKey:@"followers_count"]];
            socialEntity.reposts_count = [self checkJson:[social_accountsArray[m] valueForKey:@"reposts_count"]];
            socialEntity.statuses_count = [self checkJson:[social_accountsArray[m] valueForKey:@"statuses_count"]];
            socialEntity.kol_id = [self checkJson:[social_accountsArray[m] valueForKey:@"id"]];
            [entity.social_accounts addObject:socialEntity];
        }
    }
    return entity;
}
// RB-解除绑定成功以后返回的社交账号数组
+ (NSMutableArray *)getRBAllSocialAccounts:(id)jsonObject andBackArray:(NSMutableArray *)backArray{
    [backArray removeAllObjects];
    NSArray *social_accountsArray = [jsonObject objectForKey:@"social_accounts"];
    NSArray *providerArray = [social_accountsArray valueForKey:@"provider"];
    for (int m = 0; m < [providerArray count]; m++) {
        @autoreleasepool {
            RBKOLSocialEntity *socialEntity = [[RBKOLSocialEntity alloc] init];
            socialEntity.provider = [self checkJson:[social_accountsArray[m] valueForKey:@"provider"]];
            socialEntity.provider_name = [self checkJson:[social_accountsArray[m] valueForKey:@"provider_name"]];
            socialEntity.uid = [self checkJson:[social_accountsArray[m] valueForKey:@"uid"]];
            socialEntity.price = [self checkJson:[social_accountsArray[m] valueForKey:@"price"]];
            socialEntity.username = [self checkJson:[social_accountsArray[m] valueForKey:@"username"]];
            socialEntity.homepage = [self checkJson:[social_accountsArray[m] valueForKey:@"homepage"]];
            socialEntity.avatar_url = [self checkJson:[social_accountsArray[m] valueForKey:@"avatar_url"]];
            socialEntity.search_kol_id = [self checkJson:[social_accountsArray[m] valueForKey:@"search_kol_id"]];
            socialEntity.brief = [self checkJson:[social_accountsArray[m] valueForKey:@"brief"]];
            socialEntity.followers_count = [self checkJson:[social_accountsArray[m] valueForKey:@"followers_count"]];
            socialEntity.reposts_count = [self checkJson:[social_accountsArray[m] valueForKey:@"reposts_count"]];
            socialEntity.statuses_count = [self checkJson:[social_accountsArray[m] valueForKey:@"statuses_count"]];
            socialEntity.kol_id = [self checkJson:[social_accountsArray[m] valueForKey:@"id"]];
            [backArray addObject:socialEntity];
        }
    }
    return backArray;
}
// RB-KOL列表Banner模块
+ (NSMutableArray *)getRBKolBannerListEntity:(id)jsonObject andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"kol_announcements"];
    NSArray *categoryArray = [resultArray valueForKey:@"category"];
    for (int i = 0; i < [categoryArray count]; i++) {
        @autoreleasepool {
            RBKOLBannerEntity *entity = [[RBKOLBannerEntity alloc] init];
            entity.category = [self checkJson:[resultArray[i] valueForKey:@"category"]];
            entity.title = [self checkJson:[resultArray[i] valueForKey:@"title"]];
            entity.link = [self checkJson:[resultArray[i] valueForKey:@"content"]];
            entity.kol_id = [self checkJson:[resultArray[i] valueForKey:@"content"]];
            entity.cover_url = [self checkJson:[resultArray[i] valueForKey:@"cover_url"]];
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-KOL-我的
+ (RBUserDashboardEntity *)getRBKolDashboardDetailEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"kol"];

    RBUserDashboardEntity *entity = [[RBUserDashboardEntity alloc] init];
    //是否隐藏我的钱包
    entity.hide = [self checkJson:[jsonObject objectForKey:@"hide"]];
    entity.detail = [self checkJson:[jsonObject objectForKey:@"detail"]];
    
    entity.iid = [self checkJson:[resultArray valueForKey:@"id"]];
    entity.name = [self checkJson:[resultArray valueForKey:@"name"]];
    entity.avatar_url = [self checkJson:[resultArray valueForKey:@"avatar_url"]];
    entity.role_check_remark = [self checkJson:[resultArray valueForKey:@"role_check_remark"]];
    entity.role_apply_status = [self checkJson:[resultArray valueForKey:@"role_apply_status"]];
    entity.kol_role = [self checkJson:[resultArray valueForKey:@"kol_role"]];
    entity.is_open_indiana = [self checkJson:[jsonObject objectForKey:@"is_open_indiana"]];
    entity.max_campaign_click = [self checkJson:[resultArray valueForKey:@"max_campaign_click"]];
    entity.max_campaign_earn_money = [self checkJson:[resultArray valueForKey:@"max_campaign_earn_money"]];
    entity.campaign_total_income = [self checkJson:[resultArray valueForKey:@"campaign_total_income"]];
    entity.avg_campaign_credit = [self checkJson:[resultArray valueForKey:@"avg_campaign_credit"]];
    entity.has_any_unread_message = [self checkJson:[jsonObject objectForKey:@"has_any_unread_message"]];
    entity.brand_campany_name = [self checkJson:[jsonObject objectForKey:@"brand_campany_name"]];
    entity.logo = [self checkJson:[jsonObject objectForKey:@"logo"]];
    entity.is_show_invite_code = [self checkJson:[jsonObject objectForKey:@"is_show_invite_code"]];
    NSArray * adminsTag = [resultArray valueForKey:@"admintag"];
    if (adminsTag.count>0) {
        entity.admintag = adminsTag;
    }else{
        entity.admintag = @[];
    }
    
    //
    entity.tags = [NSMutableArray new];
    NSArray *tagsArray = [resultArray valueForKey:@"tags"];
    NSArray *nameArray = [tagsArray valueForKey:@"name"];
    for (int j = 0; j < [nameArray count]; j++) {
        @autoreleasepool {
            RBTagEntity *tagEntity = [[RBTagEntity alloc] init];
            tagEntity.name = [self checkJson:[tagsArray[j] valueForKey:@"name"]];
            tagEntity.label = [self checkJson:[tagsArray[j] valueForKey:@"label"]];
            [entity.tags addObject:tagEntity];
        }
    }
    return entity;
}

// RB-CPS模块
// RB-CPS产品列表
+ (NSMutableArray *)getRBCpsProductListEntity:(id)jsonObject
                                 andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"cps_materials"];
    NSArray *idArray = [resultArray valueForKey:@"sku_id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBCpsProductEntity *entity = [[RBCpsProductEntity alloc] init];
            entity.iid = [self checkJson:[resultArray[i] valueForKey:@"id"]];
            entity.sku_id = [self checkJson:[resultArray[i] valueForKey:@"sku_id"]];
            entity.img_url = [self checkJson:[resultArray[i] valueForKey:@"img_url"]];
            entity.material_url = [self checkJson:[resultArray[i] valueForKey:@"material_url"]];
            entity.goods_name = [self checkJson:[resultArray[i] valueForKey:@"goods_name"]];
            entity.shop_id = [self checkJson:[resultArray[i] valueForKey:@"shop_id"]];
            entity.unit_price = [self checkJson:[resultArray[i] valueForKey:@"unit_price"]];
            entity.start_date = [self checkJson:[resultArray[i] valueForKey:@"start_date"]];
            entity.end_date = [self checkJson:[resultArray[i] valueForKey:@"end_date"]];
            entity.kol_commision_wl = [self checkJson:[resultArray[i] valueForKey:@"kol_commision_wl"]];
            entity.category = [self checkJson:[resultArray[i] valueForKey:@"category"]];
            
            entity.category_label = [resultArray[i] valueForKey:@"category_label"];
            [backArray addObject:entity];
        }
    }
    return backArray;
}
// RB-CPS创作列表
+ (NSMutableArray *)getRBCpsCreateListEntity:(id)jsonObject
                                andBackArray:(NSMutableArray *)backArray {
    NSArray *resultArray = [jsonObject objectForKey:@"cps_articles"];
    NSArray *idArray = [resultArray valueForKey:@"id"];
    for (int i = 0; i < [idArray count]; i++) {
        @autoreleasepool {
            RBCpsCreateEntity *entity = [[RBCpsCreateEntity alloc] init];
            entity.iid = [self checkJson:[resultArray[i] valueForKey:@"id"]];
            entity.title = [self checkJson:[resultArray[i] valueForKey:@"title"]];
            entity.cover = [self checkJson:[resultArray[i] valueForKey:@"cover"]];
            entity.show_url = [self checkJson:[resultArray[i] valueForKey:@"show_url"]];
            entity.status = [self checkJson:[resultArray[i] valueForKey:@"status"]];
            entity.end_date = [self checkJson:[resultArray[i] valueForKey:@"end_date"]];
            entity.check_remark = [self checkJson:[resultArray[i] valueForKey:@"check_remark"]];
            entity.material_total_price = [self checkJson:[resultArray[i] valueForKey:@"material_total_price"]];
            entity.writing_forecast_commission = [self checkJson:[resultArray[i] valueForKey:@"writing_forecast_commission"]];
            entity.writing_settled_commission = [self checkJson:[resultArray[i] valueForKey:@"writing_settled_commission"]];
            entity.share_forecast_commission = [self checkJson:[resultArray[i] valueForKey:@"share_forecast_commission"]];
            entity.share_settled_commission = [self checkJson:[resultArray[i] valueForKey:@"share_settled_commission"]];
            entity.cps_article_share_count = [self checkJson:[resultArray[i] valueForKey:@"cps_article_share_count"]];
            //
            NSArray *authorArray = [resultArray[i] valueForKey:@"author"];
            entity.author_iid = [self checkJson:[authorArray valueForKey:@"id"]];
            entity.author_name = [self checkJson:[authorArray valueForKey:@"name"]];
            entity.author_avatar_url = [self checkJson:[authorArray valueForKey:@"avatar_url"]];
            //
            entity.cps_article_shares = [NSMutableArray new];
            NSArray *sharesArray = [jsonObject objectForKey:@"cps_article_shares"];
            NSArray *kolIdArray = [sharesArray valueForKey:@"kol_id"];
            for (int k = 0; k < [kolIdArray count]; k++) {
                @autoreleasepool {
                    RBKOLEntity *kolEntity = [[RBKOLEntity alloc] init];
                    kolEntity.iid = [self checkJson:[sharesArray[k] valueForKey:@"kol_id"]];
                    kolEntity.name = [self checkJson:[sharesArray[k] valueForKey:@"kol_name"]];
                    kolEntity.avatar_url = [self checkJson:[sharesArray[k] valueForKey:@"kol_avatar_url"]];
                    kolEntity.share_forecast_commission = [self checkJson:[sharesArray[k] valueForKey:@"share_forecast_commission"]];
                    kolEntity.share_settled_commission = [self checkJson:[sharesArray[k] valueForKey:@"share_settled_commission"]];
                    [entity.cps_article_shares addObject:kolEntity];
                }
            }
            [backArray addObject:entity];
        }
    }
    return backArray;
}

// RB-CPS创作详情
+ (RBCpsCreateEntity *)getRBCpsCreateDetailEntity:(id)jsonObject {
    NSArray *resultArray = [jsonObject objectForKey:@"cps_article"];
    RBCpsCreateEntity *entity = [[RBCpsCreateEntity alloc] init];
    entity.iid = [self checkJson:[resultArray valueForKey:@"id"]];
    entity.title = [self checkJson:[resultArray valueForKey:@"title"]];
    entity.content = [self checkJson:[resultArray valueForKey:@"content"]];
    entity.cover = [self checkJson:[resultArray valueForKey:@"cover"]];
    entity.show_url = [self checkJson:[resultArray valueForKey:@"show_url"]];
    entity.status = [self checkJson:[resultArray valueForKey:@"status"]];
    entity.check_remark = [self checkJson:[resultArray valueForKey:@"check_remark"]];
    entity.material_total_price = [self checkJson:[resultArray valueForKey:@"material_total_price"]];
    entity.writing_forecast_commission = [self checkJson:[resultArray valueForKey:@"writing_forecast_commission"]];
    entity.writing_settled_commission = [self checkJson:[resultArray valueForKey:@"writing_settled_commission"]];
    entity.cps_article_share_count = [self checkJson:[resultArray valueForKey:@"cps_article_share_count"]];
    //
    NSArray *authorArray = [resultArray valueForKey:@"author"];
    entity.author_iid = [self checkJson:[authorArray valueForKey:@"id"]];
    entity.author_name = [self checkJson:[authorArray valueForKey:@"name"]];
    entity.author_avatar_url = [self checkJson:[authorArray valueForKey:@"avatar_url"]];
    //
    entity.cps_article_shares = [NSMutableArray new];
    NSArray *sharesArray = [resultArray valueForKey:@"cps_article_shares"];
    NSArray *kolIdArray = [sharesArray valueForKey:@"kol_id"];
    for (int k = 0; k < [kolIdArray count]; k++) {
        @autoreleasepool {
            RBKOLEntity *kolEntity = [[RBKOLEntity alloc] init];
            kolEntity.iid = [self checkJson:[sharesArray[k] valueForKey:@"kol_id"]];
            kolEntity.name = [self checkJson:[sharesArray[k] valueForKey:@"kol_name"]];
            kolEntity.avatar_url = [self checkJson:[sharesArray[k] valueForKey:@"kol_avatar_url"]];
            kolEntity.share_forecast_commission = [self checkJson:[sharesArray[k] valueForKey:@"share_forecast_commission"]];
            kolEntity.share_settled_commission = [self checkJson:[sharesArray[k] valueForKey:@"share_settled_commission"]];
            [entity.cps_article_shares addObject:kolEntity];
        }
    }
    return entity;
}
+ (NSMutableArray *)getRBMobile:(id)jsonObject AndBackArray:(NSMutableArray *)backArray AndPhoneArray:(NSMutableArray*)PhoneArray{
    NSArray * arr = [jsonObject objectForKey:@"kol_users"];
    for (NSInteger i = 0; i<arr.count; i++) {
        @autoreleasepool {
            RBMobileEntity * entity = [[RBMobileEntity alloc]init];
            entity.mobile = [self checkJson:[arr[i] objectForKey:@"mobile_number"]];
            entity.status = [self checkJson:[arr[i] objectForKey:@"status"]];
            for (NSInteger j = 0; j<PhoneArray.count; j++) {
                if ([[PhoneArray[i] objectForKey:@"mobile"] isEqualToString:entity.mobile]) {
                    entity.name = [self checkJson:[PhoneArray[i] objectForKey:@"name"]];
                }
            }
            [backArray addObject:entity];
        }
    }
    return backArray;
}
+ (NSMutableArray *)getAllTudiList:(id)jsonObject AndBackArray:(NSMutableArray *)backArray{
    NSArray * arr = [jsonObject objectForKey:@"list"];
    for (NSInteger i = 0; i < arr.count; i ++) {
        @autoreleasepool {
            RBTudiEntity * entity = [[RBTudiEntity alloc]init];
            entity.amount = [self checkJson:[arr[i] objectForKey:@"amount"]];
            entity.avatar_url = [self checkJson:[arr[i] objectForKey:@"avatar_url"]];
            entity.campaign_invites_count = [self checkJson:[arr[i] objectForKey:@"campaign_invites_count"]];
            entity.kol_id = [self checkJson:[arr[i] objectForKey:@"kol_id"]];
            entity.kol_name = [self checkJson:[arr[i] objectForKey:@"kol_name"]];
            [backArray addObject:entity];
        }
    }
    return  backArray;
}
@end
