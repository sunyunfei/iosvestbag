//
//  main.m
//  RB
//
//  Created by AngusNi on 15/3/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RBAppDelegate.h"

int main(int argc, char *argv[]) {
	@autoreleasepool {
		return UIApplicationMain(argc, argv, nil, NSStringFromClass([RBAppDelegate class]));
	}
}
