//
//  UIWindow+Additions.h
//

#import <UIKit/UIKit.h>

@interface UIWindow (Additions)

/**
 * Searches the view hierarchy recursively for the first responder, starting
 * with this window.
 */
- (UIView *)findFirstResponder;

/**
 * Searches the view hierarchy recursively for the first responder, starting
 * with topView.
 */
- (UIView *)findFirstResponderInView:(UIView *)topView;

@end
