//
//  UIView+Badge.m
//

#import "UIView+Badge.h"
#import "Utils.h"

@implementation UIView (Badge)

- (void)setBadgeCenter:(CGPoint)point{
    UIView *tmpView = [self viewWithTag:10000];
    if (tmpView) {
        [tmpView setCenter:point];
    }
}

- (void)setBadgeText:(NSString *)text center:(CGPoint)point andTag:(int)tag{
    UILabel *tmpView = (UILabel*)[self.superview viewWithTag:10000+tag];
    [tmpView removeFromSuperview];
    tmpView.hidden=YES;
    tmpView=nil;
    if (text.length == 0) {
        return;
    }
    if ([text intValue] > 50) {
        text = @"N";
    }
    UILabel*badgeLabel=[[UILabel alloc]initWithFrame:CGRectMake(point.x, point.y, 100, 16)];
    badgeLabel.font=[UIFont systemFontOfSize:12.0];
    badgeLabel.backgroundColor=[UIColor redColor];
    badgeLabel.textColor=[UIColor whiteColor];
    badgeLabel.textAlignment=NSTextAlignmentCenter;
    badgeLabel.tag = 10000+tag;
    [self.superview addSubview:badgeLabel];
    // 显示小圆点
    if ([text intValue] == 0) {
        badgeLabel.text=@"";
        badgeLabel.frame=CGRectMake(point.x, point.y,8.0,8.0);
        badgeLabel.layer.cornerRadius=badgeLabel.frame.size.width/2.0;
        badgeLabel.layer.borderColor=[[UIColor whiteColor]CGColor];
        badgeLabel.layer.borderWidth=1.0f;
        badgeLabel.layer.masksToBounds=YES;
        return;
    }
    badgeLabel.text=text;
    CGSize badgeLabelSize=[Utils getUIFontSizeFitW:badgeLabel];
    if(badgeLabelSize.width<badgeLabelSize.height){
        badgeLabelSize.width=badgeLabelSize.height;
    }
    badgeLabel.frame=CGRectMake(point.x, point.y, badgeLabelSize.width+3, badgeLabelSize.height+3);
    badgeLabel.layer.cornerRadius=badgeLabel.frame.size.width/2.0;
    badgeLabel.layer.masksToBounds=YES;
}

@end
