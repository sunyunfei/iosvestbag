//
//  UIView+Badge.h
//

#import <UIKit/UIKit.h>
#import "NIBadgeView.h"

@interface UIView (Badge)
- (void)setBadgeText:(NSString *)text center:(CGPoint)point andTag:(int)tag;
- (void)setBadgeCenter:(CGPoint)point;
@end
