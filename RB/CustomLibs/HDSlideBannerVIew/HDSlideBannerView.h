//
//  HDSlideBannerView.h
//  HDSlideBannerView
//


#import <UIKit/UIKit.h>

@protocol HDSlideBannerViewDelegate <NSObject>

@optional
- (void)slideBannerTapIndex:(NSInteger)index;

@end




@interface HDSlideBannerView : UIView

@property (weak, nonatomic) id<HDSlideBannerViewDelegate> delegate;

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIPageControl *pageControl;

- (void)setImgList:(NSArray *)imgList;

- (void)openAutoSlideWithTimeInterval:(NSTimeInterval)timeInterval;

- (void)closeAutoSlide;

@end
