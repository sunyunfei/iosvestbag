//
//  UILabel+init.h
//  优梦优
//
//  Created by 刘文 on 16/7/13.
//  Copyright © 2016年 孙云飞. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
@interface UILabel (init)
- (instancetype)initWithFrame:(CGRect)frame
                         text:(NSString *)text
                         font:(UIFont *)font
                textAlignment:(NSTextAlignment)textAlignment
                    textColor:(UIColor *)textColor
              backgroundColor:(UIColor *)backgroundColor
                numberOfLines:(NSInteger)numberOfLines;
- (instancetype)initWithText:(NSString *)text
                        font:(UIFont *)font
               textAlignment:(NSTextAlignment)textAlignment
                   textColor:(UIColor *)textColor
             backgroundColor:(UIColor *)backgroundColor
               numberOfLines:(NSInteger)numberOfLines;
@end
