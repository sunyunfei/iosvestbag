//
//  TBTabBarController.m
//  TabbarBeyondClick
//
//  Created by 卢家浩 on 2017/4/17.
//  Copyright © 2017年 lujh. All rights reserved.
//

#import "TBTabBarController.h"
#import "TBTabBar.h"
#import "RBTabCampaignViewController.h"
#import "RBTabCpsViewController.h"
#import "RBTabKolViewController.h"
#import "RBTabUserViewController.h"
#import "InfluenceController.h"
#import "UpInfluenceViewController.h"
#import "RBInfluenceLoadingViewController.h"
#import "RBInfluenceOrcaController.h"
#import "RBInfluenceDetailViewController.h"
#import "RBTabFindViewController.h"
#import "RBAppDelegate.h"

@interface TBTabBarController ()

@end

@implementation TBTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    // 初始化所有控制器
    [self setUpChildVC];
    
    // 创建tabbar中间的tabbarItem
    [self setUpMidelTabbarItem];
    
}

#pragma mark -创建tabbar中间的tabbarItem 

- (void)setUpMidelTabbarItem {

//    TBTabBar *tabBar = [[TBTabBar alloc] init];
//    [self setValue:tabBar forKey:@"tabBar"];
    [[UITabBar appearance] setBarTintColor:[Utils getUIColorWithHexString:SysColorWhite]];
    [[UITabBar appearance] setTintColor:[Utils getUIColorWithHexString:SysColorYellow]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorYellow]} forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorBlack]} forState:UIControlStateNormal];
    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"tabbar_back.png"]];
    CGRect rect = CGRectMake(0, 0, ScreenWidth, 0.5);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,
                                   [Utils getUIColorWithHexString:@"e5e5e5"].CGColor);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [[UITabBar appearance] setShadowImage:img];
    [[UITabBar appearance] setBackgroundImage:[[UIImage alloc]init]];
//    __weak typeof(self) weakSelf = self;
//    [tabBar setDidClickPublishBtn:^{
//        //如果用户没有测试过影响力，就弹出引导界面
//        if ([LocalService getRBLocalDataUserPrivateToken] == nil || [JsonService isRBUserVisitor] == YES){
//            RBLoginViewController*toview = [[RBLoginViewController alloc]init];
//
//            //toview.hidesBottomBarWhenPushed = YES;
//            //[self.navigationController pushViewController:toview animated:YES];
//            toview.isFirst = @"first";
//            UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:toview];
//            [weakSelf presentViewController:nav animated:YES completion:nil];
//            return;
//        }else if ([LocalService getRBFirstInfluenceenter] == nil) {
//
//            RBInfluenceLoadingViewController *hmpositionVC = [[RBInfluenceLoadingViewController alloc] init];
//           // UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:hmpositionVC];
//            [weakSelf presentViewController:hmpositionVC animated:YES completion:nil];
//            return;
//        } else{
//            weakSelf.selectedIndex = 1;
//            NSLog(@"hehe");
//
//        }
//  }];

}

#pragma mark -初始化所有控制器 

- (void)setUpChildVC {
//    RBTabKolViewController *homeVC = [[RBTabKolViewController alloc] init];
//    [self setChildVC:homeVC title:NSLocalizedString(@"R0005",@"首页") image:@"tabbar_0_l@2x.png" selectedImage:@"tabbar_0_h@2x.png"];
    
    RBTabCampaignViewController *fishpidVC = [[RBTabCampaignViewController alloc] init];
    [self setChildVC:fishpidVC title:NSLocalizedString(@"R0004",@"活动") image:@"tabbar_1_l@2x.png" selectedImage:@"tabbar_1_h@2x.png"];
    
    RBInfluenceDetailViewController *influenceVC = [[RBInfluenceDetailViewController alloc] init];
    influenceVC.mark = @"mine";
    [self setChildVC:influenceVC title:NSLocalizedString(@"R5197",@"影响力") image:@"InfluenceNoSelect" selectedImage:@"InfluenceSelect"];
    RBTabFindViewController *messageVC = [[RBTabFindViewController alloc] init];
    [self setChildVC:messageVC title:NSLocalizedString(@"R0001",@"发现") image:@"tabbar_2_l@2x.png" selectedImage:@"tabbar_2_h@2x.png"];
    
    RBTabUserViewController *myVC = [[RBTabUserViewController alloc] init];
    [self setChildVC:myVC title:NSLocalizedString(@"R0003",@"我的") image:@"tabbar_3_l@2x.png" selectedImage:@"tabbar_3_h@2x.png"];
}

- (void)setChildVC:(UIViewController *)childVC title:(NSString *) title image:(NSString *) image selectedImage:(NSString *) selectedImage {
    
    childVC.tabBarItem.title = title;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[NSForegroundColorAttributeName] = [UIColor blackColor];
    dict[NSFontAttributeName] = [UIFont systemFontOfSize:10];
    [childVC.tabBarItem setTitleTextAttributes:dict forState:UIControlStateNormal];
    childVC.tabBarItem.image = [[UIImage imageNamed:image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVC.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:childVC];
    [self addChildViewController:nav];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    if (viewController == self.viewControllers[1]) {
        //如果用户没有测试过影响力，就弹出引导界面
        if ([LocalService getRBLocalDataUserPrivateToken] == nil || [JsonService isRBUserVisitor] == YES){
//            RBLoginViewController*toview = [[RBLoginViewController alloc]init];
//
//            //toview.hidesBottomBarWhenPushed = YES;
//            //[self.navigationController pushViewController:toview animated:YES];
//            toview.isFirst = @"first";
//            UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:toview];
//            [self presentViewController:nav animated:YES completion:nil];
            RBAppDelegate * appdelegate = RBAPPDELEGATE;
            [appdelegate loadViewController:0];
            return NO;
        }else if ([LocalService getRBFirstInfluenceenter] == nil) {
            
//            RBInfluenceLoadingViewController *hmpositionVC = [[RBInfluenceLoadingViewController alloc] init];
//            // UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:hmpositionVC];
//            [self presentViewController:hmpositionVC animated:YES completion:nil];
//            RBAppDelegate * appdelegate = RBAPPDELEGATE;
//            [appdelegate loadViewController:4];
            return YES;
        } else{
            NSLog(@"hehe");
            return YES;
        }
    }
    return YES;
}
//- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
//{
//    NSLog(@"item name = %@", item.title);
//    NSInteger index = [self.tabBar.items indexOfObject:item];
//    if (index ==1) {
//        //如果用户没有测试过影响力，就弹出引导界面
//        if ([LocalService getRBLocalDataUserPrivateToken] == nil || [JsonService isRBUserVisitor] == YES){
//            RBLoginViewController*toview = [[RBLoginViewController alloc]init];
//
//            //toview.hidesBottomBarWhenPushed = YES;
//            //[self.navigationController pushViewController:toview animated:YES];
//            toview.isFirst = @"first";
//            UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:toview];
//            [self presentViewController:nav animated:YES completion:nil];
//            return;
//        }else if ([LocalService getRBFirstInfluenceenter] == nil) {
//
//            RBInfluenceLoadingViewController *hmpositionVC = [[RBInfluenceLoadingViewController alloc] init];
//            // UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:hmpositionVC];
//            [self presentViewController:hmpositionVC animated:YES completion:nil];
//            return;
//        } else{
//            self.selectedIndex = 1;
//            NSLog(@"hehe");
//
//        }
//    }
//    //[self animationWithIndex:index];
//    if([item.title isEqualToString:@"影响力"])
//    {
//        // 也可以判断标题,然后做自己想做的事<img alt="得意" src="http://static.blog.csdn.net/xheditor/xheditor_emot/default/proud.gif" />
//    }
//}
- (void)animationWithIndex:(NSInteger) index {
    NSMutableArray * tabbarbuttonArray = [NSMutableArray array];
    for (UIView *tabBarButton in self.tabBar.subviews) {
        if ([tabBarButton isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            [tabbarbuttonArray addObject:tabBarButton];
        }
    }
    CABasicAnimation*pulse = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulse.timingFunction= [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    pulse.duration = 0.2;
    pulse.repeatCount= 1;
    pulse.autoreverses= YES;
    pulse.fromValue= [NSNumber numberWithFloat:0.7];
    pulse.toValue= [NSNumber numberWithFloat:1.3];
    [[tabbarbuttonArray[index] layer] 
     addAnimation:pulse forKey:nil]; 
}

@end
