//
//  InsetsTextField.m
//

#import "InsetsTextField.h"

@implementation InsetsTextField
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];

    if (self) {
        [self setAutocorrectionType:UITextAutocorrectionTypeNo];
        [self setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    }

    return self;
}

// 控制 placeHolder 的位置,左右缩 5
- (CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 5, 5);
}

// 控制文本的位置,左右缩 5
- (CGRect)editingRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 5, 5);
}

//-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
//    UIMenuController *menuController = [UIMenuController sharedMenuController];
//    if (menuController) {
//        [UIMenuController sharedMenuController].menuVisible = NO;
//    }
//    return NO;
//}

@end
