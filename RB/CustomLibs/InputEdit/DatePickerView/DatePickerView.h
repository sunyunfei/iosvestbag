//
//  DatePickerView.h

//
//  Created by AngusNi on 15/4/7.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"
@class DatePickerView;
@protocol DatePickerViewDelegate <NSObject>
- (void)DatePickerViewFinish:(DatePickerView *)sender;
@end

@interface DatePickerView : UIView
@property (nonatomic, assign) int whichTag;
@property (nonatomic, strong) UIDatePicker*datePicker;
@property (nonatomic, weak) id <DatePickerViewDelegate>   delegate;
- (void)ageWithDateOfBirth;
@end
