//
//  DatePickerView.m

//
//  Created by AngusNi on 15/4/7.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "DatePickerView.h"
#import "Service.h"

@implementation DatePickerView
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        //
        self.datePicker =
            [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 44, frame.size.width, 216)];
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        self.datePicker.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self addSubview:self.datePicker];
        //
        UIToolbar*inputAccessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,[[UIScreen mainScreen]bounds].size.width,44)];
        inputAccessoryView.barTintColor = [UIColor whiteColor];
        //
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"R1000", @"完成") style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
        [doneBtn setTintColor:[Utils getUIColorWithHexString:SysColorBlue]];
        //
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [inputAccessoryView setItems:@[flexibleSpaceLeft, doneBtn]];
        //
        UIView*lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 43.5f, [[UIScreen mainScreen]bounds].size.width, 0.5f)];
        lineView.backgroundColor = [Utils getUIColorWithHexString:@"8c8c8c"];
        [inputAccessoryView addSubview:lineView];
        //
        [self addSubview:inputAccessoryView];
    }

    return self;
}

- (void)done:(id)sender {
    if ([self.delegate respondsToSelector:@selector(DatePickerViewFinish:)]) {
        [self.delegate DatePickerViewFinish:self];
    }
}

- (void)ageWithDateOfBirth {
    // 获取系统当前 年月日
    NSDateComponents    *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger           currentDateYear = [components2 year];
    // 16岁
    NSDateComponents *components = [[NSDateComponents alloc] init];

    [components setDay:1];
    [components setMonth:1];
    [components setYear:currentDateYear - 16];
    // 99岁
    NSDateComponents *components1 = [[NSDateComponents alloc] init];
    [components1 setDay:1];
    [components1 setMonth:1];
    [components1 setYear:currentDateYear - 99];
    //
    NSCalendar  *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate      *date = [gregorian dateFromComponents:components];
    NSDate      *date1 = [gregorian dateFromComponents:components1];
    //
    self.datePicker.date = date;
    self.datePicker.minimumDate = date1;
    self.datePicker.maximumDate = date;
}

@end
