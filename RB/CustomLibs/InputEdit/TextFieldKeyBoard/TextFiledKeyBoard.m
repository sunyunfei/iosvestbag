//
//  TextFiledKeyBoard.m
//

#import "TextFiledKeyBoard.h"
#import "Service.h"

@implementation TextFiledKeyBoard

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.barTintColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor clearColor];
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"R1000", @"完成") style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
        [doneBtn setTintColor:[Utils getUIColorWithHexString:SysColorBlue]];
        //
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [self setItems:@[flexibleSpaceLeft, doneBtn]];
        //
        UIView*lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 43.5f, [[UIScreen mainScreen]bounds].size.width, 0.5f)];
        lineView.backgroundColor = [Utils getUIColorWithHexString:@"8c8c8c"];
        [self addSubview:lineView];
    }
    return self;
}

- (void)done:(id)sender {
    [self.delegateT TextFiledKeyBoardFinish:self];
}

@end
