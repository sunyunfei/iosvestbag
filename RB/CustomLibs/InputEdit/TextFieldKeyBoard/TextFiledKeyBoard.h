//
//  TextFiledKeyBoard.h
//

#import <UIKit/UIKit.h>
#import "InsetsTextField.h"
#import "Utils.h"
@class TextFiledKeyBoard;
@protocol TextFiledKeyBoardDelegate <NSObject>
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender;
@end
@interface TextFiledKeyBoard : UIToolbar
@property(nonatomic, assign) int whichTag;
@property(nonatomic, strong) InsetsTextField*inputTextFiled;
@property(nonatomic, strong) id <TextFiledKeyBoardDelegate>  delegateT;
@end
