//
//  TextViewKeyBoard.h
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import "PlaceholderTextView.h"

@class TextViewCpsKeyBoard;
@protocol TextViewCpsKeyBoardDelegate <NSObject>
- (void)TextViewCpsKeyBoardLeft:(TextViewCpsKeyBoard *)sender;
- (void)TextViewCpsKeyBoardRight:(TextViewCpsKeyBoard *)sender;
@end


@interface TextViewCpsKeyBoard : UIToolbar
@property(nonatomic, strong) PlaceholderTextView *inputTextView;
@property(nonatomic, strong) id <TextViewCpsKeyBoardDelegate>  delegateP;
@end
