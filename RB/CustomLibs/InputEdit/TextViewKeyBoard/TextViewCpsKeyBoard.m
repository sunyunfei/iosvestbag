//
//  TextViewCpsKeyBoard.m
//

#import "TextViewCpsKeyBoard.h"
#import "Service.h"

@implementation TextViewCpsKeyBoard

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.barTintColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor clearColor];
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        //
        UIButton*leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        leftBtn.frame = CGRectMake(0, 0, self.width/2.0, 44.0);
        leftBtn.titleLabel.font = font_15;
        [leftBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
        [leftBtn setTitle:NSLocalizedString(@"R8000",@"插入图片") forState:UIControlStateNormal];
        [leftBtn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:leftBtn];
        //
        UIView*gapView = [[UIView alloc]initWithFrame:CGRectMake(self.width/2.0, 5.0, 0.5, 35.0)];
        gapView.backgroundColor = [Utils getUIColorWithHexString:@"8c8c8c"];
        [self addSubview:gapView];
        //
        UIButton*rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.frame = CGRectMake(self.width/2.0, 0, self.width/2.0, 44.0);
        rightBtn.titleLabel.font = font_15;
        [rightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
        [rightBtn setTitle:NSLocalizedString(@"R8001",@"插入商品") forState:UIControlStateNormal];
        [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:rightBtn];
        //
        UIView*lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 43.5f, [[UIScreen mainScreen]bounds].size.width, 0.5f)];
        lineView.backgroundColor = [Utils getUIColorWithHexString:@"8c8c8c"];
        [self addSubview:lineView];
    }
    return self;
}

- (void)leftBtnAction:(UIButton*)sender {
    [self.delegateP TextViewCpsKeyBoardLeft:self];
}

- (void)rightBtnAction:(UIButton*)sender {
    [self.delegateP TextViewCpsKeyBoardRight:self];
}

@end


