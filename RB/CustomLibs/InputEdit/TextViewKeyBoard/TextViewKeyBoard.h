//
//  TextViewKeyBoard.h
//

#import <UIKit/UIKit.h>
#import "Utils.h"
#import "PlaceholderTextView.h"
@class TextViewKeyBoard;
@protocol TextViewKeyBoardDelegate <NSObject>
- (void)TextViewKeyBoardFinish:(TextViewKeyBoard *)sender;
@end
@interface TextViewKeyBoard : UIToolbar{}
@property(nonatomic, strong) PlaceholderTextView            *inputTextView;
@property(nonatomic, strong) id <TextViewKeyBoardDelegate>  delegateP;
@end
