//
//  PlaceholderTextView.h
//
#import <UIKit/UIKit.h>
@interface PlaceholderTextView : UITextView
@property(nonatomic, strong) NSString   *placeholder;
@property(nonatomic, strong) UIColor    *placeholderColor;
@property(nonatomic, strong) UIFont     *placeholderFont;
@end
