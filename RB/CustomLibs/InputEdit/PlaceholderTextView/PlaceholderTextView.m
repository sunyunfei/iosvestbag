//
//  PlaceholderTextView.m
//

#import "PlaceholderTextView.h"

@interface PlaceholderTextView () {
    UILabel *PlaceholderLabel;
}

@end
@implementation PlaceholderTextView

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChange:) name:UITextViewTextDidChangeNotification object:self];
        float left = 5, top = 10, hegiht = 20;
        self.placeholderColor = [UIColor lightGrayColor];
        PlaceholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top
                                                                     , CGRectGetWidth(self.frame) - 2 * left, hegiht)];
        [self addSubview:PlaceholderLabel];
        PlaceholderLabel.text = self.placeholder;

    }
    return self;
}

- (void)setPlaceholder:(NSString *)placeholder {
    if ((placeholder.length == 0) || [placeholder isEqualToString:@""]) {
        PlaceholderLabel.hidden = YES;
    } else {
        PlaceholderLabel.text = placeholder;
    }
    PlaceholderLabel.font = self.placeholderFont ? self.placeholderFont : self.font;
    PlaceholderLabel.textColor = self.placeholderColor;
    _placeholder = placeholder;
}

- (void)didChange:(NSNotification *)noti {
    if ((self.placeholder.length == 0) || [self.placeholder isEqualToString:@""]) {
        PlaceholderLabel.hidden = YES;
    }
    if (self.text.length > 0) {
        PlaceholderLabel.hidden = YES;
    } else {
        PlaceholderLabel.hidden = NO;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [PlaceholderLabel removeFromSuperview];
}

@end
