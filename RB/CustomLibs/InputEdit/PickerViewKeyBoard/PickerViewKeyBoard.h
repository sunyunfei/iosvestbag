//
//  PickerViewKeyBoard.h
//

#import <UIKit/UIKit.h>
#import "InsetsTextField.h"
#import "Utils.h"
@class PickerViewKeyBoard;
@protocol PickerViewKeyBoardDelegate <NSObject>
- (void) PickerViewKeyBoardFinish:(PickerViewKeyBoard*)sender;
@end
@interface PickerViewKeyBoard:UIView
<UIPickerViewDataSource, UIPickerViewDelegate>
@property(nonatomic, assign) int whichTag;
@property(nonatomic, strong) UITextField*inputTextFiled;
@property(nonatomic, strong) NSString*value;
@property(nonatomic, strong) NSArray*values;
@property(nonatomic, strong) UIPickerView*picker;
@property(nonatomic, strong) id<PickerViewKeyBoardDelegate> delegate;
@end
