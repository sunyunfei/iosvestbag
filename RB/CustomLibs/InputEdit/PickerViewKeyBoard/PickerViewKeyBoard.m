//
//  PickerViewKeyBoard.m
//

#import "PickerViewKeyBoard.h"
#import "Service.h"

@implementation PickerViewKeyBoard
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[UIColor clearColor];
        //
        self.picker=[[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, [[UIScreen mainScreen]bounds].size.width,216)];
        self.picker.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.picker.delegate = self;
		self.picker.dataSource = self;
        self.picker.showsSelectionIndicator = YES;
        self.picker.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self addSubview:self.picker];
        //
        UIToolbar*inputAccessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width,44)];
        inputAccessoryView.barTintColor = [UIColor whiteColor];
        //
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"R1000", @"完成") style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
        [doneBtn setTintColor:[Utils getUIColorWithHexString:SysColorBlue]];
        //
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        //
        [inputAccessoryView setItems:@[flexibleSpaceLeft, doneBtn]];
        //
        UIView*lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 43.5f, [[UIScreen mainScreen]bounds].size.width, 0.5f)];
        lineView.backgroundColor = [Utils getUIColorWithHexString:@"8c8c8c"];
        [inputAccessoryView addSubview:lineView];
        //
        [self addSubview:inputAccessoryView];
    }
    return self;
}

- (void)done:(id)sender{
    [self.delegate PickerViewKeyBoardFinish:self];
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    self.value = [NSString stringWithFormat:@"%@",[self.values objectAtIndex:0]];
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.values count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [self.values objectAtIndex:row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
	return 50.0f;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
	return [[UIScreen mainScreen]bounds].size.width;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.value = [NSString stringWithFormat:@"%@",[self.values objectAtIndex:row]];
}

@end
