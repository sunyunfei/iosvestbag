
#import "Utils.h"
@class ASHUDView;
@protocol ASHUDViewDelegate <NSObject>
@optional
- (void)ASHUDViewHide:(ASHUDView *)hudView;
@end

@interface ASHUDView : UIView
- (void)show;
- (void)dismiss;
- (void)showWithStatus:(NSString *)status;
- (void)showSuccessWithStatus:(NSString *)status;
- (void)showErrorWithStatus:(NSString *)status;

@property (nonatomic, weak)  id<ASHUDViewDelegate> delegate;
@property (nonatomic, strong) UIView *superView;
@property (nonatomic, strong) UIToolbar *hudToolbar;
@property (nonatomic, strong) UIActivityIndicatorView *hudActivityIV;
@property (nonatomic, strong) UIImageView *hudIV;
@property (nonatomic, strong) UILabel *hudLabel;
@property (nonatomic, strong) NSTimer *fadeOutTimer;

@end
