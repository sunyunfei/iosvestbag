#import "RBHUDView.h"

@implementation RBHUDView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self) {
        self.hudIV = nil;
        self.hudLabel = nil;
        self.hudActivity = nil;
        self.alpha = 0;
    }
    return self;
}

- (UIControl *)overlayView {
    if(!_overlayView) {
        CGRect windowBounds = [UIApplication sharedApplication].keyWindow.bounds;
        _overlayView = [[UIControl alloc] initWithFrame:windowBounds];
        _overlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _overlayView.backgroundColor = [UIColor clearColor];
    }
    return _overlayView;
}

- (void)dismiss {
    [self hudHide];
}

- (void)showWithStatusOverlay:(NSString *)status {
    if ([self.senderView.subviews containsObject:self.overlayView]) {
        [self.overlayView removeFromSuperview];
    }
    [self.senderView addSubview:self.overlayView];
    [self hudMake:status imgage:nil spin:YES hide:NO];
}

- (void)showOverlay {
    if ([self.senderView.subviews containsObject:self.overlayView]) {
        [self.overlayView removeFromSuperview];
    }
    [self.senderView addSubview:self.overlayView];
    [self hudMake:@"" imgage:nil spin:YES hide:NO];
}

- (void)show {
    [self hudMake:@"" imgage:nil spin:YES hide:NO];
}

- (void)showWithStatus:(NSString *)status {
    [self hudMake:status imgage:nil spin:YES hide:NO];
}

- (void)showSuccessWithStatus:(NSString *)status {
    [self hudMake:status
           imgage:[UIImage imageNamed:@"hud_success.png"]
             spin:NO
             hide:YES];
}

- (void)showErrorWithStatus:(NSString *)status {
    [self hudMake:status
           imgage:[UIImage imageNamed:@"hud_error.png"]
             spin:NO
             hide:YES];
}

- (void)hudMake:(NSString *)status
         imgage:(UIImage *)img
           spin:(BOOL)spin
           hide:(BOOL)hide {
    [self hudCreate];
    [self.hudActivity stopAnimating];

    self.hudLabel.text = status;
    self.hudLabel.hidden = (status == nil) ? YES : NO;
    
    self.hudIV.image = img;
    self.hudIV.hidden = (img == nil) ? YES : NO;
    
    [self hudSize];
    [self hudShow];

    if (spin) {
        [self.hudActivity startAnimating];
    } else {
        self.hudActivity = nil;
        [self.hudActivity stopAnimating];
    }
    
    if (hide) {
        NSTimeInterval duration = self.hudLabel.text.length * 0.04 +1.5;
        self.fadeOutTimer = [NSTimer timerWithTimeInterval:duration target:self selector:@selector(hudHide) userInfo:nil repeats:NO];
        [[NSRunLoop mainRunLoop] addTimer:self.fadeOutTimer forMode:NSRunLoopCommonModes];
    }
}

- (void)hudCreate {
    self.layer.cornerRadius = 10.0;
    self.clipsToBounds = NO;
    self.backgroundColor = [UIColor whiteColor];
    self.layer.shadowColor = [[UIColor blackColor]CGColor];
    self.layer.shadowOffset = CGSizeMake(10,10);
    self.layer.shadowOpacity = 0.1;
    self.layer.shadowRadius = 6;
    
    if ([self.senderView.subviews containsObject:self]) {
        [self removeFromSuperview];
    }
    [self.senderView addSubview:self];
    
    if (self.hudActivity == nil) {
        self.hudActivity = [[AMPActivityIndicator alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        self.hudActivity.barColor = [Utils getUIColorWithHexString:SysColorBlue];
        [self addSubview:self.hudActivity];
    }

    if (self.hudIV == nil) {
        self.hudIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [self addSubview:self.hudIV];
    }
    
    if (self.hudLabel == nil) {
        self.hudLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.hudLabel.font = font_(14.0);
        self.hudLabel.textColor = [UIColor blackColor];
        self.hudLabel.backgroundColor = [UIColor clearColor];
        self.hudLabel.textAlignment = NSTextAlignmentCenter;
        self.hudLabel.numberOfLines = 0;
        [self addSubview:self.hudLabel];
    }
}

- (void)hudDestroy {
    [self.hudActivity stopAnimating];
    [self.showTimer invalidate];
    self.showTimer = nil;
    [self.fadeOutTimer invalidate];
    self.fadeOutTimer = nil;
    [self.hudLabel removeFromSuperview];
    self.hudLabel = nil;
    [self.hudIV removeFromSuperview];
    self.hudIV = nil;
    [self.hudActivity removeFromSuperview];
    self.hudActivity = nil;
    [self.overlayView removeFromSuperview];
    self.overlayView = nil;
    [self removeFromSuperview];
}

- (void)hudSize {
    CGRect labelRect = CGRectZero;
    if (self.hudLabel.text != nil) {
        CGSize labelSize = [Utils getUIFontSizeFitW:self.hudLabel];
        float w = labelSize.width + 20.0;
        if (w < 100.0) {
            w = 100.0;
        }
        if (w > [UIScreen mainScreen].bounds.size.width-40) {
            w = [UIScreen mainScreen].bounds.size.width-40;
        }
        float gap = (100.0 -21.0-37.0)/3.0;
        float h = gap*3.0+21.0+37.0;
        labelRect = CGRectMake((w - labelSize.width) / 2.0, gap*2.0+37.0, labelSize.width, 21.0);
        //
        self.hudIV.frame = CGRectMake((w-37.0)/2.0, gap+5.0, 37, 37);
        self.hudActivity.frame = CGRectMake((w-30.0)/2.0, gap+5.0, 30.0, 30.0);
        self.hudActivity.barWidth = 4.0f;
        self.hudActivity.barHeight = 12.0f;
        self.hudActivity.aperture = 18.0f;
        
        self.frame =
        CGRectMake((self.senderView.frame.size.width - w) / 2.0,
                   (self.senderView.frame.size.height - h) / 2.0, w, h);
        //
        if (self.hudLabel.text.length==0) {
            w = 100.0;
            h = 100.0;
            self.hudIV.frame = CGRectMake((w-37.0)/2.0, (h-37.0)/2.0, 37.0, 37.0);
            self.hudActivity.frame = CGRectMake((w-30.0)/2.0, (h-30.0)/2.0, 30.0, 30.0);
            self.hudActivity.barWidth = 6.0f;
            self.hudActivity.barHeight = 20.0f;
            self.hudActivity.aperture = 25.0f;
            self.frame =
            CGRectMake((self.senderView.frame.size.width - w) / 2.0,
                       (self.senderView.frame.size.height - h) / 2.0, w, h);
            //
            self.layer.borderWidth = 0;
            self.layer.cornerRadius = 0;
            self.backgroundColor = [UIColor clearColor];
        }
    }
    self.hudLabel.frame = labelRect;
}

- (void)hudShow {
    if (self.alpha == 0) {
        [UIView animateWithDuration:0.15
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseOut
                         animations:^{
                             self.alpha = 1;
                         }
                         completion:^(BOOL finished){
                         }];
    }
}

- (void)hudHide {
    if (self.alpha == 1) {
        [UIView animateWithDuration:0.15
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseIn
                         animations:^{
                         }
                         completion:^(BOOL finished) {
                             [self hudDestroy];
                             self.alpha = 0;
                         }];
    }
}

@end
