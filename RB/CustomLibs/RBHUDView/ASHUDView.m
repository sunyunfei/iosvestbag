#import "ASHUDView.h"
#import "Service.h"

@implementation ASHUDView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self) {
        self.hudToolbar = nil;
        self.hudActivityIV = nil;
        self.hudIV = nil;
        self.hudLabel = nil;
        self.alpha = 0;
    }
    return self;
}

- (void)dismiss {
    [self hudHide];
}

- (void)show {
    [self hudMake:nil imgage:nil spin:YES hide:NO];
}

- (void)showWithStatus:(NSString *)status {
	[self hudMake:status imgage:nil spin:YES hide:NO];
}

- (void)showSuccessWithStatus:(NSString *)status {
    [self hudMake:status imgage:[UIImage imageNamed:@"hud_success_white.png"] spin:NO hide:YES];
}

- (void)showErrorWithStatus:(NSString *)status {
	[self hudMake:status imgage:[UIImage imageNamed:@"hud_error_white.png"] spin:NO hide:YES];
}

- (void)hudMake:(NSString *)status imgage:(UIImage *)img spin:(BOOL)spin hide:(BOOL)hide {
	[self hudCreate];

	self.hudLabel.text = status;
	self.hudLabel.hidden = (status == nil) ? YES : NO;

	self.hudIV.image = img;
	self.hudIV.hidden = (img == nil) ? YES : NO;

    if (spin) {
        [self.hudActivityIV startAnimating];
    } else {
        [self.hudActivityIV stopAnimating];
    }
    
	[self hudOrient];
	[self hudSize];
	[self hudShow];

    if (hide) {
        NSTimeInterval duration = self.hudLabel.text.length * 0.04+0.5;
        self.fadeOutTimer = [NSTimer timerWithTimeInterval:duration target:self selector:@selector(hudHide1) userInfo:nil repeats:NO];
        [[NSRunLoop mainRunLoop] addTimer:self.fadeOutTimer forMode:NSRunLoopCommonModes];
    }
}

- (void)hudCreate {
	if (self.hudToolbar == nil){
		self.hudToolbar = [[UIToolbar alloc] initWithFrame:CGRectZero];
        self.hudToolbar.barTintColor = [Utils getUIColorWithHexString:SysColorBlue];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotate:) name:UIDeviceOrientationDidChangeNotification object:nil];
	}
    
	if (self.hudToolbar.superview == nil) {
        [self.superView addSubview:self.hudToolbar];
    }
    
	if (self.hudActivityIV == nil) {
		self.hudActivityIV = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		self.hudActivityIV.hidesWhenStopped = YES;
	}
    
    if (self.hudActivityIV.superview == nil) {
        [self.hudToolbar addSubview:self.hudActivityIV];
    }

	if (self.hudIV == nil) {
		self.hudIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 28, 28)];
	}
    
    if (self.hudIV.superview == nil) {
        [self.hudToolbar addSubview:self.hudIV];
    }

	if (self.hudLabel == nil) {
		self.hudLabel = [[UILabel alloc] initWithFrame:CGRectZero];
		self.hudLabel.font = [UIFont systemFontOfSize:13.0f];
		self.hudLabel.textColor = [UIColor whiteColor];
		self.hudLabel.backgroundColor = [UIColor clearColor];
		self.hudLabel.textAlignment = NSTextAlignmentCenter;
		self.hudLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
		self.hudLabel.numberOfLines = 0;
	}
    
    if (self.hudLabel.superview == nil) {
        [self.hudToolbar addSubview:self.hudLabel];
    }
}

- (void)hudDestroy {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [self.fadeOutTimer invalidate];
    self.fadeOutTimer = nil;
	[self.hudLabel removeFromSuperview];
    self.hudLabel = nil;
	[self.hudIV removeFromSuperview];
    self.hudIV = nil;
	[self.hudActivityIV removeFromSuperview];
    self.hudActivityIV = nil;
	[self.hudToolbar removeFromSuperview];
    self.hudToolbar = nil;
}

- (void)rotate:(NSNotification *)notification {
	[self hudOrient];
}

- (void)hudOrient {
	CGFloat rotate;
	UIInterfaceOrientation orient = [[UIApplication sharedApplication] statusBarOrientation];
	if (orient == UIInterfaceOrientationPortrait)			rotate = 0.0;
	if (orient == UIInterfaceOrientationPortraitUpsideDown)	rotate = M_PI;
	if (orient == UIInterfaceOrientationLandscapeLeft)		rotate = - M_PI_2;
	if (orient == UIInterfaceOrientationLandscapeRight)		rotate = + M_PI_2;
	self.hudToolbar.transform = CGAffineTransformMakeRotation(rotate);
}

- (void)hudSize {
	CGRect labelRect = CGRectZero;
	if (self.hudLabel.text != nil)
	{
		NSDictionary *attributes = @{NSFontAttributeName:self.hudLabel.font};
		NSInteger options = NSStringDrawingUsesFontLeading | NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin;
		labelRect = [self.hudLabel.text boundingRectWithSize:CGSizeMake([[UIScreen mainScreen]bounds].size.width, 300) options:options attributes:attributes context:NULL];
        if (labelRect.size.width>[[UIScreen mainScreen]bounds].size.width-25) {
            labelRect.size.width=[[UIScreen mainScreen]bounds].size.width-25;
        }
		labelRect.origin.x = ([[UIScreen mainScreen]bounds].size.width-labelRect.size.width)/2.0;
		labelRect.origin.y = 0;
        labelRect.size.height=35.0;
        self.hudIV.frame=CGRectMake(labelRect.origin.x-20, (35.0-16.0)/2.0, 16, 16);
        self.hudActivityIV.frame=CGRectMake(labelRect.origin.x-34, (35.0-30.0)/2.0, 30, 30);
	}
	CGSize screen = [UIScreen mainScreen].bounds.size;
	self.hudToolbar.frame = CGRectMake(0, -35.0, screen.width, 35);
	self.hudLabel.frame = labelRect;
}

- (void)hudShow {
	if (self.alpha == 0) {
		self.alpha = 1;
		self.hudToolbar.alpha = 0;
		NSUInteger options = UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseOut;
		[UIView animateWithDuration:0.15 delay:0 options:options animations:^{
			self.hudToolbar.alpha = 1;
		}
		completion:^(BOOL finished){
        }];
	}
}

- (void)hudHide1 {
    if ([self.delegate respondsToSelector:@selector(ASHUDViewHide:)]) {
        [self.delegate ASHUDViewHide:self];
    }
}

- (void)hudHide {
	if (self.alpha == 1) {
		NSUInteger options = UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseIn;
		[UIView animateWithDuration:0.1 delay:0.5 options:options animations:^{
			self.hudToolbar.alpha = 0;
		}
		completion:^(BOOL finished) {
			[self hudDestroy];
			self.alpha = 0;
		}];
	}
}

@end
