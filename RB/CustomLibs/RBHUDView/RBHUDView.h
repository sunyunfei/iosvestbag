
#import "Utils.h"
#import "constants.h"
#import "AMPActivityIndicator.h"

@interface RBHUDView : UIView

- (void)showWithStatusOverlay:(NSString *)status;
- (void)showOverlay;

- (void)dismiss;
- (void)show;
- (void)showWithStatus:(NSString *)status;
- (void)showSuccessWithStatus:(NSString *)status;
- (void)showErrorWithStatus:(NSString *)status;

@property (nonatomic, strong) UIView *senderView;
@property (nonatomic, strong) UIImageView *hudIV;
@property (nonatomic, strong) UILabel *hudLabel;

@property (nonatomic, strong) NSTimer *fadeOutTimer;
@property (nonatomic, strong) NSTimer *showTimer;

@property (nonatomic, strong) AMPActivityIndicator*hudActivity;
@property (nonatomic, assign) double angle;
@property (nonatomic, strong) UIControl*overlayView;
@end
