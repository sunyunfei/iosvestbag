//
//  UIButton+init.h
//  优梦优
//
//  Created by 刘文 on 16/7/13.
//  Copyright © 2016年 孙云飞. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
@interface UIButton (init)
- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
                      hlTitle:(NSString *)hlTitle
                   titleColor:(UIColor *)titleColor
                 hlTitleColor:(UIColor *)hlTitleColor
              backgroundColor:(UIColor *)backgroundColor
                        image:(UIImage *)image
                      hlImage:(UIImage *)hlimage;
@end
