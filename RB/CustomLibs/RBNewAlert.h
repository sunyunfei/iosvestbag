//
//  RBNewAlert.h
//  RB
//
//  Created by RB8 on 2017/7/24.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RBNewAlert;
@protocol RBNewAlertViewDelegate <NSObject>
@optional
- (void)RBNewAlert:(RBNewAlert *)alertView clickedButtonAtIndex:(int)buttonIndex;
- (void)RBNewAlertCancelButtonAction:(RBNewAlert *)alertView;
- (void)RBNewAlertTapAction:(RBNewAlert *)alertView;
@end
@interface RBNewAlert : UIView
@property (nonatomic, weak)  id<RBNewAlertViewDelegate> delegate;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView * backView;
//分享的数据
@property(nonatomic,copy)NSString * shareTitle;
@property(nonatomic,copy)NSString * shareContent;
@property(nonatomic,copy)NSString * shareImageUrl;
@property(nonatomic,copy)NSString * shareurl;
//活动页面分享，微信分享h5,其他的分享活动
@property(nonatomic,copy)NSString * shareH5Url;
@property(nonatomic,copy)NSString * shareH5Title;
@property(nonatomic,copy)NSString * shareH5Content;
//区分是分享h5还是分享活动
@property(nonatomic,copy)NSString * mark;

- (void)showWithArray:(NSArray *)array IsCountDown:(NSString*)str AndImageStr:(NSString*)ImageStr  AndBigTitle:(NSString*)bigTitle;
- (void)showWithNewArray:(NSArray*)array;
- (void)showWithShareArray:(NSArray*)array;
- (void)showWithCampainTopArray:(NSArray*)nameArray AndbottomArray:(NSArray*)imageArray AndShareWith:(NSString*)mark;
//签到页面弹框
- (void)showWithSign;
//签到成功
- (void)successSignIn:(NSMutableAttributedString*)attStr AndTitle:(NSString*)title;
//品牌主积分规则弹框
- (void)showWithArray:(NSMutableArray *)array andBigTitle:(NSString *)title;
@end
