

typedef enum tagLProgressType
{
    LProgressTypeAnnular,
    LProgressTypeCircle = 1,
    LProgressTypePie = 2
}
LProgressType;

@interface LProgressAppearance : NSObject

@property (assign, nonatomic) LProgressType type;
//percentage supported for LProgressTypeAnnular and LProgressTypeCircle
@property (assign, nonatomic) BOOL showPercentage;

//setting schemeColor will set progressTintColor, backgroundTintColor and percentageTextColor
@property (strong, nonatomic) UIColor *schemeColor;
@property (strong, nonatomic) UIColor *progressTintColor;
@property (strong, nonatomic) UIColor *backgroundTintColor;
@property (strong, nonatomic) UIColor *percentageTextColor;


@property (strong, nonatomic) UIFont *percentageTextFont;
@property (assign, nonatomic) CGPoint percentageTextOffset;

+ (LProgressAppearance *)sharedProgressAppearance;

@end


@interface LProgressView : UIView

@property (strong, nonatomic) NSString *progressString;
@property (assign, nonatomic) float progress;
@property (strong, nonatomic) LProgressAppearance *progressAppearance;

@end



