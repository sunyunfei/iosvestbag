//
//  RBRedBagView.h
//  RB
//
//  Created by RB8 on 2018/6/21.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RBRedBagViewDelegate <NSObject>
- (void)DidClickapartBtnAction;
- (void)showRedBagButton;
@end
@interface RBRedBagView : UIView
- (void)showRedBagWithMoney:(NSString *)moneyStr;

//
- (void)apartRedBagSuccess;
@property(nonatomic,assign)BOOL isApart;
@property(nonatomic,strong)UIImageView * redBagImageView;
@property(nonatomic,strong)UIImageView * leftImageView;
@property(nonatomic,strong)UIImageView * rightImageView;
@property (nonatomic, strong) UIWindow *window;
@property(nonatomic,strong)UILabel * topLabel;
@property(nonatomic,strong)UILabel * moneyLabel;
@property(nonatomic,strong)UILabel * bottomLabel;
@property(nonatomic,strong)UILabel * ruleLabel;
@property(nonatomic,weak)id<RBRedBagViewDelegate>delegate;
@property(nonatomic,strong)NSString * redBagMoney;
@end
