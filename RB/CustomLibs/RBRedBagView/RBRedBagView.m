//
//  RBRedBagView.m
//  RB
//
//  Created by RB8 on 2018/6/21.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBRedBagView.h"
#import "Service.h"
@implementation RBRedBagView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
        id<UIApplicationDelegate> delegate  =
        [[UIApplication sharedApplication] delegate];
        if ([delegate respondsToSelector:@selector(window)]) {
            self.window = [delegate performSelector:@selector(window)];
        } else {
            self.window = [[UIApplication sharedApplication] keyWindow];
        }
        //
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.backgroundColor = SysColorCoverDeep;
        //
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
        [self addGestureRecognizer:tapGesture];
        
    }
    return self;
}
- (void)showRedBagWithMoney:(NSString *)moneyStr{
    _redBagMoney = moneyStr;
    _redBagImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2, ScreenHeight/2, 0,0)];
    _redBagImageView.image = [UIImage imageNamed:@"RBRedBagFold"];
    _redBagImageView.userInteractionEnabled = YES;
    [self addSubview:_redBagImageView];
    //
    _leftImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth - 192*ScaleWidth)/2 - 5 - 44, 248 * ScaleHeight, 44, 65)];
    _leftImageView.image = [UIImage imageNamed:@"RBStarLeft"];
    _leftImageView.alpha = 0;
    [self addSubview:_leftImageView];
    //
    _rightImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth - 192*ScaleWidth)/2 + 192*ScaleWidth + 5, 381 * ScaleHeight, 45, 60)];
    _rightImageView.image = [UIImage imageNamed:@"RBStarRight"];
    _rightImageView.alpha = 0;
    [self addSubview:_rightImageView];
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
    }
    
    [UIView animateWithDuration:0.7 animations:^{
        _redBagImageView.frame = CGRectMake((ScreenWidth - 192*ScaleWidth)/2, (ScreenHeight - 261*ScaleHeight)/2, 192*ScaleWidth, 261*ScaleHeight);
    } completion:^(BOOL finished) {
        _leftImageView.alpha = 1.0;
        _rightImageView.alpha = 1.0;
    }];
    UIButton * apartBtn = [[UIButton alloc]initWithFrame:CGRectMake(30*ScaleWidth, 185*ScaleHeight, 133*ScaleWidth, 36*ScaleHeight) title:nil hlTitle:nil titleColor:nil hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    [apartBtn addTarget:self action:@selector(apartAction:) forControlEvents:UIControlEventTouchUpInside];
    [_redBagImageView addSubview:apartBtn];
}
- (void)tapGesture{
    if (_isApart == NO) {
        if ([self.delegate respondsToSelector:@selector(showRedBagButton)]) {
            [self.delegate showRedBagButton];
        }
    }else if (_isApart == YES){
        
    }
    [self removeFromSuperview];
}
- (void)apartAction:(UIButton *)btn{
    [btn removeFromSuperview];
    //
    if ([self.delegate respondsToSelector:@selector(DidClickapartBtnAction)]) {
        [self.delegate DidClickapartBtnAction];
    }
}
- (void)apartRedBagSuccess{
    _redBagImageView.frame = CGRectMake((ScreenWidth - 192*ScaleWidth)/2, ScreenHeight - 203*ScaleHeight - 282*ScaleHeight, 192*ScaleWidth, 282*ScaleHeight);
    _redBagImageView.image = [UIImage imageNamed:@"RBRedBagApart"];
    //
    _topLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 36*ScaleHeight, 192*ScaleWidth,12) text:@"成功领取红包" font:font_12 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"957144"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [_redBagImageView addSubview:_topLabel];
    //
    _moneyLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, _topLabel.bottom+15, 192*ScaleWidth, 55) text:_redBagMoney font:font_cu_cu_(55) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"f63800"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [_redBagImageView addSubview:_moneyLabel];
    //
    _bottomLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 282*ScaleHeight - 80*ScaleHeight - 13, 192*ScaleWidth, 13) text:[NSString stringWithFormat:@"%@元奖励已放入钱包",_redBagMoney] font:font_(13) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"ffffff"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [_redBagImageView addSubview:_bottomLabel];
    //
    _ruleLabel = [[UILabel alloc]initWithFrame:CGRectMake(24*ScaleWidth, 282*ScaleHeight - 47*ScaleHeight, 192*ScaleWidth - 2*24*ScaleWidth, 29) text:@"每日阅读发现内容可以不定时获得红包奖励" font:font_(11) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"ffd6e7"] backgroundColor:[UIColor clearColor] numberOfLines:2];
    [_redBagImageView addSubview:_ruleLabel];
}
@end
