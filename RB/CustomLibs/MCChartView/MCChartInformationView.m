//
//  MCChartInformationView.m
//  MCChartView
//
//  Created by zhmch0329 on 15/8/18.
//  Copyright (c) 2015年 zhmch0329. All rights reserved.
//

#import "MCChartInformationView.h"
#import "Service.h"

CGFloat static const kMCChartInformationViewCornerRadius = 2.0;
CGFloat const kMCTextDefaultWidth = 40.0f;
CGFloat const kMCTextDefaultHeight = 16.0f;
CGFloat const kMCTipDefaultWidth = 5.0f;
CGFloat const kMCTipDefaultHeight = 3.0f;

@interface MCChartInformationView ()

@property (nonatomic, strong) UILabel *textLabel;

@end

@implementation MCChartInformationView

- (instancetype)initWithText:(NSString *)text andColor:(UIColor*)color {
    self = [super initWithFrame:CGRectMake(0, 0, kMCTextDefaultWidth, kMCTextDefaultHeight + kMCTipDefaultHeight)];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.layer.cornerRadius = kMCChartInformationViewCornerRadius;
        
        _textLabel = [[UILabel alloc] init];
        _textLabel.layer.cornerRadius = kMCChartInformationViewCornerRadius;
        _textLabel.layer.masksToBounds = YES;
        _textLabel.text = text;
        _textLabel.font = [UIFont systemFontOfSize:11.0];
        _textLabel.layer.borderColor = [color CGColor];
        _textLabel.layer.borderWidth = 1.0f;
        _textLabel.backgroundColor = [UIColor whiteColor];
        _textLabel.textColor = color;
        _textLabel.adjustsFontSizeToFitWidth = YES;
        _textLabel.numberOfLines = 1;
        _textLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_textLabel];
    }
    return self;
}

#pragma mark - Layout

- (void)layoutSubviews
{
    [super layoutSubviews];
    _textLabel.frame = CGRectMake(0, 0, kMCTextDefaultWidth, kMCTextDefaultHeight);
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    [[UIColor clearColor] set];
    CGContextFillRect(context, rect);
    
    CGContextSaveGState(context);
    {
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, CGRectGetMidX(rect), CGRectGetMaxY(rect));
        CGContextAddLineToPoint(context, CGRectGetMidX(rect) - kMCTipDefaultWidth/2, kMCTextDefaultHeight);
        CGContextAddLineToPoint(context, CGRectGetMidX(rect) + kMCTipDefaultWidth/2, kMCTextDefaultHeight);
        CGContextClosePath(context);
        CGContextSetFillColorWithColor(context, _textLabel.textColor.CGColor);
        CGContextFillPath(context);
    }
    CGContextRestoreGState(context);
}

@end
