//
//  MCRadarLayer.h
//  MCChartView
//
//  Created by zhmch0329 on 15/8/21.
//  Copyright (c) 2015年 zhmch0329. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 100000
// CAAnimationDelegate is not available before iOS 10 SDK
@interface MCRadarLayer : CALayer
#else
@interface MCRadarLayer : CALayer <CAAnimationDelegate>
#endif


@property (nonatomic, assign) CGPoint centerPoint;
@property (nonatomic, strong) NSArray *radius;

@property (nonatomic, assign) CGColorRef radarFillColor;
@property (nonatomic, assign) CGFloat pointRadius;

@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic, assign) CGColorRef fillColor;
@property (nonatomic, assign) CGColorRef strokeColor;

@property (nonatomic, assign) CGFloat progress;

- (void)reloadRadiusWithAnimate:(BOOL)animate;
- (void)reloadRadiusWithAnimate:(BOOL)animate duration:(CFTimeInterval)duration;

@end
