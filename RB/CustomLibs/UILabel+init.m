//
//  UILabel+init.m
//  优梦优
//
//  Created by 刘文 on 16/7/13.
//  Copyright © 2016年 孙云飞. All rights reserved.
//

#import "UILabel+init.h"

@implementation UILabel (init)
- (instancetype)initWithFrame:(CGRect)frame
                         text:(NSString *)text
                         font:(UIFont *)font
                textAlignment:(NSTextAlignment)textAlignment
                    textColor:(UIColor *)textColor
              backgroundColor:(UIColor *)backgroundColor
                numberOfLines:(NSInteger)numberOfLines{
    if (self = [super init]) {
        self.frame = frame;
        self.text = text;
        self.font = font;
        self.textAlignment = textAlignment;
        self.textColor = textColor;
        if ([self.backgroundColor isEqual: nil]) {
            self.backgroundColor = [UIColor clearColor];

        }else{
            self.backgroundColor = backgroundColor;
        }
        self.numberOfLines = numberOfLines;
    }
    return self;
}
- (instancetype)initWithText:(NSString *)text
                         font:(UIFont *)font
                textAlignment:(NSTextAlignment)textAlignment
                    textColor:(UIColor *)textColor
              backgroundColor:(UIColor *)backgroundColor
                numberOfLines:(NSInteger)numberOfLines{
    if (self = [super init]) {
        self.text = text;
        self.font = font;
        self.textAlignment = textAlignment;
        self.textColor = textColor;
        if ([self.backgroundColor isEqual: nil]) {
            self.backgroundColor = [UIColor clearColor];
            
        }else{
            self.backgroundColor = backgroundColor;
        }
        self.numberOfLines = numberOfLines;
    }
    return self;
}
@end
