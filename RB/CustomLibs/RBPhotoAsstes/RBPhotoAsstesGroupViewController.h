//
//  RBPhotoAsstesGroupViewController.h
//  RB
//
//  Created by AngusNi on 15/7/31.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBPhotoAsstesViewController.h"

@class RBPhotoAsstesGroupViewController;
@protocol RBPhotoAsstesGroupViewControllerDelegate <NSObject>
@optional
- (void)RBPhotoAsstesGroupViewController:(RBPhotoAsstesGroupViewController *)vc selectedList:(NSMutableArray*)list;
- (void)RBPhotoCancelUpload;
@end

@interface RBPhotoAsstesGroupViewController:RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate,RBPhotoAsstesViewControllerDelegate>
@property (nonatomic, weak)  id<RBPhotoAsstesGroupViewControllerDelegate> delegateC;
@property(nonatomic, assign) int selectNumber;

+ (ALAssetsLibrary *)defaultAssetsLibrary;
@end
