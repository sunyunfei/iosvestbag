//
//  HZPhotoBrowserView.h
//  photoBrowser
//

#import <UIKit/UIKit.h>
@interface HZPhotoBrowserView : UIView
<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollview;
@property (nonatomic, strong) UIImageView *imageview;
@property (nonatomic, assign) BOOL beginLoadingImage;
@property (nonatomic, strong) UITapGestureRecognizer *doubleTap;
@property (nonatomic, strong) UITapGestureRecognizer *singleTap;
@property (nonatomic, assign) BOOL hasLoadedImage;//图片下载成功为YES 否则为NO
@property (nonatomic, strong) void (^singleTapBlock)(UITapGestureRecognizer *recognizer);
- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder;
@end
