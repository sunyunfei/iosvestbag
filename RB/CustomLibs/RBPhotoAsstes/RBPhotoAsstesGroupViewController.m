//
//  RBPhotoAsstesGroupViewController.m
//  RB
//
//  Created by AngusNi on 15/7/31.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBPhotoAsstesGroupViewController.h"

@interface RBPhotoAsstesGroupViewController () {
    UITableView *tableviewn;
    NSMutableArray *datalist;
}
@end

@implementation RBPhotoAsstesGroupViewController

+ (ALAssetsLibrary *)defaultAssetsLibrary {
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R2095", @"选择照片");
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight, self.view.width,
                                           self.view.height-64.0)
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [self.hudView showOverlay];
    datalist = [NSMutableArray new];
    ALAssetsLibrary *assetsLibrary = [self.class defaultAssetsLibrary];
    [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll
                                 usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                                     if (!group) {
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                             [self.hudView dismiss];
                                             [tableviewn reloadData];
                                         });
                                     } else {
                                         [datalist addObject:group];
                                     }
                                 }
                               failureBlock:^(NSError *error) {
                                   [self.hudView dismiss];
                               }];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    if ([self.delegateC respondsToSelector:@selector(RBPhotoCancelUpload)]) {
        [self.delegateC RBPhotoCancelUpload];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"Cell%d%d",(int)[indexPath section],(int)[indexPath row]];
    UITableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
        cell.backgroundView = nil;
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [cell.contentView addSubview:bgView];
        //
        UIImageView*iconIV = [[UIImageView alloc]initWithFrame:CGRectZero];
        iconIV.tag = 1001;
        [cell.contentView addSubview:iconIV];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1002;
        [cell.contentView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel.font = font_13;
        cLabel.textAlignment = NSTextAlignmentRight;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        cLabel.tag = 1003;
        [cell.contentView addSubview:cLabel];
        //
        UILabel*arrowLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        arrowLabel.font = font_icon_(13.0);
        arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        arrowLabel.text = Icon_btn_right;
        arrowLabel.tag = 1004;
        [cell.contentView addSubview:arrowLabel];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1010;
        [cell.contentView addSubview:lineLabel];
    }
    UIView*bgView=(UIView*)[cell.contentView viewWithTag:1000];
    UIImageView*iconIV=(UIImageView*)[cell.contentView viewWithTag:1001];
    UILabel*tLabel=(UILabel*)[cell.contentView viewWithTag:1002];
    UILabel*cLabel=(UILabel*)[cell.contentView viewWithTag:1003];
    UILabel*arrowLabel=(UILabel*)[cell.contentView viewWithTag:1004];
    UILabel*lineLabel=(UILabel*)[cell.contentView viewWithTag:1010];
    //
    ALAssetsGroup *group = datalist[datalist.count-1-indexPath.row];
    NSString *temp = [NSString stringWithFormat:@"%@", group];
    NSString *name  = 
    [[temp componentsSeparatedByString:@"Name:"] objectAtIndex:1];
    name = [[name componentsSeparatedByString:@", Type:"] objectAtIndex:0];
    if ([name isEqualToString:@"Camera Roll"]) {
        name = NSLocalizedString(@"R2096", @"相机胶卷");
    }
    temp = [NSString stringWithFormat:@"%@", group];
    NSString *photos  = 
    [[temp componentsSeparatedByString:@"count:"] objectAtIndex:1];
    [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index,
                                       BOOL *stop) {
        if (result) {
            if ([result valueForProperty:ALAssetPropertyType] == ALAssetTypePhoto) {
                iconIV.image = [UIImage imageWithCGImage:result.thumbnail];
            }
        }
    }];
    tLabel.text = name;
    cLabel.text = [NSString stringWithFormat:@"%@", photos];
    iconIV.frame = CGRectMake(CellLeft, CellLeft, 40.0, 40.0);
    tLabel.frame = CGRectMake(iconIV.right+CellLeft, 0, 200, 65.0);
    cLabel.frame = CGRectMake(0, 0, self.view.width-CellLeft-13.0, tLabel.height);
    arrowLabel.frame=CGRectMake(ScreenWidth-CellLeft-13.0, (tLabel.height-13.0)/2.0, 13.0, 13.0);
    lineLabel.frame=CGRectMake(0, tLabel.height-0.5f, ScreenWidth, 0.5f);
    bgView.frame=CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    cell.frame=CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  = 
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ALAssetsGroup *group = datalist[datalist.count-1-indexPath.row];
    NSString *temp = [NSString stringWithFormat:@"%@", group];
    NSString *name  =
    [[temp componentsSeparatedByString:@"Name:"] objectAtIndex:1];
    name = [[name componentsSeparatedByString:@", Type:"] objectAtIndex:0];
    if ([name isEqualToString:@"Camera Roll"]) {
        name = NSLocalizedString(@"R2096", @"相机胶卷");
    }
    temp = [NSString stringWithFormat:@"%@", group];
    NSString *photos = [[temp componentsSeparatedByString:@"count:"] objectAtIndex:1];
    if(photos.intValue==0) {
        [self.hudView showErrorWithStatus:@"所选相册无照片！"];
        return;
    }
    RBPhotoAsstesViewController *toview = [[RBPhotoAsstesViewController alloc] init];
    toview.group = group;
    toview.delegateC = self;
    toview.selectNumber = self.selectNumber;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - RBPhotoAsstesGroupViewController Delegate
- (void)RBPhotoAsstesViewController:(RBPhotoAsstesViewController *)vc selectedList:(NSMutableArray *)list {
    if ([self.delegateC respondsToSelector:@selector(RBPhotoAsstesGroupViewController:selectedList:)]) {
        [self.delegateC RBPhotoAsstesGroupViewController:self selectedList:list];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
