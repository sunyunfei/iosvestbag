//
//  RBPhotoAsstesViewController.h
//  RB
//
//  Created by AngusNi on 15/7/31.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBPhotoPreviewViewController.h"


@class RBPhotoAsstesViewController;
@protocol RBPhotoAsstesViewControllerDelegate <NSObject>
@optional
- (void)RBPhotoAsstesViewController:(RBPhotoAsstesViewController *)vc selectedList:(NSMutableArray*)list;
@end


@interface RBPhotoAsstesViewController:RBBaseViewController
<RBBaseVCDelegate, UICollectionViewDelegate,
                             UICollectionViewDataSource,
                             CHTCollectionViewDelegateWaterfallLayout,RBPhotoPreviewViewControllerDelegate>
@property (nonatomic, weak)  id<RBPhotoAsstesViewControllerDelegate> delegateC;
@property(nonatomic, assign) int selectNumber;
@property(nonatomic, strong) ALAssetsGroup *group;
@property(nonatomic, strong) NSMutableArray *selectedlist;

@end
