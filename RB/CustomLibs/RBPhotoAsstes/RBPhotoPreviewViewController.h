//
//  RBPhotoPreviewViewController.h
//


#import <UIKit/UIKit.h>
#import "HZPhotoBrowserView.h"
#import "Utils.h"

@class RBPhotoPreviewViewController;
@protocol RBPhotoPreviewViewControllerDelegate <NSObject>
@optional
- (UIImage *)photoBrowserVCAsstesImage:(RBPhotoPreviewViewController *)browser forIndex:(NSInteger)index;
- (ALAsset *)photoBrowserVCAsstes:(RBPhotoPreviewViewController *)browser forIndex:(NSInteger)index;
- (void)photoBrowserVCAsstesBack:(RBPhotoPreviewViewController *)browser;
@end

@interface RBPhotoPreviewViewController : UIViewController
@property (nonatomic, assign) int currentImageIndex;
@property (nonatomic, strong) NSMutableArray *datalist;
@property (nonatomic, strong) NSMutableArray *selectedlist;
@property (nonatomic, weak)   id<RBPhotoPreviewViewControllerDelegate> delegate;
- (void)show;
- (void)hide;
+ (RBPhotoPreviewViewController *)sharedInstance;

@end
