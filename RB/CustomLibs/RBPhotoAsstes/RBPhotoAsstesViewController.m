//
//  RBPhotoAsstesViewController.m
//  FTB
//
//  Created by AngusNi on 15/7/31.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBPhotoAsstesViewController.h"

@interface AsstesBtn : UIButton
@property(nonatomic, strong) NSString *contentStr;
@property(nonatomic, strong) ALAsset *asset;
@end

@implementation AsstesBtn
@end

@interface CHTCollectionViewCellAsstes : UICollectionViewCell
@property(nonatomic, strong) UIImageView *logoIV;
@property(nonatomic, strong) AsstesBtn *ccBtn;
@end

@implementation CHTCollectionViewCellAsstes
- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        // 图片
        self.logoIV = [[UIImageView alloc]
                       initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.logoIV.contentMode = UIViewContentModeScaleAspectFit;
        self.logoIV.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.logoIV];
        // 按钮
        self.ccBtn = [AsstesBtn buttonWithType:UIButtonTypeCustom];
        self.ccBtn.frame = CGRectMake(frame.size.width - 24-1, 1, 24, 24);
        self.ccBtn.backgroundColor = [UIColor clearColor];
        self.ccBtn.layer.cornerRadius = 12.0;
        self.ccBtn.layer.masksToBounds = YES;
        [self.contentView addSubview:self.ccBtn];
    }
    return self;
}
@end

@interface RBPhotoAsstesViewController () {
    UICollectionView *collectionviewn;
    NSMutableArray *datalist;
    
    UIButton *previewBtn;
    UIButton *okBtn;
    UILabel *badgeLabel;
}
@end

@implementation RBPhotoAsstesViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    //
    NSString *temp = [NSString stringWithFormat:@"%@", self.group];
    NSString *name  = 
    [[temp componentsSeparatedByString:@"Name:"] objectAtIndex:1];
    name = [[name componentsSeparatedByString:@", Type:"] objectAtIndex:0];
    if ([name isEqualToString:@"Camera Roll"]) {
        name = NSLocalizedString(@"R2096", @"相机胶卷");
    }
    self.navTitle = name;
    //
    CHTCollectionViewWaterfallLayout *layout  = 
    [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(8, 8, 8, 8);
    layout.headerHeight = 0;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 4;
    layout.minimumInteritemSpacing = 4;
    layout.columnCount = 4;
    collectionviewn = [[UICollectionView alloc]
                       initWithFrame:CGRectMake(0, NavHeight, self.view.width,
                                                self.view.height -64.0- 45)
                       collectionViewLayout:layout];
    collectionviewn.dataSource = self;
    collectionviewn.delegate = self;
    collectionviewn.alwaysBounceVertical = YES;
    collectionviewn.backgroundColor = [UIColor clearColor];
    [collectionviewn registerClass:[CHTCollectionViewCellAsstes class]
        forCellWithReuseIdentifier:@"CHTCollectionViewCellAsstes"];
    [self.view addSubview:collectionviewn];
    //
    UIView *footView = [[UIView alloc]
                        initWithFrame:CGRectMake(0, self.view.height - 45,
                                                 self.view.width, 45)];
    footView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:footView];
    //
    UIView *lineView = [[UIView alloc]
                        initWithFrame:CGRectMake(0, 0, self.view.width, 0.5f)];
    lineView.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [footView addSubview:lineView];
    //
    previewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    previewBtn.frame = CGRectMake(0, 0, 60, 45);
    [previewBtn addTarget:self
                   action:@selector(previewBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    previewBtn.backgroundColor = [UIColor clearColor];
    [previewBtn setTitle:NSLocalizedString(@"R2097", @"预览") forState:UIControlStateNormal];
    previewBtn.titleLabel.font = font_13;
    [previewBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [footView addSubview:previewBtn];
    //
    okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame = CGRectMake(footView.width - 70.0, 10, 60, 25);
    [okBtn addTarget:self
              action:@selector(okBtnAction:)
    forControlEvents:UIControlEventTouchUpInside];
    okBtn.backgroundColor = [UIColor clearColor];
    okBtn.layer.borderWidth = 0.5;
    okBtn.layer.borderColor = [Utils getUIColorWithHexString:SysDeepGray].CGColor;
    okBtn.layer.cornerRadius = 5;
    okBtn.layer.masksToBounds = YES;
    
    [okBtn setTitle:NSLocalizedString(@"R1000", @"完成") forState:UIControlStateNormal];
    okBtn.titleLabel.font = font_13;
    [okBtn setTitleColor:[Utils getUIColorWithHexString:SysDeepGray]
                forState:UIControlStateNormal];
    [footView addSubview:okBtn];
    //
    badgeLabel  = 
    [[UILabel alloc] initWithFrame:CGRectMake(footView.width - 60.0 - 20.0,
                                              (45.0 - 16.0) / 2.0, 16, 16)];
    badgeLabel.font = [UIFont boldSystemFontOfSize:12.0];
    badgeLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    badgeLabel.textColor = [UIColor whiteColor];
    badgeLabel.textAlignment = NSTextAlignmentCenter;
    badgeLabel.layer.cornerRadius = badgeLabel.frame.size.width / 2.0;
    badgeLabel.layer.masksToBounds = YES;
    [footView addSubview:badgeLabel];
    //
    okBtn.alpha = 0.5f;
    okBtn.enabled = NO;
    previewBtn.alpha = 0.5f;
    previewBtn.enabled = NO;
    badgeLabel.hidden = YES;
    //
    datalist = [NSMutableArray new];
    self.selectedlist = [NSMutableArray new];
    //
    [self.hudView showOverlay];
    dispatch_async(
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                       [self.group enumerateAssetsUsingBlock:^(ALAsset *result,
                                                               NSUInteger index, BOOL *stop) {
                           if (result) {
                               if ([result valueForProperty:ALAssetPropertyType]  ==
                                   ALAssetTypePhoto) {
                                   [datalist addObject:result];
                               }
                           } else {
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   [self.hudView dismiss];
                                   [collectionviewn reloadData];
                                   NSIndexPath *index = [NSIndexPath indexPathForRow:datalist.count-1 inSection:0];
                                   [collectionviewn scrollToItemAtIndexPath:index atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
                               });
                           }
                       }];
                   });
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [self selectedAction:nil];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)okBtnAction:(UIButton *)sender {
    if([self.selectedlist count] > self.selectNumber){
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:NSLocalizedString(@"R2098", @"只能选择%d张图片"), self.selectNumber]];
        return;
    }
    if ([self.delegateC respondsToSelector:@selector(RBPhotoAsstesViewController:selectedList:)]) {
        [self.delegateC RBPhotoAsstesViewController:self selectedList:self.selectedlist];
    }
    [self RBNavLeftBtnAction];
}

- (void)previewBtnAction:(UIButton *)sender {
    RBPhotoPreviewViewController *photoBrowser = [RBPhotoPreviewViewController sharedInstance];
    photoBrowser.datalist = [self.selectedlist copy];
    photoBrowser.currentImageIndex = 0;
    photoBrowser.delegate = self;
    [photoBrowser show];
}

#pragma mark - RBPhotoBrowserVC Delegate
- (UIImage *)photoBrowserVCAsstesImage:(RBPhotoPreviewViewController *)browser forIndex:(NSInteger)index {
    return nil;
}

- (ALAsset *)photoBrowserVCAsstes:(RBPhotoPreviewViewController *)browser forIndex:(NSInteger)index {
    ALAsset *asset = browser.datalist[index];
    return asset;
}

- (void)photoBrowserVCAsstesBack:(RBPhotoPreviewViewController *)browser {
    self.selectedlist = browser.selectedlist;
    [collectionviewn reloadData];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return [datalist count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CHTCollectionViewCellAsstes *cell  = 
    (CHTCollectionViewCellAsstes *)[collectionView
                                    dequeueReusableCellWithReuseIdentifier:@"CHTCollectionViewCellAsstes"
                                    forIndexPath:indexPath];
    
    ALAsset *asset  = datalist[indexPath.row];
    UIImage *thumbnailImage = [UIImage imageWithCGImage:asset.thumbnail];
    cell.logoIV.image = thumbnailImage;
    cell.ccBtn.asset = asset;
    cell.ccBtn.contentStr = [[[asset defaultRepresentation] url] absoluteString];
    [cell.ccBtn addTarget:self
                   action:@selector(btnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    cell.ccBtn.titleLabel.font = font_icon_(24.0);
    [cell.ccBtn setTitle:Icon_btn_select_l forState:UIControlStateNormal];
    [cell.ccBtn
     setTitleColor:[Utils getUIColorWithHexString:SysColorWhite]
     forState:UIControlStateNormal];
    cell.ccBtn.backgroundColor = [Utils getUIColorWithHexString:SysWhiteGray];
    if ([self.selectedlist containsObject:asset]) {
        [cell.ccBtn setTitle:Icon_btn_select_h forState:UIControlStateNormal];
        [cell.ccBtn
         setTitleColor:[Utils getUIColorWithHexString:SysColorBlue]
         forState:UIControlStateNormal];
        cell.ccBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        cell.ccBtn.alpha = 0.6;
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((self.view.width - 4.0 * 5.0) / 4.0,
                      (self.view.width - 4.0 * 5.0) / 4.0);
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ALAsset *asset = datalist[indexPath.row];
    [self selectedAction:asset];
}

#pragma mark - AsstesBtn Delegate
- (void)btnAction:(AsstesBtn *)sender {
    [self selectedAction:sender.asset];
}

- (void)selectedAction:(ALAsset*)asset {
    if (asset!= nil) {
        if ([self.selectedlist containsObject:asset]) {
            [self.selectedlist removeObject:asset];
        } else {
            if([self.selectedlist count]>=self.selectNumber){
                [self.selectedlist removeLastObject];
            }
            [self.selectedlist addObject:asset];
        }
    }
    if ([self.selectedlist count] !=  0) {
        okBtn.alpha = 1.0f;
        okBtn.enabled = YES;
        okBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
        [okBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
        okBtn.layer.borderColor = [Utils getUIColorWithHexString:SysColorBlue].CGColor;
        previewBtn.alpha = 1.0f;
        previewBtn.enabled = YES;
        badgeLabel.hidden = YES;
        badgeLabel.text = [NSString stringWithFormat:@"%d", (int)[self.selectedlist count]];
        CGSize badgeLabelSize = [Utils getUIFontSizeFitW:badgeLabel];
        if (badgeLabelSize.width < badgeLabelSize.height) {
            badgeLabelSize.width = badgeLabelSize.height;
        }
        badgeLabel.frame = CGRectMake(self.view.width - 55.0 - badgeLabelSize.width,
                                      badgeLabel.top, badgeLabelSize.width + 3,
                                      badgeLabelSize.height + 3);
        badgeLabel.layer.cornerRadius = badgeLabel.frame.size.width / 2.0;
        badgeLabel.layer.masksToBounds = YES;
    } else {
        okBtn.alpha = 0.5f;
        okBtn.enabled = NO;
        okBtn.backgroundColor = [UIColor clearColor];
        [okBtn setTitleColor:[Utils getUIColorWithHexString:SysDeepGray] forState:UIControlStateNormal];
        okBtn.layer.borderColor = [Utils getUIColorWithHexString:SysDeepGray].CGColor;
        previewBtn.alpha = 0.5f;
        previewBtn.enabled = NO;
        badgeLabel.hidden = YES;
    }
    [collectionviewn reloadData];
}

@end
