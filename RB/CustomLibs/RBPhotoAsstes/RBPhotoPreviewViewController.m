//
//  RBPhotoPreviewViewController.m
//

#import "RBPhotoPreviewViewController.h"
#import "Service.h"

@interface SelectBtn : UIButton
@property(nonatomic, strong) NSString *contentStr;
@property(nonatomic, strong) ALAsset *result;
@end
@implementation SelectBtn
@end


@interface RBPhotoPreviewViewController () <UIScrollViewDelegate> {
    UILabel *numIndexLabel;
    UILabel *numTotalLabel;
    SelectBtn *selectedBtn;
    UILabel *badgeLabel;
}

@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic, strong) UIActivityIndicatorView *indicatorView;
@end

@implementation RBPhotoPreviewViewController

+ (RBPhotoPreviewViewController *)sharedInstance {
    static dispatch_once_t once = 0;
    static RBPhotoPreviewViewController *shareView;
    dispatch_once(&once, ^{
        shareView = [[RBPhotoPreviewViewController alloc]init];
    });
    return shareView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    //
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView.delegate = self;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.pagingEnabled = YES;
    [self.view addSubview:self.scrollView];
    //
    [self addToolbars];
}

#pragma mark - show Delegate
- (void)show {
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:self animated:NO completion:NULL];
    [self loadDataView];
}

- (void)hide {
    [self dismissViewControllerAnimated:NO completion:NULL];
}

#pragma mark - 加载数据
-(void)loadDataView{
    [self.scrollView removeAllSubviews];
    for (int i = 0; i < [self.datalist count]; i++) {
        @autoreleasepool {
            HZPhotoBrowserView *view = [[HZPhotoBrowserView alloc] initWithFrame:CGRectZero];
            view.imageview.tag = i + 1000;
            view.singleTapBlock = ^(UITapGestureRecognizer *recognizer) {
                //处理单击
            };
            [self.scrollView addSubview:view];
        }
    }
    [self setUpFrames];
    [self setupImageOfImageViewForIndex:self.currentImageIndex];
    //
    self.selectedlist = [NSMutableArray new];
    for (ALAsset *asset in self.datalist) {
        [self.selectedlist addObject:asset];
    }
    numIndexLabel.text = [NSString stringWithFormat:@"%d", self.currentImageIndex+1];
    if ([self.datalist count]>1) {
        numTotalLabel.text = [NSString stringWithFormat:@"/%d", (int)[self.datalist count]];
    }
    [self calculateIndexView];
    [self selectBtnChange];
}

-(void)calculateIndexView {
    CGSize numIndexLabelSize = [Utils getUIFontSizeFitW:numIndexLabel];
    CGSize numTotalLabelSize = [Utils getUIFontSizeFitW:numTotalLabel];
    numIndexLabel.frame = CGRectMake((ScreenWidth-(numIndexLabelSize.width+numTotalLabelSize.width))/2.0, numIndexLabel.top, numIndexLabelSize.width, numIndexLabel.height);
    numTotalLabel.frame = CGRectMake(numIndexLabel.right+3, numTotalLabel.top, numTotalLabel.width, numTotalLabel.height);
}

#pragma mark - setUpFrames
- (void)setUpFrames {
    float kPhotoBrowserImageViewMargin = 2.0;
    CGRect rect = self.view.bounds;
    rect.size.width +=  kPhotoBrowserImageViewMargin * 2;
    self.scrollView.bounds = rect;
    self.scrollView.center = CGPointMake(ScreenWidth *0.5, ScreenHeight *0.5);
    CGFloat y = 0;
    __block CGFloat w = ScreenWidth;
    CGFloat h = ScreenHeight;
    [self.scrollView.subviews enumerateObjectsUsingBlock:^(HZPhotoBrowserView *obj, NSUInteger idx, BOOL *stop) {
        CGFloat x = kPhotoBrowserImageViewMargin + idx * (kPhotoBrowserImageViewMargin * 2 + w);
        obj.frame = CGRectMake(x, y, w, h);
    }];
    self.scrollView.contentSize = CGSizeMake(self.scrollView.subviews.count * self.scrollView.width, ScreenHeight);
    self.scrollView.contentOffset = CGPointMake(self.currentImageIndex * self.scrollView.width, 0);
}

#pragma mark - addToolbars
- (void)addToolbars {
    UIView*toolView = [[UIView alloc]
                  initWithFrame:CGRectMake(0, self.view.height-45.0, self.view.width, 45)];
    toolView.userInteractionEnabled = YES;
    toolView.backgroundColor = SysColorCoverDeep;
    [self.view addSubview:toolView];
    // 返回
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45.0, 45.0)];
    [backBtn addTarget:self
                action:@selector(backBtnAction:)
      forControlEvents:UIControlEventTouchUpInside];
    [toolView addSubview:backBtn];
    //
    UILabel *backLabel = [[UILabel alloc]
                          initWithFrame:CGRectMake(15.0, (45.0-20.0)/2.0, 20.0, 20.0)];
    backLabel.textAlignment = NSTextAlignmentLeft;
    backLabel.textColor = [UIColor whiteColor];
    backLabel.font = font_icon_(backLabel.width);
    backLabel.text = Icon_bar_back;
    [toolView addSubview:backLabel];
    // 序号-当前
    numIndexLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 45)];
    numIndexLabel.textAlignment = NSTextAlignmentLeft;
    numIndexLabel.textColor = [UIColor whiteColor];
    numIndexLabel.font = font_15;
    [toolView addSubview:numIndexLabel];
    // 序号-总计
    numTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 3, 50, 45)];
    numTotalLabel.textAlignment = NSTextAlignmentLeft;
    numTotalLabel.textColor = [UIColor whiteColor];
    numTotalLabel.font = font_11;
    [toolView addSubview:numTotalLabel];
    // 选择
    selectedBtn = [SelectBtn buttonWithType:UIButtonTypeCustom];
    selectedBtn.frame = CGRectMake(toolView.width-25.0-15.0, (45.0-25.0)/2.0, 25.0, 25.0);
    [selectedBtn addTarget:self
              action:@selector(selectedBtnAction:)
    forControlEvents:UIControlEventTouchUpInside];
    selectedBtn.titleLabel.font = font_icon_(selectedBtn.width);
    [selectedBtn setTitle:Icon_btn_select_h forState:UIControlStateNormal];
    [selectedBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
//    [toolView addSubview:selectedBtn];
    // 已选择
    badgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(toolView.width-60.0-20.0, (45.0-16.0)/2.0, 16, 16)];
    badgeLabel.font = font_11;
    badgeLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    badgeLabel.textColor = [UIColor whiteColor];
    badgeLabel.textAlignment = NSTextAlignmentCenter;
    badgeLabel.layer.cornerRadius = badgeLabel.width/2.0;
    badgeLabel.layer.masksToBounds = YES;
//    [toolView addSubview:badgeLabel];
}

#pragma mark - UIButton Delegate
- (void)backBtnAction:(UIButton *)sender {
    [self hide];
    [self.delegate photoBrowserVCAsstesBack:self];
}

- (void)selectedBtnAction:(UIButton *)sender {
    ALAsset *asset = self.datalist[self.currentImageIndex];
    if ([self.selectedlist containsObject:asset]) {
        [self.selectedlist removeObject:asset];
    } else {
        [self.selectedlist addObject:asset];
    }
    [self selectBtnChange];
}

#pragma mark - setupImageOfImageViewForIndex
- (void)setupImageOfImageViewForIndex:(NSInteger)index {
    HZPhotoBrowserView *view = self.scrollView.subviews[index];
    if (view.beginLoadingImage) {
        return;
    }
    view.imageview.contentMode = UIViewContentModeScaleAspectFit;
    view.imageview.hidden = NO;
    view.beginLoadingImage = YES;
    UIImage*image = nil;
    if ([self.delegate photoBrowserVCAsstesImage:self forIndex:index] == nil) {
        ALAsset *asset = [self.delegate photoBrowserVCAsstes:self forIndex:index];
        image = [Utils thumbnailForAsset:asset maxPixelSize:1024];
    } else {
        image = [self.delegate photoBrowserVCAsstesImage:self forIndex:index];
    }
    view.imageview.image = image;
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int index = (scrollView.contentOffset.x+self.scrollView.width*0.5)/self.scrollView.width;
    //
    numIndexLabel.text = [NSString stringWithFormat:@"%d", index + 1];
    [self calculateIndexView];
    //
    if(index+1<[self.datalist count]){
        [self setupImageOfImageViewForIndex:index+1];
    }
    if(index<[self.datalist count]&&index>= 0){
        [self setupImageOfImageViewForIndex:index];
    }
    if(index-1>0){
        [self setupImageOfImageViewForIndex:index-1];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int index = scrollView.contentOffset.x/self.scrollView.width;
    self.currentImageIndex = index;
    [self autoScaleScrollView];
    [self selectBtnChange];
}

#pragma mark - autoScaleScrollView
- (void)autoScaleScrollView {
    for (HZPhotoBrowserView *view in self.scrollView.subviews) {
        [view.scrollview setZoomScale:1.0 animated:NO];
    }
}

-(void)selectBtnChange {
    ALAsset *asset = self.datalist[self.currentImageIndex];
    if ([self.selectedlist containsObject:asset]) {
        [selectedBtn setTitle:Icon_btn_select_h forState:UIControlStateNormal];
        [selectedBtn
         setTitleColor:[Utils getUIColorWithHexString:SysColorBlue]
         forState:UIControlStateNormal];
    } else {
        [selectedBtn setTitle:Icon_btn_select_l forState:UIControlStateNormal];
        [selectedBtn
         setTitleColor:[Utils getUIColorWithHexString:SysColorGray]
         forState:UIControlStateNormal];
    }
    //
    if ([self.selectedlist count] !=  0) {
        badgeLabel.hidden = NO;
        badgeLabel.text = [NSString stringWithFormat:@"%d", (int)[self.selectedlist count]];
        CGSize badgeLabelSize = [Utils getUIFontSizeFitW:badgeLabel];
        if (badgeLabelSize.width < badgeLabelSize.height) {
            badgeLabelSize.width = badgeLabelSize.height;
        }
        badgeLabel.frame = CGRectMake(self.view.width-55.0-badgeLabelSize.width,
                                      badgeLabel.top, badgeLabelSize.width+3,
                                      badgeLabelSize.height + 3);
        badgeLabel.layer.cornerRadius = badgeLabel.width/2.0;
        badgeLabel.layer.masksToBounds = YES;
    } else {
        badgeLabel.hidden = YES;
    }
}

@end
