//
//  HZPhotoBrowserView.m
//  photoBrowser
//

#import "HZPhotoBrowserView.h"
#import "Service.h"

@interface HZPhotoBrowserView()
@end

@implementation HZPhotoBrowserView
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //
        self.scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.scrollview.delegate = self;
        self.scrollview.clipsToBounds = YES;
        [self addSubview:self.scrollview];
        //
        self.imageview = [[UIImageView alloc] init];
        self.imageview.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        self.imageview.userInteractionEnabled = YES;
        self.imageview.hidden = YES;
        [self.scrollview addSubview:self.imageview];
        //
        self.doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        self.doubleTap.numberOfTapsRequired = 2;
        self.doubleTap.numberOfTouchesRequired   = 1;
        [self addGestureRecognizer:self.doubleTap];
        //
        self.singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        self.singleTap.numberOfTapsRequired = 1;
        self.singleTap.numberOfTouchesRequired = 1;
        [self.singleTap requireGestureRecognizerToFail:self.doubleTap];
        [self addGestureRecognizer:self.singleTap];
    }
    return self;
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer{
    if (self.singleTapBlock) {
        self.singleTapBlock(recognizer);
    }
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer{
    if (!self.hasLoadedImage) {
        return;
    }
    CGPoint touchPoint = [recognizer locationInView:self];
    if (self.scrollview.zoomScale <=  1.0) {
        CGFloat scaleX = touchPoint.x + self.scrollview.contentOffset.x;
        CGFloat sacleY = touchPoint.y + self.scrollview.contentOffset.y;
        [self.scrollview zoomToRect:CGRectMake(scaleX, sacleY, 10, 10) animated:YES];
    } else {
        [self.scrollview setZoomScale:1.0 animated:YES];
    }
}

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder{
    [self.imageview setShowActivityIndicatorView:YES];
    [self.imageview sd_setImageWithURL:url placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error){
            self.hasLoadedImage = NO;
        } else {
            self.hasLoadedImage = YES;
            [self setNeedsLayout];
        }
    }];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.scrollview.frame = self.bounds;
    [self adjustFrames];
}

- (void)adjustFrames{
    CGRect frame = self.scrollview.frame;
    if (self.imageview.image) {
        CGSize imageSize = self.imageview.image.size;
        CGRect imageFrame = CGRectMake(0, 0, imageSize.width, imageSize.height);
        CGFloat ratio = frame.size.width/imageFrame.size.width;
        imageFrame.size.height = imageFrame.size.height*ratio;
        imageFrame.size.width = frame.size.width;
        self.imageview.frame = imageFrame;
        self.scrollview.contentSize = self.imageview.frame.size;
        self.imageview.center = [self centerOfScrollViewContent:self.scrollview];
    } else {
        frame.origin = CGPointZero;
        self.imageview.frame = frame;
        self.scrollview.contentSize = self.imageview.frame.size;
    }
    self.scrollview.minimumZoomScale = 1.0;
    self.scrollview.maximumZoomScale = 3.0;
    self.scrollview.zoomScale = 1.0f;
    self.scrollview.contentOffset = CGPointZero;
}

- (CGPoint)centerOfScrollViewContent:(UIScrollView *)scrollView{
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    CGPoint actualCenter = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                       scrollView.contentSize.height * 0.5 + offsetY);
    return actualCenter;
}

#pragma mark - UIScrollView Delegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.imageview;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    self.imageview.hidden = NO;
    self.imageview.center = [self centerOfScrollViewContent:scrollView];
}

@end
