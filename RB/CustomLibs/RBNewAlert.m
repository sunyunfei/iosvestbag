//
//  RBNewAlert.m
//  RB
//
//  Created by RB8 on 2017/7/24.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBNewAlert.h"
#import "Service.h"
@interface RBNewAlert () {
    int codeTimerStr;
    NSTimer *codeTimer;
    UIImageView * successImageView;
}
@end
@implementation RBNewAlert
- (instancetype)init {
    if (self = [super init]) {
        self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
        id<UIApplicationDelegate> delegate  =
        [[UIApplication sharedApplication] delegate];
        if ([delegate respondsToSelector:@selector(window)]) {
            self.window = [delegate performSelector:@selector(window)];
        } else {
            self.window = [[UIApplication sharedApplication] keyWindow];
        }
        //
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.backgroundColor = SysColorCoverDeep;
        //
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}
//分享
- (void)showWithShareArray:(NSArray*)array{
    //
    [self removeAllSubviews];
    self.array = array;
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(43*ScaleWidth, 288*ScaleHeight, ScreenWidth - 2*43*ScaleWidth, 0)];
    self.bgView.layer.cornerRadius = 5.0;
    self.bgView.layer.masksToBounds = YES;
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bgView];
    //大标题
    UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, self.bgView.width, 17) text:@"分享到" font:font_cu_17 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:SysColorAlert] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [self.bgView addSubview:titleLabel];
    NSArray * imageArray = @[@"icon_wechat",@"icon_wechat_friends",@"icon_weibo",@"icon_qq",@"icon_qzone"];
    for (NSInteger i = 0; i<5; i++) {
       
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake((self.bgView.width - 3*48)/6 + (i%3)*(48+(self.bgView.width-3*48)/3), titleLabel.bottom+20+i/3 * (48+20), 48, 48) title:nil hlTitle:nil titleColor:nil hlTitleColor:nil backgroundColor:nil image:[UIImage imageNamed:imageArray[i]] hlImage:nil];
        [btn addTarget:self action:@selector(tempBtnAction1:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 1000+i;
        [self.bgView addSubview:btn];
    }
    self.bgView.height = titleLabel.bottom + 20+48*2+20+20;
    self.bgView.center = CGPointMake(ScreenWidth/2, ScreenHeight/2);
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = self.bgView.transform;
        self.bgView.transform  =
        CGAffineTransformScale(self.bgView.transform, 0.5, 0.5);
        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.bgView.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }
}
- (void)tempBtnAction1:(UIButton*)sender{
    SSDKPlatformType type;
   
    if (sender.tag == 1000) {
        type = SSDKPlatformSubTypeWechatSession;
        if (self.shareH5Url.length>0) {
           [self shareSDKWithTitle:self.shareH5Title Content:self.shareH5Content URL:self.shareH5Url ImgURL:self.shareImageUrl SSDKPlatformType:type];
            return;

        }
    } else if (sender.tag == 1001) {
        type = SSDKPlatformSubTypeWechatTimeline;
    } else if (sender.tag == 1002) {
        type = SSDKPlatformTypeSinaWeibo;
    } else if (sender.tag == 1003) {
        type = SSDKPlatformSubTypeQQFriend;
    } else {
        type = SSDKPlatformSubTypeQZone;
    }
    if (self.shareurl.length>0) {
        [self shareSDKWithTitle:self.shareTitle Content:self.shareContent URL:self.shareurl ImgURL:self.shareImageUrl SSDKPlatformType:type];
        return;
    }
    [self shareSDKWithTitle:self.shareTitle Content:self.shareContent URL:[NSString stringWithFormat:@"%@invite?inviter_id=%@",ApiUrl,[LocalService getRBLocalDataUserLoginId]] ImgURL:self.shareImageUrl SSDKPlatformType:type];
    
}
- (void)shareSDKWithTitle:(NSString *)titleStr
                  Content:(NSString *)contentStr
                      URL:(NSString *)urlStr
                   ImgURL:(NSString *)imgUrlStr
         SSDKPlatformType:(SSDKPlatformType)type{
    [SVProgressHUD show];
    NSLog(@"title:%@,content:%@,img:%@,url:%@",titleStr,contentStr,imgUrlStr,urlStr);
    if (type==SSDKPlatformTypeSinaWeibo) {
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKEnableUseClientShare];
        [shareParams SSDKSetupShareParamsByText:[NSString stringWithFormat:@"%@ %@",titleStr,urlStr]
                                         images:imgUrlStr
                                            url:[NSURL URLWithString:urlStr]
                                          title:[NSString stringWithFormat:@"%@ %@",titleStr,urlStr]
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            if (state == SSDKResponseStateSuccess) {
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
            }
            if (state == SSDKResponseStateFail) {
                if (error.code == 204) {
                    [SVProgressHUD showErrorWithStatus:@"相同的内容不能重复分享"];
                } else {
                    if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                        [SVProgressHUD showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                    } else {
                        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
                    }
                }
            }
            if (state == SSDKResponseStateCancel) {
                [SVProgressHUD dismiss];
                //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
            }
        }];
    } else {
        [[SDWebImageDownloader sharedDownloader]downloadImageWithURL:[NSURL URLWithString:imgUrlStr] options:SDWebImageDownloaderHighPriority progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            if(finished==NO) {
                NSLog(@"图片下载失败:%@,%@",image,data);
            } else {
                image = [Utils narrowWithImage:image];
            }
            NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
            [shareParams SSDKSetupShareParamsByText:contentStr
                                             images:image
                                                url:[NSURL URLWithString:urlStr]
                                              title:titleStr
                                               type:SSDKContentTypeWebPage];
            [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
                if (state == SSDKResponseStateSuccess) {
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
                }
                if (state == SSDKResponseStateFail) {
                    if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                        [SVProgressHUD showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                    } else {
                        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
                    }
                }
                if (state == SSDKResponseStateCancel) {
                    [SVProgressHUD dismiss];
                    //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
                }
            }];
        }];
    }

}
-(void)showWithNewArray:(NSArray *)array{
    [self removeAllSubviews];
    self.array = array;
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(43*ScaleWidth, 288*ScaleHeight, ScreenWidth - 2*43*ScaleWidth, 0)];
    self.bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    self.bgView.layer.cornerRadius = 5;
    self.bgView.clipsToBounds = YES;
    self.bgView.userInteractionEnabled = YES;
    [self addSubview:self.bgView];
    for (NSInteger i = 0; i<array.count; i++) {
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(0, i*(59+0.5), self.bgView.width, 59) title:array[i] hlTitle:nil titleColor:[Utils getUIColorWithHexString:ccColor5a5a5a] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
        btn.tag = 1000+i;
        btn.titleLabel.font = font_16;
        
        [btn addTarget:self action:@selector(tempBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgView addSubview:btn];
        if (i>0) {
            UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, i*59, self.bgView.width, 0.5)];
            lineView.backgroundColor = [Utils getUIColorWithHexString:ccColor919191 andAlpha:0.24];
            [self.bgView addSubview:lineView];
        }
    }
    self.bgView.height = 59*array.count + 0.5*(array.count-1);
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = self.bgView.transform;
        self.bgView.transform  =
        CGAffineTransformScale(self.bgView.transform, 0.5, 0.5);
        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.bgView.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }
}
//签到成功
- (void)successSignIn:(NSMutableAttributedString*)attStr AndTitle:(NSString*)title{
    [self removeAllSubviews];
    UIView * backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth ,ScreenHeight)];
    backView.backgroundColor = [UIColor clearColor];
    [self addSubview:backView];
    //
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(50,0, ScreenWidth - 50 * 2, 0)];
    self.bgView.layer.cornerRadius = 5.0;
    self.bgView.layer.masksToBounds = YES;
    self.bgView.backgroundColor = [UIColor whiteColor];
    [backView addSubview:self.bgView];
    //
   
    CGRect rect = [attStr boundingRectWithSize:CGSizeMake(self.bgView.width-CellLeft*2.0, 1000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    //
    UILabel * attLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 50, self.bgView.width-CellLeft*2.0, 0)];
    attLabel.numberOfLines = 0;
    attLabel.attributedText = attStr;
    [attLabel sizeToFit];
    [self.bgView addSubview:attLabel];
    attLabel.frame = CGRectMake((self.bgView.size.width - rect.size.width)/2, 50, rect.size.width, rect.size.height);
    attLabel.textAlignment = NSTextAlignmentCenter;
    //
    UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake((self.bgView.width - 100)/2, attLabel.bottom + 25, 100, 30) title:@"知道啦" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorBlue] image:nil hlImage:nil];
    btn.layer.cornerRadius = 15;
    btn.layer.masksToBounds = YES;
    [btn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = 1000;
    [self.bgView addSubview:btn];
    //
    self.bgView.height = btn.bottom + 17;
    self.bgView.top = (ScreenHeight - self.bgView.height)/2;
    //
    successImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth - 325)/2, self.bgView.top - 68, 325, 109)];
    successImageView.image = [UIImage imageNamed:@"signSuccess"];
    [backView addSubview:successImageView];
    //
    UILabel * successLabel= [[UILabel alloc]initWithFrame:CGRectMake(0, 45, successImageView.width, 25) text:title font:[UIFont systemFontOfSize:17] textAlignment:NSTextAlignmentCenter textColor:[UIColor whiteColor] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [successImageView addSubview:successLabel];
    //
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = backView.transform;
        backView.transform  =
        CGAffineTransformScale(backView.transform, 0.5, 0.5);
        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            backView.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }
}
//我的签到页面弹框
- (void)showWithSign{
    [self removeAllSubviews];
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(50,0, ScreenWidth - 50.0 * 2, 0)];
    self.bgView.layer.cornerRadius = 5.0;
    self.bgView.layer.masksToBounds = YES;
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bgView];
    //
    UILabel * bigTitleLabel = [[UILabel alloc]init];
    bigTitleLabel.font = font_cu_cu_17;
    bigTitleLabel.textAlignment = NSTextAlignmentCenter;
    [bigTitleLabel setTextColor:[Utils getUIColorWithHexString:SysColorAlert]];
    bigTitleLabel.text = @"签到奖励规则";
    [self.bgView addSubview:bigTitleLabel];
    bigTitleLabel.frame = CGRectMake(0, 25, self.bgView.width, 17);
    //
    NSDictionary * firstAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16],NSForegroundColorAttributeName:[Utils getUIColorWithHexString:@"828282"]};
    NSDictionary * secondAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16],NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorBlue]};
    NSMutableAttributedString * firstPart = [[NSMutableAttributedString alloc]initWithString:@"1.连续签到,获得奖励累加,第一天签到获得"];
    [firstPart setAttributes:firstAttributes range:NSMakeRange(0, firstPart.length)];
    //
    NSArray * arr = @[@"元,连续签到两天获得",@"元,连续签到三天获得",@"元,连续签到四天获得",@"元,连续签到五天获得",@"元,连续签到六天获得",@"元,连续签到七天获得",@"元.\n2.七天为一个周期，第八天重新开始计算.(若中途某一天停止签到,仍会重新计算)"];
    NSArray * arr1 = @[@"0.10",@"0.20",@"0.25",@"0.30",@"0.35",@"0.40",@"0.50"];
    for (NSInteger i = 0; i < arr.count; i ++) {
        NSMutableAttributedString * part1 = [[NSMutableAttributedString alloc]initWithString:arr1[i]];
        [part1 setAttributes:secondAttributes range:NSMakeRange(0, part1.length)];
        //
        NSMutableAttributedString * part2 = [[NSMutableAttributedString alloc]initWithString:arr[i]];
        [part2 setAttributes:firstAttributes range:NSMakeRange(0, part2.length)];
        //
        [firstPart appendAttributedString:part1];
        [firstPart appendAttributedString:part2];
    }
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    [paragraphStyle setLineSpacing:10];
    [firstPart addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [firstPart length])];
    
    CGRect rect = [firstPart boundingRectWithSize:CGSizeMake(self.bgView.width-CellLeft*2.0, 1000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    //
    UILabel * ruleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, bigTitleLabel.bottom+25.0, self.bgView.width-CellLeft*2.0, 0)];
    ruleLabel.numberOfLines = 0;
    ruleLabel.textAlignment = NSTextAlignmentLeft;
    ruleLabel.attributedText = firstPart;
    [ruleLabel sizeToFit];
    [self.bgView addSubview:ruleLabel];
    ruleLabel.frame = CGRectMake((self.bgView.size.width - rect.size.width)/2, bigTitleLabel.bottom + 25.0, rect.size.width, rect.size.height);
    //
    UIButton * tempBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, ruleLabel.bottom + 20, self.bgView.width, 50) title:@"知道了" hlTitle:nil titleColor:[UIColor whiteColor] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorBlue] image:nil hlImage:nil];
    [tempBtn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:tempBtn];
    self.bgView.height = tempBtn.bottom;
    self.bgView.top = (ScreenHeight - tempBtn.bottom)/2;
    //
    
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = self.bgView.transform;
        self.bgView.transform  =
        CGAffineTransformScale(self.bgView.transform, 0.5, 0.5);
        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.bgView.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }
    
}
//品牌主积分规则弹框
- (void)showWithArray:(NSMutableArray *)array andBigTitle:(NSString *)title{
    [self removeAllSubviews];
    self.array = array;
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(60*ScaleWidth,0, ScreenWidth-60.0*2.0*ScaleWidth,0)];
    self.bgView.layer.cornerRadius = 5.0;
    self.bgView.layer.masksToBounds = YES;
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bgView];
    //大标题
    UILabel * bigTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, self.bgView.width, 20)];
    bigTitleLabel.font = font_cu_cu_17;
    bigTitleLabel.textAlignment = NSTextAlignmentCenter;
    [bigTitleLabel setTextColor:[Utils getUIColorWithHexString:SysColorAlert]];
    [self.bgView addSubview:bigTitleLabel];
    bigTitleLabel.text = title;
    if (title == nil) {
        bigTitleLabel.height = 0;
    }
    //
    
    UILabel*titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(22, bigTitleLabel.bottom+15, self.bgView.width-44.0, 0)];
    titleLabel.font = font_cu_15;
    titleLabel.numberOfLines = 0;
    [titleLabel setTextColor:[Utils getUIColorWithHexString:SysColorAlert]];
    [self.bgView addSubview:titleLabel];
    if ([title isEqualToString:@"额外奖励"]) {
        NSString * str = array[0];
        titleLabel.text = str;
        [titleLabel sizeToFit];
        titleLabel.frame = CGRectMake(22, bigTitleLabel.bottom+15, self.bgView.width-44.0, titleLabel.frame.size.height);
    }else if ([title isEqualToString:@"积分规则"]){
        NSMutableAttributedString*titleStr = (NSMutableAttributedString*)array[0];
        titleLabel.attributedText = titleStr;
        CGRect rect = [titleStr boundingRectWithSize:CGSizeMake(self.bgView.width-44.0, 1000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        titleLabel.frame = CGRectMake(22, bigTitleLabel.bottom+15, self.bgView.width-44.0, rect.size.height);
    }
    titleLabel.textAlignment = NSTextAlignmentCenter;

   
    //
    NSString * dateStr = @"";
    if (array.count > 1) {
        dateStr = (NSString*)array[1];
    }
    UILabel * dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, titleLabel.bottom + 10, self.bgView.width, 20) text:dateStr font:font_(11) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"777777"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [self.bgView addSubview:dateLabel];
    if (dateStr.length == 0) {
        dateLabel.height = 0;
    }
    //
    UIButton * tempBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, dateLabel.bottom + 15, self.bgView.width, 50) title:@"知道了" hlTitle:nil titleColor:[UIColor whiteColor] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorBlue] image:nil hlImage:nil];
    [tempBtn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:tempBtn];
    self.bgView.height = tempBtn.bottom;
    self.bgView.top = (ScreenHeight - tempBtn.bottom)/2;
    //
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = self.bgView.transform;
        self.bgView.transform  =
        CGAffineTransformScale(self.bgView.transform, 0.5, 0.5);
        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.bgView.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }
}
- (void)showWithArray:(NSArray *)array IsCountDown:(NSString*)str AndImageStr:(NSString*)ImageStr AndBigTitle:(NSString*)bigTitle{
    [self removeAllSubviews];
    self.array = array;
    //
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(50,0, ScreenWidth-50.0*2.0,0)];
    self.bgView.layer.cornerRadius = 5.0;
    self.bgView.layer.masksToBounds = YES;
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bgView];
    //大标题
    UILabel * bigTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, self.bgView.width, 20)];
    bigTitleLabel.font = font_cu_cu_17;
    bigTitleLabel.textAlignment = NSTextAlignmentCenter;
    [bigTitleLabel setTextColor:[Utils getUIColorWithHexString:SysColorAlert]];
    [self.bgView addSubview:bigTitleLabel];
    bigTitleLabel.text = bigTitle;
    if (bigTitle == nil) {
        bigTitleLabel.height = 0;
    }
    //
    NSString*titleStr = array[0];
    UILabel*titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(22, bigTitleLabel.bottom+28, self.bgView.width-44.0, 0)];
    titleLabel.font = font_cu_15;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [titleLabel setTextColor:[Utils getUIColorWithHexString:SysColorAlert]];
    [self.bgView addSubview:titleLabel];
    titleLabel.text = titleStr;
    [Utils getUILabel:titleLabel withLineSpacing:10.0];
    //CGSize titleLabelSize = [Utils getUIFontSizeFitH:titleLabel];
    CGSize titleLabelSize = [Utils getUIFontSizeFitH:titleLabel withLineSpacing:10];
    titleLabel.height = titleLabelSize.height;
    [titleLabel sizeToFit];
    titleLabel.frame = CGRectMake((self.bgView.width - titleLabel.size.width)/2, bigTitleLabel.bottom+28, titleLabel.size.width, titleLabel.height);
    self.bgView.height = titleLabel.bottom + 50.0+45;
    self.bgView.top = (ScreenHeight-(titleLabel.bottom + 50.0+45))/2.0;
//    self.backView.height = self.bgView.height + 46;
//    self.backView.top = (ScreenHeight-self.backView.height)/2.0;
    //
    if ([array count] == 2 && [str isEqualToString:@"YES"]) {
        codeTimerStr = 3;
        UIButton*tempBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        tempBtn.frame = CGRectMake( 0, titleLabel.bottom+45, self.bgView.width, 50);
        tempBtn.tag = 1000;
        [tempBtn addTarget:self
                    action:@selector(tempBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];
        [tempBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
        tempBtn.titleLabel.font = font_cu_17;
        tempBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorLightGray];
        tempBtn.enabled = NO;
        self.bgView.userInteractionEnabled = NO;
        self.superview.userInteractionEnabled = NO;
        [self.bgView addSubview:tempBtn];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, titleLabel.bottom+45, self.bgView.width, 0.5)];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.bgView addSubview:lineLabel];
        [self codeBtnTime];
    }
    if ([array count]==2 && str == nil) {
        UIButton*tempBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [tempBtn setBackgroundColor:[Utils getUIColorWithHexString:SysBackColorBlue]];
        tempBtn.frame = CGRectMake( 0, titleLabel.bottom+45, self.bgView.width, 50);
        tempBtn.tag = 1000;
        [tempBtn addTarget:self
                    action:@selector(tempBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];
        [tempBtn setTitle:array[1] forState:UIControlStateNormal];
        [tempBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
        tempBtn.titleLabel.font = font_cu_17;
        [self.bgView addSubview:tempBtn];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, titleLabel.bottom+45, self.bgView.width, 0.5)];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.bgView addSubview:lineLabel];
        //
        titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    //
    if ([array count]==3) {
        UIButton*tempBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [tempBtn setBackgroundColor:[Utils getUIColorWithHexString:SysBackColorBlue]];
        tempBtn.frame = CGRectMake( 0, titleLabel.bottom+45, self.bgView.width/2.0, 50);
        tempBtn.tag = 1000;
        [tempBtn addTarget:self
                    action:@selector(tempBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];
        [tempBtn setTitle:array[1] forState:UIControlStateNormal];
        [tempBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
        tempBtn.titleLabel.font = font_cu_17;
        [self.bgView addSubview:tempBtn];
        //
        UIButton*tempBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [tempBtn1 setBackgroundColor:[Utils getUIColorWithHexString:SysColorbackgray]];
        tempBtn1.frame = CGRectMake( self.bgView.width/2.0, titleLabel.bottom+45, self.bgView.width/2.0, 50);
        tempBtn1.tag = 1001;
        [tempBtn1 addTarget:self
                     action:@selector(tempBtnAction:)
           forControlEvents:UIControlEventTouchUpInside];
        [tempBtn1 setTitle:array[2] forState:UIControlStateNormal];
        [tempBtn1 setTitleColor:[Utils getUIColorWithHexString:SysColorgrayText] forState:UIControlStateNormal];
        tempBtn1.titleLabel.font = font_cu_17;
        [self.bgView addSubview:tempBtn1];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, titleLabel.bottom+45, self.bgView.width, 0.5)];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.bgView addSubview:lineLabel];
        //
        UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(lineLabel.width/2.0, lineLabel.bottom, 0.5, 50.0)];
        lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.bgView addSubview:lineLabel1];
    }
    //
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = self.bgView.transform;
        self.bgView.transform  =
        CGAffineTransformScale(self.bgView.transform, 0.5, 0.5);
        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.bgView.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }
}
//活动界面
- (void)showWithCampainTopArray:(NSArray*)nameArray AndbottomArray:(NSArray*)imageArray AndShareWith:(NSString*)mark{
    [self removeAllSubviews];
    _mark = mark;
    UIView * backView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight - 188,ScreenWidth, 188)];
    backView.backgroundColor = [Utils getUIColorWithHexString:@"eaeef2"];
    [self addSubview:backView];
    //
    UILabel * topLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, ScreenWidth, 15) text:@"邀请好友一起赚收益" font:font_15 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"3a3a3a"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    if ([mark isEqualToString:@"h5"]) {
        if ([LocalService getRBIsIncheck] == YES) {
            topLabel.text = @"邀请好友";
        }else{
            topLabel.text = @"邀请好友一起赚收益";
        }
    }else{
        if ([LocalService getRBIsIncheck] == YES) {
           topLabel.text = @"分享";
        }else{
           topLabel.text = @"分享赚收益";
        }
        
    }
    [backView addSubview:topLabel];
    //
    UIScrollView * socialScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, topLabel.bottom+23, ScreenWidth, 67)];
    socialScroll.showsVerticalScrollIndicator = NO;
    socialScroll.showsHorizontalScrollIndicator = NO;
    socialScroll.userInteractionEnabled = YES;
    socialScroll.bounces = NO;
    [backView addSubview:socialScroll];
    //
    
    for (NSInteger i = 0; i<nameArray.count; i++) {
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(22+(43+40)*i, 0, 43, 43);
        [btn setImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(inviteAction:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 1000+i;
        [socialScroll addSubview:btn];
        //
        UILabel * nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(btn.left, btn.bottom+11, btn.width,10) text:nameArray[i] font:font_11 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"5e5e5e"] backgroundColor:[UIColor clearColor] numberOfLines:1];
        [socialScroll addSubview:nameLabel];
    }
    socialScroll.contentSize = CGSizeMake(22+43*imageArray.count+40*(imageArray.count-1)+22, 64);
    //
    UIButton * cancelBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, socialScroll.bottom+20, ScreenWidth, 47) title:@"取消" hlTitle:nil titleColor:[Utils getUIColorWithHexString:@"3a3a3a"] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:@"f7fafc"] image:nil hlImage:nil];
    [cancelBtn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.tag = nameArray.count;
    [backView addSubview:cancelBtn];
    
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = backView.transform;
        backView.transform  =
        CGAffineTransformScale(backView.transform, 0.5, 0.5);
        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            backView.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }

}

-(void)inviteAction:(id)sender{
    UIButton*btn = (UIButton*)sender;
    [self removeFromSuperview];
    if ([_mark isEqualToString:@"h5"]) {
        SSDKPlatformType type;
        if (btn.tag == 1000) {
            type = SSDKPlatformTypeSinaWeibo;
        } else if (btn.tag == 1001) {
            type = SSDKPlatformSubTypeWechatSession;
            if (self.shareH5Url.length>0) {
                [self shareSDKWithTitle:self.shareH5Title Content:self.shareH5Content URL:self.shareH5Url ImgURL:self.shareImageUrl SSDKPlatformType:type];
                return;
            }
        } else if (btn.tag == 1002) {
            type = SSDKPlatformSubTypeWechatTimeline;
        } else if (btn.tag == 1003) {
            type = SSDKPlatformSubTypeQQFriend;
        } else {
            type = SSDKPlatformSubTypeQZone;
        }
        if (self.shareurl.length>0) {
            [self shareSDKWithTitle:self.shareTitle Content:self.shareContent URL:self.shareurl ImgURL:self.shareImageUrl SSDKPlatformType:type];
            return;
        }
        [self shareSDKWithTitle:self.shareTitle Content:self.shareContent URL:[NSString stringWithFormat:@"%@invite?inviter_id=%@",ApiUrl,[LocalService getRBLocalDataUserLoginId]] ImgURL:self.shareImageUrl SSDKPlatformType:type];
    }else if ([_mark isEqualToString:@"leader"]){
        SSDKPlatformType type;
        if (btn.tag == 1000) {
            type = SSDKPlatformTypeSinaWeibo;
        } else if (btn.tag == 1001) {
            type = SSDKPlatformSubTypeWechatSession;
            
        } else if (btn.tag == 1002) {
            type = SSDKPlatformSubTypeWechatTimeline;
        } else if (btn.tag == 1003) {
            type = SSDKPlatformSubTypeQQFriend;
        } else {
            type = SSDKPlatformSubTypeQZone;
        }
        if (self.shareH5Url.length>0) {
            [self shareSDKWithTitle:self.shareH5Title Content:self.shareH5Content URL:self.shareH5Url ImgURL:self.shareImageUrl SSDKPlatformType:type];
            return;
        }
        [self shareSDKWithTitle:self.shareTitle Content:self.shareContent URL:[NSString stringWithFormat:@"%@invite?inviter_id=%@",ApiUrl,[LocalService getRBLocalDataUserLoginId]] ImgURL:self.shareImageUrl SSDKPlatformType:type];
    }else{
        if ([self.delegate respondsToSelector:@selector(RBNewAlert:clickedButtonAtIndex:)]) {
            [self.delegate RBNewAlert:self clickedButtonAtIndex:(int)btn.tag - 1000];
        }
    }
}

-(void)cancelBtnAction:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(RBNewAlertCancelButtonAction:)]) {
        [self.delegate RBNewAlertCancelButtonAction:self];
    }
    [self removeFromSuperview];
}
-(void)codeBtnTime {
    UIButton * codeBtn = [self.bgView viewWithTag:1000];
    [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55] forState:UIControlStateNormal];
    [codeBtn setTitle:[NSString stringWithFormat:@"%ds",codeTimerStr] forState:UIControlStateNormal];
    codeBtn.enabled = NO;
    self.userInteractionEnabled = NO;
    self.superview.userInteractionEnabled = NO;
    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                               target  :self
                                               selector:@selector(timeLimit)
                                               userInfo:nil
                                               repeats :YES];
}
- (void)timeLimit {
    UIButton * codeBtn = [self.bgView viewWithTag:1000];
    codeBtn.enabled = NO;
    self.userInteractionEnabled = NO;
    self.superview.userInteractionEnabled = NO;
    if (codeTimerStr == 0) {
        [codeTimer invalidate];
        codeTimer = nil;
        [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
        codeBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
        [codeBtn setTitle:@"知道了" forState:UIControlStateNormal];
        codeBtn.enabled = YES;
        self.userInteractionEnabled = YES;
        self.superview.userInteractionEnabled = YES;
    } else {
        codeTimerStr = codeTimerStr - 1;
        [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55] forState:UIControlStateNormal];
        codeBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorLightGray];
        [codeBtn setTitle:[NSString stringWithFormat:@"%ds",codeTimerStr] forState:UIControlStateNormal];
        codeBtn.enabled = NO;
        self.userInteractionEnabled = NO;
        self.superview.userInteractionEnabled = NO;
    }
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)tapGesture {
    if ([self.delegate respondsToSelector:@selector(RBNewAlertTapAction:)]) {
        [self.delegate RBNewAlertTapAction:self];
    }else{
        [self removeFromSuperview];
    }
}

#pragma mark - UIButton Delegate
- (void)tempBtnAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(RBNewAlert:clickedButtonAtIndex:)]) {
        [self.delegate RBNewAlert:self clickedButtonAtIndex:(int)sender.tag-1000];
    }
    [self removeFromSuperview];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
