//
//  NSObject+KVC.m
//  CloudLabel

#import "NSObject+KVC.h"

@implementation NSObject(aa)
- (id)valueForUndefinedKey:(NSString *)key{
    
    return objc_getAssociatedObject(self, (__bridge const void *)(key));
}
- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    objc_setAssociatedObject(self, (__bridge const void *)(key), value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
