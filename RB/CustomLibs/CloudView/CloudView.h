//
//  CloudView.h
//  CloudLabel
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreFoundation/CoreFoundation.h>

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 100000
// CAAnimationDelegate is not available before iOS 10 SDK
@interface CloudView : UIView
#else
@interface CloudView : UIView <CAAnimationDelegate>
#endif

-(void)reloadData:(NSArray *)ary;
@end


@interface NSArray (Modulo)
- (id)objectAtModuloIndex:(NSUInteger)index;
@end
