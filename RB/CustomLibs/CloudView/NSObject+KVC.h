//
//  NSObject+KVC.h
//  CloudLabel

#import <UIKit/UIKit.h>
#import <objc/runtime.h>
@interface NSObject (aa)
- (id)valueForUndefinedKey:(NSString *)key;
- (void)setValue:(id)value forUndefinedKey:(NSString *)key;
@end
