//
//  UIButton+init.m
//  优梦优
//
//  Created by 刘文 on 16/7/13.
//  Copyright © 2016年 孙云飞. All rights reserved.
//

#import "UIButton+init.h"

@implementation UIButton (init)
- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
                      hlTitle:(NSString *)hlTitle
                   titleColor:(UIColor *)titleColor
                 hlTitleColor:(UIColor *)hlTitleColor
              backgroundColor:(UIColor *)backgroundColor
                        image:(UIImage *)image
                      hlImage:(UIImage *)hlimage{
    if (self = [super init]) {
        self.frame = frame;
        [self setTitle:title forState:UIControlStateNormal];
        [self setTitle:hlTitle forState:UIControlStateHighlighted];
        [self setTitleColor:titleColor forState:UIControlStateNormal];
        [self setTitleColor:hlTitleColor forState:UIControlStateHighlighted];
        [self setBackgroundColor:backgroundColor];
        [self setBackgroundImage:image forState:UIControlStateNormal];
        [self setBackgroundImage:hlimage forState:UIControlStateHighlighted];
    }
    return self;
}
@end
