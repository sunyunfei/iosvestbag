//
//  AMPActivityIndicator.h
//
#import <UIKit/UIKit.h>

@interface AMPActivityIndicator : UIView

@property (nonatomic) UIColor *barColor;
@property (nonatomic) CGFloat barWidth;
@property (nonatomic) CGFloat barHeight;
@property (nonatomic) CGFloat aperture;

- (void)startAnimating;
- (void)stopAnimating;
- (BOOL)isAnimating;

@end
