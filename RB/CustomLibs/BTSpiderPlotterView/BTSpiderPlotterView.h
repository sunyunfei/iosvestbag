//
//  ViewController.m
//  BTLibrary
//
//  Created by Byte on 5/29/13.
//  Copyright (c) 2013 Byte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface BTSpiderPlotterView : UIView

//example dictionary
/*
 NSDictionary *valueDictionary =  @{@"Design": @"7",
 @"Camera" : @"6",
 @"Reception": @"7",
 @"Performance" : @"9",
 @"Software": @"8"};
 BTSpiderPlotterView *spiderView = [[BTSpiderPlotterView alloc] initWithFrame:self.view.frame valueDictionary:valueDictionary];
 [spiderView setMaxValue:10];
 [spiderView setTransform:CGAffineTransformMakeRotation(-M_PI/2.0)];
 spiderView.plotColor = [UIColor colorWithRed:.8 green:.4 blue:.3 alpha:.7];
 [self.view addSubview:spiderView];
 */
- (id)initWithFrame:(CGRect)frame valueDictionary:(NSDictionary *)valueDictionary;

@property (nonatomic, assign) CGFloat valueDivider; // default 1
@property (nonatomic, assign) CGFloat maxValue; // default to the highest value in the dictionary
@property (nonatomic, strong) UIColor *drawboardColor; // defualt black
@property (nonatomic, strong) UIColor *plotColor; // defualt dark grey

@property (nonatomic, strong) NSDictionary*valueDictionary;
@property (nonatomic, strong) NSMutableArray*pointsLengthArrayArray;
@property (nonatomic, strong) NSMutableArray*pointsToPlotArray;

@end
