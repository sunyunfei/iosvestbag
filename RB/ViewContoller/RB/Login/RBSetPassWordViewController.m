//
//  RBSetPassWordViewController.m
//  RB
//
//  Created by RB8 on 2017/7/13.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBSetPassWordViewController.h"
#import "RegularHelp.h"
#import "RBKolApplyInfoViewController.h"
#import "RBWelcomeToViewController.h"
#import "RBAppDelegate.h"
@interface RBSetPassWordViewController ()
{
    InsetsTextField*nameTF;
    InsetsTextField*passwordTF;
    UIView*editView;
}
@end

@implementation RBSetPassWordViewController
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEdit];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.navTitle = @"设置密码";
    self.delegate = self;
    [self loadEditView];
}
- (void)RBNavLeftBtnAction {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)loadEditView{
   
    editView = [[UIView alloc]initWithFrame:CGRectMake(0, (ScreenHeight-50.0*2.0 - 144)/2.0,ScreenWidth, 0)];
    editView.userInteractionEnabled = YES;
    [self.view addSubview:editView];
    //
    nameTF = [[InsetsTextField alloc]
               initWithFrame:CGRectMake(15, 0, ScreenWidth-30.0, 50.0)];
    nameTF.font = font_(16.0);
    nameTF.backgroundColor = [UIColor clearColor];
    nameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    if ([self.mark isEqualToString:@"register"]) {
        nameTF.attributedPlaceholder = [[NSAttributedString alloc]
                                        initWithString:NSLocalizedString(@"R1110", @"名字")
                                        attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
        nameTF.secureTextEntry = NO;
        nameTF.keyboardType = UIKeyboardTypeDefault;
    }else{
        nameTF.attributedPlaceholder = [[NSAttributedString alloc]
                                        initWithString:NSLocalizedString(@"R1105", @"密码")
                                        attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
        nameTF.secureTextEntry = YES;
        nameTF.keyboardType = UIKeyboardTypeASCIICapable;
    }
    
    nameTF.returnKeyType = UIReturnKeyDone;
    nameTF.delegate = self;
    [editView addSubview:nameTF];
    //    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]
    //                                      initWithFrame:CGRectMake(0, ScreenHeight - 216 - 44,
    //                                                               ScreenWidth, 216 + 44)];
    //    textFiledKB.backgroundColor = [UIColor clearColor];
    //    textFiledKB.delegateT = self;
    //    [emailTF setInputAccessoryView:textFiledKB];
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(nameTF.left,nameTF.bottom-0.5f, nameTF.width, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    [editView addSubview:lineLabel];
    //
    passwordTF = [[InsetsTextField alloc]
              initWithFrame:CGRectMake(15, nameTF.bottom+10.0,nameTF.width, 50.0)];
    passwordTF.font = font_(16.0);
    passwordTF.delegate = self;
    passwordTF.backgroundColor = [UIColor clearColor];
    passwordTF.secureTextEntry = YES;
    //    codeTF.keyboardType = UIKeyboardTypeNumberPad;
    passwordTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    if ([self.mark isEqualToString:@"register"]) {
        passwordTF.attributedPlaceholder = [[NSAttributedString alloc]
                                            initWithString:NSLocalizedString(@"R1105", @"密码")
                                            attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
        
    }else{
        passwordTF.attributedPlaceholder = [[NSAttributedString alloc]
                                            initWithString:NSLocalizedString(@"R1112", @"确认密码")
                                            attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
    }
    passwordTF.keyboardType = UIKeyboardTypeASCIICapable;
    passwordTF.returnKeyType = UIReturnKeyDone;
    [editView addSubview:passwordTF];
    //    [codeTF setInputAccessoryView:textFiledKB];
    UILabel *lineLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(nameTF.left,passwordTF.bottom-0.5f, nameTF.width, 0.5f)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    [editView addSubview:lineLabel1];
    editView.height = lineLabel1.bottom;
    //
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setTitleColor:[Utils getUIColorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [loginBtn setTitle:NSLocalizedString(@"R1109", @"下一步") forState:UIControlStateNormal];
    loginBtn.titleLabel.font=font_cu_cu_15;
    loginBtn.frame = CGRectMake(15.0, editView.bottom+100.0, ScreenWidth-30.0, 44.0);
    [loginBtn addTarget:self
                 action:@selector(nextAction:)
       forControlEvents:UIControlEventTouchUpInside];
    loginBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    [self.view addSubview:loginBtn];
    //
    
}
- (void)nextAction:(id)sender{
    if ([self.mark isEqualToString:@"register"]) {
        if (nameTF.text.length == 0) {
            [self.hudView showErrorWithStatus:@"请输入名字"];
            return;
        }
        if (passwordTF.text.length == 0) {
            [self.hudView showErrorWithStatus:@"请输入密码"];
            return;
        }
        if (passwordTF.text.length < 6) {
            [self.hudView showErrorWithStatus:@"密码最少6位"];
            return;
        }
        Handler * hanlder = [Handler shareHandler];
        hanlder.delegate = self;
        [hanlder getRBRegisterName:nameTF.text AndPassword:passwordTF.text AndEmail:self.email AndVtoken:self.vtoken];
    }else{
        if (nameTF.text.length == 0) {
            [self.hudView showErrorWithStatus:@"请输入密码"];
            return;
        }
        if (nameTF.text.length < 6) {
            [self.hudView showErrorWithStatus:@"密码至少6位"];
        }
        if (![nameTF.text isEqualToString:passwordTF.text]) {
            [self.hudView showErrorWithStatus:@"两次密码输入不一致"];
            return;
        }
        
        Handler * hanlder = [Handler shareHandler];
        hanlder.delegate = self;
        [hanlder getRBUpdatePassword:self.email AndNewPassword:nameTF.text AndpasswordConfirm:passwordTF.text andVtoken:self.vtoken];
    }
    
}
#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}
#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEdit];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    float keyboardTop = ScreenHeight-320;
    if (editView.bottom > keyboardTop) {
        [UIView animateWithDuration:0.2 animations:^{
            editView.bottom = keyboardTop;
        } completion:^(BOOL finished) {
        }];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.2 animations:^{
        editView.top = (ScreenHeight-50.0*2.0 - 144.0)/2.0;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)endEdit {
    [self.view endEditing:YES];
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"registers"]) {
        [GeTuiSdk setPushModeForOff:NO];
        [JsonService setRBUserEntityWith:jsonObject];
        RBUserEntity*userEntity = [JsonService getRBUserEntity:jsonObject];
        [LocalService setRBLocalDataUserLoginIdWith:userEntity.iid];
        [LocalService setRBLocalDataUserLoginNameWith:userEntity.name];
        [LocalService setRBLocalDataUserLoginPhoneWith:userEntity.mobile_number];
        [LocalService setRBLocalDataUserLoginAvatarWith:userEntity.avatar_url];
        [LocalService setRBLocalDataUserPrivateTokenWith:userEntity.issue_token];
        [LocalService setRBLocalDataUserLoginKolIdWith:userEntity.kol_uuid];
        [LocalService setRBLocalEmail:userEntity.email];
        
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R1113", @"注册成功")];
        RBWelcomeToViewController * welcome = [[RBWelcomeToViewController alloc]init];
        welcome.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:welcome animated:YES];
       
    }
    if ([sender isEqualToString:@"sessions/update_password"]) {
        [GeTuiSdk setPushModeForOff:NO];
        [JsonService setRBUserEntityWith:jsonObject];
        RBUserEntity*userEntity = [JsonService getRBUserEntity:jsonObject];
        [LocalService setRBLocalDataUserLoginIdWith:userEntity.iid];
        [LocalService setRBLocalDataUserLoginNameWith:userEntity.name];
        [LocalService setRBLocalDataUserLoginPhoneWith:userEntity.mobile_number];
        [LocalService setRBLocalDataUserLoginAvatarWith:userEntity.avatar_url];
        [LocalService setRBLocalDataUserPrivateTokenWith:userEntity.issue_token];
        [LocalService setRBLocalDataUserLoginKolIdWith:userEntity.kol_uuid];
        [LocalService setRBLocalEmail:userEntity.email];
        //
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:1];
    }
}
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
