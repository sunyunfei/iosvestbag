//
//  RBWelcomeToViewController.h
//  RB
//
//  Created by RB8 on 2018/1/29.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBWelcomeToViewController : RBBaseViewController
- (IBAction)GO:(UIButton *)sender;

@end
