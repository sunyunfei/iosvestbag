//
//  RBRegisteremailViewController.h
//  RB
//
//  Created by RB8 on 2018/1/26.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBRegisteremailViewController : RBBaseViewController<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,RBActionSheetDelegate,UIAlertViewDelegate>

@end
