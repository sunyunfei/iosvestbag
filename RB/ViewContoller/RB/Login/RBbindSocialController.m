//
//  RBbindSocialController.m
//  RB
//
//  Created by RB8 on 2017/8/3.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBbindSocialController.h"
#import "RBAppDelegate.h"
#import "Service.h"
#import "RBInfluenceOrcaController.h"
@interface RBbindSocialController ()
{
    NSString * editString;
    NSString * userName;
}
@end

@implementation RBbindSocialController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
    self.navigationController.navigationBar.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    [self loadEditView];
}
-(void)loadEditView{
    //
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(ScreenWidth-30-40, 40, 40, 20);
    [btn setTitle:@"跳过" forState:UIControlStateNormal];
    [btn setTitleColor:[Utils getUIColorWithHexString:ccColor797979] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(jump) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = font_13;
    [self.view addSubview:btn];
    //
    UIImageView * logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth - 57)/2, 167, 57, 85)];
    logoImageView.image = [UIImage imageNamed:@"RBlogo"];
    [self.view addSubview:logoImageView];
    //
    UILabel * bindLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, logoImageView.bottom+57, ScreenWidth, 20)];
    bindLabel.text = @"绑定你的社交账号";
    bindLabel.font = [UIFont systemFontOfSize:21];
    bindLabel.textColor = [Utils getUIColorWithHexString:ccColor444444];
    bindLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:bindLabel];
    //
    UILabel * describeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, bindLabel.bottom+31, ScreenWidth, 15)];
    describeLabel.text = @"我们免费为你提供最精准的数据服务";
    describeLabel.font = font_15;
    describeLabel.textColor = [Utils getUIColorWithHexString:ccColor444444];
    describeLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:describeLabel];
    //
    UIButton * weiboBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    weiboBtn.frame = CGRectMake(110, describeLabel.bottom+57, 50, 50);
    [weiboBtn setImage:[UIImage imageNamed:@"RbWeibo"] forState:UIControlStateNormal];
    [weiboBtn addTarget:self action:@selector(weiboBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:weiboBtn];
    //
    UIButton * wechatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    wechatBtn.frame = CGRectMake(ScreenWidth-160, describeLabel.bottom+57, 50, 50);
    [wechatBtn setImage:[UIImage imageNamed:@"RbWechat"] forState:UIControlStateNormal];
    [wechatBtn addTarget:self action:@selector(wechatBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:wechatBtn];
    //
//    UILabel * clauseLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, ScreenHeight-60, 100, 10)];
//    clauseLabel.text = @"我同意添加robin8个人助手";
//    clauseLabel.textColor = [Utils getUIColorWithHexString:ccColor797979];
//    clauseLabel.font = font_11;
//    [clauseLabel sizeToFit];
//    clauseLabel.frame = CGRectMake((ScreenWidth-clauseLabel.size.width)/2, ScreenHeight-60, clauseLabel.size.width, clauseLabel.size.height);
//    [self.view addSubview:clauseLabel];
    //
    //
    TTTAttributedLabel *agreeLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, ScreenHeight-60,200, 10)];
    agreeLabel.font = font_10;
    agreeLabel.textColor = [Utils getUIColorWithHexString:ccColor797979];
    agreeLabel.textAlignment = NSTextAlignmentCenter;
    agreeLabel.text = @"点击登录即同意用户服务协议";
    [agreeLabel sizeToFit];
    agreeLabel.frame = CGRectMake((ScreenWidth-agreeLabel.size.width)/2, ScreenHeight-60, agreeLabel.size.width, agreeLabel.size.height);
    [self.view addSubview:agreeLabel];
    [agreeLabel setText:agreeLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:@"用户服务协议" options:NSCaseInsensitiveSearch];
         CTFontRef font = CTFontCreateWithName((CFStringRef)font_10.fontName, font_10.pointSize, NULL);
         [mStr addAttributes:@{(NSString *)kCTFontAttributeName:(__bridge id)font,(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:ccColor2bdbe2] CGColor]} range:range];
         return mStr;
     }];
    UIButton * isagreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    isagreeBtn.frame = CGRectMake(agreeLabel.left-20, agreeLabel.top-2, 15, 15);
    isagreeBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
    isagreeBtn.layer.cornerRadius = 15/2;
    isagreeBtn.clipsToBounds = YES;
    [isagreeBtn addTarget:self action:@selector(agreeBtn:) forControlEvents:UIControlEventTouchUpInside];
    [isagreeBtn setImage:[UIImage imageNamed:@"RBcheck"] forState:UIControlStateNormal];
    [self.view addSubview:isagreeBtn];
}
-(void)jump{
    RBInfluenceOrcaController * orcaVC = [[RBInfluenceOrcaController alloc]init];
    [self.navigationController pushViewController:orcaVC animated:YES];
}
-(void)weiboBtnClick:(id)sender{
    [self ThirdLoginWithTag:5002];
}
-(void)wechatBtnClick:(id)sender{
    [self ThirdLoginWithTag:5000];
}
-(void)agreeBtn:(id)sender{
   
}
//社交账号绑定
- (void)ThirdLoginWithTag:(int)tag {
    SSDKPlatformType type;
    NSString*provider=@"";
    if (tag == 5002) {
        provider = @"weibo";
        type = SSDKPlatformTypeSinaWeibo;
    } else {
        provider = @"wechat";
        type = SSDKPlatformTypeWechat;
    }
    [ShareSDK cancelAuthorize:type];
    [ShareSDK getUserInfo:type
           onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error){
               NSLog(@"user:%@,credential:%@,rawData:%@",user,user.credential,user.rawData);
               if (state == SSDKResponseStateSuccess) {
                   userName = [NSString stringWithFormat:@"%@",user.nickname];
                   //screenView.tLabel.text = [NSString stringWithFormat:@"%@已绑定 %@",self.editString,userName];
                   // RB-社交账号绑定
                   Handler*handler = [Handler shareHandler];
                   handler.delegate = self;
                   if (type == SSDKPlatformTypeWechat) {
                       NSString*unionid = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"unionid"]];
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       editString = @"微信";
                       // socialBtn.iconIV.image = [UIImage imageNamed:@"微信_ON.png"];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:unionid andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                       [self.hudView show];
                       
                   }
                   if (type == SSDKPlatformTypeSinaWeibo) {
                       editString = @"微博";
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*followers_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"followers_count"]];
                       NSString*created_at = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"created_at"]];
                       NSString*statuses_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"statuses_count"]];
                       NSString*verified = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"verified"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"avatar_large"]];
                       NSString*refresh_token = nil;
                       NSString*str = [NSString stringWithFormat:@"%@",user.credential];
                       if([[str componentsSeparatedByString:@"\"refresh_token\" = \""] count]>1) {
                           str = [str componentsSeparatedByString:@"\"refresh_token\" = \""][1];
                           if([[str componentsSeparatedByString:@"\";"] count]>1) {
                               refresh_token = [str componentsSeparatedByString:@"\";"][0];
                           }
                       }
                       //socialBtn.iconIV.image = [UIImage imageNamed:@"微博_ON.png"];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:followers_count andStatuses_count:statuses_count andRegistered_at:created_at andVerified:verified andRefresh_token:refresh_token andUnionid:nil andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                       [self.hudView show];
                   }
                   if (type == SSDKPlatformTypeQQ) {
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*city = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"city"]];
                       NSString*is_yellow_vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"is_yellow_vip"]];
                       NSString*vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"vip"]];
                       NSString*province = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"province"]];
                       NSString*gender = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"gender"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"figureurl_qq_2"]];
                       //  socialBtn.iconIV.image = [UIImage imageNamed:@"QQ_ON.png"];
                       
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:nil andProvince:province andCity:city andGender:gender andIs_vip:vip andIs_yellow_vip:is_yellow_vip];
                       [self.hudView show];
                       
                   }
               }
               if (state == SSDKResponseStateFail) {
                   if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                       [self.hudView showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                   } else {
                       [self.hudView showErrorWithStatus:NSLocalizedString(@"R1042", @"登录失败")];
                   }
               }
               if (state == SSDKResponseStateCancel) {
                   [self.hudView dismiss];
                   //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
               }
           }];
}

-(void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    if ([sender isEqualToString:@"kols/identity_bind"]) {
        [self.hudView dismiss];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBKOLApplyWithProvider:editString andHomepage:nil andUsername:userName andPrice:@"1" andFollowers:@"10" andScreenshot:nil andUid:nil];
    }
    if ([sender isEqualToString:@"update_social"]) {
        [self.hudView dismiss];
        RBInfluenceOrcaController * orcaVC = [[RBInfluenceOrcaController alloc]init];
        orcaVC.hidesBottomBarWhenPushed = YES;
        if ([editString isEqualToString:@"微博"]) {
            orcaVC.isBind = @"yes";
        }
        [self.navigationController pushViewController:orcaVC animated:YES];
    }

}
-(void)handlerError:(NSError *)error Tag:(NSString *)sender{
    
}
-(void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
