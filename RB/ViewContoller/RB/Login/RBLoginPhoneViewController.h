//
//  RBLoginPhoneViewController.h
//  RB
//
//  Created by AngusNi on 16/2/6.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBLoginPhoneViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,RBActionSheetDelegate>

@end
