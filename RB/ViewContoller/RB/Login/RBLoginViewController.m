//
//  RBLoginViewController.m
//  RB
//
//  Created by AngusNi on 15/7/11.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBLoginViewController.h"
#import "RBAppDelegate.h"
#import "RBKolApplyInfoViewController.h"
#import "RBSetPassWordViewController.h"
#import "RBbindSocialController.h"
#import "RBLoginSocialEntity.h"
#import "RBInfluenceOrcaController.h"
#import <ShareSDKExtension/ShareSDK+Extension.h>
#import "RBRegisteremailViewController.h"
#import "RBForgetSecretViewController.h"
#import "RegularHelp.h"
@interface RBLoginViewController (){
    InsetsTextField*phoneTF;
    InsetsTextField*codeTF;
    InsetsTextField*inviteTF;
    UIButton *codeBtn;
    NSTimer *codeTimer;
    int codeTimerStr;
    UIView*thirdView;
    UIView*editView;
    UILabel*changeLabel;
    RBUserEntity*SocialuserEntity;
    UIButton * forgetBtn;
    UIButton * phoneLogin;
    UIButton * emailLogin;
    
    NSString * editString;
    NSString * userName;
    //微博数据
    NSString*unionid;
    NSString*serial_params;
    NSString*followers_count;
    NSString*created_at;
    NSString*statuses_count;
    NSString*verified;
    NSString*refresh_token;
    
    NSString*city;
    NSString*is_yellow_vip;
    NSString*vip;
    NSString*province;
    NSString*gender;
    NSString*uid;
    NSString * token;
    NSString * url;
    NSString * icon;
    NSString * aboutMe;
    
    RBUserEntity*userEntity;
}
@end

@implementation RBLoginViewController
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEdit];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.bgIV.hidden = NO;
    self.navTitle = NSLocalizedString(@"R1013", @"注册/登录");
    self.navView.backgroundColor = [UIColor clearColor];
    self.navLineLabel.hidden = YES;
    self.navLeftLabel.hidden = NO;
    self.edgePGR.enabled = NO;
    [self loadEditView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    //
    [TalkingData trackPageBegin:@"login"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [codeTimer invalidate];
    codeTimer = nil;
    //
    //
    [TalkingData trackPageEnd:@"login"];
}

- (BOOL)becomeFirstResponder{
    return YES;
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationShowLoginView {
}

- (void)RBNotificationShowMessageView {
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    if(self.isPush==YES) {
        [self dismissVC];
    } else {
        RBAlertView *alertView = [[RBAlertView alloc]init];
        alertView.delegate = self;
        alertView.tag = 12000;
        [alertView showWithArray:@[NSLocalizedString(@"R6085", @"用游客身份登录"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
    }
}
- (void)RBNavRightBtnAction{
   
}
- (void)visitorBtnAction:(UIButton*)sender {
    [self.hudView show];
    // RB-游客登录
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserLoginWithPhone:@"13000000000" andCode:@"123456" andInvteCode:nil];
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if (alertView.tag == 12000) {
        if (buttonIndex==0) {
            [self.hudView showOverlay];
            // RB-游客登录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBUserLoginWithPhone:@"13000000000" andCode:@"123456" andInvteCode:nil];
            return;
        }
    }else if (alertView.tag == 13000){
        if (buttonIndex == 0) {
            //直接登录
            [self.hudView showOverlay];
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBUserLoginWithPhone:phoneTF.text andCode:codeTF.text andInvteCode:nil];
        }else if (buttonIndex == 1){
            //注册
            RBSetPassWordViewController * setPass = [[RBSetPassWordViewController alloc]init];
            [self.navigationController pushViewController:setPass animated:YES];
        }
    }
    
}
#pragma mark- 注册
- (void)registerVC{
    RBRegisteremailViewController * registerVC = [[RBRegisteremailViewController alloc]init];
    registerVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:registerVC animated:YES];
}
#pragma mark- loadEditView Delegate
- (void)loadEditView {
    editView = [[UIView alloc]initWithFrame:CGRectMake(0, (ScreenHeight-50.0*5.0)/2.0,ScreenWidth, 0)];
    editView.userInteractionEnabled = YES;
    [self.view addSubview:editView];
    //手机号登录按钮
    phoneLogin = [[UIButton alloc]initWithFrame:CGRectMake(0, editView.top - 90, ScreenWidth/2, 20) title:@"手机号登录" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorBlue] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    [phoneLogin addTarget:self action:@selector(phoneLoginAction:) forControlEvents:UIControlEventTouchUpInside];
    phoneLogin.tag = 1024;
    [self.view addSubview:phoneLogin];
    //
    CGSize size = [Utils getUIFontSizeFitW:phoneLogin.titleLabel];
    changeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, phoneLogin.bottom, size.width, 2) text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorBlue] numberOfLines:1];
    [self.view addSubview:changeLabel];
    changeLabel.centerX = phoneLogin.centerX;
    //邮箱登录按钮
    emailLogin = [[UIButton alloc]initWithFrame:CGRectMake(phoneLogin.right, phoneLogin.top, phoneLogin.width, phoneLogin.height) title:@"邮箱登录" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorGray] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    [emailLogin addTarget:self action:@selector(phoneLoginAction:) forControlEvents:UIControlEventTouchUpInside];
    emailLogin.tag = 1025;
    [self.view addSubview:emailLogin];
    //
    
    //
    phoneTF = [[InsetsTextField alloc]
               initWithFrame:CGRectMake(15, 0, ScreenWidth-30.0, 50.0)];
    phoneTF.font = font_(16.0);
    phoneTF.backgroundColor = [UIColor clearColor];
    phoneTF.keyboardType = UIKeyboardTypeASCIICapable;
    phoneTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    phoneTF.attributedPlaceholder = [[NSAttributedString alloc]
                                     initWithString:NSLocalizedString(@"R1003", @"手机号")
                                     attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
    phoneTF.returnKeyType = UIReturnKeyDone;
    phoneTF.delegate = self;
    [editView addSubview:phoneTF];
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(phoneTF.left,phoneTF.bottom-0.5f, phoneTF.width, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    [editView addSubview:lineLabel];
    //
    codeTF = [[InsetsTextField alloc]
              initWithFrame:CGRectMake(15, phoneTF.bottom+10.0,ScreenWidth-30.0-95.0, 50.0)];
    codeTF.font = font_(16.0);
    codeTF.delegate = self;
    codeTF.backgroundColor = [UIColor clearColor];
    codeTF.keyboardType = UIKeyboardTypeASCIICapable;
    codeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    codeTF.attributedPlaceholder = [[NSAttributedString alloc]
                                    initWithString:NSLocalizedString(@"R1004", @"验证码")
                                    attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
    codeTF.returnKeyType = UIReturnKeyDone;
    [editView addSubview:codeTF];
    UILabel *lineLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(phoneTF.left,codeTF.bottom-0.5f, phoneTF.width, 0.5f)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    [editView addSubview:lineLabel1];
    //
    UILabel *lineLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-15.0-100.0,lineLabel.bottom+20.0, 0.5f, 30)];
    lineLabel2.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    lineLabel2.tag = 1026;
    [editView addSubview:lineLabel2];
    codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    [codeBtn setTitle:NSLocalizedString(@"R1005", @"获取验证码") forState:UIControlStateNormal];
    codeBtn.titleLabel.font = font_(14);
    codeBtn.frame = CGRectMake(ScreenWidth-15.0-100.0, codeTF.top, 100.0, codeTF.height);
    [codeBtn addTarget:self
                action:@selector(codeBtnAction:)
      forControlEvents:UIControlEventTouchUpInside];
    codeBtn.backgroundColor = [UIColor clearColor];
    [editView addSubview:codeBtn];
    //
    inviteTF = [[InsetsTextField alloc]
               initWithFrame:CGRectMake(15, codeTF.bottom+10.0, ScreenWidth-30.0, 50.0)];
    inviteTF.font = font_(16.0);
    inviteTF.backgroundColor = [UIColor clearColor];
    inviteTF.keyboardType = UIKeyboardTypeURL;
    inviteTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    inviteTF.attributedPlaceholder = [[NSAttributedString alloc]
                                     initWithString:NSLocalizedString(@"R1104", @"邀请码")
                                     attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
    inviteTF.returnKeyType = UIReturnKeyDone;
    inviteTF.delegate = self;
    [editView addSubview:inviteTF];
    UILabel *lineLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(phoneTF.left,inviteTF.bottom-0.5f, inviteTF.width, 0.5f)];
    lineLabel3.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    lineLabel3.tag = 2000;
    [editView addSubview:lineLabel3];
    //
    editView.height = inviteTF.bottom;
    //
    forgetBtn = [[UIButton alloc]initWithFrame:CGRectMake(phoneTF.left, codeTF.bottom + 5, 200,25) title:NSLocalizedString(@"R1111", @"忘记密码") hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorBlue] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    [forgetBtn addTarget:self action:@selector(forgetVC) forControlEvents:UIControlEventTouchUpInside];
    forgetBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    forgetBtn.titleLabel.font = font_13;
    forgetBtn.hidden = YES;
    [editView addSubview:forgetBtn];
    //
    UIButton *visitorBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    visitorBtn.frame = CGRectMake(0, ScreenHeight-220.0, ScreenWidth, 16);
    NSString * str = NSLocalizedString(@"R5172",@"游客 >");
    NSMutableAttributedString * mAttStr = [[NSMutableAttributedString alloc]initWithString:str];
    [ mAttStr addAttribute:NSFontAttributeName value:font_cu_(13) range:NSMakeRange(0, str.length)];
    [mAttStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorBlue] range:NSMakeRange(0, str.length)];
    [visitorBtn setAttributedTitle:mAttStr forState:UIControlStateNormal];
    [visitorBtn addTarget:self
                 action:@selector(visitorBtnAction:)
       forControlEvents:UIControlEventTouchUpInside];
    visitorBtn.tag = 1919;
    [self.view addSubview:visitorBtn];
    //
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setTitleColor:[Utils getUIColorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [loginBtn setTitle:NSLocalizedString(@"R1016", @"登录") forState:UIControlStateNormal];
    loginBtn.titleLabel.font=font_cu_cu_15;
    loginBtn.frame = CGRectMake(15.0, visitorBtn.bottom+15.0, ScreenWidth-30.0, 44.0);
    [loginBtn addTarget:self
                 action:@selector(loginBtnAction:)
       forControlEvents:UIControlEventTouchUpInside];
    loginBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    [self.view addSubview:loginBtn];
    //
    TTTAttributedLabel *aboutLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(0, loginBtn.bottom+7.0, ScreenWidth, 15)];
    aboutLabel.font = font_11;
    aboutLabel.textAlignment = NSTextAlignmentCenter;
    aboutLabel.textColor = [Utils getUIColorWithHexString:SysColorSubBlack];
    aboutLabel.text = NSLocalizedString(@"R1014", @"*点击登录或社交账号即同意用户服务协议");
    [self.view addSubview:aboutLabel];
    [aboutLabel setText:aboutLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:NSLocalizedString(@"R1015", @"用户服务协议") options:NSCaseInsensitiveSearch];
         [mStr
          addAttribute:(NSString *)kCTForegroundColorAttributeName
          value:(id)[[Utils getUIColorWithHexString:SysColorBlue] CGColor]
          range:range];
         return mStr;
     }];
    //
    UIButton *aboutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    aboutBtn.frame = aboutLabel.frame;
    [aboutBtn addTarget:self
                 action:@selector(aboutBtnAction:)
       forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:aboutBtn];
    //
    thirdView = [[UIView alloc]initWithFrame:CGRectMake(0, aboutLabel.bottom + 28.0, ScreenWidth, 0)];
    thirdView.userInteractionEnabled = YES;
    [self.view addSubview:thirdView];
    UILabel *thirdLabel = [[UILabel alloc]
                           initWithFrame:CGRectMake(0, 0,
                                                    ScreenWidth, 18)];
    thirdLabel.backgroundColor = [UIColor clearColor];
    thirdLabel.font = font_(14.0);
    thirdLabel.textAlignment = NSTextAlignmentCenter;
    thirdLabel.text = NSLocalizedString(@"R1017", @"或");
    thirdLabel.textColor = [Utils getUIColorWithHexString:@"878787"];
    [thirdView addSubview:thirdLabel];
    CGSize thirdLabelSize = [Utils getUIFontSizeFitW:thirdLabel];
    thirdLabel.frame = CGRectMake((self.view.width - thirdLabelSize.width-20.0) / 2.0,
                                  thirdLabel.top, thirdLabelSize.width+20.0, 18);
    //
    UIView *leftLineView = [[UIView alloc]
                            initWithFrame:CGRectMake(20, thirdLabel.bottom - 9.0,
                                                     (self.view.width - thirdLabelSize.width-20.0 - 40.0) /
                                                     2.0,
                                                     1.0f)];
    leftLineView.backgroundColor = [Utils getUIColorWithHexString:@"c6c6c6"];
    [thirdView addSubview:leftLineView];
    //
    UIView *rightLineView = [[UIView alloc]
                             initWithFrame:CGRectMake(thirdLabel.right, leftLineView.top,
                                                      leftLineView.width, 1.0f)];
    rightLineView.backgroundColor = [Utils getUIColorWithHexString:@"c6c6c6"];
    [thirdView addSubview:rightLineView];
    //
    UIButton *weixinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    weixinBtn.frame = CGRectMake((self.view.width - 50.0 * 3.0 - 50.0) / 3.0, thirdLabel.bottom + 15.0, 50, 50);
    weixinBtn.tag = 1000;
    [weixinBtn addTarget:self
                  action:@selector(weixinBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [weixinBtn setBackgroundImage:[UIImage imageNamed:@"icon_wechat.png"] forState:UIControlStateNormal];
    if ([ShareSDK isClientInstalled:SSDKPlatformTypeWechat]) {
        weixinBtn.hidden = NO;
    }else{
        weixinBtn.hidden = YES;
    }
    [thirdView addSubview:weixinBtn];
    //
    UIButton *weiboBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    weiboBtn.frame = CGRectMake(weixinBtn.right + 50.0, weixinBtn.top, weixinBtn.width,weixinBtn.height);
    weiboBtn.tag = 1001;
    [weiboBtn addTarget:self
                 action:@selector(weiboBtnAction:)
       forControlEvents:UIControlEventTouchUpInside];
    [weiboBtn setBackgroundImage:[UIImage imageNamed:@"icon_weibo.png"] forState:UIControlStateNormal];
    [thirdView addSubview:weiboBtn];
    //
    UIButton *qqBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    qqBtn.frame = CGRectMake(weiboBtn.right + 50.0, weixinBtn.top, weixinBtn.width,weixinBtn.height);
    qqBtn.tag = 1002;
    [qqBtn addTarget:self
              action:@selector(qqBtnAction:)
    forControlEvents:UIControlEventTouchUpInside];
    [qqBtn setBackgroundImage:[UIImage imageNamed:@"icon_qq.png"] forState:UIControlStateNormal];
    if ([LocalService getRBIsIncheck] == YES) {
        qqBtn.hidden = YES;
    }else{
        qqBtn.hidden = NO;
    }
    
    [thirdView addSubview:qqBtn];
    //
    thirdView.height = weiboBtn.bottom;
//    thirdView.hidden = YES;
}
//忘记密码
- (void)forgetVC{
    RBForgetSecretViewController * forgetVC = [[RBForgetSecretViewController alloc]init];
    forgetVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:forgetVC animated:YES];
}
//切换登录方式
- (void)phoneLoginAction:(id)sender{
    UIButton * btn = (UIButton*)sender;
    UILabel * lineLabel = (UILabel*)[editView viewWithTag:1026];
    UILabel * linelabel1 = (UILabel*)[editView viewWithTag:2000];
    UIButton * visitBtn = [self.view viewWithTag:1919];
    if (btn.tag == 1024) {
        [phoneLogin setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        [emailLogin setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
        codeBtn.hidden = NO;
        lineLabel.hidden = NO;
        forgetBtn.hidden = YES;
        inviteTF.hidden = NO;
        linelabel1.hidden = NO;
        codeTF.secureTextEntry = NO;
        phoneTF.attributedPlaceholder = [[NSAttributedString alloc]
                                         initWithString:NSLocalizedString(@"R1003", @"手机号")
                                         attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
        codeTF.attributedPlaceholder = [[NSAttributedString alloc]
                                        initWithString:NSLocalizedString(@"R1004", @"验证码")
                                        attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
        
        phoneTF.text = @"";
        codeTF.text = @"";
        inviteTF.text = @"";
        [UIView animateWithDuration:0.5 animations:^{
            changeLabel.centerX = btn.centerX;
        }];
        codeTF.width = ScreenWidth-30.0-95.0;
        NSString * str = NSLocalizedString(@"R5172",@"游客 >");
        NSMutableAttributedString * mAttStr = [[NSMutableAttributedString alloc]initWithString:str];
        [ mAttStr addAttribute:NSFontAttributeName value:font_cu_(13) range:NSMakeRange(0, str.length)];
        [mAttStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorBlue] range:NSMakeRange(0, str.length)];
        [visitBtn setAttributedTitle:mAttStr forState:UIControlStateNormal];
        [visitBtn removeTarget:self action:@selector(registerVC) forControlEvents:UIControlEventTouchUpInside];
        [visitBtn addTarget:self
                       action:@selector(visitorBtnAction:)
             forControlEvents:UIControlEventTouchUpInside];
    }else if (btn.tag == 1025){
        [phoneLogin setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
        [emailLogin setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];

        codeBtn.hidden = YES;
        lineLabel.hidden = YES;
        forgetBtn.hidden = NO;
        inviteTF.hidden = YES;
        linelabel1.hidden = YES;
        codeTF.secureTextEntry = YES;
        phoneTF.attributedPlaceholder = [[NSAttributedString alloc]
                                         initWithString:NSLocalizedString(@"R5181", @"邮箱")
                                         attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
        codeTF.attributedPlaceholder = [[NSAttributedString alloc]
                                        initWithString:NSLocalizedString(@"R1105", @"密码")
                                        attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
        phoneTF.text = @"";
        codeTF.text = @"";
        inviteTF.text = @"";
        [UIView animateWithDuration:0.5 animations:^{
            changeLabel.centerX = btn.centerX;
        }];
        codeTF.width = phoneTF.width;
        NSString * str = @"还没有邮箱?立即注册";
        NSMutableAttributedString * mAttStr = [[NSMutableAttributedString alloc]initWithString:str];
        NSRange font_range = [str rangeOfString:@"立即注册"];
        [mAttStr addAttribute:NSFontAttributeName value:font_cu_cu_(14) range:font_range];
        [mAttStr addAttribute:NSFontAttributeName value:font_cu_(13) range:NSMakeRange(0, 6)];
        [mAttStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorBlue] range:NSMakeRange(0, mAttStr.length)];
        [visitBtn setAttributedTitle:mAttStr forState:UIControlStateNormal];
        [visitBtn removeTarget:self action:@selector(visitorBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [visitBtn addTarget:self action:@selector(registerVC) forControlEvents:UIControlEventTouchUpInside];
    }
}
#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - ThirdLogin Delegate
- (void)aboutBtnAction:(UIButton *)sender {
    [self endEdit];
    TOWebViewController *toview = [[TOWebViewController alloc] init];
    toview.url = [NSURL URLWithString:@"http://7xuw3n.com2.z0.glb.qiniucdn.com/Robin8_Service.html"];
    toview.title = NSLocalizedString(@"R1015", @"用户服务协议");
    toview.showPageTitles = NO;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)loginBtnAction:(UIButton *)sender {
    [self endEdit];
    if (forgetBtn.hidden == YES) {
        if (phoneTF.text.length==0) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1006", @"请输入手机号码")];
            return;
        }
        if (phoneTF.text.length!=11) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1007", @"手机号码格式错误")];
            return;
        }
        if (codeTF.text.length==0) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1008", @"请输入验证码")];
            return;
        }
        // RB-登录
        [self.hudView showOverlay];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        if (inviteTF.text.length <= 0) {
            [handler getRBUserLoginWithPhone:phoneTF.text andCode:codeTF.text andInvteCode:nil];
        }else if ([self isNum:inviteTF.text] == NO){
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R5221", @"邀请码只能为数字")];
        }else{
            [handler getRBUserLoginWithPhone:phoneTF.text andCode:codeTF.text andInvteCode:inviteTF.text];
        }
    }else{
        if (phoneTF.text.length==0) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1107", @"请输入邮箱账号")];
            return;
        }
        if (![RegularHelp validateUserEmail:phoneTF.text]) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1114", @"邮箱账号格式错误")];
            return;
        }
        if (codeTF.text.length==0) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1115", @"请输入密码R1115")];
            return;
        }
        //
        [self.hudView showOverlay];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        if (inviteTF.text.length <= 0) {
            [handler getRBUserLoginWithEmail:phoneTF.text andCode:codeTF.text];
        }else if ([self isNum:inviteTF.text] == NO){
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R5221", @"邀请码只能为数字")];
        }else{
            [handler getRBUserLoginWithEmail:phoneTF.text andCode:codeTF.text];
        }
    }
    
}
- (BOOL)isNum:(NSString *)checkedNumString {
    checkedNumString = [checkedNumString stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(checkedNumString.length > 0) {
        return NO;
    }
    return YES;
}
- (void)weiboBtnAction:(UIButton *)sender {
    [self endEdit];
    [self ThirdLoginWithTag:5000];
}

- (void)weixinBtnAction:(UIButton *)sender {
    [self endEdit];
    [self ThirdLoginWithTag:5001];
}

- (void)qqBtnAction:(UIButton *)sender {
    [self endEdit];
    [self ThirdLoginWithTag:5002];
}

- (void)ThirdLoginWithTag:(int)tag {
    SSDKPlatformType type;
    thirdView.userInteractionEnabled = NO;
    NSString*provider=@"";
    if (tag == 5000) {
        // 微博
        provider = @"weibo";
        type = SSDKPlatformTypeSinaWeibo;
    } else if (tag == 5001) {
        // 微信
        provider = @"wechat";
        type = SSDKPlatformTypeWechat;
    } else {
        // QQ
        provider = @"qq";
        type = SSDKPlatformTypeQQ;
    }
    //
    [ShareSDK cancelAuthorize:type];
    [ShareSDK getUserInfo:type
           onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error){
               NSLog(@"user:%@,credential：%@",user,user.credential);
               if (state == SSDKResponseStateSuccess) {
                   // RB-社交账号绑定
                   [self.hudView showOverlay];
                   Handler*handler=[Handler shareHandler];
                   handler.delegate=self;
                   userName = [NSString stringWithFormat:@"%@",user.nickname];
                   uid = user.uid;
                   token = user.credential.token;
                   url = user.url;
                   icon = user.icon;
                   aboutMe = user.aboutMe;
                   if (type == SSDKPlatformTypeWechat) {
                       editString = @"微信";
                       [self.hudView show];
                       unionid = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"unionid"]];
                       serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       [handler getRBUserLoginThirdWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:unionid andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                   }
                   if (type == SSDKPlatformTypeSinaWeibo) {
                       editString = @"微博";
                       serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       followers_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"followers_count"]];
                       created_at = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"created_at"]];
                       statuses_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"statuses_count"]];
                       verified = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"verified"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"avatar_large"]];
                       icon = user.icon;
                       refresh_token = nil;
                       NSString*str = [NSString stringWithFormat:@"%@",user.credential];
                       if([[str componentsSeparatedByString:@"\"refresh_token\" = \""] count]>1) {
                           str = [str componentsSeparatedByString:@"\"refresh_token\" = \""][1];
                           if([[str componentsSeparatedByString:@"\";"] count]>1) {
                               refresh_token = [str componentsSeparatedByString:@"\";"][0];
                           }
                       }
                       [self.hudView show];
                       [handler getRBUserLoginThirdWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:followers_count andStatuses_count:statuses_count andRegistered_at:created_at andVerified:verified andRefresh_token:refresh_token  andUnionid:nil andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                   }
                   if (type == SSDKPlatformTypeQQ) {
                       editString = @"QQ";
                       [self.hudView show];
                       serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       city = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"city"]];
                       is_yellow_vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"is_yellow_vip"]];
                       vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"vip"]];
                       province = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"province"]];
                       gender = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"gender"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"figureurl_qq_2"]];
                       icon = user.icon;
                       [handler getRBUserLoginThirdWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:nil andProvince:province andCity:city andGender:gender andIs_vip:vip andIs_yellow_vip:is_yellow_vip];
                   }
               }
               if (state == SSDKResponseStateFail) {
                   thirdView.userInteractionEnabled = YES;
                   if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                       [self.hudView showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                   } else {
                       [self.hudView showErrorWithStatus:NSLocalizedString(@"R1042", @"登录失败")];
                   }
               }
               if (state == SSDKResponseStateCancel) {
                   thirdView.userInteractionEnabled = YES;
                   [self.hudView dismiss];
               }
           }];
}
#pragma mark - UIButton Delegate
- (void)codeBtnAction:(UIButton *)sender {
    if (phoneTF.text.length==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1006", @"请输入手机号码")];
        return;
    }
    if (phoneTF.text.length!=11) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1007", @"手机号码格式错误")];
        return;
    }
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"R1009", @"收不到验证码?")]){
        RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
        actionSheet.delegate = self;
        actionSheet.tag = 1000;
        [actionSheet showWithArray:@[NSLocalizedString(@"R1010", @"获取语音验证码"),NSLocalizedString(@"R1011", @"取消")]];
        return;
    }
    // RB-获取短信验证码
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserLoginPhoneCodeWith:phoneTF.text];
    //
    [self codeBtnTime];
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if (actionSheet.tag == 1000){
        if (buttonIndex==0) {
            // RB-获取语音验证码
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBUserLoginPhoneVoiceCodeWith:phoneTF.text];
            //
            [self codeBtnTime];
        }
    }
}

- (void)timeLimit {
    if (codeTimerStr == 0) {
        [codeTimer invalidate];
        codeTimer = nil;
        [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        [codeBtn setTitle:NSLocalizedString(@"R1009", @"收不到验证码?") forState:UIControlStateNormal];
        codeBtn.enabled = YES;
    } else {
        codeTimerStr = codeTimerStr - 1;
        [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55] forState:UIControlStateNormal];
        [codeBtn setTitle:[NSString stringWithFormat:@"%@%ds",NSLocalizedString(@"R1012", @"重新获取"),codeTimerStr] forState:UIControlStateNormal];
        codeBtn.enabled = NO;
    }
}

-(void)codeBtnTime {
    codeTimerStr = 59;
    [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55] forState:UIControlStateNormal];
    [codeBtn setTitle:[NSString stringWithFormat:@"%@%ds",NSLocalizedString(@"R1012", @"重新获取"),codeTimerStr] forState:UIControlStateNormal];
    codeBtn.enabled = NO;
    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                               target  :self
                                               selector:@selector(timeLimit)
                                               userInfo:nil
                                               repeats :YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEdit];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    float keyboardTop = ScreenHeight-320;
    if (editView.bottom > keyboardTop) {
        [UIView animateWithDuration:0.2 animations:^{
            editView.top = keyboardTop-editView.height;
        } completion:^(BOOL finished) {
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.2 animations:^{
        editView.top = (ScreenHeight-50.0*5.0)/2.0;
    } completion:^(BOOL finished) {
    }];
}
#pragma mark RBNewAlertDelegate
- (void)RBNewAlertTapAction:(RBNewAlert *)alertView{
    
}
- (void)RBNewAlertCancelButtonAction:(RBNewAlert *)alertView{
    if (userEntity.mobile_number.length==0) {
        RBLoginPhoneViewController *toview = [[RBLoginPhoneViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
        return;
    }
    if ([JsonService isRBUserVisitor]) {
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:22];
        return;
    }
    BOOL isBind = NO;
    for (RBLoginSocialEntity * entity in userEntity.socialArray) {
        if ([entity.provider isEqualToString:@"weibo"]) {
            isBind = YES;
        }
    }
    //没有登录点击影响力
    if ([_isFirst isEqualToString:@"first"] && isBind == YES && [LocalService getRBLocalTestInfluence] != nil) {
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:33];
        return;
    }
    //没有绑定微博和微信跳到绑定
    if (isBind == NO) {
        RBInfluenceOrcaController * bindVC = [[RBInfluenceOrcaController alloc]init];
        bindVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:bindVC animated:YES];
        return;
    }else if([LocalService getRBLocalTestInfluence] == nil){
        //绑定了微博微信但是是第一次进入APP
        RBInfluenceOrcaController * orcaVC = [[RBInfluenceOrcaController alloc]init];
        orcaVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:orcaVC animated:YES];
        return;
    }else{
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:22];
    }
}
#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"phones/get_code"]) {
        [self.hudView showSuccessWithStatus:[jsonObject objectForKey:@"detail"]];
    }
    if ([sender isEqualToString:@"kols/identity_bind"]) {
        [self.hudView dismiss];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBKOLApplyWithProvider:editString andHomepage:nil andUsername:userName andPrice:@"1" andFollowers:@"10" andScreenshot:nil andUid:nil];
 
    }
    if ([sender isEqualToString:@"update_social"]) {
        if (SocialuserEntity.mobile_number.length==0) {
            RBLoginPhoneViewController *toview = [[RBLoginPhoneViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
            return;
        }
        if ([JsonService isRBUserVisitor]) {
            RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate getRongIMConnect];
            [appDelegate loadViewController:22];
            return;
        }
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:22];

    }
    //社交账号登录
    if ([sender isEqualToString:@"kols/oauth_login"]) {
//        [self.hudView dismiss];
        [GeTuiSdk setPushModeForOff:NO];
        [JsonService setRBUserEntityWith:jsonObject];
        SocialuserEntity = [JsonService getRBUserEntity:jsonObject];
        [LocalService setRBLocalDataUserLoginIdWith:SocialuserEntity.iid];
        [LocalService setRBLocalDataUserLoginNameWith:SocialuserEntity.name];
        [LocalService setRBLocalDataUserLoginPhoneWith:SocialuserEntity.mobile_number];
        [LocalService setRBLocalDataUserLoginAvatarWith:SocialuserEntity.avatar_url];
        [LocalService setRBLocalDataUserPrivateTokenWith:SocialuserEntity.issue_token];
        [LocalService setRBLocalDataUserLoginKolIdWith:SocialuserEntity.kol_uuid];
        // 离线数据
        [Utils getFileDeleteWithFileName:@"RBCampaign" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBKol" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBKolNew" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBUser" andFilePath:@"RB"];
        //
        if (SocialuserEntity.mobile_number.length==0) {
            RBLoginPhoneViewController *toview = [[RBLoginPhoneViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
            thirdView.userInteractionEnabled = YES;
            [self.hudView dismiss];
            return;
        }
        if ([JsonService isRBUserVisitor]) {
            RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate getRongIMConnect];
            [appDelegate loadViewController:22];
            [self.hudView dismiss];
            return;
        }
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [self.hudView dismiss];
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:22];
        }
    //手机号登录
    if ([sender isEqualToString:@"kols/sign_in"]) {
        [self.hudView dismiss];
        [GeTuiSdk setPushModeForOff:NO];
        [JsonService setRBUserEntityWith:jsonObject];
        userEntity = [JsonService getRBUserEntity:jsonObject];
        [LocalService setRBLocalDataUserLoginIdWith:userEntity.iid];
        [LocalService setRBLocalDataUserLoginNameWith:userEntity.name];
        [LocalService setRBLocalDataUserLoginPhoneWith:userEntity.mobile_number];
        [LocalService setRBLocalDataUserLoginAvatarWith:userEntity.avatar_url];
        [LocalService setRBLocalDataUserPrivateTokenWith:userEntity.issue_token];
        [LocalService setRBLocalDataUserLoginKolIdWith:userEntity.kol_uuid];
        [LocalService setRBLocalEmail:userEntity.email];
        [LocalService setRBIsNewMember:userEntity.is_new_member];
        // 离线数据
        [Utils getFileDeleteWithFileName:@"RBCampaign" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBKol" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBKolNew" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBUser" andFilePath:@"RB"];
        //
        NSString * alert = [JsonService checkJson:[jsonObject objectForKey:@"alert"]];
        if (alert.length > 0) {
            RBNewAlert * newAlert = [[RBNewAlert alloc]init];
            newAlert.delegate = self;
            NSMutableArray * array = [[NSMutableArray alloc]init];
            [array addObject:alert];
            [newAlert showWithArray:array andBigTitle:@"额外奖励"];
            return;
        }
        //
        if (userEntity.mobile_number.length==0) {
            RBLoginPhoneViewController *toview = [[RBLoginPhoneViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
            return;
        }
        if ([JsonService isRBUserVisitor]) {
            RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate getRongIMConnect];
            [appDelegate loadViewController:22];
            return;
        }
        BOOL isBind = NO;
        for (RBLoginSocialEntity * entity in userEntity.socialArray) {
            if ([entity.provider isEqualToString:@"weibo"]) {
                isBind = YES;
            }
        }
        //没有登录点击影响力
        if ([_isFirst isEqualToString:@"first"] && isBind == YES && [LocalService getRBLocalTestInfluence] != nil) {
            RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate getRongIMConnect];
            [appDelegate loadViewController:33];
            return;
        }
        //没有绑定微博和微信跳到绑定
        if (isBind == NO) {
            RBInfluenceOrcaController * bindVC = [[RBInfluenceOrcaController alloc]init];
            bindVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:bindVC animated:YES];
            return;
        }else if([LocalService getRBLocalTestInfluence] == nil){
            //绑定了微博微信但是是第一次进入APP
            RBInfluenceOrcaController * orcaVC = [[RBInfluenceOrcaController alloc]init];
            orcaVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:orcaVC animated:YES];
            return;
        }else{
            RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate getRongIMConnect];
            [appDelegate loadViewController:22];
        }
    }
    if ([sender isEqualToString:@"sessions"]) {
        [self.hudView dismiss];
        [GeTuiSdk setPushModeForOff:NO];
        [JsonService setRBUserEntityWith:jsonObject];
        RBUserEntity*userEntity = [JsonService getRBUserEntity:jsonObject];
        [LocalService setRBLocalDataUserLoginIdWith:userEntity.iid];
        [LocalService setRBLocalDataUserLoginNameWith:userEntity.name];
        [LocalService setRBLocalDataUserLoginPhoneWith:userEntity.mobile_number];
        [LocalService setRBLocalDataUserLoginAvatarWith:userEntity.avatar_url];
        [LocalService setRBLocalDataUserPrivateTokenWith:userEntity.issue_token];
        [LocalService setRBLocalDataUserLoginKolIdWith:userEntity.kol_uuid];
        [LocalService setRBLocalEmail:userEntity.email];
       
        // 离线数据
        [Utils getFileDeleteWithFileName:@"RBCampaign" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBKol" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBKolNew" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBUser" andFilePath:@"RB"];
        //
        BOOL isBind = NO;
        for (RBLoginSocialEntity * entity in userEntity.socialArray) {
            if ([entity.provider isEqualToString:@"weibo"]) {
                isBind = YES;
            }
        }
        //没有登录点击影响力
        if ([_isFirst isEqualToString:@"first"] && isBind == YES && [LocalService getRBLocalTestInfluence] != nil) {
            RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate getRongIMConnect];
            [appDelegate loadViewController:33];
            return;
        }
        //没有绑定微博和微信跳到绑定
        if (isBind == NO) {
            RBInfluenceOrcaController * bindVC = [[RBInfluenceOrcaController alloc]init];
            bindVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:bindVC animated:YES];
            return;
        }else if([LocalService getRBLocalTestInfluence] == nil){
            //绑定了微博微信但是是第一次进入APP
            RBInfluenceOrcaController * orcaVC = [[RBInfluenceOrcaController alloc]init];
            orcaVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:orcaVC animated:YES];
            return;
        }else{
            RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate getRongIMConnect];
            [appDelegate loadViewController:22];
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        thirdView.userInteractionEnabled = YES;
        if ([sender isEqualToString:@"kols/identity_bind"] || [sender isEqualToString:@"update_social"]) {
            if ([[jsonObject objectForKey:@"error"]isEqual:@1]) {
                [self.hudView dismiss];
                return;
            }
        }
        if ([sender isEqualToString:@"phones/get_code"]){
            [self.hudView showErrorWithStatus:@"验证码发送过于频繁，请稍后再试"];
            return;
        }
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    thirdView.userInteractionEnabled = YES;
    if ([sender isEqualToString:@"kols/identity_bind"]||[sender isEqualToString:@"update_social"]) {
        [self.hudView dismiss];
    }else if([sender isEqualToString:@"phones/get_code"]){
        [self.hudView showErrorWithStatus:@"验证码发送过于频繁，请稍后再试"];
    }
    else{
        [self.hudView showErrorWithStatus:error.localizedDescription];
    }
}

@end
