//
//  RBSetPassWordViewController.h
//  RB
//
//  Created by RB8 on 2017/7/13.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBSetPassWordViewController : RBBaseViewController<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>
@property(nonatomic,copy)NSString * email;
@property(nonatomic,copy)NSString * mark;
@property(nonatomic,copy)NSString * vtoken;
@end
