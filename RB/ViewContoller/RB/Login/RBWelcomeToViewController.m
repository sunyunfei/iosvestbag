//
//  RBWelcomeToViewController.m
//  RB
//
//  Created by RB8 on 2018/1/29.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBWelcomeToViewController.h"
#import "RBAppDelegate.h"
@interface RBWelcomeToViewController ()

@end

@implementation RBWelcomeToViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)GO:(UIButton *)sender {
    RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate getRongIMConnect];
    [appDelegate loadViewController:22];
    return;
}
@end
