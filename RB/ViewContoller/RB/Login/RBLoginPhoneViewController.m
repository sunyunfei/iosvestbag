//
//  RBLoginPhoneViewController.m
//  RB
//
//  Created by AngusNi on 16/2/6.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBLoginPhoneViewController.h"
#import "RBAppDelegate.h"
#import "RBKolApplyInfoViewController.h"
@interface RBLoginPhoneViewController (){
    UILabel *tLabel;
    InsetsTextField*phoneTF;
    InsetsTextField*codeTF;
    UIButton *codeBtn;
    NSTimer *codeTimer;
    int codeTimerStr;
    UIView *editView;
}
@end

@implementation RBLoginPhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R1001", @"绑定手机号码");
    //
    [self loadEditView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    //
    [TalkingData trackPageBegin:@"login-phone"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [codeTimer invalidate];
    codeTimer = nil;
    //
    [TalkingData trackPageEnd:@"login-phone"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationShowLoginView {
}

- (void)RBNotificationShowMessageView {
}

#pragma mark - loadEditView Delegate
- (void)loadEditView {
    tLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, ((ScreenHeight-64.0-100.0)/2.0-60.0)/2.0, ScreenWidth, 60)];
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
    tLabel.font = font_15;
    tLabel.numberOfLines = 0;
    tLabel.text = NSLocalizedString(@"R1002", @"为保障您得到最优质的服务体验\n请输入您的手机号");
    tLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:tLabel];
    [Utils getUILabel:tLabel withLineSpacing:5.0];
    //
    editView = [[UIView alloc]initWithFrame:CGRectMake(0, (ScreenHeight-64.0-100.0)/2.0, ScreenWidth, 0)];
    editView.userInteractionEnabled = YES;
    [self.view addSubview:editView];
    //
    phoneTF = [[InsetsTextField alloc]
               initWithFrame:CGRectMake(15, 0, ScreenWidth-30.0, 50.0)];
    phoneTF.font = font_15;
    phoneTF.textColor = [Utils getUIColorWithHexString:SysColorGray];
    phoneTF.keyboardType = UIKeyboardTypeNumberPad;
    phoneTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    phoneTF.attributedPlaceholder = [[NSAttributedString alloc]
                                     initWithString:NSLocalizedString(@"R1003", @"手机号")
                                     attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorSubGray]}];
    phoneTF.returnKeyType = UIReturnKeyDone;
    phoneTF.delegate = self;
    [editView addSubview:phoneTF];
    //
    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]
                                      initWithFrame:CGRectMake(0, ScreenHeight-260,
                                                               ScreenWidth, 260)];
    textFiledKB.delegateT = self;
    [phoneTF setInputAccessoryView:textFiledKB];
    //
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(phoneTF.left, phoneTF.bottom-0.5f, phoneTF.width, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editView addSubview:lineLabel];
    //
    codeTF = [[InsetsTextField alloc]
              initWithFrame:CGRectMake(15, phoneTF.bottom+10.0, ScreenWidth-30.0-95.0, 50.0)];
    codeTF.font = phoneTF.font;
    codeTF.delegate = self;
    codeTF.textColor = phoneTF.textColor;
    codeTF.keyboardType = UIKeyboardTypeNumberPad;
    codeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    codeTF.attributedPlaceholder = [[NSAttributedString alloc]
                                    initWithString:NSLocalizedString(@"R1004", @"验证码")
                                    attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorSubGray]}];
    codeTF.returnKeyType = UIReturnKeyDone;
    [editView addSubview:codeTF];
    //
    [codeTF setInputAccessoryView:textFiledKB];
    //
    UILabel *lineLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(phoneTF.left, codeTF.bottom-0.5f, phoneTF.width, 0.5f)];
    lineLabel1.backgroundColor = lineLabel.backgroundColor;
    [editView addSubview:lineLabel1];
    //
    UILabel *lineLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-15.0-100.0, lineLabel.bottom+20.0, 0.5f, 30)];
    lineLabel2.backgroundColor = lineLabel.backgroundColor;
    [editView addSubview:lineLabel2];
    //
    codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    [codeBtn setTitle:NSLocalizedString(@"R1005", @"获取验证码") forState:UIControlStateNormal];
    codeBtn.titleLabel.font = phoneTF.font;
    codeBtn.frame = CGRectMake(ScreenWidth-15.0-100.0, codeTF.top, 100.0, codeTF.height);
    [codeBtn addTarget:self
                action:@selector(codeBtnAction:)
      forControlEvents:UIControlEventTouchUpInside];
    codeBtn.backgroundColor = [UIColor clearColor];
    [editView addSubview:codeBtn];
    //
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    [loginBtn setTitle:NSLocalizedString(@"R1000", @"完成") forState:UIControlStateNormal];
    loginBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
    loginBtn.layer.borderWidth = 1.0f;
    loginBtn.titleLabel.font = font_15;
    loginBtn.frame = CGRectMake((ScreenWidth-200.0)/2.0, codeBtn.bottom+50.0, 200.0, 44.0);
    [loginBtn addTarget:self
                 action:@selector(loginBtnAction:)
       forControlEvents:UIControlEventTouchUpInside];
    [editView addSubview:loginBtn];
    //
    editView.height = loginBtn.bottom;
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
//    RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
//    [appDelegate getRongIMConnect];
//    [appDelegate loadViewController:1];
}

- (void)loginBtnAction:(UIButton *)sender {
    [self endEdit];
    if (phoneTF.text.length==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1006", @"请输入手机号码")];
        return;
    }
    if (phoneTF.text.length!=11) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1007", @"手机号码格式错误")];
        return;
    }
    if (codeTF.text.length==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1008", @"请输入验证码")];
        return;
    }
    [self.hudView show];
    // RB-第三方登录绑定手机
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoThirdAccountBindingWithPhone:phoneTF.text andCode:codeTF.text];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - UIButton Delegate
- (void)codeBtnAction:(UIButton *)sender {
    if (phoneTF.text.length==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1006", @"请输入手机号码")];
        return;
    }
    if (phoneTF.text.length!=11) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1007", @"手机号码格式错误")];
        return;
    }
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"R1009", @"收不到验证码?")]){
        RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
        actionSheet.delegate = self;
        [actionSheet showWithArray:@[NSLocalizedString(@"R1010", @"获取语音验证码"),NSLocalizedString(@"R1011", @"取消")]];
        return;
    }
    // RB-获取短信验证码
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserLoginPhoneCodeWith:phoneTF.text];
    //
    [self codeBtnTime];
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if (buttonIndex==0) {
        // RB-获取语音验证码
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBUserLoginPhoneVoiceCodeWith:phoneTF.text];
        //
        [self codeBtnTime];
    }
}

#pragma mark - TimeLimit Delegate
- (void)timeLimit {
    if (codeTimerStr == 0) {
        [codeTimer invalidate];
        codeTimer = nil;
        [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        [codeBtn setTitle:NSLocalizedString(@"R1009", @"收不到验证码?") forState:UIControlStateNormal];
        codeBtn.enabled = YES;
    } else {
        codeTimerStr = codeTimerStr - 1;
        [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        [codeBtn setTitle:[NSString stringWithFormat:@"%@%ds",NSLocalizedString(@"R1012", @"重新获取"),codeTimerStr] forState:UIControlStateNormal];
        codeBtn.enabled = NO;
    }
}

-(void)codeBtnTime {
    codeTimerStr = 59;
    [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
    [codeBtn setTitle:[NSString stringWithFormat:@"%@%ds",NSLocalizedString(@"R1012", @"重新获取"),codeTimerStr] forState:UIControlStateNormal];
    codeBtn.enabled = NO;
    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                               target  :self
                                               selector:@selector(timeLimit)
                                               userInfo:nil
                                               repeats :YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEdit];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    float keyboardTop = ScreenHeight-320;
    if (editView.bottom > keyboardTop) {
        [UIView animateWithDuration:0.2 animations:^{
            tLabel.hidden = YES;
            editView.top = keyboardTop-editView.height;
        } completion:^(BOOL finished) {
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.2 animations:^{
        editView.top = (ScreenHeight-64.0-100.0)/2.0;
        tLabel.hidden = NO;
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"phones/get_code"]) {
        [self.hudView showSuccessWithStatus:[jsonObject objectForKey:@"detail"]];
    }
    
    if ([sender isEqualToString:@"bind_mobile"]) {
        [self.hudView dismiss];
        [GeTuiSdk setPushModeForOff:NO];
        [JsonService setRBUserEntityWith:jsonObject];
        RBUserEntity*userEntity = [JsonService getRBUserEntity:jsonObject];
        [LocalService setRBLocalDataUserLoginIdWith:userEntity.iid];
        [LocalService setRBLocalDataUserLoginNameWith:userEntity.name];
        [LocalService setRBLocalDataUserLoginPhoneWith:userEntity.mobile_number];
        [LocalService setRBLocalDataUserLoginAvatarWith:userEntity.avatar_url];
        [LocalService setRBLocalDataUserPrivateTokenWith:userEntity.issue_token];
        [LocalService setRBLocalDataUserLoginKolIdWith:userEntity.kol_uuid];
        // 离线数据
        [Utils getFileDeleteWithFileName:@"RBCampaign" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBKol" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBKolNew" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBUser" andFilePath:@"RB"];
        //
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:22];
        //跳转到申请KOL界面
//        RBKolApplyInfoViewController * KolApply = [[RBKolApplyInfoViewController alloc]init];
//        KolApply.hidesBottomBarWhenPushed = YES;
//        KolApply.path = @"first";
//        [self.navigationController pushViewController:KolApply animated:YES];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
