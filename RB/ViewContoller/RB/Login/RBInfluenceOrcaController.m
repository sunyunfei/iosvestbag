//
//  RBInfluenceOrcaController.m
//  RB
//
//  Created by RB8 on 2017/8/7.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBInfluenceOrcaController.h"
#import "RBAppDelegate.h"
#import "RBInfluenceLoadingViewController.h"
@interface RBInfluenceOrcaController ()
{
    NSString *userName;
    NSString *editString;
}
@end

@implementation RBInfluenceOrcaController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView * BgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    BgImageView.image = [UIImage imageNamed:@"welcome.jpg"];
    [self.view addSubview:BgImageView];
    UIView * bgView = [[UIImageView alloc]initWithFrame:BgImageView.bounds];
    bgView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.68];
    [self.view addSubview:bgView];
    [self loadEditView];
    [LocalService setRBLocalTestInfluence];
    [LocalService setRBFirstInfluenceenter:nil];
}
-(void)loadEditView{
    //
    UIButton * jumpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [jumpBtn setTitle:@"跳过" forState:UIControlStateNormal];
    [jumpBtn setTitleColor:[Utils getUIColorWithHexString:@"cccccc"] forState:UIControlStateNormal];
    jumpBtn.titleLabel.font = font_13;
    jumpBtn.frame = CGRectMake(ScreenWidth - 100, 40, 85, 20);
    jumpBtn.tag = 100;
    jumpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [jumpBtn addTarget:self action:@selector(ClickBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:jumpBtn];
    //
    UILabel * welLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, (ScreenHeight-30)/2 - 50, ScreenWidth, 30) text:@"WELCOME" font:font_cu_cu_30 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:ccColorf3f3f3] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [Utils getUILabel:welLabel withSpacing:6.0];
    [self.view addSubview:welLabel];
    //
    UIButton * campainBtn = [[UIButton alloc]initWithFrame:CGRectMake(20, ScreenHeight-105-49, ScreenWidth-40, 49) title:@"去做活动" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysBackColorBlue] image:nil hlImage:nil];
    [campainBtn addTarget:self action:@selector(ClickBtn:) forControlEvents:UIControlEventTouchUpInside];
    campainBtn.titleLabel.font = font_17;
    campainBtn.tag = 101;
    [self.view addSubview:campainBtn];
    //
    UIButton * testBtn = [[UIButton alloc]initWithFrame:CGRectMake(20, campainBtn.top-30-49, ScreenWidth-40, 49) title:@"测试我的影响力" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysBackColorBlue] image:nil hlImage:nil];
    [testBtn addTarget:self action:@selector(ClickBtn:) forControlEvents:UIControlEventTouchUpInside];
    testBtn.titleLabel.font = font_17;
    testBtn.tag = 102;
    [self.view addSubview:testBtn];
}
-(void)ClickBtn:(id)sender{
    UIButton * btn = (UIButton*)sender;
    if (btn.tag == 100) {
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:22];
        return;
    }else if (btn.tag == 101){
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:22];
        return;
    }else if (btn.tag == 102){
//        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
//        [appDelegate getRongIMConnect];
//        [appDelegate loadViewController:33];
        if ([LocalService getRBFirstInfluenceenter] == nil || self.isBind == NO) {
//            RBInfluenceLoadingViewController *hmpositionVC = [[RBInfluenceLoadingViewController alloc] init];
//            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:hmpositionVC];
//            hmpositionVC.isBind = self.isBind;
//            [self presentViewController:nav animated:YES completion:nil];
//            return;
            RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate loadViewController:33];
        }else{
            RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate getRongIMConnect];
            [appDelegate loadViewController:33];
        }
    }
}
-(void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    if ([sender isEqualToString:@"kols/identity_bind"]) {
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:33];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
