//
//  RBLoginViewController.h
//  RB
//
//  Created by AngusNi on 15/7/11.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBLoginPhoneViewController.h"
@interface RBLoginViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,RBActionSheetDelegate,UIAlertViewDelegate,RBNewAlertViewDelegate>
@property (nonatomic, assign) BOOL isPush;
@property (nonatomic, assign) NSString * isFirst;
@end
