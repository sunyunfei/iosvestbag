//
//  RBRegisteremailViewController.m
//  RB
//
//  Created by RB8 on 2018/1/26.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBRegisteremailViewController.h"
#import "RegularHelp.h"
#import "RBSetPassWordViewController.h"
@interface RBRegisteremailViewController ()
{
    InsetsTextField*emailTF;
    InsetsTextField*codeTF;
    UIButton *codeBtn;
    NSTimer *codeTimer;
    int codeTimerStr;
    UIView*editView;
}
@end

@implementation RBRegisteremailViewController
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEdit];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navTitle = NSLocalizedString(@"R1106", @"注册");
    self.navLineLabel.hidden = YES;
    self.delegate = self;
    self.bgIV.hidden = NO;
    [self loadEditView];
}
- (void)loadEditView{
    editView = [[UIView alloc]initWithFrame:CGRectMake(0, (ScreenHeight-50.0*2.0 - 144.0)/2.0,ScreenWidth, 0)];
    editView.userInteractionEnabled = YES;
    [self.view addSubview:editView];
    //
    emailTF = [[InsetsTextField alloc]
               initWithFrame:CGRectMake(15, 0, ScreenWidth-30.0, 50.0)];
    emailTF.font = font_(16.0);
    emailTF.backgroundColor = [UIColor clearColor];
    emailTF.keyboardType = UIKeyboardTypeASCIICapable;
    emailTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    emailTF.attributedPlaceholder = [[NSAttributedString alloc]
                                     initWithString:NSLocalizedString(@"R5181", @"邮箱")
                                     attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
    emailTF.returnKeyType = UIReturnKeyDone;
    emailTF.delegate = self;
    [editView addSubview:emailTF];
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(emailTF.left,emailTF.bottom-0.5f, emailTF.width, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    [editView addSubview:lineLabel];
    //
    codeTF = [[InsetsTextField alloc]
              initWithFrame:CGRectMake(15, emailTF.bottom+10.0,ScreenWidth-30.0-95.0, 50.0)];
    codeTF.font = font_(16.0);
    codeTF.delegate = self;
    codeTF.backgroundColor = [UIColor clearColor];
    codeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    codeTF.attributedPlaceholder = [[NSAttributedString alloc]
                                    initWithString:NSLocalizedString(@"R1004", @"验证码")
                                    attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
    codeTF.returnKeyType = UIReturnKeyDone;
    codeTF.keyboardType = UIKeyboardTypeASCIICapable;
    [editView addSubview:codeTF];
    UILabel *lineLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(emailTF.left,codeTF.bottom-0.5f, emailTF.width, 0.5f)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    [editView addSubview:lineLabel1];
    //
    UILabel *lineLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-15.0-100.0,lineLabel.bottom+20.0, 0.5f, 30)];
    lineLabel2.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    lineLabel2.tag = 1026;
    [editView addSubview:lineLabel2];
    codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    [codeBtn setTitle:NSLocalizedString(@"R1005", @"获取验证码") forState:UIControlStateNormal];
    codeBtn.titleLabel.font = font_(14);
    codeBtn.frame = CGRectMake(ScreenWidth-15.0-100.0, codeTF.top, 100.0, codeTF.height);
    [codeBtn addTarget:self
                action:@selector(codeBtnAction:)
      forControlEvents:UIControlEventTouchUpInside];
    codeBtn.backgroundColor = [UIColor clearColor];
    [editView addSubview:codeBtn];
    //
    editView.height = lineLabel1.bottom;
    //
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setTitleColor:[Utils getUIColorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [loginBtn setTitle:NSLocalizedString(@"R1109", @"下一步") forState:UIControlStateNormal];
    loginBtn.titleLabel.font=font_cu_cu_15;
    loginBtn.frame = CGRectMake(15.0, editView.bottom+100.0, ScreenWidth-30.0, 44.0);
    [loginBtn addTarget:self
                 action:@selector(nextAction:)
       forControlEvents:UIControlEventTouchUpInside];
    loginBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    [self.view addSubview:loginBtn];
    //
    
}
- (void)nextAction:(id)sender{
    if (emailTF.text.length==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1107", @"请输入邮箱账号")];
        return;
    }
    if (![RegularHelp validateUserEmail:emailTF.text]) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1108", @"邮箱格式错误")];
        return;
    }
    if (codeTF.text.length==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1008", @"请输入验证码")];
        return;
    }
    // RB-验证码验证
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler validationRBEmailCode:emailTF.text AndCode:codeTF.text];
}
#pragma mark - UIButton Delegate
- (void)codeBtnAction:(UIButton *)sender {
    if (emailTF.text.length==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1107", @"请输入邮箱账号")];
        return;
    }
    if (![RegularHelp validateUserEmail:emailTF.text]) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1108", @"邮箱格式错误")];
        return;
    }
    // RB-获取邮箱验证码
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserLoginEmailCodeWith:emailTF.text Andtype:nil];
    //
    
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}
#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}
#pragma mark BaseVCDDelegate
- (void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)codeBtnTime {
    codeTimerStr = 59;
    [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55] forState:UIControlStateNormal];
    [codeBtn setTitle:[NSString stringWithFormat:@"%@%ds",NSLocalizedString(@"R1012", @"重新获取"),codeTimerStr] forState:UIControlStateNormal];
    codeBtn.enabled = NO;
    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                               target  :self
                                               selector:@selector(timeLimit)
                                               userInfo:nil
                                               repeats :YES];
}
- (void)timeLimit {
    if (codeTimerStr == 0) {
        [codeTimer invalidate];
        codeTimer = nil;
        [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        [codeBtn setTitle:NSLocalizedString(@"R1012", @"重新获取") forState:UIControlStateNormal];
        codeBtn.enabled = YES;
    } else {
        codeTimerStr = codeTimerStr - 1;
        [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55] forState:UIControlStateNormal];
        [codeBtn setTitle:[NSString stringWithFormat:@"%@%ds",NSLocalizedString(@"R1012", @"重新获取"),codeTimerStr] forState:UIControlStateNormal];
        codeBtn.enabled = NO;
    }
}
#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEdit];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    float keyboardTop = ScreenHeight-320;
    if (editView.bottom > keyboardTop) {
        [UIView animateWithDuration:0.2 animations:^{
//            editView.top = keyboardTop-editView.height;
            editView.bottom = keyboardTop;
        } completion:^(BOOL finished) {
        }];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.2 animations:^{
        editView.top = (ScreenHeight-50.0*2.0 - 144.0)/2.0;
    } completion:^(BOOL finished) {
    
    }];
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"registers/valid_code"]) {
        [self codeBtnTime];
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"提示" message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"alert"]] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
    if ([sender isEqualToString:@"registers/valid_email"]) {
        [self.hudView showSuccessWithStatus:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"alert"]]];
        sleep(1);
        RBSetPassWordViewController * toview = [[RBSetPassWordViewController alloc]init];
        toview.hidesBottomBarWhenPushed = YES;
        toview.email = emailTF.text;
        toview.mark = @"register";
        toview.vtoken = [jsonObject objectForKey:@"vtoken"];
        [self.navigationController pushViewController:toview animated:YES];
    }
}
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}
- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
     [self.hudView showErrorWithStatus:error.localizedDescription];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
