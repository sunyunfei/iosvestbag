//
//  RBCampaignRecruitApplyViewController.m
//  RB-TEST
//
//  Created by AngusNi on 30/09/2016.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBCampaignRecruitApplyViewController.h"

@interface RBCampaignRecruitApplyViewController () {
    UIScrollView*scrollviewn;
    UIButton*uploadBtn;
    UIImage*uploadImg;
    UIView*editView;
    UIView*picView;
    PlaceholderTextView*editTV;
    
}
@end

@implementation RBCampaignRecruitApplyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R2400",@"招募活动报名");
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height-NavHeight-45.0)];
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.userInteractionEnabled = YES;
    scrollviewn.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self.view addSubview:scrollviewn];
    //
    [self loadContentView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageBegin:@"campaign-recruit-apply"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"campaign-recruit-apply"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton*)sender {
    [self.hudView show];
    if(self.activityEntity.campaign.is_applying_note_required.intValue==1) {
        if(editTV.text.length==0) {
            [self.hudView showErrorWithStatus:self.activityEntity.campaign.applying_note_description];
            return;
        }
    }
    if(self.activityEntity.campaign.is_applying_picture_required.intValue==1) {
        if(uploadImg==nil) {
            [self.hudView showErrorWithStatus:self.activityEntity.campaign.applying_picture_description];
            return;
        }
    }
    // RB-报名活动
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityApplyWithId:self.activityEntity.campaign.iid andNote:editTV.text andPicture:uploadImg];
}

- (void)uploadBtnAction:(UIButton*)sender {
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    actionSheet.tag = 1000;
    [actionSheet showWithArray:@[NSLocalizedString(@"R2051", @"打开相册"),NSLocalizedString(@"R1011",@"取消")]];
}

#pragma mark - RBPictureUpload Delegate
- (void)RBPictureUpload:(RBPictureUpload *)pictureUpload selectImage:(UIImage *)img {
    NSLog(@"%f,%f-%f,%f",img.size.width,img.size.height,uploadBtn.width,uploadBtn.height);
    img = [Utils getUIImageScalingFromSourceImage:img targetSize:uploadBtn.size];
    [uploadBtn setBackgroundImage:img forState:UIControlStateNormal];
    uploadImg = img;
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if (actionSheet.tag == 1000) {
        if (buttonIndex == 0) {
            RBPictureUpload*picUpload = [RBPictureUpload sharedRBPictureUpload];
            picUpload.scaleFrame = CGRectMake((ScreenWidth-uploadBtn.width)/2.0, (ScreenHeight-uploadBtn.height)/2.0, uploadBtn.width, uploadBtn.height);
            picUpload.fromVC = self.navigationController;
            picUpload.delegate = self;
            [picUpload RBPictureUploadClickedButtonAtIndex:1];
            return;
        }
    }
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadContentView Delegate
- (void)loadContentView {
    if(self.activityEntity.campaign.is_applying_note_required.intValue==1) {
        editView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 130.0)];
        editView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        [scrollviewn addSubview:editView];
        //
        UILabel*editLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth-2*CellLeft, 30.0)];
        editLabel.font = font_13;
        editLabel.text = self.activityEntity.campaign.applying_note_description;
        editLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        [editView addSubview:editLabel];
        //
        UILabel*editLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, editLabel.bottom-0.5, ScreenWidth, 0.5f)];
        editLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [editView addSubview:editLineLabel];
        //
        editTV = [[PlaceholderTextView alloc]initWithFrame:CGRectMake(0, editLabel.bottom, ScreenWidth, 100)];
        editTV.font = font_15;
        editTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        editTV.keyboardType = UIKeyboardTypeDefault;
        [editView addSubview:editTV];
        //
        TextViewKeyBoard*textViewKB = [[TextViewKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
        textViewKB.delegateP = self;
        [editTV setInputAccessoryView:textViewKB];
        //
        UILabel*editLineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, editTV.bottom-0.5, ScreenWidth, 0.5f)];
        editLineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [editView addSubview:editLineLabel1];
    } else {
        editView.frame = CGRectMake(0, 0, ScreenWidth, 0);
    }
    //
    if(self.activityEntity.campaign.is_applying_picture_required.intValue==1) {
        picView = [[UIView alloc]initWithFrame:CGRectMake(0, editView.bottom, ScreenWidth, 30.0+ScreenWidth*9.0/16.0)];
        picView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        [scrollviewn addSubview:picView];
        //
        UILabel*picLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth-2*CellLeft, 30.0)];
        picLabel.font = font_13;
        picLabel.text = self.activityEntity.campaign.applying_picture_description;
        picLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        [picView addSubview:picLabel];
        //
        uploadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        uploadBtn.frame = CGRectMake(0, picLabel.bottom, ScreenWidth, ScreenWidth*9.0/16.0);
        [uploadBtn setBackgroundImage:[UIImage imageNamed:@"pic_task_recurit_pic.png"] forState:UIControlStateNormal];
        [uploadBtn addTarget:self
                      action:@selector(uploadBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        [picView addSubview:uploadBtn];
    } else {
        picView.frame = CGRectMake(0, editView.bottom, ScreenWidth, 0);
    }
    //
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, picView.bottom+CellLeft)];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextViewKeyBoardFinish:(TextViewKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if (alertView.tag==10002) {
        if (buttonIndex==0) {
            [self RBNavLeftBtnAction];
        }
    }
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"apply"]) {
        [self.hudView dismiss];
        RBAlertView *alertView = [[RBAlertView alloc]init];
        alertView.tag = 10002;
        alertView.delegate = self;
        [alertView showWithArray:@[NSLocalizedString(@"R2093", @"活动报名成功,等待审核中"),NSLocalizedString(@"R1020", @"确定")]];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
