//
//  RBUploadImageViewController.m
//  RB
//
//  Created by RB8 on 2018/1/10.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBUploadImageViewController.h"

@interface RBUploadImageViewController ()<UIScrollViewDelegate>
{
    UIButton * completeBtn;
    NSInteger imageTag;
    CGRect oldframe;
    NSMutableArray * imageArray;
    UILabel * markLabel;
    //返回按钮标识，NO表示隐藏返回按钮，YES表示出现返回按钮
    BOOL isPop;
}
@property(nonatomic,strong)NSMutableArray * uploadArray;
@end

@implementation RBUploadImageViewController
-(NSMutableArray*)uploadArray{
    if (!_uploadArray) {
        _uploadArray = [NSMutableArray array];
    }
    return _uploadArray;
}
- (void)RBNavLeftBtnAction{
    if ([self.delegateC respondsToSelector:@selector(RBUploadImageViewControllerDelegateAction)]) {
        [self.delegateC RBUploadImageViewControllerDelegateAction];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
    if ([self.mark isEqualToString:@"upload"]) {
        self.navTitle = NSLocalizedString(@"R2034", @"上传截图");
    }else if ([self.mark isEqualToString:@"example"] ||[self.mark isEqualToString:@"first"]){
        self.navTitle = NSLocalizedString(@"R2052", @"截图参考");
    }else if ([self.mark isEqualToString:@"watch"]){
        self.navTitle = NSLocalizedString(@"R2042", @"查看截图");
    }
    [self.view removeGestureRecognizer:self.edgePGR];
    //
    imageArray = [[NSMutableArray alloc]init];
    
    [self initUI];
}
- (void)initUI{
    UILabel * topLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 35.0) text:@"  注意:请按照截图示例顺序,上传对应图片" font:font_(15) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorWhite] backgroundColor:[Utils getUIColorWithHexString:SysColorBlue] numberOfLines:1];
    [self.view addSubview:topLabel];
    self.view.userInteractionEnabled = YES;
    //
    CGFloat imgWidth = (ScreenWidth - 4 * CellLeft)/3;
    for (NSInteger i = 0; i < _imgArr.count; i ++) {
        UIButton * imgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        imgBtn.frame = CGRectMake(CellLeft + (imgWidth + CellLeft) * i,topLabel.bottom + 30, imgWidth, imgWidth);
        if (self.nameArr.count < _imgArr.count) {
            [imgBtn setTitle:@"上传截图" forState:UIControlStateNormal];
        }else{
            [imgBtn setTitle:self.nameArr[i] forState:UIControlStateNormal];
        }
        [imgBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
        [imgBtn setImage:[UIImage imageNamed:@"RBuploadAdd"] forState:UIControlStateNormal];
        imgBtn.titleLabel.font = font_(11);
        imgBtn.titleLabel.numberOfLines = 0;
        imgBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        imgBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
        [imgBtn addTarget:self action:@selector(btnSelectImage:) forControlEvents:UIControlEventTouchUpInside];
        imgBtn.tag = 100+i;
        CAShapeLayer * border = [CAShapeLayer layer];
        border.strokeColor = [Utils getUIColorWithHexString:SysColorBlue].CGColor;
        border.fillColor = [UIColor clearColor].CGColor;
        border.path = [UIBezierPath bezierPathWithRect:imgBtn.bounds].CGPath;
        border.frame = imgBtn.bounds;
        border.lineWidth = 1.f;
        border.lineDashPattern = @[@4,@2];
        [imgBtn.layer addSublayer:border];
        [self.view addSubview:imgBtn];
        //
        UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(CellLeft + (imgWidth + CellLeft) * i,topLabel.bottom + 30, imgWidth, imgWidth)];
        imageView.tag = 1000 + i;
        imageView.userInteractionEnabled = YES;
        [self.view addSubview:imageView];
        //
        if ([self.mark isEqualToString:@"upload"]) {
            UIButton * deleteBtn = [[UIButton alloc]initWithFrame:CGRectMake(imgWidth - 15, 0, 15, 15) title:nil hlTitle:nil titleColor:nil hlTitleColor:nil backgroundColor:nil image:[UIImage imageNamed:@"RBDeleteImg"] hlImage:nil];
            [deleteBtn addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
            [deleteBtn setImage:[UIImage imageNamed:@"RBDeleteImg"] forState:UIControlStateNormal];
            imageView.hidden = YES;
            [imageView addSubview:deleteBtn];
        }
        if ([self.mark isEqualToString:@"example"]) {
            imageView.hidden = NO;
            imgBtn.hidden = YES;
            [imageView sd_setImageWithURL:[NSURL URLWithString:_imgArr[i]] placeholderImage:nil];
        }
       
        if ([self.mark isEqualToString:@"watch"]) {
            UIButton * deleteBtn = [[UIButton alloc]initWithFrame:CGRectMake(imgWidth - 15, 0, 15, 15) title:nil hlTitle:nil titleColor:nil hlTitleColor:nil backgroundColor:nil image:[UIImage imageNamed:@"RBDeleteImg"] hlImage:nil];
            [deleteBtn addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
            [deleteBtn setImage:[UIImage imageNamed:@"RBDeleteImg"] forState:UIControlStateNormal];
            imageView.hidden = NO;
            [imageView addSubview:deleteBtn];
            [imageView sd_setImageWithURL:[NSURL URLWithString:_imgArr[i]] placeholderImage:nil];
        }
        //第一次做活动时查看示例截图
        if ([self.mark isEqualToString:@"first"]) {
            imageView.hidden = NO;
            imgBtn.hidden = YES;
//            [imageView sd_setImageWithURL:[NSURL URLWithString:_imgArr[i]] placeholderImage:nil];
            imageView.image = [UIImage imageNamed:_imgArr[i]];
        }
        //
        UIImageView * bottomImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, imgWidth - 22, imgWidth, 22)];
        bottomImageView.image = [UIImage imageNamed:@"RBUploadBottom"];
        [imageView addSubview:bottomImageView];
        //
        
        UILabel * markLabel = [[UILabel alloc]initWithFrame:bottomImageView.frame text:@"" font:font_(12) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorWhite] backgroundColor:[UIColor clearColor] numberOfLines:1];
        if (self.nameArr.count >= _imgArr.count) {
            markLabel.text = self.nameArr[i];
        }else{
            markLabel.text = @"";
        }
        [imageView addSubview:markLabel];
        //
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(SelectImage:)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        [imageView addGestureRecognizer:tap];
        //
    }
    completeBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, ScreenHeight - 40, ScreenWidth, 40) title:@"上传截图" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorBlue] image:nil hlImage:nil];
    if ([self.mark isEqualToString:@"example"]) {
        [completeBtn setTitle:NSLocalizedString(@"R2034", @"上传截图") forState:UIControlStateNormal];
    }else if ([self.mark isEqualToString:@"first"]){
        completeBtn.hidden = YES;
    }else{
         [completeBtn setTitle:NSLocalizedString(@"R1000", @"完成") forState:UIControlStateNormal];
    }
    completeBtn.titleLabel.font = font_(18);
    [completeBtn addTarget:self action:@selector(CompleteAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:completeBtn];
}
- (void)deleteAction:(id)sender{
    UIButton * btn = (UIButton*)sender;
    UIImageView * imageView = (UIImageView*)btn.superview;
    imageView.hidden = YES;
    //删除图片以后让背景按钮显示出来
    UIButton * backBtn = [self.view viewWithTag:imageView.tag - 900];
    backBtn.hidden = NO;
}
//点击按钮选择图片
- (void)btnSelectImage:(id)sender{
    UIButton * btn = (UIButton*)sender;
    imageTag = btn.tag + 900;
    ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
    if (author == ALAuthorizationStatusDenied) {
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.alertBody = @"照片不可用,请设置打开";
        [[UIApplication sharedApplication]
         presentLocalNotificationNow:notification];
        return;
    }
    RBPhotoAsstesGroupViewController*toview = [[RBPhotoAsstesGroupViewController alloc]init];
    toview.selectNumber = 1;
    toview.delegateC = self;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}
//点击图片放大图片
- (void)SelectImage:(UITapGestureRecognizer*)tap{
    UIImageView * imageView1 = [self.view viewWithTag:1000];
    UIImageView * imageView2 = [self.view viewWithTag:1001];
    UIImageView * imageView3 = [self.view viewWithTag:1002];
    [imageArray removeAllObjects];
    if (imageView1.hidden == NO && imageView1.image != nil) {
        [imageArray addObject:imageView1.image];
    }
    if (imageView2.hidden == NO && imageView2.image != nil) {
        [imageArray addObject:imageView2.image];
    }
    if (imageView3.hidden == NO && imageView3.image != nil) {
        [imageArray addObject:imageView3.image];
    }
    if (imageArray.count == self.imgArr.count) {
        UIImageView * imageView = (UIImageView*)tap.view;
        [self scanBigImageWithImageView:imageView andTag:imageView.tag];
    }else{
        //放大单张图片
        UIImageView * imageView = (UIImageView*)tap.view;
        [self scanSingleImageView:imageView];
    }
    
}
//放大单张图片
- (void)scanSingleImageView:(UIImageView*)currentImageView{
    self.navLeftBtn.enabled = NO;
    self.navLeftLabel.hidden = YES;
    
    //当前视图
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    //背景
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,NavHeight, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - NavHeight)];
   
    //当前imageview的原始尺寸->将像素currentImageview.bounds由currentImageview.bounds所在视图转换到目标视图window中，返回在目标视图window中的像素值
    oldframe = [currentImageView convertRect:currentImageView.bounds toView:window];
    [backgroundView setBackgroundColor:[UIColor whiteColor]];
    //此时视图不会显示
    [backgroundView setAlpha:0];
    //将所展示的imageView重新绘制在Window中
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,ScreenWidth, ScreenHeight - NavHeight)];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [imageView setImage:currentImageView.image];
    [imageView setTag:0];
    [backgroundView addSubview:imageView];
    
    if (self.nameArr.count == self.imgArr.count) {
        self.navTitle = [self.nameArr objectAtIndex:(currentImageView.tag - 1000)];
    }
    //将原始视图添加到背景视图中
    [window addSubview:backgroundView];
    
    //添加点击事件同样是类方法 -> 作用是再次点击回到初始大小
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideImageView:)];
    [backgroundView addGestureRecognizer:tapGestureRecognizer];
    
    //动画放大所展示的ImageView
    
    [UIView animateWithDuration:0.4 animations:^{
        //        CGFloat y,width,height;
        //        y = ([UIScreen mainScreen].bounds.size.height - image.size.height * [UIScreen mainScreen].bounds.size.width / image.size.width) * 0.5;
        //        //宽度为屏幕宽度
        //        width = [UIScreen mainScreen].bounds.size.width;
        //高度 根据图片宽高比设置
        //        height = image.size.height * [UIScreen mainScreen].bounds.size.width / image.size.width;
        //        if (y>0) {
        //            [imageView setFrame:CGRectMake(0, y, width, height)];
        //        }else{
        //            [imageView setFrame:CGRectMake(0, 0, width, height)];
        //        }
        //重要！ 将视图显示出来
        //        [imageView setFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
        
        [backgroundView setAlpha:1];
    } completion:^(BOOL finished) {
        
    }];
}

//完成
- (void)CompleteAction:(id)sender{
    
    if ([self.mark isEqualToString:@"example"]) {
        //参考截图界面进入上传截图界面
        RBUploadImageViewController * toView = [[RBUploadImageViewController alloc]init];
        toView.mark = @"upload";
        toView.hidesBottomBarWhenPushed = YES;
        toView.imgArr = self.imgArr;
        toView.campainID = self.campainID;
        toView.nameArr = self.nameArr;
        [self.navigationController pushViewController:toView animated:YES];
    }else if([self.mark isEqualToString:@"upload"] || [self.mark isEqualToString:@"watch"]){
        //上传截图界面上传图片
        [self.uploadArray removeAllObjects];
        for (int i = 0; i < self.imgArr.count; i ++) {
            UIImageView * imageView = [self.view viewWithTag:1000 + i];
            if (imageView.hidden == NO) {
                [self.uploadArray addObject:imageView.image];
            }
        }
        if (self.uploadArray.count == self.imgArr.count) {
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBActivityRecuritWithId:self.campainID andScreenShoot:self.uploadArray andCampaignLogo:nil];
        }else{
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请选择所有图片" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }else if ([self.mark isEqualToString:@"watch"]){
        
    }
}
#pragma RBPhotoAsstesGroupViewControllerDelegate
- (void)RBPhotoAsstesGroupViewController:(RBPhotoAsstesGroupViewController *)vc selectedList:(NSMutableArray *)list {
    // 图片压缩处理
    ALAsset *asset = list[0];
    UIImage *originalImage = [Utils thumbnailForAsset:asset maxPixelSize:1024];
    UIImage *tempimg = [Utils
                        getUIImageScalingFromSourceImage:originalImage
                        targetSize:originalImage.size];
    UIImageView * imageView = [self.view viewWithTag:imageTag];
    imageView.hidden = NO;
    imageView.image = tempimg;
    UIButton * btn = [self.view viewWithTag:imageTag - 900];
    btn.hidden = YES;
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    [self.hudView dismiss];
    NSArray * vcArr = self.navigationController.viewControllers;
    for (UIViewController * vc in vcArr) {
        if ([vc isKindOfClass:[RBCampaignDetailViewController class]] || [vc isKindOfClass:[RBCampaignRecruitAutoViewController class]] || [vc isKindOfClass:[RBCampaignRecruitViewController class]]) {
            if ([vc isKindOfClass:[RBCampaignDetailViewController class]]) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCampainDetail" object:nil];
            }else if ([vc isKindOfClass:[RBCampaignRecruitAutoViewController class]]){
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCampainAuto" object:nil];
            }else if([vc isKindOfClass:[RBCampaignRecruitViewController class]]){
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCampainRecuit" object:nil];
            }
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}
- (void)handlerError:(NSError *)error Tag:(NSString *)sender{
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender{
    [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
}
//点击放大
-(void)scanBigImageWithImageView:(UIImageView *)currentImageview andTag:(NSInteger)tag{
    self.navLeftBtn.enabled = NO;
    self.navLeftLabel.hidden = YES;
  
    //当前视图
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    //背景
    UIScrollView *backgroundView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,NavHeight, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - NavHeight)];
    backgroundView.showsVerticalScrollIndicator = NO;
    backgroundView.showsHorizontalScrollIndicator = NO;
    backgroundView.bounces = NO;
    backgroundView.pagingEnabled = YES;
    backgroundView.delegate = self;
    //当前imageview的原始尺寸->将像素currentImageview.bounds由currentImageview.bounds所在视图转换到目标视图window中，返回在目标视图window中的像素值
    oldframe = [currentImageview convertRect:currentImageview.bounds toView:window];
    [backgroundView setBackgroundColor:[UIColor whiteColor]];
    //此时视图不会显示
    [backgroundView setAlpha:0];
    //将所展示的imageView重新绘制在Window中
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:oldframe];
//    [imageView setImage:image];
//    [imageView setTag:0];
//    [backgroundView addSubview:imageView];
    if (imageArray.count == self.imgArr.count) {
        for (NSInteger i = 0; i < imageArray.count; i ++) {
            UIImageView * img = [[UIImageView alloc]initWithFrame:CGRectMake(i * ScreenWidth, 0, ScreenWidth, ScreenHeight - NavHeight)];
            [img setImage:imageArray[i]];
            img.contentMode = UIViewContentModeScaleAspectFit;
            [backgroundView addSubview:img];
        }
        backgroundView.contentOffset = CGPointMake((tag - 1000)*ScreenWidth, 0);
    }
    markLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, ScreenHeight-50, ScreenWidth, 20) text:@"" font:font_(13) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:SysColorGray] backgroundColor:[UIColor clearColor] numberOfLines:1];
    markLabel.text = [NSString stringWithFormat:@"%ld/%ld",tag - 999,imageArray.count];
    if (self.nameArr.count == self.imgArr.count) {
        self.navTitle = [self.nameArr objectAtIndex:(tag - 1000)];
    }
    //将原始视图添加到背景视图中
    [window addSubview:backgroundView];
    [window addSubview:markLabel];

    
    //添加点击事件同样是类方法 -> 作用是再次点击回到初始大小
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideImageView:)];
    [backgroundView addGestureRecognizer:tapGestureRecognizer];
    
    //动画放大所展示的ImageView
    
    [UIView animateWithDuration:0.4 animations:^{
//        CGFloat y,width,height;
//        y = ([UIScreen mainScreen].bounds.size.height - image.size.height * [UIScreen mainScreen].bounds.size.width / image.size.width) * 0.5;
//        //宽度为屏幕宽度
//        width = [UIScreen mainScreen].bounds.size.width;
        //高度 根据图片宽高比设置
//        height = image.size.height * [UIScreen mainScreen].bounds.size.width / image.size.width;
//        if (y>0) {
//            [imageView setFrame:CGRectMake(0, y, width, height)];
//        }else{
//            [imageView setFrame:CGRectMake(0, 0, width, height)];
//        }
        //重要！ 将视图显示出来
//        [imageView setFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
        
        backgroundView.contentSize = CGSizeMake(imageArray.count * ScreenWidth, ScreenHeight - NavHeight);
        [backgroundView setAlpha:1];
    } completion:^(BOOL finished) {
        
    }];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger a = (int)(scrollView.contentOffset.x / ScreenWidth) + 1;
    NSString * str = [NSString stringWithFormat:@"%ld/%ld",(long)a,(long)self.imgArr.count];
    markLabel.text = str;
    if (self.nameArr.count == self.imgArr.count) {
        self.navTitle = [self.nameArr objectAtIndex:(a - 1)];
    }
}
-(void)hideImageView:(UITapGestureRecognizer *)tap{
    self.navLeftBtn.enabled = YES;
    self.navLeftLabel.hidden = NO;
    [markLabel removeFromSuperview];

    if ([self.mark isEqualToString:@"upload"]) {
        self.navTitle = NSLocalizedString(@"R2034", @"上传截图");
    }else if ([self.mark isEqualToString:@"example"]){
        self.navTitle = NSLocalizedString(@"R2052", @"截图参考");
    }else if ([self.mark isEqualToString:@"watch"]){
        self.navTitle = NSLocalizedString(@"R2042", @"查看截图");
    }
    
    UIView *backgroundView = tap.view;
    
    //原始imageview
  //  UIImageView *imageView = [tap.view viewWithTag:0];
    //恢复
//    [UIView animateWithDuration:0.4 animations:^{
 //       [imageView setFrame:oldframe];
        [backgroundView setAlpha:0];
//    } completion:^(BOOL finished) {
        //完成后操作->将背景视图删掉
        [backgroundView removeFromSuperview];
//    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
