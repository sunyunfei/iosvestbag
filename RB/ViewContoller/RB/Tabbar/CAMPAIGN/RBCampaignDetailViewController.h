//
//  RBCampaignDetailViewController.h
//  RB
//
//  Created by AngusNi on 16/1/28.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "TOWebViewController.h"
#import "RBPictureView.h"
#import "RBPhotoAsstesGroupViewController.h"
#import "RBCampaignDetailKolsViewController.h"

@interface RBCampaignDetailViewController : RBBaseViewController
<RBBaseVCDelegate,UIWebViewDelegate,UIScrollViewDelegate,RBActionSheetDelegate,RBAlertViewDelegate,WXApiDelegate,RBPhotoAsstesGroupViewControllerDelegate,UIAlertViewDelegate,RBNewAlertViewDelegate>
@property(nonatomic, strong) NSString*activityId;
@property(nonatomic, strong) NSString*campaignId;


@end
