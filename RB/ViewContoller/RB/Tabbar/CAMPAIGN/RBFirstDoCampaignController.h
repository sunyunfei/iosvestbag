//
//  RBFirstDoCampaignController.h
//  RB
//
//  Created by RB8 on 2018/4/28.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBPictureView.h"
#import "RBPhotoAsstesGroupViewController.h"
#import "RBCampaignDetailKolsViewController.h"
#import "Handler.h"
#import "RBUploadImageViewController.h"
 
@interface RBFirstDoCampaignController : RBBaseViewController<RBBaseVCDelegate,UIWebViewDelegate,UIScrollViewDelegate,RBActionSheetDelegate,RBAlertViewDelegate,WXApiDelegate,RBPhotoAsstesGroupViewControllerDelegate,UIAlertViewDelegate,RBPictureViewDelegate,RBNewAlertViewDelegate,HandlerDelegate,RBUploadImageViewControllerDelegate>

@end
