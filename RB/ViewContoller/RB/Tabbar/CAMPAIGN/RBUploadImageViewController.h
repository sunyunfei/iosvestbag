//
//  RBUploadImageViewController.h
//  RB
//
//  Created by RB8 on 2018/1/10.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBPhotoAsstesGroupViewController.h"

#import "RBCampaignDetailViewController.h"
#import "RBCampaignRecruitAutoViewController.h"
#import "RBCampaignRecruitViewController.h"
@protocol RBUploadImageViewControllerDelegate <NSObject>
- (void)RBUploadImageViewControllerDelegateAction;
@end
@interface RBUploadImageViewController : RBBaseViewController<RBBaseVCDelegate,HandlerDelegate,RBPhotoAsstesGroupViewControllerDelegate>

//判断是上传截图还是截图参考
@property(nonatomic,copy)NSString * mark;
//活动ID
@property(nonatomic,copy)NSString * campainID;
//截图示例数组
@property(nonatomic,strong)NSArray * imgArr;
//名称数组
@property(nonatomic,strong)NSArray * nameArr;
@property(nonatomic,weak)id<RBUploadImageViewControllerDelegate>delegateC;
@end
