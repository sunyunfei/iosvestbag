//
//  RBMapSelectView.h
//  RB
//
//  Created by AngusNi on 5/21/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBMapSelectView : UIView

- (void)RBMapSelectViewShowWithArray:(NSArray *)array;
@end
