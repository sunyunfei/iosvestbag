//
//  RBMapSelectView.m
//  RB
//
//  Created by AngusNi on 5/21/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBMapSelectView.h"
#import "Service.h"

@implementation RBMapSelectView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.width*0.85)];
        bgIV.image = [UIImage imageNamed:@"中国.png"];
        [self addSubview:bgIV];
        //
        self.height = bgIV.bottom;
    }
    return self;
}

- (void)RBMapSelectViewShowWithArray:(NSArray *)array {
    for (NSString*str in array) {
        UIImageView *bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.width, self.width*0.85)];
        bgIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[str stringByReplacingOccurrencesOfString:@"市" withString:@""]]];
        [self addSubview:bgIV];
    }
}


@end
