//
//  RBCampaignKolAnalysisView.m
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBCampaignKolAnalysisView.h"

@interface RBCampaignKolAnalysisView () {
    NSArray*colorArray;
}
@end

@implementation RBCampaignKolAnalysisView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        colorArray = @[@"f5484f",@"4e5460",@"4dc0bd",@"fcb464",@"95a0b1",SysColorBlue,SysColorBrown];
    }
    return self;
}

- (void)loadContentView:(RBNlpEntity*)nlpEntity {
    self.nlpEntity = nlpEntity;
    //
    UIScrollView*editScrollView = [[UIScrollView alloc]initWithFrame:self.bounds];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self addSubview:editScrollView];
    // 1
    UIView*textView1 = [[UIView alloc]initWithFrame:CGRectMake(CellLeft, CellBottom, ScreenWidth-2*CellLeft, ScreenWidth/2.0+25.0)];
    textView1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:textView1];
    //
    UILabel*tipLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellBottom, CellBottom, 3.0, 15.0)];
    tipLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [textView1 addSubview:tipLabel1];
    //
    UILabel*tipTextLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(tipLabel1.right+5.0, tipLabel1.top, 200, tipLabel1.height)];
    tipTextLabel1.font = font_cu_cu_13;
    tipTextLabel1.text = NSLocalizedString(@"R8026", @"KOL分类");
    tipTextLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.8];
    [textView1 addSubview:tipTextLabel1];
    //
    MCPieChartView*pieView = [[MCPieChartView alloc] initWithFrame:CGRectMake(0, tipLabel1.bottom, ScreenWidth/2.0, ScreenWidth/2.0)];
    pieView.dataSource = self;
    pieView.delegate = self;
    pieView.ringWidth = 50;
    pieView.tag = 1001;
    [textView1 addSubview:pieView];
    //
    int pieCount = (int)[self.nlpEntity.tag_analysis count];
    float gap = ((ScreenWidth/2.0)-pieCount*20.0)/(pieCount+1);
    for (int i=0; i<pieCount; i++) {
        RBTagEntity*entity = self.nlpEntity.tag_analysis[i];
        //
        UILabel*pieTLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2.0+10.0, tipLabel1.bottom+gap+2.5+i*(gap+20.0), 15.0, 15.0)];
        pieTLabel.backgroundColor = [Utils getUIColorWithHexString:colorArray[i]];
        [textView1 addSubview:pieTLabel];
        //
        UILabel*pieLabel = [[UILabel alloc]initWithFrame:CGRectMake(pieTLabel.right+2.0, tipLabel1.bottom+gap+i*(gap+20.0), ScreenWidth/2.0-27.0, 20.0)];
        pieLabel.font = font_cu_15;
        pieLabel.text = [NSString stringWithFormat:@"%@ %.1f%%",entity.name,entity.ratio.floatValue*100];
        pieLabel.textColor = [Utils getUIColorWithHexString:colorArray[i]];
        [textView1 addSubview:pieLabel];
    }
    // 2
    UIView*textView3 = [[UIView alloc]initWithFrame:CGRectMake(textView1.left, textView1.bottom+CellBottom, textView1.width, ScreenWidth/2.0+25.0)];
    textView3.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:textView3];
    //
    UILabel*tipLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(CellBottom, CellBottom, 3.0, 15.0)];
    tipLabel3.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [textView3 addSubview:tipLabel3];
    //
    UILabel*tipTextLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(tipLabel1.right+5.0, tipLabel1.top, 200, tipLabel1.height)];
    tipTextLabel3.font = font_cu_cu_13;
    tipTextLabel3.text = NSLocalizedString(@"R8025", @"KOL性别");
    tipTextLabel3.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.8];
    [textView3 addSubview:tipTextLabel3];
    //
    MCPieChartView*pieView3 = [[MCPieChartView alloc] initWithFrame:CGRectMake(0, tipLabel1.bottom, ScreenWidth/2.0, ScreenWidth/2.0)];
    pieView3.dataSource = self;
    pieView3.delegate = self;
    pieView3.ringWidth = 50;
    pieView3.tag = 1002;
    [textView3 addSubview:pieView3];
    //
    int pieCount3 = (int)[self.nlpEntity.gender_analysis count];
    float gap3 = ((ScreenWidth/2.0)-pieCount3*20.0)/(pieCount3+1);
    for (int i=0; i<pieCount3; i++) {
        RBTagEntity*entity = self.nlpEntity.gender_analysis[i];
        //
        UILabel*pieTLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2.0+10.0, tipLabel3.bottom+10+gap3+2.5+i*(gap3), 15.0, 15.0)];
        pieTLabel.backgroundColor = [Utils getUIColorWithHexString:colorArray[i]];
        [textView3 addSubview:pieTLabel];
        //
        UILabel*pieLabel = [[UILabel alloc]initWithFrame:CGRectMake(pieTLabel.right+2.0, tipLabel3.bottom+10+gap3+i*(gap3), ScreenWidth/2.0-27.0, 20.0)];
        pieLabel.font = font_cu_15;
        pieLabel.text = [NSString stringWithFormat:@"%@ %.1f%%",entity.name,entity.ratio.floatValue*100];
        pieLabel.textColor = [Utils getUIColorWithHexString:colorArray[i]];
        [textView3 addSubview:pieLabel];
    }
    // 3
    UIView*textView4 = [[UIView alloc]initWithFrame:CGRectMake(textView1.left, textView3.bottom+CellBottom, textView1.width, ScreenWidth/2.0+25.0)];
    textView4.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:textView4];
    //
    UILabel*tipLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(CellBottom, CellBottom, 3.0, 15.0)];
    tipLabel4.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [textView4 addSubview:tipLabel4];
    //
    UILabel*tipTextLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(tipLabel4.right+5.0, tipLabel4.top, 200, tipLabel3.height)];
    tipTextLabel4.font = font_cu_cu_13;
    tipTextLabel4.text = NSLocalizedString(@"R8024", @"KOL年龄");
    tipTextLabel4.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.8];
    [textView4 addSubview:tipTextLabel4];
    //
    MCPieChartView*pieView4 = [[MCPieChartView alloc] initWithFrame:CGRectMake(0, tipLabel4.bottom, ScreenWidth/2.0, ScreenWidth/2.0)];
    pieView4.dataSource = self;
    pieView4.delegate = self;
    pieView4.ringWidth = 50;
    pieView4.tag = 1003;
    [textView4 addSubview:pieView4];
    //
    int pieCount4 = (int)[self.nlpEntity.age_analysis count];
    float gap4 = ((ScreenWidth/2.0)-pieCount4*20.0)/(pieCount4+1);
    for (int i=0; i<pieCount4; i++) {
        RBTagEntity*entity = self.nlpEntity.age_analysis[i];
        //
        UILabel*pieTLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2.0+10.0, tipLabel4.bottom+gap4+2.5+i*(gap4+20.0), 15.0, 15.0)];
        pieTLabel.backgroundColor = [Utils getUIColorWithHexString:colorArray[i]];
        [textView4 addSubview:pieTLabel];
        //
        UILabel*pieLabel = [[UILabel alloc]initWithFrame:CGRectMake(pieTLabel.right+2.0, tipLabel4.bottom+gap4+i*(gap4+20.0), ScreenWidth/2.0-27.0, 20.0)];
        pieLabel.font = font_cu_15;
        pieLabel.text = [NSString stringWithFormat:@"%@ %.1f%%",entity.name,entity.ratio.floatValue*100];
        pieLabel.textColor = [Utils getUIColorWithHexString:colorArray[i]];
        [textView4 addSubview:pieLabel];
    }
    // 4
    UIView*textView5 = [[UIView alloc]initWithFrame:CGRectMake(textView1.left, textView4.bottom+CellBottom, textView1.width, ScreenWidth*0.85+25.0)];
    textView5.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:textView5];
    //
    UILabel*tipLabel5 = [[UILabel alloc] initWithFrame:CGRectMake(CellBottom, CellBottom, 3.0, 15.0)];
    tipLabel5.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [textView5 addSubview:tipLabel5];
    //
    UILabel*tipTextLabel5 = [[UILabel alloc] initWithFrame:CGRectMake(tipLabel4.right+5.0, tipLabel5.top, 200, tipLabel3.height)];
    tipTextLabel5.font = font_cu_cu_13;
    tipTextLabel5.text = NSLocalizedString(@"R8027", @"KOL城市");
    tipTextLabel5.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.8];
    [textView5 addSubview:tipTextLabel5];
    //
    RBMapSelectView*mapView = [[RBMapSelectView alloc]initWithFrame:CGRectMake(0, tipLabel5.bottom, textView5.width, 0)];
    [textView5 addSubview:mapView];
    NSMutableArray*temp = [NSMutableArray new];
    for (RBTagEntity*entity in self.nlpEntity.region_analysis) {
        if(![temp containsObject:entity.name]){
            [temp addObject:entity.name];
        }
    }
    [mapView RBMapSelectViewShowWithArray:temp];
    //
    [editScrollView setContentSize:CGSizeMake(editScrollView.width, textView5.bottom+CellLeft)];
}

#pragma mark - MCPieChartView Delegate
- (NSInteger)numberOfPieInPieChartView:(MCPieChartView *)pieChartView {
    if (pieChartView.tag==1001) {
        return [self.nlpEntity.tag_analysis count];
    } else if (pieChartView.tag==1002) {
        return [self.nlpEntity.gender_analysis count];
    } else {
        return [self.nlpEntity.age_analysis count];
    }
}

- (id)pieChartView:(MCPieChartView *)pieChartView valueOfPieAtIndex:(NSInteger)index {
    if (pieChartView.tag==1001) {
        RBTagEntity*entity = self.nlpEntity.tag_analysis[index];
        return entity.ratio;
    } else if (pieChartView.tag==1002) {
        RBTagEntity*entity = self.nlpEntity.gender_analysis[index];
        return entity.ratio;
    } else {
        RBTagEntity*entity = self.nlpEntity.age_analysis[index];
        return entity.ratio;
    }
}

- (UIColor *)pieChartView:(MCPieChartView *)pieChartView colorOfPieAtIndex:(NSInteger)index {
    return [Utils getUIColorWithHexString:colorArray[index]];
}

@end
