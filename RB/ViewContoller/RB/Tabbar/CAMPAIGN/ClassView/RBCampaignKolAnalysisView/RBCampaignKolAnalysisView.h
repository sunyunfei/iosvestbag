//
//  RBCampaignKolAnalysisView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBAdvertiserTableViewCell.h"
#import "RBMapSelectView.h"

@class RBCampaignKolAnalysisView;
@protocol RBCampaignKolAnalysisViewDelegate <NSObject>
@optional
- (void)RBCampaignKolAnalysisViewLoadRefreshViewData:(RBCampaignKolAnalysisView *)view;
@end


@interface RBCampaignKolAnalysisView : UIView
<MCPieChartViewDelegate,MCPieChartViewDataSource,HandlerDelegate>
@property(nonatomic, weak) id<RBCampaignKolAnalysisViewDelegate> delegate;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) RBNlpEntity *nlpEntity;
- (void)loadContentView:(RBNlpEntity*)nlpEntity;
@end
