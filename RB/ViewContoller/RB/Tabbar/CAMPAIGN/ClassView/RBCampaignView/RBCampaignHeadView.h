//
//  RBCampaignHeadView.h
//  RB
//
//  Created by RB8 on 2018/5/25.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
#import "Service.h"
@protocol RBCampaingnHeadViewDelegate <NSObject>
- (void)didSelectBannerWithIndex:(NSInteger)index;
@end
@interface RBCampaignHeadView : UITableViewHeaderFooterView<SDCycleScrollViewDelegate>
@property(nonatomic,strong)NSArray * imgArr;
@property(nonatomic,strong)SDCycleScrollView *cycleScrollView;
@property(nonatomic,weak)id<RBCampaingnHeadViewDelegate>delegate;
@end
