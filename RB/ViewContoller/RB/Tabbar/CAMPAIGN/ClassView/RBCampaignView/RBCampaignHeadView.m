//
//  RBCampaignHeadView.m
//  RB
//
//  Created by RB8 on 2018/5/25.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBCampaignHeadView.h"

@implementation RBCampaignHeadView
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth * 7/15) shouldInfiniteLoop:YES imageNamesGroup:_imgArr];
        _cycleScrollView.delegate = self;
        _cycleScrollView.autoScrollTimeInterval = 5.0;
        _cycleScrollView.currentPageDotImage = [UIImage imageNamed:@"RBCircleSelec"];
        _cycleScrollView.pageDotImage = [UIImage imageNamed:@"RBCircleUnSelect"];
        [self addSubview:_cycleScrollView];
    }
    return self;
}
- (void)setImgArr:(NSArray *)imgArr{
    _imgArr = imgArr;
    [_cycleScrollView setImageURLStringsGroup:imgArr];
}
#pragma mark - SDCycleScrollView Delegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if ([self.delegate respondsToSelector:@selector(didSelectBannerWithIndex:)]) {
        [self.delegate didSelectBannerWithIndex:index];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
