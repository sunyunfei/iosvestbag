//
//  RBCampaignView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBHomeCampaignTableViewCell.h"

@class RBCampaignView;
@protocol RBCampaignViewDelegate <NSObject>
@optional
- (void)RBCampaignViewLoadRefreshViewData:(RBCampaignView *)view;
- (void)RBCampaignViewSelected:(RBCampaignView *)view andIndex:(int)index;
- (void)RBCampaignViewBannerSelected:(NSInteger)index;
@end


@interface RBCampaignView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate,RBCampaingnHeadViewDelegate>
@property(nonatomic, weak) id<RBCampaignViewDelegate> delegate;
@property(nonatomic, strong) UIView*defaultView;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) id jsonObject;
@property(nonatomic, strong) NSString *status;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
@property(nonatomic,strong) NSArray * imgList;
- (void)RBCampaignViewLoadRefreshViewFirstData;
- (void)RBCampaignViewSetRefreshViewFinish;

@end
