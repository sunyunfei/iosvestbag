//
//  RBSwitchView.m
//  RB
//
//  Created by AngusNi on 3/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBSwitchView.h"
@implementation RBSwitchView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        self.frame = frame;
        //
        self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.leftBtn.frame = CGRectMake(self.width/2.0-100.0, 0, 100.0, 44);
        self.leftBtn.backgroundColor = [UIColor clearColor];
        [self.leftBtn addTarget:self
                    action:@selector(leftBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
        self.leftBtn.exclusiveTouch = YES;
        [self addSubview:self.leftBtn];
        //
        self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.rightBtn.frame = CGRectMake(self.width/2.0, 0, 100.0, 44);
        self.rightBtn.backgroundColor = [UIColor clearColor];
        [self.rightBtn addTarget:self
                    action:@selector(rightBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];
        self.rightBtn.exclusiveTouch = YES;
        [self addSubview:self.rightBtn];
        //
        self.leftLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.leftLabel.font = font_cu_17;
        [self addSubview:self.leftLabel];
        //
        self.lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.width/2.0-0.5, (44.0-20.0)/2.0, 1.0, 20.0)];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
        [self addSubview:self.lineLabel];
        //
        self.rightLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.rightLabel.font = self.leftLabel.font;
        [self addSubview:self.rightLabel];
    }
    return self;
}

#pragma mark - RBSelectView Delegate
- (void)showWithData:(NSArray *)data andSelected:(int)tag {
    if(self.array==nil){
        self.array = data;
        self.leftLabel.text = data[0];
        self.rightLabel.text = data[1];
    }
    if (tag==0) {
        self.leftLabel.font = font_cu_cu_17;
        self.rightLabel.font = font_17;
        CGSize leftLabelSize = [Utils getUIFontSizeFitW:self.leftLabel];
        CGSize rightLabelSize = [Utils getUIFontSizeFitW:self.rightLabel];
        self.leftLabel.frame = CGRectMake(self.width/2.0-leftLabelSize.width-20.0, 0, leftLabelSize.width, 44);
        self.rightLabel.frame = CGRectMake(self.width/2.0+20.0, 0, rightLabelSize.width, 44);
        self.leftLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.rightLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        self.lineLabel.frame = CGRectMake(self.leftLabel.left-10.0, 35.0, self.leftLabel.width+20.0, 3.0);
    } else {
        self.leftLabel.font = font_17;
        self.rightLabel.font = font_cu_cu_17;
        CGSize leftLabelSize = [Utils getUIFontSizeFitW:self.leftLabel];
        CGSize rightLabelSize = [Utils getUIFontSizeFitW:self.rightLabel];
        self.leftLabel.frame = CGRectMake(self.width/2.0-leftLabelSize.width-20.0, 0, leftLabelSize.width, 44);
        self.rightLabel.frame = CGRectMake(self.width/2.0+20.0, 0, rightLabelSize.width, 44);
        self.leftLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        self.rightLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.lineLabel.frame = CGRectMake(self.rightLabel.left-10.0, 35.0, self.rightLabel.width+20.0, 3.0);
    }
}

#pragma mark - UIButton Delegate
- (void)leftBtnAction:(UIButton *)sender {
    [self showWithData:self.array andSelected:0];
    if ([self.delegate
         respondsToSelector:@selector(RBSwitchViewSelected:andIndex:)]) {
        [self.delegate RBSwitchViewSelected:self andIndex:0];
    }
}

- (void)rightBtnAction:(UIButton *)sender {
    [self showWithData:self.array andSelected:1];
    if ([self.delegate
         respondsToSelector:@selector(RBSwitchViewSelected:andIndex:)]) {
        [self.delegate RBSwitchViewSelected:self andIndex:1];
    }
}

@end
