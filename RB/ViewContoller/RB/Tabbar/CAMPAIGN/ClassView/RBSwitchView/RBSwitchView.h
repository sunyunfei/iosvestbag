//
//  RBSwitchView.h
//  RB
//
//  Created by AngusNi on 3/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"

@class RBSwitchView;
@protocol RBSwitchViewDelegate <NSObject>
@optional
- (void)RBSwitchViewSelected:(RBSwitchView *)view andIndex:(int)index;
@end


@interface RBSwitchView : UIView
@property(nonatomic, weak) id<RBSwitchViewDelegate> delegate;
@property(nonatomic, strong) NSArray*array;
@property(nonatomic, strong) UILabel*leftLabel;
@property(nonatomic, strong) UILabel*lineLabel;
@property(nonatomic, strong) UILabel*rightLabel;
@property(nonatomic, strong) UIButton *leftBtn;
@property(nonatomic, strong) UIButton *rightBtn;
@property(nonatomic, assign) int selected;

-(void)showWithData:(NSArray*)data andSelected:(int)tag;
@end