//
//  RBHomeCampaignTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBHomeCampaignTableViewCell.h"
#import "Service.h"

@implementation RBHomeCampaignTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
        self.bgView.userInteractionEnabled = YES;
        self.bgView.tag = 1000;
        [self.contentView addSubview:self.bgView];
        //
        self.bgIV = [[YYAnimatedImageView alloc] initWithFrame:CGRectZero];
        self.bgIV.contentMode = UIViewContentModeScaleAspectFill;
        self.bgIV.clipsToBounds = YES;
        self.bgIV.tag = 1001;
        [self.contentView addSubview:self.bgIV];
        //
        self.bgIVLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.bgIVLabel.tag = 1002;
        [self.contentView addSubview:self.bgIVLabel];
        //
        self.tagIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.tagIV.tag = 999;
        [self.contentView addSubview:self.tagIV];
        //
        self.endLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.endLabel.text = NSLocalizedString(@"R2013",@"已结束");
        self.endLabel.font = font_cu_cu_(10.0);
        self.endLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.endLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorRed];
        self.endLabel.textAlignment = NSTextAlignmentCenter;
        self.endLabel.tag = 1003;
        [self.contentView addSubview:self.endLabel];
        //
        self.tLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.tLabel.font = font_cu_cu_15;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.tLabel.textAlignment = NSTextAlignmentCenter;
        self.tLabel.tag = 1004;
        [self.contentView addSubview:self.tLabel];
        //
        self.cLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.cLabel.font = font_cu_11;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.cLabel.textAlignment = NSTextAlignmentCenter;
        self.cLabel.tag = 1005;
        [self.contentView addSubview:self.cLabel];
        //
        self.robLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.robLabel.font = font_cu_15;
        self.robLabel.backgroundColor = SysColorCoverDeep;
        self.robLabel.textAlignment = NSTextAlignmentCenter;
        self.robLabel.tag = 2000;
        self.robLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        [self.contentView addSubview:self.robLabel];
        //
        self.bottomBgLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.bottomBgLabel.backgroundColor = SysColorCoverDeep;
        self.bottomBgLabel.tag = 1100;
        [self.contentView addSubview:self.bottomBgLabel];
        //
        self.bottomLeftTLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.bottomLeftTLabel.font = font_cu_11;
        self.bottomLeftTLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.bottomLeftTLabel.tag = 1101;
        [self.contentView addSubview:self.bottomLeftTLabel];
        //
        self.dayLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.dayLabel.font = font_cu_15;
        self.dayLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.dayLabel.tag = 20000;
        [self.contentView addSubview:self.dayLabel];
        //
        self.bottomLeftCLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.bottomLeftCLabel.font = self.tLabel.font;
        self.bottomLeftCLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.bottomLeftCLabel.tag = 1102;
        [self.contentView addSubview:self.bottomLeftCLabel];
        //
        self.bottomRightTLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.bottomRightTLabel.font = self.bottomLeftTLabel.font ;
        self.bottomRightTLabel.textColor = self.bottomLeftTLabel.textColor ;
        self.bottomRightTLabel.tag = 1103;
        [self.contentView addSubview:self.bottomRightTLabel];
        //
        self.bottomRightCLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.bottomRightCLabel.font = self.bottomLeftCLabel.font;
        self.bottomRightCLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
        self.bottomRightCLabel.tag = 1104;
        [self.contentView addSubview:self.bottomRightCLabel];
        //
        //
        self.centerLeftTLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.centerLeftTLabel.font = font_cu_11;
        self.centerLeftTLabel.textAlignment = NSTextAlignmentCenter;
        self.centerLeftTLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.centerLeftTLabel.backgroundColor = SysColorCoverDeep;
        self.centerLeftTLabel.tag = 1200;
        [self.contentView addSubview:self.centerLeftTLabel];
        //
        self.centerLeftCLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.centerLeftCLabel.font = self.tLabel.font;
        self.centerLeftCLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.centerLeftCLabel.tag = 1201;
        self.centerLeftCLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.centerLeftCLabel];
        //
        self.centerRightTLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.centerRightTLabel.font = self.centerLeftTLabel.font;
        self.centerRightTLabel.textAlignment = NSTextAlignmentCenter;
        self.centerRightTLabel.textColor = self.centerLeftTLabel.textColor;
        self.centerRightTLabel.backgroundColor = self.centerLeftTLabel.backgroundColor ;
        self.centerRightTLabel.tag = 1202;
        [self.contentView addSubview:self.centerRightTLabel];
        //
        self.centerRightCLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.centerRightCLabel.font = self.centerLeftCLabel.font;
        self.centerRightCLabel.textColor = self.centerLeftCLabel.textColor;
        self.centerRightCLabel.tag = 1203;
        [self.contentView addSubview:self.centerRightCLabel];
        //
        self.centerLineLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.centerLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.6];
        self.centerLineLabel.tag = 1204;
        [self.contentView addSubview:self.centerLineLabel];
    }
    return self;
}
- (instancetype)loadRBCampaignCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {

    RBActivityEntity*entity = datalist[indexPath.row];
    self.entity = entity;
    NSString*per_action_budget = [NSString stringWithFormat:@"￥%@",[Utils getNSStringTwoFloat:entity.campaign.per_action_budget]];
    NSString*earn_money = [NSString stringWithFormat:@"￥%@",[Utils getNSStringTwoFloat:entity.earn_money]];
    NSString*take_budget = [NSString stringWithFormat:@"￥%@",[Utils getNSStringTwoFloat:entity.campaign.take_budget]];
   
    
    NSString*deadline;
    NSArray * arr;
    NSArray * languages = [NSLocale preferredLanguages];
    NSString * currentLanguage = [languages objectAtIndex:0];
    if ([currentLanguage isEqualToString:@"en-CN"]) {
        arr = [[Utils getUIDateCompareNowForEnglish:entity.campaign.deadline] componentsSeparatedByString:@","];
        if (arr.count == 3) {
            deadline = [NSString stringWithFormat:@"%@ hour(s) %@ min(s)",arr[1],arr[2]];
            if ([arr[0] isEqualToString:@"0"]) {
                self.dayLabel.hidden = YES;
                self.dayLabel.text = @"";
            }else{
                self.dayLabel.text = [NSString stringWithFormat:@"%@ day(s)",arr[0]];
            }
        }else{
            self.dayLabel.hidden = YES;
            self.dayLabel.text = @"";
            deadline = [Utils getUIDateCompareNow:entity.campaign.deadline];
        }
    }else{
        self.dayLabel.hidden = YES;
        deadline = [NSString stringWithFormat:@"%@",[Utils getUIDateCompareNow:entity.campaign.deadline]];
        
    }
    
    NSString*per_budget_type = [NSString stringWithFormat:@"%@",entity.campaign.per_budget_type];
    NSString*status = [NSString stringWithFormat:@"%@",entity.status];
    //
    self.tLabel.text = [NSString stringWithFormat:@"%@",entity.campaign.name];
    if ([LocalService getRBIsIncheck] == YES) {
        self.cLabel.text = @"";
    }else{
        self.cLabel.text = [NSString stringWithFormat:@"%@",entity.campaign.brand_name];

    }
    
    
    [Utils getUILabel:self.cLabel withSpacing:8];
    // 判断活动类型
    self.tagIV.image = nil;
    if ([per_budget_type isEqualToString:@"click"]) {
        self.bottomLeftTLabel.text = NSLocalizedString(@"R2014",@"点击");
    } else if ([per_budget_type isEqualToString:@"cpa"]) {
        self.bottomLeftTLabel.text = NSLocalizedString(@"R2015",@"效果");
    } else if ([per_budget_type isEqualToString:@"cpt"]) {
        self.bottomLeftTLabel.text = NSLocalizedString(@"R2301",@"任务");
        self.tagIV.image = [UIImage imageNamed:@"icon_task_tag_new.png"];
    } else if ([per_budget_type isEqualToString:@"simple_cpi"]) {
        self.bottomLeftTLabel.text = NSLocalizedString(@"R2200",@"下载");
        self.tagIV.image = [UIImage imageNamed:@"icon_task_tag_new.png"];
    } else if ([per_budget_type isEqualToString:@"recruit"]) {
        self.bottomLeftTLabel.text = NSLocalizedString(@"R2016",@"招募");
        self.tagIV.image = [UIImage imageNamed:@"icon_task_tag_recruit.png"];
    } else if ([per_budget_type isEqualToString:@"invite"]) {
        self.bottomLeftTLabel.text = NSLocalizedString(@"R2107",@"特邀");
        self.tagIV.image = [UIImage imageNamed:@"icon_task_tag_invite.png"];
    } else {
        self.bottomLeftTLabel.text = NSLocalizedString(@"R2017",@"转发");
    }
    self.centerLeftTLabel.text = self.bottomLeftTLabel.text; // This is overridden for executing campaigns below
    self.endLabel.hidden = YES;
    // 判断活动是否结束？
    NSString*campaignStatus = [NSString stringWithFormat:@"%@",entity.campaign.status];
    if ([campaignStatus isEqualToString:@"countdown"]) {
        deadline = [NSString stringWithFormat:@"%@",[Utils getUIDateCompareNowFortrailer:entity.campaign.start_time]];
        
    }
    if ([deadline isEqualToString:NSLocalizedString(@"R2518", @"活动已开始")]) {
        entity.campaign.status = @"running";
        campaignStatus = @"running";
    }
    if ([campaignStatus isEqualToString:@"settled"]||[campaignStatus isEqualToString:@"executed"]||[campaignStatus isEqualToString:@"rejected"]) {
        self.endLabel.hidden = NO;
        self.bgIVLabel.backgroundColor = SysColorCoverDeep;
    } else {
        self.endLabel.hidden = YES;
        self.bgIVLabel.backgroundColor = SysColorCover;
    }
    // 判断是否参与活动？
    self.bottomBgLabel.hidden=YES;
    self.bottomLeftTLabel.hidden=YES;
    self.bottomLeftCLabel.hidden=YES;
    self.bottomRightTLabel.hidden=YES;
    self.bottomRightCLabel.hidden=YES;
    self.centerLeftTLabel.hidden=YES;
    self.centerLeftCLabel.hidden=YES;
    self.centerRightTLabel.hidden=YES;
    self.centerRightCLabel.hidden=YES;
    self.centerLineLabel.hidden=YES;
    self.dayLabel.hidden = YES;
    self.robLabel.hidden = YES;
    // Completed campaigns
    if ([status isEqualToString:@"approved"]||[status isEqualToString:@"rejected"]||[status isEqualToString:@"finished"]||[status isEqualToString:@"settled"]) {
        self.bottomBgLabel.hidden=NO;
        //self.bottomLeftTLabel.hidden=NO; //Hide per-action-budget
        //self.bottomLeftCLabel.hidden=NO; //Hide per-action-budget
        self.bottomRightTLabel.hidden=NO;
        self.bottomRightCLabel.hidden=NO;
        //
        self.bottomLeftCLabel.text = per_action_budget;
        if ([status isEqualToString:@"rejected"]) {
            self.bottomRightTLabel.text = NSLocalizedString(@"R2018",@"已赚");
            self.bottomRightCLabel.text = @"￥0.0";
        } else if ([status isEqualToString:@"settled"]) {
            self.bottomRightTLabel.text = NSLocalizedString(@"R2018",@"已赚");
            self.bottomRightCLabel.text = earn_money;
        } else {
            self.bottomRightTLabel.text = NSLocalizedString(@"R2019",@"即将赚");
            self.bottomRightCLabel.text = earn_money;
         //   self.endLabel.hidden = NO;
        }
    } else { // Campaign is executing
        self.centerLeftTLabel.hidden=NO;
        self.centerLeftCLabel.hidden=NO;
        if (arr.count == 3) {
            if ([arr[0]isEqualToString:@"0"]) {
                self.dayLabel.hidden = YES;
            }else{
                self.dayLabel.hidden = NO;
            }
        }
        self.centerRightTLabel.hidden=NO;
        self.centerRightCLabel.hidden=NO;
        self.centerLineLabel.hidden=NO;
        
        if ([campaignStatus isEqualToString:@"countdown"]) {
            self.bottomBgLabel.hidden = NO;
            self.bottomRightTLabel.hidden = NO;
            self.bottomRightTLabel.text = NSLocalizedString(@"R2519", @"活动即将开始,请准时参加!");
            self.bgIVLabel.backgroundColor = SysColorCoverDeep;
            self.centerLeftTLabel.text = NSLocalizedString(@"R2516",@"距离开始倒计时:");
            self.centerLeftCLabel.text = [NSString stringWithFormat:@"%@", deadline];
        }else{
            self.centerLeftTLabel.text = NSLocalizedString(@"R2504",@"活动剩余:");
            self.centerLeftCLabel.text = [NSString stringWithFormat:@"%@", deadline];
        }

        if ([campaignStatus isEqualToString:@"settled"]||[campaignStatus isEqualToString:@"executed"]||[campaignStatus isEqualToString:@"rejected"]) {
            self.robLabel.text = NSLocalizedString(@"R2020",@"已抢完");
            self.robLabel.hidden = NO;
            self.centerRightCLabel.text = take_budget;
            self.centerRightTLabel.hidden = YES;
            self.centerLeftTLabel.hidden = YES;
            self.centerLeftCLabel.hidden = YES;
            self.dayLabel.hidden = YES;
            self.centerLineLabel.hidden = YES;
            self.centerRightCLabel.hidden = YES;
        }else {
            self.centerLeftTLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
            self.centerRightTLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
            self.centerRightTLabel.text = NSLocalizedString(@"R2501",@"最多可赚:");
            self.centerRightCLabel.text = [NSString stringWithFormat:@"¥%@",entity.campaign.budget];;
        }
        if ([per_budget_type isEqualToString:@"recruit"]) {
            self.centerRightTLabel.text = NSLocalizedString(@"R2022",@"招募人数");
            self.centerRightCLabel.text = [NSString stringWithFormat:@"%@",entity.campaign.max_action];
        }
        if ([per_budget_type isEqualToString:@"invite"]) {
            self.centerRightTLabel.text = NSLocalizedString(@"R2108",@"特邀人数");
            self.centerRightCLabel.text = [NSString stringWithFormat:@"%@",entity.campaign.max_action];
        }
    }
    if([NSString stringWithFormat:@"%@",entity.campaign.hide_brand_name].intValue==1) {
        self.cLabel.hidden = YES;
    } else {
        self.cLabel.hidden = NO;
    }
    //
    self.bgIV.frame = CGRectMake(0, 5.0, ScreenWidth, (ScreenWidth*9.0)/16.0);
    self.tagIV.frame = CGRectMake(self.bgIV.left, self.bgIV.top, 50.0 , 50.0);
    NSString*imgUrl = [NSString stringWithFormat:@"%@?imageView2/0/w/%d/h/%d/q/30",entity.campaign.img_url,(int)self.bgIV.width*2,(int)self.bgIV.height*2];
    if([[entity.campaign.img_url componentsSeparatedByString:@"?"]count]>1) {
        imgUrl = [NSString stringWithFormat:@"%@?imageView2/0/w/%d/h/%d/q/30",[entity.campaign.img_url componentsSeparatedByString:@"?"][0],(int)self.bgIV.width*2,(int)self.bgIV.height*2];
    }
    if([[entity.campaign.img_url componentsSeparatedByString:@"!cover2"]count]>1) {
        imgUrl = [NSString stringWithFormat:@"%@?imageView2/0/w/%d/h/%d/q/30",[entity.campaign.img_url componentsSeparatedByString:@"!cover2"][0],(int)self.bgIV.width*2,(int)self.bgIV.height*2];
    }
//    [self.bgIV yy_setImageWithURL:[NSURL URLWithString:imgUrl] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
    [self.bgIV setImageWithURL:[NSURL URLWithString:imgUrl] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
    //    [self.bgIV sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:nil options:SDWebImageProgressiveDownload];
    //
    self.bgIVLabel.frame = self.bgIV.frame;
    
//    if ([campaignStatus isEqualToString:@"executed"] || ![entity.img_status isEqualToString:@"passed"]) {
//        self.endLabel.text = NSLocalizedString(@"R2515",@"正在结算");
//        self.endLabel.frame = CGRectMake(self.bgIV.left+(self.bgIV.width-60.0)/2.0, self.bgIV.top+30.0*(ScreenWidth/375.0), 60.0, 16.0);
//    }
//    if([campaignStatus isEqualToString:@"settled"] || ([campaignStatus isEqualToString:@"executed"]&&[entity.img_status isEqualToString:@"passed"])){
//        self.endLabel.text = NSLocalizedString(@"R2013",@"已结束");
//        self.endLabel.frame = CGRectMake(self.bgIV.left+(self.bgIV.width-40.0)/2.0, self.bgIV.top+30.0*(ScreenWidth/375.0), 40.0, 16.0);
//    }
    if ([status isEqualToString:@"approved"] || [status isEqualToString:@"finished"]) {
        self.endLabel.text = NSLocalizedString(@"R2515",@"正在结算");
        self.endLabel.frame = CGRectMake(self.bgIV.left+(self.bgIV.width-60.0)/2.0, self.bgIV.top+30.0*(ScreenWidth/375.0), 60.0, 16.0);
    }else{
        self.endLabel.text = NSLocalizedString(@"R2013",@"已结束");
        self.endLabel.frame = CGRectMake(self.bgIV.left+(self.bgIV.width-40.0)/2.0, self.bgIV.top+30.0*(ScreenWidth/375.0), 40.0, 16.0);
    }
    // 招募活动
    if ([per_budget_type isEqualToString:@"recruit"]) {
        if ([status isEqualToString:@"running"]) {
            if([[Utils getUIDateCompareNow:[NSString stringWithFormat:@"%@",entity.campaign.recruit_end_time]] isEqualToString:NSLocalizedString(@"R2056", @"活动已结束")]) {
                self.endLabel.hidden = NO;
                self.bgIVLabel.backgroundColor = SysColorCoverDeep;
                self.endLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorRed];
                self.endLabel.text = NSLocalizedString(@"R2300",@"报名结束");
                self.endLabel.frame = CGRectMake(self.bgIV.left+(self.bgIV.width-50.0)/2.0, self.bgIV.top+30.0*(ScreenWidth/375.0), 50.0, 16.0);
            } else {
                self.endLabel.hidden = NO;
                self.bgIVLabel.backgroundColor = SysColorCoverDeep;
                self.endLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorRed];
                self.endLabel.text = NSLocalizedString(@"R2303",@"报名中");
                self.endLabel.frame = CGRectMake(self.bgIV.left+(self.bgIV.width-40.0)/2.0, self.bgIV.top+30.0*(ScreenWidth/375.0), 40.0, 16.0);
            }
        }
        
        if ([status isEqualToString:@"applying"]) {
            self.endLabel.hidden = NO;
            self.bgIVLabel.backgroundColor = SysColorCoverDeep;
            self.endLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorRed];
            self.endLabel.text = NSLocalizedString(@"R2304",@"已报名");
            self.endLabel.frame = CGRectMake(self.bgIV.left+(self.bgIV.width-40.0)/2.0, self.bgIV.top+30.0*(ScreenWidth/375.0), 40.0, 16.0);
        }
    }
    
    self.tLabel.frame = CGRectMake(self.bgIV.left+10.0, self.bgIV.top+54.0*(ScreenWidth/375.0), self.bgIV.width-20.0, 18.0);
    self.cLabel.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom+4.0, self.tLabel.width, 13.0);
    //
    self.bottomBgLabel.frame = CGRectMake(self.bgIV.left, self.bgIV.bottom-40, self.bgIV.width, 40);
    self.bottomLeftTLabel.frame = CGRectMake(self.bgIV.left, self.bottomBgLabel.top, 1000.0, self.bottomBgLabel.height);
    self.bottomLeftCLabel.frame = self.bottomLeftTLabel.frame;
    self.bottomRightTLabel.frame = self.bottomLeftTLabel.frame;
    self.bottomRightCLabel.frame = self.bottomLeftTLabel.frame;
    CGSize bottomLeftTLabelSize = [Utils getUIFontSizeFitW:self.bottomLeftTLabel];
    CGSize bottomLeftCLabelSize = [Utils getUIFontSizeFitW:self.bottomLeftCLabel];
    CGSize bottomRightTLabelSize = [Utils getUIFontSizeFitW:self.bottomRightTLabel];
    CGSize bottomRightCLabelSize = [Utils getUIFontSizeFitW:self.bottomRightCLabel];
    self.bottomLeftTLabel.frame = CGRectMake(self.bottomBgLabel.left+(ScreenWidth-115.0*(ScreenWidth/375.0)-bottomLeftTLabelSize.width-bottomLeftCLabelSize.width-8.0-bottomRightTLabelSize.width-bottomRightCLabelSize.width)/2.0, self.bottomBgLabel.top, bottomLeftTLabelSize.width, self.bottomBgLabel.height);
    self.bottomLeftCLabel.frame = CGRectMake(self.bottomLeftTLabel.right+4.0, self.bottomBgLabel.top, bottomLeftCLabelSize.width, self.bottomBgLabel.height);
    if ([campaignStatus isEqualToString:@"countdown"]) {
        self.bottomRightTLabel.frame = CGRectMake((ScreenWidth - bottomRightTLabelSize.width)/2, self.bottomBgLabel.top, bottomRightTLabelSize.width, self.bottomBgLabel.height);
        self.bottomRightTLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
    }else{
        self.bottomRightTLabel.frame = CGRectMake((ScreenWidth - bottomRightCLabelSize.width - bottomRightTLabelSize.width-4)/2, self.bottomBgLabel.top, bottomRightTLabelSize.width, self.bottomBgLabel.height);
        self.bottomRightTLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    }
    self.bottomRightCLabel.frame = CGRectMake(self.bottomRightTLabel.right+4.0, self.bottomBgLabel.top, bottomRightCLabelSize.width, self.bottomBgLabel.height);
    //
    self.centerLeftTLabel.frame = CGRectMake(self.bgIV.left, self.bgIV.top+125.0*(ScreenWidth/375.0), 1000.0, 18.0);
    self.centerLeftCLabel.frame = self.centerLeftTLabel.frame;
    self.centerRightTLabel.frame = self.centerLeftTLabel.frame;
    self.centerRightCLabel.frame = self.centerLeftTLabel.frame;
    CGSize centerLeftTLabelSize = [Utils getUIFontSizeFitW:self.centerLeftTLabel];
    CGSize centerLeftCLabelSize = [Utils getUIFontSizeFitW:self.centerLeftCLabel];
    CGSize centerRightTLabelSize = [Utils getUIFontSizeFitW:self.centerRightTLabel];
    CGSize centerRightCLabelSize = [Utils getUIFontSizeFitW:self.centerRightCLabel];
    CGSize dayLabelSize = [Utils getUIFontSizeFitW:self.dayLabel];
    CGSize robLabelSize = [Utils getUIFontSizeFitW:self.robLabel];
    //英文下frame
    if(arr.count == 3){
        self.centerLeftTLabel.frame = CGRectMake(self.bgIV.left+(self.bgIV.width-centerLeftTLabelSize.width-centerRightTLabelSize.width-40.0-90.0*(ScreenWidth/375.0))/2.0, self.centerLeftTLabel.top, centerLeftTLabelSize.width+20.0, 18.0);
        self.centerRightTLabel.frame = CGRectMake(self.centerLeftTLabel.right+90.0*(ScreenWidth/375.0), self.centerLeftTLabel.top, centerRightTLabelSize.width+20.0, 18.0);
    }else{//中文下frame
        self.centerLeftTLabel.frame = CGRectMake(self.bgIV.left+(self.bgIV.width-centerLeftTLabelSize.width-centerRightTLabelSize.width-40.0-115.0*(ScreenWidth/375.0))/2.0, self.centerLeftTLabel.top, centerLeftTLabelSize.width+20.0, 18.0);
        self.centerRightTLabel.frame = CGRectMake(self.centerLeftTLabel.right+115.0*(ScreenWidth/375.0), self.centerLeftTLabel.top, centerRightTLabelSize.width+20.0, 18.0);
    }
    if (self.dayLabel.hidden == YES) {
        self.dayLabel.frame = CGRectZero;
        self.centerLeftCLabel.frame = CGRectMake(0, self.centerLeftTLabel.bottom+5.0, centerLeftCLabelSize.width, 18.0);
    }else if (self.dayLabel.hidden == NO){
        self.dayLabel.frame = CGRectMake(0, self.centerLeftTLabel.bottom +5.0, dayLabelSize.width, 18.0);
        self.dayLabel.center = CGPointMake(self.centerLeftTLabel.center.x, self.dayLabel.center.y);
        self.centerLeftCLabel.frame = CGRectMake(0, self.dayLabel.bottom+5.0, centerLeftCLabelSize.width, 18.0);
    }
    
    self.centerLeftCLabel.center = CGPointMake(self.centerLeftTLabel.center.x, self.centerLeftCLabel.center.y);
    self.robLabel.frame = CGRectMake((ScreenWidth - robLabelSize.width - 20)/2, self.cLabel.bottom +10, robLabelSize.width+20, 18);
   
    //self.centerRightTLabel.frame = CGRectMake(self.centerLeftTLabel.right+115.0*(ScreenWidth/375.0), self.centerLeftTLabel.top, centerRightTLabelSize.width+20.0, 18.0);
    self.centerRightCLabel.frame = CGRectMake(0, self.centerRightTLabel.bottom+5.0, centerRightCLabelSize.width, 18.0);
    self.centerRightCLabel.center = CGPointMake(self.centerRightTLabel.center.x, self.centerRightCLabel.center.y);

    
    if ([LocalService getRBIsIncheck] == YES) {
        self.centerLeftTLabel.hidden = YES;
        self.centerLeftCLabel.hidden = YES;
        self.centerRightTLabel.hidden = YES;
        self.centerRightCLabel.hidden = YES;
        self.bottomRightTLabel.hidden = YES;
        self.bottomRightCLabel.hidden = YES;
    }
    self.centerLineLabel.frame = CGRectMake(ScreenWidth/2.0-0.75, self.centerRightCLabel.top, 1.5, 18.0);
    if ([campaignStatus isEqualToString:@"countdown"]) {
        self.centerLeftTLabel.top = self.bgIV.top+125.0*(ScreenWidth/375.0) - 20;
        self.centerLeftCLabel.top = self.centerLeftTLabel.bottom+5.0;
        self.centerRightTLabel.top = self.bgIV.top+125.0*(ScreenWidth/375.0) - 20;
        self.centerRightCLabel.top = self.centerLeftTLabel.bottom+5.0;
        self.centerLineLabel.top = self.centerRightCLabel.top;
        self.tLabel.top = self.bgIV.top+54.0*(ScreenWidth/375.0) - 20;
        self.cLabel.top = self.tLabel.bottom+4.0;
    }else{
        self.centerLeftTLabel.top = self.bgIV.top+125.0*(ScreenWidth/375.0);
        if (self.dayLabel.hidden == NO) {
            self.dayLabel.frame = CGRectMake(0, self.centerLeftTLabel.bottom +5.0, dayLabelSize.width, 18.0);
            self.dayLabel.center = CGPointMake(self.centerLeftTLabel.center.x, self.dayLabel.center.y);
//            self.centerLeftCLabel.frame = CGRectMake(0, self.dayLabel.bottom+5.0, centerLeftCLabelSize.width, 18.0);
            self.centerLeftCLabel.top = self.dayLabel.bottom+5.0;
        }else{
            self.dayLabel.frame = CGRectZero;
            self.centerLeftCLabel.top = self.centerLeftTLabel.bottom+5.0;
        }
        self.centerRightTLabel.top = self.bgIV.top+125.0*(ScreenWidth/375.0);
        self.centerRightCLabel.top = self.centerLeftTLabel.bottom+5.0;
        self.centerLineLabel.top = self.centerRightCLabel.top;
        self.tLabel.top = self.bgIV.top+54.0*(ScreenWidth/375.0);
        self.cLabel.top = self.tLabel.bottom+4.0;
    }
    //
    self.bgView.frame = CGRectMake(0, 0, ScreenWidth, self.bgIV.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, self.bgView.bottom);
    //
    UIView*cellBgView = [[UIView alloc]initWithFrame:self.frame];
    cellBgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.1];
    self.backgroundView = cellBgView;
    return self;
}
@end
