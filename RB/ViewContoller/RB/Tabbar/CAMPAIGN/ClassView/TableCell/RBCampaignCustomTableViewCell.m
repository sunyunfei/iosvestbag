//
//  RBCampaignCustomTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBCampaignCustomTableViewCell.h"
#import "Service.h"

@implementation RBCampaignCustomTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UIImageView*tIV = [[UIImageView alloc]initWithFrame:CGRectZero];
        tIV.tag = 1001;
        [self.contentView addSubview:tIV];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1003;
        [self.contentView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel.font = font_11;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        cLabel.tag = 1004;
        cLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:cLabel];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1005;
        [self.contentView addSubview:lineLabel];
        //
        UILabel*arrowLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        arrowLabel.font = font_icon_(13.0);
        arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        arrowLabel.text = Icon_btn_right;
        arrowLabel.tag = 1007;
        [self.contentView addSubview:arrowLabel];
    }
    return self;
}

- (instancetype)loadRBCampaignCustomCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UIImageView*tIV=(UIImageView*)[self.contentView viewWithTag:1001];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1004];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1005];
    UILabel*arrowLabel=(UILabel*)[self.contentView viewWithTag:1007];
    //
    RBKOLBannerEntity *entity = datalist[indexPath.row];
    bgView.backgroundColor = [Utils getUIColorWithHexString:@"ffffff"];
    tLabel.text = entity.cover_url;
    cLabel.text = @"";
    if ([[NSString stringWithFormat:@"%@",entity.category]isEqualToString:@"image"]) {
        tIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_task_pic.png"]];
    } else if ([[NSString stringWithFormat:@"%@",entity.category]isEqualToString:@"video"]) {
        tIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_task_video.png"]];
    } else {
        tIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_task_link.png"]];
    }
    tIV.frame = CGRectMake(CellLeft, (60.0-20.0)/2.0, 20.0, 20.0);
    tLabel.frame = CGRectMake(tIV.right+CellLeft, 0, ScreenWidth-tIV.right-CellLeft-13.0-5.0, 60.0);
    cLabel.frame = CGRectMake(CellLeft, 0, ScreenWidth-CellLeft*2.0-13.0-5.0, tLabel.height);
    lineLabel.frame = CGRectMake(0, tLabel.bottom-0.5f, ScreenWidth, 0.5f);
    arrowLabel.frame = CGRectMake(ScreenWidth-CellLeft-13.0, (tLabel.height-13.0)/2.0, 13.0, 13.0);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    return self;
}
@end
