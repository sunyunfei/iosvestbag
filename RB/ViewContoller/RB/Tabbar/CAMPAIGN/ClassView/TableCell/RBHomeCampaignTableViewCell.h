//
//  RBHomeCampaignTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYAnimatedImageView.h"
#import "RBActivityEntity.h"
@interface RBHomeCampaignTableViewCell : UITableViewCell
@property(nonatomic, strong)  UIView *bgView;
//
@property(nonatomic, strong)  UIImageView *tagIV;
//
@property(nonatomic, strong)  YYAnimatedImageView *bgIV;
//
@property(nonatomic, strong)  UILabel*bgIVLabel;
//
@property(nonatomic, strong)  UILabel *endLabel;
//
@property(nonatomic, strong)  UILabel *tLabel;
//
@property(nonatomic, strong)  UILabel *cLabel;
//
@property(nonatomic, strong)  UILabel*bottomBgLabel;
//
@property(nonatomic, strong)  UILabel*bottomLeftTLabel;
//
@property(nonatomic, strong)  UILabel*bottomLeftCLabel;
//
@property(nonatomic, strong)  UILabel*bottomRightTLabel;
//
@property(nonatomic, strong)  UILabel*bottomRightCLabel;
//
@property(nonatomic, strong)  UILabel*centerLeftTLabel;
//
@property(nonatomic, strong)  UILabel*centerLeftCLabel;
//
@property(nonatomic, strong)  UILabel*centerRightTLabel;
//
@property(nonatomic, strong)  UILabel*centerRightCLabel;
//
@property(nonatomic, strong)  UILabel*centerLineLabel;
//英文天数
@property(nonatomic, strong)  UILabel * dayLabel;
//已抢完label
@property(nonatomic,strong)   UILabel * robLabel;
//
@property(nonatomic,strong)   RBActivityEntity * entity;

- (instancetype)loadRBCampaignCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
@end
