//
//  RBCampaignKolCollectionCell.m
//

#import "RBCampaignKolCollectionCell.h"
#import "Service.h"

@implementation RBCampaignKolCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
	if (self = [super initWithFrame:frame]) {
        self.cellIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.cellIV.backgroundColor = [UIColor clearColor];
        self.cellIV.contentMode = UIViewContentModeScaleAspectFill;
        self.cellIV.clipsToBounds = YES;
        [self.contentView addSubview:self.cellIV];
        //
        UIImageView*cellBgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, frame.size.height-(frame.size.width/374.0*165.0), frame.size.width, frame.size.width/374.0*165.0)];
        cellBgIV.image = [UIImage imageNamed:@"pic_kol_cell_bg.png"];
        [self.contentView addSubview:cellBgIV];
        //
        self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, frame.size.height-30.0, frame.size.width, 30.0)];
        self.nameLabel.textAlignment = NSTextAlignmentCenter;
        self.nameLabel.font = font_cu_15;
        self.nameLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        [self.contentView addSubview:self.nameLabel];
	}
	return self;
}

- (void)loadCampaignKolCollectionCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    if([datalist count]!=0) {
        RBUserEntity*userEntity = datalist[indexPath.row];
        [self.cellIV sd_setImageWithURL:[NSURL URLWithString:userEntity.avatar_url] placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"pic_kol_avatar_%d.jpg",arc4random()%4]]];
        self.nameLabel.text = userEntity.name;
    }
}

@end
