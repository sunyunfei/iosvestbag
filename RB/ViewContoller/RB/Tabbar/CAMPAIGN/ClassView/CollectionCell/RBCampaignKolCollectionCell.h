//
//  RBCampaignKolCollectionCell.h
//

#import <UIKit/UIKit.h>
@interface RBCampaignKolCollectionCell : UICollectionViewCell
@property (nonatomic,strong) UIImageView *cellIV;
@property (nonatomic,strong) UILabel*nameLabel;
- (void)loadCampaignKolCollectionCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
@end
