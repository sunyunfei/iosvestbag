//
//  RBCampaignKolView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBCampaignKolCollectionCell.h"

@class RBCampaignKolView;
@protocol RBCampaignKolViewDelegate <NSObject>
@optional
- (void)RBCampaignKolViewLoadRefreshViewData:(RBCampaignKolView *)view;
- (void)RBCampaignKolViewSelected:(RBCampaignKolView *)view andIndex:(int)index;
@end


@interface RBCampaignKolView : UIView
<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>
@property(nonatomic, weak) id<RBCampaignKolViewDelegate> delegate;
@property(nonatomic, strong) UIView*defaultView;
@property(nonatomic, strong) UICollectionView *collectionviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;

- (void)RBCampaignKolViewLoadRefreshViewFirstData;
- (void)RBCampaignKolViewSetRefreshViewFinish;

@end
