//
//  RBCampaignKolView.m
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBCampaignKolView.h"
@interface RBCampaignKolView () {
}
@end

@implementation RBCampaignKolView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        //
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        layout.minimumColumnSpacing = 2;
        layout.minimumInteritemSpacing = layout.minimumColumnSpacing;
        layout.headerHeight = 0;
        layout.footerHeight = 0;
        layout.columnCount = 3;
        self.collectionviewn = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:layout];
        self.collectionviewn.dataSource = self;
        self.collectionviewn.delegate = self;
        [self.collectionviewn registerClass:[RBCampaignKolCollectionCell class]
            forCellWithReuseIdentifier:@"RBKolCollectionCell"];
        self.collectionviewn.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        [self addSubview:self.collectionviewn];
        [self setRefreshHeaderView];
        //
        self.defaultView = [[UIView alloc]initWithFrame:CGRectMake(0, (self.height-120.0)/2.0, ScreenWidth, 120.0)];
        self.defaultView.tag = 9999;
        self.defaultView.hidden = YES;
        [self addSubview:self.defaultView];
        //
        UIImageView*defaultIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-100.0)/2.0, 0, 100.0, 100.0)];
        defaultIV.image = [UIImage imageNamed:@"icon_task_default.png"];
        [self.defaultView addSubview:defaultIV];
        //
        UILabel*defaultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, defaultIV.bottom, ScreenWidth, 20.0)];
        defaultLabel.font = font_cu_17;
        defaultLabel.textAlignment = NSTextAlignmentCenter;
        defaultLabel.text = NSLocalizedString(@"R1028", @"当前页面无数据,试试别的页面吧");
        defaultLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [self.defaultView addSubview:defaultLabel];
    }
    return self;
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)RBCampaignKolViewLoadRefreshViewFirstData {
    self.pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    self.pageSize = 12;
    if ([self.delegate
         respondsToSelector:@selector(RBCampaignKolViewLoadRefreshViewData:)]) {
        [self.delegate RBCampaignKolViewLoadRefreshViewData:self];
    }
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    self.collectionviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    self.collectionviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.pageIndex = self.pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)RBCampaignKolViewSetRefreshViewFinish {
    [self.collectionviewn.mj_header endRefreshing];
    [self.collectionviewn.mj_footer endRefreshing];
    if ([self.datalist count] == (self.pageIndex+1)*self.pageSize) {
        if (self.collectionviewn.mj_footer == nil) {
            [self setRefreshFooterView];
        }
    } else {
        [self.collectionviewn.mj_footer removeFromSuperview];
        self.collectionviewn.mj_footer = nil;
    }
    [UIView performWithoutAnimation:^{
        [self.collectionviewn reloadData];
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.datalist count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RBCampaignKolCollectionCell *cell = (RBCampaignKolCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"RBKolCollectionCell" forIndexPath:indexPath];
    [cell loadCampaignKolCollectionCellWith:self.datalist andIndex:indexPath];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((ScreenWidth-6.0)/3.0, ((ScreenWidth-6.0)/3.0)*450.0/375.0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate
         respondsToSelector:@selector(RBCampaignKolViewSelected:andIndex:)]) {
        [self.delegate RBCampaignKolViewSelected:self andIndex:(int)indexPath.row];
    }
}

@end
