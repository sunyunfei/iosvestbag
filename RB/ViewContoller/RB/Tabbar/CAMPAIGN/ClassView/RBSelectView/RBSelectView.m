//
//  RBSelectView.m
//  RB
//
//  Created by AngusNi on 16/1/28.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBSelectView.h"

@implementation RBSelectView

DEFINE_SINGLETON_FOR_CLASS(RBSelectView);

- (instancetype)init {
    if (self = [super init]) {
        self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
        id<UIApplicationDelegate> delegate  =
        [[UIApplication sharedApplication] delegate];
        if ([delegate respondsToSelector:@selector(window)]) {
            self.window = [delegate performSelector:@selector(window)];
        } else {
            self.window = [[UIApplication sharedApplication] keyWindow];
        }
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.backgroundColor = SysColorCoverDeep;
        //
        UIButton *bgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        bgBtn.frame = self.bounds;
        bgBtn.backgroundColor = [UIColor clearColor];
        [bgBtn addTarget:self
                  action:@selector(hide)
        forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:bgBtn];
        //
        self.tableviewn = [[UITableView alloc]
                           initWithFrame:CGRectMake(45.0, 0, ScreenWidth-45.0*2.0, ScreenHeight)
                           style:UITableViewStylePlain];
        self.tableviewn.dataSource = self;
        self.tableviewn.delegate = self;
        self.tableviewn.backgroundColor = [UIColor whiteColor];
        self.tableviewn.backgroundView = nil;
        self.tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:self.tableviewn];
        [self.tableviewn setSeparatorInset:UIEdgeInsetsZero];
    }
    return self;
}

#pragma mark - RBSelectView Delegate
-(void)showWithData:(NSArray*)data andHeight:(float)height andSelected:(int)tag {
    [self removeFromSuperview];
    [self.window addSubview:self];
    //
    CGAffineTransform oldTransform = self.tableviewn.transform;
    self.tableviewn.transform  =
    CGAffineTransformScale(self.tableviewn.transform, 0.5, 0.5);
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.tableviewn.transform = oldTransform;
    } completion:^(BOOL finished) {
    }];
    //
    self.selected = tag;
    self.datalist = data;
    self.tableviewn.height = height;
    self.tableviewn.top = (ScreenHeight-height)/2.0;
    [UIView performWithoutAnimation:^{
        [self.tableviewn reloadData];
    }];
}

-(void)hide {
    [self removeFromSuperview];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self.datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d",(int)[indexPath section],(int)[indexPath row]];
    UITableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundView = nil;
        cell.backgroundColor = [UIColor clearColor];
        //
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.width, 44.0)];
        textLabel.tag = 10;
        textLabel.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:textLabel];
        //
        UILabel *tipsLabel = [[UILabel alloc] initWithFrame:CGRectMake((tableView.width-30.0)/2.0, 40.0, 30.0, 4.0)];
        tipsLabel.tag = 20;
        tipsLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
        [cell.contentView addSubview:tipsLabel];
        //
        UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 44.0-0.5, tableView.width, 0.5)];
        lineLabel.tag = 30;
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [cell.contentView addSubview:lineLabel];
    }
    UILabel *textLabel = (UILabel*)[cell.contentView viewWithTag:10];
    UILabel *tipsLabel = (UILabel*)[cell.contentView viewWithTag:20];
    tipsLabel.hidden=YES;
    textLabel.font = font_15;
    textLabel.textColor = SysColorCoverDeep;
    if (indexPath.row == self.selected) {
        tipsLabel.hidden = NO;
        textLabel.font = font_cu_15;
        textLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    }
    textLabel.text = self.datalist[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self hide];
    if ([self.delegate
         respondsToSelector:@selector(RBSelectViewSelected:andIndex:)]) {
        [self.delegate RBSelectViewSelected:self andIndex:(int)indexPath.row];
    }
}

@end
