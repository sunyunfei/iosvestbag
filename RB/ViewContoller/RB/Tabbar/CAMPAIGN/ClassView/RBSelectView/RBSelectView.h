//
//  RBSelectView.h
//  RB
//
//  Created by AngusNi on 16/1/28.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"


@class RBSelectView;
@protocol RBSelectViewDelegate <NSObject>
@optional
- (void)RBSelectViewSelected:(RBSelectView *)view andIndex:(int)index;
@end


@interface RBSelectView : UIView
<UITableViewDataSource,UITableViewDelegate>

DEFINE_SINGLETON_FOR_HEADER(RBSelectView);

@property(nonatomic, weak) id<RBSelectViewDelegate> delegate;
@property(nonatomic, strong) NSArray *datalist;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) UIWindow *window;
@property(nonatomic, assign) int selected;

-(void)showWithData:(NSArray*)data andHeight:(float)height andSelected:(int)tag;
-(void)hide;

@end
