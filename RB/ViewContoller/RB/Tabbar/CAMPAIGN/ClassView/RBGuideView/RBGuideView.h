//
//  RBGuideView.h
//  RB
//
//  Created by AngusNi on 5/25/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RBGuideView;
@protocol RBGuideViewDelegate <NSObject>
@optional
- (void)RBGuideView:(RBGuideView *)view clickedButtonAtIndex:(int)buttonIndex;
@end

@interface RBGuideView : UIView
<UIScrollViewDelegate>
@property (nonatomic, weak)  id<RBGuideViewDelegate> delegate;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UIScrollView*pageScroll;
- (void)show;
@end
