//
//  RBGuideView.m
//  RB
//
//  Created by AngusNi on 5/25/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBGuideView.h"
#import "Service.h"

@interface RBGuideView () {
}
@end

@implementation RBGuideView

- (instancetype)init {
    if (self = [super init]) {
        self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
        id<UIApplicationDelegate> delegate  =
        [[UIApplication sharedApplication] delegate];
        if ([delegate respondsToSelector:@selector(window)]) {
            self.window = [delegate performSelector:@selector(window)];
        } else {
            self.window = [[UIApplication sharedApplication] keyWindow];
        }
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    }
    return self;
}

- (void)show {
    [self removeAllSubviews];
    //
    self.pageScroll = [[UIScrollView alloc]
                       initWithFrame:self.bounds];
    self.pageScroll.pagingEnabled = YES;
    self.pageScroll.bounces = NO;
    self.pageScroll.delegate = self;
    self.pageScroll.userInteractionEnabled = YES;
    self.pageScroll.showsHorizontalScrollIndicator = NO;
    self.pageScroll.showsVerticalScrollIndicator = NO;
    self.pageScroll.contentSize = CGSizeMake(self.pageScroll.width * 2, self.pageScroll.height);
    [self addSubview:self.pageScroll];
    //
    for (int i=0; i<2; i++) {
        UIButton*btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(self.pageScroll.width*i, 0, self.pageScroll.width, self.pageScroll.height);
        [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"pic_kol_guide_%d.jpg",i]] forState:UIControlStateNormal];
        btn.tag = 1000+i;
        if (i==1) {
            [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        [self.pageScroll addSubview:btn];
    }
    //
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = self.pageScroll.transform;
        self.pageScroll.transform  =
        CGAffineTransformScale(self.pageScroll.transform, 0.5, 0.5);
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.pageScroll.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }
}

#pragma mark - UIButton Delegate
- (void)btnAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(RBGuideView:clickedButtonAtIndex:)]) {
        [self.delegate RBGuideView:self clickedButtonAtIndex:(int)sender.tag-1000];
    }
    [LocalService setRBLocalDataUserVersionShowWith:[Utils getCurrentBundleShortVersion]];
    [self removeFromSuperview];
}



@end
