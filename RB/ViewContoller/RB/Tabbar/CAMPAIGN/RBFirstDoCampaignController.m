//
//  RBFirstDoCampaignController.m
//  RB
//
//  Created by RB8 on 2018/4/28.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBFirstDoCampaignController.h"

@interface RBFirstDoCampaignController (){
    UIScreenEdgePanGestureRecognizer *sepgr;
    UIScrollView*subScrollView;
    UIImageView*bgIV;
    UIScrollView*scrollviewn;
    UIWebView*webviewn;
    UIView*webViewNV;
    UIView*footerView;
    UIButton*footerBtn;
    UIButton*footerRightBtn;
    UILabel*footerLineLabel;
    NSTimer*updateTimer;
    NSDate*tempDate;
    RBActivityEntity*activityEntity;
    UILabel *tagTimeLabel;
    TTTAttributedLabel *tagTextLabel;
    UILabel *tagLabel;
    RBKOLEntity*kolEntity;
    NSString * editString;
    NSString * userName;
    int  count;
    //现在绑定微信的UID
    NSString * wechatUID;
    //记录点击的是微博还是微信分享
    NSUInteger markShare;
    //记录分享进度
    NSString * shareStatus;
    //活动图片
    NSString * imageURL;
    //
    UIImageView * markImageView;
    //
    UIImageView * wordImageView;
    //
    UIButton * coverBtn;
    //
    UIView * backView;
    //
    RBActionSheet *shareActionSheet;
    //分享标题
    NSString * shareTitle;
    //分享内容
    NSString * shareDsp;
    //分享图片
    NSString * shareImgUrl;
    //分享链接
    NSString * shareCampaignURL;
    //
    UIImageView * markShareImageView;
    //
    UIImageView * wordShareImageView;
    
    
}
@end

@implementation RBFirstDoCampaignController
- (void)RBNavLeftBtnAction{
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController popViewControllerAnimated:YES];
}
- (RBActionSheet*)shareActionSheet{
    if (!shareActionSheet) {
        shareActionSheet = [[RBActionSheet alloc]init];
        shareActionSheet.delegate = self;
        shareActionSheet.tag = 12000;
        [shareActionSheet showWithArray:@[NSLocalizedString(@"R2034", @"上传截图"),NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R1011", @"取消")]];
        shareActionSheet.backgroundColor = [UIColor clearColor];
    }
    return shareActionSheet;
}
#pragma mark - UIButton Delegate
- (void)navLeftBtnAction:(UIButton *)sender {
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)edgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender {
    [self navLeftBtnAction:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = @"ROBIN8";
    shareStatus = @"running";
    imageURL = @"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png";
    shareTitle = @"我最近发现一款每天都可以赚钱的APP,您也来试试?";
    shareDsp = @"Robin8基于大数据的社交影响力平台";
    Handler * handler = [Handler shareHandler];
    shareCampaignURL = [NSString stringWithFormat:@"%@invite?inviter_id=%@",handler.postUrl,[LocalService getRBLocalDataUserLoginId]];
    [self loadScrollView];
}
-(void)loadScrollView {
    
    [scrollviewn removeFromSuperview];
    scrollviewn = nil;
    sepgr = nil;
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth , ScreenHeight-45.0)];
    scrollviewn.backgroundColor = [UIColor whiteColor];
    scrollviewn.bounces = YES;
    scrollviewn.pagingEnabled = YES;
    scrollviewn.userInteractionEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.delegate = self;
    [self.view addSubview:scrollviewn];
    if ([LocalService getRBIsIncheck] == YES) {
        scrollviewn.scrollEnabled = NO;
    }
    //
    if (@available(iOS 11.0,*)) {
        scrollviewn.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    //
    //
    subScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-45.0)];
    subScrollView.backgroundColor = [UIColor whiteColor];
    subScrollView.bounces = YES;
    subScrollView.pagingEnabled = NO;
    subScrollView.userInteractionEnabled = YES;
    subScrollView.showsHorizontalScrollIndicator = NO;
    subScrollView.showsVerticalScrollIndicator = NO;
    [scrollviewn addSubview:subScrollView];
    //
    bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth , ScreenWidth*8.0/16.0)];
    bgIV.image = [UIImage imageNamed:@"pic_income_bg"];
    bgIV.userInteractionEnabled = YES;
    [subScrollView addSubview:bgIV];
    //
    UILabel*bgLabel = [[UILabel alloc]initWithFrame:bgIV.bounds];
    bgLabel.backgroundColor = SysColorCover;
    [bgIV addSubview:bgLabel];
    //
    UIView *scrollViewNV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, NavHeight)];
    scrollViewNV.userInteractionEnabled = YES;
    scrollViewNV.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    [bgIV addSubview:scrollViewNV];
    //
    UILabel *navCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(44.0, StatusHeight, self.view.width-44.0*2.0, 44.0)];
    navCenterLabel.text = @"Robin8";
    navCenterLabel.font = font_cu_17;
    navCenterLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    navCenterLabel.textAlignment = NSTextAlignmentCenter;
    navCenterLabel.userInteractionEnabled = YES;
    [scrollViewNV addSubview:navCenterLabel];
    //
    UILabel *navLeftLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, StatusHeight+(44.0-20.0)/2.0, 20.0, 20.0)];
    navLeftLabel.font = font_icon_(20.0);
    navLeftLabel.text = Icon_bar_back;
    navLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    [scrollViewNV addSubview:navLeftLabel];
    //
    UIButton*navLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navLeftBtn.frame = CGRectMake(0, StatusHeight, 44.0, 44.0);
    [navLeftBtn addTarget:self
                   action:@selector(navLeftBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    [scrollViewNV addSubview:navLeftBtn];
    //
    UIButton * navRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navRightBtn.frame = CGRectMake(ScreenWidth-44.0, StatusHeight, 25, 25);
    navRightBtn.centerY = navLeftBtn.centerY;
    [navRightBtn setImage:[UIImage imageNamed:@"inviteFriends"] forState:UIControlStateNormal];
    [scrollViewNV addSubview:navRightBtn];
    if ([LocalService getRBIsIncheck] == YES) {
        navRightBtn.hidden = YES;
    }
    //活动要求按钮
    UIButton * campainBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    campainBtn.backgroundColor = [UIColor clearColor];
    [campainBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    if (ScreenWidth == 320) {
        campainBtn.frame = CGRectMake(scrollviewn.width - 41, bgIV.bottom+5, 24, 24);
    }else{
        campainBtn.frame = CGRectMake(scrollviewn.width - 41, bgIV.bottom+17, 24, 24);
    }
    campainBtn.titleLabel.font = font_cu_15;
    [campainBtn setImage:[UIImage imageNamed:@"RBrequire"] forState:UIControlStateNormal];
    //
    UIButton * campainBtn1 = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth-16, bgIV.bottom+17, 0, 0) title:@"活动要求" hlTitle:nil titleColor:[Utils getUIColorWithHexString:ccColorffc700] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    
    campainBtn1.titleLabel.font = font_12;
    [campainBtn1 sizeToFit];
    if (ScreenWidth == 320) {
        campainBtn1.frame = CGRectMake(ScreenWidth-16-campainBtn1.size.width, bgIV.bottom+5, campainBtn1.size.width, campainBtn1.size.height);
    }else{
        campainBtn1.frame = CGRectMake(ScreenWidth-16-campainBtn1.size.width, bgIV.bottom+17, campainBtn1.size.width, campainBtn1.size.height);
        
    }
    campainBtn.frame = CGRectMake(campainBtn1.left-5-12, campainBtn1.top+(campainBtn1.size.height-12)/2
                                  , 12, 12);
    [subScrollView addSubview:campainBtn1];
    if ([activityEntity.campaign.per_budget_type isEqualToString:@"cpt"]||[activityEntity.campaign.per_budget_type isEqualToString:@"simple_cpi"]||[activityEntity.campaign.per_budget_type isEqualToString:@"post"]) {
        campainBtn.hidden = NO;
        campainBtn1.hidden = NO;
    }else{
        campainBtn.hidden = YES;
        campainBtn1.hidden = YES;
    }
    if ([LocalService getRBIsIncheck] == YES) {
        campainBtn.hidden = YES;
        campainBtn1.hidden = YES;
        
    }
    [subScrollView addSubview:campainBtn];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, bgIV.bottom+45.0, ScreenWidth - 32, 18)];
    
    titleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 2;
    titleLabel.text = @"这是一个新手教学活动";
    [titleLabel sizeToFit];
    if (ScreenWidth == 320) {
        titleLabel.font = font_cu_14;
        titleLabel.frame = CGRectMake(16, bgIV.bottom+22, ScreenWidth-32, titleLabel.size.height);
    }else{
        titleLabel.font = font_cu_17;
        titleLabel.frame = CGRectMake(16, bgIV.bottom+45.0, ScreenWidth - 32, titleLabel.size.height);
    }
    [subScrollView addSubview:titleLabel];
    //
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, titleLabel.bottom+6.0, scrollviewn.width, 13)];
    subTitleLabel.font = font_11;
    subTitleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:subTitleLabel];
    subTitleLabel.text = @"Robin8";
    [Utils getUILabel:subTitleLabel withSpacing:6.0];
    //
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subTitleLabel.bottom+6.0, scrollviewn.width, 13)];
    timeLabel.font = font_11;
    timeLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:timeLabel];
    //
    timeLabel.text = [NSString stringWithFormat:@"%@ -  %@",@"2017.10.27",@"2017.12.02 00:00:00"];
    CGSize timeLabelSize = [Utils getUIFontSizeFitW:timeLabel];
    timeLabel.width = timeLabelSize.width+8.0;
    timeLabel.left = (ScreenWidth-timeLabel.width-8.0)/2.0;
    
    
    NSArray * languages = [NSLocale preferredLanguages];
    NSString * currentLanguage = [languages objectAtIndex:0];
    //
    UITextView*contentTV = [[UITextView alloc]initWithFrame:CGRectMake(20, timeLabel.bottom+5.0, scrollviewn.width-20.0*2, subScrollView.height-35.0-64.0-35-60.0-26-0.5-timeLabel.bottom-5.0-5)];
    if([currentLanguage isEqualToString:@"en-CN"]){
        contentTV.frame = CGRectMake(20, timeLabel.bottom+5.0, scrollviewn.width-20.0*2, subScrollView.height-35.0-64.0-40.0-35.0-14.0-40.0-timeLabel.bottom-10.0);
    }
    contentTV.font = font_13;
    contentTV.text = [contentTV.text stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    contentTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    contentTV.editable = NO;
    contentTV.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:contentTV];
    if ([LocalService getRBIsIncheck] == YES) {
        contentTV.hidden = YES;
    }
    contentTV.text = @"这是一个新手教学活动，帮助您更好的使用robin8，欢迎关注robin8的微信公众号,我们会不定时带来更多活动供您转发";
    CGSize contentTVSize = [Utils getUITextViewSizeFitH:contentTV withLineSpacing:6.0];
    if (contentTVSize.height+6.0*3.0 < contentTV.height) {
        contentTV.height = contentTVSize.height+6.0*3.0;
    }
    
    // 底部框线条
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subScrollView.height-35.0-64.0-35-60.0-26-0.5, scrollviewn.width, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [subScrollView addSubview:lineLabel];
    //
    tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subScrollView.height-35.0-64.0-30.0-60.0-26, scrollviewn.width, 35.0)];
    tagLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    if ([currentLanguage isEqualToString:@"en-CN"]) {
        tagLabel.numberOfLines = 2;
        tagLabel.font = font_cu_13;
        tagLabel.textAlignment = NSTextAlignmentCenter;
    }else{
        tagLabel.font = font_cu_15;
        tagLabel.numberOfLines = 1;
        tagLabel.textAlignment = NSTextAlignmentCenter;
    }
    [subScrollView addSubview:tagLabel];
    if ([LocalService getRBIsIncheck] == YES) {
        tagLabel.hidden = YES;
    }
    //
    UIView*tagView = [[UIImageView alloc] initWithFrame:CGRectMake(20, subScrollView.height-64.0-20.0-60.0-26, ScreenWidth-2*20.0, 64.0)];
    tagView.backgroundColor = [Utils getUIColorWithHexString:SysColorSubBlack];
    [subScrollView addSubview:tagView];
    if ([LocalService getRBIsIncheck] == YES) {
        tagView.hidden = YES;
    }
    //
    tagTextLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, (tagView.height-18.0-15.0-5.0)/2.0, tagView.width, 18.0)];
    tagTextLabel.font = font_cu_15;
    tagTextLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    tagTextLabel.textAlignment = NSTextAlignmentCenter;
    [tagView addSubview:tagTextLabel];
    if ([LocalService getRBIsIncheck] == YES) {
        tagTextLabel.hidden = YES;
    }
    //
    tagTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, tagTextLabel.bottom+5.0, tagView.width, 15)];
    tagTimeLabel.font = font_11;
    tagTimeLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
    tagTimeLabel.textAlignment = NSTextAlignmentCenter;
    [tagView addSubview:tagTimeLabel];
    //
    UIButton*kolBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    kolBtn.frame = CGRectMake(tagView.left, subScrollView.height-10.0-60.0-26, tagView.width, 60.0);
    [subScrollView addSubview:kolBtn];
    //
    UILabel*kolLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kolBtn.width, 15)];
    kolLabel.font = font_11;
    kolLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    kolLabel.textAlignment = NSTextAlignmentCenter;
    [kolBtn addSubview:kolLabel];
    kolLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R4014", @"已有%@人参加"),@"3000"];
    //
    float temw = 40.0;
    float temg = 18.0;
    if(ScreenHeight<=568) {
        temw = 35.0;
        temg = 15.0;
    }
    int num = 25;
    num = num+1;
    if (num>5) {
        num=5;
    }
    float temleft = (kolBtn.width-temw*num-temg*(num-1))/2.0;
    for (int i=0; i<num; i++) {
        if(i==(num-1)) {
            UILabel*kolIVLabel = [[UILabel alloc] initWithFrame:CGRectMake(temleft+(temw+temg)*i, kolLabel.bottom+5.0, temw, temw)];
            kolIVLabel.font = font_icon_(temw-1);
            kolIVLabel.text = Icon_btn_more;
            kolIVLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            [kolBtn addSubview:kolIVLabel];
        } else {
            RBUserEntity*userEntity = activityEntity.invitees[i];
            UIImageView*kolIV = [[UIImageView alloc]initWithFrame:CGRectMake(temleft+(temw+temg)*i, kolLabel.bottom+5.0, temw, temw)];
            [kolIV sd_setImageWithURL:[NSURL URLWithString:userEntity.avatar_url] placeholderImage:PlaceHolderUserImage];
            if (userEntity.avatar_url.length==0) {
                kolIV.layer.borderWidth = 1.0f;
                kolIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
            }
            kolIV.layer.cornerRadius = kolIV.width/2.0;
            kolIV.layer.masksToBounds = YES;
            [kolBtn addSubview:kolIV];
        }
    }
    // 活动详情
    UIButton*detailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    detailBtn.frame = CGRectMake(0, subScrollView.height-26.0, ScreenWidth, 26.0);
    [subScrollView addSubview:detailBtn];
    //
    UILabel *btnLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 13.0)];
    btnLabel.font = font_11;
    btnLabel.text = NSLocalizedString(@"R2026", @"推广内容详情");
    btnLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    btnLabel.textAlignment=NSTextAlignmentCenter;
    [detailBtn addSubview:btnLabel];
    if ([LocalService getRBIsIncheck] == YES) {
        btnLabel.hidden = YES;
        detailBtn.hidden = YES;
    }
    //
    UILabel*downLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth-13.0)/2.0, btnLabel.bottom, 13.0, 13.0)];
    downLabel.font = font_icon_(13.0);
    downLabel.text = Icon_btn_down;
    downLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [detailBtn addSubview:downLabel];
    //
    [self updateText];
    [self updateFooterView];
    //
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth,ScreenHeight)];
    imageView.image = [UIImage imageNamed:@"RBShareDim"];
    imageView.userInteractionEnabled = YES;
    imageView.tag = 1000;
    UIButton * shareButton = [[UIButton alloc]initWithFrame:CGRectMake(0, ScreenHeight - 45.0, ScreenWidth, 45.0) title:@"" hlTitle:nil titleColor:nil hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    [shareButton addTarget:self action:@selector(PressBtn:) forControlEvents:UIControlEventTouchUpInside];
    shareButton.tag = 1001;
    [imageView addSubview:shareButton];
    [self.view addSubview:imageView];
}
//
- (void)PressBtn:(id)sender{
    UIImageView * imageView = [self.view viewWithTag:1000];
    imageView.hidden = YES;
    if (!markShareImageView) {
        markShareImageView = [[UIImageView alloc]initWithFrame:CGRectMake(43.5, ScreenHeight - 188 - 123, 104, 123)];
        markShareImageView.image = [UIImage imageNamed:@"RBcover6"];
        wordShareImageView = [[UIImageView alloc]initWithFrame:CGRectMake(markShareImageView.right, markShareImageView.top - 13.5, 113, 27)];
        wordShareImageView.image = [UIImage imageNamed:@"RBcover7"];
        UIWindow * window = [[UIApplication sharedApplication].windows lastObject];
        [window addSubview:markShareImageView];
        [window addSubview:wordShareImageView];
    }else{
        markShareImageView.hidden = NO;
        wordShareImageView.hidden = NO;
    }
    //
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.delegate = self;
    alert.tag = 1009;
    [alert showWithCampainTopArray:@[@"朋友圈",@"微信"] AndbottomArray:@[@"icon_wechat_friends",@"icon_wechat"] AndShareWith:@"campain"];
}
-(void)updateText {
    
    NSString*per_budget_type = [NSString stringWithFormat:@"%@",activityEntity.campaign.per_budget_type];
    NSString*per_action_budget = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:activityEntity.campaign.per_action_budget]];
    NSString*status = [NSString stringWithFormat:@"%@",activityEntity.status];
    NSString*avail_click = [NSString stringWithFormat:@"%@",activityEntity.avail_click];
    NSString*earn_money = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:activityEntity.earn_money]];
    per_budget_type = @"click";
    per_action_budget = @"0.5";
    status = @"running";
    avail_click = @"0";
    earn_money = @"0";
    tagLabel.text = [NSString stringWithFormat:@"%@%@%@%@",NSLocalizedString(@"R2502",@"根据你的历史表现，你可单次"),NSLocalizedString(@"R2301",@"任务"),NSLocalizedString(@"R2503",@"赚到：¥ "),per_action_budget];
    tagTextLabel.frame = CGRectMake(0, 0, ScreenWidth-2*20.0, 64.0);
    tagTimeLabel.hidden = YES;
    if ([shareStatus isEqualToString:@"running"]) {
        tagTimeLabel.hidden = NO;
        tagTextLabel.frame = CGRectMake(0, (64.0-18.0-15.0-5.0)/2.0, ScreenWidth-2*20.0, 18.0);
        tagTimeLabel.text = @"一天后";
         tagTextLabel.text = NSLocalizedString(@"R2302", @"分享后完成指定任务立即获得报酬");
    }else if([shareStatus isEqualToString:@"approved"]){
        //参与了开始或者结束
        tagTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R2508", @"活动进行中,即将赚￥%@"),earn_money];
    }else if ([shareStatus isEqualToString:@"settled"]){
        tagTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R2507", @"已完成,已赚￥%@"),earn_money];
    }
    [tagTextLabel setText:tagTextLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:avail_click options:NSCaseInsensitiveSearch];
         NSRange range1 = [[mStr string] rangeOfString:[NSString stringWithFormat:@"￥"] options:NSCaseInsensitiveSearch];
         NSRange range2 = [[mStr string] rangeOfString:earn_money options:NSCaseInsensitiveSearch];
         CTFontRef font = CTFontCreateWithName((CFStringRef)font_cu_17.fontName, font_cu_17.pointSize, NULL);
         [mStr addAttributes:@{(NSString *)kCTFontAttributeName:(__bridge id)font,(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range];
         [mStr addAttributes:@{(NSString *)kCTFontAttributeName:(__bridge id)font,(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range2];
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range1];
         CFRelease(font);
         return mStr;
     }];
}
//
- (void)updateFooterView {
    [footerView removeFromSuperview];
    footerView = nil;
    //
    footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
    //
    footerRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerRightBtn.frame = CGRectMake(footerView.width/2.0, 0, footerView.width/2.0, footerView.height);
    footerRightBtn.titleLabel.font = font_cu_15;
    [footerRightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerRightBtn];
    //
    footerLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(footerView.width/2.0-0.5f, 10.0, 1, 45.0-20.0)];
    footerLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [footerView addSubview:footerLineLabel];
    //
    [self updateText];
    footerBtn.enabled = NO;
    footerBtn.frame = CGRectMake(0, 0, ScreenWidth, 45.0);
    footerRightBtn.frame = CGRectMake(ScreenWidth/2.0, 0, ScreenWidth/2.0, 45.0);
    footerRightBtn.hidden = YES;
    footerLineLabel.hidden = YES;

    if ([shareStatus isEqualToString:@"running"]) {
        [footerBtn setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R1102", @"分享赚收益")]forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
        footerBtn.enabled = YES;
        [footerBtn addTarget:self
                      action:@selector(footerShareApproveBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
    }else if ([shareStatus isEqualToString:@"approved"]){
        UIImageView * imageView = [self.view viewWithTag:1000];
        [imageView removeFromSuperview];
        //
        backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
        backView.backgroundColor = SysColorCoverDeep;
        coverBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, ScreenHeight - 45, ScreenWidth/2, 45)];
        [coverBtn setImage:[UIImage imageNamed:@"RBcover1"] forState:UIControlStateNormal];
        [coverBtn addTarget:self action:@selector(footerUploadBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [backView addSubview:coverBtn];
        //
        markImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/5, ScreenHeight - 45 - 143, 74, 143)];
        markImageView.image = [UIImage imageNamed:@"RBcover3"];
        [backView addSubview:markImageView];
        //
        wordImageView = [[UIImageView alloc]initWithFrame:CGRectMake(markImageView.right, markImageView.top - 14, 112, 28)];
        wordImageView.image = [UIImage imageNamed:@"RBcover4"];
        [backView addSubview:wordImageView];
        UIWindow * window = [[UIApplication sharedApplication].windows lastObject];
        [window addSubview:backView];
        
        
        
        
        [footerBtn setTitle:NSLocalizedString(@"R2034", @"上传截图") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
        footerBtn.enabled = YES;

        //
        footerBtn.frame = CGRectMake(0, 0, ScreenWidth/2.0, 45.0);
        footerRightBtn.frame = CGRectMake(ScreenWidth/2.0, 0, ScreenWidth/2.0, 45.0);
        footerRightBtn.hidden = NO;
        footerLineLabel.hidden = NO;
        [footerRightBtn setTitle:NSLocalizedString(@"R2035", @"再次分享") forState:UIControlStateNormal];
        footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];

    }else if ([shareStatus isEqualToString:@"settled"]){
       NSString * temp = NSLocalizedString(@"R2039", @"审核通过");
        [footerRightBtn setTitle:NSLocalizedString(@"R2035", @"再次分享") forState:UIControlStateNormal];
        [footerRightBtn addTarget:self
                           action:@selector(footerShareApproveBtnAction:)
                 forControlEvents:UIControlEventTouchUpInside];
        [footerBtn setTitle:[NSString stringWithFormat:@"%@-%@",temp,NSLocalizedString(@"R2042", @"查看截图")] forState:UIControlStateNormal];
        footerBtn.frame = CGRectMake(0,0, ScreenWidth/2.0, 45.0);
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
        footerBtn.enabled = YES;
        footerLineLabel.hidden = NO;
        footerRightBtn.frame = CGRectMake(ScreenWidth/2.0,0, ScreenWidth/2.0, 45.0);
        footerRightBtn.hidden = NO;
        footerRightBtn.enabled = YES;
        footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
    }
}
- (void)footerShareApproveBtnAction:(UIButton*)sender{
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.delegate = self;
    alert.tag = 1009;
    [alert showWithCampainTopArray:@[@"朋友圈",@"微信"] AndbottomArray:@[@"icon_wechat_friends",@"icon_wechat"] AndShareWith:@"campain"];
    
}
//验证用户是否绑定了微信
-(void)verifyWechat{
    if(![JsonService isRBUserVisitor]) {
        [self.hudView show];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBKOLDetailWithKolId:[LocalService getRBLocalDataUserLoginId]];
    }
}
- (void)updateFrame{
    
}
#pragma mark - RBNewAlert Delegate
- (void)RBNewAlertTapAction:(RBNewAlert *)alertView{
    if (alertView.tag == 1009) {
//        UIImageView * imageView = [self.view viewWithTag:1000];
//        imageView.hidden = NO;
    }else if (alertView.tag == 1222){
//         [self updateFooterView];
    }else if (alertView.tag == 1223){

    }else if (alertView.tag == 1224){
    }
}
-(void)RBNewAlertCancelButtonAction:(RBNewAlert *)alertView{
    if (alertView.tag == 1009) {
        markShareImageView.hidden = YES;
        wordShareImageView.hidden = YES;
        UIImageView * imageView = [self.view viewWithTag:1000];
        imageView.hidden = NO;
    }else if (alertView.tag == 1222){
         [self updateFooterView];
    }else if (alertView.tag == 1223){
        backView.hidden = NO;
        shareStatus = @"upload";
        coverBtn.frame = CGRectMake(0, ScreenHeight - 45, ScreenWidth/2, 45);
        [coverBtn setImage:[UIImage imageNamed:@"RBcover1"] forState:UIControlStateNormal];
        [coverBtn addTarget:self action:@selector(footerUploadBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        //
        markImageView.frame = CGRectMake(ScreenWidth/5, ScreenHeight - 45 - 143, 74, 143);
        markImageView.image = [UIImage imageNamed:@"RBcover3"];
        //
        wordImageView.frame = CGRectMake(markImageView.right, markImageView.top - 14, 112, 28);
        wordImageView.image = [UIImage imageNamed:@"RBcover4"];
    }else if (alertView.tag == 1224){
#pragma mark 完成全部任务
        Handler * hand = [Handler shareHandler];
        hand.delegate = self;
        [hand getRBFirstDoCampaigngetTheAward];
    }
}
-(void)RBNewAlert:(RBNewAlert *)alertView clickedButtonAtIndex:(int)buttonIndex{
    if (alertView.tag == 1009) {
        if (buttonIndex == 0){
            markShareImageView.hidden = YES;
            wordShareImageView.hidden = YES;
            markShare = SSDKPlatformSubTypeWechatTimeline;
            [self verifyWechat];
        }else if (buttonIndex == 1){
            markShareImageView.hidden = YES;
            wordShareImageView.hidden = YES;
            markShare = SSDKPlatformSubTypeWechatSession;
            [self verifyWechat];
        }
    }else if(alertView.tag == 1222){
        
    }else if (alertView.tag == 1223){
        
    }else if (alertView.tag == 1224){
    }
}
#pragma mark - ShareSDK Delegate
- (void)shareSDKWithTitle:(NSString *)titleStr
                  Content:(NSString *)contentStr
                      URL:(NSString *)urlStr
                   ImgURL:(NSString *)imgUrlStr
         SSDKPlatformType:(SSDKPlatformType)type {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = contentStr;
    if(type == SSDKPlatformTypeSinaWeibo) {
        
        contentStr = [NSString stringWithFormat:@"#Robin8#   「%@「%@」」",titleStr,urlStr];
    }
    [self.hudView show];
    NSLog(@"title:%@,content:%@,img:%@,url:%@",titleStr,contentStr,imgUrlStr,urlStr);
    __weak __typeof(self) weakSelf = self;
    [bgIV setImageWithURL:[NSURL URLWithString:imgUrlStr] placeholder:PlaceHolderImage options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        if(error!=nil) {
            [weakSelf.hudView showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
            UIImageView * imageView = [weakSelf.view viewWithTag:1000];
            imageView.hidden = NO;
            return ;
        } else {
            image = [Utils narrowWithImage:image];
        }
        // shareSDK
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        if(type == SSDKPlatformTypeSinaWeibo) {
            [shareParams SSDKEnableUseClientShare];
            
            [shareParams SSDKSetupShareParamsByText:contentStr
                                             images:imgUrlStr
                                                url:[NSURL URLWithString:urlStr]
                                              title:titleStr
                                               type:SSDKContentTypeAuto];
            //SSDKContentTypeAuto
        } else {
            [shareParams SSDKSetupShareParamsByText:contentStr
                                             images:imgUrlStr
                                                url:[NSURL URLWithString:urlStr]
                                              title:titleStr
                                               type:SSDKContentTypeWebPage];
        }
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            NSLog(@"error:%@",error);
            if (state == SSDKResponseStateSuccess) {
                RBNewAlert * alert = [[RBNewAlert alloc]init];
                alert.tag = 1222;
                alert.delegate = weakSelf;
                NSString * str = @"分享成功,0.5元奖励已放入您的钱包!\n继续完成任务,可获得更多奖励哦~";
                NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:str];
                NSRange range = [str rangeOfString:@"0.5"];
                [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorBlue] range:range];
                [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, attStr.length)];
                [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:range];
                NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
                style.lineSpacing = 10;
                [attStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, attStr.length)];
                [alert successSignIn:attStr AndTitle:@"分享成功"];
                shareStatus = @"approved";
            }
            if (state == SSDKResponseStateFail) {
                UIImageView * imageView = [weakSelf.view viewWithTag:1000];
                imageView.hidden = NO;
                if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                    [weakSelf.hudView showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                } else {
                    [weakSelf.hudView showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
                }
            }
            if (state == SSDKResponseStateCancel) {
                UIImageView * imageView = [weakSelf.view viewWithTag:1000];
                imageView.hidden = NO;
                [weakSelf.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
            }
        }];
    }];
}

- (void)footerUploadBtnAction:(UIButton *)sender {
    if([self isVisitorLogin]==YES){
        return;
    };
    [self shareActionSheet];
    //
    coverBtn.frame = CGRectMake(0, ScreenHeight - 105, ScreenWidth, 55);
    [coverBtn removeTarget:self action:@selector(footerUploadBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [coverBtn addTarget:self action:@selector(exampleScreenShoot) forControlEvents:UIControlEventTouchUpInside];
    markImageView.frame = CGRectMake(ScreenWidth/2 - 20, ScreenHeight - 100 - 143, 74, 143);
    wordImageView.frame = CGRectMake(markImageView.right, markImageView.top - 13, 111, 27);
    [coverBtn setImage:[UIImage imageNamed:@"RBExampleScreenShoot"] forState:UIControlStateNormal];
    wordImageView.image = [UIImage imageNamed:@"RBWatchScreenShoot"];
    //
    
    if ([shareStatus isEqualToString:@"upload"]) {
        shareActionSheet.hidden = NO;
        [coverBtn removeTarget:self action:@selector(exampleScreenShoot) forControlEvents:UIControlEventTouchUpInside];
        [coverBtn addTarget:self action:@selector(uploadImage) forControlEvents:UIControlEventTouchUpInside];
        coverBtn.frame = CGRectMake(0, ScreenHeight - 160, ScreenWidth, 55);
        markImageView.frame = CGRectMake(ScreenWidth/2 - 20, ScreenHeight - 150 - 143, 74, 143);
        wordImageView.frame = CGRectMake(markImageView.right, markImageView.top - 26, 80, 52);
        wordImageView.image = [UIImage imageNamed:@"RBcover2"];
        [coverBtn setImage:[UIImage imageNamed:@"RBcover1"] forState:UIControlStateNormal];
    }
    UIWindow * window = [[UIApplication sharedApplication].windows lastObject];
    [window bringSubviewToFront:backView];
}
#pragma mark - 查看截图示例返回触发的代理方法
- (void)RBUploadImageViewControllerDelegateAction{
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.tag = 1223;
    alert.delegate = self;
    NSString * str = @"查看截图示例任务完成,0.50元奖励已放入您的钱包!继续完成任务,可获得更多奖励哦~";
    NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:str];
    NSRange range = [str rangeOfString:@"0.50"];
    [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorBlue] range:range];
    [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, attStr.length)];
    [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:range];
    NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
    style.lineSpacing = 10;
    [attStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, attStr.length)];
    [alert successSignIn:attStr AndTitle:@"查看截图示例"];
}
#pragma mark - 上传截图或示例截图
- (void)pushUploadController:(NSString*)mark{
    RBUploadImageViewController * upView = [[RBUploadImageViewController alloc]init];
    upView.delegateC = self;
    upView.hidesBottomBarWhenPushed = YES;
    upView.mark = mark;
    upView.nameArr =@[@"示例截图"];
    upView.imgArr = @[@"pic_task_screenshot"];
    [self.navigationController pushViewController:upView animated:YES];
}

//截图参考
- (void)exampleScreenShoot{
    backView.hidden = YES;
    shareActionSheet.hidden = YES;
    [coverBtn removeTarget:self action:@selector(exampleScreenShoot) forControlEvents:UIControlEventTouchUpInside];
    [self pushUploadController:@"first"];
}
- (void)RBPictureView:(RBPictureView *)actionSheet clickedButtonAtIndex:(int)buttonIndex{
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.tag = 1223;
    alert.delegate = self;
    NSString * str = @"查看截图示例任务完成,0.50元奖励已放入您的钱包!继续完成任务,可获得更多奖励哦~";
    NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:str];
    NSRange range = [str rangeOfString:@"0.50"];
    [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorBlue] range:range];
    [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, attStr.length)];
    [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:range];
    NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
    style.lineSpacing = 10;
    [attStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, attStr.length)];
    [alert successSignIn:attStr AndTitle:@"查看截图示例"];
}
//点击按钮选择图片
- (void)uploadImage{
    backView.hidden = YES;
    shareActionSheet.hidden = YES;
    ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
    if (author == ALAuthorizationStatusDenied) {
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.alertBody = @"照片不可用,请设置打开";
        [[UIApplication sharedApplication]
         presentLocalNotificationNow:notification];
        backView.hidden = NO;
        shareActionSheet.hidden = NO;
        return;
    }
    RBPhotoAsstesGroupViewController*toview = [[RBPhotoAsstesGroupViewController alloc]init];
    toview.selectNumber = 1;
    toview.delegateC = self;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}
//选择图片回来以后得代理方法
#pragma RBPhotoAsstesGroupViewControllerDelegate
- (void)RBPhotoCancelUpload{
    backView.hidden = NO;
    shareActionSheet.hidden = NO;
}
- (void)RBPhotoAsstesGroupViewController:(RBPhotoAsstesGroupViewController *)vc selectedList:(NSMutableArray *)list {
    // 图片压缩处理
    [self.hudView showSuccessWithStatus:@"上传图片成功"];
    shareStatus = @"settled";
    [backView removeFromSuperview];
    [shareActionSheet removeFromSuperview];
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.tag = 1224;
    alert.delegate = self;
    NSString * str = @"上传截图成功,1.00元奖励已放入您的钱包!新手任务已全部完成~";
    NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:str];
    NSRange range = [str rangeOfString:@"1.00"];
    [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorBlue] range:range];
    [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, attStr.length)];
    [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:range];
    NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
    style.lineSpacing = 10;
    [attStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, attStr.length)];
    [alert successSignIn:attStr AndTitle:@"任务完成"];
}
#pragma mark - RBActionSheet Delegate
    - (void)ThirdLoginWithTag:(int)tag {
        SSDKPlatformType type = SSDKPlatformTypeWechat;
        NSString*provider=@"wechat";
        [ShareSDK cancelAuthorize:type];
        [ShareSDK getUserInfo:type
               onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error){
                   NSLog(@"user:%@,credential:%@,rawData:%@",user,user.credential,user.rawData);
                   if (state == SSDKResponseStateSuccess) {
                       userName = [NSString stringWithFormat:@"%@",user.nickname];
                       // RB-社交账号绑定
                       Handler*handler = [Handler shareHandler];
                       handler.delegate = self;
                       if (type == SSDKPlatformTypeWechat) {
                           NSString*unionid = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"unionid"]];
                           NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                           editString = @"微信";
                           // socialBtn.iconIV.image = [UIImage imageNamed:@"微信_ON.png"];
                           [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:unionid andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                       }
                   }
                   if (state == SSDKResponseStateFail) {
                       if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                           //   [self.hudView showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                       } else {
                           [self.hudView showErrorWithStatus:NSLocalizedString(@"R1042", @"登录失败")];
                       }
                   }
                   if (state == SSDKResponseStateCancel) {
                       [self.hudView showErrorWithStatus:@"取消分享"];
                       // [self.hudView dismiss];
                   }
               }];
        
    }
//进行微信绑定
-(void)bindWechat{
    [self ThirdLoginWithTag:1];
}
//分享
-(void)shareCampaign:(NSUInteger)type{
    [self shareSDKWithTitle:shareTitle Content:shareDsp URL:shareCampaignURL ImgURL:imageURL SSDKPlatformType:type];
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    //验证用户是否绑定了微信
    if ([sender isEqualToString:@"big_v/detail"]) {
        kolEntity = [JsonService getRBKolDetailEntity:jsonObject];
        //记录是否绑定微信
        BOOL isBinding = NO;
        for (RBKOLSocialEntity * socialEntity in kolEntity.social_accounts) {
            if ([socialEntity.provider isEqualToString:@"wechat"]) {
                isBinding = YES;
            }
        }
        if (isBinding == YES) {
            [self.hudView dismiss];
            //直接分享
            if (markShare == SSDKPlatformSubTypeWechatTimeline) {
                [self shareSDKWithTitle:shareTitle Content:shareDsp URL:shareCampaignURL ImgURL:imageURL SSDKPlatformType:SSDKPlatformSubTypeWechatTimeline];
            }else if (markShare == SSDKPlatformSubTypeWechatSession){
                [self shareSDKWithTitle:shareTitle Content:shareDsp URL:shareCampaignURL ImgURL:imageURL SSDKPlatformType:SSDKPlatformSubTypeWechatSession];
            }
        }else{
            //用户还没有绑定微信；
//            UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth,ScreenHeight)];
//            imageView.image = [UIImage imageNamed:@"RBShareDim"];
//            imageView.userInteractionEnabled = YES;
//            imageView.tag = 1000;
//            UIButton * shareButton = [[UIButton alloc]initWithFrame:CGRectMake(0, ScreenHeight - 45.0, ScreenWidth, 45.0) title:@"" hlTitle:nil titleColor:nil hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
//            [shareButton addTarget:self action:@selector(PressBtn:) forControlEvents:UIControlEventTouchUpInside];
//            shareButton.tag = 1001;
//            [imageView addSubview:shareButton];
//            [self.view addSubview:imageView];
            UIImageView * imageView = [self.view viewWithTag:1000];
            imageView.hidden = NO;
            [self bindWechat];
        }
    }
    
    
    if([sender isEqualToString:@"kols/identity_bind"]){
        [self.hudView show];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBKOLApplyWithProvider:editString andHomepage:nil andUsername:userName andPrice:@"1" andFollowers:@"10" andScreenshot:nil andUid:nil];
    }
    if ([sender isEqualToString:@"update_social"]) {
        UIImageView * imageView = [self.view viewWithTag:1000];
        imageView.hidden = YES;
        [self shareCampaign:markShare];
        [self.hudView dismiss];
    }
    if ([sender isEqualToString:@"finish_newbie"]) {
        [LocalService setRBIsNewMember:@"0"];
        [self RBNavLeftBtnAction];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
