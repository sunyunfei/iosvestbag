//
//  RBCampaignDetailViewController.m
//  RB
//
//  Created by AngusNi on 16/1/28.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBCampaignDetailViewController.h"
#import "RBUploadImageViewController.h"
#import "RBWatchImageController.h"
@interface RBCampaignDetailViewController () {
    UIScreenEdgePanGestureRecognizer *sepgr;
    UIScrollView*subScrollView;
    UIImageView*bgIV;
    UIScrollView*scrollviewn;
    UIWebView*webviewn;
    UIView*webViewNV;
    UIView*footerView;
    UIButton*footerBtn;
    UIButton*footerRightBtn;
    UILabel*footerLineLabel;
    NSTimer*updateTimer;
    NSDate*tempDate;
    RBActivityEntity*activityEntity;
    UILabel *tagTimeLabel;
    TTTAttributedLabel *tagTextLabel;
    UILabel *tagLabel;
    RBKOLEntity*kolEntity;
    NSString * editString;
    NSString * userName;
    int  count;
    //现在绑定微信的UID
    NSString * wechatUID;
    //记录点击的是微博还是微信分享
    NSUInteger markShare;
}
@end

@implementation RBCampaignDetailViewController

-(void)removeImageView:(UITapGestureRecognizer*)tap{
    UIView * tapView = (UIView*)tap.view;
    [tapView removeFromSuperview];
   
    //
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth,ScreenHeight)];
    imageView.image = [UIImage imageNamed:@"RBShareDim"];
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeImageView1:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [imageView addGestureRecognizer:tap1];
    UIWindow * window = [[UIApplication sharedApplication].windows lastObject];
    [window addSubview:imageView];
}
-(void)removeImageView1:(UITapGestureRecognizer*)tap{
    UIImageView * tapView = (UIImageView*)tap.view;
    [tapView removeFromSuperview];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = @"ROBIN8";
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getActivityData) name:@"refreshCampainDetail" object:nil];
    count = 0;
    [self getActivityData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
    [self changeStatusBar];
    //
    [TalkingData trackPageBegin:@"campaign-detail"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [updateTimer invalidate];
    updateTimer = nil;
 
    //
    [TalkingData trackPageEnd:@"campaign-detail"];
}
#pragma mark - BaseDelegate
-(void)navRightBtnAction:(id)sender{

    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.delegate = self;
    
    if (activityEntity.leader_club.length >0) {
        [alert showWithCampainTopArray:@[@"微博",@"微信",@"朋友圈",@"QQ好友",@"QQ空间"] AndbottomArray:@[@"icon_weibo",@"icon_wechat",@"icon_wechat_friends",@"icon_qq",@"icon_qzone"] AndShareWith:@"leader"];
        alert.shareTitle = activityEntity.campaign.name;
        alert.shareContent = activityEntity.campaign.idescription;
        alert.shareImageUrl = @"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png";
        alert.shareurl = [NSString stringWithFormat:@"%@",activityEntity.campaign.url];
        
        alert.shareH5Title = [NSString stringWithFormat:@"Robin8邀请你成为%@的代言人",activityEntity.campaign.brand_name];
        alert.shareH5Url = [NSString stringWithFormat:@"http://robin8.net/club_campaign/campaign_page?campaign_id=%@&club=%@",self.campaignId,activityEntity.leader_club];
        alert.shareH5Content = [NSString stringWithFormat:@"http://robin8.net/club_campaign/campaign_page?campaign_id=%@&club=%@",self.campaignId,activityEntity.leader_club];
    }else{
        [alert showWithCampainTopArray:@[@"微博",@"微信",@"朋友圈",@"QQ好友",@"QQ空间"] AndbottomArray:@[@"icon_weibo",@"icon_wechat",@"icon_wechat_friends",@"icon_qq",@"icon_qzone"] AndShareWith:@"h5"];
        alert.shareTitle = activityEntity.campaign.name;
        alert.shareContent = activityEntity.campaign.idescription;
        alert.shareImageUrl = @"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png";
        alert.shareurl = [NSString stringWithFormat:@"%@",activityEntity.campaign.url];
    
        alert.shareH5Title = [NSString stringWithFormat:@"Robin8邀请你成为%@的代言人",activityEntity.campaign.brand_name];
        alert.shareH5Url = [NSString stringWithFormat:@"http://robin8.net/wechat_campaign/campaign_page?campaign_id=%@",self.campaignId];
        alert.shareH5Content = [NSString stringWithFormat:@"http://robin8.net/wechat_campaign/campaign_page?campaign_id=%@",self.campaignId];
    }
}
#pragma mark - NSNotification Delegate
- (void)RBNotificationDismissView {
    [self changeStatusBar];
}

#pragma mark - Data Delegate
- (void)getActivityData {
    [self.hudView show];
    // RB-获取活动详情
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    if (self.activityId!=nil) {
        [handler getRBActivityDetailWithId:self.activityId];
    }
    if (self.campaignId!=nil) {
        [handler getRBCampaignDetailWithId:self.campaignId];
    }

}

- (void)getActivityEnjoy {
    [self.hudView show];
    // RB-获取活动参与
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    if (self.campaignId!=nil) {
        [handler getRBCampaignEnjoyWithId:self.campaignId];
    }
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    scrollviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf getActivityData];
    }];
}

- (void)setRefreshViewFinish {
    scrollviewn.contentOffset = CGPointMake(0, 0);
    [scrollviewn.mj_header endRefreshing];
}

#pragma mark - UIScreenEdgePanGestureRecognizer Delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (void)edgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender {
    [self navLeftBtnAction:nil];
}

#pragma mark - ContentView Delegate
-(void)loadScrollView {
    
    [scrollviewn removeFromSuperview];
    scrollviewn = nil;
    sepgr = nil;
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth , ScreenHeight-45.0)];
    scrollviewn.backgroundColor = [UIColor whiteColor];
    scrollviewn.bounces = YES;
    scrollviewn.pagingEnabled = YES;
    scrollviewn.userInteractionEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.delegate = self;
    [self.view addSubview:scrollviewn];
    if ([LocalService getRBIsIncheck] == YES) {
        scrollviewn.scrollEnabled = NO;
    }
    [self setRefreshHeaderView];
    //
    if (@available(iOS 11.0,*)) {
        scrollviewn.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    //
    sepgr = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(edgePanGesture:)];
    sepgr.delegate = self;
    sepgr.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:sepgr];
    //
    subScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-45.0)];
    subScrollView.backgroundColor = [UIColor whiteColor];
    subScrollView.bounces = YES;
    subScrollView.pagingEnabled = NO;
    subScrollView.userInteractionEnabled = YES;
    subScrollView.showsHorizontalScrollIndicator = NO;
    subScrollView.showsVerticalScrollIndicator = NO;
    [scrollviewn addSubview:subScrollView];
    //
    bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth , ScreenWidth*8.0/16.0)];
//    [bgIV yy_setImageWithURL:[NSURL URLWithString:activityEntity.campaign.img_url] placeholder:PlaceHolderImage options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation completion:NULL];
    [bgIV setImageWithURL:[NSURL URLWithString:activityEntity.campaign.img_url] placeholder:PlaceHolderImage options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation completion:NULL];
    bgIV.userInteractionEnabled = YES;
    [subScrollView addSubview:bgIV];
    //
    UILabel*bgLabel = [[UILabel alloc]initWithFrame:bgIV.bounds];
    bgLabel.backgroundColor = SysColorCover;
    [bgIV addSubview:bgLabel];
    
    // CPI CPA提示
    if (activityEntity.leader_club.length>0) {
        NSArray *textArray = @[@"点击右上角分享按钮将活动分享给你的成员",@"点击右上角分享按钮将活动分享给你的成员",@"点击右上角分享按钮将活动分享给你的成员"];
        YFRollingLabel*rollingLabel = [[YFRollingLabel alloc] initWithFrame:CGRectMake(0, bgIV.bottom-40.0, bgIV.width, 40)  textArray:textArray font:font_cu_15 textColor:[UIColor whiteColor]];
        rollingLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        rollingLabel.speed = 1;
        [rollingLabel setOrientation:RollingOrientationLeft];
        [rollingLabel setInternalWidth:rollingLabel.width/3.0];
        [bgIV addSubview:rollingLabel];
    }
    //
    UIView *scrollViewNV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, NavHeight)];
    scrollViewNV.userInteractionEnabled = YES;
    scrollViewNV.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    [bgIV addSubview:scrollViewNV];
    //
    UILabel *navCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(44.0, StatusHeight, self.view.width-44.0*2.0, 44.0)];
    if ([LocalService getRBIsIncheck] == YES) {
        navCenterLabel.text = @"Robin8";
    }else{
        navCenterLabel.text = [NSString stringWithFormat:@"%@",activityEntity.campaign.brand_name];
    }
    navCenterLabel.font = font_cu_17;
    navCenterLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    navCenterLabel.textAlignment = NSTextAlignmentCenter;
    navCenterLabel.userInteractionEnabled = YES;
    [scrollViewNV addSubview:navCenterLabel];
    //
    UILabel *navLeftLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, StatusHeight+(44.0-20.0)/2.0, 20.0, 20.0)];
    navLeftLabel.font = font_icon_(20.0);
    navLeftLabel.text = Icon_bar_back;
    navLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    [scrollViewNV addSubview:navLeftLabel];
    //
    UIButton*navLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navLeftBtn.frame = CGRectMake(0, StatusHeight, 44.0, 44.0);
    [navLeftBtn addTarget:self
                   action:@selector(navLeftBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    [scrollViewNV addSubview:navLeftBtn];
    //
    UIButton * navRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navRightBtn.frame = CGRectMake(ScreenWidth-44.0, StatusHeight, 25, 25);
    navRightBtn.centerY = navLeftBtn.centerY;
    [navRightBtn addTarget:self action:@selector(navRightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [navRightBtn setImage:[UIImage imageNamed:@"inviteFriends"] forState:UIControlStateNormal];
    [scrollViewNV addSubview:navRightBtn];
    if ([LocalService getRBIsIncheck] == YES) {
        navRightBtn.hidden = YES;
    }
   //活动要求按钮
    UIButton * campainBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    campainBtn.backgroundColor = [UIColor clearColor];
    [campainBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [campainBtn addTarget:self action:@selector(alertShow) forControlEvents:UIControlEventTouchUpInside];
    if (ScreenWidth == 320) {
        campainBtn.frame = CGRectMake(scrollviewn.width - 41, bgIV.bottom+5, 24, 24);
    }else{
        campainBtn.frame = CGRectMake(scrollviewn.width - 41, bgIV.bottom+17, 24, 24);
    }
    campainBtn.titleLabel.font = font_cu_15;
    [campainBtn setImage:[UIImage imageNamed:@"RBrequire"] forState:UIControlStateNormal];
    //
    UIButton * campainBtn1 = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth-16, bgIV.bottom+17, 0, 0) title:@"活动要求" hlTitle:nil titleColor:[Utils getUIColorWithHexString:ccColorffc700] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    
    campainBtn1.titleLabel.font = font_12;
    [campainBtn1 sizeToFit];
    if (ScreenWidth == 320) {
        campainBtn1.frame = CGRectMake(ScreenWidth-16-campainBtn1.size.width, bgIV.bottom+5, campainBtn1.size.width, campainBtn1.size.height);
    }else{
        campainBtn1.frame = CGRectMake(ScreenWidth-16-campainBtn1.size.width, bgIV.bottom+17, campainBtn1.size.width, campainBtn1.size.height);

    }
    campainBtn.frame = CGRectMake(campainBtn1.left-5-12, campainBtn1.top+(campainBtn1.size.height-12)/2
                                  , 12, 12);
    [campainBtn1 addTarget:self action:@selector(alertShow) forControlEvents:UIControlEventTouchUpInside];
    [subScrollView addSubview:campainBtn1];
    if ([activityEntity.campaign.per_budget_type isEqualToString:@"cpt"]||[activityEntity.campaign.per_budget_type isEqualToString:@"simple_cpi"]||[activityEntity.campaign.per_budget_type isEqualToString:@"post"]) {
        campainBtn.hidden = NO;
        campainBtn1.hidden = NO;
    }else{
        campainBtn.hidden = YES;
        campainBtn1.hidden = YES;
    }
    if ([LocalService getRBIsIncheck] == YES) {
        campainBtn.hidden = YES;
        campainBtn1.hidden = YES;
        
    }
    [subScrollView addSubview:campainBtn];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, bgIV.bottom+45.0, ScreenWidth - 32, 18)];
    
    titleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 2;
    titleLabel.text = [NSString stringWithFormat:@"%@",activityEntity.campaign.name];
    [titleLabel sizeToFit];
    if (ScreenWidth == 320) {
        titleLabel.font = font_cu_14;
        titleLabel.frame = CGRectMake(16, bgIV.bottom+22, ScreenWidth-32, titleLabel.size.height);
    }else{
        titleLabel.font = font_cu_17;
        titleLabel.frame = CGRectMake(16, bgIV.bottom+45.0, ScreenWidth - 32, titleLabel.size.height);
    }
    [subScrollView addSubview:titleLabel];
    //
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, titleLabel.bottom+6.0, scrollviewn.width, 13)];
    subTitleLabel.font = font_11;
    subTitleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:subTitleLabel];
    if ([LocalService getRBIsIncheck] == YES) {
        subTitleLabel.text = @"Robin8";
    }else{
        subTitleLabel.text = [NSString stringWithFormat:@"%@",activityEntity.campaign.brand_name];
    }
    [Utils getUILabel:subTitleLabel withSpacing:6.0];
    //
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subTitleLabel.bottom+6.0, scrollviewn.width, 13)];
    timeLabel.font = font_11;
    timeLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:timeLabel];
    //
    if(activityEntity!=nil) {
        NSString *sDate = [[NSString stringWithFormat:@"%@",activityEntity.campaign.start_time]componentsSeparatedByString:@"T"][0];
        sDate = [sDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
        NSString *sDate1 = [[NSString stringWithFormat:@"%@",activityEntity.campaign.start_time]componentsSeparatedByString:@"T"][1];
        sDate1 = [[NSString stringWithFormat:@"%@",sDate1]componentsSeparatedByString:@"+"][0];
        NSString *eDate = [[NSString stringWithFormat:@"%@",activityEntity.campaign.deadline]componentsSeparatedByString:@"T"][0];
        eDate = [eDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
        NSString *eDate1 = [[NSString stringWithFormat:@"%@",activityEntity.campaign.deadline]componentsSeparatedByString:@"T"][1];
        eDate1 = [[NSString stringWithFormat:@"%@",eDate1]componentsSeparatedByString:@"+"][0];
        timeLabel.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",sDate,sDate1,eDate,eDate1];
        CGSize timeLabelSize = [Utils getUIFontSizeFitW:timeLabel];
        timeLabel.width = timeLabelSize.width+8.0;
        timeLabel.left = (ScreenWidth-timeLabel.width-8.0)/2.0;
    }
    
    NSArray * languages = [NSLocale preferredLanguages];
    NSString * currentLanguage = [languages objectAtIndex:0];
    //
    UITextView*contentTV = [[UITextView alloc]initWithFrame:CGRectMake(20, timeLabel.bottom+5.0, scrollviewn.width-20.0*2, subScrollView.height-35.0-64.0-35-60.0-26-0.5-timeLabel.bottom-5.0-5)];
    if([currentLanguage isEqualToString:@"en-CN"]){
        contentTV.frame = CGRectMake(20, timeLabel.bottom+5.0, scrollviewn.width-20.0*2, subScrollView.height-35.0-64.0-40.0-35.0-14.0-40.0-timeLabel.bottom-10.0);
    }
    contentTV.font = font_13;
    contentTV.text = [contentTV.text stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    contentTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    contentTV.editable = NO;
    contentTV.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:contentTV];
    if ([LocalService getRBIsIncheck] == YES) {
        contentTV.hidden = YES;
    }
    contentTV.text = activityEntity.campaign.idescription;
    CGSize contentTVSize = [Utils getUITextViewSizeFitH:contentTV withLineSpacing:6.0];
    if (contentTVSize.height+6.0*3.0 < contentTV.height) {
        contentTV.height = contentTVSize.height+6.0*3.0;
    }
    
    // 底部框线条
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subScrollView.height-35.0-64.0-35-60.0-26-0.5, scrollviewn.width, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [subScrollView addSubview:lineLabel];
    //
    tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subScrollView.height-35.0-64.0-30.0-60.0-26, scrollviewn.width, 35.0)];
    tagLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    if ([currentLanguage isEqualToString:@"en-CN"]) {
        tagLabel.numberOfLines = 2;
        tagLabel.font = font_cu_13;
        tagLabel.textAlignment = NSTextAlignmentCenter;
    }else{
        tagLabel.font = font_cu_15;
        tagLabel.numberOfLines = 1;
        tagLabel.textAlignment = NSTextAlignmentCenter;
    }
    [subScrollView addSubview:tagLabel];
    if ([LocalService getRBIsIncheck] == YES) {
        tagLabel.hidden = YES;
    }
    //
    UIView*tagView = [[UIImageView alloc] initWithFrame:CGRectMake(20, subScrollView.height-64.0-20.0-60.0-26, ScreenWidth-2*20.0, 64.0)];
    tagView.backgroundColor = [Utils getUIColorWithHexString:SysColorSubBlack];
    [subScrollView addSubview:tagView];
    if ([LocalService getRBIsIncheck] == YES) {
        tagView.hidden = YES;
    }
    //
    tagTextLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, (tagView.height-18.0-15.0-5.0)/2.0, tagView.width, 18.0)];
    tagTextLabel.font = font_cu_15;
    tagTextLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    tagTextLabel.textAlignment = NSTextAlignmentCenter;
    [tagView addSubview:tagTextLabel];
    if ([LocalService getRBIsIncheck] == YES) {
        tagTextLabel.hidden = YES;
    }
    //
    tagTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, tagTextLabel.bottom+5.0, tagView.width, 15)];
    tagTimeLabel.font = font_11;
    tagTimeLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
    tagTimeLabel.textAlignment = NSTextAlignmentCenter;
    [tagView addSubview:tagTimeLabel];
    //
    UIButton*kolBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    kolBtn.frame = CGRectMake(tagView.left, subScrollView.height-10.0-60.0-26, tagView.width, 60.0);
    [kolBtn addTarget:self
               action:@selector(kolBtnAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [subScrollView addSubview:kolBtn];
    //
    UILabel*kolLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kolBtn.width, 15)];
    kolLabel.font = font_11;
    kolLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    kolLabel.textAlignment = NSTextAlignmentCenter;
    [kolBtn addSubview:kolLabel];
    kolLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R4014", @"已有%@人参加"),activityEntity.invitees_count];
    //
    float temw = 40.0;
    float temg = 18.0;
    if(ScreenHeight<=568) {
        temw = 35.0;
        temg = 15.0;
    }
    int num = (int)activityEntity.invitees.count;
    num = num+1;
    if (num>5) {
        num=5;
    }
    float temleft = (kolBtn.width-temw*num-temg*(num-1))/2.0;
    for (int i=0; i<num; i++) {
        if(i==(num-1)) {
            UILabel*kolIVLabel = [[UILabel alloc] initWithFrame:CGRectMake(temleft+(temw+temg)*i, kolLabel.bottom+5.0, temw, temw)];
            kolIVLabel.font = font_icon_(temw-1);
            kolIVLabel.text = Icon_btn_more;
            kolIVLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            [kolBtn addSubview:kolIVLabel];
        } else {
            RBUserEntity*userEntity = activityEntity.invitees[i];
            UIImageView*kolIV = [[UIImageView alloc]initWithFrame:CGRectMake(temleft+(temw+temg)*i, kolLabel.bottom+5.0, temw, temw)];
            [kolIV sd_setImageWithURL:[NSURL URLWithString:userEntity.avatar_url] placeholderImage:PlaceHolderUserImage];
            if (userEntity.avatar_url.length==0) {
                kolIV.layer.borderWidth = 1.0f;
                kolIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
            }
            kolIV.layer.cornerRadius = kolIV.width/2.0;
            kolIV.layer.masksToBounds = YES;
            [kolBtn addSubview:kolIV];
        }
    }
    // 活动详情
    UIButton*detailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    detailBtn.frame = CGRectMake(0, subScrollView.height-26.0, ScreenWidth, 26.0);
    [detailBtn addTarget:self
                  action:@selector(detailBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [subScrollView addSubview:detailBtn];
    //
    UILabel *btnLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 13.0)];
    btnLabel.font = font_11;
    btnLabel.text = NSLocalizedString(@"R2026", @"推广内容详情");
    btnLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    btnLabel.textAlignment=NSTextAlignmentCenter;
    [detailBtn addSubview:btnLabel];
    if ([LocalService getRBIsIncheck] == YES) {
        btnLabel.hidden = YES;
        detailBtn.hidden = YES;
    }
    //
    UILabel*downLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth-13.0)/2.0, btnLabel.bottom, 13.0, 13.0)];
    downLabel.font = font_icon_(13.0);
    downLabel.text = Icon_btn_down;
    downLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [detailBtn addSubview:downLabel];
    //
    [self updateText];
}
//点击活动要求出现弹框
-(void)alertShow{
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.delegate = self;
    if ([LocalService getRBIsIncheck] == NO) {
        if ([activityEntity.campaign.per_budget_type isEqualToString:@"post"]) {
            [alert showWithArray:@[@"此任务要求分享后保留至少30分钟后截图并上传，否则不予通过",@"知道了"] IsCountDown:nil AndImageStr:@"rule" AndBigTitle:@"活动要求"];
        }else{
            [alert showWithArray:@[[NSString stringWithFormat:@"%@",activityEntity.campaign.remark],@"知道了"] IsCountDown:nil AndImageStr:@"rule" AndBigTitle:@"活动要求"];
        }
    }
}
-(void)updateText {
    NSString*per_budget_type = [NSString stringWithFormat:@"%@",activityEntity.campaign.per_budget_type];
    NSString*per_action_budget = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:activityEntity.campaign.per_action_budget]];
    NSString*status = [NSString stringWithFormat:@"%@",activityEntity.status];
    NSString*avail_click = [NSString stringWithFormat:@"%@",activityEntity.avail_click];
    NSString*earn_money = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:activityEntity.earn_money]];
    if ([per_budget_type isEqualToString:@"click"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@%@%@%@",NSLocalizedString(@"R2502",@"根据你的历史表现，你可单次"),NSLocalizedString(@"R2014",@"点击"),NSLocalizedString(@"R2503",@"赚到：¥ "),per_action_budget];
    } else if ([per_budget_type isEqualToString:@"cpa"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@%@%@%@",NSLocalizedString(@"R2502",@"根据你的历史表现，你可单次"),NSLocalizedString(@"R2015",@"效果"),NSLocalizedString(@"R2503",@"赚到：¥ "),per_action_budget];
    } else if ([per_budget_type isEqualToString:@"cpt"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@%@%@%@",NSLocalizedString(@"R2502",@"根据你的历史表现，你可单次"),NSLocalizedString(@"R2301",@"任务"),NSLocalizedString(@"R2503",@"赚到：¥ "),per_action_budget];
    } else if ([per_budget_type isEqualToString:@"simple_cpi"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@%@%@%@",NSLocalizedString(@"R2502",@"根据你的历史表现，你可单次"),NSLocalizedString(@"R2200",@"下载"),NSLocalizedString(@"R2503",@"赚到：¥ "),per_action_budget];
    } else {
        tagLabel.text = [NSString stringWithFormat:@"%@%@%@%@",NSLocalizedString(@"R2502",@"根据你的历史表现，你可单次"),NSLocalizedString(@"R2017",@"转发"),NSLocalizedString(@"R2503",@"赚到：¥ "),per_action_budget];
    }
    if ([per_budget_type isEqualToString:@"click"]) {
            tagTextLabel.text = NSLocalizedString(@"R2027", @"分享后好友点击此文章立即获得报酬");
    } else if ([per_budget_type isEqualToString:@"cpa"]) {
            tagTextLabel.text = NSLocalizedString(@"R2028", @"分享后好友完成指定活动立即获得报酬");
    } else if ([per_budget_type isEqualToString:@"cpt"]) {
            tagTextLabel.text = NSLocalizedString(@"R2302", @"分享后完成指定任务立即获得报酬");
    } else if ([per_budget_type isEqualToString:@"simple_cpi"]) {
            tagTextLabel.text = NSLocalizedString(@"R2201", @"分享后下载指定APP立即获得报酬");
    } else {
            tagTextLabel.text = NSLocalizedString(@"R2029", @"分享此文章立即获得报酬");
    }
    tagTextLabel.frame = CGRectMake(0, 0, ScreenWidth-2*20.0, 64.0);
    tagTimeLabel.hidden = YES;
    if ([per_budget_type isEqualToString:@"click"]) {
        if ([status isEqualToString:@"pending"] || [status isEqualToString:@"running"]) {
                
                tagTimeLabel.hidden = NO;
                tagTextLabel.frame = CGRectMake(0, (64.0-18.0-15.0-5.0)/2.0, ScreenWidth-2*20.0, 18.0);
                tagTimeLabel.text = [Utils getUIDateCompareNow:[NSString stringWithFormat:@"%@",activityEntity.campaign.deadline]];
            } else if ([status isEqualToString:@"approved"]||[status isEqualToString:@"finished"]) {
                //参与了开始或者结束
                tagTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R2506", @"已获得 %@ 次点击,即将赚￥%@"),avail_click,earn_money];
            } else if ([status isEqualToString:@"settled"]||[status isEqualToString:@"missed"]||[status isEqualToString:@"rejected"]) {
                //settled参与通过 missed错失活动  rejected活动截图被拒
                tagTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R2031", @"已获得 %@ 次点击,已赚￥%@"),avail_click,earn_money];
            }
        }else{
            if ([status isEqualToString:@"pending"] || [status isEqualToString:@"running"]) {
                
                tagTimeLabel.hidden = NO;
                tagTextLabel.frame = CGRectMake(0, (64.0-18.0-15.0-5.0)/2.0, ScreenWidth-2*20.0, 18.0);
                tagTimeLabel.text = [Utils getUIDateCompareNow:[NSString stringWithFormat:@"%@",activityEntity.campaign.deadline]];
            } else if ([status isEqualToString:@"approved"]||[status isEqualToString:@"finished"]) {
                //参与了开始或者结束
                tagTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R2508", @"活动进行中,即将赚￥%@"),earn_money];
            } else if ([status isEqualToString:@"settled"]||[status isEqualToString:@"missed"]||[status isEqualToString:@"rejected"]) {
                tagTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R2507", @"已完成,已赚￥%@"),earn_money];
            }
            
        }
    [tagTextLabel setText:tagTextLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:avail_click options:NSCaseInsensitiveSearch];
         NSRange range1 = [[mStr string] rangeOfString:[NSString stringWithFormat:@"￥"] options:NSCaseInsensitiveSearch];
         NSRange range2 = [[mStr string] rangeOfString:earn_money options:NSCaseInsensitiveSearch];
         CTFontRef font = CTFontCreateWithName((CFStringRef)font_cu_17.fontName, font_cu_17.pointSize, NULL);
         [mStr addAttributes:@{(NSString *)kCTFontAttributeName:(__bridge id)font,(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range];
         [mStr addAttributes:@{(NSString *)kCTFontAttributeName:(__bridge id)font,(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range2];
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range1];
         CFRelease(font);
         return mStr;
     }];
}

- (void)loadWebView {
    [webViewNV removeFromSuperview];
    [webviewn removeFromSuperview];
    webViewNV = nil;
    webviewn = nil;
    //
    webViewNV = [[UIView alloc]initWithFrame:CGRectMake(0, scrollviewn.height, self.view.width, NavHeight)];
    webViewNV.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    webViewNV.userInteractionEnabled = YES;
    [scrollviewn addSubview:webViewNV];
    //
    UILabel *navCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, StatusHeight, self.view.width, 44)];
    navCenterLabel.text = NSLocalizedString(@"R2026", @"推广内容详情");
    navCenterLabel.font = font_cu_17;
    navCenterLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    navCenterLabel.textAlignment = NSTextAlignmentCenter;
    navCenterLabel.userInteractionEnabled = YES;
    [webViewNV addSubview:navCenterLabel];
    //
    UILabel*navLeftLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, StatusHeight+(44.0-20.0)/2.0, 20.0, 20.0)];
    navLeftLabel.font = font_icon_(20.0);
    navLeftLabel.text = Icon_bar_back;
    navLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [webViewNV addSubview:navLeftLabel];
    //
    UIButton*navLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navLeftBtn.frame = CGRectMake(0, StatusHeight, 44.0, 44.0);
    [navLeftBtn addTarget:self
                   action:@selector(webLeftBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    [webViewNV addSubview:navLeftBtn];
    //
    UIButton * navRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navRightBtn.frame = CGRectMake(ScreenWidth-44.0, StatusHeight, 44.0, 44.0);
    [navRightBtn addTarget:self action:@selector(navRightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [navRightBtn setImage:[UIImage imageNamed:@"RBshareImage"] forState:UIControlStateNormal];
    [webViewNV addSubview:navRightBtn];

    //
    UILabel*navLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, webViewNV.height-0.5, webViewNV.width, 0.5)];
    navLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBarLine];
    [webViewNV addSubview:navLineLabel];
    //
    webviewn = [[UIWebView alloc] initWithFrame:CGRectMake(10, webViewNV.bottom, ScreenWidth-20.0, ScreenHeight-45.0-NavHeight)];
    webviewn.delegate = self;
    webviewn.scalesPageToFit = YES;
    [scrollviewn addSubview:webviewn];
    //
    NSString*url = [activityEntity.campaign.url stringByReplacingOccurrencesOfString:@"#wechat_redirect" withString:@""];
    [webviewn loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, webviewn.bottom)];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([[request.URL.absoluteString componentsSeparatedByString:@"https://itunes.apple.com"]count]>1) {
        return NO;
    }
    if ([[request.URL.absoluteString componentsSeparatedByString:@".apk"]count]>1) {
        return NO;
    }
    return YES;
}

#pragma mark - FooterView Delegate
- (void)updateFooterView {
    [footerView removeFromSuperview];
    footerView = nil;
    //
    footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
    //
    footerRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerRightBtn.frame = CGRectMake(footerView.width/2.0, 0, footerView.width/2.0, footerView.height);
    footerRightBtn.titleLabel.font = font_cu_15;
    [footerRightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerRightBtn];
    //
    footerLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(footerView.width/2.0-0.5f, 10.0, 1, 45.0-20.0)];
    footerLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [footerView addSubview:footerLineLabel];
    //
    [self updateText];
    footerBtn.enabled = NO;
    footerBtn.frame = CGRectMake(0, 0, ScreenWidth, 45.0);
    footerRightBtn.frame = CGRectMake(ScreenWidth/2.0, 0, ScreenWidth/2.0, 45.0);
    footerRightBtn.hidden = YES;
    footerLineLabel.hidden = YES;
    //
    NSString*status = [NSString stringWithFormat:@"%@",activityEntity.status];
    if ([status isEqualToString:@"pending"]) {
        [footerBtn setTitle:NSLocalizedString(@"R2032", @"活动未开始") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
        footerBtn.enabled = NO;
        return;
    } else if ([status isEqualToString:@"running"]) {
        NSString*str = NSLocalizedString(@"R1024", @"朋友圈");
        if([activityEntity.campaign.sub_type isEqualToString:@"weibo"]) {
            str = NSLocalizedString(@"R1025", @"微博");
        } else if([activityEntity.campaign.sub_type isEqualToString:@"qq"]) {
            str = NSLocalizedString(@"R1027", @"QQ空间");
        } else {
            str = NSLocalizedString(@"R1024", @"朋友圈");
        }
        if([activityEntity.campaign.wechat_auth_type isEqualToString:@"self_info"]) {
            str = NSLocalizedString(@"R1045", @"微信收藏");
        }
        if ([LocalService getRBIsIncheck] == YES) {
            [footerBtn setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R5242", @"分享")]forState:UIControlStateNormal];
        }else{
            [footerBtn setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R1102", @"分享赚收益")]forState:UIControlStateNormal];
        }
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
        footerBtn.enabled = YES;
        [footerBtn addTarget:self
                      action:@selector(footerShareApproveBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        return;
    } else if ([status isEqualToString:@"approved"] || [status isEqualToString:@"finished"]) {
        if ([NSString stringWithFormat:@"%@",activityEntity.screenshot].length==0) {
            if ([status isEqualToString:@"finished"]) {
                if ([NSString stringWithFormat:@"%@",activityEntity.start_upload_screenshot].intValue==1) {
                    if ([NSString stringWithFormat:@"%@",activityEntity.can_upload_screenshot].intValue==1) {
                        [footerBtn setTitle:NSLocalizedString(@"R2034", @"上传截图") forState:UIControlStateNormal];
                        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                        footerBtn.enabled = YES;
                        [footerBtn addTarget:self
                                      action:@selector(footerUploadBtnAction:)
                            forControlEvents:UIControlEventTouchUpInside];
                        //
                        footerBtn.frame = CGRectMake(0, 0, ScreenWidth/2.0, 45.0);
                        footerRightBtn.frame = CGRectMake(ScreenWidth/2.0, 0, ScreenWidth/2.0, 45.0);
                        footerRightBtn.hidden = NO;
                        footerLineLabel.hidden = NO;
                        [footerRightBtn setTitle:NSLocalizedString(@"R2035", @"再次分享") forState:UIControlStateNormal];
                        footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                        [footerRightBtn addTarget:self
                                           action:@selector(footerShareApproveBtnAction:)
                                 forControlEvents:UIControlEventTouchUpInside];
                        return;
                    } else {
                        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                        footerBtn.enabled = NO;
                        [footerBtn setTitle:NSLocalizedString(@"R2036", @"上传截图时间已过") forState:UIControlStateNormal];
                        //
                        footerBtn.frame = CGRectMake(0, 0, ScreenWidth/2.0, 45.0);
                        footerRightBtn.frame = CGRectMake(ScreenWidth/2.0, 0, ScreenWidth/2.0, 45.0);
                        footerRightBtn.hidden = NO;
                        [footerRightBtn setTitle:NSLocalizedString(@"R2035", @"再次分享") forState:UIControlStateNormal];
                        footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                        [footerRightBtn addTarget:self
                                           action:@selector(footerShareApproveBtnAction:)
                                 forControlEvents:UIControlEventTouchUpInside];
                        return;
                    }
                } else {
                    NSString*tempTime = [NSString stringWithFormat:@"%@:%@",activityEntity.upload_interval_time[2],activityEntity.upload_interval_time[3]];
                    if ([tempTime isEqualToString:@"0:0"]) {
                        [footerBtn setTitle:NSLocalizedString(@"R2037", @"未知状态") forState:UIControlStateNormal];
                    } else {
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"m:s"];
                        tempDate = [formatter dateFromString:tempTime];
                        [updateTimer invalidate];
                        updateTimer = nil;
                        updateTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                                     target  :self
                                                                     selector:@selector(updateTimer)
                                                                     userInfo:nil
                                                                     repeats :YES];
                        NSString*temp = [NSString stringWithFormat:NSLocalizedString(@"R2038", @"%@分%@秒 后请上传截图"),activityEntity.upload_interval_time[2],activityEntity.upload_interval_time[3]];
                        [footerBtn setTitle:temp forState:UIControlStateNormal];
                    }
                    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
                    footerBtn.enabled = NO;
                    //
                    footerBtn.frame = CGRectMake(0, 0, ScreenWidth/2.0, 45.0);
                    footerRightBtn.frame = CGRectMake(ScreenWidth/2.0,0, ScreenWidth/2.0, 45.0);
                    footerRightBtn.hidden = NO;
                    [footerRightBtn setTitle:NSLocalizedString(@"R2035", @"再次分享") forState:UIControlStateNormal];
                    footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                    [footerRightBtn addTarget:self
                                       action:@selector(footerShareApproveBtnAction:)
                             forControlEvents:UIControlEventTouchUpInside];
                    //
                    return;
                }
            } else {
                if ([NSString stringWithFormat:@"%@",activityEntity.can_upload_screenshot].intValue==1) {
                    [footerBtn setTitle:NSLocalizedString(@"R2034", @"上传截图") forState:UIControlStateNormal];
                    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                    footerBtn.enabled = YES;
                    [footerBtn addTarget:self
                                  action:@selector(footerUploadBtnAction:)
                        forControlEvents:UIControlEventTouchUpInside];
                    //
                    footerBtn.frame = CGRectMake(0, 0, ScreenWidth/2.0, 45.0);
                    footerRightBtn.frame = CGRectMake(ScreenWidth/2.0, 0, ScreenWidth/2.0, 45.0);
                    footerRightBtn.hidden = NO;
                    footerLineLabel.hidden = NO;
                    [footerRightBtn setTitle:NSLocalizedString(@"R2035", @"再次分享") forState:UIControlStateNormal];
                    footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                    [footerRightBtn addTarget:self
                                       action:@selector(footerShareApproveBtnAction:)
                             forControlEvents:UIControlEventTouchUpInside];
                    //
                    return;
                } else {
                    NSString*tempTime = [NSString stringWithFormat:@"%@:%@",activityEntity.upload_interval_time[2],activityEntity.upload_interval_time[3]];
                    if ([tempTime isEqualToString:@"0:0"]) {
                        [footerBtn setTitle:NSLocalizedString(@"R2037", @"未知状态") forState:UIControlStateNormal];
                    } else {
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"m:s"];
                        tempDate = [formatter dateFromString:tempTime];
                        [updateTimer invalidate];
                        updateTimer = nil;
                        updateTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                                     target  :self
                                                                     selector:@selector(updateTimer)
                                                                     userInfo:nil
                                                                     repeats :YES];
                        NSString*temp = [NSString stringWithFormat:NSLocalizedString(@"R2038", @"%@分%@秒 后请上传截图"),activityEntity.upload_interval_time[2],activityEntity.upload_interval_time[3]];
                        [footerBtn setTitle:temp forState:UIControlStateNormal];
                    }
                    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
                    footerBtn.enabled = NO;
                    footerLineLabel.hidden = YES;
                    //
                    footerBtn.frame = CGRectMake(0,0, ScreenWidth/2.0, 45.0);
                    footerRightBtn.frame = CGRectMake(ScreenWidth/2.0,0, ScreenWidth/2.0, 45.0);
                    footerRightBtn.hidden = NO;
                    [footerRightBtn setTitle:NSLocalizedString(@"R2035", @"再次分享") forState:UIControlStateNormal];
                    footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                    [footerRightBtn addTarget:self
                                       action:@selector(footerShareApproveBtnAction:)
                             forControlEvents:UIControlEventTouchUpInside];
                    return;
                }
            }
        } else {
            NSString*temp = @"";
            NSString*img_status = [NSString stringWithFormat:@"%@",activityEntity.img_status];
            if ([img_status isEqualToString:@"passed"]) {
                temp = NSLocalizedString(@"R2039", @"审核通过");
                [footerRightBtn setTitle:NSLocalizedString(@"R2035", @"再次分享") forState:UIControlStateNormal];
                [footerRightBtn addTarget:self
                                   action:@selector(footerShareApproveBtnAction:)
                         forControlEvents:UIControlEventTouchUpInside];
                [footerBtn setTitle:[NSString stringWithFormat:@"%@-%@",temp,NSLocalizedString(@"R2042", @"查看截图")] forState:UIControlStateNormal];
                
            } else if ([img_status isEqualToString:@"pending"]) {
                temp = NSLocalizedString(@"R2040", @"审核中");
                //再次分享
                [footerRightBtn setTitle:NSLocalizedString(@"R2035", @"再次分享") forState:UIControlStateNormal];
                [footerRightBtn addTarget:self action:@selector(footerShareApproveBtnAction:) forControlEvents:UIControlEventTouchUpInside];

                [footerBtn setTitle:NSLocalizedString(@"R2510", @"再次上传") forState:UIControlStateNormal];
            } else if ([img_status isEqualToString:@"rejected"]) {
                temp = NSLocalizedString(@"R2041", @"审核拒绝");
                [footerRightBtn setTitle:NSLocalizedString(@"R2044", @"重新上传") forState:UIControlStateNormal];
                [footerRightBtn addTarget:self
                                   action:@selector(footerUploadBtnAction:)
                         forControlEvents:UIControlEventTouchUpInside];
                [footerBtn setTitle:[NSString stringWithFormat:@"%@-%@",temp,NSLocalizedString(@"R2043", @"查看原因")] forState:UIControlStateNormal];
            }
            footerBtn.frame = CGRectMake(0,0, ScreenWidth/2.0, 45.0);
            footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
            footerBtn.enabled = YES;
            [footerBtn addTarget:self
                          action:@selector(footerSeeBtnAction:)
                forControlEvents:UIControlEventTouchUpInside];
            footerLineLabel.hidden = NO;
            footerRightBtn.frame = CGRectMake(ScreenWidth/2.0,0, ScreenWidth/2.0, 45.0);
            footerRightBtn.hidden = NO;
            footerRightBtn.enabled = YES;
            footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
            return;
        }
    } else if ([status isEqualToString:@"settled"]) {
        [footerBtn setTitle:NSLocalizedString(@"R2045", @"活动已完成") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
        footerBtn.enabled = NO;
        return;
    } else if ([status isEqualToString:@"rejected"]) {
        [footerBtn setTitle:NSLocalizedString(@"R2046", @"活动审核失败") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
        footerBtn.enabled = NO;
        return;
    } else if ([status isEqualToString:@"missed"]) {
        [footerBtn setTitle:NSLocalizedString(@"R2047", @"活动已错失") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
        footerBtn.enabled = NO;
//        [footerBtn addTarget:self
//                      action:@selector(footerShareNormalBtnAction:)
//            forControlEvents:UIControlEventTouchUpInside];
        return;
    }else if ([status isEqualToString:@"countdown"]){
        [footerBtn setTitle:NSLocalizedString(@"R2520", @"活动即将开始") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
        footerBtn.enabled = NO;
        return;
    }
}
- (void)updateFoot{
    if ([[Utils getUIDateCompareNowFortrailer:activityEntity.campaign.start_time] isEqualToString: NSLocalizedString(@"R2518", @"活动已开始")]) {
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
        footerBtn.enabled = YES;
        [footerBtn addTarget:self
                      action:@selector(footerShareApproveBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        [footerBtn setTitle:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R1102", @"分享赚收益")]forState:UIControlStateNormal];
    }
}
#pragma mark - Timer Delegate
- (void)updateTimer {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"m:s"];
    NSDate*zeroDate = [formatter dateFromString:@"0:0"];
    BOOL isEqual = [tempDate isEqualToDate:zeroDate];
    if (isEqual) {
        [updateTimer invalidate];
        updateTimer = nil;
        [self getActivityData];
        
    } else {
        tempDate = [NSDate dateWithTimeInterval:-1 sinceDate:tempDate];
        NSString*temp = [formatter stringFromDate:tempDate];
        temp = [temp stringByReplacingOccurrencesOfString:@":" withString:NSLocalizedString(@"R2049", @"分")];
        NSString*str = [NSString stringWithFormat:NSLocalizedString(@"R2048", @"%@秒 后请上传截图"),temp];
        [footerBtn setTitle:str forState:UIControlStateNormal];
    }
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollviewn.mj_header isRefreshing]==YES) {
        scrollviewn.contentOffset = CGPointMake(0, 0);
        return;
    }
    [self changeStatusBar];
}

-(void)changeStatusBar {
    if (scrollviewn.contentOffset.y >= scrollviewn.height) {
        [[UIApplication sharedApplication]
         setStatusBarStyle:UIStatusBarStyleDefault];
    } else {
        [[UIApplication sharedApplication]
         setStatusBarStyle:UIStatusBarStyleLightContent];
    }
}

#pragma mark - UIButton Delegate
- (void)navLeftBtnAction:(UIButton *)sender {
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)webLeftBtnAction:(UIButton *)sender {
    [self changeStatusBar];
    [scrollviewn scrollRectToVisible:CGRectMake(0, 0, scrollviewn.width, scrollviewn.height) animated:YES];
}

- (void)detailBtnAction:(UIButton *)sender {
    [self changeStatusBar];
    [scrollviewn scrollRectToVisible:CGRectMake(0, scrollviewn.height, scrollviewn.width, scrollviewn.height) animated:YES];
}

- (void)kolBtnAction:(UIButton *)sender {
    RBCampaignDetailKolsViewController *toview = [[RBCampaignDetailKolsViewController alloc] init];
    toview.activityEntity = activityEntity;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)footerSeeBtnAction:(UIButton *)sender {
    NSString*img_status = [NSString stringWithFormat:@"%@",activityEntity.img_status];
    if([self isVisitorLogin]==YES){
        return;
    };
    //
    if(activityEntity.reject_reason.length!=0) {
        RBNewAlert * alertView = [[RBNewAlert alloc]init];
        alertView.tag = 13000;
        alertView.delegate = self;
        [alertView showWithArray:@[[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2050", @"图片审核不通过:\n"),activityEntity.reject_reason],NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R2042", @"查看截图")] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
        return;
    }
    if ([img_status isEqualToString:@"pending"]) {
        RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
        actionSheet.delegate = self;
        actionSheet.tag = 13000;
        [actionSheet showWithArray:@[[NSString stringWithFormat:@"%@-%@",NSLocalizedString(@"R2040", @"审核中"),NSLocalizedString(@"R2042", @"查看截图")],NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R2510", @"再次上传"),NSLocalizedString(@"R1011", @"取消")]];
        return;
    }
    //
    TOWebViewController *toview = [[TOWebViewController alloc] init];
    toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.screenshot]];
    toview.title = NSLocalizedString(@"R2042", @"查看截图");
    toview.showPageTitles = NO;
    toview.navigationButtonsHidden = YES;
    toview.hidesBottomBarWhenPushed = YES;
    toview.campainID = activityEntity.iid;
    [self.navigationController pushViewController:toview animated:YES];
}
#pragma 再次上传按钮触发的方法
- (void)footerUploadBtnAction:(UIButton *)sender {
    if([self isVisitorLogin]==YES){
        return;
    };
    //
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    actionSheet.tag = 12000;
    [actionSheet showWithArray:@[NSLocalizedString(@"R2034", @"上传截图"),NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R1011", @"取消")]];
}
//验证用户是否绑定了微信
-(void)verifyWechat{
    if(![JsonService isRBUserVisitor]) {
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBKOLDetailWithKolId:[LocalService getRBLocalDataUserLoginId]];
    }
}
- (void)footerShareApproveBtnAction:(UIButton *)sender {
    if([self isVisitorLogin]==YES){
        return;
    };
    if ([LocalService getRBLocalDataUserAgree].length!=0) {
        if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"R2035", @"再次分享")]) {
            if([activityEntity.sub_type isEqualToString:@"weibo"]) {
                [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.share_url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformTypeSinaWeibo];
                RBNewAlert * alert = [[RBNewAlert alloc]init];
                alert.delegate = self;
                alert.tag = 1333;
                [alert showWithCampainTopArray:@[@"微博"] AndbottomArray:@[@"icon_weibo"] AndShareWith:@"campain"];
            }else {
                RBNewAlert * alert = [[RBNewAlert alloc]init];
                alert.delegate = self;
                alert.tag = 1333;
                [alert showWithCampainTopArray:@[@"朋友圈",@"微信"] AndbottomArray:@[@"icon_wechat_friends",@"icon_wechat"] AndShareWith:@"campain"];
                
            }
        }else{
            RBNewAlert * alert = [[RBNewAlert alloc]init];
            alert.delegate = self;
            alert.tag = 1009;
            if ([activityEntity.campaign.per_action_type isEqualToString:@"wechat,weibo"]) {
                [alert showWithCampainTopArray:@[@"微博",@"朋友圈",@"微信"] AndbottomArray:@[@"icon_weibo",@"icon_wechat_friends",@"icon_wechat"] AndShareWith:@"campain"];
            }else if ([activityEntity.campaign.per_action_type isEqualToString:@"weibo"]){
                [alert showWithCampainTopArray:@[@"微博"] AndbottomArray:@[@"icon_weibo"] AndShareWith:@"campain"];
            }else{
                [alert showWithCampainTopArray:@[@"朋友圈",@"微信"] AndbottomArray:@[@"icon_wechat_friends",@"icon_wechat"] AndShareWith:@"campain"];
            }
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Robin8免责声明" message:@"Robin8提醒用户在使用Robin8平台（robin8.com）服务以及robin8 APP（以下简称“Robin8服务”）之前，仔细阅读并理解以下条款，尤其是限制用户权利的条款。使用Robin8服务的行为将视为用户已经同意本声明的全部内容，并将在使用Robin8服务的过程中遵守执行。\n1、Robin8是供有广告投放推广需求的客户（下称“广告主”）和能够提供广告发布推广服务的自媒体账号或个人账户（以下简称“KOL”）交易的第三方平台。Robin8平台展示广告主提供的广告内容、KOL提供的账号名称、粉丝数等原始数据，供广告主、KOL根据需要甄选符合自己需求的服务对象进行交易。\n2、Robin8将对广告主的身份、广告内容、KOL的身份信息、账号名称等内容进行审查，筛选出Robin8认为不真实的信息，但由于网络平台的特殊性及微播易的局限性，Robin8无法筛选出所有的不真实信息并做处理，但将尽最大努力保证所有信息的真实性。\n3、广告主应如实填写广告信息，对广告内容的真实性、准确性、合法性、推广后的影响负责，Robin8倡导各广告主诚信发布广告、遵守法律法规以共同维护网络环境。\n4、KOL应如实填写并及时更新对应平台的相应数据，包括但不限于账号名称、地域、账号粉丝数、受众信息等，并反馈真实的推广效果，Robin8倡导KOL诚信服务、遵守法律法规以共同维护网络环境。\n5、KOL应综合考虑广告内容的真实性、准确性、合法性等因素后选择是否接单，一旦选择接单则应自行承担广告内容发布后对其账号造成的一切影响。广告主与KOL因广告发布产生的纠纷由双方自行解决。\n6、Robin8基于广告主需求向其推荐的账号，仅是微播易基于账号自身数据分析判断做出的推荐，由广告主最终确认是否使用Robin8推荐的账号。\n7、Robin8将采取必要手段为用户信息保密，但基于技术限制等，Robin8并不能保证用户的信息完全不被泄露。\n8、用户应同意Robin8有限度的使用其信息，包括但不限于向用户及时发送重要通知或派发订单、Robin8内部进行审计、数据分析和研究等Robin8自行判断为合理使用的情形及法律法规要求等。\n9、用户认为Robin8的其它用户侵犯其权利的，可向Robin8提供权利证明文件、身份证明文件等，Robin8审核评估后将采取适当措施予以处理。" delegate:self cancelButtonTitle:NSLocalizedString(@"R8037",@"同意") otherButtonTitles:NSLocalizedString(@"R8038",@"拒绝"),nil];
        [alert show];
    }
}

- (void)footerShareNormalBtnAction:(UIButton *)sender {
    if([self isVisitorLogin]==YES){
        return;
    };
    //
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.delegate = self;
    alert.tag = 1010;
    if ([activityEntity.campaign.per_action_type isEqualToString:@"wechat,weibo"]) {
        [alert showWithCampainTopArray:@[@"微博",@"朋友圈",@"微信"] AndbottomArray:@[@"icon_weibo",@"icon_wechat_friends",@"icon_wechat"] AndShareWith:@"campain"];
    }else if ([activityEntity.campaign.per_action_type isEqualToString:@"weibo"]){
        [alert showWithCampainTopArray:@[@"微博"] AndbottomArray:@[@"icon_weibo"] AndShareWith:@"campain"];
    }else{
        [alert showWithCampainTopArray:@[@"朋友圈",@"微信"] AndbottomArray:@[@"icon_wechat_friends",@"icon_wechat"] AndShareWith:@"campain"];
    }
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==alertView.cancelButtonIndex) {
        [LocalService setRBLocalDataUserAgreeWith:@"YES"];
        if ([footerRightBtn.titleLabel.text isEqualToString:NSLocalizedString(@"R2035", @"再次分享")]) {
            if([activityEntity.sub_type isEqualToString:@"weibo"]) {
                [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.share_url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformTypeSinaWeibo];
            }else {
                markShare = SSDKPlatformSubTypeWechatTimeline;
                [self verifyWechat];
            }
        }else{
            RBNewAlert * alert = [[RBNewAlert alloc]init];
            alert.delegate = self;
            alert.tag = 1009;
            if ([activityEntity.campaign.per_action_type isEqualToString:@"wechat,weibo"]) {
                [alert showWithCampainTopArray:@[@"微博",@"朋友圈",@"微信"] AndbottomArray:@[@"icon_weibo",@"icon_wechat_friends",@"icon_wechat"] AndShareWith:@"campain"];
            }else if ([activityEntity.campaign.per_action_type isEqualToString:@"weibo"]){
                [alert showWithCampainTopArray:@[@"微博"] AndbottomArray:@[@"icon_weibo"] AndShareWith:@"campain"];
            }else{
                [alert showWithCampainTopArray:@[@"朋友圈",@"微信"] AndbottomArray:@[@"icon_wechat_friends",@"icon_wechat"] AndShareWith:@"campain"];
            }
        }
        return;
    }
    return;
}
#pragma mark - 上传截图或示例截图
- (void)pushUploadController:(NSString*)mark{
    RBUploadImageViewController * upView = [[RBUploadImageViewController alloc]init];
    upView.hidesBottomBarWhenPushed = YES;
    upView.mark = mark;
    if ([mark isEqualToString:@"watch"]) {
        upView.imgArr = activityEntity.screenshots;
    }else{
        upView.imgArr = activityEntity.cpi_example_screenshots;
    }
    upView.campainID = activityEntity.iid;
    upView.nameArr = activityEntity.screenshot_comment;
    [self.navigationController pushViewController:upView animated:YES];
}
#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if(actionSheet.tag==12000){
        if (buttonIndex==0) {
            //上传截图
            [self pushUploadController:@"upload"];
        }
        
        if (buttonIndex==1) {
            if (activityEntity.cpi_example_screenshots.count != 0) {
                
                //截图参考
                [self pushUploadController:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
    }else if (actionSheet.tag == 13000){
        if (buttonIndex == 0) {
//            RBWatchImageController * toview = [[RBWatchImageController alloc]init];
//            toview.campainID = activityEntity.iid;
//            toview.hidesBottomBarWhenPushed = YES;
//            toview.imgArr = activityEntity.screenshots;
//            toview.nameArr = activityEntity.screenshot_comment;
//            [self.navigationController pushViewController:toview animated:YES];
            [self pushUploadController:@"watch"];
        }else if (buttonIndex == 1){
            if (activityEntity.cpi_example_screenshots.count != 0) {
            //截图参考
                [self pushUploadController:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }else if (buttonIndex == 2){
            //上传截图
            [self pushUploadController:@"watch"];
        }
    }
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if (alertView.tag==11000) {
        if (buttonIndex==0) {
            if (activityEntity.cpi_example_screenshots.count != 0) {
            //截图参考
                [self pushUploadController:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
    }
    if (alertView.tag==12000) {
        if (buttonIndex==0) {
            if (activityEntity.cpi_example_screenshots.count != 0) {
                //截图参考
                [self pushUploadController:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
    }
    if(alertView.tag==13000){
        if (buttonIndex==0) {
            if (activityEntity.cpi_example_screenshots.count != 0) {
               //截图参考
                [self pushUploadController:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
        if (buttonIndex==1) {
//            RBWatchImageController * toview = [[RBWatchImageController alloc]init];
//            toview.campainID = activityEntity.iid;
//            toview.hidesBottomBarWhenPushed = YES;
//            toview.imgArr = activityEntity.screenshots;
//            toview.nameArr = activityEntity.screenshot_comment;
//            [self.navigationController pushViewController:toview animated:YES];
             [self pushUploadController:@"watch"];
        }
    }
    if(alertView.tag==14000){
        if (buttonIndex==0) {
            if (activityEntity.cpi_example_screenshots.count != 0) {

                //截图参考
                [self pushUploadController:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
    }
}

#pragma mark - ShareSDK Delegate
- (void)shareSDKWithTitle:(NSString *)titleStr
                  Content:(NSString *)contentStr
                      URL:(NSString *)urlStr
                   ImgURL:(NSString *)imgUrlStr
         SSDKPlatformType:(SSDKPlatformType)type {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = contentStr;
    if(type == SSDKPlatformTypeSinaWeibo) {
        
        contentStr = [NSString stringWithFormat:@"#Robin8#   「%@「%@」」",titleStr,urlStr];
    }
    [self.hudView show];
    WeakSelf(weakself)
    __weak __typeof(RBActivityEntity) *weakActivityEntity = activityEntity;
    __weak __typeof(UIButton) * weakFooterBtn = footerBtn;
    NSLog(@"title:%@,content:%@,img:%@,url:%@",titleStr,contentStr,imgUrlStr,urlStr);
    [bgIV setImageWithURL:[NSURL URLWithString:activityEntity.campaign.img_url] placeholder:PlaceHolderImage options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        if(error!=nil) {
            [weakself.hudView showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
            return ;
        } else {
            image = [Utils narrowWithImage:image];
        }
        // shareSDK
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        if(type == SSDKPlatformTypeSinaWeibo) {
            [shareParams SSDKEnableUseClientShare];
            
            [shareParams SSDKSetupShareParamsByText:contentStr
                                             images:weakActivityEntity.campaign.img_url
                                                url:[NSURL URLWithString:urlStr]
                                              title:titleStr
                                               type:SSDKContentTypeAuto];
            //SSDKContentTypeAuto
        } else {
            [shareParams SSDKSetupShareParamsByText:contentStr
                                             images:image
                                                url:[NSURL URLWithString:urlStr]
                                              title:titleStr
                                               type:SSDKContentTypeWebPage];
        }
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            NSLog(@"error:%@",error);
            if (state == SSDKResponseStateSuccess) {
                [weakself.hudView showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
//                NSString*status = [NSString stringWithFormat:@"%@",weakActivityEntity.status];
                [weakself updateFooterView];
                if([weakActivityEntity.campaign.wechat_auth_type isEqualToString:@"self_info"]) {
                    RBNewAlert * alertView = [[RBNewAlert alloc]init];
                    alertView.tag = 14000;
                    alertView.delegate = weakself;
                    [alertView showWithArray:@[@"Robin8官方授权活动\n此活动需要进入 微信->我->收藏->打开链接->授权登录->获得奖励",NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R2500", @"知道了")] IsCountDown:nil AndImageStr:@"bell" AndBigTitle:nil];
                } else {
//                    if ([weakActivityEntity.campaign.per_budget_type isEqualToString:@"post"]) {
//                        if ([weakFooterBtn.titleLabel.text isEqualToString:@"上传截图"]) {
//                            RBNewAlert * alertView = [[RBNewAlert alloc]init];
//                            alertView.delegate = weakself;
//                            alertView.tag = 11000;
//                            [alertView showWithArray:@[@"必须点击左下角上传截图才能获得奖励哦！",@"上传截图",@"知道了"] IsCountDown:nil AndImageStr:@"bell" AndBigTitle:nil];
//                        }
//
//                    }else {
//                        if ([status isEqualToString:@"missed"]) {
//
//                        }else if([NSString stringWithFormat:@"%@",activityEntity.screenshot].length==0) {
//                            RBNewAlert * alertView = [[RBNewAlert alloc]init];
//                            alertView.tag = 11000;
//                            alertView.delegate = weakself;
//                            [alertView showWithArray:@[@"必须点击左下角上传截图才能获得奖励哦！",@"上传截图",@"知道了"] IsCountDown:nil AndImageStr:@"bell" AndBigTitle:nil];
//
//                        }
//                    }
                    if (weakActivityEntity.alert.length > 0) {
                        RBNewAlert * alertView = [[RBNewAlert alloc]init];
                        alertView.delegate = weakself;
                        [alertView showWithArray:@[weakActivityEntity.alert,@"知道了"] IsCountDown:nil AndImageStr:@"bell" AndBigTitle:nil];
                        weakActivityEntity.alert = @"";
                    }else{
                        if ([weakActivityEntity.campaign.per_budget_type isEqualToString:@"post"]) {
                            if ([weakFooterBtn.titleLabel.text isEqualToString:@"上传截图"]) {
                                RBNewAlert * alertView = [[RBNewAlert alloc]init];
                                alertView.delegate = weakself;
                                alertView.tag = 11000;
                                [alertView showWithArray:@[@"必须点击左下角上传截图才能获得奖励哦！",@"上传截图",@"知道了"] IsCountDown:nil AndImageStr:@"bell" AndBigTitle:nil];
                            }
                        }else{
                            if ([NSString stringWithFormat:@"%@",activityEntity.screenshot].length==0) {
                                RBNewAlert * alertView = [[RBNewAlert alloc]init];
                                alertView.tag = 11000;
                                alertView.delegate = weakself;
                                [alertView showWithArray:@[@"必须点击左下角上传截图才能获得奖励哦！",@"上传截图",@"知道了"] IsCountDown:nil AndImageStr:@"bell" AndBigTitle:nil];
                            }
                        }
                    }
                }
                //    }
            }
            if (state == SSDKResponseStateFail) {
                if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                    [weakself.hudView showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                } else {
                    [weakself.hudView showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
                }
            }
            if (state == SSDKResponseStateCancel) {
                [weakself.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
                // [self.hudView showErrorWithStatus:NSLocalizedString(@"R1103", @"分享失败，请重新分享")];
            }
        }];
    }];
//    [bgIV yy_setImageWithURL:[NSURL URLWithString:activityEntity.campaign.img_url] placeholder:PlaceHolderImage options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
//        if(error!=nil) {
//            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
//            return ;
//        } else {
//            image = [Utils narrowWithImage:image];
//        }
//        // shareSDK
//        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
//        if(type == SSDKPlatformTypeSinaWeibo) {
//            [shareParams SSDKEnableUseClientShare];
//
//            [shareParams SSDKSetupShareParamsByText:contentStr
//                                             images:activityEntity.campaign.img_url
//                                                url:[NSURL URLWithString:urlStr]
//                                              title:titleStr
//                                               type:SSDKContentTypeAuto];
//            //SSDKContentTypeAuto
//        } else {
//            [shareParams SSDKSetupShareParamsByText:contentStr
//                                             images:image
//                                                url:[NSURL URLWithString:urlStr]
//                                              title:titleStr
//                                               type:SSDKContentTypeWebPage];
//        }
//        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
//            NSLog(@"error:%@",error);
//            if (state == SSDKResponseStateSuccess) {
//                [self.hudView showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
//                NSString*status = [NSString stringWithFormat:@"%@",activityEntity.status];
//                    [self updateFooterView];
//                    if([activityEntity.campaign.wechat_auth_type isEqualToString:@"self_info"]) {
//                        RBNewAlert * alertView = [[RBNewAlert alloc]init];
//                        alertView.tag = 14000;
//                        alertView.delegate = self;
//                        [alertView showWithArray:@[@"Robin8官方授权活动\n此活动需要进入 微信->我->收藏->打开链接->授权登录->获得奖励",NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R2500", @"知道了")] IsCountDown:nil AndImageStr:@"bell" AndBigTitle:nil];
//                    } else {
//                        if ([activityEntity.campaign.per_budget_type isEqualToString:@"post"]) {
//                            if ([footerBtn.titleLabel.text isEqualToString:@"上传截图"]) {
//                                RBNewAlert * alertView = [[RBNewAlert alloc]init];
//                                alertView.delegate = self;
//                                alertView.tag = 11000;
//                                [alertView showWithArray:@[@"必须点击左下角上传截图才能获得奖励哦！",@"上传截图",@"知道了"] IsCountDown:nil AndImageStr:@"bell" AndBigTitle:nil];
//                            }
//
//                        }else {
//                            if ([status isEqualToString:@"missed"]) {
//
//                            }else if([NSString stringWithFormat:@"%@",activityEntity.screenshot].length==0) {
//                                RBNewAlert * alertView = [[RBNewAlert alloc]init];
//                                alertView.tag = 11000;
//                                alertView.delegate = self;
//                                [alertView showWithArray:@[@"必须点击左下角上传截图才能获得奖励哦！",@"上传截图",@"知道了"] IsCountDown:nil AndImageStr:@"bell" AndBigTitle:nil];
//
//                            }
//                        }
//                    }
//            //    }
//            }
//            if (state == SSDKResponseStateFail) {
//                if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
//                    [self.hudView showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
//                } else {
//                    [self.hudView showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
//                }
//            }
//            if (state == SSDKResponseStateCancel) {
//                [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
//               // [self.hudView showErrorWithStatus:NSLocalizedString(@"R1103", @"分享失败，请重新分享")];
//            }
//        }];
//    } ];
}

#pragma mark - RBPhotoAsstesGroupViewController Delegate
- (void)RBPhotoAsstesGroupViewController:(RBPhotoAsstesGroupViewController *)vc selectedList:(NSMutableArray *)list {
    // 图片压缩处理
    ALAsset *asset = list[0];
    UIImage *originalImage = [Utils thumbnailForAsset:asset maxPixelSize:1024];
    UIImage *tempimg = [Utils
                        getUIImageScalingFromSourceImage:originalImage
                        targetSize:originalImage.size];
    // RB-获取活动上传截图
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityUpaloadWithId:activityEntity.iid andScreenShot:tempimg andCampaignLogo:nil];
}
#pragma mark - RBNewAlert Delegate
-(void)RBNewAlert:(RBNewAlert *)alertView clickedButtonAtIndex:(int)buttonIndex{
    if (alertView.tag == 11000) {
        if (buttonIndex == 0) {
            if([self isVisitorLogin]==YES){
                return;
            };
            //
            RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
            actionSheet.delegate = self;
            actionSheet.tag = 12000;
            [actionSheet showWithArray:@[NSLocalizedString(@"R2034", @"上传截图"),NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R1011", @"取消")]];
        }
    }else if(alertView.tag==13000){
        if (buttonIndex==0) {
            if (activityEntity.cpi_example_screenshots.count!=0) {
                //截图参考
                [self pushUploadController:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
        if (buttonIndex==1) {

//            RBWatchImageController * toview = [[RBWatchImageController alloc]init];
//            toview.campainID = activityEntity.iid;
//            toview.hidesBottomBarWhenPushed = YES;
//            toview.imgArr = activityEntity.screenshots;
//            toview.nameArr = activityEntity.screenshot_comment;
//            [self.navigationController pushViewController:toview animated:YES];
             [self pushUploadController:@"watch"];
        }
    }else if (alertView.tag == 14000){
        if (buttonIndex == 0) {
            if([self isVisitorLogin]==YES){
                return;
            };
            //
            RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
            actionSheet.delegate = self;
            actionSheet.tag = 12000;
            [actionSheet showWithArray:@[NSLocalizedString(@"R2034", @"上传截图"),NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R1011", @"取消")]];
        }
    }else if (alertView.tag == 1009){
        if ([self isVisitorLogin] == YES) {
            return;
        };
        if (buttonIndex == 0) {
            //weibo
            if ([activityEntity.campaign.per_action_type isEqualToString:@"weibo"] || [activityEntity.campaign.per_action_type isEqualToString:@"wechat,weibo"]) {
                markShare = SSDKPlatformTypeSinaWeibo;
                if ([NSString stringWithFormat:@"%@",activityEntity.uuid].length==0) {
                    [self getActivityEnjoy];
                }else{
                [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.share_url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformTypeSinaWeibo];
                }
            }else if ([activityEntity.campaign.per_action_type isEqualToString:@"wechat"]){
                markShare = SSDKPlatformSubTypeWechatTimeline;
                //验证用户是否绑定了微信
                if ([NSString stringWithFormat:@"%@",activityEntity.uuid].length==0) {
                    [self getActivityEnjoy];
                }else{
                    [self verifyWechat];
                }
            }
        }else if (buttonIndex == 1){
            if ([activityEntity.campaign.per_action_type isEqualToString:@"wechat,weibo"]) {
                markShare = SSDKPlatformSubTypeWechatTimeline;
            }else if ([activityEntity.campaign.per_action_type isEqualToString:@"wechat"]){
                markShare = SSDKPlatformSubTypeWechatSession;
            }
            //验证用户是否绑定了微信
            if ([NSString stringWithFormat:@"%@",activityEntity.uuid].length==0) {
                [self getActivityEnjoy];
            }else{
                [self verifyWechat];
            }
        }else{
            //微信好友
            markShare = SSDKPlatformSubTypeWechatSession;
            if ([NSString stringWithFormat:@"%@",activityEntity.uuid].length==0) {
                [self getActivityEnjoy];
            }else{
                [self verifyWechat];
            }
        }
    }else if (alertView.tag == 1010){
        if (buttonIndex == 0) {
            //weibo
            if ([activityEntity.campaign.per_action_type isEqualToString:@"weibo"] || [activityEntity.campaign.per_action_type isEqualToString:@"wechat,weibo"]) {
                [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.campaign.url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformTypeSinaWeibo];
            }else if ([activityEntity.campaign.per_action_type isEqualToString:@"wechat"]){
                [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.campaign.url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformSubTypeWechatTimeline];
            }
        }else if (buttonIndex == 1){
            if ([activityEntity.campaign.per_action_type isEqualToString:@"wechat,weibo"]) {
                [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.campaign.url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformSubTypeWechatTimeline];
            }else if ([activityEntity.campaign.per_action_type isEqualToString:@"wechat"]){
                [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.campaign.url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformSubTypeWechatSession];
            }
        }else{
            [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.campaign.url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformSubTypeWechatSession];
        }
    }else if (alertView.tag == 1333){
        //再次分享
        if (buttonIndex == 0) {
            if ([activityEntity.campaign.sub_type isEqualToString:@"weibo"]) {
                [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.share_url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformTypeSinaWeibo];
            }else{
                markShare = SSDKPlatformSubTypeWechatTimeline;
                [self verifyWechat];
            }
        }else if (buttonIndex == 1){
            markShare = SSDKPlatformSubTypeWechatSession;
            [self verifyWechat];
        }
    }
}
#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"campaigns_receive"]) {
        [self.hudView dismiss];
        activityEntity = [JsonService getRBActivityEntity:jsonObject];
        
        NSString*status = [NSString stringWithFormat:@"%@",activityEntity.status];
        if ([status isEqualToString:@"running"]&&[NSString stringWithFormat:@"%@",activityEntity.uuid].length!=0) {
            // RB-获取活动-分享成功
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            if (markShare == SSDKPlatformTypeSinaWeibo) {
                [handler getRBActivityShareWithId:activityEntity.iid andSuntype:@"weibo"];
            }else{
                [handler getRBActivityShareWithId:activityEntity.iid andSuntype:@"wechat"];
            }
            return;
        }
        
        if (markShare == SSDKPlatformTypeSinaWeibo) {
            [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.share_url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformTypeSinaWeibo];
        }else{
            [self verifyWechat];
        }
    }
    
    if ([sender isEqualToString:@"campaign_invites_share"]) {
        [self.hudView dismiss];
        activityEntity = [JsonService getRBActivityEntity:jsonObject];
        
        //
        [[NSNotificationQueue defaultQueue] enqueueNotification:[NSNotification notificationWithName:NotificationRefreshHomeView object:nil] postingStyle:NSPostNow];
        if (markShare == SSDKPlatformTypeSinaWeibo) {
            [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.share_url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformTypeSinaWeibo];
        }else{
            [self verifyWechat];
        }
    }
    
    if ([sender isEqualToString:@"upload_screenshot"]) {
        activityEntity = [JsonService getRBActivityEntity:jsonObject];
        [self updateFooterView];
        [self setRefreshViewFinish];
    }
    
    if ([sender isEqualToString:@"campaign_invites_detail"] || [sender isEqualToString:@"campaigns_detail"]) {
        [self.hudView dismiss];
        self.navView.hidden = YES;
        if (activityEntity.alert.length > 0) {
            NSString * alertStr = activityEntity.alert;
            activityEntity = [JsonService getRBActivityEntity:jsonObject];
            activityEntity.alert = alertStr;
        }else{
            activityEntity = [JsonService getRBActivityEntity:jsonObject];
        }
        
        count ++;
        if ([activityEntity.campaign.per_budget_type isEqualToString:@"cpt"]||[activityEntity.campaign.per_budget_type isEqualToString:@"simple_cpi"]) {
            if ([activityEntity.status isEqualToString:@"running"]||[activityEntity.status isEqualToString:@"pending"]) {
                if (count == 1 && [LocalService getRBIsIncheck] == NO) {
                RBNewAlert * alert = [[RBNewAlert alloc]init];
                alert.delegate = self;
                [alert showWithArray:@[[NSString stringWithFormat:@"%@",activityEntity.campaign.remark],NSLocalizedString(@"R2500", @"知道了")] IsCountDown:@"YES" AndImageStr:@"rule" AndBigTitle:@"活动要求"];
                }
            }
        }
        if ([activityEntity.campaign.per_budget_type isEqualToString:@"post"]) {
            if ([activityEntity.status isEqualToString:@"running"]||[activityEntity.status isEqualToString:@"pending"]) {
                if (count == 1 && [LocalService getRBIsIncheck] == NO) {
                    RBNewAlert * alert = [[RBNewAlert alloc]init];
                    alert.delegate = self;
                    [alert showWithArray:@[[NSString stringWithFormat:@"%@",@"此任务要求分享后保留至少30分钟后截图并上传，否则不予以通过"],NSLocalizedString(@"R2500", @"知道了")] IsCountDown:@"YES" AndImageStr:@"rule" AndBigTitle:@"活动要求"];
                }
            }
        }
        //
        [self loadScrollView];
        [self loadWebView];
        [self updateFooterView];
        [self setRefreshViewFinish];
        [self changeStatusBar];
       
        if ([[NSString stringWithFormat:@"%@",activityEntity.status]isEqualToString:@"running"]) {
            if ([LocalService getRBfirstEnterCampain] == NO && [LocalService getRBIsIncheck] == NO) {
                UIView * backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
                backView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.68];
                backView.userInteractionEnabled = YES;
                UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeImageView:)];
                tap.numberOfTapsRequired = 1;
                tap.numberOfTouchesRequired = 1;
                [backView addGestureRecognizer:tap];
                //
                UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 19-565/2, 42-12.5, 565/2, 346/2)];
                imageView.image = [UIImage imageNamed:@"RBCampainDim"];
                [backView addSubview:imageView];
                imageView.userInteractionEnabled = YES;
                UIWindow * window = [[UIApplication sharedApplication].windows lastObject];
                [window addSubview:backView];
                [LocalService setRBfirstEnterCampain];
            }

        }
    }
    //验证用户是否绑定了微信
    if ([sender isEqualToString:@"big_v/detail"]) {
        kolEntity = [JsonService getRBKolDetailEntity:jsonObject];
        //记录是否绑定微信
        BOOL isBinding = NO;
        for (RBKOLSocialEntity * socialEntity in kolEntity.social_accounts) {
            if ([socialEntity.provider isEqualToString:@"wechat"]) {
                isBinding = YES;
            }
        }
        if (isBinding == YES) {
            //直接分享
            if (markShare == SSDKPlatformSubTypeWechatTimeline) {
                [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.share_url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformSubTypeWechatTimeline];
            }else if (markShare == SSDKPlatformSubTypeWechatSession){
                [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.share_url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:SSDKPlatformSubTypeWechatSession];
            }
        }else{
            //用户还没有绑定微信；
            
            [self bindWechat];
        }
    }
    if ([sender isEqualToString:@"update_social"]) {
        [self.hudView dismiss];
        [self shareCampaign:markShare];
    }
    if([sender isEqualToString:@"kols/identity_bind"]){
        [self.hudView show];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBKOLApplyWithProvider:editString andHomepage:nil andUsername:userName andPrice:@"1" andFollowers:@"10" andScreenshot:nil andUid:nil];
    }
}
//进行微信绑定
-(void)bindWechat{
    [self ThirdLoginWithTag:1];
}
//分享
-(void)shareCampaign:(NSUInteger)type{
    [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.share_url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:type];
}
- (void)ThirdLoginWithTag:(int)tag {
    SSDKPlatformType type = SSDKPlatformTypeWechat;
    NSString*provider=@"wechat";
    [ShareSDK cancelAuthorize:type];
    [ShareSDK getUserInfo:type
           onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error){
               NSLog(@"user:%@,credential:%@,rawData:%@",user,user.credential,user.rawData);
               if (state == SSDKResponseStateSuccess) {
                   userName = [NSString stringWithFormat:@"%@",user.nickname];
                   // RB-社交账号绑定
                   Handler*handler = [Handler shareHandler];
                   handler.delegate = self;
                  if (type == SSDKPlatformTypeWechat) {
                       NSString*unionid = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"unionid"]];
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       editString = @"微信";
                       // socialBtn.iconIV.image = [UIImage imageNamed:@"微信_ON.png"];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:unionid andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                   }
                   }
               if (state == SSDKResponseStateFail) {
                   if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                    //   [self.hudView showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                   } else {
                       [self.hudView showErrorWithStatus:NSLocalizedString(@"R1042", @"登录失败")];
                   }
               }
               if (state == SSDKResponseStateCancel) {
                   [self.hudView showErrorWithStatus:@"取消分享"];
                  // [self.hudView dismiss];
               }
           }];
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        if ([sender isEqualToString:@"bind_count"]) {
            RBNewAlert * alert = [[RBNewAlert alloc]init];
            alert.delegate = self;
            [alert showWithArray:@[[jsonObject objectForKey:@"detail"],@"知道了"] IsCountDown:nil AndImageStr:nil AndBigTitle:@"检测到绑定微信与登录微信不符"];
        }else{
            [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
            if ([[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"detail"]]isEqualToString:NSLocalizedString(@"R2054", @"该活动不存在")]) {
                dispatch_after(
                               dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)),
                               dispatch_get_main_queue(), ^{
                                   [self navLeftBtnAction:nil];
                               });
            }

        }
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
