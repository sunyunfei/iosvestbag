//
//  RBCampaignDetailKolsViewController.h
//  RB
//
//  Created by AngusNi on 7/5/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolDetailViewController.h"
#import "RBAdvertiserSwitchView.h"
#import "RBCampaignKolView.h"
#import "RBCampaignKolAnalysisView.h"


@interface RBCampaignDetailKolsViewController : RBBaseViewController
<RBBaseVCDelegate,RBCampaignKolViewDelegate,RBAdvertiserSwitchViewDelegate,UIScrollViewDelegate>
@property(nonatomic, strong) RBActivityEntity*activityEntity;

@end
