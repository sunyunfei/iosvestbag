//
//  RBCampaignInviteViewController.h
//  RB
//
//  Created by AngusNi on 4/20/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBPictureView.h"
#import "RBPhotoAsstesGroupViewController.h"
#import "RBCampaignCustomTableViewCell.h"
#import "TOWebViewController.h"

@interface RBCampaignInviteViewController : RBBaseViewController
<RBBaseVCDelegate,UIWebViewDelegate,UIScrollViewDelegate,RBActionSheetDelegate,RBAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,RBPhotoAsstesGroupViewControllerDelegate,UIAlertViewDelegate,RBNewAlertViewDelegate>
@property(nonatomic, strong) NSString*activityId;
@property(nonatomic, strong) NSString*campaignId;
@end
