//
//  RBCampaignRecruitApplyViewController.h
//  RB-TEST
//
//  Created by AngusNi on 30/09/2016.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolEditView.h"
#import "RBKolApplyInfoInputViewController.h"
#import "RBKolApplyInfoInputTextViewController.h"
#import "RBKolApplyInfoInterestViewController.h"
#import "RBKolApplySocialViewController.h"
@interface RBCampaignRecruitApplyViewController : RBBaseViewController
<RBBaseVCDelegate,RBActionSheetDelegate,RBPictureUploadDelegate,TextViewKeyBoardDelegate>
@property(nonatomic, strong) RBActivityEntity*activityEntity;
@end
