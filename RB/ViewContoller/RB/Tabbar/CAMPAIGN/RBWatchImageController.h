//
//  RBWatchImageController.h
//  RB
//
//  Created by RB8 on 2018/1/11.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBWatchImageController : RBBaseViewController<RBBaseVCDelegate,HandlerDelegate>
@property(nonatomic,copy)NSString * campainID;
@property(nonatomic,strong)NSArray * imgArr;
@property(nonatomic,strong)NSArray * nameArr;
@end
