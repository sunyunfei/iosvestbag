//
//  RBCampaignRecruitAutoViewController.m
//  RB
//
//  Created by AngusNi on 16/9/12.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBCampaignRecruitAutoViewController.h"
#import "RBUploadImageViewController.h"
#import "RBWatchImageController.h"
@interface RBCampaignRecruitAutoViewController () {
    UIScreenEdgePanGestureRecognizer *sepgr;
    UIScrollView*subScrollView;
    UIImageView*bgIV;
    UIScrollView*scrollviewn;
    UIWebView*webviewn;
    UIView*webViewNV;
    UIView*footerView;
    UIButton*footerBtn;
    UIButton*footerRightBtn;
    UILabel*footerLineLabel;
    RBActivityEntity*activityEntity;
    UILabel *tagTimeLabel;
    TTTAttributedLabel *tagTextLabel;
    UILabel *tagLabel;
   // NSTimer*startTimer;
}
@end

@implementation RBCampaignRecruitAutoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = @"ROBIN8";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getActivityData) name:@"refreshCampainAuto" object:nil];
    [self changeStatusBar];
    [self getActivityData];
    //
    [TalkingData trackPageBegin:@"campaign-recruit-auto"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    //
    [TalkingData trackPageEnd:@"campaign-recruit-auto"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationDismissView {
    [self changeStatusBar];
}

#pragma mark - Data Delegate
- (void)getActivityData {
    [self.hudView show];
    // RB-获取活动详情
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    if (self.activityId!=nil) {
        [handler getRBActivityDetailWithId:self.activityId];
    }
    if (self.campaignId!=nil) {
        [handler getRBCampaignDetailWithId:self.campaignId];
    }
}

- (void)getActivityEnjoy {
    [self.hudView show];
    // RB-获取活动参与
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    if (self.campaignId!=nil) {
        [handler getRBCampaignEnjoyWithId:self.campaignId];
    }
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    scrollviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf getActivityData];
    }];
}

- (void)setRefreshViewFinish {
    scrollviewn.contentOffset = CGPointMake(0, 0);
    [scrollviewn.mj_header endRefreshing];
}

#pragma mark - UIScreenEdgePanGestureRecognizer Delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (void)edgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender {
    [self navLeftBtnAction:nil];
}
-(void)navRightBtnAction:(id)sender{
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.delegate = self;
    [alert showWithCampainTopArray:@[@"微博",@"微信",@"朋友圈",@"QQ好友",@"QQ空间"] AndbottomArray:@[@"icon_weibo",@"icon_wechat",@"icon_wechat_friends",@"icon_qq",@"icon_qzone"] AndShareWith:@"h5"];
    
    alert.shareTitle = activityEntity.campaign.name;
    alert.shareContent = activityEntity.campaign.idescription;
    alert.shareImageUrl = @"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png";
    alert.shareurl = [NSString stringWithFormat:@"%@",activityEntity.campaign.url];
    
    alert.shareH5Title = [NSString stringWithFormat:@"Robin8邀请你成为%@的代言人",activityEntity.campaign.brand_name];
    alert.shareH5Url = [NSString stringWithFormat:@"http://robin8.net/wechat_campaign/campaign_page?campaign_id=%@",self.campaignId];
    alert.shareH5Content = [NSString stringWithFormat:@"http://robin8.net/wechat_campaign/campaign_page?campaign_id=%@",self.campaignId];
//    RBNewAlert * alert = [[RBNewAlert alloc]init];
//    alert.delegate = self;
//    [alert showWithShareArray:nil];
//    alert.shareTitle = activityEntity.campaign.name;
//    alert.shareContent = activityEntity.campaign.idescription;
//    alert.shareImageUrl = @"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png";
//    alert.shareurl = [NSString stringWithFormat:@"%@",activityEntity.campaign.url];
//    
//    alert.shareH5Title = [NSString stringWithFormat:@"Robin8邀请你成为%@的代言人",activityEntity.campaign.brand_name];
//    alert.shareH5Url = [NSString stringWithFormat:@"http://robin8.net/wechat_campaign/campaign_page?campaign_id=%@",self.campaignId];
//    alert.shareH5Content = [NSString stringWithFormat:@"http://robin8.net/wechat_campaign/campaign_page?campaign_id=%@",self.campaignId];
}

#pragma mark - ContentView Delegate
-(void)loadScrollView {
    [scrollviewn removeFromSuperview];
    scrollviewn = nil;
    sepgr = nil;
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth , ScreenHeight-45.0)];
    scrollviewn.backgroundColor = [UIColor whiteColor];
    scrollviewn.bounces = YES;
    scrollviewn.pagingEnabled = YES;
    scrollviewn.userInteractionEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.delegate = self;
    [self.view addSubview:scrollviewn];
    if ([LocalService getRBIsIncheck] == YES) {
        scrollviewn.scrollEnabled = NO;
    }
    [self setRefreshHeaderView];
    //
    sepgr = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(edgePanGesture:)];
    sepgr.delegate = self;
    sepgr.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:sepgr];
    //
    subScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-45.0)];
    subScrollView.backgroundColor = [UIColor whiteColor];
    subScrollView.bounces = YES;
    subScrollView.pagingEnabled = NO;
    subScrollView.userInteractionEnabled = YES;
    subScrollView.showsHorizontalScrollIndicator = NO;
    subScrollView.showsVerticalScrollIndicator = NO;
    [scrollviewn addSubview:subScrollView];
    //
    bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth , ScreenWidth*8.0/16.0)];
//    [bgIV yy_setImageWithURL:[NSURL URLWithString:activityEntity.campaign.img_url] placeholder:PlaceHolderImage options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation completion:NULL];
    [bgIV setImageWithURL:[NSURL URLWithString:activityEntity.campaign.img_url] placeholder:PlaceHolderImage options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation completion:NULL];
    bgIV.userInteractionEnabled = YES;
    [subScrollView addSubview:bgIV];
    //
    UILabel*bgLabel = [[UILabel alloc]initWithFrame:bgIV.bounds];
    bgLabel.backgroundColor = SysColorCover;
    [bgIV addSubview:bgLabel];
    // CPI CPA提示
    NSArray *textArray = @[activityEntity.campaign.remark,activityEntity.campaign.remark,activityEntity.campaign.remark];
    YFRollingLabel*rollingLabel = [[YFRollingLabel alloc] initWithFrame:CGRectMake(0, bgIV.bottom-40.0, bgIV.width, 40)  textArray:textArray font:font_cu_15 textColor:[UIColor whiteColor]];
    rollingLabel.speed = 1;
    [rollingLabel setOrientation:RollingOrientationLeft];
    [rollingLabel setInternalWidth:rollingLabel.width/3.0];
    [bgIV addSubview:rollingLabel];
    //
    UIView*scrollViewNV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, NavHeight)];
    scrollViewNV.userInteractionEnabled = YES;
    [bgIV addSubview:scrollViewNV];
    //
    UILabel*navCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(44.0, StatusHeight, self.view.width-44.0*2.0, 44.0)];
    if ([LocalService getRBIsIncheck] == YES) {
        navCenterLabel.text = @"Robin8";
    }else{
        navCenterLabel.text = [NSString stringWithFormat:@"%@",activityEntity.campaign.brand_name];
    }
    navCenterLabel.font = font_cu_17;
    navCenterLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    navCenterLabel.textAlignment = NSTextAlignmentCenter;
    navCenterLabel.userInteractionEnabled = YES;
    [scrollViewNV addSubview:navCenterLabel];
    //
    UILabel*navLeftLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, StatusHeight+(44.0-20.0)/2.0, 20.0, 20.0)];
    navLeftLabel.font = font_icon_(20.0);
    navLeftLabel.text = Icon_bar_back;
    navLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    [scrollViewNV addSubview:navLeftLabel];
    //
    UIButton*navLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navLeftBtn.frame = CGRectMake(0, StatusHeight, 44.0, 44.0);
    [navLeftBtn addTarget:self
                   action:@selector(navLeftBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    [scrollViewNV addSubview:navLeftBtn];
    //
    UIButton * navRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navRightBtn.frame = CGRectMake(ScreenWidth-44.0, StatusHeight, 44.0, 44.0);
    [navRightBtn addTarget:self action:@selector(navRightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [navRightBtn setImage:[UIImage imageNamed:@"RBShareWhite"] forState:UIControlStateNormal];
    [scrollViewNV addSubview:navRightBtn];
    if ([LocalService getRBIsIncheck] == YES) {
        navRightBtn.hidden = YES;
    }
    //
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, bgIV.bottom+16.0, scrollviewn.width-32, 18)];
    titleLabel.font = font_cu_15;
    titleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 2;
    [subScrollView addSubview:titleLabel];
    [titleLabel sizeToFit];
    titleLabel.frame = CGRectMake(16, bgIV.bottom+45.0, ScreenWidth - 32, titleLabel.size.height);
    titleLabel.text = [NSString stringWithFormat:@"%@",activityEntity.campaign.name];
    //
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, titleLabel.bottom+6.0, scrollviewn.width, 13)];
    subTitleLabel.font = font_11;
    subTitleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:subTitleLabel];
    if ([LocalService getRBIsIncheck] == YES) {
        subTitleLabel.text = @"Robin8";
        
    }else{
        subTitleLabel.text = [NSString stringWithFormat:@"%@",activityEntity.campaign.brand_name];
    }
    [Utils getUILabel:subTitleLabel withSpacing:6.0];
    //
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subTitleLabel.bottom+6.0, scrollviewn.width, 13)];
    timeLabel.font = font_11;
    timeLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:timeLabel];
    //
    if(activityEntity!=nil) {
        NSString *sDate = [[NSString stringWithFormat:@"%@",activityEntity.campaign.start_time]componentsSeparatedByString:@"T"][0];
        sDate = [sDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
        NSString *sDate1 = [[NSString stringWithFormat:@"%@",activityEntity.campaign.start_time]componentsSeparatedByString:@"T"][1];
        sDate1 = [[NSString stringWithFormat:@"%@",sDate1]componentsSeparatedByString:@"+"][0];
        NSString *eDate = [[NSString stringWithFormat:@"%@",activityEntity.campaign.deadline]componentsSeparatedByString:@"T"][0];
        eDate = [eDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
        NSString *eDate1 = [[NSString stringWithFormat:@"%@",activityEntity.campaign.deadline]componentsSeparatedByString:@"T"][1];
        eDate1 = [[NSString stringWithFormat:@"%@",eDate1]componentsSeparatedByString:@"+"][0];
        timeLabel.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",sDate,sDate1,eDate,eDate1];
        CGSize timeLabelSize = [Utils getUIFontSizeFitW:timeLabel];
        timeLabel.width = timeLabelSize.width+8.0;
        timeLabel.left = (ScreenWidth-timeLabel.width-8.0)/2.0;
    }
    //
    UITextView*contentTV = [[UITextView alloc]initWithFrame:CGRectMake(20, timeLabel.bottom+5.0, scrollviewn.width-20.0*2, subScrollView.height-35.0-64.0-40.0-18.0-14.0-40.0-timeLabel.bottom-10.0)];
    contentTV.font = font_13;
    contentTV.text = [contentTV.text stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    contentTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    contentTV.editable = NO;
    contentTV.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:contentTV];
    contentTV.text = activityEntity.campaign.idescription;
    CGSize contentTVSize = [Utils getUITextViewSizeFitH:contentTV withLineSpacing:6.0];
    if (contentTVSize.height+6.0*3.0 < contentTV.height) {
        contentTV.height = contentTVSize.height+6.0*3.0;
    }
    // 底部框
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subScrollView.height-35.0-64.0-40.0-18.0-14.0-40.0, scrollviewn.width, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [subScrollView addSubview:lineLabel];
    //
    tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subScrollView.height-35.0-64.0-40.0-18.0-14.0, scrollviewn.width, 18.0)];
    tagLabel.top = lineLabel.bottom+10.0;
    tagLabel.font = font_cu_15;
    tagLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tagLabel.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:tagLabel];
    //
    UIView*tagView = [[UIImageView alloc] initWithFrame:CGRectMake(20, subScrollView.height-35.0-64.0-40.0, ScreenWidth-2*20.0, 64.0)];
    tagView.top = tagLabel.bottom+10.0;
    tagView.backgroundColor = [Utils getUIColorWithHexString:SysColorSubBlack];
    [subScrollView addSubview:tagView];
    //
    tagTextLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, (tagView.height-18.0-15.0-5.0)/2.0, tagView.width, 18.0)];
    tagTextLabel.font = font_cu_15;
    tagTextLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    tagTextLabel.textAlignment = NSTextAlignmentCenter;
    [tagView addSubview:tagTextLabel];
    //
    tagTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, tagTextLabel.bottom+5.0, tagView.width, 15)];
    tagTimeLabel.font = font_11;
    tagTimeLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
    tagTimeLabel.textAlignment = NSTextAlignmentCenter;
    [tagView addSubview:tagTimeLabel];
    //
    UIButton*kolBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    kolBtn.frame = CGRectMake(tagView.left, tagView.bottom+10.0, tagView.width, 60.0);
    [kolBtn addTarget:self
               action:@selector(kolBtnAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [subScrollView addSubview:kolBtn];
    //
    UILabel*kolLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kolBtn.width, 15)];
    kolLabel.font = font_11;
    kolLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    kolLabel.textAlignment = NSTextAlignmentCenter;
    [kolBtn addSubview:kolLabel];
    kolLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R4014", @"已有%@人参加"),activityEntity.invitees_count];
    //
    float temw = 40.0;
    float temg = 18.0;
    if(ScreenHeight<=568) {
        temw = 35.0;
        temg = 15.0;
    }
    int num = (int)activityEntity.invitees.count;
    num = num+1;
    if (num>5) {
        num=5;
    }
    float temleft = (kolBtn.width-temw*num-temg*(num-1))/2.0;
    for (int i=0; i<num; i++) {
        if(i==(num-1)) {
            UILabel*kolIVLabel = [[UILabel alloc] initWithFrame:CGRectMake(temleft+(temw+temg)*i, kolLabel.bottom+5.0, temw, temw)];
            kolIVLabel.font = font_icon_(temw-1);
            kolIVLabel.text = Icon_btn_more;
            kolIVLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            [kolBtn addSubview:kolIVLabel];
        } else {
            RBUserEntity*userEntity = activityEntity.invitees[i];
            UIImageView*kolIV = [[UIImageView alloc]initWithFrame:CGRectMake(temleft+(temw+temg)*i, kolLabel.bottom+5.0, temw, temw)];
            [kolIV sd_setImageWithURL:[NSURL URLWithString:userEntity.avatar_url] placeholderImage:PlaceHolderUserImage];
            if (userEntity.avatar_url.length==0) {
                kolIV.layer.borderWidth = 1.0f;
                kolIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
            }
            kolIV.layer.cornerRadius = kolIV.width/2.0;
            kolIV.layer.masksToBounds = YES;
            [kolBtn addSubview:kolIV];
        }
    }
    // 活动详情
    UIButton*detailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    detailBtn.frame = CGRectMake(0, subScrollView.height-26.0, ScreenWidth, 26.0);
    [detailBtn addTarget:self
                  action:@selector(detailBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [subScrollView addSubview:detailBtn];
    if ([LocalService getRBIsIncheck] == YES) {
        detailBtn.hidden = YES;
    }
    //
    UILabel *btnLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 13.0)];
    btnLabel.font = font_11;
    btnLabel.text = NSLocalizedString(@"R2026", @"推广内容详情");
    btnLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    btnLabel.textAlignment=NSTextAlignmentCenter;
    [detailBtn addSubview:btnLabel];
    //
    UILabel*downLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth-13.0)/2.0, btnLabel.bottom, 13.0, 13.0)];
    downLabel.font = font_icon_(13.0);
    downLabel.text = Icon_btn_down;
    downLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [detailBtn addSubview:downLabel];
    //
    [self updateText];
}

-(void)updateText {
    NSString*avail_click = [NSString stringWithFormat:@"%@",activityEntity.avail_click];
    NSString*earn_money = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:activityEntity.earn_money]];
    NSString*per_action_budget = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:activityEntity.campaign.per_action_budget]];
    NSString*status = [NSString stringWithFormat:@"%@",activityEntity.status];
    tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2016",@"招募"),per_action_budget];
    tagTextLabel.frame = CGRectMake(0, 0, ScreenWidth-2*20.0, 64.0);
    tagTimeLabel.hidden = YES;
    tagTextLabel.text = NSLocalizedString(@"R2055", @"参与招募活动获得奖励");
    if (([status isEqualToString:@"pending"] || [status isEqualToString:@"running"]) && (![[Utils getUIDateCompareNow:[NSString stringWithFormat:@"%@",activityEntity.campaign.recruit_end_time]] isEqualToString:NSLocalizedString(@"R2056", @"活动已结束")])) {
        tagTimeLabel.hidden = NO;
        tagTextLabel.frame = CGRectMake(0, (64.0-18.0-15.0-5.0)/2.0, ScreenWidth-2*20.0, 18.0);
        tagTimeLabel.text = [[Utils getUIDateCompareNow:[NSString stringWithFormat:@"%@",activityEntity.campaign.recruit_end_time]] stringByReplacingOccurrencesOfString:NSLocalizedString(@"R2065", @"距结束") withString:NSLocalizedString(@"R2066", @"距报名截止")];
    }
    if ([status isEqualToString:@"approved"]||[status isEqualToString:@"finished"]) {
        tagTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R2030", @"已获得 %@ 次点击,即将赚￥%@"),avail_click,earn_money];
    }
    if ([status isEqualToString:@"settled"]||[status isEqualToString:@"missed"]||[status isEqualToString:@"rejected"]) {
        tagTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R2031", @"已获得 %@ 次点击,已赚￥%@"),avail_click,earn_money];
    }
    [tagTextLabel setText:tagTextLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:avail_click options:NSCaseInsensitiveSearch];
         NSRange range1 = [[mStr string] rangeOfString:[NSString stringWithFormat:@"￥"] options:NSCaseInsensitiveSearch];
         NSRange range2 = [[mStr string] rangeOfString:earn_money options:NSCaseInsensitiveSearch];
         CTFontRef font = CTFontCreateWithName((CFStringRef)font_cu_17.fontName, font_cu_17.pointSize, NULL);
         [mStr addAttributes:@{(NSString *)kCTFontAttributeName:(__bridge id)font,(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range];
         [mStr addAttributes:@{(NSString *)kCTFontAttributeName:(__bridge id)font,(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range2];
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range1];
         CFRelease(font);
         return mStr;
     }];
}

- (void)loadWebView {
    [webViewNV removeFromSuperview];
    [webviewn removeFromSuperview];
    webViewNV = nil;
    webviewn = nil;
    //
    webViewNV = [[UIView alloc]initWithFrame:CGRectMake(0, scrollviewn.height, self.view.width, 64)];
    webViewNV.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    webViewNV.userInteractionEnabled = YES;
    [scrollviewn addSubview:webViewNV];
    //
    UILabel *navCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20.0, self.view.width, 44)];
    navCenterLabel.text = NSLocalizedString(@"R2026", @"推广内容详情");
    navCenterLabel.font = font_cu_17;
    navCenterLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    navCenterLabel.textAlignment = NSTextAlignmentCenter;
    navCenterLabel.userInteractionEnabled = YES;
    [webViewNV addSubview:navCenterLabel];
    //
    UILabel*navLeftLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 20.0+(44.0-20.0)/2.0, 20.0, 20.0)];
    navLeftLabel.font = font_icon_(20.0);
    navLeftLabel.text = Icon_bar_back;
    navLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [webViewNV addSubview:navLeftLabel];
    //
    UIButton*navLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navLeftBtn.frame = CGRectMake(0, 20.0, 44.0, 44.0);
    [navLeftBtn addTarget:self
                   action:@selector(webLeftBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    [webViewNV addSubview:navLeftBtn];
    //
    UILabel*navLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, webViewNV.height-0.5, webViewNV.width, 0.5)];
    navLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBarLine];
    [webViewNV addSubview:navLineLabel];
    //
    webviewn = [[UIWebView alloc] initWithFrame:CGRectMake(10, webViewNV.bottom, ScreenWidth-20.0, ScreenHeight-45.0-64.0)];
    webviewn.delegate = self;
    webviewn.scalesPageToFit = YES;
    [scrollviewn addSubview:webviewn];
    //
    NSString*url = [activityEntity.campaign.url stringByReplacingOccurrencesOfString:@"#wechat_redirect" withString:@""];
    [webviewn loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, webviewn.bottom)];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([[request.URL.absoluteString componentsSeparatedByString:@"https://itunes.apple.com"]count]>1) {
        return NO;
    }
    if ([[request.URL.absoluteString componentsSeparatedByString:@".apk"]count]>1) {
        return NO;
    }
    return YES;
}

#pragma mark - FooterView Delegate
- (void)updateFooterView {
    [footerView removeFromSuperview];
    footerView = nil;
    //
    footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
    //
    footerRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerRightBtn.frame = CGRectMake(footerView.width/2.0, 0, footerView.width/2.0, footerView.height);
    footerRightBtn.titleLabel.font = font_cu_15;
    [footerRightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorgrayText] forState:UIControlStateNormal];
    [footerView addSubview:footerRightBtn];
    //
//    footerLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(footerView.width/2.0-0.5f, 10.0, 1, 45.0-20.0)];
//    footerLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
//    [footerView addSubview:footerLineLabel];
    //
    [self updateText];
    footerBtn.enabled = NO;
    footerBtn.frame = CGRectMake(0, 0, ScreenWidth, 45.0);
    footerRightBtn.frame = CGRectMake(ScreenWidth/2.0, 0, ScreenWidth/2.0, 45.0);
    footerRightBtn.hidden = YES;
    footerLineLabel.hidden = YES;
    //
    NSString*status = [NSString stringWithFormat:@"%@",activityEntity.status];
    NSString*per_action_type = [NSString stringWithFormat:@"%@",activityEntity.campaign.per_action_type];
    //***********************************************
    status = @"approved";
    activityEntity.screenshot = @"1";
    //***********************************************
    if ([status isEqualToString:@"pending"]) {
        [footerBtn setTitle:NSLocalizedString(@"R2032", @"活动未开始") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
        footerBtn.enabled = NO;
        return;
    } else if ([status isEqualToString:@"running"]) {
        if([[Utils getUIDateCompareNow:[NSString stringWithFormat:@"%@",activityEntity.campaign.recruit_end_time]] isEqualToString:NSLocalizedString(@"R2056", @"活动已结束")]) {
            [footerBtn setTitle:NSLocalizedString(@"R2057", @"报名已结束") forState:UIControlStateNormal];
            footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
            footerBtn.enabled = NO;
        } else {
            [footerBtn setTitle:NSLocalizedString(@"R2070", @"立即报名") forState:UIControlStateNormal];
            footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
            footerBtn.enabled = YES;
            [footerBtn addTarget:self
                          action:@selector(footerApplyApproveBtnAction:)
                forControlEvents:UIControlEventTouchUpInside];
        }
        return;
    } else if ([status isEqualToString:@"applying"]) {
        [footerBtn setTitle:NSLocalizedString(@"R2071", @"活动已报名,正在审核") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
        footerBtn.enabled = NO;
        return;
    } else if ([status isEqualToString:@"approved"] || [status isEqualToString:@"finished"]) {
        if(![[Utils getUIDateCompareNow:[NSString stringWithFormat:@"%@",activityEntity.campaign.start_time]] isEqualToString:NSLocalizedString(@"R2056", @"活动已结束")]) {
            if([per_action_type isEqualToString:@"weibo"]||[per_action_type isEqualToString:@"wechat"]||[per_action_type isEqualToString:@"qq"]) {
                [footerBtn setTitle:NSLocalizedString(@"R2033", @"立即分享") forState:UIControlStateNormal];
                footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                footerBtn.enabled = YES;
                [footerBtn addTarget:self
                              action:@selector(footerShareApproveBtnAction:)
                    forControlEvents:UIControlEventTouchUpInside];
            } else {
                [footerBtn setTitle:NSLocalizedString(@"R2072", @"活动报名成功") forState:UIControlStateNormal];
                footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
                footerBtn.enabled = NO;
            }
            return;
        } else {
            if ([NSString stringWithFormat:@"%@",activityEntity.screenshot].length==0) {
                if ([NSString stringWithFormat:@"%@",activityEntity.can_upload_screenshot].intValue==1) {
                    [footerBtn setTitle:NSLocalizedString(@"R2034", @"上传截图") forState:UIControlStateNormal];
                    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                    footerBtn.enabled = YES;
                    [footerBtn addTarget:self
                                  action:@selector(footerUploadBtnAction:)
                        forControlEvents:UIControlEventTouchUpInside];
                    //
                    footerBtn.frame = CGRectMake(0, 0, ScreenWidth/2.0, 45.0);
                    footerRightBtn.frame = CGRectMake(ScreenWidth/2.0, 0, ScreenWidth/2.0, 45.0);
                    footerRightBtn.hidden = NO;
                    footerLineLabel.hidden = NO;
                    [footerRightBtn setTitle:NSLocalizedString(@"R2035", @"再次分享") forState:UIControlStateNormal];
                    footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorbackgray];
                    [footerRightBtn addTarget:self
                                       action:@selector(footerShareApproveBtnAction:)
                             forControlEvents:UIControlEventTouchUpInside];
                    return;
                } else {
                    [footerBtn setTitle:NSLocalizedString(@"R2033", @"立即分享") forState:UIControlStateNormal];
                    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                    footerBtn.enabled = YES;
                    [footerBtn addTarget:self
                                  action:@selector(footerShareApproveBtnAction:)
                        forControlEvents:UIControlEventTouchUpInside];
                    footerLineLabel.hidden = YES;
                    return;
                }
            } else {
                NSString*temp = @"";
                NSString*img_status = [NSString stringWithFormat:@"%@",activityEntity.img_status];
                //******************************************
                img_status = @"rejected";
                //******************************************
                if ([img_status isEqualToString:@"passed"]) {
                    temp = NSLocalizedString(@"R2039", @"审核通过");
                    [footerBtn setTitle:[NSString stringWithFormat:@"%@-%@",temp,NSLocalizedString(@"R2042", @"查看截图")] forState:UIControlStateNormal];
                    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                    footerBtn.enabled = YES;
                    [footerBtn addTarget:self
                                  action:@selector(footerSeeBtnAction:)
                        forControlEvents:UIControlEventTouchUpInside];
                    return;
                } else if ([img_status isEqualToString:@"pending"]) {
                    temp = NSLocalizedString(@"R2040", @"审核中");
                    [footerRightBtn setTitle:NSLocalizedString(@"R2044", @"重新上传") forState:UIControlStateNormal];
                    [footerRightBtn addTarget:self
                                       action:@selector(footerUploadBtnAction:)
                             forControlEvents:UIControlEventTouchUpInside];
                    [footerBtn setTitle:[NSString stringWithFormat:@"%@-%@",temp,NSLocalizedString(@"R2042", @"查看截图")] forState:UIControlStateNormal];
                } else if ([img_status isEqualToString:@"rejected"]) {
                    temp = NSLocalizedString(@"R2041", @"审核拒绝");
                    [footerRightBtn setTitle:NSLocalizedString(@"R2044", @"重新上传") forState:UIControlStateNormal];
                    [footerRightBtn addTarget:self
                                       action:@selector(footerUploadBtnAction:)
                             forControlEvents:UIControlEventTouchUpInside];
                    [footerBtn setTitle:[NSString stringWithFormat:@"%@-%@",temp,NSLocalizedString(@"R2043", @"查看原因")] forState:UIControlStateNormal];
                }
                footerBtn.frame = CGRectMake(0, 0, ScreenWidth/2.0, 45.0);
                footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
                footerBtn.enabled = YES;
                [footerBtn addTarget:self
                              action:@selector(footerSeeBtnAction:)
                    forControlEvents:UIControlEventTouchUpInside];
                footerLineLabel.hidden = NO;
                footerRightBtn.frame = CGRectMake(ScreenWidth/2.0, 0, ScreenWidth/2.0, 45.0);
                footerRightBtn.hidden = NO;
                footerRightBtn.enabled = YES;
                footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorbackgray];
                return;
            }
        }
    } else if ([status isEqualToString:@"settled"]) {
        [footerBtn setTitle:NSLocalizedString(@"R2045", @"活动已完成") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
        footerBtn.enabled = NO;
        return;
    } else if ([status isEqualToString:@"rejected"]) {
        [footerBtn setTitle:NSLocalizedString(@"R2074", @"报名审核失败") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
        footerBtn.enabled = NO;
        return;
    } else if ([status isEqualToString:@"missed"]) {
        [footerBtn setTitle:NSLocalizedString(@"R2075", @"活动已错失") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
        footerBtn.enabled = NO;
        return;
    }
//    else if ([status isEqualToString:@"countdown"]){
//        [footerBtn setTitle:NSLocalizedString(@"R2520", @"活动即将开始") forState:UIControlStateNormal];
//        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
//        footerBtn.enabled = NO;
//        return;
//    }
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollviewn.mj_header isRefreshing]==YES) {
        scrollviewn.contentOffset = CGPointMake(0, 0);
        return;
    }
    [self changeStatusBar];
}

-(void)changeStatusBar {
    if (scrollviewn.contentOffset.y >= scrollviewn.height) {
        [[UIApplication sharedApplication]
         setStatusBarStyle:UIStatusBarStyleDefault];
    } else {
        [[UIApplication sharedApplication]
         setStatusBarStyle:UIStatusBarStyleLightContent];
    }
}

#pragma mark - UIButton Delegate
- (void)navLeftBtnAction:(UIButton *)sender {
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)webLeftBtnAction:(UIButton *)sender {
    [self changeStatusBar];
    [scrollviewn scrollRectToVisible:CGRectMake(0, 0, scrollviewn.width, scrollviewn.height) animated:YES];
}

- (void)detailBtnAction:(UIButton *)sender {
    [self changeStatusBar];
    [scrollviewn scrollRectToVisible:CGRectMake(0, scrollviewn.height, scrollviewn.width, scrollviewn.height) animated:YES];
}

- (void)kolBtnAction:(UIButton *)sender {
    RBCampaignDetailKolsViewController *toview = [[RBCampaignDetailKolsViewController alloc] init];
    toview.activityEntity = activityEntity;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}
#pragma mark 上传图片和截图示例
-(void)uploadImage:(NSString*)imgPath{
    RBUploadImageViewController * upView = [[RBUploadImageViewController alloc]init];
    upView.hidesBottomBarWhenPushed = YES;
    upView.mark = imgPath;
    if ([imgPath isEqualToString:@"watch"]) {
        upView.imgArr = activityEntity.screenshots;
    }else{
        upView.imgArr = activityEntity.cpi_example_screenshots;
    }
    upView.nameArr = activityEntity.screenshot_comment;
    upView.campainID = activityEntity.iid;
    [self.navigationController pushViewController:upView animated:YES];
}
- (void)footerSeeBtnAction:(UIButton *)sender {
    if([self isVisitorLogin]==YES){
        return;
    };
    //
    activityEntity.reject_reason = @"哈哈哈哈哈";
    if(activityEntity.reject_reason.length!=0) {
        //***************************
        RBNewAlert * alert = [[RBNewAlert alloc]init];
        alert.delegate = self;
        alert.tag = 13000;
        [alert showWithArray:@[[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2050", @"图片审核不通过:\n"),activityEntity.reject_reason],NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R2042", @"查看截图")] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
        return;
    }
    //
//    TOWebViewController *toview = [[TOWebViewController alloc] init];
//    toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.screenshot]];
//    toview.title = NSLocalizedString(@"R2042", @"查看截图");
//    toview.showPageTitles = NO;
//    toview.hidesBottomBarWhenPushed = YES;
//    toview.navigationButtonsHidden = YES;
//    toview.campainID = activityEntity.iid;
//    [self.navigationController pushViewController:toview animated:YES];
//    RBWatchImageController * toview = [[RBWatchImageController alloc]init];
//    toview.campainID = activityEntity.iid;
//    toview.hidesBottomBarWhenPushed = YES;
//    toview.imgArr = activityEntity.screenshots;
//    toview.nameArr = activityEntity.screenshot_comment;
//    [self.navigationController pushViewController:toview animated:YES];
//    [self pushUploadController:@"watch"];
     [self uploadImage:@"watch"];
}

- (void)footerUploadBtnAction:(UIButton *)sender {
    if([self isVisitorLogin]==YES){
        return;
    };
    //
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    actionSheet.tag = 12000;
    [actionSheet showWithArray:@[NSLocalizedString(@"R2034", @"上传截图"),NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R1011", @"取消")]];
}

- (void)footerShareApproveBtnAction:(UIButton *)sender {
    if([self isVisitorLogin]==YES){
        return;
    };
    if ([NSString stringWithFormat:@"%@",activityEntity.uuid].length==0) {
        [self getActivityEnjoy];
    } else {
        NSString*per_action_type = [NSString stringWithFormat:@"%@",activityEntity.campaign.per_action_type];
        SSDKPlatformType type = SSDKPlatformSubTypeWechatTimeline;
        if([per_action_type isEqualToString:@"weibo"]) {
            type = SSDKPlatformTypeSinaWeibo;
        } else if ([per_action_type isEqualToString:@"qq"]) {
            type = SSDKPlatformSubTypeQZone;
        } else {
            type = SSDKPlatformSubTypeWechatTimeline;
        }
        [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.share_url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:type];
    }
}

- (void)footerApplyApproveBtnAction:(UIButton *)sender {
    if([self isVisitorLogin]==YES){
        return;
    };
    if(activityEntity.campaign.is_applying_note_required.intValue==1||activityEntity.campaign.is_applying_picture_required.intValue==1) {
        RBCampaignRecruitApplyViewController *toview = [[RBCampaignRecruitApplyViewController alloc] init];
        toview.activityEntity = activityEntity;
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
        return;
    }
    if ([LocalService getRBLocalDataUserAgree].length!=0) {
        // RB-报名活动
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBActivityApplyWithId:activityEntity.campaign.iid andNote:@"" andPicture:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Robin8免责声明" message:@"Robin8提醒用户在使用Robin8平台（robin8.com）服务以及robin8 APP（以下简称“Robin8服务”）之前，仔细阅读并理解以下条款，尤其是限制用户权利的条款。使用Robin8服务的行为将视为用户已经同意本声明的全部内容，并将在使用Robin8服务的过程中遵守执行。\n1、Robin8是供有广告投放推广需求的客户（下称“广告主”）和能够提供广告发布推广服务的自媒体账号或个人账户（以下简称“KOL”）交易的第三方平台。Robin8平台展示广告主提供的广告内容、KOL提供的账号名称、粉丝数等原始数据，供广告主、KOL根据需要甄选符合自己需求的服务对象进行交易。\n2、Robin8将对广告主的身份、广告内容、KOL的身份信息、账号名称等内容进行审查，筛选出Robin8认为不真实的信息，但由于网络平台的特殊性及微播易的局限性，Robin8无法筛选出所有的不真实信息并做处理，但将尽最大努力保证所有信息的真实性。\n3、广告主应如实填写广告信息，对广告内容的真实性、准确性、合法性、推广后的影响负责，Robin8倡导各广告主诚信发布广告、遵守法律法规以共同维护网络环境。\n4、KOL应如实填写并及时更新对应平台的相应数据，包括但不限于账号名称、地域、账号粉丝数、受众信息等，并反馈真实的推广效果，Robin8倡导KOL诚信服务、遵守法律法规以共同维护网络环境。\n5、KOL应综合考虑广告内容的真实性、准确性、合法性等因素后选择是否接单，一旦选择接单则应自行承担广告内容发布后对其账号造成的一切影响。广告主与KOL因广告发布产生的纠纷由双方自行解决。\n6、Robin8基于广告主需求向其推荐的账号，仅是微播易基于账号自身数据分析判断做出的推荐，由广告主最终确认是否使用Robin8推荐的账号。\n7、Robin8将采取必要手段为用户信息保密，但基于技术限制等，Robin8并不能保证用户的信息完全不被泄露。\n8、用户应同意Robin8有限度的使用其信息，包括但不限于向用户及时发送重要通知或派发订单、Robin8内部进行审计、数据分析和研究等Robin8自行判断为合理使用的情形及法律法规要求等。\n9、用户认为Robin8的其它用户侵犯其权利的，可向Robin8提供权利证明文件、身份证明文件等，Robin8审核评估后将采取适当措施予以处理。" delegate:self cancelButtonTitle:@"同意" otherButtonTitles:@"拒绝",nil];
        [alert show];
    }
}

- (void)footerShareNormalBtnAction:(UIButton *)sender {
    if([self isVisitorLogin]==YES) {
        return;
    };
    NSString*per_action_type = [NSString stringWithFormat:@"%@",activityEntity.campaign.per_action_type];
    SSDKPlatformType type = SSDKPlatformSubTypeWechatTimeline;
    if([per_action_type isEqualToString:@"weibo"]) {
        type = SSDKPlatformTypeSinaWeibo;
    } else if ([per_action_type isEqualToString:@"qq"]) {
        type = SSDKPlatformSubTypeQZone;
    } else {
        type = SSDKPlatformSubTypeWechatTimeline;
    }
    [self shareSDKWithTitle:activityEntity.campaign.name Content:activityEntity.campaign.idescription URL:activityEntity.campaign.url ImgURL:activityEntity.campaign.img_url SSDKPlatformType:type];
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==alertView.cancelButtonIndex) {
        [LocalService setRBLocalDataUserAgreeWith:@"YES"];
        // RB-报名活动
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBActivityApplyWithId:activityEntity.campaign.iid andNote:@"" andPicture:nil];
        return;
    }
    return;
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if(actionSheet.tag==12000){
        if (buttonIndex==0) {
            
//            ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
//            if (author == ALAuthorizationStatusDenied) {
//                UILocalNotification *notification = [[UILocalNotification alloc] init];
//                notification.alertBody = @"照片不可用,请设置打开";
//                [[UIApplication sharedApplication]
//                 presentLocalNotificationNow:notification];
//                return;
//            }
//            RBPhotoAsstesGroupViewController*toview = [[RBPhotoAsstesGroupViewController alloc]init];
//            toview.selectNumber = 1;
//            toview.delegateC = self;
//            toview.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:toview animated:YES];
//            RBUploadImageViewController * upView = [[RBUploadImageViewController alloc]init];
//            upView.hidesBottomBarWhenPushed = YES;
//            upView.mark = @"upload";
//            upView.campainID = activityEntity.iid;
//            [self.navigationController pushViewController:upView animated:YES];
            [self uploadImage:@"upload"];
        }
        
        if (buttonIndex==1) {
            if (activityEntity.campaign.cpi_example_screenshots.count!=0) {
//                TOWebViewController *toview = [[TOWebViewController alloc] init];
//                toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.campaign.cpi_example_screenshot]];
//                toview.showPageTitles = NO;
//                toview.title = NSLocalizedString(@"R2052", @"截图参考");
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.navigationButtonsHidden = YES;
//                toview.campainID = activityEntity.iid;
//                [self.navigationController pushViewController:toview animated:YES];
//                RBUploadImageViewController * toview = [[RBUploadImageViewController alloc]init];
//                toview.campainID = activityEntity.iid;
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.mark = @"example";
//                [self.navigationController pushViewController:toview animated:YES];
                [self uploadImage:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
            
        }
    }
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if (alertView.tag==11000) {
        if (buttonIndex==0) {
            if (activityEntity.campaign.cpi_example_screenshots.count != 0) {
//                TOWebViewController *toview = [[TOWebViewController alloc] init];
//                toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.campaign.cpi_example_screenshot]];
//                toview.showPageTitles = NO;
//                toview.title = NSLocalizedString(@"R2052", @"截图参考");
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.navigationButtonsHidden = YES;
//                toview.campainID = activityEntity.iid;
//                [self.navigationController pushViewController:toview animated:YES];
//                RBUploadImageViewController * toview = [[RBUploadImageViewController alloc]init];
//                toview.campainID = activityEntity.iid;
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.mark = @"example";
//                [self.navigationController pushViewController:toview animated:YES];
                [self uploadImage:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
    }
    if (alertView.tag==12000) {
        if (buttonIndex==0) {
            if (activityEntity.campaign.cpi_example_screenshots.count != 0) {
//                TOWebViewController *toview = [[TOWebViewController alloc] init];
//                toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.campaign.cpi_example_screenshot]];
//                toview.showPageTitles = NO;
//                toview.title = NSLocalizedString(@"R2052", @"截图参考");
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.navigationButtonsHidden = YES;
//                toview.campainID = activityEntity.iid;
//                [self.navigationController pushViewController:toview animated:YES];
//                RBUploadImageViewController * toview = [[RBUploadImageViewController alloc]init];
//                toview.campainID = activityEntity.iid;
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.mark = @"example";
//                [self.navigationController pushViewController:toview animated:YES];
                [self uploadImage:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
    }
    if(alertView.tag==13000){
        if (buttonIndex==0) {
            if (activityEntity.campaign.cpi_example_screenshots.count != 0) {
//                TOWebViewController *toview = [[TOWebViewController alloc] init];
//                toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.campaign.cpi_example_screenshot]];
//                toview.showPageTitles = NO;
//                toview.title = NSLocalizedString(@"R2052", @"截图参考");
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.navigationButtonsHidden = YES;
//                toview.campainID = activityEntity.iid;
//                [self.navigationController pushViewController:toview animated:YES];
//                RBUploadImageViewController * toview = [[RBUploadImageViewController alloc]init];
//                toview.campainID = activityEntity.iid;
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.mark = @"example";
//                [self.navigationController pushViewController:toview animated:YES];
                [self uploadImage:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
        if (buttonIndex==1) {
//            TOWebViewController *toview = [[TOWebViewController alloc] init];
//            toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.screenshot]];
//            toview.title = NSLocalizedString(@"R2042", @"查看截图");
//            toview.showPageTitles = NO;
//            toview.hidesBottomBarWhenPushed = YES;
//            toview.navigationButtonsHidden = YES;
//            toview.campainID = activityEntity.iid;
//            [self.navigationController pushViewController:toview animated:YES];
//            RBWatchImageController * toview = [[RBWatchImageController alloc]init];
//            toview.campainID = activityEntity.iid;
//            toview.hidesBottomBarWhenPushed = YES;
//            toview.imgArr = activityEntity.screenshots;
//            toview.nameArr = activityEntity.screenshot_comment;
//            [self.navigationController pushViewController:toview animated:YES];
            [self uploadImage:@"watch"];

        }
    }
}
#pragma mark - RBNewAlert
-(void)RBNewAlert:(RBNewAlert *)alertView clickedButtonAtIndex:(int)buttonIndex{
    if (alertView.tag==11000) {
        if (buttonIndex==0) {
            if (activityEntity.campaign.cpi_example_screenshots.count != 0) {
//                TOWebViewController *toview = [[TOWebViewController alloc] init];
//                toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.campaign.cpi_example_screenshot]];
//                toview.showPageTitles = NO;
//                toview.title = NSLocalizedString(@"R2052", @"截图参考");
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.navigationButtonsHidden = YES;
//                toview.campainID = activityEntity.iid;
//                [self.navigationController pushViewController:toview animated:YES];
//                RBUploadImageViewController * toview = [[RBUploadImageViewController alloc]init];
//                toview.campainID = activityEntity.iid;
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.mark = @"example";
//                [self.navigationController pushViewController:toview animated:YES];
                [self uploadImage:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
    }
    if (alertView.tag==12000) {
        if (buttonIndex==0) {
            if (activityEntity.campaign.cpi_example_screenshots.count != 0) {
//                TOWebViewController *toview = [[TOWebViewController alloc] init];
//                toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.campaign.cpi_example_screenshot]];
//                toview.showPageTitles = NO;
//                toview.title = NSLocalizedString(@"R2052", @"截图参考");
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.navigationButtonsHidden = YES;
//                toview.campainID = activityEntity.iid;
//                [self.navigationController pushViewController:toview animated:YES];
//                RBUploadImageViewController * toview = [[RBUploadImageViewController alloc]init];
//                toview.campainID = activityEntity.iid;
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.mark = @"example";
//                [self.navigationController pushViewController:toview animated:YES];
                [self uploadImage:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
    }
    if(alertView.tag==13000){
        if (buttonIndex==0) {
            if (activityEntity.campaign.cpi_example_screenshots.count != 0) {
//                TOWebViewController *toview = [[TOWebViewController alloc] init];
//                toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.campaign.cpi_example_screenshot]];
//                toview.showPageTitles = NO;
//                toview.title = NSLocalizedString(@"R2052", @"截图参考");
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.navigationButtonsHidden = YES;
//                toview.campainID = activityEntity.iid;
//                [self.navigationController pushViewController:toview animated:YES];
//                RBUploadImageViewController * toview = [[RBUploadImageViewController alloc]init];
//                toview.campainID = activityEntity.iid;
//                toview.hidesBottomBarWhenPushed = YES;
//                toview.mark = @"example";
//                [self.navigationController pushViewController:toview animated:YES];
                [self uploadImage:@"example"];
            } else {
                RBPictureView*pictureView = [[RBPictureView alloc]init];
                [pictureView showWithPic:nil];
            }
        }
        if (buttonIndex==1) {
//            TOWebViewController *toview = [[TOWebViewController alloc] init];
//            toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.screenshot]];
//            toview.title = NSLocalizedString(@"R2042", @"查看截图");
//            toview.showPageTitles = NO;
//            toview.hidesBottomBarWhenPushed = YES;
//            toview.navigationButtonsHidden = YES;
//            toview.campainID = activityEntity.iid;
//            [self.navigationController pushViewController:toview animated:YES];
//            RBWatchImageController * toview = [[RBWatchImageController alloc]init];
//            toview.campainID = activityEntity.iid;
//            toview.hidesBottomBarWhenPushed = YES;
//            toview.imgArr = activityEntity.screenshots;
//            toview.nameArr = activityEntity.screenshot_comment;
//            [self.navigationController pushViewController:toview animated:YES];
            [self uploadImage:@"watch"];

        }
    }

}
#pragma mark - RBPhotoAsstesGroupViewController Delegate
- (void)RBPhotoAsstesGroupViewController:(RBPhotoAsstesGroupViewController *)vc selectedList:(NSMutableArray *)list {
    // 图片压缩处理
    ALAsset *asset = list[0];
    UIImage *originalImage = [Utils thumbnailForAsset:asset maxPixelSize:1024];
    UIImage *tempimg = [Utils
                        getUIImageScalingFromSourceImage:originalImage
                        targetSize:originalImage.size];
    // RB-获取活动上传截图
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityUpaloadWithId:activityEntity.iid andScreenShot:tempimg andCampaignLogo:nil];
}

#pragma mark - ShareSDK Delegate
- (void)shareSDKWithTitle:(NSString *)titleStr
                  Content:(NSString *)contentStr
                      URL:(NSString *)urlStr
                   ImgURL:(NSString *)imgUrlStr
         SSDKPlatformType:(SSDKPlatformType)type {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = contentStr;
    if(type == SSDKPlatformTypeSinaWeibo) {
        contentStr = [NSString stringWithFormat:@"%@%@",contentStr,urlStr];
    }
    [self.hudView show];
    WeakSelf(weakself)
    __weak __typeof(RBActivityEntity) *weakActivityEntity = activityEntity;
    __weak __typeof(UIButton) * weakFooterBtn = footerBtn;
    NSLog(@"title:%@,content:%@,img:%@,url:%@",titleStr,contentStr,imgUrlStr,urlStr);
    [bgIV setImageWithURL:[NSURL URLWithString:activityEntity.campaign.img_url] placeholder:PlaceHolderImage options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        if(error!=nil) {
            [weakself.hudView showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
            return ;
        } else {
            image = [Utils narrowWithImage:image];
        }
        // shareSDK
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        if(type == SSDKPlatformTypeSinaWeibo) {
            [shareParams SSDKSetupShareParamsByText:contentStr
                                             images:image
                                                url:[NSURL URLWithString:urlStr]
                                              title:titleStr
                                               type:SSDKContentTypeAuto];
        } else {
            [shareParams SSDKSetupShareParamsByText:contentStr
                                             images:image
                                                url:[NSURL URLWithString:urlStr]
                                              title:titleStr
                                               type:SSDKContentTypeWebPage];
        }
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            NSLog(@"error:%@",error);
            if (state == SSDKResponseStateSuccess) {
                [weakself.hudView showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
                RBNewAlert * alertView = [[RBNewAlert alloc]init];
                alertView.delegate = weakself;
                alertView.tag = 11000;
                [alertView showWithArray:@[NSLocalizedString(@"R1100", @"分享成功啦-《Robin8截图审核规则》"),NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R2500", @"知道了")] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
            }
            if (state == SSDKResponseStateFail) {
                if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                    [weakself.hudView showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                } else {
                    [weakself.hudView showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
                }
            }
            if (state == SSDKResponseStateCancel) {
                [weakself.hudView dismiss];
                //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
            }
        }];
    }];

}
#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"apply"]) {
        [self.hudView dismiss];
        RBNewAlert * alertView = [[RBNewAlert alloc]init];
        alertView.tag = 10002;
        alertView.delegate = self;
        [alertView showWithArray:@[NSLocalizedString(@"R2093", @"活动报名成功,等待审核中"),NSLocalizedString(@"R1020", @"确定")] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
        [self getActivityData];
    }
    
    if ([sender isEqualToString:@"campaigns_receive"]) {
        [self.hudView dismiss];
        activityEntity = [JsonService getRBActivityEntity:jsonObject];
        [self footerShareApproveBtnAction:nil];
    }
    
    if ([sender isEqualToString:@"upload_screenshot"]) {
        activityEntity = [JsonService getRBActivityEntity:jsonObject];
        [self updateFooterView];
        [self setRefreshViewFinish];
    }
    
    if ([sender isEqualToString:@"campaign_invites_detail"] || [sender isEqualToString:@"campaigns_detail"]) {
        [self.hudView dismiss];
        self.navView.hidden = YES;
        activityEntity = [JsonService getRBActivityEntity:jsonObject];
        //
        [self loadScrollView];
        [self loadWebView];
        [self updateFooterView];
        [self setRefreshViewFinish];
        [self changeStatusBar];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
        if ([[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"detail"]]isEqualToString:NSLocalizedString(@"R2054", @"该活动不存在")]) {
            dispatch_after(
                           dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(), ^{
                               [self navLeftBtnAction:nil];
                           });
        }
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
