//
//  RBCampaignDetailKolsViewController.m
//  RB
//
//  Created by AngusNi on 7/5/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBCampaignDetailKolsViewController.h"

@interface RBCampaignDetailKolsViewController () {
    UIScrollView*scrollviewn;
    RBAdvertiserSwitchView*switchView;
    RBCampaignKolAnalysisView*nlpView;
    RBCampaignKolView*kolView;
}
@end

@implementation RBCampaignDetailKolsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R6065", @"参与人员");
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    switchView = [[RBAdvertiserSwitchView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 40.0)];
    switchView.delegate = self;
    [switchView showWithData:@[NSLocalizedString(@"R6115",@"人员列表"),NSLocalizedString(@"R6116",@"人员分析")] andSelected:0];
    [self.view addSubview:switchView];
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, switchView.bottom, ScreenWidth, ScreenHeight-switchView.bottom)];
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.userInteractionEnabled = YES;
    scrollviewn.bounces = YES;
    scrollviewn.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [scrollviewn setContentSize:CGSizeMake(ScreenWidth*2.0, scrollviewn.height)];
    scrollviewn.delegate = self;
    [self.view addSubview:scrollviewn];
    //
    kolView = [[RBCampaignKolView alloc]initWithFrame:scrollviewn.bounds];
    kolView.delegate = self;
    [scrollviewn addSubview:kolView];
    [kolView RBCampaignKolViewLoadRefreshViewFirstData];
    //
    nlpView = [[RBCampaignKolAnalysisView alloc]initWithFrame:CGRectMake(scrollviewn.width, 0, ScreenWidth, scrollviewn.height)];
    [scrollviewn addSubview:nlpView];
    //
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBCampaignAnalysisWithId:self.activityEntity.campaign.iid];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //
    [TalkingData trackPageBegin:@"campaign-detail-kols-list"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //
    [TalkingData trackPageEnd:@"campaign-detail-kols-list"];
}

- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- UIScrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView==scrollviewn){
        CGPoint offset = scrollView.contentOffset;
        int currentPage = offset.x / (self.view.bounds.size.width);
        [switchView showWithData:@[NSLocalizedString(@"R6115",@"人员列表"),NSLocalizedString(@"R6116",@"人员分析")] andSelected:currentPage];
        [self RBAdvertiserSwitchViewSelected:switchView andIndex:currentPage];
    }
}

#pragma mark- RBAdvertiserSwitchView Delegate
- (void)RBAdvertiserSwitchViewSelected:(RBAdvertiserSwitchView *)view andIndex:(int)index {
    [scrollviewn scrollRectToVisible:CGRectMake(scrollviewn.width*index, 0, scrollviewn.width, scrollviewn.height) animated:YES];
}

#pragma mark- RBCampaignKolView Delegate
- (void)RBCampaignKolViewLoadRefreshViewData:(RBCampaignKolView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBCampaignAnalysisWithId:self.activityEntity.campaign.iid andPage:[NSString stringWithFormat:@"%d",view.pageIndex+1]];
}

- (void)RBCampaignKolViewSelected:(RBCampaignKolView *)view andIndex:(int)index {
    //RBUserEntity*userEntity = self.activityEntity.invitees[index];
    RBUserEntity*userEntity = kolView.datalist[index];
    RBKolDetailViewController *toview = [[RBKolDetailViewController alloc] init];
    toview.kolId = userEntity.iid;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"invitee_analysis"]) {
        [self.hudView dismiss];
        RBNlpEntity*nlpEntity = [JsonService getRBCampaignKolResultNlpEntity:jsonObject];
        [nlpView loadContentView:nlpEntity];
    }
    
    if ([sender isEqualToString:@"invitees"]) {
        if (kolView.pageIndex == 0) {
            kolView.datalist = [NSMutableArray new];
        }
        kolView.datalist = [JsonService getRBCampaignKolList:jsonObject andBackArray:kolView.datalist];
        kolView.defaultView.hidden = YES;
        if ([kolView.datalist count] == 0) {
            kolView.defaultView.hidden = NO;
        }
        [self.hudView dismiss];
        [kolView RBCampaignKolViewSetRefreshViewFinish];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
