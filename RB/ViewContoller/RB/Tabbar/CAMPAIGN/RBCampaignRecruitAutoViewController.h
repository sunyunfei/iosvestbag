//
//  RBCampaignRecruitAutoViewController.h
//  RB
//
//  Created by AngusNi on 16/9/12.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "TOWebViewController.h"
#import "RBPictureView.h"
#import "RBPhotoAsstesGroupViewController.h"
#import "RBCampaignDetailKolsViewController.h"
#import "RBCampaignRecruitApplyViewController.h"

@interface RBCampaignRecruitAutoViewController : RBBaseViewController
<RBBaseVCDelegate,UIWebViewDelegate,UIScrollViewDelegate,RBActionSheetDelegate,RBAlertViewDelegate,WXApiDelegate,RBPhotoAsstesGroupViewControllerDelegate,UIAlertViewDelegate,RBNewAlertViewDelegate>
@property(nonatomic, strong) NSString*activityId;
@property(nonatomic, strong) NSString*campaignId;

@end
