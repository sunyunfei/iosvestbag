//
//  RBWatchImageController.m
//  RB
//
//  Created by RB8 on 2018/1/11.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBWatchImageController.h"

@interface RBWatchImageController ()<UIScrollViewDelegate>
{
    UIScrollView * mainScroll ;
    UILabel * markLabel ;
}
@end

@implementation RBWatchImageController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navTitle = self.nameArr[0];
    self.delegate = self;
    [self initUI];
}
- (void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)initUI{
    mainScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight - NavHeight)];
    mainScroll.scrollEnabled = YES;
    mainScroll.bounces = NO;
    mainScroll.showsVerticalScrollIndicator = NO;
    mainScroll.showsHorizontalScrollIndicator = NO;
    mainScroll.pagingEnabled = YES;
    mainScroll.delegate = self;
    [self.view addSubview:mainScroll];
    for (NSInteger i = 0; i < self.imgArr.count; i ++) {
        UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * ScreenWidth, 0, ScreenWidth, ScreenHeight - NavHeight)];
        [imageView sd_setImageWithURL:[NSURL URLWithString:self.imgArr[i]] placeholderImage:[UIImage imageNamed:@"pic_kol_cell_bg"]];
        [mainScroll addSubview:imageView];
    }
    mainScroll.contentSize = CGSizeMake(ScreenWidth * self.imgArr.count, ScreenHeight - NavHeight);
    //
    markLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, ScreenHeight - 60, ScreenWidth, 40) text:[NSString stringWithFormat:@"1/%ld",(unsigned long)self.imgArr.count] font:font_(16) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:SysDeepGray] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [self.view addSubview:markLabel];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger a = (int)(scrollView.contentOffset.x / ScreenWidth) + 1;
    NSString * str = [NSString stringWithFormat:@"%ld/%ld",(long)a,(long)self.imgArr.count];
    markLabel.text = str;
    self.navTitle = self.nameArr[a-1];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
