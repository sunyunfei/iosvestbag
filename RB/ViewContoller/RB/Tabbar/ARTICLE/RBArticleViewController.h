//
//  RBTabDiscoverViewController.h
//  RB
//
//  Created by AngusNi on 6/1/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
//
#import "RBArticleNewView.h"
//
#import "RBArticleDetailViewController.h"
#import "RBArticleSearchViewController.h"
@interface RBArticleViewController : RBBaseViewController
<RBBaseVCDelegate,RBArticleNewViewDelegate,ASHUDViewDelegate,UIScrollViewDelegate>
@property(nonatomic, strong) RBArticleNewView *articleView;
@property(nonatomic, strong) ASHUDView *hudViewTop;
@property(nonatomic,strong)  UIView * hiddenView;
@end
