//
//  RBTabDiscoverViewController.m
//  RB
//
//  Created by AngusNi on 6/1/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBArticleViewController.h"

@interface RBArticleViewController () {
}
@end

@implementation RBArticleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R4002", @"文章");
    self.navRightTitle = Icon_bar_search;
    self.bgIV.hidden = YES;
    //
    self.hiddenView = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight - NavHeight)];
    UIImageView * hiddeImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth - 106)/2, (ScreenHeight-NavHeight - 95)/2, 106, 95)];
    hiddeImageView.image = [UIImage imageNamed:@"RBNoEnough"];
    [self.hiddenView addSubview:hiddeImageView];
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, hiddeImageView.bottom + 10, ScreenWidth, 20)];
    label.text = @"敬请期待";
    label.font = font_cu_15;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor lightGrayColor];
    [self.hiddenView addSubview:label];
    self.hiddenView.hidden = YES;
    self.hiddenView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.hiddenView];
    //
    self.articleView = [[RBArticleNewView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    self.articleView.delegate = self;
    [self.view addSubview:self.articleView];
    //
    [self.hudView show];
    self.articleView.type = @"discovery";
    [self.articleView RBArticleNewViewLoadRefreshViewFirstData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:@"discover-list"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"discover-list"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationStatusBarAction {
    CGPoint off = self.articleView.tableviewn.contentOffset;
    off.y = 0 - self.articleView.tableviewn.contentInset.top;
    [self.articleView.tableviewn setContentOffset:off animated:YES];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)RBNavRightBtnAction {
    if([self isVisitorLogin]==YES){
        return;
    };
    RBArticleSearchViewController*toview = [[RBArticleSearchViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - RBArticleNewView Delegate
-(void)RBArticleNewViewLoadRefreshViewData:(RBArticleNewView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBArticleWithTitle:@"" andType:self.articleView.type];
}

- (void)RBArticleNewViewSelected:(RBArticleNewView *)view andIndex:(int)index {
    RBArticleEntity*entity = view.datalist[index];
    entity.article_read = @"1";
    [ArticleEntityManager updateCoreDataArticleEntity_By:entity.article_id andRBArticleEntity:entity];
    [self.articleView.datalist replaceObjectAtIndex:index withObject:entity];
    //
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.articleView.tableviewn reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    //
    RBArticleDetailViewController*toview = [[RBArticleDetailViewController alloc] init];
    toview.entity = entity;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - ASHUDView Delegate
- (void)ASHUDViewHide:(ASHUDView *)hudView {
    [self.articleView.tableviewn setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.hudViewTop dismiss];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"articles"]) {
        [self.hudView dismiss];
        if(self.articleView.datalist == nil) {
            self.articleView.datalist = [NSMutableArray new];
            self.articleView.datalist = [NSMutableArray arrayWithArray:[ArticleEntityManager selectCoreDataArticleEntity_All]];
        }
        if (self.articleView.pageIndex == 0) {
            self.articleView.datalist = [JsonService getRBArticleListEntity:jsonObject insertBackArray:self.articleView.datalist];
        } else {
            self.articleView.datalist = [JsonService getRBArticleListEntity:jsonObject andBackArray:self.articleView.datalist];
        }
        [self.articleView RBArticleNewViewSetRefreshViewFinish];
        if (self.articleView.pageIndex == 0) {
            [self.articleView.tableviewn setContentOffset:CGPointMake(0, -35.0) animated:NO];
            NSString*showString = @"";
            NSString*articles_count = [JsonService checkJson:[jsonObject valueForKey:@"articles_count"]];
            if (articles_count.intValue==0) {
                showString = NSLocalizedString(@"R2008",@"当前是最新数据,无需更新");
            } else {
                showString = [NSString stringWithFormat:NSLocalizedString(@"R2009",@"Robin8引擎为您找到%@条更新"),articles_count];
            }
            if(self.hudViewTop == nil) {
                self.hudViewTop = [[ASHUDView alloc] initWithFrame:CGRectZero];
                self.hudViewTop.superView = self.articleView.tableviewn;
                self.hudViewTop.delegate = self;
            }
            [self.hudViewTop showSuccessWithStatus:showString];
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"article"]) {
        if (self.articleView.datalist==nil || [self.articleView.datalist count]==0) {
            self.articleView.datalist = [NSMutableArray arrayWithArray:[ArticleEntityManager selectCoreDataArticleEntity_All]];
        }
    }
    [self.articleView RBArticleNewViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    if (self.articleView.datalist==nil || [self.articleView.datalist count]==0) {
      //  self.articleView.datalist = [NSMutableArray arrayWithArray:[ArticleEntityManager selectCoreDataArticleEntity_All]];
        self.articleView.hidden = YES;
        self.hiddenView.hidden = NO;
    }
    [self.articleView RBArticleNewViewSetRefreshViewFinish];
   // [self.hudView showErrorWithStatus:error.localizedDescription];
    [self.hudView dismiss];
    
}

@end
