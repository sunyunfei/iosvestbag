//
//  RBArticleNewTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBArticleNewTableViewCell.h"
#import "Service.h"

@implementation RBArticleNewTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
        self.bgView.userInteractionEnabled = YES;
        self.bgView.backgroundColor = [UIColor whiteColor];
        self.bgView.tag = 1000;
        [self.contentView addSubview:self.bgView];
        //
        self.bgIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.bgIV.contentMode = UIViewContentModeScaleAspectFill;
        self.bgIV.layer.masksToBounds = YES;
        self.bgIV.tag = 1001;
        [self.contentView addSubview:self.bgIV];
        //
        self.bgIVLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.bgIVLabel.tag = 1002;
        [self.contentView addSubview:self.bgIVLabel];
        //
        self.tLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.tLabel.font = font_cu_15;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.tLabel.tag = 1003;
        [self.contentView addSubview:self.tLabel];
        //
        self.cLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.cLabel.font = font_11;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.cLabel.tag = 1004;
        [self.contentView addSubview:self.cLabel];
        //
        self.refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.refreshBtn.titleLabel.font = font_13;
        [self.refreshBtn setTitle:NSLocalizedString(@"R2012",@"上次看到这里 点击刷新") forState:UIControlStateNormal];
        [self.refreshBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        [self.refreshBtn setBackgroundColor:[Utils getUIColorWithHexString:SysColorBkg]];
        self.refreshBtn.tag = 1005;
        [self.contentView addSubview:self.refreshBtn];
        //
        self.lineLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        self.lineLabel.tag = 1006;
        [self.contentView addSubview:self.lineLabel];
    }
    return self;
}

- (instancetype)loadRBArticleNewCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    RBArticleEntity*entity = datalist[indexPath.row];
    self.tLabel.text = [entity.article_title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    self.cLabel.text = [NSString stringWithFormat:@"%@",entity.article_author];
    NSString*imgUrl = [NSString stringWithFormat:@"%@",entity.article_avatar_url];
    if (indexPath.row%4 == 0) {
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.bgIV.frame = CGRectMake(0, 0, ScreenWidth, (ScreenWidth*358.0/665.0));
        self.tLabel.frame = CGRectMake(CellLeft, self.bgIV.bottom+CellLeft, ScreenWidth-CellLeft*2.0, 1000);
        self.tLabel.numberOfLines = 3;
        [Utils getUILabel:self.tLabel withLineSpacing:2.0];
        CGSize tLabelSize = [Utils getUIFontSizeFitH:self.tLabel withLineSpacing:2.0];
        self.tLabel.height = tLabelSize.height;
        if (tLabelSize.height > 61) {
            self.tLabel.height = 62;
        }
        self.cLabel.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom+CellLeft, self.tLabel.width, 13.0);
        self.bgIVLabel.frame = self.bgIV.frame;
        self.lineLabel.frame = CGRectMake(0, self.cLabel.bottom+CellLeft, ScreenWidth, 0);
    } else {
        self.bgIV.frame = CGRectMake(ScreenWidth/2.0, 0, ScreenWidth/2.0, ((ScreenWidth/2.0)*242.0/332.0));
        self.tLabel.frame = CGRectMake(CellLeft, CellLeft, ScreenWidth/2.0-CellLeft*2.0, 1000);
        self.tLabel.numberOfLines = 3;
        [Utils getUILabel:self.tLabel withLineSpacing:2.0];
        CGSize tLabelSize = [Utils getUIFontSizeFitH:self.tLabel withLineSpacing:2.0];
        self.tLabel.height = tLabelSize.height;
        if (tLabelSize.height > 61) {
            self.tLabel.height = 62;
        }
        self.cLabel.frame = CGRectMake(self.tLabel.left, self.bgIV.bottom-13.0-8.0, self.tLabel.width, 13.0);
        self.bgIVLabel.frame = self.bgIV.frame;
        self.lineLabel.frame = CGRectMake(0, self.bgIV.bottom, ScreenWidth, 0);
        //
        imgUrl = [NSString stringWithFormat:@"%@640",entity.article_avatar_url];
    }
//    [self.bgIV sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:nil options:SDWebImageProgressiveDownload];
//    [self.bgIV yy_setImageWithURL:[NSURL URLWithString:imgUrl] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
    [self.bgIV setImageWithURL:[NSURL URLWithString:imgUrl] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
    if ([NSString stringWithFormat:@"%@",entity.article_read].intValue==1) {
        self.bgIVLabel.backgroundColor = [UIColor clearColor];
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.cLabel.textColor = self.tLabel.textColor;
    } else {
        self.bgIVLabel.backgroundColor = [UIColor clearColor];
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    }
    if ([NSString stringWithFormat:@"%@",entity.article_refresh].intValue==1) {
        self.lineLabel.hidden = YES;
        self.refreshBtn.hidden = NO;
        self.refreshBtn.frame = CGRectMake(0, self.lineLabel.bottom-0.5, ScreenWidth, 35.5);
        self.bgView.frame = CGRectMake(0, 0, ScreenWidth, self.lineLabel.bottom+35.0);
        self.frame = CGRectMake(0, 0, ScreenWidth, self.bgView.bottom);
    } else {
        self.lineLabel.hidden = NO;
        self.refreshBtn.hidden = YES;
        self.bgView.frame = CGRectMake(0, 0, ScreenWidth, self.lineLabel.bottom);
        self.frame = CGRectMake(0, 0, ScreenWidth, self.bgView.bottom+CellBottom);
    }
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
