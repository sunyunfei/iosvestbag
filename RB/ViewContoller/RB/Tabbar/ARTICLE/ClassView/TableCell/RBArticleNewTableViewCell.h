//
//  RBArticleNewTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface RBArticleNewTableViewCell : UITableViewCell

//
@property(nonatomic, strong) UIView *bgView;
//
@property(nonatomic, strong) UIImageView *bgIV;
//
@property(nonatomic, strong) UILabel *bgIVLabel;
//
@property(nonatomic, strong) UILabel *tLabel;
//
@property(nonatomic, strong) UILabel *cLabel;
//
@property(nonatomic, strong) UIButton *refreshBtn;
//
@property(nonatomic, strong) UILabel*lineLabel;

- (instancetype)loadRBArticleNewCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;

@end
