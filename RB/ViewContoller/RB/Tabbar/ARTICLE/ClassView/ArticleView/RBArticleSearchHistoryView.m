//
//  RBArticleSearchHistoryView.m
//  RB
//
//  Created by AngusNi on 3/29/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBArticleSearchHistoryView.h"

@interface RBArticleSearchHistoryView () {
}
@end

@implementation RBArticleSearchHistoryView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor whiteColor];
        //
        self.tableviewn = [[UITableView alloc]
                           initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)
                           style:UITableViewStylePlain];
        self.tableviewn.backgroundView = nil;
        self.tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableviewn.backgroundColor = [UIColor clearColor];
        self.tableviewn.delegate = self;
        self.tableviewn.dataSource = self;
        [self addSubview:self.tableviewn];
        //
        self.clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.clearBtn.frame = CGRectMake((frame.size.width-120.0)/2.0, 40.0*7.0, 120.0, 40.0);
        self.clearBtn.titleLabel.font = font_13;
        [self.clearBtn setTitle:NSLocalizedString(@"R2011",@"清除历史记录") forState:UIControlStateNormal];
        [self.clearBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        self.clearBtn.layer.borderWidth = 0.5;
        self.clearBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
        self.clearBtn.layer.masksToBounds = YES;
        [self.clearBtn addTarget:self action:@selector(clearBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.clearBtn];
    }
    return self;
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if ([self.datalist count]==0) {
        self.clearBtn.hidden = YES;
    } else {
        self.clearBtn.hidden = NO;
        self.clearBtn.top = 40.0*[self.datalist count]+20.0;
    }
    return [self.datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d",(int)[indexPath section],(int)[indexPath row]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
        cell.backgroundView = nil;
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40.0)];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        bgView.backgroundColor = [UIColor whiteColor];
        [cell.contentView addSubview:bgView];
        //
        UILabel *tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth-CellLeft*2.0, 37.0)];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1001;
        [cell.contentView addSubview:tLabel];
        //
        RBButton*deleteBtn = [RBButton buttonWithType:UIButtonTypeCustom];
        deleteBtn.frame = CGRectMake(ScreenWidth-CellLeft-40.0, 0, 40.0, 40.0);
        [deleteBtn addTarget:self action:@selector(deleteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        deleteBtn.tag = 1002;
        [cell.contentView addSubview:deleteBtn];
        //
        UILabel*deleteLabel = [[UILabel alloc]initWithFrame:CGRectMake((40.0-13.0)/2.0, (40.0-13.0)/2.0, 13.0, 13.0)];
        deleteLabel.font = font_icon_(deleteLabel.width);
        deleteLabel.text = Icon_btn_delete;
        deleteLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [deleteBtn addSubview:deleteLabel];
    }
    UILabel*tLabel=(UILabel*)[cell.contentView viewWithTag:1001];
    RBButton*deleteBtn=(RBButton*)[cell.contentView viewWithTag:1002];
    deleteBtn.typeStr = [NSString stringWithFormat:@"%d",(int)indexPath.row];
    tLabel.text = self.datalist[indexPath.row];
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    cell.backgroundView = cellBgView;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate
         respondsToSelector:@selector(RBArticleSearchHistoryViewSelected:andIndex:)]) {
        [self.delegate RBArticleSearchHistoryViewSelected:self andIndex:(int)indexPath.row];
    }
}

#pragma mark - RBButton Delegate
- (void)clearBtnAction:(UIButton *)sender {
    [self.datalist removeAllObjects];
    if([self.type isEqualToString:@"KOL"]){
        [LocalService setRBLocalDataUserKolSearchListWith:nil];
    }
    if([self.type isEqualToString:@"Ranking"]){
        [LocalService setRBLocalDataUserRankingSearchListWith:nil];
    }
    if([self.type isEqualToString:@"Article"]){
        [LocalService setRBLocalDataUserSearchListWith:nil];
    }
    [UIView performWithoutAnimation:^{
        [self.tableviewn reloadData];
    }];
}

- (void)deleteBtnAction:(RBButton *)sender {
    [self.datalist removeObjectAtIndex:sender.typeStr.intValue];
    
    NSString*temp=nil;
    for (int i =0; i <[self.datalist count]; i++) {
        if([NSString stringWithFormat:@"%@",self.datalist[i]].length!=0){
            if (i==0) {
                temp = self.datalist[i];
            } else {
                temp = [NSString stringWithFormat:@"%@*^*%@",temp,self.datalist[i]];
            }
        }
    }
    if([self.type isEqualToString:@"KOL"]){
        [LocalService setRBLocalDataUserKolSearchListWith:temp];
    }
    if([self.type isEqualToString:@"Ranking"]){
        [LocalService setRBLocalDataUserRankingSearchListWith:temp];
    }
    if([self.type isEqualToString:@"Article"]){
        [LocalService setRBLocalDataUserSearchListWith:temp];
    }
    [UIView performWithoutAnimation:^{
        [self.tableviewn reloadData];
    }];
}


@end
