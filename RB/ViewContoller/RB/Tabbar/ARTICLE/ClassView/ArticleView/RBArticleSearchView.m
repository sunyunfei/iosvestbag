//
//  RBArticleSearchView.m
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBArticleSearchView.h"
#import "Service.h"

@implementation RBArticleSearchView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = CGRectMake(0, 0, ScreenWidth, NavHeight);
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor whiteColor];
        //
        NSMutableParagraphStyle *paragraphStyle  =
        [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
        CGSize labelSize  =
        [NSLocalizedString(@"R1011", @"取消") boundingRectWithSize:CGSizeMake(1000, 20)
                                                         options:NSStringDrawingUsesLineFragmentOrigin |
         NSStringDrawingUsesFontLeading
                                                      attributes:@{
                                                                   NSFontAttributeName : font_cu_15,
                                                                   NSParagraphStyleAttributeName : paragraphStyle
                                                                   } context:nil]
        .size;
        //
        self.bgView = [[UIView alloc]initWithFrame:CGRectMake(CellLeft, StatusHeight+(44.0-30.0)/2.0, ScreenWidth-CellLeft*2.0-labelSize.width-8.0, 30.0)];
        self.bgView.layer.borderWidth = 0.5;
        self.bgView.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlack]CGColor];
        self.bgView.layer.masksToBounds = YES;
        self.bgView.userInteractionEnabled = YES;
        [self addSubview:self.bgView];
        //
        UILabel*iconLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, (self.bgView.height-15.0)/2.0, 15, 15)];
        iconLabel.font = font_icon_(iconLabel.width);
        iconLabel.text = Icon_bar_search;
        iconLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self.bgView addSubview:iconLabel];
        //
        self.searchTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(iconLabel.right+5.0, 0, self.bgView.width-(iconLabel.right+5.0), self.bgView.height)];
        self.searchTF.font = font_cu_15;
        self.searchTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.searchTF.placeholder = NSLocalizedString(@"R2010",@"搜索");
        self.searchTF.keyboardType = UIKeyboardTypeDefault;
        self.searchTF.returnKeyType = UIReturnKeySearch;
        self.searchTF.delegate = self;
        self.searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self.bgView addSubview:self.searchTF];
        //
        self.cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.cancelBtn.frame = CGRectMake(self.bgView.right+8.0, self.bgView.top, labelSize.width, self.bgView.height);
        self.cancelBtn.titleLabel.font = font_cu_15;
        [self.cancelBtn setTitle:NSLocalizedString(@"R1011", @"取消") forState:UIControlStateNormal];
        [self.cancelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
        [self.cancelBtn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.cancelBtn];
        //
        UILabel *navLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NavHeight-0.5, ScreenWidth, 0.5)];
        navLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBarLine];
        [self addSubview:navLineLabel];
    }
    return self;
}

- (BOOL)becomeFirstResponder{
    return YES;
}

#pragma mark - UIButton Delegate
-(void)cancelBtnAction:(UIButton*)sender {
    [self endEditing:YES];
    if ([self.delegate
         respondsToSelector:@selector(RBArticleSearchViewSearchCancel:)]) {
        [self.delegate RBArticleSearchViewSearchCancel:self];
    }
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self.delegate
         respondsToSelector:@selector(RBArticleSearchViewSearch:andText:)]) {
        [self.delegate RBArticleSearchViewSearch:self andText:self.searchTF.text];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    if ([self.delegate
         respondsToSelector:@selector(RBArticleSearchViewSearchClear:)]) {
        [self.delegate RBArticleSearchViewSearchClear:self];
    }
    return YES;
}

@end
