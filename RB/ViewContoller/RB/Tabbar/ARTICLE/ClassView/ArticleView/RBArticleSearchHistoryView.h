//
//  RBArticleSearchHistoryView.h
//  RB
//
//  Created by AngusNi on 3/29/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBArticleSearchView.h"
#import "RBArticleTableViewCell.h"

@class RBArticleSearchHistoryView;
@protocol RBArticleSearchHistoryViewDelegate <NSObject>
@optional
- (void)RBArticleSearchHistoryViewLoadRefreshViewData:(RBArticleSearchHistoryView *)view;
- (void)RBArticleSearchHistoryViewSelected:(RBArticleSearchHistoryView *)view andIndex:(int)index;

@end


@interface RBArticleSearchHistoryView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBArticleSearchHistoryViewDelegate> delegate;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) UIButton *clearBtn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) NSString *type;

@end