//
//  RBArticleView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBArticleSearchView.h"
#import "RBArticleTableViewCell.h"
@class RBArticleView;
@protocol RBArticleViewDelegate <NSObject>
@optional
- (void)RBArticleViewLoadRefreshViewData:(RBArticleView *)view;
- (void)RBArticleViewSelected:(RBArticleView *)view andIndex:(int)index;

@end

@interface RBArticleView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBArticleViewDelegate> delegate;
@property(nonatomic, strong) UIView*defaultView;
@property(nonatomic, strong) RBArticleSearchView *searchView;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
- (void)RBArticleViewLoadRefreshViewFirstData;
- (void)RBArticleViewSetRefreshViewFinish;

@end