//
//  RBArticleSearchView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"

@class RBArticleSearchView;
@protocol RBArticleSearchViewDelegate <NSObject>
@optional
- (void)RBArticleSearchViewSearchClear:(RBArticleSearchView *)view;
- (void)RBArticleSearchViewSearchCancel:(RBArticleSearchView *)view;
- (void)RBArticleSearchViewSearch:(RBArticleSearchView *)view andText:(NSString*)text;
@end


@interface RBArticleSearchView : UIView
<UITextFieldDelegate>
@property(nonatomic, weak) id<RBArticleSearchViewDelegate> delegate;
@property(nonatomic, strong) UIView*bgView;
@property(nonatomic, strong) UIButton*cancelBtn;
@property(nonatomic, strong) InsetsTextField*searchTF;
@end
