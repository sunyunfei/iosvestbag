//
//  RBArticleNewView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBArticleSearchView.h"
#import "RBArticleNewTableViewCell.h"

@class RBArticleNewView;
@protocol RBArticleNewViewDelegate <NSObject>
@optional
- (void)RBArticleNewViewLoadRefreshViewData:(RBArticleNewView *)view;
- (void)RBArticleNewViewSelected:(RBArticleNewView *)view andIndex:(int)index;

@end


@interface RBArticleNewView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBArticleNewViewDelegate> delegate;
@property(nonatomic, strong) RBArticleSearchView *searchView;

@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
- (void)RBArticleNewViewLoadRefreshViewFirstData;
- (void)RBArticleNewViewSetRefreshViewFinish;

@end