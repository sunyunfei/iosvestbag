//
//  RBArticleDetailViewController.m
//  RB
//
//  Created by AngusNi on 3/17/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBArticleDetailViewController.h"
#import "RBLoginViewController.h"

@interface RBArticleDetailViewController () {
    UIWebView *webviewn;
    UIButton *zanBtn;
    UIButton *likeBtn;
    UIButton *shareBtn;
    RBArticleActionEntity*articleActionEntity;
    UIButton*footerBtn;
}
@end

@implementation RBArticleDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R8002",@"详情");
//    self.navRightLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
//    self.navRightTitle = Icon_btn_care_l;
    //
    [self loadFooterView];
    //
    webviewn = [[UIWebView alloc] initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    webviewn.delegate = self;
    webviewn.scalesPageToFit = YES;
    [self.view addSubview:webviewn];
    [webviewn loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.entity.article_url]]];
    // RB-获取文章阅读
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBArticleReadLikeWithAction:@"look" andEntity:self.entity];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"discover-detail"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"discover-detail"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)RBNavRightBtnAction {
    if([self isVisitorLogin]==YES) {
        return;
    };
    //
    Handler*handler = [Handler shareHandler];
    [handler getRBArticleWithAction:@"collect" andIId:articleActionEntity.iid];
    //
    if([self.navRightTitle isEqualToString:Icon_btn_care_l]) {
        self.navRightLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
        self.navRightTitle = Icon_btn_care_h;
    } else {
        self.navRightLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.navRightTitle = Icon_btn_care_l;
    }
}

- (void)footerBtnAction:(UIButton*)sender {
    NSString *urlStr = [NSString stringWithFormat:@"%@",self.entity.article_url];
    OGNode *data = [ObjectiveGumbo parseDocumentWithUrl:[NSURL URLWithString:urlStr] encoding:NSUTF8StringEncoding];
    NSArray * listRows = [data elementsWithTag:GUMBO_TAG_BODY];
    for (OGElement * listRow in listRows) {
        NSArray *itemRows = [listRow elementsWithTag:GUMBO_TAG_IMG];
        for (OGElement * itemRow in itemRows) {
            NSLog(@"itemRow:%@",itemRow.attributes[@"data-src"]);
            NSString*src = [NSString stringWithFormat:@"%@",itemRow.attributes[@"data-src"]];
            if(src.length!=0) {
                [[SDWebImageManager sharedManager]downloadImageWithURL:[NSURL URLWithString:src] options:SDWebImageHighPriority progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                    [self.hudView show];
                } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                    [self.hudView dismiss];
                    [footerBtn setTitle:NSLocalizedString(@"R8004",@"图片已保存至手机相册") forState:UIControlStateNormal];
                }];
            }
        }
    }
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth , 45.0)];
    footerView.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    [self.view addSubview:footerView];
    footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.frame = footerView.bounds;
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerBtn setTitle:NSLocalizedString(@"R8003",@"下载文章中所有图片") forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark - UIWebView Delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    // 防止应用宝自动跳转
    if ([[request.URL.absoluteString componentsSeparatedByString:@"https://itunes.apple.com"]count]>1) {
        return NO;
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.hudView dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.hudView dismiss];
}

//#pragma mark- loadFooterView Delegate
//- (void)loadFooterView {
//    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-48.0, ScreenWidth, 48.0)];
//    [self.view addSubview:footerView];
//    //
//    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 0.5)];
//    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
//    [footerView addSubview:lineLabel];
//    //
//    zanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    zanBtn.frame = CGRectMake(35.0, (footerView.height-22.0)/2.0, 22.0, 22.0);
//    [zanBtn addTarget:self
//                  action:@selector(zanBtnAction:)
//        forControlEvents:UIControlEventTouchUpInside];
//    zanBtn.titleLabel.font = font_icon_(22.0);
//    [zanBtn setTitle:Icon_btn_like forState:UIControlStateNormal];
//    [footerView addSubview:zanBtn];
//    //
//    likeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    likeBtn.frame = CGRectMake(ScreenWidth/3.0+(ScreenWidth/3.0-zanBtn.width)/2.0, (footerView.height-zanBtn.width)/2.0, zanBtn.width, zanBtn.width);
//    [likeBtn addTarget:self
//               action:@selector(likeBtnAction:)
//     forControlEvents:UIControlEventTouchUpInside];
//    likeBtn.titleLabel.font = font_icon_(22.0);
//    [likeBtn setTitle:Icon_btn_fav forState:UIControlStateNormal];
//    [footerView addSubview:likeBtn];
//    //
//    shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    shareBtn.frame = CGRectMake(ScreenWidth-zanBtn.width-35.0, (footerView.height-zanBtn.width)/2.0, zanBtn.width, zanBtn.width);
//    [shareBtn addTarget:self
//                action:@selector(shareBtnAction:)
//      forControlEvents:UIControlEventTouchUpInside];
//    shareBtn.titleLabel.font = font_icon_(22.0);
//    [shareBtn setTitle:Icon_btn_share forState:UIControlStateNormal];
//    [footerView addSubview:shareBtn];
//}
//
//
//#pragma mark - UIButton Delegate
//-(void)shakeStart:(UIButton*)sender {
//    CGAffineTransform fromTransform = sender.transform;
//    CGAffineTransform toTransform = CGAffineTransformScale(sender.transform, 1.5, 1.5);
//    sender.transform = toTransform;
//    [UIView animateWithDuration: 0.5 animations: ^{
//        sender.transform = fromTransform;
//    } completion:^(BOOL finished) {
//    }];
//}
//
//- (void)zanBtnAction:(UIButton *)sender {
//    if([self isVisitorLogin]==YES){
//        return;
//    };
//    //
//    [self shakeStart:sender];
//    if([NSString stringWithFormat:@"%@",articleActionEntity.like].intValue==0) {
//        [zanBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
//    } else {
//        [zanBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
//    }
//    Handler*handler = [Handler shareHandler];
//    [handler getRBArticleWithAction:@"like" andIId:articleActionEntity.iid];
//}
//
//- (void)likeBtnAction:(UIButton *)sender {
//    if([self isVisitorLogin]==YES){
//        return;
//    };
//    //
//    [self shakeStart:sender];
//    if([NSString stringWithFormat:@"%@",articleActionEntity.collect].intValue==0) {
//        [likeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
//    } else {
//        [likeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
//    }
//    Handler*handler = [Handler shareHandler];
//    [handler getRBArticleWithAction:@"collect" andIId:articleActionEntity.iid];
//}
//
//- (void)shareBtnAction:(UIButton *)sender {
//    [self shakeStart:sender];
//    [shareBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
//    Handler*handler = [Handler shareHandler];
//    [handler getRBArticleWithAction:@"forward" andIId:articleActionEntity.iid];
//    //
//    RBShareView *shareView = [RBShareView sharedRBShareView];
//    [shareView showViewWithTitle:articleActionEntity.article_title Content:articleActionEntity.article_title URL:articleActionEntity.share_url ImgURL:articleActionEntity.article_avatar_url];
//}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"articles/action"] || [sender isEqualToString:@"article_actions"]) {
        articleActionEntity = [JsonService getRBArticleActionEntity:jsonObject];
//        if([NSString stringWithFormat:@"%@",articleActionEntity.collect].intValue==0) {
//            self.navRightLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
//            self.navRightTitle = Icon_btn_care_l;
//        } else {
//            self.navRightLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
//            self.navRightTitle = Icon_btn_care_h;
//        }
//
//        if([NSString stringWithFormat:@"%@",articleActionEntity.collect].intValue==0) {
//            [likeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                [[NSNotificationQueue defaultQueue] enqueueNotification:[NSNotification notificationWithName:NotificationRefreshArticleView object:nil] postingStyle:NSPostNow];
//            });
//        } else {
//            [likeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
//        }
//        if([NSString stringWithFormat:@"%@",articleActionEntity.like].intValue==0) {
//            [zanBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
//        } else {
//            [zanBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
//        }
//        if([NSString stringWithFormat:@"%@",articleActionEntity.forward].intValue==0) {
//            [shareBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
//        } else {
//            [shareBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
//        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
//        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
//    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
