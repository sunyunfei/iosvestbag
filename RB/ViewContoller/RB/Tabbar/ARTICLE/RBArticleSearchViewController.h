//
//  RBArticleSearchViewController.h
//  RB
//
//  Created by AngusNi on 3/28/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBArticleSearchView.h"
#import "RBArticleSearchHistoryView.h"
#import "RBArticleView.h"
#import "RBArticleDetailViewController.h"

@interface RBArticleSearchViewController : RBBaseViewController
<RBArticleSearchViewDelegate,RBArticleSearchHistoryViewDelegate,RBArticleViewDelegate>
@property(nonatomic, strong) NSString*str;

@end
