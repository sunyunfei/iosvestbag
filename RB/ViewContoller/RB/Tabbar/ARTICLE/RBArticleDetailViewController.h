//
//  RBArticleDetailViewController.h
//  RB
//
//  Created by AngusNi on 3/17/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBArticleDetailViewController : RBBaseViewController
<RBBaseVCDelegate,UIWebViewDelegate>
@property(nonatomic, strong) RBArticleEntity*entity;
@end
