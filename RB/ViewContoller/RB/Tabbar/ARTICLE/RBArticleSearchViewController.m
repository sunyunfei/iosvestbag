//
//  RBArticleSearchViewController.m
//  RB
//
//  Created by AngusNi on 3/28/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBArticleSearchViewController.h"

@interface RBArticleSearchViewController (){
    RBArticleView*articleView;
    RBArticleSearchView*searchView;
    RBArticleSearchHistoryView*historyView;
    NSString*searchStr;
}
@end

@implementation RBArticleSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    [self loadNavView];
    [self showSearchHistory];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"discover-search"];
    //
    if(self.str!=nil){
        searchStr = self.str;
        searchView.searchTF.text = searchStr;
        [articleView RBArticleViewLoadRefreshViewFirstData];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"discover-search"];
}

#pragma mark - UIButton Delegate
- (void)loadNavView {
    searchView = [[RBArticleSearchView alloc]initWithFrame:CGRectZero];
    searchView.delegate = self;
    [self.view addSubview:searchView];
    //
    historyView = [[RBArticleSearchHistoryView alloc]initWithFrame:CGRectMake(searchView.left, searchView.bottom, searchView.width, ScreenHeight - (searchView.bottom))];
    historyView.delegate = self;
    historyView.type = @"Article";
    [self.view addSubview:historyView];
    //
    articleView = [[RBArticleView alloc]initWithFrame:CGRectMake(0, searchView.bottom, ScreenWidth, ScreenHeight - searchView.bottom)];
    articleView.delegate = self;
    [self.view addSubview:articleView];
    articleView.tableviewn.mj_header=nil;
    //
    UIScreenEdgePanGestureRecognizer *edgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(edgePanGesture:)];
    edgePanGestureRecognizer.delegate = self;
    edgePanGestureRecognizer.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:edgePanGestureRecognizer];
}

-(void)showSearchHistory {
    historyView.hidden = NO;
    articleView.hidden = YES;
    historyView.datalist = [NSMutableArray arrayWithArray:[[LocalService getRBLocalDataUserSearchList] componentsSeparatedByString:@"*^*"]];
    [historyView.tableviewn reloadData];
    [searchView.searchTF becomeFirstResponder];
}

- (BOOL)becomeFirstResponder{
    return YES;
}

#pragma mark- UIScreenEdgePanGestureRecognizer Delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (void)edgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self.view removeGestureRecognizer:sender];
}

#pragma mark - RBArticleSearchView Delegate
-(void)RBArticleSearchHistoryViewSelected:(RBArticleSearchHistoryView *)view andIndex:(int)index {
    searchStr = view.datalist[index];
    searchView.searchTF.text = searchStr;
    [articleView RBArticleViewLoadRefreshViewFirstData];
}

#pragma mark - RBArticleSearchView Delegate
-(void)RBArticleSearchViewSearchCancel:(RBArticleSearchView *)view {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)RBArticleSearchViewSearchClear:(RBArticleSearchView *)view {
    [self showSearchHistory];
}

-(void)RBArticleSearchViewSearch:(RBArticleSearchView *)view andText:(NSString *)text {
    [self.view endEditing:YES];
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([text isEqualToString:searchStr]) {
        return;
    }
    if (text.length==0) {
        return;
    }
    searchStr = text;
    NSMutableArray*tempList = [NSMutableArray arrayWithArray:[[LocalService getRBLocalDataUserSearchList] componentsSeparatedByString:@"*^*"]];
    if (![tempList containsObject:searchStr]) {
        if ([tempList count]>=5) {
            [tempList removeLastObject];
        }
        [tempList insertObject:searchStr atIndex:0];
    }
    NSString*temp=@"";
    for (int i =0; i <[tempList count]; i++) {
        if([NSString stringWithFormat:@"%@",tempList[i]].length!=0){
            if (i==0) {
                temp = tempList[i];
            } else {
                temp = [NSString stringWithFormat:@"%@*^*%@",temp,tempList[i]];
            }
        }
    }
    if([NSString stringWithFormat:@"%@",temp].length!=0) {
        [LocalService setRBLocalDataUserSearchListWith:temp];
        [articleView RBArticleViewLoadRefreshViewFirstData];
    }
}

#pragma mark - RBArticleView Delegate
-(void)RBArticleViewLoadRefreshViewData:(RBArticleView *)view {
    // RB-获取文章搜索列表
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBArticleSearchWith:searchStr andPage:[NSString stringWithFormat:@"%d",articleView.pageIndex+1]];
}

- (void)RBArticleViewSelected:(RBArticleView *)view andIndex:(int)index {
    RBArticleEntity*entity = view.datalist[index];
    entity.article_read = @"1";
    [ArticleEntityManager updateCoreDataArticleEntity_By:entity.article_id andRBArticleEntity:entity];
    [articleView.datalist replaceObjectAtIndex:index withObject:entity];
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:index inSection:0];
    [articleView.tableviewn reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    //
    RBArticleDetailViewController*toview = [[RBArticleDetailViewController alloc] init];
    toview.entity = entity;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}


#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"search"]) {
        [self.hudView dismiss];
        if (articleView.pageIndex == 0) {
            articleView.datalist = [NSMutableArray new];
        }
        articleView.datalist = [JsonService getRBArticleListEntity:jsonObject andBackArrayNormal:articleView.datalist];
        if (articleView.pageIndex == 0) {
            NSString*articles_count = [JsonService checkJson:[jsonObject valueForKey:@"articles_count"]];
            if (articles_count.intValue==0) {
                [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2024", @"无相关搜索结果")];
                [self showSearchHistory];
                [searchView.searchTF becomeFirstResponder];
            } else {
                [searchView.searchTF resignFirstResponder];
                historyView.hidden = YES;
                articleView.hidden = NO;
            }
            [articleView RBArticleViewSetRefreshViewFinish];
            [articleView.tableviewn scrollRectToVisible:CGRectMake( 0, 0, articleView.tableviewn.width, articleView.tableviewn.height) animated:YES];
        } else {
            NSString*articles_count = [JsonService checkJson:[jsonObject valueForKey:@"articles_count"]];
            if (articles_count.intValue==0) {
                [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2025", @"无更多搜索结果")];
            }
            [articleView RBArticleViewSetRefreshViewFinish];
        }
        //
        NSString *articles_count = [jsonObject objectForKey:@"articles_count"];
        if(articles_count.intValue < 10) {
            articleView.tableviewn.mj_footer = nil;
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [articleView RBArticleViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [articleView RBArticleViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
