//
//  RBEngineDetailViewController.h
//  RB
//
//  Created by AngusNi on 5/10/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "BTSpiderPlotterView.h"
#import "CloudView.h"
#import "RBRadarView.h"
#import "MCCircleChartView.h"
#import "RBRankingDetailSwitchView.h"
#import "RBArticleView.h"
#import "DBSphereView.h"
#import "RBWeiboView.h"
#import "RBWechatView.h"
#import "RBZhihuView.h"
#import "RBMeipaiView.h"
#import "TOWebViewController.h"
#import "RBUserContactKolViewController.h"


@interface RBEngineDetailViewController : RBBaseViewController
<RBBaseVCDelegate,RBMeipaiViewDelegate,RBWechatViewDelegate,RBWeiboViewDelegate,RBRankingDetailSwitchViewDelegate,UIScrollViewDelegate,MCCircleChartViewDataSource,MCCircleChartViewDelegate,RBZhihuViewDelegate>

@property(nonatomic, strong) RBRankingWeiboEntity*entity;

@property(nonatomic, strong) RBKOLSocialEntity*socialEntity;

@end
