//
//  RBWechatView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBWechatTableViewCell.h"

@class RBWechatView;
@protocol RBWechatViewDelegate <NSObject>
@optional
- (void)RBWechatViewLoadRefreshViewData:(RBWechatView *)view;
- (void)RBWechatViewSelected:(RBWechatView *)view andIndex:(int)index;

@end


@interface RBWechatView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBWechatViewDelegate> delegate;
@property(nonatomic, strong) UIView *defaultView;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;

- (void)RBWechatViewLoadRefreshViewFirstData;
- (void)RBWechatViewSetRefreshViewFinish;

@end