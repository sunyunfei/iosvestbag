//
//  RBRankingView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBRankingTableViewCell.h"

@class RBRankingView;
@protocol RBRankingViewDelegate <NSObject>
@optional
- (void)RBRankingViewLoadRefreshViewData:(RBRankingView *)view;
- (void)RBRankingViewSelected:(RBRankingView *)view andIndex:(int)index;

@end


@interface RBRankingView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBRankingViewDelegate> delegate;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) UIView *defaultView;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
- (void)RBRankingViewLoadRefreshViewFirstData;
- (void)RBRankingViewSetRefreshViewFinish;

@end