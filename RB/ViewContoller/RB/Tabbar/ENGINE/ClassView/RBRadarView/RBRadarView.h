//
//  RBRadarView.h
//  RB
//
//  Created by AngusNi on 5/11/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBRadarView : UIView
- (void)loadRBRadarView:(NSMutableArray*)list andColor:(NSString*)color;
@end
