//
//  RBRadarView.m
//  RB
//
//  Created by AngusNi on 5/11/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBRadarView.h"
#import "Service.h"
#import "BTSpiderPlotterView.h"

@implementation RBRadarView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void)loadRBRadarView:(NSMutableArray*)list andColor:(NSString*)color {
    [self removeAllSubviews];
    //
    RBRankingTagsEntity *entity0 = list[0];
    RBRankingTagsEntity *entity1 = list[1];
    RBRankingTagsEntity *entity2 = list[2];
    RBRankingTagsEntity *entity3 = list[3];
    RBRankingTagsEntity *entity4 = list[4];
    NSDictionary *valueDic = @{@"1": [NSString stringWithFormat:@"%.2f",entity0.weight.floatValue],
                               @"2": [NSString stringWithFormat:@"%.2f",entity1.weight.floatValue],
                               @"3": [NSString stringWithFormat:@"%.2f",entity2.weight.floatValue],
                               @"4": [NSString stringWithFormat:@"%.2f",entity3.weight.floatValue],
                               @"5": [NSString stringWithFormat:@"%.2f",entity4.weight.floatValue]};
    //    NSDictionary *valueDic = @{@"1": @"0.8",//活跃程度
    //                                          @"2": @"0.9",//佳文分享
    //                                          @"3": @"0.9",//身份特质
    //                                          @"4": @"0.9",//参加悬赏
    //                                          @"5": @"0.5"};//人脉关系
    //    oneLabel.text = NSLocalizedString(@"R5056", @"身份特质");
    //    twoLabel.text = NSLocalizedString(@"R5060", @"人脉关系");
    //    threeLabel.text = NSLocalizedString(@"R5059", @"佳文分享");
    //    fourLabel.text = @"参加悬赏";
    //    fiveLabel.text = NSLocalizedString(@"R5057", @"活跃程度");
    //
    UILabel*oneLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.width, 16.0)];
    oneLabel.font = font_cu_13;
    oneLabel.textAlignment = NSTextAlignmentCenter;
    oneLabel.textColor = [Utils getUIColorWithHexString:color];
    [self addSubview:oneLabel];
    //
    BTSpiderPlotterView*spiderView = [[BTSpiderPlotterView alloc] initWithFrame:CGRectMake(70.0, oneLabel.bottom+5.0, self.width-70.0*2.0, self.width-70.0*2.0) valueDictionary:valueDic];
    [spiderView setMaxValue:1];
    [spiderView setValueDivider:0.33333];
    [spiderView setDrawboardColor:[Utils getUIColorWithHexString:SysColorBlue]];
    [spiderView setPlotColor:[Utils getUIColorWithHexString:SysColorBlue andAlpha:0.58]];
    [spiderView setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
    [self addSubview:spiderView];
    //
    UILabel*twoLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, spiderView.top+spiderView.height/3.0-5.0, 69.0, oneLabel.height)];
    twoLabel.font = oneLabel.font;
    twoLabel.textAlignment = NSTextAlignmentRight;
    twoLabel.textColor = oneLabel.textColor;
    [self addSubview:twoLabel];
    //
    UILabel*threeLabel = [[UILabel alloc]initWithFrame:CGRectMake(70.0-10.0, spiderView.bottom-15.0,spiderView.width/2.0, oneLabel.height)];
    threeLabel.font = oneLabel.font;
    threeLabel.textAlignment = NSTextAlignmentCenter;
    threeLabel.textColor = oneLabel.textColor;
    [self addSubview:threeLabel];
    //
    UILabel*fourLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.width/2.0+10.0, threeLabel.top, spiderView.width/2.0, oneLabel.height)];
    fourLabel.font = oneLabel.font;
    fourLabel.textAlignment = NSTextAlignmentCenter;
    fourLabel.textColor = oneLabel.textColor;
    [self addSubview:fourLabel];
    //
    UILabel*fiveLabel = [[UILabel alloc]initWithFrame:CGRectMake(spiderView.right, twoLabel.top, self.width, oneLabel.height)];
    fiveLabel.font = oneLabel.font;
    fiveLabel.textAlignment = NSTextAlignmentLeft;
    fiveLabel.textColor = oneLabel.textColor;
    [self addSubview:fiveLabel];
    //
    oneLabel.text = entity0.text;
    twoLabel.text = entity1.text;
    threeLabel.text = entity2.text;
    fourLabel.text = entity3.text;
    fiveLabel.text = entity4.text;
}

@end
