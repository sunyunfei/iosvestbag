//
//  RBRankingDetailSwitchView.m
//  RB
//
//  Created by AngusNi on 3/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBRankingDetailSwitchView.h"
#import "Service.h"

@implementation RBRankingDetailSwitchView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        //
        self.topLineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.topLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self addSubview:self.topLineLabel];
        //
        self.scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0.5f, frame.size.width, frame.size.height-0.5f)];
        self.scrollviewn.bounces = NO;
        self.scrollviewn.pagingEnabled = NO;
        self.scrollviewn.scrollEnabled = YES;
        self.scrollviewn.showsHorizontalScrollIndicator = NO;
        self.scrollviewn.showsVerticalScrollIndicator = NO;
        self.scrollviewn.delegate = self;
        [self.scrollviewn setContentSize:CGSizeMake(self.scrollviewn.width, self.scrollviewn.height)];
        [self addSubview:self.scrollviewn];
        //
        self.lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.scrollviewn addSubview:self.lineLabel];
        //
//        self.tipLabel = [[UILabel alloc]initWithFrame:CGRectZero];
//        self.tipLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
//        [self.scrollviewn addSubview:self.tipLabel];
    }
    return self;
}


#pragma mark - RBRankingDetailSwitchView Delegate
- (void)RBRankingDetailSwitchViewShow:(NSArray *)data andSelected:(int)tag {
    float tempWidth = ScreenWidth/5.0;
    if([data count]<5) {
        tempWidth=ScreenWidth/[data count];
    }
    if(self.dataArray==nil) {
        self.dataArray = data;
        [self.scrollviewn setContentSize:CGSizeMake([self.dataArray count]*tempWidth, self.scrollviewn.height)];
        //
        for (int i=0; i<[self.dataArray count]; i++) {
            UIButton*tabBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            tabBtn.frame = CGRectMake(tempWidth*i, 0, tempWidth, self.scrollviewn.height);
            tabBtn.titleLabel.font = font_cu_15;
            [tabBtn setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
            tabBtn.tag = 5000+i;
            tabBtn.backgroundColor = [UIColor clearColor];
            [tabBtn addTarget:self
                       action:@selector(tabBtnAction:)
             forControlEvents:UIControlEventTouchUpInside];
            [tabBtn setTitle:self.dataArray[i] forState:UIControlStateNormal];
            [self.scrollviewn addSubview:tabBtn];
            //
            UILabel*tabLabel = [[UILabel alloc]initWithFrame:CGRectMake(tabBtn.right, tabBtn.top+10.0, 0.5f, tabBtn.height-20.0)];
            tabLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorGray];
            [self.scrollviewn addSubview:tabLabel];
        }
    }
    for (int i=0; i<[self.dataArray count]; i++) {
        UIButton*tabBtn = (UIButton*)[self.scrollviewn viewWithTag:5000+i];
        [tabBtn setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
    }
    self.selectedTag = tag;
    //
    UIButton*tabBtn = (UIButton*)[self.scrollviewn viewWithTag:5000+self.selectedTag];
    CGSize tabLabelSize = [Utils getUIFontSizeFitW:tabBtn.titleLabel];
    self.tipLabel.frame = CGRectMake(tempWidth*tag+(tempWidth-tabLabelSize.width-20.0)/2.0, self.scrollviewn.height -2.0, tabLabelSize.width+20.0, 2.0);
    [tabBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    self.lineLabel.frame = CGRectMake(0, self.scrollviewn.height-0.5f, self.scrollviewn.contentSize.width, 0.5f);
    self.topLineLabel.frame = CGRectMake(0, 0, self.scrollviewn.width, 0.5f);
    //
    if(self.selectedTag>=4) {
        float w = tempWidth*tag+(tempWidth-tabLabelSize.width-20.0)/2.0;
        [self.scrollviewn scrollRectToVisible:CGRectMake(w-ScreenWidth/2.0, 0, self.scrollviewn.width, self.scrollviewn.height) animated:YES];
    }
}

#pragma mark - UIButton Delegate
- (void)tabBtnAction:(UIButton *)sender {
    self.selectedTag = (int)sender.tag-5000;
    [self RBRankingDetailSwitchViewShow:self.dataArray andSelected:self.selectedTag];
    if ([self.delegate
         respondsToSelector:@selector(RBRankingDetailSwitchViewSelected:andIndex:)]) {
        [self.delegate RBRankingDetailSwitchViewSelected:self andIndex:self.selectedTag];
    }
}


@end
