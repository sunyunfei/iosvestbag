//
//  RBRankingDetailSwitchView.h
//  RB
//
//  Created by AngusNi on 3/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RBRankingDetailSwitchView;
@protocol RBRankingDetailSwitchViewDelegate <NSObject>
@optional
- (void)RBRankingDetailSwitchViewSelected:(RBRankingDetailSwitchView *)view andIndex:(int)index;
@end

@interface RBRankingDetailSwitchView : UIView
<UIScrollViewDelegate>
@property(nonatomic, weak) id<RBRankingDetailSwitchViewDelegate> delegate;
@property(nonatomic, strong) UIScrollView*scrollviewn;
@property(nonatomic, strong) UILabel*topLineLabel;
@property(nonatomic, strong) UILabel*tipLabel;
@property(nonatomic, strong) UILabel*lineLabel;
@property(nonatomic, strong) NSArray*dataArray;
@property(nonatomic, assign) int selectedTag;
-(void)RBRankingDetailSwitchViewShow:(NSArray*)data andSelected:(int)tag;

@end