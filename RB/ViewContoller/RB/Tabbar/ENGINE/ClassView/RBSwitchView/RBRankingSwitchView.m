//
//  RBRankingSwitchView.m
//  RB
//
//  Created by AngusNi on 3/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBRankingSwitchView.h"
#import "Service.h"

@implementation RBRankingSwitchView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
//        self.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
        //
        self.scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.scrollviewn.bounces = NO;
        self.scrollviewn.pagingEnabled = NO;
        self.scrollviewn.scrollEnabled = YES;
        self.scrollviewn.showsHorizontalScrollIndicator = NO;
        self.scrollviewn.showsVerticalScrollIndicator = NO;
        self.scrollviewn.delegate = self;
        [self addSubview:self.scrollviewn];
        //
        self.tipLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.tipLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
//        [self.scrollviewn addSubview:self.tipLabel];
        //
        self.lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.scrollviewn addSubview:self.lineLabel];
    }
    return self;
}


#pragma mark - RBRankingSwitchView Delegate
- (void)RBRankingSwitchViewShow:(NSArray *)data andSelected:(int)tag {
    float tempWidth = ScreenWidth/5.0;
    if([data count]<5) {
        tempWidth = (float)ScreenWidth/(float)[data count];
    }
    self.selectedTag = tag;
    if(self.dataArray==nil) {
        self.dataArray = data;
        for (int i=0; i<[self.dataArray count]; i++) {
            UIButton*tabBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            tabBtn.frame = CGRectMake(tempWidth*i, 0, tempWidth, self.scrollviewn.height);
            tabBtn.backgroundColor = [UIColor clearColor
                                      ];
            tabBtn.titleLabel.font = font_15;
            [tabBtn setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
            [tabBtn setTitle:self.dataArray[i] forState:UIControlStateNormal];
            tabBtn.tag = 50000+i;
            [tabBtn addTarget:self
                       action:@selector(tabBtnAction:)
             forControlEvents:UIControlEventTouchUpInside];
            [self.scrollviewn addSubview:tabBtn];
        }
        [self.scrollviewn setContentSize:CGSizeMake([self.dataArray count]*tempWidth, self.scrollviewn.height)];
        self.lineLabel.frame = CGRectMake(0, self.height-0.5f, self.scrollviewn.contentSize.width, 0.5f);
    }
    for (int i=0; i<[self.dataArray count]; i++) {
        UIButton*tabBtn1 = (UIButton*)[self.scrollviewn viewWithTag:50000+i];
        tabBtn1.titleLabel.font = font_15;
        [tabBtn1 setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
    }
    UIButton*tabBtn2 = (UIButton*)[self.scrollviewn viewWithTag:50000+self.selectedTag];
    tabBtn2.titleLabel.font = font_cu_cu_15;
    [tabBtn2 setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
    CGSize tabLabelSize = [Utils getUIFontSizeFitW:tabBtn2.titleLabel];
    self.tipLabel.frame = CGRectMake(tempWidth*self.selectedTag+(tempWidth-tabLabelSize.width-10.0)/2.0, self.height-3.0, tabLabelSize.width+10.0, 3.0);
    //
    if(self.selectedTag>=4) {
        float w = self.tipLabel.left+self.tipLabel.width/2.0-ScreenWidth/2.0;
        [self.scrollviewn scrollRectToVisible:CGRectMake(w, 0, self.scrollviewn.width, self.scrollviewn.height) animated:YES];
    } else {
        [self.scrollviewn scrollRectToVisible:CGRectMake(0, 0, self.scrollviewn.width, self.scrollviewn.height) animated:YES];
    }
}


#pragma mark - UIButton Delegate
- (void)tabBtnAction:(UIButton *)sender {
    self.selectedTag = (int)sender.tag-50000;
    [self RBRankingSwitchViewShow:self.dataArray andSelected:self.selectedTag];
    if ([self.delegate
         respondsToSelector:@selector(RBRankingSwitchViewSelected:andIndex:)]) {
        [self.delegate RBRankingSwitchViewSelected:self andIndex:self.selectedTag];
    }
}


@end
