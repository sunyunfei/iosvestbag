//
//  RBRankingSwitchView.h
//  RB
//
//  Created by AngusNi on 3/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RBRankingSwitchView;
@protocol RBRankingSwitchViewDelegate <NSObject>
@optional
- (void)RBRankingSwitchViewSelected:(RBRankingSwitchView *)view andIndex:(int)index;
@end

@interface RBRankingSwitchView : UIView
<UIScrollViewDelegate>
@property(nonatomic, weak) id<RBRankingSwitchViewDelegate> delegate;
@property(nonatomic, strong) UIScrollView*scrollviewn;
@property(nonatomic, strong) UILabel*topLineLabel;
@property(nonatomic, strong) UILabel*tipLabel;
@property(nonatomic, strong) UILabel*lineLabel;
@property(nonatomic, strong) NSArray*dataArray;
@property(nonatomic, assign) int selectedTag;
-(void)RBRankingSwitchViewShow:(NSArray*)data andSelected:(int)tag;

@end