//
//  RBMeipaiView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBMeipaiTableViewCell.h"

@class RBMeipaiView;
@protocol RBMeipaiViewDelegate <NSObject>
@optional
- (void)RBMeipaiViewLoadRefreshViewData:(RBMeipaiView *)view;
- (void)RBMeipaiViewSelected:(RBMeipaiView *)view andIndex:(int)index;

@end


@interface RBMeipaiView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBMeipaiViewDelegate> delegate;
@property(nonatomic, strong) UIView *defaultView;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;

- (void)RBMeipaiViewLoadRefreshViewFirstData;
- (void)RBMeipaiViewSetRefreshViewFinish;

@end