//
//  RBEngineSearchView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"

@class RBEngineSearchView;
@protocol RBEngineSearchViewDelegate <NSObject>
@optional
- (void)RBEngineSearchViewSearchClear:(RBEngineSearchView *)view;
- (void)RBEngineSearchViewSearchCancel:(RBEngineSearchView *)view;
- (void)RBEngineSearchViewSearch:(RBEngineSearchView *)view andText:(NSString*)text;
@end


@interface RBEngineSearchView : UIView
<UITextFieldDelegate>
@property(nonatomic, weak) id<RBEngineSearchViewDelegate> delegate;
@property(nonatomic, strong) UILabel*typeLabel;
@property(nonatomic, strong) UIView*bgView;
@property(nonatomic, strong) UIButton*cancelBtn;
@property(nonatomic, strong) InsetsTextField*searchTF;
@end
