//
//  RBEngineView.m
//  RB
//
//  Created by AngusNi on 6/30/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBEngineView.h"

@interface RBEngineView () {
}
@end

@implementation RBEngineView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor whiteColor];
        //
        self.tableviewn = [[UITableView alloc]
                           initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)
                           style:UITableViewStylePlain];
        self.tableviewn.backgroundView = nil;
        self.tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableviewn.backgroundColor = [UIColor clearColor];
        self.tableviewn.delegate = self;
        self.tableviewn.dataSource = self;
        if (@available(iOS 11.0,*)) {
            self.tableviewn.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            self.tableviewn.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        [self addSubview:self.tableviewn];
        [self setRefreshHeaderView];
        //
        UIView*headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, (ScreenWidth/750.0)*470.0+40.0)];
        headView.userInteractionEnabled = YES;
        //
        UIImageView*bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, headView.width, (headView.width/750.0)*470.0)];
        bgIV.image = [UIImage imageNamed:@"pic_engine_banner.jpg"];
        [headView addSubview:bgIV];
        //
        UIButton*searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        searchBtn.frame = CGRectMake(CellLeft, bgIV.bottom - 60.0, headView.width-2*CellLeft, ((headView.width-2*CellLeft)/700.0)*80.0);
        [searchBtn setBackgroundImage:[UIImage imageNamed:@"pic_engine_box.png"] forState:UIControlStateNormal];
        [searchBtn addTarget:self action:@selector(searchBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [headView addSubview:searchBtn];
        //
        UIView*tView = [[UIView alloc]initWithFrame:CGRectMake(0, bgIV.bottom, bgIV.width, 40.0)];
        tView.backgroundColor = [UIColor whiteColor];
        [headView addSubview:tView];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, CellLeft, 3.0, 40.0-2*CellLeft)];
        tLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
        [tView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectMake(tLabel.right+CellLeft, 0, tView.width-(tLabel.right+CellLeft), 40.0)];
        cLabel.text = @"热门榜单";
        cLabel.font = font_15;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [tView addSubview:cLabel];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, tView.height-0.5, tView.width, 0.5)];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [tView addSubview:lineLabel];
        [self.tableviewn setTableHeaderView:headView];
    }
    return self;
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)RBEngineViewLoadRefreshViewFirstData {
    self.pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    self.pageSize = 10;
    if ([self.delegate
         respondsToSelector:@selector(RBEngineViewLoadRefreshViewData:)]) {
        [self.delegate RBEngineViewLoadRefreshViewData:self];
    }
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    self.tableviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    self.tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.pageIndex = self.pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)RBEngineViewSetRefreshViewFinish {
    [self.tableviewn.mj_header endRefreshing];
    [self.tableviewn.mj_footer endRefreshing];
//    if (self.tableviewn.mj_footer == nil) {
//        [self setRefreshFooterView];
//    }
    [UIView performWithoutAnimation:^{
        [self.tableviewn reloadData];
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self.datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"Cell%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBEngineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBEngineTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                    reuseIdentifier:cellIdentifier];
    }
    [cell loadRBEngineCellWith:self.datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate
         respondsToSelector:@selector(RBEngineViewSelected:andIndex:)]) {
        [self.delegate RBEngineViewSelected:self andIndex:(int)indexPath.row];
    }
}
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return (ScreenWidth/750.0)*470.0+40.0;
//}
#pragma mark - UIButton Delegate
- (void)searchBtnAction:(UIButton*)sender {
    if ([self.delegate
         respondsToSelector:@selector(RBEngineViewSearchButtonAction:)]) {
        [self.delegate RBEngineViewSearchButtonAction:self];
    }
}

@end

