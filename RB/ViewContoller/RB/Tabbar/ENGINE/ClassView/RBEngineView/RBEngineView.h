//
//  RBEngineView.h
//  RB
//
//  Created by AngusNi on 6/30/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBEngineTableViewCell.h"

@class RBEngineView;
@protocol RBEngineViewDelegate <NSObject>
@optional
- (void)RBEngineViewLoadRefreshViewData:(RBEngineView *)view;
- (void)RBEngineViewSelected:(RBEngineView *)view andIndex:(int)index;
- (void)RBEngineViewSearchButtonAction:(RBEngineView *)view;
@end

@interface RBEngineView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBEngineViewDelegate> delegate;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
- (void)RBEngineViewLoadRefreshViewFirstData;
- (void)RBEngineViewSetRefreshViewFinish;

@end