//
//  RBEngineFilterView.h
//  RB
//
//  Created by AngusNi on 7/1/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RBEngineFilterView;
@protocol RBEngineFilterViewDelegate <NSObject>
@optional
- (void)RBEngineFilterViewSelected:(RBEngineFilterView *)view andIndex:(int)index;
@end

@interface RBEngineFilterView : UIView
@property(nonatomic, weak) id<RBEngineFilterViewDelegate> delegate;
@property(nonatomic, strong) NSArray*dataArray;
@property(nonatomic, strong) NSMutableArray*categoryArray;
@property(nonatomic, strong) NSMutableArray*keywordsArray;
@property(nonatomic, strong) NSMutableArray*brandsArray;

@property(nonatomic, strong) NSMutableArray*categorySArray;
@property(nonatomic, strong) NSMutableArray*keywordsSArray;
@property(nonatomic, strong) NSMutableArray*brandsSArray;
@property(nonatomic, strong) UIView*filterBgView;
@property(nonatomic, assign) int selectedTag;
-(void)RBEngineFilterViewSelected:(int)tag;
@end