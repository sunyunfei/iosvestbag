//
//  RBEngineSearchView.m
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBEngineSearchView.h"
#import "Service.h"
@interface RBEngineSearchView () {
}
@end
@implementation RBEngineSearchView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = CGRectMake(0, 0, ScreenWidth, NavHeight);
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor whiteColor];
        //
        NSMutableParagraphStyle *paragraphStyle  =
        [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
        CGSize labelSize  =
        [NSLocalizedString(@"R1011", @"取消") boundingRectWithSize:CGSizeMake(1000, 20)
                                                         options:NSStringDrawingUsesLineFragmentOrigin |
         NSStringDrawingUsesFontLeading
                                                      attributes:@{
                                                                   NSFontAttributeName : font_cu_15,
                                                                   NSParagraphStyleAttributeName : paragraphStyle
                                                                   } context:nil]
        .size;
        //
        self.bgView = [[UIView alloc]initWithFrame:CGRectMake(CellLeft, 20.0+(44.0-30.0)/2.0, ScreenWidth-CellLeft*3.0-labelSize.width, 30.0)];
        self.bgView.layer.borderWidth = 0.5;
        self.bgView.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlack]CGColor];
        self.bgView.layer.masksToBounds = YES;
        self.bgView.userInteractionEnabled = YES;
        [self addSubview:self.bgView];
        //
        UIButton*typeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        typeBtn.frame = CGRectMake(CellLeft, 20.0+(44.0-30.0)/2.0, 84+8.4, 30.0);
        [typeBtn addTarget:self action:@selector(typeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:typeBtn];
        //
        self.typeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 84, self.bgView.height)];
        self.typeLabel.font = font_cu_13;
        self.typeLabel.textAlignment = NSTextAlignmentCenter;
        self.typeLabel.text = NSLocalizedString(@"R3002",@"微信公众号");
        self.typeLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self.bgView addSubview:self.typeLabel];
        //
        UIImageView*typeIV = [[UIImageView alloc]initWithFrame:CGRectMake(self.typeLabel.right, (self.bgView.height-7.0*0.7)/2.0, 12.0*0.7, 7.0*0.7)];
        typeIV.image = [UIImage imageNamed:@"icon_engine_down.png"];
        [self.bgView addSubview:typeIV];
        //
        //        UILabel*iconLabel = [[UILabel alloc]initWithFrame:CGRectMake(typeIV.right, (self.bgView.height-15.0)/2.0, 15, 15)];
        //        iconLabel.font = font_icon_(iconLabel.width);
        //        iconLabel.text = Icon_bar_search;
        //        iconLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        //        [self.bgView addSubview:iconLabel];
        //
        self.searchTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(typeIV.right+5.0, 0, self.bgView.width-(typeIV.right+5.0), self.bgView.height)];
        self.searchTF.font = font_cu_15;
        self.searchTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.searchTF.placeholder = NSLocalizedString(@"R2010",@"搜索");
        self.searchTF.keyboardType = UIKeyboardTypeDefault;
        self.searchTF.returnKeyType = UIReturnKeySearch;
        self.searchTF.delegate = self;
        self.searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self.bgView addSubview:self.searchTF];
        //
        self.cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.cancelBtn.frame = CGRectMake(self.bgView.right, self.bgView.top, labelSize.width+2*CellLeft, self.bgView.height);
        self.cancelBtn.titleLabel.font = font_cu_15;
        [self.cancelBtn setTitle:NSLocalizedString(@"R1011", @"取消") forState:UIControlStateNormal];
        [self.cancelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
        [self.cancelBtn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.cancelBtn];
        //
        UILabel *navLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NavHeight-0.5, ScreenWidth, 0.5)];
        navLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBarLine];
        [self addSubview:navLineLabel];
    }
    return self;
}

- (BOOL)becomeFirstResponder{
    return YES;
}

#pragma mark - UIButton Delegate
-(void)typeBtnAction:(UIButton*)sender {
    [KxMenu setTintColor:[Utils getUIColorWithHexString:SysColorWhite]];
    [KxMenu setTitleFont:font_cu_13];
    NSMutableArray *menuItems = [[NSMutableArray alloc]initWithCapacity:20];
    KxMenuItem *item0 = [KxMenuItem menuItem:NSLocalizedString(@"R3002",@"微信公众号") image:nil target:self action:@selector(menuItem0:)];
    item0.alignment = NSTextAlignmentCenter;
    item0.foreColor = [Utils getUIColorWithHexString:SysColorBlack];
    [menuItems addObject:item0];
    KxMenuItem *item1 = [KxMenuItem menuItem:NSLocalizedString(@"R3001",@"新浪微博") image:nil target:self action:@selector(menuItem1:)];
    item1.alignment = item0.alignment;
    item1.foreColor = item0.foreColor;
    [menuItems addObject:item1];
    KxMenuItem *item2 = [KxMenuItem menuItem:NSLocalizedString(@"R30020",@"知乎") image:nil target:self action:@selector(menuItem2:)];
    item2.alignment = item0.alignment;
    item2.foreColor = item0.foreColor;
    [menuItems addObject:item2];
    KxMenuItem *item3 = [KxMenuItem menuItem:NSLocalizedString(@"R30021",@"美拍") image:nil target:self action:@selector(menuItem3:)];
    item3.alignment = item0.alignment;
    item3.foreColor = item0.foreColor;
    [menuItems addObject:item3];
    KxMenuItem *item4 = [KxMenuItem menuItem:NSLocalizedString(@"R30022",@"秒拍") image:nil target:self action:@selector(menuItem4:)];
    item4.alignment = item0.alignment;
    item4.foreColor = item0.foreColor;
    [menuItems addObject:item4];
    [KxMenu showMenuInView:self.superview fromRect:CGRectMake(CellLeft, 0, 84.0+8.4, 57.0) menuItems:menuItems];
}

#pragma mark - UIButton Delegate
- (void)menuItem0:(KxMenuItem *)sender {
    self.typeLabel.text = NSLocalizedString(@"R3002",@"微信公众号");
    [self dismissMenu];
}

- (void)menuItem1:(KxMenuItem *)sender {
    self.typeLabel.text = NSLocalizedString(@"R3001",@"新浪微博");
    [self dismissMenu];
}

- (void)menuItem2:(KxMenuItem *)sender {
    self.typeLabel.text = NSLocalizedString(@"R30020",@"知乎");
    [self dismissMenu];
}

- (void)menuItem3:(KxMenuItem *)sender {
    self.typeLabel.text = NSLocalizedString(@"R30021",@"美拍");
    [self dismissMenu];
}

- (void)menuItem4:(KxMenuItem *)sender {
    self.typeLabel.text = NSLocalizedString(@"R30022",@"秒拍");
    [self dismissMenu];
}

- (void)dismissMenu {
    [KxMenu dismissMenu];
    //
    if(self.searchTF.text.length!=0){
        if ([self.delegate
             respondsToSelector:@selector(RBEngineSearchViewSearch:andText:)]) {
            [self.delegate RBEngineSearchViewSearch:self andText:self.searchTF.text];
        }
    }
}

-(void)cancelBtnAction:(UIButton*)sender {
    [self endEditing:YES];
    if ([self.delegate
         respondsToSelector:@selector(RBEngineSearchViewSearchCancel:)]) {
        [self.delegate RBEngineSearchViewSearchCancel:self];
    }
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.delegate
         respondsToSelector:@selector(RBEngineSearchViewSearch:andText:)]) {
        [self.delegate RBEngineSearchViewSearch:self andText:self.searchTF.text];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    if ([self.delegate
         respondsToSelector:@selector(RBEngineSearchViewSearchClear:)]) {
        [self.delegate RBEngineSearchViewSearchClear:self];
    }
    return YES;
}

@end
