//
//  RBEngineSearchHistoryView.h
//  RB
//
//  Created by AngusNi on 3/29/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBEngineSearchView.h"

@class RBEngineSearchHistoryView;
@protocol RBEngineSearchHistoryViewDelegate <NSObject>
@optional
- (void)RBEngineSearchHistoryViewLoadRefreshViewData:(RBEngineSearchHistoryView *)view;
- (void)RBEngineSearchHistoryViewSelected:(RBEngineSearchHistoryView *)view andIndex:(int)index;

@end


@interface RBEngineSearchHistoryView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBEngineSearchHistoryViewDelegate> delegate;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) UIButton *clearBtn;
@property(nonatomic, strong) NSMutableArray *datalist;

@end