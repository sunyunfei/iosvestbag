//
//  RBEngineFilterView.m
//  RB
//
//  Created by AngusNi on 7/1/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBEngineFilterView.h"
#import "Service.h"

@implementation RBEngineFilterView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        self.dataArray = @[NSLocalizedString(@"R4006", @"综合排序"),NSLocalizedString(@"R4007", @"相关度排序"),NSLocalizedString(@"R4008", @"影响力排序"),NSLocalizedString(@"R4009", @"筛选")];
        //
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
        CGSize labelSize = [NSLocalizedString(@"R4006", @"综合排序") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_cu_15,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
        CGSize labelSize1 = [NSLocalizedString(@"R4007", @"相关度排序") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_cu_15,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
        CGSize labelSize2 = [NSLocalizedString(@"R4008", @"影响力排序") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_cu_15,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
        CGSize labelSize3 = [NSLocalizedString(@"R4009", @"筛选") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_cu_15,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
        float gap = (ScreenWidth*3.0/4.0 - labelSize.width-labelSize1.width-labelSize2.width)/4.0;
        for (int i=0; i<[self.dataArray count]; i++) {
            UIButton*tabBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            if (i==0) {
                tabBtn.frame = CGRectMake(gap, 0, labelSize.width, self.height);
            }
            if (i==1) {
                tabBtn.frame = CGRectMake(gap*2+labelSize.width, 0, labelSize1.width, self.height);
            }
            if (i==2) {
                tabBtn.frame = CGRectMake(gap*3+labelSize.width+labelSize1.width, 0, labelSize2.width, self.height);
            }
            if (i==3) {
                tabBtn.frame = CGRectMake(ScreenWidth*3.0/4.0, 0, ScreenWidth/4.0, self.height);
                UIImageView*tabIV = [[UIImageView alloc]initWithFrame:CGRectMake((tabBtn.width-labelSize3.width)/2.0+labelSize3.width, (tabBtn.height-3.5)/2.0, 6.0, 3.5)];
                tabIV.image = [UIImage imageNamed:@"icon_engine_filter_un.png"];
                tabIV.tag = 1000;
                [tabBtn addSubview:tabIV];
            }
            tabBtn.titleLabel.font = font_cu_13;
            [tabBtn setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
            tabBtn.tag = 5000+i;
            tabBtn.backgroundColor = [UIColor clearColor];
            [tabBtn addTarget:self
                       action:@selector(tabBtnAction:)
             forControlEvents:UIControlEventTouchUpInside];
            [tabBtn setTitle:self.dataArray[i] forState:UIControlStateNormal];
            [self addSubview:tabBtn];
        }
        UILabel *lineLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth*3.0/4.0, 0, 0.5, self.height)];
        lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self addSubview:lineLabel1];
        //
        UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.height-0.5, ScreenWidth, 0.5)];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self addSubview:lineLabel];
    }
    return self;
}


#pragma mark - RBEngineFilterView Delegate
- (void)RBEngineFilterViewSelected:(int)tag {
    self.selectedTag = tag;
    if (self.selectedTag!=3) {
        for (int i=0; i<[self.dataArray count]; i++) {
            UIButton*tabBtn = (UIButton*)[self viewWithTag:5000+i];
            [tabBtn setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        }
        UIButton*tabBtn = (UIButton*)[self viewWithTag:5000+self.selectedTag];
        [tabBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    }
    if(!([self.keywordsSArray count]==0&&[self.brandsSArray count]==0&&[self.categorySArray count]==0)){
        UIButton*tabBtn = (UIButton*)[self viewWithTag:5000+3];
        [tabBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        UIImageView*tabIV = (UIImageView*)[tabBtn viewWithTag:1000];
        tabIV.image = [UIImage imageNamed:@"icon_engine_filter_in.png"];
    } else {
        UIButton*tabBtn = (UIButton*)[self viewWithTag:5000+3];
        [tabBtn setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        UIImageView*tabIV = (UIImageView*)[tabBtn viewWithTag:1000];
        tabIV.image = [UIImage imageNamed:@"icon_engine_filter_un.png"];
    }
}

#pragma mark - UIButton Delegate
- (void)tabBtnAction:(UIButton *)sender {
    self.selectedTag = (int)sender.tag-5000;
    if(self.selectedTag==3) {
        if(self.filterBgView!=nil) {
            [self.filterBgView removeAllSubviews];
            [self.filterBgView removeFromSuperview];
            self.filterBgView = nil;
            [self RBEngineFilterViewSelected:self.selectedTag];
        } else {
            [self loadFilterView];
        }
    } else {
        [self.filterBgView removeAllSubviews];
        [self.filterBgView removeFromSuperview];
        self.filterBgView = nil;
        [self RBEngineFilterViewSelected:self.selectedTag];
        if ([self.delegate
             respondsToSelector:@selector(RBEngineFilterViewSelected:andIndex:)]) {
            [self.delegate RBEngineFilterViewSelected:self andIndex:self.selectedTag];
        }
    }
}

#pragma mark - loadFilterView Delegate
- (void)loadFilterView {
    UIButton*tabBtn = (UIButton*)[self viewWithTag:5000+3];
    [tabBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    UIImageView*tabIV = (UIImageView*)[tabBtn viewWithTag:1000];
    tabIV.image = [UIImage imageNamed:@"icon_engine_filter.png"];
    //
    [self.filterBgView removeAllSubviews];
    [self.filterBgView removeFromSuperview];
    self.filterBgView = nil;
    self.filterBgView = [[UIView alloc]initWithFrame:CGRectMake(0, self.bottom, ScreenWidth, ScreenHeight-self.bottom)];
    self.filterBgView.backgroundColor = SysColorCoverDeep;
    [self.superview addSubview:self.filterBgView];
    //
    UIButton*bottomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    bottomBtn.frame = CGRectMake(0, self.filterBgView.height-100.0, ScreenWidth, 100.0);
    bottomBtn.tag = 5000+3;
    [bottomBtn addTarget:self
                      action:@selector(tabBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
    [self.filterBgView addSubview:bottomBtn];
    //
    UIScrollView*scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, self.filterBgView.height-68.0-100.0)];
    scrollviewn.bounces = NO;
    scrollviewn.pagingEnabled = NO;
    scrollviewn.scrollEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.backgroundColor = [UIColor whiteColor];
    [self.filterBgView addSubview:scrollviewn];
    //
    UIView*bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, scrollviewn.bottom, ScreenWidth, 68.0)];
    bottomView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.filterBgView addSubview:bottomView];
    //
    UIButton*bottomLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    bottomLeftBtn.frame = CGRectMake((ScreenWidth-104.0*2.0-30.0)/2.0, (bottomView.height-35.0)/2.0, 104.0, 35.0);
    bottomLeftBtn.titleLabel.font = font_13;
    [bottomLeftBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    bottomLeftBtn.backgroundColor = [Utils getUIColorWithHexString:@"8d8d8d"];
    [bottomLeftBtn addTarget:self
                  action:@selector(bottomLeftBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [bottomLeftBtn setTitle:NSLocalizedString(@"R4013", @"重置") forState:UIControlStateNormal];
    [bottomView addSubview:bottomLeftBtn];
    //
    UIButton*bottomRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    bottomRightBtn.frame = CGRectMake(bottomLeftBtn.right+30.0, (bottomView.height-35.0)/2.0, 104.0, 35.0);
    bottomRightBtn.titleLabel.font = font_13;
    [bottomRightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    bottomRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    [bottomRightBtn addTarget:self
                      action:@selector(bottomRightBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
    [bottomRightBtn setTitle:NSLocalizedString(@"R1000", @"完成") forState:UIControlStateNormal];
    [bottomView addSubview:bottomRightBtn];
    //
    UILabel *tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth, 40)];
    tLabel.font = font_cu_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tLabel.text = NSLocalizedString(@"R4010", @"相关行业");
    [scrollviewn addSubview:tLabel];
    NSArray*tLabelArray = @[@"互联网",@"美妆",@"母婴",@"娱乐",@"旅游",@"教育",@"时尚",@"游戏",@"房地产",@"财经",@"数码",@"家电",@"健康",@"图书",@"体育",@"航空",@"家居",@"汽车",@"酒店",@"消费电子",@"摄影",@"手机",@"美食",@"健身",@"音乐",@"综合"];
    if (self.categorySArray==nil) {
        self.categorySArray = [NSMutableArray new];
    }
    float tempWidth = (ScreenWidth-5*CellLeft)/4.0;
    for (int i=0; i<[tLabelArray count]; i++) {
        UIButton*tLabelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        tLabelBtn.frame = CGRectMake(CellLeft+(tempWidth+CellLeft)*(i%4), tLabel.bottom+(30.0+CellLeft)*(i/4), tempWidth, 30.0);
        tLabelBtn.layer.borderWidth = 0.5f;
        tLabelBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
        tLabelBtn.titleLabel.font = font_13;
        [tLabelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        tLabelBtn.tag = 1000+i;
        tLabelBtn.backgroundColor = [UIColor clearColor];
        [tLabelBtn addTarget:self
                   action:@selector(tLabelBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
        [tLabelBtn setTitle:tLabelArray[i] forState:UIControlStateNormal];
        [scrollviewn addSubview:tLabelBtn];
        if([self.categorySArray containsObject:tLabelArray[i]]){
            [tLabelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
            tLabelBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
        }
    }
    //
    UILabel *tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, tLabel.bottom+([tLabelArray count]/4)*(30.0+CellLeft), ScreenWidth, 40)];
    if ([tLabelArray count]%4!=0) {
        tLabel1.top = tLabel1.top+(30.0+CellLeft);
    }
    tLabel1.font = font_cu_15;
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tLabel1.text = NSLocalizedString(@"R4011", @"相关关键词");
    [scrollviewn addSubview:tLabel1];
    if (self.keywordsSArray==nil) {
        self.keywordsSArray = [NSMutableArray new];
    }
    if ([self.keywordsArray count]==0) {
        self.keywordsSArray = [NSMutableArray new];
    }
    for (int i=0; i<[self.keywordsArray count]; i++) {
        UIButton*tLabelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        tLabelBtn.frame = CGRectMake(CellLeft+(tempWidth+CellLeft)*(i%4), tLabel1.bottom+(30.0+CellLeft)*(i/4), tempWidth, 30.0);
        tLabelBtn.layer.borderWidth = 0.5f;
        tLabelBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
        tLabelBtn.titleLabel.font = font_13;
        [tLabelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        tLabelBtn.tag = 2000+i;
        tLabelBtn.backgroundColor = [UIColor clearColor];
        [tLabelBtn addTarget:self
                      action:@selector(tLabel1BtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        [tLabelBtn setTitle:self.keywordsArray[i] forState:UIControlStateNormal];
        [scrollviewn addSubview:tLabelBtn];
        if([self.keywordsSArray containsObject:self.keywordsArray[i]]){
            [tLabelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
            tLabelBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
        }
    }
    //
    UILabel *tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, tLabel1.bottom+([self.keywordsArray count]/4)*(30.0+CellLeft), ScreenWidth, 40)];
    if ([self.keywordsArray count]%4!=0) {
        tLabel2.top = tLabel2.top+(30.0+CellLeft);
    }
    tLabel2.font = font_cu_15;
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tLabel2.text = NSLocalizedString(@"R4012", @"相关品牌");
    [scrollviewn addSubview:tLabel2];
    if (self.brandsSArray==nil) {
        self.brandsSArray = [NSMutableArray new];
    }
    if ([self.brandsArray count]==0) {
        self.brandsSArray = [NSMutableArray new];
    }
    for (int i=0; i<[self.brandsArray count]; i++) {
        UIButton*tLabelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        tLabelBtn.frame = CGRectMake(CellLeft+(tempWidth+CellLeft)*(i%4), tLabel2.bottom+(30.0+CellLeft)*(i/4), tempWidth, 30.0);
        tLabelBtn.layer.borderWidth = 0.5f;
        tLabelBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
        tLabelBtn.titleLabel.font = font_13;
        [tLabelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        tLabelBtn.tag = 2000+i;
        tLabelBtn.backgroundColor = [UIColor clearColor];
        [tLabelBtn addTarget:self
                      action:@selector(tLabel2BtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        [tLabelBtn setTitle:self.brandsArray[i] forState:UIControlStateNormal];
        [scrollviewn addSubview:tLabelBtn];
        if([self.brandsSArray containsObject:self.brandsArray[i]]){
            [tLabelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
            tLabelBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
        }
    }
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, tLabel2.bottom+([self.brandsArray count]/4)*(30.0+CellLeft))];
}

- (void)tLabelBtnAction:(UIButton *)sender {
    if([self.categorySArray containsObject:sender.titleLabel.text]){
        [self.categorySArray removeObject:sender.titleLabel.text];
        [sender setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        sender.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
    } else {
        [self.categorySArray addObject:sender.titleLabel.text];
        [sender setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        sender.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
    }
}

- (void)tLabel1BtnAction:(UIButton *)sender {
    if([self.keywordsSArray containsObject:sender.titleLabel.text]){
        [self.keywordsSArray removeObject:sender.titleLabel.text];
        [sender setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        sender.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
    } else {
        [self.keywordsSArray addObject:sender.titleLabel.text];
        [sender setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        sender.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
    }
}

- (void)tLabel2BtnAction:(UIButton *)sender {
    if([self.brandsSArray containsObject:sender.titleLabel.text]){
        [self.brandsSArray removeObject:sender.titleLabel.text];
        [sender setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        sender.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
    } else {
        [self.brandsSArray addObject:sender.titleLabel.text];
        [sender setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        sender.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
    }
}

- (void)bottomLeftBtnAction:(UIButton *)sender {
    [self.filterBgView removeAllSubviews];
    [self.filterBgView removeFromSuperview];
    self.filterBgView = nil;
    self.brandsSArray = nil;
    self.keywordsArray = nil;
    self.categorySArray = nil;
    [self RBEngineFilterViewSelected:3];
    if ([self.delegate
         respondsToSelector:@selector(RBEngineFilterViewSelected:andIndex:)]) {
        [self.delegate RBEngineFilterViewSelected:self andIndex:self.selectedTag];
    }
}

- (void)bottomRightBtnAction:(UIButton *)sender {
    [self RBEngineFilterViewSelected:3];
    [self.filterBgView removeAllSubviews];
    [self.filterBgView removeFromSuperview];
    self.filterBgView = nil;
    if ([self.delegate
         respondsToSelector:@selector(RBEngineFilterViewSelected:andIndex:)]) {
        [self.delegate RBEngineFilterViewSelected:self andIndex:self.selectedTag];
    }
}

@end
