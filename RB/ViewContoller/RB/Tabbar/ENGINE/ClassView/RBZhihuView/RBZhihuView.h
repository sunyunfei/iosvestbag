//
//  RBZhihuView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBZhihuTableViewCell.h"

@class RBZhihuView;
@protocol RBZhihuViewDelegate <NSObject>
@optional
- (void)RBZhihuViewLoadRefreshViewData:(RBZhihuView *)view;
- (void)RBZhihuViewSelected:(RBZhihuView *)view andIndex:(int)index;

@end


@interface RBZhihuView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, strong) UIView *defaultView;
@property(nonatomic, weak) id<RBZhihuViewDelegate> delegate;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;

- (void)RBZhihuViewLoadRefreshViewFirstData;
- (void)RBZhihuViewSetRefreshViewFinish;

@end