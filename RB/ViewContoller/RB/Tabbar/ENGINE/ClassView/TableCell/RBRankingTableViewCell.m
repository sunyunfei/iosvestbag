//
//  RBRankingTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBRankingTableViewCell.h"
#import "Service.h"

@implementation RBRankingTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
        self.bgView.userInteractionEnabled = YES;
        self.bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        [self.contentView addSubview:self.bgView];
        //
        self.iconIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:self.iconIV];
        //
        self.tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.tLabel.font = font_cu_15;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self.contentView addSubview:self.tLabel];
        //
        self.tLabel1 = [[UILabel alloc]initWithFrame:CGRectZero];
        self.tLabel1.font = font_11;
        self.tLabel1.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [self.contentView addSubview:self.tLabel1];
        //
        self.cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.cLabel.font = font_11;
        self.cLabel.textAlignment = NSTextAlignmentCenter;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [self.contentView addSubview:self.cLabel];
        //
        self.cLabel1 = [[UILabel alloc]initWithFrame:CGRectZero];
        self.cLabel1.font = font_11;
        self.cLabel1.textAlignment = NSTextAlignmentCenter;
        self.cLabel1.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [self.contentView addSubview:self.cLabel1];
        //
        LProgressAppearance*appear = [[LProgressAppearance alloc]init];
        [appear setType:LProgressTypeAnnular];
        appear.backgroundTintColor = [Utils getUIColorWithHexString:@"cbcbcb"];
        appear.progressTintColor = [Utils getUIColorWithHexString:SysColorYellow];
        appear.percentageTextColor = [Utils getUIColorWithHexString:SysColorYellow];
        self.progressView = [[LProgressView alloc]initWithFrame:CGRectMake(ScreenWidth-107.0, 14.0, 37.0, 37.0)];
        [self.progressView setProgressAppearance:appear];
        [self.contentView addSubview:self.progressView];
        //
        self.progressView1 = [[LProgressView alloc]initWithFrame:CGRectMake(ScreenWidth-49.0, 14.0, 37.0, 37.0)];
        [self.progressView1 setProgressAppearance:appear];
        [self.contentView addSubview:self.progressView1];
        //
        self.lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.contentView addSubview:self.lineLabel];
    }
    return self;
}

- (instancetype)loadRBRankingCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    RBRankingWeiboEntity*entity = datalist[indexPath.row];
    self.tLabel.text = [NSString stringWithFormat:@"%@",entity.kol_name];
    if(entity.biz_code!=nil){
        self.tLabel1.text = [NSString stringWithFormat:@"微信号: %@",entity.biz_code];
    } else if(entity.thanks!=nil){
        self.tLabel1.text = [NSString stringWithFormat:@"%@ %@ | %@ %@",NSLocalizedString(@"R30023",@"点赞数"),entity.ups,NSLocalizedString(@"R30024", @"感谢数"),entity.thanks];
    } else if(entity.kol_sum_like_num_30!=nil){
        self.tLabel1.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"R30027",@"最近30天点赞数"),entity.kol_sum_like_num_30];
    } else if(entity.kol_sum_read_num_30!=nil){
        self.tLabel1.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"R30029",@"最近30天观看数"),entity.kol_sum_read_num_30];
    } else {
        self.tLabel1.text = [NSString stringWithFormat:@"%@ %@ | %@ %@",NSLocalizedString(@"R3023",@"粉丝"),entity.fans,NSLocalizedString(@"R1025", @"微博"),entity.posts];
    }
    self.cLabel.text = @"相关度";
    self.cLabel1.text = @"影响力";
    [self.progressView setProgress:[NSString stringWithFormat:@"%@",entity.kol_relative].floatValue];
    [self.progressView1 setProgress:[NSString stringWithFormat:@"%@",entity.kol_influence_score].floatValue/1100.0];
    [self.progressView1 setProgressString:[NSString stringWithFormat:@"%d",entity.kol_influence_score.intValue]];
    [self.iconIV sd_setImageWithURL:[NSURL URLWithString:entity.kol_avatar_url] placeholderImage:PlaceHolderUserImage options:SDWebImageProgressiveDownload];
    //
    self.iconIV.frame = CGRectMake(CellLeft, 16.0, 50.0, 50.0);
    self.iconIV.layer.cornerRadius = self.iconIV.width/2.0;
    self.iconIV.layer.masksToBounds = YES;
    //
    self.tLabel.frame = CGRectMake(self.iconIV.right+self.iconIV.left, 21.0, 150.0, 18.0);
    self.tLabel1.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom+5.0, self.tLabel.width, 13.0);
    self.cLabel.frame = CGRectMake(self.progressView.left, 55.0, 37.0, 13.0);
    self.cLabel1.frame = CGRectMake(self.progressView1.left, self.cLabel.top, self.cLabel.width, self.cLabel.height);
    self.lineLabel.frame = CGRectMake(0, self.iconIV.bottom+self.iconIV.top-0.5, ScreenWidth, 0.5);
    self.bgView.frame = CGRectMake(0, 0, ScreenWidth, self.lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, self.bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
