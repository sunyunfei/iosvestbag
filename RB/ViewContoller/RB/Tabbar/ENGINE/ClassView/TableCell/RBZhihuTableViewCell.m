//
//  RBZhihuTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBZhihuTableViewCell.h"
#import "Service.h"

@implementation RBZhihuTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
        self.bgView.userInteractionEnabled = YES;
        self.bgView.tag = 1000;
        [self.contentView addSubview:self.bgView];
        //
        self.iconIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.iconIV.tag = 1001;
        [self.contentView addSubview:self.iconIV];
        //
        self.tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.tLabel.numberOfLines = 0;
        self.tLabel.font = font_15;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.tLabel.tag = 1002;
        [self.contentView addSubview:self.tLabel];
        //
        self.tLabel1 = [[UILabel alloc]initWithFrame:CGRectZero];
        self.tLabel1.numberOfLines = 0;
        self.tLabel1.font = font_13;
        self.tLabel1.textColor = [Utils getUIColorWithHexString:SysColorGray];
        self.tLabel1.tag = 1003;
        [self.contentView addSubview:self.tLabel1];
        //
        self.cLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        self.cLabel.font = font_11;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.cLabel.tag = 1004;
        [self.contentView addSubview:self.cLabel];
        //
        self.cLabel1 = [[UILabel alloc]initWithFrame:CGRectZero];
        self.cLabel1.font = font_11;
        self.cLabel1.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.cLabel1.textAlignment = NSTextAlignmentRight;
        self.cLabel1.tag = 1005;
        [self.contentView addSubview:self.cLabel1];
        //
        self.lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        self.lineLabel.tag = 1010;
        [self.contentView addSubview:self.lineLabel];
    }
    return self;
}

- (instancetype)loadRBZhihuCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    RBRankingWeiboDetailEntity*entity = datalist[indexPath.row];
    self.tLabel.text = [NSString stringWithFormat:@"%@",entity.title];
    self.tLabel1.text = [NSString stringWithFormat:@"%@",entity.content];
    
    self.cLabel.text = [NSString stringWithFormat:@"%@:%@  %@:%@",NSLocalizedString(@"R30026",@"话题"),entity.board_name,NSLocalizedString(@"R30023",@"点赞数"),entity.ups];
    self.cLabel1.text = [NSString stringWithFormat:@"%@",entity.pub_time];
    
    [self.cLabel setText:self.cLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range_0 = [[mStr string] rangeOfString:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"R30026",@"话题")] options:NSCaseInsensitiveSearch];
         NSRange range0 = NSMakeRange(range_0.location+range_0.length,entity.board_name.length);
         NSRange range_1 = [[mStr string] rangeOfString:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"R30023",@"点赞数")] options:NSCaseInsensitiveSearch];
         NSRange range1 = NSMakeRange(range_1.location+range_1.length,entity.ups.length);
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorBlue] CGColor]} range:range0];
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorBlue] CGColor]} range:range1];
         return mStr;
     }];
    
    //
    self.tLabel.frame = CGRectMake(CellLeft, 20.0, ScreenWidth-CellLeft*2.0, 0);
    CGSize tLabelSize = [Utils getUIFontSizeFitH:self.tLabel];
    self.tLabel.height = tLabelSize.height;
    
    self.tLabel1.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom+8.0, self.tLabel.width, 0);
    CGSize tLabel1Size = [Utils getUIFontSizeFitH:self.tLabel1];
    self.tLabel1.height = tLabel1Size.height;
    
    
    self.cLabel.frame = CGRectMake(CellLeft, self.tLabel1.bottom+15.0, ScreenWidth-2*CellLeft, 13.0);
    self.cLabel1.frame = self.cLabel.frame;

    self.lineLabel.frame = CGRectMake(0, self.cLabel.bottom+20.0-0.5f, ScreenWidth, 0.5);
    self.bgView.frame = CGRectMake(0, 0, ScreenWidth, self.lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, self.bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}


@end
