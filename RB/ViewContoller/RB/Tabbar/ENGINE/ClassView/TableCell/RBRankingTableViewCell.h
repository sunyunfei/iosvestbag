//
//  RBCampaignTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LProgressView.h"
@interface RBRankingTableViewCell : UITableViewCell

@property(nonatomic, strong) UIView*bgView;
//
@property(nonatomic, strong) UIImageView*iconBgIV;
//
@property(nonatomic, strong) UIImageView*iconIV;
//
@property(nonatomic, strong) UILabel*tLabel;
//
@property(nonatomic, strong) UILabel*tLabel1;
//
@property(nonatomic, strong) UILabel*tLabel2;
//
@property(nonatomic, strong) UILabel*tLabel3;
//
@property(nonatomic, strong) UILabel*cLabel;
//
@property(nonatomic, strong) UILabel*cLabel1;
//
@property(nonatomic, strong) LProgressView*progressView;
//
@property(nonatomic, strong) LProgressView*progressView1;
//
@property(nonatomic, strong) UILabel*lineLabel;

- (instancetype)loadRBRankingCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;

@end
