//
//  RBEngineTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBEngineTableViewCell.h"
#import "Service.h"

@implementation RBEngineTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UILabel *tLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1002;
        [self.contentView addSubview:tLabel];
        //
        UILabel *cLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        cLabel.font = font_11;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        cLabel.tag = 1003;
        [self.contentView addSubview:cLabel];
        //
        UILabel*lineLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1020;
        [self.contentView addSubview:lineLabel];
    }
    return self;
}

- (instancetype)loadRBEngineCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1002];
//    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1020];
    //
    RBArticleEntity*entity = datalist[indexPath.row];
    tLabel.text = entity.article_title;
//    cLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"R4005", @"发布时间"),entity.article_publish_at];
    //
    tLabel.frame = CGRectMake(CellLeft, 0, ScreenWidth-CellLeft*2.0, 60.0);
//    cLabel.frame = CGRectMake(tLabel.left, tLabel.bottom+5.0, tLabel.width, 16.0);
    lineLabel.frame = CGRectMake(0, 60.0-0.5, ScreenWidth, 0.5);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
