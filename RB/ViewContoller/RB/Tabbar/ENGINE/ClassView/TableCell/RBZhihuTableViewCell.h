//
//  RBZhihuTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface RBZhihuTableViewCell : UITableViewCell
//
@property(nonatomic, strong) UIView*bgView;
//
@property(nonatomic, strong) UIImageView*iconIV;
//
@property(nonatomic, strong) UILabel*tLabel;
//
@property(nonatomic, strong) UILabel*tLabel1;
//
@property(nonatomic, strong) TTTAttributedLabel*cLabel;
//
@property(nonatomic, strong) UILabel*cLabel1;
//
@property(nonatomic, strong) UILabel*lineLabel;

- (instancetype)loadRBZhihuCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;

@end
