//
//  RBCampaignTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface RBWechatTableViewCell : UITableViewCell

- (instancetype)loadRBWechatCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;

@end
