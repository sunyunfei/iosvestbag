//
//  RBWechatTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBWechatTableViewCell.h"
#import "Service.h"

@implementation RBWechatTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UIImageView *bgIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        bgIV.contentMode = UIViewContentModeScaleAspectFill;
        bgIV.layer.masksToBounds = YES;
        bgIV.tag = 1001;
        [self.contentView addSubview:bgIV];
        //
        UILabel *tLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.numberOfLines = 2;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1002;
        [self.contentView addSubview:tLabel];
        //
        UILabel *cLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        cLabel.font = font_11;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        cLabel.tag = 1003;
        [self.contentView addSubview:cLabel];
        //
        UILabel*lineLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1020;
        [self.contentView addSubview:lineLabel];
    }
    return self;
}

- (instancetype)loadRBWechatCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UIImageView*bgIV=(UIImageView*)[self.contentView viewWithTag:1001];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1002];
    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1020];
    //
    RBRankingWeiboDetailEntity*entity = datalist[indexPath.row];
    tLabel.text = [NSString stringWithFormat:@"%@",entity.title];
    cLabel.text = [NSString stringWithFormat:@"%@",entity.publish_date];
    [bgIV sd_setImageWithURL:[NSURL URLWithString:entity.msg_cdn_url] placeholderImage:PlaceHolderImage options:SDWebImageProgressiveDownload];
    //
    bgIV.frame = CGRectMake(17.5, 10.0, 115.0, 87.0);
    tLabel.frame = CGRectMake(bgIV.right+bgIV.top, bgIV.top, ScreenWidth-(bgIV.right+bgIV.top*2.0), 42.0);
    [Utils getUILabel:tLabel withLineSpacing:4.0];
    cLabel.frame = CGRectMake(tLabel.left, bgIV.bottom-22.0, tLabel.width, 22.0);
    [Utils getUILabel:tLabel withLineSpacing:4.0];
    CGSize tLabelSize = [Utils getUIFontSizeFitH:tLabel withLineSpacing:4.0];
    tLabel.height = tLabelSize.height;
    if (tLabelSize.height > 39) {
        tLabel.height = 40.0;
    }
    //    if ([NSString stringWithFormat:@"%@",entity.article_read].intValue==1) {
    //        tLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
    //    } else {
    //        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    //    }
    CGSize cLabelSize = [Utils getUIFontSizeFitH:cLabel];
    cLabel.height = cLabelSize.height;
    cLabel.top = bgIV.bottom-cLabelSize.height;
    lineLabel.frame = CGRectMake(bgIV.left, bgIV.bottom+bgIV.top-0.5, (ScreenWidth-bgIV.left*2.0) , 0.5);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    if (indexPath.row == [datalist count]-1) {
        bgView.frame = CGRectMake(0, 0, ScreenWidth, bgView.height+15.0);
    }
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}


@end
