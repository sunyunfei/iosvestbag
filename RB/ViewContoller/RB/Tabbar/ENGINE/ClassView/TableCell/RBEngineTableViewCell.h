//
//  RBEngineTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface RBEngineTableViewCell : UITableViewCell
@property(nonatomic, strong) UIButton *refreshBtn;
- (instancetype)loadRBEngineCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
@end
