//
//  RBWeiboTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBWeiboTableViewCell.h"
#import "Service.h"

@implementation RBWeiboTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
        self.bgView.userInteractionEnabled = YES;
        self.bgView.tag = 1000;
        [self.contentView addSubview:self.bgView];
        //
        self.iconIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.iconIV.tag = 1001;
        [self.contentView addSubview:self.iconIV];
        //
        self.tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.tLabel.numberOfLines = 0;
        self.tLabel.font = font_13;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.tLabel.tag = 1002;
        [self.contentView addSubview:self.tLabel];
        //
        self.tLabel1 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        self.tLabel1.font = font_11;
        self.tLabel1.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.tLabel1.tag = 1003;
        [self.contentView addSubview:self.tLabel1];
        //
        self.cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.cLabel.font = font_11;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.cLabel.textAlignment = NSTextAlignmentRight;
        self.cLabel.tag = 1004;
        [self.contentView addSubview:self.cLabel];
        //
        self.lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        self.lineLabel.tag = 1005;
        [self.contentView addSubview:self.lineLabel];
    }
    return self;
}

- (instancetype)loadRBWeiboCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    RBRankingWeiboDetailEntity*entity = datalist[indexPath.row];
    self.tLabel.text = [NSString stringWithFormat:@"%@",entity.summary];
    self.tLabel1.text = [NSString stringWithFormat:@"%@%@  %@%@  %@%@",NSLocalizedString(@"R2017",@"转发"),entity.retweets,NSLocalizedString(@"R3032",@"评论"),entity.comments,NSLocalizedString(@"R3033",@"赞"),entity.likes];
    self.cLabel.text = [NSString stringWithFormat:@"%@",entity.publish_time];
    [self.tLabel1 setText:self.tLabel1.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range_0 = [[mStr string] rangeOfString:NSLocalizedString(@"R2017",@"转发") options:NSCaseInsensitiveSearch];
         NSRange range0 = NSMakeRange(range_0.location+range_0.length,entity.retweets.length);
         NSRange range_1 = [[mStr string] rangeOfString:NSLocalizedString(@"R3032",@"评论") options:NSCaseInsensitiveSearch];
         NSRange range1 = NSMakeRange(range_1.location+range_1.length,entity.comments.length);
         NSRange range_2 = [[mStr string] rangeOfString:NSLocalizedString(@"R3033",@"赞") options:NSCaseInsensitiveSearch];
         NSRange range2 = NSMakeRange(range_2.location+range_2.length,entity.likes.length);
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorBlue] CGColor]} range:range0];
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorBlue] CGColor]} range:range1];
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorBlue] CGColor]} range:range2];
         return mStr;
     }];
    
    //
    self.tLabel.frame = CGRectMake(CellLeft, 20.0, ScreenWidth-CellLeft*2.0, 100);
    CGSize tLabelSize = [Utils getUIFontSizeFitH:self.tLabel];
    self.tLabel.height = tLabelSize.height;
    self.tLabel1.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom+8.0, self.tLabel.width, 13);
    self.cLabel.frame = CGRectMake(0, self.tLabel1.top, ScreenWidth-CellLeft, self.tLabel1.height);
    self.lineLabel.frame = CGRectMake(0, self.cLabel.bottom+20.0-0.5f, ScreenWidth, 0.5);
    self.bgView.frame = CGRectMake(0, 0, ScreenWidth, self.lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, self.bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}


@end
