//
//  RBCampaignTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface RBWeiboTableViewCell : UITableViewCell
//
@property(nonatomic, strong) UIView*bgView;
//
@property(nonatomic, strong) UIImageView*iconIV;
//
@property(nonatomic, strong) UILabel*tLabel;
//
@property(nonatomic, strong) TTTAttributedLabel*tLabel1;
//
@property(nonatomic, strong) UILabel*cLabel;
//
@property(nonatomic, strong) UILabel*lineLabel;

- (instancetype)loadRBWeiboCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;

@end
