//
//  RBEngineViewController.h
//  RB
//
//  Created by AngusNi on 7/11/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
//
#import "RBEngineView.h"
//
#import "RBEngineSearchViewController.h"
#import "RBEngineDetailViewController.h"
//
#import "RBUserContactRankingViewController.h"
@interface RBEngineViewController : RBBaseViewController
<RBBaseVCDelegate,RBEngineViewDelegate>
@property(nonatomic, strong) RBEngineView *engineView;
@end
