//
//  RBEngineViewController.m
//  RB
//
//  Created by AngusNi on 7/11/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBEngineViewController.h"

@interface RBEngineViewController () {
}
@end

@implementation RBEngineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    //
    self.engineView = [[RBEngineView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    self.engineView.delegate = self;
    [self.view addSubview:self.engineView];
    //
    self.navTitle = NSLocalizedString(@"R7000",@"搜索引擎");
    self.navLeftLabel.textColor = [UIColor whiteColor];
    self.navTitleLabel.hidden = YES;
    self.navView.backgroundColor = [UIColor clearColor];
    self.navLineLabel.backgroundColor = [UIColor clearColor];
    //
    [self.hudView show];
    [self.engineView RBEngineViewLoadRefreshViewFirstData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:@"engine-list"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"engine-list"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - RBEngineView Delegate
-(void)RBEngineViewLoadRefreshViewData:(RBEngineView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBRankingListWithPageIndex:view.pageIndex andPageSize:view.pageSize];
}

- (void)RBEngineViewSelected:(RBEngineView *)view andIndex:(int)index {
    RBArticleEntity*entity = view.datalist[index];
    TOWebViewController*toview = [[TOWebViewController alloc]init];
    toview.url = [NSURL URLWithString:entity.article_url];
    toview.title = entity.article_title;
    toview.showPageTitles = NO;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)RBEngineViewSearchButtonAction:(RBEngineView *)view {
    RBEngineSearchViewController*toview = [[RBEngineSearchViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"hot_items"]) {
        [self.hudView dismiss];
        if (self.engineView.pageIndex == 0) {
            self.engineView.datalist = [NSMutableArray new];
        }
        self.engineView.datalist = [JsonService getRBEngineListEntity:jsonObject andBackArray:self.engineView.datalist];
        [self.engineView RBEngineViewSetRefreshViewFinish];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self.engineView RBEngineViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.engineView RBEngineViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
