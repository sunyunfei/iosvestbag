//
//  RBEngineDetailViewController.m
//  RB
//
//  Created by AngusNi on 5/10/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//


#import "RBEngineDetailViewController.h"

@interface RBEngineDetailViewController () {
    UIScrollView *scrollviewn;
    RBRankingDetailSwitchView *switchView;
    RBWeiboView*weiboView;
    RBWechatView*wechatView;
    RBZhihuView*zhihuView;
    RBMeipaiView*meipaiView;
    UIScrollView*head_scrollview;
    NSMutableArray*categoriesList;
}
@end

@implementation RBEngineDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = self.entity.kol_name;
    self.navRightTitle = Icon_bar_invite;
    //
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    if (self.socialEntity!=nil) {
        self.entity = [[RBRankingWeiboEntity alloc]init];
        self.navTitle = self.socialEntity.username;
        if ([self.socialEntity.provider isEqualToString:@"public_wechat"]) {
            [handler getRBRankingWechatSearchWithBiz_code:self.socialEntity.uid];
        } else if ([self.socialEntity.provider isEqualToString:@"zhihu"])  {
            [handler getRBRankingZhihuSearchWithTerm:[self.socialEntity.username componentsSeparatedByString:@" "][0] andOrder:nil andCategory:nil andBrands:nil andKeywords:nil  andPageIndex:0 andPageSize:10];
        } else if ([self.socialEntity.provider isEqualToString:@"meipai"])  {
            [handler getRBRankingMeipaiSearchWithTerm:[self.socialEntity.username componentsSeparatedByString:@" "][0] andOrder:nil andCategory:nil andBrands:nil andKeywords:nil  andPageIndex:0 andPageSize:10];
        } else if ([self.socialEntity.provider isEqualToString:@"miaopai"])  {
            [handler getRBRankingMiaopaiSearchWithTerm:[self.socialEntity.username componentsSeparatedByString:@" "][0] andOrder:nil andCategory:nil andBrands:nil andKeywords:nil  andPageIndex:0 andPageSize:10];
        } else if ([self.socialEntity.provider isEqualToString:@"weibo"]) {
            [handler getRBRankingWeiboDetailWithId:self.socialEntity.uid];
        }
    } else {
        self.navTitle = self.entity.kol_name;
        [self loadHeadView];
        [self loadContentView];
        if (self.entity.biz_code!=nil) {
            [handler getRBRankingWechatTagsWithId:self.entity.kol_id];
            [handler getRBRankingWechatCategoriesWithId:self.entity.kol_id];
        } else if (self.entity.thanks!=nil)  {
            [handler getRBRankingZhihuTagsWithId:self.entity.kol_id];
            [handler getRBRankingZhihuCategoriesWithId:self.entity.kol_id];
        } else if (self.entity.kol_sum_like_num_30!=nil)  {
            [handler getRBRankingMeipaiTagsWithId:self.entity.kol_id];
            [handler getRBRankingMeipaiCategoriesWithId:self.entity.kol_id];
        } else if (self.entity.kol_sum_read_num_30!=nil)  {
            [handler getRBRankingMiaopaiTagsWithId:self.entity.kol_id];
            [handler getRBRankingMiaopaiCategoriesWithId:self.entity.kol_id];
        } else {
            [handler getRBRankingWeiboTagsWithId:self.entity.kol_id];
            [handler getRBRankingWeiboCategoriesWithId:self.entity.kol_id];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //
    [TalkingData trackPageBegin:@"engine-detail"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //
    [TalkingData trackPageEnd:@"engine-detail"];
}

#pragma mark - loadNavView Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)RBNavRightBtnAction {
    RBUserContactKolViewController*toview = [[RBUserContactKolViewController alloc] init];
    toview.kolName = self.entity.kol_name;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - loadNavView Delegate
- (void)loadHeadView {
    head_scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 150.0)];
    head_scrollview.bounces = NO;
    head_scrollview.pagingEnabled = YES;
    head_scrollview.scrollEnabled = YES;
    head_scrollview.showsHorizontalScrollIndicator = NO;
    head_scrollview.showsVerticalScrollIndicator = NO;
    head_scrollview.delegate = self;
    [head_scrollview setContentSize:CGSizeMake(head_scrollview.width, head_scrollview.height)];
    [self.view addSubview:head_scrollview];
    //
    UIView*headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, head_scrollview.width, head_scrollview.height)];
    headView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:1];
    headView.userInteractionEnabled = YES;
    [head_scrollview addSubview:headView];
    //
    UIImageView*headIV = [[UIImageView alloc] initWithFrame:CGRectMake((head_scrollview.width-65.0)/2.0, 15.0, 65.0, 65.0)];
    [headIV sd_setImageWithURL:[NSURL URLWithString:self.entity.kol_avatar_url] placeholderImage:PlaceHolderUserImage options:SDWebImageProgressiveDownload];
    headIV.layer.cornerRadius = headIV.width/2.0;
    headIV.layer.masksToBounds = YES;
    [head_scrollview addSubview:headIV];
    //
    UILabel*headTLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, headIV.bottom+15.0, head_scrollview.width, 13.0)];
    headTLabel.font = font_13;
    headTLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
    if (self.entity.biz_code!=nil) {
        headTLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"R2086", @"微信号"),self.entity.biz_code];
    } else if (self.entity.thanks!=nil) {
        headTLabel.text = [NSString stringWithFormat:@"%@ %@ | %@ %@",NSLocalizedString(@"R30023",@"点赞数"),self.entity.ups,NSLocalizedString(@"R30024", @"感谢数"),self.entity.thanks];
    } else if (self.entity.kol_sum_like_num_30!=nil) {
        headTLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"R30027",@"最近30天点赞数"),self.entity.kol_sum_like_num_30];
    } else if (self.entity.kol_sum_read_num_30!=nil) {
        headTLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"R30029",@"最近30天观看数"),self.entity.kol_sum_read_num_30];
    } else {
        headTLabel.text = [NSString stringWithFormat:@"%@ %@ | %@ %@",NSLocalizedString(@"R3023",@"粉丝"),self.entity.fans,NSLocalizedString(@"R1025", @"微博"),self.entity.posts];
    }
    headTLabel.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:headTLabel];
    //
    UILabel*headCLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, headTLabel.bottom+15.0, head_scrollview.width-20.0*2.0, 40.0)];
    headCLabel.font = font_11;
    headCLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    headCLabel.numberOfLines = 2;
    headCLabel.textAlignment = NSTextAlignmentCenter;
    headCLabel.text = [NSString stringWithFormat:@"%@",self.entity.kol_info];
    [headView addSubview:headCLabel];
    CGSize headCLabelSize = [Utils getUIFontSizeFitH:headCLabel];
    if(headCLabelSize.height<40.0){
        headCLabel.height = headCLabelSize.height;
    }
    //
    UILabel*headGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, headCLabel.bottom+15.0, head_scrollview.width, 8.0)];
    headGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [headView addSubview:headGapLabel];
    if(self.entity.kol_info.length==0){
        headGapLabel.top = headTLabel.bottom+15.0;
    }
    //
    UILabel*headGapLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, headGapLabel.height-0.5f, headGapLabel.width, 0.5f)];
    headGapLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [headGapLabel addSubview:headGapLineLabel];
    //
    head_scrollview.height = headGapLabel.bottom;
}

#pragma mark - loadContentView Delegate
- (void)loadContentView {
    switchView = [[RBRankingDetailSwitchView alloc]initWithFrame:CGRectMake(0, head_scrollview.bottom, ScreenWidth, 38.0)];
    switchView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    if (self.entity.biz_code!=nil) {
        [switchView RBRankingDetailSwitchViewShow:@[NSLocalizedString(@"R4000", @"图谱"),NSLocalizedString(@"R4001", @"标签"),NSLocalizedString(@"R4002", @"文章")] andSelected:0];
    } else if (self.entity.thanks!=nil){
        [switchView RBRankingDetailSwitchViewShow:@[NSLocalizedString(@"R4000", @"图谱"),NSLocalizedString(@"R4001", @"标签"),NSLocalizedString(@"R30025", @"回答")] andSelected:0];
    } else if (self.entity.kol_sum_like_num_30!=nil){
        [switchView RBRankingDetailSwitchViewShow:@[NSLocalizedString(@"R4000", @"图谱"),NSLocalizedString(@"R4001", @"标签"),NSLocalizedString(@"R30028", @"视频")] andSelected:0];
    } else if (self.entity.kol_sum_read_num_30!=nil){
        [switchView RBRankingDetailSwitchViewShow:@[NSLocalizedString(@"R4000", @"图谱"),NSLocalizedString(@"R4001", @"标签"),NSLocalizedString(@"R30028", @"视频")] andSelected:0];
    } else {
        [switchView RBRankingDetailSwitchViewShow:@[NSLocalizedString(@"R4000", @"图谱"),NSLocalizedString(@"R4001", @"标签"),NSLocalizedString(@"R1025", @"微博")] andSelected:0];
    }
    switchView.delegate = self;
    [self.view addSubview:switchView];
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, switchView.bottom, ScreenWidth, ScreenHeight-switchView.bottom)];
    scrollviewn.bounces = NO;
    scrollviewn.pagingEnabled = YES;
    scrollviewn.scrollEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.delegate = self;
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width*[switchView.dataArray count], scrollviewn.height)];
    [self.view addSubview:scrollviewn];
    //
    if (self.entity.biz_code!=nil) {
        wechatView = [[RBWechatView alloc]initWithFrame:CGRectMake(scrollviewn.width*2.0, 0, scrollviewn.width, scrollviewn.height)];
        wechatView.delegate = self;
        [scrollviewn addSubview:wechatView];
        [wechatView RBWechatViewLoadRefreshViewFirstData];
    } else if (self.entity.thanks!=nil) {
        zhihuView = [[RBZhihuView alloc]initWithFrame:CGRectMake(scrollviewn.width*2.0, 0, scrollviewn.width, scrollviewn.height)];
        zhihuView.delegate = self;
        [scrollviewn addSubview:zhihuView];
        [zhihuView RBZhihuViewLoadRefreshViewFirstData];
    } else if (self.entity.kol_sum_like_num_30!=nil) {
        meipaiView = [[RBMeipaiView alloc]initWithFrame:CGRectMake(scrollviewn.width*2.0, 0, scrollviewn.width, scrollviewn.height)];
        meipaiView.delegate = self;
        [scrollviewn addSubview:meipaiView];
        [meipaiView RBMeipaiViewLoadRefreshViewFirstData];
    } else if (self.entity.kol_sum_read_num_30!=nil) {
        meipaiView = [[RBMeipaiView alloc]initWithFrame:CGRectMake(scrollviewn.width*2.0, 0, scrollviewn.width, scrollviewn.height)];
        meipaiView.delegate = self;
        [scrollviewn addSubview:meipaiView];
        [meipaiView RBMeipaiViewLoadRefreshViewFirstData];
    } else {
        weiboView = [[RBWeiboView alloc]initWithFrame:CGRectMake(scrollviewn.width*2.0, 0, scrollviewn.width, scrollviewn.height)];
        weiboView.delegate = self;
        [scrollviewn addSubview:weiboView];
        [weiboView RBWeiboViewLoadRefreshViewFirstData];
    }
}

- (void)loadEditView:(NSMutableArray*)list {
    MCCircleChartView*circleChartView = [[MCCircleChartView alloc] initWithFrame:CGRectMake(30.0, (scrollviewn.height-(ScreenWidth-60.0))/2.0, ScreenWidth-60.0, ScreenWidth-60.0)];
    circleChartView.introduceColor = [Utils getUIColorWithHexString:SysColorGray];
    circleChartView.maxRadius = 120;
    circleChartView.circleWidth = 20;
    circleChartView.dataSource = self;
    circleChartView.delegate = self;
    [scrollviewn addSubview:circleChartView];
    [circleChartView reloadDataWithAnimate:YES];
}

#pragma mark - MCCircleChartView Delegate
- (NSInteger)numberOfCircleInCircleChartView:(MCCircleChartView *)circleChartView {
    return categoriesList.count;
}

- (id)circleChartView:(MCCircleChartView *)circleChartView valueOfCircleAtIndex:(NSInteger)index {
    RBRankingTagsEntity*entity = categoriesList[index];
    return [NSString stringWithFormat:@"%f",[[NSString stringWithFormat:@"%@",entity.weight]floatValue]*100.0];
}

- (NSString *)circleChartView:(MCCircleChartView *)circleChartView introduceAtIndex:(NSInteger)index {
    RBRankingTagsEntity*entity = categoriesList[index];
    return [NSString stringWithFormat:@"%@:%@", entity.text,[NSString stringWithFormat:@"%.1f%%",[[NSString stringWithFormat:@"%@",entity.weight]floatValue]*100.0]];
}

- (UIColor *)circleChartView:(MCCircleChartView *)circleChartView colorOfCircleAtIndex:(NSInteger)index {
    NSArray*array = @[@"BCE784",@"5DD39E",@"348AA7",@"525174",@"513B56",@"BCE784",@"5DD39E",@"348AA7",@"525174",@"513B56"];
    return [Utils getUIColorWithHexString:array[index]];
}

#pragma mark - RBMeipaiView Delegate
-(void)RBMeipaiViewLoadRefreshViewData:(RBMeipaiView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    if (self.entity.kol_sum_like_num_30!=nil) {
        [handler getRBRankingMeipaiListWithKolId:self.entity.kol_id andPageIndex:view.pageIndex andPageSize:view.pageSize];
    }
    if (self.entity.kol_sum_read_num_30!=nil) {
        [handler getRBRankingMiaopaiListWithKolId:self.entity.kol_id andPageIndex:view.pageIndex andPageSize:view.pageSize];
    }
}

- (void)RBMeipaiViewSelected:(RBMeipaiView *)view andIndex:(int)index {
    RBRankingWeiboDetailEntity*entity = view.datalist[index];
    TOWebViewController*toview = [[TOWebViewController alloc]init];
    toview.url = [NSURL URLWithString:entity.url];
    toview.title = self.entity.kol_name;
    toview.showPageTitles = NO;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - RBZhihuView Delegate
-(void)RBZhihuViewLoadRefreshViewData:(RBZhihuView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBRankingZhihuListWithKolId:self.entity.kol_id andPageIndex:view.pageIndex andPageSize:view.pageSize];
}

- (void)RBZhihuViewSelected:(RBZhihuView *)view andIndex:(int)index {
    RBRankingWeiboDetailEntity*entity = view.datalist[index];
    entity.url = [[NSString stringWithFormat:@"%@",entity.url]stringByReplacingOccurrencesOfString:@"www" withString:@"m"];
    if ([[[NSString stringWithFormat:@"%@",entity.url]componentsSeparatedByString:@"answer"] count]>1) {
        entity.url = [[NSString stringWithFormat:@"%@",entity.url]componentsSeparatedByString:@"answer"][0];
    }
    TOWebViewController*toview = [[TOWebViewController alloc]init];
    toview.url = [NSURL URLWithString:entity.url];
    toview.title = self.entity.kol_name;
    toview.showPageTitles = NO;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - RBWechatView Delegate
-(void)RBWechatViewLoadRefreshViewData:(RBWechatView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBRankingWechatListWithKolId:self.entity.kol_id andPageIndex:view.pageIndex andPageSize:view.pageSize];
}

- (void)RBWechatViewSelected:(RBWechatView *)view andIndex:(int)index {
    RBRankingWeiboDetailEntity*entity = view.datalist[index];
    TOWebViewController*toview = [[TOWebViewController alloc]init];
    toview.url = [NSURL URLWithString:entity.url];
    toview.title = self.entity.kol_name;
    toview.showPageTitles = NO;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - RBWeiboView Delegate
-(void)RBWeiboViewLoadRefreshViewData:(RBWeiboView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBRankingWeiboListWithKolId:self.entity.kol_id andPageIndex:view.pageIndex andPageSize:view.pageSize];
}

- (void)RBWeiboViewSelected:(RBWeiboView *)view andIndex:(int)index {
    RBRankingWeiboDetailEntity*entity = view.datalist[index];
    TOWebViewController*toview = [[TOWebViewController alloc]init];
    toview.url = [NSURL URLWithString:entity.url];
    toview.title = self.entity.kol_name;
    toview.showPageTitles = NO;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark- UIScrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView==scrollviewn) {
        CGPoint offset = scrollView.contentOffset;
        int currentPage = offset.x / (self.view.bounds.size.width);
        [switchView RBRankingDetailSwitchViewShow:switchView.dataArray andSelected:currentPage];
        [self RBRankingDetailSwitchViewSelected:switchView andIndex:currentPage];
    }
}

#pragma mark- RBCampaignSwitchView Delegate
- (void)RBRankingDetailSwitchViewSelected:(RBRankingDetailSwitchView *)view andIndex:(int)index {
    [scrollviewn scrollRectToVisible:CGRectMake(scrollviewn.width*index, 0, scrollviewn.width, scrollviewn.height) animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"search/ranking/weibo/categories"]||[sender isEqualToString:@"search/ranking/wechat/categories"]||[sender isEqualToString:@"search/ranking/meipai/categories"]||[sender isEqualToString:@"search/ranking/miaopai/categories"]) {
        categoriesList = [NSMutableArray new];
        categoriesList = [JsonService getRBRankingCategoriesEntity:jsonObject andBackArray:categoriesList];
        [self loadEditView:categoriesList];
    }
    
    if ([sender isEqualToString:@"search/ranking/zhihu/boards"]) {
        categoriesList = [NSMutableArray new];
        categoriesList = [JsonService getRBRankingBoardsEntity:jsonObject andBackArray:categoriesList];
        [self loadEditView:categoriesList];
    }
    
    
    if ([sender isEqualToString:@"search/ranking/weibo/tags"]||[sender isEqualToString:@"search/ranking/wechat/tags"]||[sender isEqualToString:@"search/ranking/zhihu/tags"]||[sender isEqualToString:@"search/ranking/meipai/tags"]||[sender isEqualToString:@"search/ranking/miaopai/tags"]) {
        NSMutableArray*tagsList = [NSMutableArray new];
        tagsList = [JsonService getRBRankingTagsEntity:jsonObject andBackArray:tagsList];
        DBSphereView *sphereView = [[DBSphereView alloc] initWithFrame:CGRectMake(scrollviewn.width+40.0, (scrollviewn.height-(scrollviewn.width-80.0))/2.0, scrollviewn.width-80.0, scrollviewn.width-80.0)];
        NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
        for (NSInteger i = 0; i < [tagsList count]; i ++) {
            UILabel*tagsLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 22)];
            tagsLabel.text = tagsList[i];
            tagsLabel.font = font_30;
            tagsLabel.textAlignment = NSTextAlignmentCenter;
            tagsLabel.textColor = [Utils getUIColorRandom];
            [array addObject:tagsLabel];
            CGSize tagsLabelSize = [Utils getUIFontSizeFitW:tagsLabel];
            tagsLabel.width = tagsLabelSize.width;
            [sphereView addSubview:tagsLabel];
        }
        [sphereView setCloudTags:array];
        [scrollviewn addSubview:sphereView];
    }
    
    if ([sender isEqualToString:@"search/ranking/weibo/posts"]) {
        [self.hudView dismiss];
        if (weiboView.pageIndex == 0) {
            weiboView.datalist = [NSMutableArray new];
        }
        weiboView.datalist = [JsonService getRBRankingWeiboListEntity:jsonObject andBackArray:weiboView.datalist];
        [weiboView RBWeiboViewSetRefreshViewFinish];
    }
    
    if ([sender isEqualToString:@"search/ranking/wechat/posts"]) {
        [self.hudView dismiss];
        if (wechatView.pageIndex == 0) {
            wechatView.datalist = [NSMutableArray new];
        }
        wechatView.datalist = [JsonService getRBRankingWechatListEntity:jsonObject andBackArray:wechatView.datalist];
        [wechatView RBWechatViewSetRefreshViewFinish];
    }
    
    if ([sender isEqualToString:@"search/ranking/zhihu/posts"]) {
        [self.hudView dismiss];
        if (zhihuView.pageIndex == 0) {
            zhihuView.datalist = [NSMutableArray new];
        }
        zhihuView.datalist = [JsonService getRBRankingZhihuListEntity:jsonObject andBackArray:zhihuView.datalist];
        [zhihuView RBZhihuViewSetRefreshViewFinish];
    }
    
    if ([sender isEqualToString:@"search/ranking/meipai/posts"]) {
        [self.hudView dismiss];
        if (meipaiView.pageIndex == 0) {
            meipaiView.datalist = [NSMutableArray new];
        }
        meipaiView.datalist = [JsonService getRBRankingMeipaiListEntity:jsonObject andBackArray:meipaiView.datalist];
        [meipaiView RBMeipaiViewSetRefreshViewFinish];
    }
    
    if ([sender isEqualToString:@"search/ranking/miaopai/posts"]) {
        [self.hudView dismiss];
        if (meipaiView.pageIndex == 0) {
            meipaiView.datalist = [NSMutableArray new];
        }
        meipaiView.datalist = [JsonService getRBRankingMiaopaiListEntity:jsonObject andBackArray:meipaiView.datalist];
        [meipaiView RBMeipaiViewSetRefreshViewFinish];
    }
    
    if ([sender isEqualToString:@"search/ranking/weibo/infos"]) {
        [self.hudView dismiss];
        self.entity = [JsonService getRBRankingDetailWeiboEntity:jsonObject];
        [self loadHeadView];
        [self loadContentView];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBRankingWeiboTagsWithId:self.entity.kol_id];
        [handler getRBRankingWeiboCategoriesWithId:self.entity.kol_id];
    }
    
    if ([sender isEqualToString:@"search/ranking/wechat"]) {
        NSMutableArray*templist = [NSMutableArray new];
        templist = [JsonService getRBRankingListWechatEntity:jsonObject andBackArray:templist];
        if([templist count]>0){
            self.entity = templist[0];
            [self.hudView show];
            [self loadHeadView];
            [self loadContentView];
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBRankingWechatTagsWithId:self.entity.kol_id];
            [handler getRBRankingWechatCategoriesWithId:self.entity.kol_id];
        } else {
            [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2025", @"无更多搜索结果")];
        }
    }
    if ([sender isEqualToString:@"search/ranking/zhihu"]) {
        NSMutableArray*templist = [NSMutableArray new];
        templist = [JsonService getRBRankingListZhihuEntity:jsonObject andBackArray:templist];
        if([templist count]>0){
            self.entity = templist[0];
            [self.hudView show];
            [self loadHeadView];
            [self loadContentView];
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBRankingZhihuTagsWithId:self.entity.kol_id];
            [handler getRBRankingZhihuCategoriesWithId:self.entity.kol_id];
        } else {
            [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2025", @"无更多搜索结果")];
        }
    }
    if ([sender isEqualToString:@"search/ranking/meipai"]) {
        NSMutableArray*templist = [NSMutableArray new];
        templist = [JsonService getRBRankingListMeipaiEntity:jsonObject andBackArray:templist];
        if([templist count]>0){
            self.entity = templist[0];
            [self.hudView show];
            [self loadHeadView];
            [self loadContentView];
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBRankingMeipaiTagsWithId:self.entity.kol_id];
            [handler getRBRankingMeipaiCategoriesWithId:self.entity.kol_id];
        } else {
            [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2025", @"无更多搜索结果")];
        }
    }
    if ([sender isEqualToString:@"search/ranking/miaopai"]) {
        NSMutableArray*templist = [NSMutableArray new];
        templist = [JsonService getRBRankingListMiaopaiEntity:jsonObject andBackArray:templist];
        if([templist count]>0){
            self.entity = templist[0];
            [self.hudView show];
            [self loadHeadView];
            [self loadContentView];
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBRankingMiaopaiTagsWithId:self.entity.kol_id];
            [handler getRBRankingMiaopaiCategoriesWithId:self.entity.kol_id];
        } else {
            [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2025", @"无更多搜索结果")];
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [wechatView RBWechatViewSetRefreshViewFinish];
    [weiboView RBWeiboViewSetRefreshViewFinish];
    [zhihuView RBZhihuViewSetRefreshViewFinish];
    [meipaiView RBMeipaiViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"message"]]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [wechatView RBWechatViewSetRefreshViewFinish];
    [weiboView RBWeiboViewSetRefreshViewFinish];
    [zhihuView RBZhihuViewSetRefreshViewFinish];
    [meipaiView RBMeipaiViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end


