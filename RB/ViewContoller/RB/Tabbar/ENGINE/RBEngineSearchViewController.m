//
//  RBEngineSearchViewController.m
//  RB
//
//  Created by AngusNi on 6/30/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBEngineSearchViewController.h"

@interface RBEngineSearchViewController (){
    RBEngineFilterView *filterView;
    RBRankingView*rankingView;
    RBEngineSearchView*searchView;
    RBEngineSearchHistoryView*historyView;
    NSString*searchStr;
}
@end

@implementation RBEngineSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    [self loadNavView];
    [self showSearchHistory];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"engine-search"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"engine-search"];
}

#pragma mark - UIButton Delegate
- (void)loadNavView {
    searchView = [[RBEngineSearchView alloc]initWithFrame:CGRectZero];
    searchView.delegate = self;
    [self.view addSubview:searchView];
    //
    historyView = [[RBEngineSearchHistoryView alloc]initWithFrame:CGRectMake(searchView.left, searchView.bottom, searchView.width, ScreenHeight - (searchView.bottom))];
    historyView.delegate = self;
    [self.view addSubview:historyView];
    //
    filterView = [[RBEngineFilterView alloc]initWithFrame:CGRectMake(0, searchView.bottom, ScreenWidth, 40.0)];
    filterView.delegate = self;
    filterView.backgroundColor = [Utils getUIColorWithHexString:@"f8f8fa"];
    [self.view addSubview:filterView];
    [filterView RBEngineFilterViewSelected:0];
    //
    rankingView = [[RBRankingView alloc]initWithFrame:CGRectMake(0, filterView.bottom, ScreenWidth, ScreenHeight - filterView.bottom)];
    rankingView.delegate = self;
    [self.view addSubview:rankingView];
    rankingView.tableviewn.mj_header=nil;
    //
    UIScreenEdgePanGestureRecognizer *edgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(edgePanGesture:)];
    edgePanGestureRecognizer.delegate = self;
    edgePanGestureRecognizer.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:edgePanGestureRecognizer];
}

-(void)showSearchHistory {
    historyView.hidden = NO;
    rankingView.hidden = YES;
    filterView.hidden = YES;
    historyView.datalist = [NSMutableArray arrayWithArray:[[LocalService getRBLocalDataUserEngineSearchList] componentsSeparatedByString:@"*^*"]];
    [historyView.tableviewn reloadData];
    [searchView.searchTF becomeFirstResponder];
}

- (BOOL)becomeFirstResponder{
    return YES;
}

#pragma mark- UIScreenEdgePanGestureRecognizer Delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (void)edgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender {
    [self.view removeGestureRecognizer:sender];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - RBEngineSearchView Delegate
-(void)RBEngineFilterViewSelected:(RBEngineFilterView *)view andIndex:(int)index {
    [self.hudView show];
    [rankingView RBRankingViewLoadRefreshViewFirstData];
}

#pragma mark - RBEngineSearchView Delegate
-(void)RBEngineSearchHistoryViewSelected:(RBEngineSearchHistoryView *)view andIndex:(int)index {
    searchStr = view.datalist[index];
    if([[searchStr componentsSeparatedByString:[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R1025",@"微博")]]count]>1) {
        searchView.typeLabel.text = NSLocalizedString(@"R3001",@"新浪微博");
    }
    if([[searchStr componentsSeparatedByString:[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R1023",@"微信")]]count]>1) {
        searchView.typeLabel.text = NSLocalizedString(@"R3002",@"微信公众号");
    }
    if([[searchStr componentsSeparatedByString:[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R30020",@"知乎")]]count]>1) {
        searchView.typeLabel.text = NSLocalizedString(@"R30020",@"知乎");
    }
    if([[searchStr componentsSeparatedByString:[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R30021",@"美拍")]]count]>1) {
        searchView.typeLabel.text = NSLocalizedString(@"R30021",@"美拍");
    }
    if([[searchStr componentsSeparatedByString:[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R30022",@"秒拍")]]count]>1) {
        searchView.typeLabel.text = NSLocalizedString(@"R30022",@"秒拍");
    }
    NSString*str = [searchStr stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@] ",NSLocalizedString(@"R1023",@"微信")] withString:@""];
    str = [str stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@] ",NSLocalizedString(@"R1025",@"微博")] withString:@""];
    str = [str stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@] ",NSLocalizedString(@"R30020",@"知乎")] withString:@""];
    str = [str stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@] ",NSLocalizedString(@"R30021",@"美拍")] withString:@""];
    str = [str stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@] ",NSLocalizedString(@"R30022",@"秒拍")] withString:@""];
    searchView.searchTF.text = str;
    [self.hudView show];
    [rankingView RBRankingViewLoadRefreshViewFirstData];
}

#pragma mark - RBEngineSearchView Delegate
-(void)RBEngineSearchViewSearchCancel:(RBEngineSearchView *)view {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)RBEngineSearchViewSearchClear:(RBEngineSearchView *)view {
    [self showSearchHistory];
}

-(void)RBEngineSearchViewSearch:(RBEngineSearchView *)view andText:(NSString *)text {
    [self.view endEditing:YES];
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (text.length==0) {
        return;
    }
    if ([text isEqualToString:searchStr]) {
        return;
    }
    if([view.typeLabel.text isEqualToString:NSLocalizedString(@"R3002",@"微信公众号")]) {
        text = [NSString stringWithFormat:@"%@ %@",[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R1023",@"微信")],text];
    }
    if([view.typeLabel.text isEqualToString:NSLocalizedString(@"R3001",@"新浪微博")]) {
        text = [NSString stringWithFormat:@"%@ %@",[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R1025",@"微博")],text];
    }
    if([view.typeLabel.text isEqualToString:NSLocalizedString(@"R30020",@"知乎")]) {
        text = [NSString stringWithFormat:@"%@ %@",[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R30020",@"知乎")],text];
    }
    if([view.typeLabel.text isEqualToString:NSLocalizedString(@"R30021",@"美拍")]) {
        text = [NSString stringWithFormat:@"%@ %@",[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R30021",@"美拍")],text];
    }
    if([view.typeLabel.text isEqualToString:NSLocalizedString(@"R30022",@"秒拍")]) {
        text = [NSString stringWithFormat:@"%@ %@",[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R30022",@"秒拍")],text];
    }
    searchStr = text;
    NSMutableArray*tempList = [NSMutableArray arrayWithArray:[[LocalService getRBLocalDataUserEngineSearchList] componentsSeparatedByString:@"*^*"]];
    if (![tempList containsObject:searchStr]) {
        if ([tempList count]>=5) {
            [tempList removeLastObject];
        }
        [tempList insertObject:searchStr atIndex:0];
    }
    NSString*temp = @"";
    for (int i =0; i <[tempList count]; i++) {
        if([NSString stringWithFormat:@"%@",tempList[i]].length!=0){
            if (i==0) {
                temp = tempList[i];
            } else {
                temp = [NSString stringWithFormat:@"%@*^*%@",temp,tempList[i]];
            }
        }
    }
    if([NSString stringWithFormat:@"%@",temp].length!=0) {
        [LocalService setRBLocalDataUserEngineSearchListWith:temp];
        [self.hudView show];
        [rankingView RBRankingViewLoadRefreshViewFirstData];
    }
}

#pragma mark - RBAlertView Delegate
-(void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if(alertView.tag==10006) {
        if (buttonIndex==0) {
            RBUserContactRankingViewController*toview = [[RBUserContactRankingViewController alloc]init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }
    }
}

#pragma mark - RBRankingView Delegate
-(void)RBRankingViewLoadRefreshViewData:(RBRankingView *)view {
    if(view.pageIndex>1) {
        view.pageIndex = 1;
        [view RBRankingViewSetRefreshViewFinish];
        RBAlertView *alertView = [[RBAlertView alloc]init];
        alertView.tag = 10006;
        alertView.delegate = self;
        [alertView showWithArray:@[NSLocalizedString(@"R4004", @"更多结果请联系Robin8客服"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
        return;
    }
    //
    NSString*str = [searchStr stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@] ",NSLocalizedString(@"R1023",@"微信")] withString:@""];
    str = [str stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@] ",NSLocalizedString(@"R1025",@"微博")] withString:@""];
    str = [str stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@] ",NSLocalizedString(@"R30020",@"知乎")] withString:@""];
    str = [str stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@] ",NSLocalizedString(@"R30021",@"美拍")] withString:@""];
    str = [str stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@] ",NSLocalizedString(@"R30022",@"秒拍")] withString:@""];
    //
    NSArray*orderArray = @[@"overall",@"relative",@"influence",@"overall"];
    NSString*order = orderArray[filterView.selectedTag];
    //
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    if([[searchStr componentsSeparatedByString:[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R1025",@"微博")]]count]>1) {
        [handler getRBRankingWeiboSearchWithTerm:str andOrder:order andCategory:filterView.categorySArray andBrands:filterView.brandsSArray andKeywords:filterView.keywordsSArray andRelative:nil andPageIndex:view.pageIndex andPageSize:view.pageSize];
    } else if([[searchStr componentsSeparatedByString:[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R30020",@"知乎")]]count]>1) {
        [handler getRBRankingZhihuSearchWithTerm:str andOrder:order andCategory:filterView.categorySArray andBrands:filterView.brandsSArray andKeywords:filterView.keywordsSArray  andPageIndex:view.pageIndex andPageSize:view.pageSize];
    } else if([[searchStr componentsSeparatedByString:[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R30021",@"美拍")]]count]>1) {
        [handler getRBRankingMeipaiSearchWithTerm:str andOrder:order andCategory:filterView.categorySArray andBrands:filterView.brandsSArray andKeywords:filterView.keywordsSArray  andPageIndex:view.pageIndex andPageSize:view.pageSize];
    } else if([[searchStr componentsSeparatedByString:[NSString stringWithFormat:@"[%@]",NSLocalizedString(@"R30022",@"秒拍")]]count]>1) {
        [handler getRBRankingMiaopaiSearchWithTerm:str andOrder:order andCategory:filterView.categorySArray andBrands:filterView.brandsSArray andKeywords:filterView.keywordsSArray  andPageIndex:view.pageIndex andPageSize:view.pageSize];
    } else {
        [handler getRBRankingWechatSearchWithTerm:str andOrder:order andCategory:filterView.categorySArray andBrands:filterView.brandsSArray andKeywords:filterView.keywordsSArray  andPageIndex:view.pageIndex andPageSize:view.pageSize];
    }
}

- (void)RBRankingViewSelected:(RBRankingView *)view andIndex:(int)index {
    RBRankingWeiboEntity*temEntity = view.datalist[index];
    RBEngineDetailViewController*toview = [[RBEngineDetailViewController alloc] init];
    toview.entity = temEntity;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"search/ranking/weibo"]) {
        [self.hudView dismiss];
        if (rankingView.pageIndex == 0) {
            rankingView.datalist = [NSMutableArray new];
        }
        rankingView.datalist = [JsonService getRBRankingListWeiboEntity:jsonObject andBackArray:rankingView.datalist];
        NSMutableArray*keywordArray = [NSMutableArray new];
        NSMutableArray*brandArray = [NSMutableArray new];
        for (RBRankingWeiboEntity*entity in rankingView.datalist) {
            if (![entity.kol_keywords isEqual:[NSNull null]]) {
                for (NSString*key in entity.kol_keywords) {
                    if([keywordArray count]<20&&![keywordArray containsObject:key]) {
                        [keywordArray addObject:key];
                    }
                }
            }
            if (![entity.kol_brands isEqual:[NSNull null]]) {
                for (NSString*brand in entity.kol_brands) {
                    if([brandArray count]<20&&![brandArray containsObject:brand]) {
                        [brandArray addObject:brand];
                    }
                }
            }
        }
        filterView.brandsArray = brandArray;
        filterView.keywordsArray = keywordArray;
        
        NSString*total_count = [JsonService checkJson:[jsonObject valueForKey:@"total"]];
        if (rankingView.pageIndex == 0) {
            rankingView.defaultView.hidden = YES;
            if (total_count.intValue==0) {
                rankingView.defaultView.hidden = NO;
            }
            [searchView.searchTF resignFirstResponder];
            historyView.hidden = YES;
            rankingView.hidden = NO;
            filterView.hidden = NO;
            [rankingView RBRankingViewSetRefreshViewFinish];
            [rankingView.tableviewn scrollRectToVisible:CGRectMake( 0, 0, rankingView.tableviewn.width, rankingView.tableviewn.height) animated:YES];
        } else {
            if (total_count.intValue==0) {
                [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2025", @"无更多搜索结果")];
            }
            [rankingView RBRankingViewSetRefreshViewFinish];
        }
        if(total_count.intValue < 10) {
            rankingView.tableviewn.mj_footer = nil;
        }
    }
    if ([sender isEqualToString:@"search/ranking/wechat"]) {
        [self.hudView dismiss];
        if (rankingView.pageIndex == 0) {
            rankingView.datalist = [NSMutableArray new];
        }
        rankingView.datalist = [JsonService getRBRankingListWechatEntity:jsonObject andBackArray:rankingView.datalist];
        NSMutableArray*keywordArray = [NSMutableArray new];
        NSMutableArray*brandArray = [NSMutableArray new];
        for (RBRankingWeiboEntity*entity in rankingView.datalist) {
            if (![entity.kol_keywords isEqual:[NSNull null]]) {
                for (NSString*key in entity.kol_keywords) {
                    if([keywordArray count]<20&&![keywordArray containsObject:key]) {
                        [keywordArray addObject:key];
                    }
                }
            }
            if (![entity.kol_brands isEqual:[NSNull null]]) {
                for (NSString*brand in entity.kol_brands) {
                    if([brandArray count]<20&&![brandArray containsObject:brand]) {
                        [brandArray addObject:brand];
                    }
                }
            }
        }
        filterView.brandsArray = brandArray;
        filterView.keywordsArray = keywordArray;
        NSString*total_count = [JsonService checkJson:[jsonObject valueForKey:@"total"]];
        if (rankingView.pageIndex == 0) {
            rankingView.defaultView.hidden = YES;
            if (total_count.intValue==0) {
                rankingView.defaultView.hidden = NO;
            }
            [searchView.searchTF resignFirstResponder];
            historyView.hidden = YES;
            rankingView.hidden = NO;
            filterView.hidden = NO;
            [rankingView RBRankingViewSetRefreshViewFinish];
            [rankingView.tableviewn scrollRectToVisible:CGRectMake( 0, 0, rankingView.tableviewn.width, rankingView.tableviewn.height) animated:YES];
        } else {
            if (total_count.intValue==0) {
                [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2025", @"无更多搜索结果")];
            }
            [rankingView RBRankingViewSetRefreshViewFinish];
        }
        if(total_count.intValue < 10) {
            rankingView.tableviewn.mj_footer = nil;
        }
    }
    
    if ([sender isEqualToString:@"search/ranking/zhihu"]) {
        [self.hudView dismiss];
        if (rankingView.pageIndex == 0) {
            rankingView.datalist = [NSMutableArray new];
        }
        rankingView.datalist = [JsonService getRBRankingListZhihuEntity:jsonObject andBackArray:rankingView.datalist];
        NSMutableArray*keywordArray = [NSMutableArray new];
        NSMutableArray*brandArray = [NSMutableArray new];
        for (RBRankingWeiboEntity*entity in rankingView.datalist) {
            if (![entity.kol_keywords isEqual:[NSNull null]]) {
                for (NSString*key in entity.kol_keywords) {
                    if([keywordArray count]<20&&![keywordArray containsObject:key]) {
                        [keywordArray addObject:key];
                    }
                }
            }
            if (![entity.kol_brands isEqual:[NSNull null]]) {
                for (NSString*brand in entity.kol_brands) {
                    if([brandArray count]<20&&![brandArray containsObject:brand]) {
                        [brandArray addObject:brand];
                    }
                }
            }
        }
        filterView.brandsArray = brandArray;
        filterView.keywordsArray = keywordArray;
        NSString*total_count = [JsonService checkJson:[jsonObject valueForKey:@"total"]];
        if (rankingView.pageIndex == 0) {
            rankingView.defaultView.hidden = YES;
            if (total_count.intValue==0) {
                rankingView.defaultView.hidden = NO;
            }
            [searchView.searchTF resignFirstResponder];
            historyView.hidden = YES;
            rankingView.hidden = NO;
            filterView.hidden = NO;
            [rankingView RBRankingViewSetRefreshViewFinish];
            [rankingView.tableviewn scrollRectToVisible:CGRectMake( 0, 0, rankingView.tableviewn.width, rankingView.tableviewn.height) animated:YES];
        } else {
            if (total_count.intValue==0) {
                [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2025", @"无更多搜索结果")];
            }
            [rankingView RBRankingViewSetRefreshViewFinish];
        }
        if(total_count.intValue < 10) {
            rankingView.tableviewn.mj_footer = nil;
        }
    }
    
    if ([sender isEqualToString:@"search/ranking/meipai"]) {
        [self.hudView dismiss];
        if (rankingView.pageIndex == 0) {
            rankingView.datalist = [NSMutableArray new];
        }
        rankingView.datalist = [JsonService getRBRankingListMeipaiEntity:jsonObject andBackArray:rankingView.datalist];
        NSMutableArray*keywordArray = [NSMutableArray new];
        NSMutableArray*brandArray = [NSMutableArray new];
        for (RBRankingWeiboEntity*entity in rankingView.datalist) {
            if (![entity.kol_keywords isEqual:[NSNull null]]) {
                for (NSString*key in entity.kol_keywords) {
                    if([keywordArray count]<20&&![keywordArray containsObject:key]) {
                        [keywordArray addObject:key];
                    }
                }
            }
            if (![entity.kol_brands isEqual:[NSNull null]]) {
                for (NSString*brand in entity.kol_brands) {
                    if([brandArray count]<20&&![brandArray containsObject:brand]) {
                        [brandArray addObject:brand];
                    }
                }
            }
        }
        filterView.brandsArray = brandArray;
        filterView.keywordsArray = keywordArray;
        NSString*total_count = [JsonService checkJson:[jsonObject valueForKey:@"total"]];
        if (rankingView.pageIndex == 0) {
            rankingView.defaultView.hidden = YES;
            if (total_count.intValue==0) {
                rankingView.defaultView.hidden = NO;
            }
            [searchView.searchTF resignFirstResponder];
            historyView.hidden = YES;
            rankingView.hidden = NO;
            filterView.hidden = NO;
            [rankingView RBRankingViewSetRefreshViewFinish];
            [rankingView.tableviewn scrollRectToVisible:CGRectMake( 0, 0, rankingView.tableviewn.width, rankingView.tableviewn.height) animated:YES];
        } else {
            if (total_count.intValue==0) {
                [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2025", @"无更多搜索结果")];
            }
            [rankingView RBRankingViewSetRefreshViewFinish];
        }
        if(total_count.intValue < 10) {
            rankingView.tableviewn.mj_footer = nil;
        }
    }
    
    if ([sender isEqualToString:@"search/ranking/miaopai"]) {
        [self.hudView dismiss];
        if (rankingView.pageIndex == 0) {
            rankingView.datalist = [NSMutableArray new];
        }
        rankingView.datalist = [JsonService getRBRankingListMiaopaiEntity:jsonObject andBackArray:rankingView.datalist];
        NSMutableArray*keywordArray = [NSMutableArray new];
        NSMutableArray*brandArray = [NSMutableArray new];
        for (RBRankingWeiboEntity*entity in rankingView.datalist) {
            if (![entity.kol_keywords isEqual:[NSNull null]]) {
                for (NSString*key in entity.kol_keywords) {
                    if([keywordArray count]<20&&![keywordArray containsObject:key]) {
                        [keywordArray addObject:key];
                    }
                }
            }
            if (![entity.kol_brands isEqual:[NSNull null]]) {
                for (NSString*brand in entity.kol_brands) {
                    if([brandArray count]<20&&![brandArray containsObject:brand]) {
                        [brandArray addObject:brand];
                    }
                }
            }
        }
        filterView.brandsArray = brandArray;
        filterView.keywordsArray = keywordArray;
        NSString*total_count = [JsonService checkJson:[jsonObject valueForKey:@"total"]];
        if (rankingView.pageIndex == 0) {
            rankingView.defaultView.hidden = YES;
            if (total_count.intValue==0) {
                rankingView.defaultView.hidden = NO;
            }
            [searchView.searchTF resignFirstResponder];
            historyView.hidden = YES;
            rankingView.hidden = NO;
            filterView.hidden = NO;
            [rankingView RBRankingViewSetRefreshViewFinish];
            [rankingView.tableviewn scrollRectToVisible:CGRectMake( 0, 0, rankingView.tableviewn.width, rankingView.tableviewn.height) animated:YES];
        } else {
            if (total_count.intValue==0) {
                [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2025", @"无更多搜索结果")];
            }
            [rankingView RBRankingViewSetRefreshViewFinish];
        }
        if(total_count.intValue < 10) {
            rankingView.tableviewn.mj_footer = nil;
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [rankingView RBRankingViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [rankingView RBRankingViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
