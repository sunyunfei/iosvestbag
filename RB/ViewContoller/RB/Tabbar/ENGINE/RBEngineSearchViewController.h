//
//  RBEngineSearchViewController.h
//  RB
//
//  Created by AngusNi on 6/30/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBEngineSearchView.h"
#import "RBEngineSearchHistoryView.h"
#import "RBRankingView.h"
#import "RBEngineDetailViewController.h"
#import "RBEngineFilterView.h"
#import "RBUserContactRankingViewController.h"
@interface RBEngineSearchViewController : RBBaseViewController
<RBEngineSearchViewDelegate,RBEngineSearchHistoryViewDelegate,RBRankingViewDelegate,RBEngineFilterViewDelegate>
@end
