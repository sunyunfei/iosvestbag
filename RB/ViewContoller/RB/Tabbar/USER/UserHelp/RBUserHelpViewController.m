//
//  RBUserHelpViewController.m
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBUserHelpViewController.h"

@interface RBUserHelpViewController (){
    UITableView*tableviewn;
    NSMutableArray*datalist;
    int pageIndex;
    int pageSize;
    BOOL close[30];
}
@end

@implementation RBUserHelpViewController
-(void)PressBtn:(id)sender{
    UIView *view = (UIView*)[self.view viewWithTag:1009];
    UITextField * textField = (UITextField*)[view viewWithTag:1010];
    if (textField.text.length>0) {
        [LocalService setRBDevelopIPURL:textField.text];
    }
    [view removeFromSuperview];
}

-(void)ClickStraight{
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight - NavHeight)];
    view.backgroundColor = [UIColor whiteColor];
    view.tag = 1009;
    //
    UITextField * textField = [[UITextField alloc]initWithFrame:CGRectMake(50, 100, ScreenWidth - 100, 50)];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.backgroundColor = [UIColor whiteColor];
    textField.placeholder = @"请输入域名";
    textField.clearButtonMode = UITextFieldViewModeAlways;
    textField.tag = 1010;
    [view addSubview: textField];
    //
    UIButton * OKBtn = [[UIButton alloc]initWithFrame:CGRectMake((ScreenWidth - 100)/2, textField.bottom + 40, 100, 40) title:@"确定" hlTitle:nil titleColor:[UIColor whiteColor] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorBlue] image:nil hlImage:nil];
    [OKBtn addTarget:self action:@selector(PressBtn:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:OKBtn];
    [self.view addSubview:view];
    NSLog(@"haha");
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R7036",@"帮助中心");
    //
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake((ScreenWidth - 80)/2, [[UIApplication sharedApplication] statusBarFrame].size.height, 80, NavHeight - [[UIApplication sharedApplication] statusBarFrame].size.height)];
    view.backgroundColor = [UIColor clearColor];
    [self.navView addSubview:view];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ClickStraight)];
    tap.numberOfTouchesRequired = 1;
    tap.numberOfTapsRequired = 8;
    view.userInteractionEnabled = YES;
    [view addGestureRecognizer:tap];
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height-NavHeight)
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg andAlpha:1];
    tableviewn.estimatedRowHeight = 0;
    tableviewn.estimatedSectionFooterHeight = 0;
    tableviewn.estimatedSectionHeaderHeight = 0;
    [self.view addSubview:tableviewn];
    [self loadHeaderView];
    //
    for (int i=0; i<30; i++) {
        close[i] = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    //
    [self.hudView show];
    [self loadRefreshViewFirstData];
    //
    [TalkingData trackPageBegin:@"my-help"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [TalkingData trackPageEnd:@"my-help"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationDismissView {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)serviceBtnAction:(UIButton*)sender {
    self.navigationController.navigationBarHidden = NO;
    // 在线客服
    RBUserSettingChatViewController *toview = [[RBUserSettingChatViewController alloc] init];
    toview.conversationType = ConversationType_APPSERVICE;
    toview.targetId = RCIMServiceID;
    toview.title = NSLocalizedString(@"R5107", @"在线客服");
    toview.hidesBottomBarWhenPushed = YES;
    toview.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)feedbackBtnAction:(UIButton*)sender {
    RBUserSettingFeedBackViewController *toview = [[RBUserSettingFeedBackViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self presentVC:toview];
}

- (void)loadHeaderView {
    UIView*headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 122.5)];
    headerView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [tableviewn setTableHeaderView:headerView];
    //
    RBKolEditView*serviceView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, CellBottom, ScreenWidth, 50.0)];
    serviceView.tLabel.text = [NSString stringWithFormat:@"%@(%@)",NSLocalizedString(@"R5107", @"在线客服"),NSLocalizedString(@"R5214",@"工作日10:00-17:00")];
    serviceView.cLabel.text = @"";
    [serviceView addTarget:self
                 action:@selector(serviceBtnAction:)
       forControlEvents:UIControlEventTouchUpInside];
    serviceView.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:serviceView];
    //
    RBKolEditView*feedbackView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, serviceView.bottom+CellBottom, ScreenWidth, 50.0)];
    feedbackView.tLabel.text = NSLocalizedString(@"R5103", @"意见反馈");
    feedbackView.cLabel.text = @"";
    [feedbackView addTarget:self
                    action:@selector(feedbackBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];
    feedbackView.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:feedbackView];
}


#pragma mark - LoadTableHeadFootView Delegate
- (void)loadRefreshViewFirstData {
    pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    pageSize = 20;
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserHelpListWithPage:pageIndex+1];
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageIndex = pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshViewFinish {
    [tableviewn.mj_header endRefreshing];
    [tableviewn.mj_footer endRefreshing];
    if ([datalist count] == (pageIndex+1)*pageSize) {
        if (tableviewn.mj_footer == nil) {
            [self setRefreshFooterView];
        }
    } else {
        [tableviewn.mj_footer removeFromSuperview];
        tableviewn.mj_footer = nil;
    }
    [UIView performWithoutAnimation:^{
        [tableviewn reloadData];
    }];
    if ([datalist count]==0) {
        self.defaultView.hidden = NO;
    } else {
        self.defaultView.hidden = YES;
    }
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [datalist count];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if (close[section]) {
        return 0;
    }
    return 1;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIControl *view = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 50)];
    view.tag = 1000 + section;
    view.backgroundColor = [UIColor whiteColor];
    [view addTarget:self action:@selector(sectionClick:) forControlEvents:UIControlEventTouchUpInside];
    //
    RBQAEntity*entity = datalist[section];
    UILabel *tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth-CellLeft, 50)];
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tLabel.font = font_15;
    tLabel.text = [NSString stringWithFormat:@"%d. %@",(int)section+1,entity.question];
    [view addSubview:tLabel];
    //
    UILabel*arrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-CellLeft-8.0, (tLabel.height-13.0)/2.0, 13.0, 13.0)];
    arrowLabel.font = font_icon_(13.0);
    arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    arrowLabel.text = Icon_btn_right;
    [view addSubview:arrowLabel];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 49.5, ScreenWidth, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [view addSubview:lineLabel];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBUserHelpTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBUserHelpTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:cellIdentifier];
    }
    [cell loadRBUserHelpCellWith:datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

-(void)sectionClick:(UIControl *)view {
    NSInteger i = view.tag - 1000;
    close[i] = !close[i];
    NSIndexSet * index = [NSIndexSet indexSetWithIndex:i];
    [tableviewn reloadSections:index withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"account_notice"]) {
        [self.hudView dismiss];
        if (pageIndex==0) {
            datalist=[NSMutableArray new];
        }
        datalist = [JsonService getRBQAListEntity:jsonObject andBackArray:datalist];
        [self setRefreshViewFinish];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
