//
//  RBUserHelpTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBUserHelpTableViewCell.h"
#import "Service.h"

@implementation RBUserHelpTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_13;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.numberOfLines = 0;
        tLabel.tag = 1001;
        [self.contentView addSubview:tLabel];
    }
    return self;
}

- (instancetype)loadRBUserHelpCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1001];
    //
    RBQAEntity*entity = datalist[indexPath.section];
    tLabel.text = [NSString stringWithFormat:@"%@",entity.answer];
    tLabel.frame = CGRectMake(CellLeft, 0, ScreenWidth-CellLeft*2.0, 0);
    CGSize tLabelSize = [Utils getUIFontSizeFitH:tLabel];
    tLabel.height = tLabelSize.height+10.0;
    
    bgView.frame = CGRectMake(0, 0, ScreenWidth, tLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    return self;
}

@end
