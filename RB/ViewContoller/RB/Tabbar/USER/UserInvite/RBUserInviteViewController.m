//
//  RBUserInviteViewController.m
//  RB
//
//  Created by AngusNi on 5/19/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserInviteViewController.h"
#import "UserInviteCell.h"
#import "UserPhoneInviteCell.h"
#import "UserInviteView.h"
#import "RegularHelp.h"
#import "RBTudiHeadView.h"
#import "RBEnlighteningCell.h"
#import "RBTudiFootView.h"
#import "RBPhoneInviteViewController.h"
@interface RBUserInviteViewController ()<UserPhoneInviteCellDelegate,RBTudiFootViewDelegate,RBEnlighteningCellDelegate,RBShareViewDelegate> {
    TTTAttributedLabel*leftTLabel;
    TTTAttributedLabel*rightTLabel;
    //记录中间cell拉长时的高度
    CGFloat cellHeight;
    //记录奖励规则是否是展开状态
    BOOL IsFold;
    //邀请好友数
    NSString * invite_count;
    //已获得奖励
    NSString * invite_amount;
    //邀请码
    NSString * invite_code;
    //
    NSString * is_show_newbie;
    //
    NSString * desc;
    //
    NSString * invite_desc;
    //
    UITableView * MyTable;
    //
    NSMutableArray*array;
    NSMutableArray*array1;
    NSMutableArray*array2;
    NSMutableArray*array3;
    NSMutableArray*array4;
    int uploadCount;
    //
    NSArray * imageArr;
    NSArray * titleArr;
    NSArray * btnArr;
    //
    NSMutableArray * dataSource;
    //
    NSIndexPath * cellIndex;
    //
    BOOL isPermit;
    //记录footerView中奖励规则label的高度
    CGFloat height;
}
@end

@implementation RBUserInviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    invite_count = @"";
    invite_amount = @"";
    IsFold = NO;
    isPermit = YES;
    UILabel *ttContentLabel = [[UILabel alloc] init];
    ttContentLabel.numberOfLines = 0;
    ttContentLabel.font = font_13;
    ttContentLabel.text = NSLocalizedString(@"R5098", @"1.点击立即邀请,分享“Robin8-APP”下载页\n2.邀请好友下载体验“Robin8-APP”\n3.好友下载进入“Robin8-APP”并完成登录\n4.好友完成一个活动\n5.成功后奖励会进入您的钱包");
    ttContentLabel.textColor = [UIColor blackColor];
    //
    [Utils getUILabel:ttContentLabel withLineSpacing:20.0];
    CGSize ttContentLabelSize = [Utils getUIFontSizeFitH:ttContentLabel withLineSpacing:20.0];
    //
    UILabel *aboutLabel = [[UILabel alloc] init];
    aboutLabel.numberOfLines = 0;
    aboutLabel.textAlignment = NSTextAlignmentCenter;
    aboutLabel.font = font_11;
    aboutLabel.text = NSLocalizedString(@"R5099", @"注意：只有朋友在 Robin8 进行一个活动并且上传截图后才可以获得奖励，每天最多获得 10 次奖励，成功后奖励直接进入您的钱包");
    aboutLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [Utils getUILabel:aboutLabel withLineSpacing:4.0];
    CGSize aboutLabelSize = [Utils getUIFontSizeFitH:aboutLabel withLineSpacing:4.0];
    cellHeight = ttContentLabelSize.height + aboutLabelSize.height + 18.5+CellLeft+20 + 13 + 5;
    
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5243", @"收徒赚收益");
    self.bgIV.hidden = NO;
    
    dataSource = [[NSMutableArray alloc]init];
    //
    imageArr = @[@"RBShareInvite",@"RBPhoneInvite",@"RBInviteCode1"];
    titleArr = @[@"分享邀请收徒",@"通讯录邀请收徒",@"邀请码收徒"];
    btnArr = @[@"去分享",@"去邀请",@"复制"];
    //
    [self loadEditView];
    [self loadFooterView];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-invite"];
    //RB-获取邀请好友信息
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserRewardInviteCode];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-invite"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)shareSuccess{
    [self.hudView showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
}
- (void)inviteAction{
    RBShareView *shareView = [RBShareView sharedRBShareView];
    Handler * handler = [Handler shareHandler];
    shareView.delegate = self;
    [shareView showViewWithTitle:@"我最近发现一款每天都可以赚钱的APP,您也来试试?" Content:@"Robin8基于大数据的社交影响力平台" URL:[NSString stringWithFormat:@"%@invite?inviter_id=%@",handler.postUrl,[LocalService getRBLocalDataUserLoginId]] ImgURL:@"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png"];
    
}
// 获取本机通讯录信息
- (void)getAddressBookInfo {
    CFErrorRef *error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    __block BOOL accessGranted = NO;
    if (&ABAddressBookRequestAccessWithCompletion != NULL) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    if (accessGranted == NO) {
        //        RBAlertView *alertView = [[RBAlertView alloc]init];
        //        alertView.delegate = self;
        //        alertView.tag = 1000;
        //        [alertView showWithArray:@[NSLocalizedString(@"R5044", @"是否允许ROBIN8\n获取您的手机通讯录？"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
        isPermit = NO;
        [MyTable reloadData];
        return;
    } else {
        array = [NSMutableArray new];
        array1 = [NSMutableArray new];
        array2 = [NSMutableArray new];
        array3 = [NSMutableArray new];
        array4 = [NSMutableArray new];
        NSArray*listContacts = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
        for (int i=0; i<[listContacts count]; i++) {
            ABRecordRef thisPerson = CFBridgingRetain(listContacts[i]);
            NSString *firstName = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonFirstNameProperty));
            NSString *lastName = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonLastNameProperty));
            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(thisPerson, kABPersonPhoneProperty);
            NSArray* phoneNumberArray = CFBridgingRelease(ABMultiValueCopyArrayOfAllValues(phoneNumberProperty));
            for(int index = 0; index<[phoneNumberArray count]; index++) {
                NSMutableDictionary*diclist = [NSMutableDictionary new];
                NSString*fName = @"";
                NSString*sName = @"";
                if (firstName!=nil&&![firstName isEqual:[NSNull null]]&&firstName.length!=0) {
                    fName = firstName;
                }
                if (lastName!=nil&&![lastName isEqual:[NSNull null]]&&lastName.length!=0) {
                    sName = lastName;
                }
                NSString*mobile = [NSString stringWithFormat:@"%@",phoneNumberArray[index]];
                mobile = [mobile stringByReplacingOccurrencesOfString:@" (" withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@") " withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"+86" withString:@""];
                if (mobile.length==11 && [Utils getIsValidateNumber:mobile]) {
                    [diclist setObject:[NSString stringWithFormat:@"%@%@",sName,fName] forKey:@"name"];
                    [diclist setObject:[NSString stringWithFormat:@"%@",mobile] forKey:@"mobile"];
                    if([array count]<2000) {
                        [array addObject:diclist];
                    } else {
                        if ([array1 count]<2000) {
                            [array1 addObject:diclist];
                        } else {
                            if ([array2 count]<2000) {
                                [array2 addObject:diclist];
                            } else {
                                if ([array3 count]<2000) {
                                    [array3 addObject:diclist];
                                } else {
                                    [array4 addObject:diclist];
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if ([array count]!=0) {
            isPermit = YES;
            uploadCount = (int)[array count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array];
        }
    }
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {

}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    //
    MyTable = [[UITableView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight) style:UITableViewStyleGrouped];
    MyTable.delegate =self;
    MyTable.dataSource = self;
    MyTable.showsVerticalScrollIndicator = NO;
    MyTable.showsHorizontalScrollIndicator = NO;
    MyTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:MyTable];
   //
//    [MyTable registerClass:[UserInviteCell class] forCellReuseIdentifier:@"UserInviteCell"];
    [MyTable registerNib:[UINib nibWithNibName:@"RBEnlighteningCell" bundle:nil] forCellReuseIdentifier:@"RBEnlighteningCell"];
    [MyTable registerNib:[UINib nibWithNibName:@"RBTudiHeadView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"RBTudiHeadView"];
    [MyTable registerNib:[UINib nibWithNibName:@"RBTudiFootView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"RBTudiFootView"];
}
#pragma mark- cellDelegate
-(void)UserPhoneCellBtnClick:(UITableViewCell *)cell{
    cellIndex = [MyTable indexPathForCell:cell];
    RBMobileEntity * entity = [dataSource objectAtIndex:cellIndex.row];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBInfluenceSmsWithMobile:entity.mobile];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    if (section == 0) {
//        return 1;
//    }else{
//        if (isPermit == NO) {
//            return 0;
//        }else{
//            return dataSource.count;
//        }
//    }
    return 3;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * iden = @"RBEnlighteningCell";
    RBEnlighteningCell * cell = [tableView dequeueReusableCellWithIdentifier:iden forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    cell.titleImageView.image = [UIImage imageNamed:imageArr[indexPath.row]];
    cell.describeLabel.text = titleArr[indexPath.row];
    if (indexPath.row == 2) {
        cell.describeLabel.text = [NSString stringWithFormat:@"邀请码收徒(%@)",invite_code];
    }
    cell.inviteBtn.layer.cornerRadius = 12.5;
    cell.inviteBtn.layer.masksToBounds = YES;
    [cell.inviteBtn setTitle:btnArr[indexPath.row] forState:UIControlStateNormal];
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    static NSString * iden = @"RBTudiHeadView";
    RBTudiHeadView * view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:iden];
    NSString * amount = [NSString stringWithFormat:@"%@ 元",invite_amount];
    NSMutableAttributedString * str = [[NSMutableAttributedString alloc]initWithString:amount];
    NSRange range1 = [[str string]rangeOfString:@"元"];
    [str addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorYellow] range:NSMakeRange(0, str.length)];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:27 weight:UIFontWeightSemibold] range:NSMakeRange(0, str.length - 1)];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:range1];
    [view.moneyLabel setAttributedText:str];
    //
    NSString * count = [NSString stringWithFormat:@"%@ 人",invite_count];
    NSMutableAttributedString * str1 = [[NSMutableAttributedString alloc]initWithString:count];
    [str1 addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorYellow] range:NSMakeRange(0, str1.length)];
    [str1 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:27 weight:UIFontWeightSemibold] range:NSMakeRange(0, str1.length - 1)];
    [str1 addAttribute:NSFontAttributeName value:font_(14) range:NSMakeRange(str1.length - 1, 1)];
    [view.numLabel setAttributedText:str1];
    //
//    NSMutableAttributedString * str2 = [[NSMutableAttributedString alloc]initWithString:@"   每日前 10 位徒弟有 2 元额外奖励"];
//    [str2 addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:@"777777"] range:NSMakeRange(0, str2.length)];
//    NSRange range2 = [[str2 string]rangeOfString:@"10"];
//    NSRange range3 = [[str2 string]rangeOfString:@"2"];
//    [str2 addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorYellow] range:range2];
//    [str2 addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorYellow] range:range3];
//    [str2 addAttribute:NSFontAttributeName value:font_cu_cu_(15) range:range2];
//    [str2 addAttribute:NSFontAttributeName value:font_cu_cu_(15) range:range3];
//    [view.bottomLabel setAttributedText:str2];
    view.bottomLabel.text = [NSString stringWithFormat:@"   %@",invite_desc];
    return view;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    static NSString * iden = @"RBTudiFootView";
    RBTudiFootView * view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:iden];
    view.delegate = self;
    view.descLabel.text = desc;
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 63;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return 141;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (IsFold == YES) {
        return 46;
    }
    return 46 + 106 + height + 10;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}
#pragma GestureAction
- (void)longPressView:(UIGestureRecognizer*)press{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = invite_code;
    [self.hudView showSuccessWithStatus:@"邀请码已复制到剪切板"];
}
- (void)foldtheRules{
    IsFold = ! IsFold;
    [MyTable reloadData];
}
#pragma RBEnlighteningCellDelegate
- (void)ClickBtnAction:(UITableViewCell *)cell{
    NSIndexPath * indexpath =  [MyTable indexPathForCell:cell];
    if (indexpath.row == 0) {
        [self inviteAction];
    }else if (indexpath.row == 1){
        RBPhoneInviteViewController * inviteVC = [[RBPhoneInviteViewController alloc]init];
        [self.navigationController pushViewController:inviteVC animated:YES];
    }else if (indexpath.row == 2){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = invite_code;
        [self.hudView showSuccessWithStatus:@"邀请码已复制到剪切板"];
    }
}
#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"invite_info"]) {
        invite_count = [JsonService checkJson:[jsonObject objectForKey:@"invite_count"]];
        invite_amount = [JsonService checkJson:[jsonObject objectForKey:@"invite_amount"]];
        self.allAmount = [JsonService checkJson:[jsonObject objectForKey:@"invite_amount"]];
        invite_code = [JsonService checkJson:[jsonObject objectForKey:@"invite_code"]];
        is_show_newbie = [JsonService checkJson:[jsonObject objectForKey:@"is_show_newbie"]];
        desc = [JsonService checkJson:[jsonObject objectForKey:@"desc"]];
        invite_desc = [JsonService checkJson:[jsonObject objectForKey:@"invite_desc"]];
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, ScreenWidth - 2*16, 0) text:desc font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentLeft textColor:nil backgroundColor:nil numberOfLines:0];
        label.numberOfLines = 0;
        [label sizeToFit];
        height = label.frame.size.height;
        [MyTable reloadData];
    }
    if ([sender isEqualToString:@"kol_contacts"]) {
        //        Handler*handler = [Handler shareHandler];
        //        handler.delegate = self;
        //        [handler getRBInfluenceSmsWithMobile:@"18817774982"];
        
       
        if(uploadCount==2000&&[array1 count]!=0) {
            uploadCount = uploadCount+(int)[array1 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array1];
            return;
        }
        if(uploadCount==4000&&[array2 count]!=0) {
            uploadCount = uploadCount+(int)[array2 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array2];
            return;
        }
        if(uploadCount==6000&&[array3 count]!=0) {
            uploadCount = uploadCount+(int)[array3 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array3];
            return;
        }
        if(uploadCount==8000&&[array4 count]!=0) {
            uploadCount = uploadCount+(int)[array4 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array4];
            return;
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dataSource = [JsonService getRBMobile:jsonObject AndBackArray:dataSource AndPhoneArray:array];
            dispatch_async(dispatch_get_main_queue(), ^{
                [MyTable reloadData];
            });
        });
        
    }
    if ([sender isEqualToString:@"influences/send_invite"]) {
        RBMobileEntity * entity = [dataSource objectAtIndex:cellIndex.row];
        entity.status = @"already_invited";
        [dataSource replaceObjectAtIndex:cellIndex.row withObject:entity];
        [MyTable reloadData];
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"" message:@"已发送邀请" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        if ([[jsonObject objectForKey:@"error"]isEqual:@1]&&[sender isEqualToString:@"influences/send_invite"]) {
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"" message:@"邀请失败" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
