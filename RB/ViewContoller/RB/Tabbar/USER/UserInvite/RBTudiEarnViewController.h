//
//  RBTudiEarnViewController.h
//  RB
//
//  Created by RB8 on 2018/3/7.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBTudiEarnViewController : RBBaseViewController
@property(nonatomic,copy) NSString * earnMoney;
@end
