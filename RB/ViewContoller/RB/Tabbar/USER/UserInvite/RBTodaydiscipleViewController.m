//
//  RBTodaydiscipleViewController.m
//  RB
//
//  Created by RB8 on 2018/3/7.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBTodaydiscipleViewController.h"
#import "RBTodayShouTuCell.h"
#import "RBTudiEntity.h"
#import "RBTestWaitView.h"
@interface RBTodaydiscipleViewController ()<RBBaseVCDelegate,UITableViewDelegate,UITableViewDataSource,HandlerDelegate>
{
    UITableView * myTable;
    NSMutableArray * dataArray;
    int pageIndex;
    RBTestWaitView * waitView;
}
@end

@implementation RBTodaydiscipleViewController
- (void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5249", @"今日已收徒");
    self.navigationController.navigationBar.hidden = YES;
    dataArray = [[NSMutableArray alloc]init];
    pageIndex = 1;
    waitView = [[[NSBundle mainBundle]loadNibNamed:@"RBTestWaitView" owner:nil options:nil]lastObject];
    waitView.frame = CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight - NavHeight);
    waitView.hidden = YES;
    waitView.caculateLabel.text = @"您今日还没有收徒，赶快收徒赚收益吧～";
    [self.view addSubview:waitView];
    
    [self.hudView show];
    //
    [self loadEditView];
    //
    [self request];
}
//
- (void)request{
    Handler * hander = [Handler shareHandler];
    hander.delegate = self;
    [hander getRBTudiToday:[NSString stringWithFormat:@"%d",pageIndex]];
}
- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    myTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageIndex = 1;
        [weakSelf request];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    myTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageIndex = pageIndex + 1;
        [weakSelf request];
    }];
}
- (void)loadEditView{
    myTable = [[UITableView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth,ScreenHeight - NavHeight) style:UITableViewStylePlain];
    myTable.delegate = self;
    myTable.dataSource = self;
    myTable.showsVerticalScrollIndicator = NO;
    myTable.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:myTable];
    [myTable registerNib:[UINib nibWithNibName:@"RBTodayShouTuCell" bundle:nil] forCellReuseIdentifier:@"RBTodayShouTuCell"];
    [self setRefreshHeaderView];
    [self setRefreshFooterView];
    myTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * iden = @"RBTodayShouTuCell";
    RBTodayShouTuCell * cell = [tableView dequeueReusableCellWithIdentifier:iden forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    RBTudiEntity * entity = dataArray[indexPath.row];
    cell.headImageView.layer.cornerRadius = 23;
    cell.headImageView.layer.masksToBounds = YES;
    [cell.headImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",entity.avatar_url]] placeholderImage:PlaceHolderUserImage];
    cell.nameLabel.text = entity.kol_name;
    cell.campainNumLabel.text = [NSString stringWithFormat:@"已接活动数:%@",entity.campaign_invites_count];
    //
    NSString * str = [NSString stringWithFormat:@"￥%@",entity.amount];
    NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:str];
    NSRange range1 = [[attStr string]rangeOfString:[NSString stringWithFormat:@"%@",entity.amount]];
    [attStr addAttribute:NSFontAttributeName value:font_cu_(13) range:NSMakeRange(0, attStr.length)];
    [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:@"000000"] range:range1];
    [attStr addAttribute:NSFontAttributeName value:font_cu_cu_(18) range:range1];
    [cell.moneyLabel setAttributedText:attStr];
//    cell.moneyLabel.text = [NSString stringWithFormat:@"￥%@",entity.amount];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    [self.hudView dismiss];
    if ([sender isEqualToString:@"today_friends"]) {
        if (pageIndex == 1) {
            [dataArray removeAllObjects];
        }
        [myTable.mj_footer endRefreshing];
        [myTable.mj_header endRefreshing];
        dataArray = [JsonService getAllTudiList:jsonObject AndBackArray:dataArray];
        if (dataArray.count == 0) {
            waitView.hidden = NO;
            myTable.hidden = YES;
        }
        //
        if (dataArray.count < [[jsonObject objectForKey:@"total_count"]integerValue]) {
            if (myTable.mj_footer == nil) {
                [self setRefreshFooterView];
            }
        }else{
            [myTable.mj_footer removeFromSuperview];
            myTable.mj_footer = nil;
        }
        
        [myTable reloadData];
    }
    
}
- (void)handlerError:(NSError *)error Tag:(NSString *)sender{
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender{
    [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
