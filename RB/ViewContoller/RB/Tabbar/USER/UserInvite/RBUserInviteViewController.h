//
//  RBUserInviteViewController.h
//  RB
//
//  Created by AngusNi on 5/19/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBUserInviteViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,copy) NSString * allAmount;;
@end
