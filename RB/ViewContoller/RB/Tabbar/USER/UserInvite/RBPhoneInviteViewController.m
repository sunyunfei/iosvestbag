//
//  RBPhoneInviteViewController.m
//  RB
//
//  Created by RB8 on 2018/3/7.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBPhoneInviteViewController.h"
#import "UserPhoneInviteCell.h"
#import "RBMobileEntity.h"
@interface RBPhoneInviteViewController ()<RBBaseVCDelegate,UITableViewDelegate,UITableViewDataSource,HandlerDelegate,UserPhoneInviteCellDelegate>
{
    UITableView * MyTable;
    NSMutableArray*array;
    NSMutableArray*array1;
    NSMutableArray*array2;
    NSMutableArray*array3;
    NSMutableArray*array4;
    BOOL isPermit;
    int uploadCount;
    NSMutableArray * dataSource;
    NSIndexPath * cellIndex;


}
@end

@implementation RBPhoneInviteViewController
- (void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5250", @"通讯录收徒");
    self.navigationController.navigationBar.hidden = YES;
    isPermit = YES;
    dataSource = [[NSMutableArray alloc]init];
    [self getAddressBookInfo];
    [self loadEditView];
}
#pragma mark- cellDelegate
-(void)UserPhoneCellBtnClick:(UITableViewCell *)cell{
    cellIndex = [MyTable indexPathForCell:cell];
    RBMobileEntity * entity = [dataSource objectAtIndex:cellIndex.row];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBInfluenceSmsWithMobile:entity.mobile];
}
// 获取本机通讯录信息
- (void)getAddressBookInfo {
    CFErrorRef *error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    __block BOOL accessGranted = NO;
    if (&ABAddressBookRequestAccessWithCompletion != NULL) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    if (accessGranted == NO) {
        //        RBAlertView *alertView = [[RBAlertView alloc]init];
        //        alertView.delegate = self;
        //        alertView.tag = 1000;
        //        [alertView showWithArray:@[NSLocalizedString(@"R5044", @"是否允许ROBIN8\n获取您的手机通讯录？"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
        isPermit = NO;
//        [MyTable reloadData];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    } else {
        array = [NSMutableArray new];
        array1 = [NSMutableArray new];
        array2 = [NSMutableArray new];
        array3 = [NSMutableArray new];
        array4 = [NSMutableArray new];
        NSArray*listContacts = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
        for (int i=0; i<[listContacts count]; i++) {
            ABRecordRef thisPerson = CFBridgingRetain(listContacts[i]);
            NSString *firstName = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonFirstNameProperty));
            NSString *lastName = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonLastNameProperty));
            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(thisPerson, kABPersonPhoneProperty);
            NSArray* phoneNumberArray = CFBridgingRelease(ABMultiValueCopyArrayOfAllValues(phoneNumberProperty));
            for(int index = 0; index<[phoneNumberArray count]; index++) {
                NSMutableDictionary*diclist = [NSMutableDictionary new];
                NSString*fName = @"";
                NSString*sName = @"";
                if (firstName!=nil&&![firstName isEqual:[NSNull null]]&&firstName.length!=0) {
                    fName = firstName;
                }
                if (lastName!=nil&&![lastName isEqual:[NSNull null]]&&lastName.length!=0) {
                    sName = lastName;
                }
                NSString*mobile = [NSString stringWithFormat:@"%@",phoneNumberArray[index]];
                mobile = [mobile stringByReplacingOccurrencesOfString:@" (" withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@") " withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"+86" withString:@""];
                if (mobile.length==11 && [Utils getIsValidateNumber:mobile]) {
                    [diclist setObject:[NSString stringWithFormat:@"%@%@",sName,fName] forKey:@"name"];
                    [diclist setObject:[NSString stringWithFormat:@"%@",mobile] forKey:@"mobile"];
                    if([array count]<2000) {
                        [array addObject:diclist];
                    } else {
                        if ([array1 count]<2000) {
                            [array1 addObject:diclist];
                        } else {
                            if ([array2 count]<2000) {
                                [array2 addObject:diclist];
                            } else {
                                if ([array3 count]<2000) {
                                    [array3 addObject:diclist];
                                } else {
                                    [array4 addObject:diclist];
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if ([array count]!=0) {
            isPermit = YES;
            uploadCount = (int)[array count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array];
        }
    }
}
- (void)loadEditView{
    MyTable = [[UITableView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight - NavHeight) style:UITableViewStylePlain];
    MyTable.delegate = self;
    MyTable.dataSource = self;
    MyTable.showsVerticalScrollIndicator = NO;
    MyTable.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:MyTable];
    //
    [MyTable registerNib:[UINib nibWithNibName:@"UserPhoneInviteCell" bundle:nil] forCellReuseIdentifier:@"UserPhoneInviteCell"];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataSource.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * iden = @"UserPhoneInviteCell";
    UserPhoneInviteCell * cell = [tableView dequeueReusableCellWithIdentifier:iden forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    if (dataSource.count>0) {
        RBMobileEntity * entity = [dataSource objectAtIndex:indexPath.row];
        cell.nameLabel.text = entity.name;
        cell.phoneLabel.text = entity.mobile;
        if ([entity.status isEqualToString:@"not_invited"]) {
            [cell.inviteButton setTitle:@"邀请" forState:UIControlStateNormal];
            cell.inviteButton.userInteractionEnabled = YES;
            cell.inviteButton.backgroundColor = [Utils getUIColorWithHexString:ccColor2bdbe2];
        }else if([entity.status isEqualToString:@"already_invited"]){
            [cell.inviteButton setTitle:@"已邀请" forState:UIControlStateNormal];
            cell.inviteButton.backgroundColor = [Utils getUIColorWithHexString:ccColord7d7d7];
            cell.inviteButton.userInteractionEnabled = NO;
        }else if ([entity.status isEqualToString:@"already_kol"]){
            [cell.inviteButton setTitle:@"已注册" forState:UIControlStateNormal];
            cell.inviteButton.backgroundColor = [Utils getUIColorWithHexString:ccColord7d7d7];
            cell.inviteButton.userInteractionEnabled = NO;
            cell.userInteractionEnabled = YES;
        }
    }

    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 74;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return  0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  0.1;
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    if ([sender isEqualToString:@"kol_contacts"]) {
        dataSource = [JsonService getRBMobile:jsonObject AndBackArray:dataSource AndPhoneArray:array];
        if(uploadCount==2000&&[array1 count]!=0) {
            uploadCount = uploadCount+(int)[array1 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array1];
            return;
        }
        if(uploadCount==4000&&[array2 count]!=0) {
            uploadCount = uploadCount+(int)[array2 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array2];
            return;
        }
        if(uploadCount==6000&&[array3 count]!=0) {
            uploadCount = uploadCount+(int)[array3 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array3];
            return;
        }
        if(uploadCount==8000&&[array4 count]!=0) {
            uploadCount = uploadCount+(int)[array4 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array4];
            return;
        }
        [MyTable reloadData];
    }
    if ([sender isEqualToString:@"influences/send_invite"]) {
        RBMobileEntity * entity = [dataSource objectAtIndex:cellIndex.row];
        entity.status = @"already_invited";
        [dataSource replaceObjectAtIndex:cellIndex.row withObject:entity];
        [MyTable reloadData];
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"" message:@"已发送邀请" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        if ([[jsonObject objectForKey:@"error"]isEqual:@1]&&[sender isEqualToString:@"influences/send_invite"]) {
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"" message:@"邀请失败" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
