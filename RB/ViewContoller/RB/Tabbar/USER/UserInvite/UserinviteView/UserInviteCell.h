//
//  UserInviteCell.h
//  RB
//
//  Created by RB8 on 2017/8/14.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInviteCell : UITableViewCell
@property(nonatomic,strong)UIView * editView;
@property(nonatomic,copy)void(^FoldBlock)(void);
@end
