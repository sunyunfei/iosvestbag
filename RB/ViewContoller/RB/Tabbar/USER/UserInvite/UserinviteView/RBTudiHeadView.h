//
//  RBTudiHeadView.h
//  RB
//
//  Created by RB8 on 2018/3/6.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBTudiHeadView : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
- (IBAction)moneyAction:(id)sender;
- (IBAction)peopleAction:(id)sender;

@end
