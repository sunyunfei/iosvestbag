//
//  UserPhoneInviteCell.m
//  RB
//
//  Created by RB8 on 2017/8/14.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "UserPhoneInviteCell.h"

@implementation UserPhoneInviteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.inviteButton.layer.cornerRadius = 25/2;
    self.inviteButton.clipsToBounds = YES;
    // Configure the view for the selected state
}

- (IBAction)inviteAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(UserPhoneCellBtnClick:)]) {
        [self.delegate UserPhoneCellBtnClick:self];
    }
}
@end
