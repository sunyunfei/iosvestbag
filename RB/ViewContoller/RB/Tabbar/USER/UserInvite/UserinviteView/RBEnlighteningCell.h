//
//  RBEnlighteningCell.h
//  RB
//
//  Created by RB8 on 2018/3/6.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RBEnlighteningCellDelegate<NSObject>
- (void)ClickBtnAction:(UITableViewCell*)cell;
@end
@interface RBEnlighteningCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UILabel *describeLabel;
@property (weak, nonatomic) IBOutlet UIButton *inviteBtn;
@property (nonatomic,weak) id<RBEnlighteningCellDelegate>delegate;
- (IBAction)btnAction:(id)sender;

@end
