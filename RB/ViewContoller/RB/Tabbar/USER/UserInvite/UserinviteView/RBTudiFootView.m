//
//  RBTudiFootView.m
//  RB
//
//  Created by RB8 on 2018/3/6.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBTudiFootView.h"

@implementation RBTudiFootView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)rewardAction:(id)sender {
    static BOOL isFold = YES;
    if (isFold == NO) {
        [_rewardBtn setImage:[UIImage imageNamed:@"RBBackPin1"] forState:UIControlStateNormal];
        _footView.hidden = NO;
    }else{
        [_rewardBtn setImage:[UIImage imageNamed:@"RBBackPin2"] forState:UIControlStateNormal];
        _footView.hidden = YES;
    }
    isFold = !isFold;
    if ([self.delegate respondsToSelector:@selector(foldtheRules)]) {
        [self.delegate foldtheRules];
    }
}
@end
