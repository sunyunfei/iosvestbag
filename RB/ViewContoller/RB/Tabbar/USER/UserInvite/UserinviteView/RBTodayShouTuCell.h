//
//  RBTodayShouTuCell.h
//  RB
//
//  Created by RB8 on 2018/3/7.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBTodayShouTuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *campainNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@end
