//
//  RBEnlighteningCell.m
//  RB
//
//  Created by RB8 on 2018/3/6.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBEnlighteningCell.h"

@implementation RBEnlighteningCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(ClickBtnAction:)]) {
        [self.delegate ClickBtnAction:self];
    }
}
@end
