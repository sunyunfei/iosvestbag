//
//  UserPhoneInviteCell.h
//  RB
//
//  Created by RB8 on 2017/8/14.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol UserPhoneInviteCellDelegate <NSObject>
-(void)UserPhoneCellBtnClick:(UITableViewCell*)cell;
@end
@interface UserPhoneInviteCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
- (IBAction)inviteAction:(id)sender;
@property (nonatomic,weak)id<UserPhoneInviteCellDelegate>delegate;
@end
