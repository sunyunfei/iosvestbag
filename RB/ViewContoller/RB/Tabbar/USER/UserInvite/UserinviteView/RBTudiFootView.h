//
//  RBTudiFootView.h
//  RB
//
//  Created by RB8 on 2018/3/6.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RBTudiFootViewDelegate<NSObject>
- (void)foldtheRules;
@end
@interface RBTudiFootView : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UIButton *rewardBtn;
- (IBAction)rewardAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *footView;
@property (weak,nonatomic)id<RBTudiFootViewDelegate>delegate;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@end
