//
//  UserInviteView.m
//  RB
//
//  Created by RB8 on 2017/8/16.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "UserInviteView.h"

@implementation UserInviteView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib{
    [super awakeFromNib];
    self.inviteButton.layer.cornerRadius = 16;
    self.inviteButton.clipsToBounds = YES;
}
- (IBAction)inviteAction:(id)sender {
    id object = [self nextResponder];
    while (![object isKindOfClass:[UIViewController class]] && object != nil) {
        object = [object nextResponder];
    }
    UIViewController *superController = (UIViewController*)object;
    UIAlertController * alertView = [UIAlertController alertControllerWithTitle:@"提示" message:@"去开通通讯录权限" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * defaultBtn = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    UIAlertAction * cancelBtn = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertView addAction:defaultBtn];
    [alertView addAction:cancelBtn];
    [superController presentViewController:alertView animated:YES completion:nil];
}
@end
