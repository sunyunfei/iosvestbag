//
//  UserInviteCell.m
//  RB
//
//  Created by RB8 on 2017/8/14.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "UserInviteCell.h"
#import "Service.h"
@implementation UserInviteCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.userInteractionEnabled = YES;
        //
     
        UIView * ruleView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenHeight, 37)];
        ruleView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        [self.contentView addSubview:ruleView];
//        UIView*editView1 = [[UIView alloc]initWithFrame:CGRectMake(editView.left, editView.left+editView.bottom, editView.width, 0)];
//        editView1.userInteractionEnabled = YES;
//        editView1.backgroundColor = editView.backgroundColor;
//        [scrollviewn addSubview:editView1];
       
        //
        UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, 100, 37)];
        tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel1.font = font_(16);
        tLabel1.text = NSLocalizedString(@"R5097", @"奖励规则");
        [ruleView addSubview:tLabel1];
        //
        UIImageView * pinImageview = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth-CellLeft-16, (37-9)/2, 16, 9)];
        [ruleView addSubview:pinImageview];
        //
        self.editView = [[UIView alloc]initWithFrame:CGRectMake(0, ruleView.bottom, ScreenWidth, 0)];
        self.editView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
//        self.editView.alpha = 0.5;
        [self.contentView addSubview:self.editView];
        //
        UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 0.5)];
        lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.editView addSubview:lineLabel1];
        //
        UILabel *ttContentLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineLabel1.bottom+18.0, ScreenWidth-2*CellLeft, 20.0)];
        ttContentLabel.numberOfLines = 0;
        ttContentLabel.font = font_(14);
        ttContentLabel.text = NSLocalizedString(@"R5098", @"方式1:点击下方立即邀请按钮,分享给朋友完成注册\n方式2:将您的邀请码告诉朋友,在登录Robin8时输入");
        ttContentLabel.textColor = [UIColor blackColor];
        ttContentLabel.backgroundColor = [UIColor whiteColor];
        [self.editView addSubview:ttContentLabel];
        //
        [Utils getUILabel:ttContentLabel withLineSpacing:20.0];
        CGSize ttContentLabelSize = [Utils getUIFontSizeFitH:ttContentLabel withLineSpacing:20.0];
        ttContentLabel.height = ttContentLabelSize.height;
        //
        UILabel *aboutLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, ttContentLabel.bottom+CellLeft, ScreenWidth-CellLeft*2.0, 20.0)];
        aboutLabel.numberOfLines = 0;
        aboutLabel.textAlignment = NSTextAlignmentLeft;
        aboutLabel.font = font_(13);
        aboutLabel.text = NSLocalizedString(@"R5099", @"注意：只有朋友在 Robin8 进行一个活动并且上传截图后才可以获得奖励，每天最多获得 10 次奖励，成功后奖励直接进入您的钱包");
        aboutLabel.textColor = [Utils getUIColorWithHexString:SysColorGray1];
        aboutLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.5];
        [self.editView addSubview:aboutLabel];
        [Utils getUILabel:aboutLabel withLineSpacing:4.0];
        CGSize aboutLabelSize = [Utils getUIFontSizeFitH:aboutLabel withLineSpacing:4.0];
        aboutLabel.height = aboutLabelSize.height;
        //
        UILabel * explainLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, aboutLabel.bottom + 5, ScreenWidth-CellLeft*2.0,13) text:NSLocalizedString(@"R5241", @"本活动最终解释权归Robin8所有") font:font_(13) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorGray2] backgroundColor:[UIColor clearColor] numberOfLines:0];
        [self.editView addSubview:explainLabel];
        //
        self.editView.height = explainLabel.bottom;
        self.editView.hidden = YES;
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
