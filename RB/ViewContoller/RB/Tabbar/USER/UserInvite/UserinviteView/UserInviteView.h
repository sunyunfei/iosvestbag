//
//  UserInviteView.h
//  RB
//
//  Created by RB8 on 2017/8/16.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInviteView : UIView
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;

- (IBAction)inviteAction:(id)sender;
@end
