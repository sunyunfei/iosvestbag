//
//  RBTudiAllRewardView.h
//  RB
//
//  Created by RB8 on 2018/3/7.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBTudiAllRewardView : UIView
@property (weak, nonatomic) IBOutlet UILabel *tudiNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *shouyiLabel;

@end
