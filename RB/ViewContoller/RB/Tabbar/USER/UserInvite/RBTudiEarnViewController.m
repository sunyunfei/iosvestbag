//
//  RBTudiEarnViewController.m
//  RB
//
//  Created by RB8 on 2018/3/7.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBTudiEarnViewController.h"
#import "RBTudiAllRewardView.h"
#import "RBTudiShouyiCell.h"
#import "RBTudiEntity.h"
@interface RBTudiEarnViewController ()<RBBaseVCDelegate,UITableViewDelegate,UITableViewDataSource,HandlerDelegate>
{
    UITableView * MyTable;
    RBTudiAllRewardView * rewardView;
    NSMutableArray * dataArray;
    int pageIndex;
}
@end

@implementation RBTudiEarnViewController
- (void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5247", @"收徒总收益");
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    self.navigationController.navigationBar.hidden = YES;
    dataArray = [[NSMutableArray alloc]init];
    pageIndex = 1;
    [self loadTopView];
    [self loadEditView];
    [self.hudView show];
    [self request];
    
}
- (void)request{
    Handler * hanld = [Handler shareHandler];
    hanld.delegate = self;
    [hanld getRBTuDiAllReward:[NSString stringWithFormat:@"%d",pageIndex]];
}
- (void)loadTopView{
    rewardView = [[[NSBundle mainBundle]loadNibNamed:@"RBTudiAllRewardView" owner:self options:nil]lastObject];
    rewardView.frame = CGRectMake(0, NavHeight, ScreenWidth, 60.5);
    [self.view addSubview:rewardView];
    //
    
}
- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    MyTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageIndex = 1;
        [weakSelf request];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    MyTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageIndex = pageIndex + 1;
        [weakSelf request];
    }];
}
- (void)loadEditView{
    MyTable = [[UITableView alloc]initWithFrame:CGRectMake(0, rewardView.bottom + 8, ScreenWidth, ScreenHeight - rewardView.bottom - 8) style:UITableViewStylePlain];
    MyTable.delegate = self;
    MyTable.dataSource = self;
    MyTable.showsVerticalScrollIndicator = NO;
    MyTable.showsHorizontalScrollIndicator = NO;
    MyTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:MyTable];
    [self setRefreshHeaderView];
    [self setRefreshFooterView];
    [MyTable registerNib:[UINib nibWithNibName:@"RBTudiShouyiCell" bundle:nil] forCellReuseIdentifier:@"RBTudiShouyiCell"];
    //
    UIView * headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth,20 + 23)];
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(12, 20, 23, 23)];
    imageView.image = [UIImage imageNamed:@"RBTudiListIcon"];
    imageView.layer.cornerRadius = 23/2;
    imageView.layer.masksToBounds = YES;
    [headView addSubview:imageView];
    //
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(imageView.right + 15, 25, 100, 13) text:NSLocalizedString(@"R5248", @"排行榜") font:font_(13) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:@"000000"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [headView addSubview:label];
    label.centerY = imageView.centerY;
    [MyTable setTableHeaderView:headView];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * iden = @"RBTudiShouyiCell";
    RBTudiShouyiCell * cell = [tableView dequeueReusableCellWithIdentifier:iden forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (dataArray.count > 0) {
        RBTudiEntity * entity = dataArray[indexPath.row];
        if (indexPath.row == 0) {
            cell.titleImageView.hidden = NO;
            cell.titleImageView.image = [UIImage imageNamed:@"RBRewardFirst"];
            cell.numlabel.text = @"";
        }else if (indexPath.row == 1){
            cell.titleImageView.hidden = NO;
            cell.titleImageView.image = [UIImage imageNamed:@"RBRewardSecond"];
            cell.numlabel.text = @"";
        }else if (indexPath.row == 2){
            cell.titleImageView.hidden = NO;
            cell.titleImageView.image = [UIImage imageNamed:@"RBRewardThird"];
            cell.numlabel.text = @"";
        }else{
            cell.numlabel.text = [NSString stringWithFormat:@"%ld",indexPath.row + 1];
            cell.titleImageView.hidden = YES;
        }
        [cell.headImageView sd_setImageWithURL:[NSURL URLWithString:entity.avatar_url] placeholderImage:PlaceHolderUserImage];
        cell.nameLabel.text = entity.kol_name;
        cell.campainNumLabel.text = [NSString stringWithFormat:@"已接活动数:%@",entity.campaign_invites_count];
        /*
         NSString * str = [NSString stringWithFormat:@"共收徒 %@ 人",[jsonObject objectForKey:@"total_count"]];
         NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:str];
         NSRange range1 = [[attStr string]rangeOfString:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"total_count"]]];
         [attStr addAttribute:NSFontAttributeName value:font_(13) range:NSMakeRange(0, attStr.length)];
         [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorYellow] range:range1];
         [attStr addAttribute:NSFontAttributeName value:font_cu_cu_(18) range:range1];
         [rewardView.tudiNumLabel setAttributedText:attStr];
         */
        NSString * str = [NSString stringWithFormat:@"￥%@",entity.amount];
        NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:str];
        NSRange range1 = [[attStr string]rangeOfString:[NSString stringWithFormat:@"%@",entity.amount]];
        [attStr addAttribute:NSFontAttributeName value:font_cu_(13) range:NSMakeRange(0, attStr.length)];
        [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:@"000000"] range:range1];
        [attStr addAttribute:NSFontAttributeName value:font_cu_cu_(18) range:range1];
        [cell.moneyLabel setAttributedText:attStr];
        
//        cell.moneyLabel.text = [NSString stringWithFormat:@"￥%@",entity.amount];
        cell.headImageView.layer.cornerRadius = 23.0;
        cell.headImageView.layer.masksToBounds = YES;
    }
    
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    [self.hudView dismiss];
    if ([sender isEqualToString:@"percentage_on_friend"]) {
        if (pageIndex == 1) {
            [dataArray removeAllObjects];
        }
        [MyTable.mj_footer endRefreshing];
        [MyTable.mj_header endRefreshing];
        dataArray = [JsonService getAllTudiList:jsonObject AndBackArray:dataArray];
        NSString * str = [NSString stringWithFormat:@"共收徒 %@ 人",[jsonObject objectForKey:@"total_count"]];
        NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:str];
        NSRange range1 = [[attStr string]rangeOfString:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"total_count"]]];
        [attStr addAttribute:NSFontAttributeName value:font_(13) range:NSMakeRange(0, attStr.length)];
        [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorYellow] range:range1];
        [attStr addAttribute:NSFontAttributeName value:font_cu_cu_(18) range:range1];
        [rewardView.tudiNumLabel setAttributedText:attStr];
        //
        NSString * str1 = [NSString stringWithFormat:@"共收益 %@ 元",self.earnMoney];
        NSMutableAttributedString * attStr1 = [[NSMutableAttributedString alloc]initWithString:str1];
        NSRange range2 = [[attStr1 string]rangeOfString:[NSString stringWithFormat:@"%@",self.earnMoney]];
        [attStr1 addAttribute:NSFontAttributeName value:font_(13) range:NSMakeRange(0, attStr1.length)];
        [attStr1 addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorYellow] range:range2];
        [attStr1 addAttribute:NSFontAttributeName value:font_cu_cu_(18) range:range2];
        [rewardView.shouyiLabel setAttributedText:attStr1];
        
        if (dataArray.count < [[jsonObject objectForKey:@"total_count"]integerValue]) {
            if (MyTable.mj_footer == nil) {
                [self setRefreshFooterView];
            }
        }else{
            [MyTable.mj_footer removeFromSuperview];
            MyTable.mj_footer = nil;
        }
        
        [MyTable reloadData];
    }
    
}
- (void)handlerError:(NSError *)error Tag:(NSString *)sender{
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender{
    [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
