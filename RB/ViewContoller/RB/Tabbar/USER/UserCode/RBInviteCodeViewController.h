//
//  RBInviteCodeViewController.h
//  RB
//
//  Created by RB8 on 2017/10/24.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBInviteCodeViewController : RBBaseViewController<RBBaseVCDelegate,UITextFieldDelegate,TextFiledKeyBoardDelegate,HandlerDelegate>

@end
