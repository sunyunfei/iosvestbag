//
//  RBInviteCodeViewController.m
//  RB
//
//  Created by RB8 on 2017/10/24.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBInviteCodeViewController.h"

@interface RBInviteCodeViewController ()
{
    InsetsTextField*inviteTF;
}
@end

@implementation RBInviteCodeViewController
-(void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navTitle = NSLocalizedString(@"R5218",@"输入邀请码");
    self.delegate = self;
    inviteTF = [[InsetsTextField alloc]
                initWithFrame:CGRectMake(15, NavHeight+10.0, ScreenWidth-30.0, 50.0)];
    inviteTF.font = font_(16.0);
    inviteTF.backgroundColor = [UIColor clearColor];
    inviteTF.keyboardType = UIKeyboardTypeNumberPad;
    inviteTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    inviteTF.attributedPlaceholder = [[NSAttributedString alloc]
                                      initWithString:NSLocalizedString(@"R5218", @"输入邀请码")
                                      attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
    inviteTF.returnKeyType = UIReturnKeyDone;
    inviteTF.delegate = self;
    [self.view addSubview:inviteTF];
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(inviteTF.left,inviteTF.bottom-0.5f, inviteTF.width, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    [self.view addSubview:lineLabel];
    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]
                                      initWithFrame:CGRectMake(0, ScreenHeight - 216 - 44,
                                                               ScreenWidth, 216 + 44)];
    textFiledKB.backgroundColor = [UIColor clearColor];
    textFiledKB.delegateT = self;
    [inviteTF setInputAccessoryView:textFiledKB];
    //
    UIButton * submitBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, ScreenHeight - 50, ScreenWidth, 50) title:NSLocalizedString(@"R2080", @"提交") hlTitle:nil titleColor:[UIColor whiteColor] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysBackColorBlue] image:nil hlImage:nil];
    [submitBtn addTarget:self action:@selector(SubmitAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submitBtn];
}
- (BOOL)isNum:(NSString *)checkedNumString {
    checkedNumString = [checkedNumString stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(checkedNumString.length > 0) {
        return NO;
    }
    return YES;
}
-(void)SubmitAction:(id)sender{
    if (inviteTF.text.length <= 0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5220", @"邀请码不能为空")];
    }else if ([self isNum:inviteTF.text] == NO){
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5221", @"邀请码只能为数字")];
    }else{
        Handler * handler = [Handler shareHandler];
        handler.delegate = self;
        [handler submitInviteCode:inviteTF.text];
    }
}
-(void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    if ([[jsonObject objectForKey:@"error"]intValue] == 0) {
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5219", @"验证成功")];
        [self performSelector:@selector(pop) withObject:nil afterDelay:2.0];
    }
}
-(void)pop{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)handlerError:(NSError *)error Tag:(NSString *)sender{
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
-(void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender{
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}
#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
