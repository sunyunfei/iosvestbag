//
//  RBUserTagsView.h
//  RB
//
//  Created by AngusNi on 16/1/28.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>


@class RBUserTagsView;
@protocol RBUserTagsViewDelegate <NSObject>
@optional
@end


@interface RBUserTagsView : UIView
@property(nonatomic, weak) id<RBUserTagsViewDelegate> delegate;
@property(nonatomic, strong) UIScrollView* scrollviewn;

- (void)loadWithTags:(NSArray*)tagsArray;
@end
