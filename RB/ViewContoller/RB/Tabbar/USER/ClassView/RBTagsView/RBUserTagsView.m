//
//  RBUserTagsView.m
//  RB
//
//  Created by AngusNi on 16/1/28.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBUserTagsView.h"
#import "Service.h"

@implementation RBUserTagsView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame=frame;
        //
        self.scrollviewn = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self.scrollviewn setPagingEnabled:NO];
        [self.scrollviewn setShowsHorizontalScrollIndicator:NO];
        [self.scrollviewn setShowsVerticalScrollIndicator:NO];
        self.scrollviewn.autoresizesSubviews = YES;
        self.scrollviewn.directionalLockEnabled = YES;
        [self addSubview:self.scrollviewn];
    }
    return self;
}

- (void)loadWithTags:(NSArray*)tagsArray {
    [self.scrollviewn removeAllSubviews];
    if ([tagsArray count]==0) {
        return;
    }
    float tempw=30.0;
    float temph=15.0;
    float tempgap=15.0;
    if ([tagsArray count]>1) {
        float tempW=0;
        int tagsCount = (int)[tagsArray count];
        if (tagsCount>=5) {
            tagsCount=5;
        }
        for (int i=0; i<tagsCount; i++) {
            UILabel*tempLabel = [[UILabel alloc]initWithFrame:CGRectMake(i*(tempw+tempgap), 0, tempw, temph)];
            tempLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
            tempLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
            tempLabel.font = font_11;
            tempLabel.textAlignment = NSTextAlignmentCenter;
            tempLabel.text = tagsArray[i];
            tempLabel.tag = 1000+i;
            [self.scrollviewn addSubview:tempLabel];
            CGSize tempLabelSize = [Utils getUIFontSizeFitW:tempLabel];
            tempLabel.width = tempLabelSize.width+6.0;
            if (i>0) {
                UILabel*tempLabel1 = (UILabel*)[self.scrollviewn viewWithTag:1000+i-1];
                tempLabel.left = tempgap+tempLabel1.right;
            } else {
                tempLabel.left = tempgap;
            }
            tempW = tempW+tempLabelSize.width+6.0;
        }
        if (tempW > self.width) {
            [self.scrollviewn setContentSize:CGSizeMake(tempW, self.height)];
        } else {
            [self.scrollviewn setContentSize:CGSizeMake(self.width, self.height)];
            float firstgap = (self.width-tempW-(tagsCount-1)*tempgap)/2.0;
            for (int i=0; i<[tagsArray count]; i++) {
                UILabel*tempLabel = (UILabel*)[self.scrollviewn viewWithTag:1000+i];
                if (i==0) {
                    tempLabel.left = firstgap;
                } else {
                    UILabel*tempLabel1 = (UILabel*)[self.scrollviewn viewWithTag:1000+i-1];
                    tempLabel.left = tempgap+tempLabel1.right;
                }
            }
        }
    } else {
        UILabel*tempLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, tempw, temph)];
        tempLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
        tempLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        tempLabel.font = font_11;
        tempLabel.textAlignment = NSTextAlignmentCenter;
        tempLabel.text = tagsArray[0];
        [self.scrollviewn addSubview:tempLabel];
        CGSize tempLabelSize = [Utils getUIFontSizeFitW:tempLabel];
        tempLabel.width = tempLabelSize.width+6.0;
        tempLabel.left = (self.width-tempLabel.width)/2.0;
        [self.scrollviewn setContentSize:CGSizeMake(self.width, self.height)];
    }
}

@end
