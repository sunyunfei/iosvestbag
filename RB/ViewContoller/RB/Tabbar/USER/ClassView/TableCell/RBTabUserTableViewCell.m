//
//  RBTabUserTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBTabUserTableViewCell.h"
#import "Service.h"

@implementation RBTabUserTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UILabel*iconLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        iconLabel.font = font_icon_(25.0);
        iconLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        iconLabel.tag = 1002;
        [self.contentView addSubview:iconLabel];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1003;
        [self.contentView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel.font = font_11;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        cLabel.tag = 1004;
        cLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:cLabel];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1005;
        [self.contentView addSubview:lineLabel];
        //
        UILabel*arrowLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        arrowLabel.font = font_icon_(13.0);
        arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        arrowLabel.text = Icon_btn_right;
        arrowLabel.tag = 1007;
        [self.contentView addSubview:arrowLabel];
        //
        UIImageView * iconImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
        iconImageView.tag = 1008;
        [self.contentView addSubview:iconImageView];
    }
    return self;
}

- (instancetype)loadRBTabUserCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UILabel*iconLabel=(UILabel*)[self.contentView viewWithTag:1002];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1004];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1005];
    UILabel*arrowLabel=(UILabel*)[self.contentView viewWithTag:1007];
    UIImageView*iconImageView = (UIImageView*)[self.contentView viewWithTag:1008];
//    NSMutableArray*imglist = [NSMutableArray arrayWithArray: @[@[Icon_btn_money,Icon_btn_campaign,Icon_btn_care_l],@[Icon_btn_ad],@[Icon_btn_indiana,Icon_btn_checkin,Icon_btn_invite],@[Icon_btn_help]]];
    NSMutableArray*imglist = [NSMutableArray arrayWithArray: @[@[Icon_btn_money,Icon_btn_campaign],@[Icon_btn_ad],@[Icon_btn_indiana,Icon_btn_checkin,Icon_btn_invite],@[@"",Icon_btn_help]]];
    
    
    
//    @[@[NSLocalizedString(@"R5030", @"我的钱包"),NSLocalizedString(@"R5120",@"我的活动"),NSLocalizedString(@"R5251",@"我的收藏")],@[NSLocalizedString(@"R5019", @"品牌主")],@[NSLocalizedString(@"R5132",@"一元夺宝"),NSLocalizedString(@"R5253", @"每日任务"),NSLocalizedString(@"R5243", @"收徒赚收益"),NSLocalizedString(@"R5218", @"输入邀请码")],@[NSLocalizedString(@"R5107", @"在线客服"),NSLocalizedString(@"R7036",@"帮助中心")]]]
    //
    bgView.backgroundColor = [Utils getUIColorWithHexString:@"ffffff" andAlpha:0.7];
    NSArray*array = datalist[indexPath.section];
    NSArray*imgarray = imglist[indexPath.section];
    tLabel.text = array[indexPath.row];
    if ([tLabel.text isEqualToString:NSLocalizedString(@"R5218", @"输入邀请码")]) {
        iconImageView.frame = CGRectMake(CellLeft, (60.0-18.0)/2.0, 25, 18);
        iconImageView.image = [UIImage imageNamed:@"RBInvitecode"];
    }else if ([tLabel.text isEqualToString:NSLocalizedString(@"R5107", @"在线客服")]){
        iconImageView.frame = CGRectMake(CellLeft, (60.0-18.0)/2, 25, 18);
        iconImageView.image = [UIImage imageNamed:@"RBCustomService"];
    }else if ([tLabel.text isEqualToString:NSLocalizedString(@"R5251",@"我的收藏")]){
        iconImageView.frame = CGRectMake(CellLeft, (60.0-22)/2, 22, 22);
        iconImageView.image = [UIImage imageNamed:@"RBMyCollectAD"];
    }else{
        iconLabel.text = imgarray[indexPath.row];
    }
    cLabel.text = @"";
    iconLabel.frame = CGRectMake(CellLeft, (60.0-25.0)/2.0, 25.0, 25.0);
    tLabel.frame = CGRectMake(iconLabel.right+CellLeft, 0, ScreenWidth, 60.0);
    cLabel.frame = CGRectMake(CellLeft, 0, ScreenWidth-CellLeft*2.0-13.0-5.0, tLabel.height);
    lineLabel.frame = CGRectMake(tLabel.left, tLabel.bottom-0.5f, ScreenWidth-tLabel.left, 0.5f);
    if (indexPath.row==[array count]-1) {
        lineLabel.frame = CGRectMake(0, tLabel.bottom-0.5f, ScreenWidth, 0.5f);
    }
    arrowLabel.frame = CGRectMake(ScreenWidth-CellLeft-13.0, (tLabel.height-13.0)/2.0, 13.0, 13.0);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    return self;
}

@end
