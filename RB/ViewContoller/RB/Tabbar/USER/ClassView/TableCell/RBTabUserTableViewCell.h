//
//  RBTabUserTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBInfluenceReadEntity.h"

@interface RBTabUserTableViewCell : UITableViewCell

- (instancetype)loadRBTabUserCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;

@end
