//
//  RBUserBtn.m
//  RB
//
//  Created by AngusNi on 5/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserBtn.h"
#import "Service.h"

@implementation RBUserBtn

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        //
        self.btnIV = [[UIImageView alloc]initWithFrame:CGRectMake((self.width-25.0)/2.0, 10.0, 25.0, 25.0)];
        [self addSubview:self.btnIV];
        //
        self.btnIVLabel = [[UILabel alloc]initWithFrame:CGRectMake((self.width-25.0)/2.0, 15.0, 25.0, 25.0)];
        self.btnIVLabel.font = font_icon_(self.btnIVLabel.width);
        self.btnIVLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self addSubview:self.btnIVLabel];
        //
        self.btnLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.btnIVLabel.bottom+6.0, self.width, 18.0)];
        self.btnLabel.font = font_15;
        self.btnLabel.textAlignment = NSTextAlignmentCenter;
        self.btnLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self addSubview:self.btnLabel];
    }
    return self;
}

@end
