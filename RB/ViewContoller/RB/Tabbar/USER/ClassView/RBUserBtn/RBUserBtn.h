//
//  RBUserBtn.h
//  RB
//
//  Created by AngusNi on 5/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBUserBtn : UIButton
@property(nonatomic, strong) UILabel*btnLabel;
@property(nonatomic, strong) UIImageView*btnIV;
@property(nonatomic, strong) UILabel*btnIVLabel;


@end
