//
//  RBMyCollectViewController.m
//  RB
//
//  Created by RB8 on 2018/3/30.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBMyCollectViewController.h"
#import "NewDynamicsTableViewCell.h"
#import "RBShareView.h"
#import "Handler.h"
#import "RBTestWaitView.h"
#import "RBFindDetailViewController.h"
@interface RBMyCollectViewController ()<RBBaseVCDelegate,UITableViewDelegate,UITableViewDataSource,NewDynamicsCellDelegate,HandlerDelegate,RBShareViewDelegate,RBFindDetailControllerDelegate>
@property(nonatomic,strong)UITableView * myTable;
@property(nonatomic,strong)NSMutableArray * layoutsArr;
//记录选择的按钮index
@property(nonatomic,assign)int pageIndex;
@property(nonatomic,assign)NSIndexPath * KCIndexPath;
@property(nonatomic,strong)NSString * KCType;
@property(nonatomic,strong)RBTestWaitView * waitView;
@end

@implementation RBMyCollectViewController
- (void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
-(NSMutableArray*)layoutsArr{
    if (!_layoutsArr) {
        _layoutsArr = [NSMutableArray array];
    }
    return _layoutsArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = YES;
    self.navTitle = NSLocalizedString(@"R5251",@"我的收藏");
    self.delegate = self;
    _pageIndex = 1;
    [self loadData];
    [self loadEditView];
}
- (void)loadData{
    [self.hudView show];
    Handler * hander = [Handler shareHandler];
    hander.delegate = self;
    [hander getRBMyCollectArticleList:@"collects" andPageIndex:_pageIndex];
}
- (void)setRefreshHeaderView{
    __weak typeof(self) weakSelf = self;
    self.myTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pageIndex = 1;
        [weakSelf loadData];
    }];
}
- (void)setRefreshFooterView{
    __weak typeof(self) weakSelf = self;
    self.myTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.pageIndex = self.pageIndex + 1;
        [weakSelf loadData];
    }];
}
- (void)loadEditView{
    _myTable = [[UITableView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight - NavHeight) style:UITableViewStylePlain];
    _myTable.delegate = self;
    _myTable.dataSource = self;
    _myTable.showsVerticalScrollIndicator = NO;
    _myTable.showsHorizontalScrollIndicator = NO;
    [_myTable registerClass:[NewDynamicsTableViewCell class] forCellReuseIdentifier:@"NewDynamicsTableViewCell"];
    _myTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self setRefreshHeaderView];
    [self setRefreshFooterView];
    [self.view addSubview:_myTable];
    //
    _waitView = [[[NSBundle mainBundle]loadNibNamed:@"RBTestWaitView" owner:nil options:nil]lastObject];
    _waitView.frame = CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-49);
    _waitView.caculateLabel.text = NSLocalizedString(@"R5252", @"还未收藏内容，快去收藏吧~");
    _waitView.hidden = YES;
    [self.view addSubview:_waitView];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.layoutsArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewDynamicsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NewDynamicsTableViewCell"];
    cell.layout = self.layoutsArr[indexPath.row];
    cell.delegate = self;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewDynamicsLayout * layout = self.layoutsArr[indexPath.row];
    return layout.height;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RBFindDetailViewController * vc = [[RBFindDetailViewController alloc]init];
    vc.findDelegate = self;
    NewDynamicsLayout * layout = self.layoutsArr[indexPath.row];
    vc.layout = layout;
    vc.hidesBottomBarWhenPushed = YES;
    vc.postId = layout.model.post_id;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)DynamicsCell:(NewDynamicsTableViewCell *)cell didClicktext:(NSString *)text{
    NSIndexPath * indexpath = [self.myTable indexPathForCell:cell];
    RBFindDetailViewController * vc = [[RBFindDetailViewController alloc]init];
    vc.findDelegate = self;
    NewDynamicsLayout * layout = self.layoutsArr[indexpath.row];
    vc.layout = layout;
    vc.hidesBottomBarWhenPushed = YES;
    vc.postId = layout.model.post_id;
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (void)reloadAction{
    [self.myTable reloadData];
}
- (void)DidClickCollectDynamicsCell:(NewDynamicsTableViewCell *)cell{
    NSLog(@"点击了收藏");
    _KCIndexPath = [self.myTable indexPathForCell:cell];
    
    NewDynamicsLayout * layout = self.layoutsArr[_KCIndexPath.row];
    _KCType = @"collect";
    if ([layout.model.is_collected isEqualToString:@"0"]) {
        Handler * handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBAddOrCancellike:@"collect" andpost_id:layout.model.post_id andType:@"add"];
    }else{
        Handler * handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBAddOrCancellike:@"collect" andpost_id:layout.model.post_id andType:@"cancel"];
    }
}
- (void)DidClickZanBtnCell:(NewDynamicsTableViewCell *)cell{
    NSLog(@"点击了点赞");
    _KCIndexPath = [self.myTable indexPathForCell:cell];
    _KCType = @"like";
    NewDynamicsLayout * layout = self.layoutsArr[_KCIndexPath.row];
    
    if ([layout.model.is_liked isEqualToString:@"0"]) {
        Handler * handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBAddOrCancellike:@"like" andpost_id:layout.model.post_id andType:@"add"];
    }else{
        Handler * handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBAddOrCancellike:@"like" andpost_id:layout.model.post_id andType:@"cancel"];
    }

}
- (void)DidClickShareInDynamicsCell:(NewDynamicsTableViewCell *)cell{
    NSLog(@"点击了分享");
    _KCIndexPath = [self.myTable indexPathForCell:cell];
    _KCType = @"forward";
    NewDynamicsLayout * layout = self.layoutsArr[_KCIndexPath.row];
    Handler * handler = [Handler shareHandler];
    RBShareView *shareView = [RBShareView sharedRBShareView];
    shareView.delegate = self;
    [shareView showViewWithTitle:[NSString stringWithFormat:@"%@",layout.model.title] Content:[NSString stringWithFormat:@"%@",layout.model.title] URL:[NSString stringWithFormat:@"%@%@",handler.postUrl,layout.model.forward_url] ImgURL:@"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png"];
}
-(void)DidClickMoreLessInDynamicsCell:(NewDynamicsTableViewCell *)cell
{
    NSIndexPath * indexPath = [self.myTable indexPathForCell:cell];
    NewDynamicsLayout * layout = self.layoutsArr[indexPath.row];
    layout.model.isOpening = !layout.model.isOpening;
    [layout resetLayout];
    CGRect cellRect = [self.myTable rectForRowAtIndexPath:indexPath];
    
    [self.myTable reloadData];
    
    if (cellRect.origin.y < self.myTable.contentOffset.y + 64) {
        [self.myTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}
- (void)DynamicsCell:(NewDynamicsTableViewCell *)cell didClickUrl:(NSString *)url PhoneNum:(NSString *)phoneNum{
    if (url) {
        if ([url rangeOfString:@"wemall"].length != 0 || [url rangeOfString:@"t.cn"].length != 0) {
            if (![url hasPrefix:@"http://"]) {
                url = [NSString stringWithFormat:@"http://%@",url];
            }
            NSLog(@"点击了链接:%@",url);
        }else{
            [SVProgressHUD showErrorWithStatus:@"暂不支持打开外部链接"];
        }
    }
    if (phoneNum) {
        NSLog(@"点击了电话:%@",phoneNum);
    }
}
//判断是否是纯数字
- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}
//
- (void)shareSuccess{
    NewDynamicsLayout * layout = self.layoutsArr[_KCIndexPath.row];
    
    Handler * handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBAddOrCancellike:@"forward" andpost_id:layout.model.post_id andType:@"add"];
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    if ([sender isEqualToString:@"my/article_lists"]) {
        [self.hudView dismiss];
        [_myTable.mj_header endRefreshing];
        [_myTable.mj_footer endRefreshing];
        NSArray * dataArray = [jsonObject objectForKey:@"list"];
        if (_pageIndex == 1) {
            [_layoutsArr removeAllObjects];
            [self.myTable setContentOffset:CGPointMake(0, 0) animated:NO];
        }
        for (id dict in dataArray) {
            DynamicsModel * model = [DynamicsModel modelWithDictionary:dict];
            NewDynamicsLayout * layout = [[NewDynamicsLayout alloc]initWithModel:model];
            [self.layoutsArr addObject:layout];
        }
        NSInteger totalCount = [[jsonObject objectForKey:@"total_count"]integerValue];
        if (self.layoutsArr.count == totalCount) {
            [self.myTable.mj_footer removeFromSuperview];
        }
        if (self.layoutsArr.count == 0) {
            self.waitView.hidden = NO;
            self.myTable.hidden = YES;
        }
        [self.myTable reloadData];
        return;
    }
    if ([sender isEqualToString:@"articles/set"]) {
        NewDynamicsLayout * layout = self.layoutsArr[_KCIndexPath.row];
        if ([_KCType isEqualToString:@"like"]) {
            if ([layout.model.is_liked isEqualToString:@"0"]) {
                layout.model.is_liked = @"1";
                if ([self isPureInt:layout.model.likes_count]) {
                    int likeCount = [layout.model.likes_count intValue];
                    likeCount = likeCount + 1;
                    layout.model.likes_count = [NSString stringWithFormat:@"%d",likeCount];
                }
            }else{
                layout.model.is_liked = @"0";
                if ([self isPureInt:layout.model.likes_count]) {
                    int likeCount = [layout.model.likes_count intValue];
                    likeCount = likeCount - 1;
                    layout.model.likes_count = [NSString stringWithFormat:@"%d",likeCount];
                }
            }
        }else if([_KCType isEqualToString:@"collect"]){
            if ([layout.model.is_collected isEqualToString:@"0"]) {
                layout.model.is_collected = @"1";
            }else{
                layout.model.is_collected = @"0";
            }
        }else if([_KCType isEqualToString:@"forward"]){
            if ([self isPureInt:layout.model.forwards_count]) {
                int likeCount = [layout.model.forwards_count intValue];
                likeCount = likeCount + 1;
                layout.model.forwards_count = [NSString stringWithFormat:@"%d",likeCount];
                 [self.hudView showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
            }
        }
        [self.myTable reloadData];
        return;
    }
}
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender{
    [self.hudView dismiss];
    [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
}
- (void)handlerError:(NSError *)error Tag:(NSString *)sender{
    [self.hudView dismiss];
    [self.hudView showErrorWithStatus:error.description];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
