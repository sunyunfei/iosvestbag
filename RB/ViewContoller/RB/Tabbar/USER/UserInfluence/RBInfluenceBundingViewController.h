//
//  RBInfluenceBundingViewController.h
//  RB
//
//  Created by AngusNi on 3/21/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInfoSocialTableViewCell.h"
#import "RBInfluenceViewController.h"
#import "TOWebViewController.h"
#import "RBTabUserViewController.h"
#import "RBInfluenceLoadingResultViewController.h"
#import "RBNewInfluenceViewController.h"
@interface RBInfluenceBundingViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate,RBActionSheetDelegate,RBAlertViewDelegate>
@property(nonatomic, strong) NSString*uploaded_contacts;

@end
