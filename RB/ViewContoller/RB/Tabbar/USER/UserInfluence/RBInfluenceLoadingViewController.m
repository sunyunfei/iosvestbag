//
//  RBInfluenceLoadingViewController.m
//  RB
//
//  Created by AngusNi on 5/30/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBInfluenceLoadingViewController.h"
#import "RBAppDelegate.h"
#import "InfluenceController.h"
#import "RBTabInfluenceController.h"
#import "RBInfluenceDetailViewController.h"

@interface RBInfluenceLoadingViewController ()
{
    NSString *userName;
    NSString *editString;
    NSMutableArray * datalist;
}
@end

@implementation RBInfluenceLoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    //
    UIImageView*bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, ScreenWidth, ScreenHeight - self.tabBarController.tabBar.height)];
    bgIV.image = [UIImage imageNamed:@"pic_influence_bg1"];
    bgIV.userInteractionEnabled = YES;
    [self.view addSubview:bgIV];
    UIView * vc = [[UIView alloc]initWithFrame:bgIV.bounds];
    vc.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.68];
    [self.view addSubview:vc];
    //
    [self loadEditView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-influence-loading"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-influence-loading"];
}
- (void)RBNotificationShowMessageView {

}
#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIImageView*logoIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-319.0/2.0)/2.0, (ScreenHeight-101.0/2.0)/2.0 - 49, 319.0/2.0, 101.0/2.0)];
    logoIV.image = [UIImage imageNamed:@"pic_influence_logo.png"];
    [self.view addSubview:logoIV];
    //
    UIImageView*textBgIV = [[UIImageView alloc]initWithFrame:CGRectMake(20.0, ScreenHeight-60.0-(398.0*(ScreenWidth-40.0))/668.0 - 49,ScreenWidth-40.0 , (398.0*(ScreenWidth-40.0))/668.0)];
    textBgIV.image = [UIImage imageNamed:@"pic_influence_text_bg.jpg"];
    textBgIV.userInteractionEnabled = YES;
    [self.view addSubview:textBgIV];
    float gap = (textBgIV.height - 28.0 - 28.0-48.0)/3.0;
    //
    UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, gap, textBgIV.width , 28.0)];
    tLabel.font = font_cu_(20.0);
    tLabel.textAlignment = NSTextAlignmentCenter;
    tLabel.text = NSLocalizedString(@"R5169",@"你,比想象中更有价值");
    tLabel.textColor = [Utils getUIColorWithHexString:@"ffffff"];
    [textBgIV addSubview:tLabel];
    //
    UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel.bottom+8.0, textBgIV.width , 20.0)];
    cLabel.font = font_15;
    cLabel.textAlignment = NSTextAlignmentCenter;
    if ([LocalService getRBIsIncheck] == YES) {
        cLabel.text = @"";
    }else{
        cLabel.text = NSLocalizedString(@"R5170",@"用您的社交账号赚钱");
    }
        cLabel.textColor = [Utils getUIColorWithHexString:@"939393"];
    [textBgIV addSubview:cLabel];
    //
    UIButton*textBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    textBtn.titleLabel.font = font_cu_15;
    textBtn.frame = CGRectMake(27.0*(ScreenWidth/375.0), cLabel.bottom+gap, textBgIV.width-27.0*(ScreenWidth/375.0)*2.0, 48.0);
    [textBtn addTarget:self
                action:@selector(textBtnAction:)
      forControlEvents:UIControlEventTouchUpInside];
    textBgIV.userInteractionEnabled = YES;
    self.view.userInteractionEnabled = YES;
    
    if ([LocalService getRBIsIncheck] == YES) {
        [textBtn setTitle:@"测试您的Robin8分数" forState:UIControlStateNormal];
    }else{
        [textBtn setTitle:NSLocalizedString(@"R5171",@"测试您的社交价值") forState:UIControlStateNormal];
    }
    [textBtn setTitleColor:[Utils getUIColorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    textBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
    [textBgIV addSubview:textBtn];
    //
//    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    footerBtn.titleLabel.font = font_cu_13;
//    footerBtn.frame = CGRectMake(0, ScreenHeight-60.0,ScreenWidth , 60.0);
//    [footerBtn addTarget:self
//                  action:@selector(footerBtnAction:)
//        forControlEvents:UIControlEventTouchUpInside];
//    [footerBtn setTitleColor:[Utils getUIColorWithHexString:@"ffffff"] forState:UIControlStateNormal];
//    [self.view addSubview:footerBtn];
    //
//    if ([LocalService getRBLocalDataUserPrivateToken]!=nil) {
//        [footerBtn setTitle:NSLocalizedString(@"R5172",@"游客 >") forState:UIControlStateNormal];
//    } else {
//        if([[LocalService getRBLocalDataUserLoginPhone] isEqualToString:@"13000000000"]) {
//            [footerBtn setTitle:NSLocalizedString(@"R5172",@"游客 >") forState:UIControlStateNormal];
//        } else {
//            [footerBtn setTitle:NSLocalizedString(@"R5173",@"跳过 >") forState:UIControlStateNormal];
//        }
//    }
}

#pragma mark - UIButton Delegate
- (void)textBtnAction:(UIButton *)sender {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoThirdAccountList];
    
//    if ([self.isBind isEqualToString:@"yes"]) {
//        RBAppDelegate * appdelegate = RBAPPDELEGATE;
//        [appdelegate loadViewController:33];
//    }else{
//        [self ThirdLoginWithTag:1];
//    }
}
//- (void)footerBtnAction:(UIButton *)sender {
//    [LocalService setRBLocalDataUserKolWith:nil];
//    if ([LocalService getRBLocalDataUserPrivateToken]!=nil) {
//        RBAppDelegate *appDelegate = RBAPPDELEGATE;
//        if ([LocalService getRBLocalDataUserLoginPhone]==nil) {
//            // loginPage
//            [appDelegate loadViewController:0];
//        } else {
//            // homePage
//            [appDelegate loadViewController:1];
//        }
//    } else {
//        // RB-游客登录
//        Handler*handler = [Handler shareHandler];
//        handler.delegate = self;
//        [handler getRBUserLoginWithPhone:@"13000000000" andCode:@"123456" andInvteCode:nil];
//    }
//}
- (void)ThirdLoginWithTag:(int)tag {
    SSDKPlatformType type;
    NSString*provider=@"";
    //    if (tag == 5002) {
    provider = @"weibo";
    type = SSDKPlatformTypeSinaWeibo;
    //    } else {
    //        provider = @"wechat";
    //        type = SSDKPlatformTypeWechat;
    //    }
    [ShareSDK cancelAuthorize:type];
    [ShareSDK getUserInfo:type
           onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error){
               NSLog(@"user:%@,credential:%@,rawData:%@",user,user.credential,user.rawData);
               if (state == SSDKResponseStateSuccess) {
                   userName = [NSString stringWithFormat:@"%@",user.nickname];
                   //screenView.tLabel.text = [NSString stringWithFormat:@"%@已绑定 %@",self.editString,userName];
                   // RB-社交账号绑定
                   Handler*handler = [Handler shareHandler];
                   //handler.delegate = self;
                   if (type == SSDKPlatformTypeSinaWeibo) {
                       editString = @"微博";
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*followers_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"followers_count"]];
                       NSString*created_at = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"created_at"]];
                       NSString*statuses_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"statuses_count"]];
                       NSString*verified = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"verified"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"avatar_large"]];
                       NSString*refresh_token = nil;
                       NSString*str = [NSString stringWithFormat:@"%@",user.credential];
                       if([[str componentsSeparatedByString:@"\"refresh_token\" = \""] count]>1) {
                           str = [str componentsSeparatedByString:@"\"refresh_token\" = \""][1];
                           if([[str componentsSeparatedByString:@"\";"] count]>1) {
                               refresh_token = [str componentsSeparatedByString:@"\";"][0];
                           }
                       }
                       //socialBtn.iconIV.image = [UIImage imageNamed:@"微博_ON.png"];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:followers_count andStatuses_count:statuses_count andRegistered_at:created_at andVerified:verified andRefresh_token:refresh_token andUnionid:nil andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                       [self.hudView show];
                       
                   }
               }
               if (state == SSDKResponseStateFail) {
                   if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                       [self.hudView showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                   } else {
                       [self.hudView showErrorWithStatus:NSLocalizedString(@"R1042", @"登录失败")];
                   }
                   RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
                   [appDelegate getRongIMConnect];
                   [appDelegate loadViewController:22];
               }
               if (state == SSDKResponseStateCancel) {
                   [self.hudView dismiss];
                   RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
                   [appDelegate getRongIMConnect];
                   [appDelegate loadViewController:22];
                   //[self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
               }
           }];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"kols/sign_in"]) {
        RBAppDelegate *appDelegate = RBAPPDELEGATE;
        [GeTuiSdk setPushModeForOff:NO];
        [JsonService setRBUserEntityWith:jsonObject];
        RBUserEntity*userEntity = [JsonService getRBUserEntity:jsonObject];
        [LocalService setRBLocalDataUserPrivateTokenWith:userEntity.issue_token];
        // homePage
        [appDelegate loadViewController:1];
    }
    if ([sender isEqualToString:@"kols/identity_bind"]) {
        [self.hudView dismiss];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBKOLApplyWithProvider:editString andHomepage:nil andUsername:userName andPrice:@"1" andFollowers:@"10" andScreenshot:nil andUid:nil];
        
        }
    if ([sender isEqualToString:@"update_social"]) {
        [self.hudView dismiss];
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:33];
//        [self.view removeFromSuperview];
        [LocalService setRBFirstInfluenceenter:@"yes"];
    }
    if ([sender isEqualToString:@"identities"]) {
        datalist = [NSMutableArray new];
        datalist = [JsonService getRBThirdListEntity:jsonObject andBackArray:datalist];
        [self.hudView show];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        //判断有没有绑定微信或者微博
        BOOL isBind = NO;
        for (RBThirdEntity * entity in datalist) {
            if ([entity.provider isEqualToString:@"weibo"]) {
                isBind = YES;
            }
        }
        if (isBind == YES) {
            RBAppDelegate * appdelegate = RBAPPDELEGATE;
            [appdelegate loadViewController:33];
//            [self.view removeFromSuperview];
            [LocalService setRBFirstInfluenceenter:@"yes"];
        }else{
            [self ThirdLoginWithTag:1];
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
