//
//  RBInfluenceLoadingResultViewController.m
//  RB
//
//  Created by AngusNi on 5/30/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBInfluenceLoadingResultViewController.h"
#import "RBTabUserViewController.h"
#import "RBAppDelegate.h"

@interface RBInfluenceLoadingResultViewController () {
    int pageIndex;
    int pageSize;
    UITableView*tableviewn;
    RBInfluenceEntity*influenceEntity;
    RBInfluenceView*influenceView;
    RBContactEntity*contactEntity;
    int contactIndex;
    NSString*uploaded_contacts;
}
@end

@implementation RBInfluenceLoadingResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5184", @"影响力排名");
    self.navRightTitle = Icon_bar_share;
    self.bgIV.hidden = NO;
    self.navView.backgroundColor = [UIColor clearColor];
    self.navLineLabel.hidden = YES;
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [self.hudView show];
    // RB-获取社交排名
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBInfluenceRankWithPage:@"1"];
    //
    [TalkingData trackPageBegin:@"my-influence-loading-result"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-influence-loading-result"];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth , 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2078", @"提升影响力") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)RBNavRightBtnAction {
    if([NSString stringWithFormat:@"%@",influenceEntity.influence_score].intValue==-1) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5064", @"未进行社交影响力评测")];
        return;
    }
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBInfluenceShare];
    //
    RBShareView *shareView = [RBShareView sharedRBShareView];
    [shareView showViewWithTitle:[NSString stringWithFormat:@"我在Robin8的影响力分数是%@,敢不敢和我PK一下?",influenceEntity.influence_score] Content:@"Robin8基于大数据的社交影响力平台" URL:[NSString stringWithFormat:@"%@invite?inviter_id=%@",ApiUrl,[LocalService getRBLocalDataUserLoginId]] ImgURL:@"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png"];
}

- (void)footerBtnAction:(UIButton *)sender {
    // RB-页面加载
    RBAppDelegate *appDelegate = RBAPPDELEGATE;
    if ([LocalService getRBLocalDataUserPrivateToken]!=nil) {
        if ([LocalService getRBLocalDataUserLoginPhone]==nil||[[LocalService getRBLocalDataUserLoginPhone] isEqualToString:@"13000000000"]) {
            // loginPage
            RBLoginViewController*toview = [[RBLoginViewController alloc]init];
            toview.isPush = YES;
            [self presentVC:toview];
        } else {
            // homePage
            [appDelegate loadViewController:1];
        }
    } else {
        // loginPage
        RBLoginViewController*toview = [[RBLoginViewController alloc]init];
        toview.isPush = YES;
        [self presentVC:toview];
    }
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    influenceView = [[RBInfluenceView alloc]initWithFrame:CGRectMake(0, NavHeight+35.0, ScreenWidth, 150)];
    [self.view addSubview:influenceView];
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, influenceView.bottom+15.0, self.view.width, (ScreenHeight-45.0-(influenceView.bottom+15.0)))
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
}

-(void)updateEditView {
    influenceView.rate = ([NSString stringWithFormat:@"%@",influenceEntity.influence_score].floatValue-350.0)/650.0; // 350 - 1000
    [influenceView.userIV sd_setImageWithURL:[NSURL URLWithString:influenceEntity.avatar_url] placeholderImage:PlaceHolderUserImage];
    influenceView.userLabel.text = [NSString stringWithFormat:@"%@",influenceEntity.influence_score];
    influenceView.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5054", @"第%@名"),influenceEntity.rank_index];
    influenceView.dateLabel.text = [NSString stringWithFormat:@"%@",influenceEntity.influence_level];
    [influenceView setNeedsDisplay];
    [UIView performWithoutAnimation:^{
        [tableviewn reloadData];
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [influenceEntity.contacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBInfluenceRankingTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBInfluenceRankingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                      reuseIdentifier:cellIdentifier];
    }
    [cell loadRBInfluenceRankingTableViewCellWith:influenceEntity.contacts andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    contactIndex = (int)indexPath.row;
    contactEntity = influenceEntity.contacts[indexPath.row];
    NSString*score = [NSString stringWithFormat:@"%@",contactEntity.influence_score];
    NSString*mobile = [Utils getNSStringPhoneNumber:[NSString stringWithFormat:@"%@",contactEntity.mobile]];
    if(score.length == 0) {
        if([NSString stringWithFormat:@"%@",mobile].length!= 11) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R5065", @"暂不支持此手机号码")];
            return;
        }
        RBAlertView *alertView = [[RBAlertView alloc]init];
        alertView.delegate = self;
        alertView.tag = 999;
        [alertView showWithArray:@[[NSString stringWithFormat:NSLocalizedString(@"R5066", @"确定邀请【%@】加入Robin8, 我们将给TA发送一条邀请短信"),contactEntity.name],NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
    }
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if(alertView.tag==999) {
        if(buttonIndex==0) {
            contactEntity.is_send = 1;
            [influenceEntity.contacts replaceObjectAtIndex:contactIndex withObject:contactEntity];
            [UIView performWithoutAnimation:^{
                [tableviewn reloadData];
            }];
            // RB-邀请朋友
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceSmsWithMobile:contactEntity.mobile];
        }
    }
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"influences/rank"]) {
        [self.hudView dismiss];
        influenceEntity = [JsonService getRBInfluenceEntity:jsonObject];
        [self updateEditView];
    }
    if ([sender isEqualToString:@"influences/send_invite"]) {
        
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

