//
//  RBInfluenceBundingViewController.m
//  RB
//
//  Created by AngusNi on 3/21/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBInfluenceBundingViewController.h"
#import "RBInfluenceViewController.h"

@interface RBInfluenceBundingViewController (){
    int pageIndex;
    int pageSize;
    UITableView*tableviewn;
    NSMutableArray*datalist;
    NSString*uid;
    NSString*provide;
    UILabel*messageLabel;
    UIView*loginView;
    NSMutableArray*array;
    NSMutableArray*array1;
    NSMutableArray*array2;
    NSMutableArray*array3;
    NSMutableArray*array4;
    int uploadCount;
}
@end

@implementation RBInfluenceBundingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5037", @"影响力评测");
    self.bgIV.hidden = NO;
    self.navView.backgroundColor = [UIColor clearColor];
    self.navLineLabel.hidden = YES;
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight+20.0, self.view.width, (ScreenHeight-110.0-85.0-NavHeight+45.0))
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
    //
    [self loadFooterView];
    // RB-开始测试
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBInfluence];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-influence-bunding"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-influence-bunding"];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    if (messageLabel==nil) {
        messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NavHeight+(ScreenHeight-NavHeight-136.0-85.0-25.0-30.0)/2.0, ScreenWidth, 25.0)];
        messageLabel.font =  font_cu_17;
        messageLabel.text = NSLocalizedString(@"R5038", @"绑定社交账号,精准评测");
        messageLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        messageLabel.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:messageLabel];
    }
    if (loginView==nil) {
        loginView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 85.0)];
        loginView.userInteractionEnabled = YES;
        [self.view addSubview:loginView];
        //
        UILabel *thirdLabel = [[UILabel alloc]
                               initWithFrame:CGRectMake(0, 0, ScreenWidth, 18.0)];
        thirdLabel.font = font_11;
        thirdLabel.textAlignment = NSTextAlignmentCenter;
        thirdLabel.text = NSLocalizedString(@"R5039", @"添加更多社交账号提升影响力");
        thirdLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        [loginView addSubview:thirdLabel];
        //
        UIButton *weixinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        weixinBtn.frame = CGRectMake((self.view.width - 50.0 * 3.0 - 50.0) / 3.0, thirdLabel.bottom + 15.0, 50, 50);
        [weixinBtn addTarget:self
                      action:@selector(weixinBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        [weixinBtn setBackgroundImage:[UIImage imageNamed:@"icon_wechat.png"] forState:UIControlStateNormal];
        [loginView addSubview:weixinBtn];
        //
        UIButton *weiboBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        weiboBtn.frame = CGRectMake(weixinBtn.right + 50.0, weixinBtn.top, weixinBtn.width,weixinBtn.height);
        [weiboBtn addTarget:self
                     action:@selector(weiboBtnAction:)
           forControlEvents:UIControlEventTouchUpInside];
        [weiboBtn setBackgroundImage:[UIImage imageNamed:@"icon_weibo.png"] forState:UIControlStateNormal];
        [loginView addSubview:weiboBtn];
        //
        UIButton *qqBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        qqBtn.frame = CGRectMake(weiboBtn.right + 50.0, weixinBtn.top, weixinBtn.width,weixinBtn.height);
        [qqBtn addTarget:self
                  action:@selector(qqBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
        [qqBtn setBackgroundImage:[UIImage imageNamed:@"icon_qq.png"] forState:UIControlStateNormal];
        [loginView addSubview:qqBtn];
    }
    loginView.top = messageLabel.bottom + 30.0;
    //
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-110.0,ScreenWidth , 110.0)];
    footerView.userInteractionEnabled = YES;
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(42.0*(ScreenWidth/375.0),0, ScreenWidth-42.0*(ScreenWidth/375.0)*2.0, 48.0);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R5040", @"开始评测") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
    //
    TTTAttributedLabel *aboutLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(0, footerBtn.bottom+10.0,ScreenWidth, 18)];
    aboutLabel.font = font_11;
    aboutLabel.textAlignment = NSTextAlignmentCenter;
    aboutLabel.backgroundColor = [UIColor clearColor];
    aboutLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    aboutLabel.text = NSLocalizedString(@"R5041", @"*点击开始评测即同意用户服务协议");
    [footerView addSubview:aboutLabel];
    [aboutLabel setText:aboutLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:NSLocalizedString(@"R1015", @"用户服务协议") options:NSCaseInsensitiveSearch];
         [mStr
          addAttribute:(NSString *)kCTForegroundColorAttributeName
          value:(id)[[Utils getUIColorWithHexString:SysColorBlue] CGColor]
          range:range];
         return mStr;
     }];
    //
    UIButton *aboutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    aboutBtn.frame = aboutLabel.frame;
    [aboutBtn addTarget:self
                 action:@selector(aboutBtnAction:)
       forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:aboutBtn];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    if([datalist count]==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5042", @"请先绑定社交账号")];
        return;
    }
    // 获取通讯录
    [self getAddressBookInfo];
}

- (void)aboutBtnAction:(UIButton *)sender {
    TOWebViewController *toview = [[TOWebViewController alloc] init];
    toview.url = [NSURL URLWithString:@"http://7xuw3n.com2.z0.glb.qiniucdn.com/Robin8_Service.html"];
    toview.title = NSLocalizedString(@"R1015", @"用户服务协议");
    toview.showPageTitles = NO;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBInfoSocialTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBInfoSocialTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:cellIdentifier];
    }
    [cell loadRBInfoSocialCellWith:datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RBThirdEntity*entity = datalist[indexPath.row];
    uid = [NSString stringWithFormat:@"%@",entity.uid];
    provide = [NSString stringWithFormat:@"%@",entity.provider];
    if (uid.length!=0) {
        RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
        actionSheet.delegate = self;
        [actionSheet showWithArray:@[NSLocalizedString(@"R5043", @"解除绑定"),NSLocalizedString(@"R1011", @"取消")]];
    }
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if (buttonIndex == 0) {
        // RB-获取社交账号解除绑定
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBInfluenceThirdAccountUnbind:provide andUid:uid];
    }
}

- (void)getAddressBookInfo {
    // 获取本机通讯录信息
    CFErrorRef *error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    __block BOOL accessGranted = NO;
    if (&ABAddressBookRequestAccessWithCompletion != NULL) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    if (accessGranted == NO) {
        RBAlertView *alertView = [[RBAlertView alloc]init];
        alertView.delegate = self;
        alertView.tag = 1000;
        [alertView showWithArray:@[NSLocalizedString(@"R5044", @"是否允许ROBIN8\n获取您的手机通讯录？"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
        return;
    } else {
        array = [NSMutableArray new];
        array1 = [NSMutableArray new];
        array2 = [NSMutableArray new];
        array3 = [NSMutableArray new];
        array4 = [NSMutableArray new];
        NSArray*listContacts = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
        for (int i=0; i<[listContacts count]; i++) {
            ABRecordRef thisPerson = CFBridgingRetain(listContacts[i]);
            NSString *firstName = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonFirstNameProperty));
            NSString *lastName = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonLastNameProperty));
            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(thisPerson, kABPersonPhoneProperty);
            NSArray* phoneNumberArray = CFBridgingRelease(ABMultiValueCopyArrayOfAllValues(phoneNumberProperty));
            for(int index = 0; index<[phoneNumberArray count]; index++) {
                NSMutableDictionary*diclist = [NSMutableDictionary new];
                NSString*fName = @"";
                NSString*sName = @"";
                if (firstName!=nil&&![firstName isEqual:[NSNull null]]&&firstName.length!=0) {
                    fName = firstName;
                }
                if (lastName!=nil&&![lastName isEqual:[NSNull null]]&&lastName.length!=0) {
                    sName = lastName;
                }
                NSString*mobile = [NSString stringWithFormat:@"%@",phoneNumberArray[index]];
                mobile = [mobile stringByReplacingOccurrencesOfString:@" (" withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@") " withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"+86" withString:@""];
                if (mobile.length==11 && [Utils getIsValidateNumber:mobile]) {
                    [diclist setObject:[NSString stringWithFormat:@"%@%@",sName,fName] forKey:@"name"];
                    [diclist setObject:[NSString stringWithFormat:@"%@",mobile] forKey:@"mobile"];
                    if([array count]<2000) {
                        [array addObject:diclist];
                    } else {
                        if ([array1 count]<2000) {
                            [array1 addObject:diclist];
                        } else {
                            if ([array2 count]<2000) {
                                [array2 addObject:diclist];
                            } else {
                                if ([array3 count]<2000) {
                                    [array3 addObject:diclist];
                                } else {
                                    [array4 addObject:diclist];
                                }
                            }
                        }
                    }
                }
            }
        }
//        // TEST DATA
//        for(int index = 1000; index< 9999; index++) {
//            NSMutableDictionary*diclist = [NSMutableDictionary new];
//            [diclist setObject:[NSString stringWithFormat:@"同事%d",index] forKey:@"name"];
//            [diclist setObject:[NSString stringWithFormat:@"1381290%d",index] forKey:@"mobile"];
//            if([array count]<2000) {
//                [array addObject:diclist];
//            } else {
//                if ([array1 count]<2000) {
//                    [array1 addObject:diclist];
//                } else {
//                    if ([array2 count]<2000) {
//                        [array2 addObject:diclist];
//                    } else {
//                        if ([array3 count]<2000) {
//                            [array3 addObject:diclist];
//                        } else {
//                            [array4 addObject:diclist];
//                        }
//                    }
//                }
//            }
//        }
        if ([array count]!=0) {
            uploadCount = (int)[array count];
            [self.hudView show];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array];
        } else {
            [self getTestResult];
        }
    }
}

- (void)getTestResult {
    [self.hudView show];
    // RB-获取社交测试结果
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBInfluenceInResultWithCity:[LocalService getRBLocalDataUserCityName]];
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if(alertView.tag==1000) {
        if (buttonIndex==0) {
            // IOS8以上系统可直接跳转到本应用的设置界面
            if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
        }
        if (buttonIndex==1) {
            [self getTestResult];
        }
    }
}

#pragma mark - UIButton Delegate
- (void)weiboBtnAction:(UIButton *)sender {
    [self ThirdLoginWithTag:5000];
}

- (void)weixinBtnAction:(UIButton *)sender {
    [self ThirdLoginWithTag:5001];
}

- (void)qqBtnAction:(UIButton *)sender {
    [self ThirdLoginWithTag:5002];
}

- (void)ThirdLoginWithTag:(int)tag {
    SSDKPlatformType type;
    NSString*provider=@"";
    if (tag == 5000) {
        provider = @"weibo";
        type = SSDKPlatformTypeSinaWeibo;
    } else if (tag == 5001) {
        provider = @"wechat";
        type = SSDKPlatformTypeWechat;
    } else {
        provider = @"qq";
        type = SSDKPlatformTypeQQ;
    }
    [ShareSDK cancelAuthorize:type];
    [ShareSDK getUserInfo:type
           onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error){
               NSLog(@"user.rawData:%@",user.rawData);
               if (state == SSDKResponseStateSuccess) {
                   [self.hudView show];
                   // RB-社交账号绑定
                   Handler*handler = [Handler shareHandler];
                   handler.delegate = self;
                   if (type == SSDKPlatformTypeWechat) {
                       NSString*unionid = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"unionid"]];
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       [handler getRBInfluenceThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:unionid andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                   }
                   if (type == SSDKPlatformTypeSinaWeibo) {
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*followers_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"followers_count"]];
                       NSString*created_at = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"created_at"]];
                       NSString*statuses_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"statuses_count"]];
                       NSString*verified = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"verified"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"avatar_large"]];
                       NSString*refresh_token = nil;
                       NSString*str = [NSString stringWithFormat:@"%@",user.credential];
                       if([[str componentsSeparatedByString:@"\"refresh_token\" = \""] count]>1) {
                           str = [str componentsSeparatedByString:@"\"refresh_token\" = \""][1];
                           if([[str componentsSeparatedByString:@"\";"] count]>1) {
                               refresh_token = [str componentsSeparatedByString:@"\";"][0];
                           }
                       }
                       [handler getRBInfluenceThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:followers_count andStatuses_count:statuses_count andRegistered_at:created_at andVerified:verified andRefresh_token:refresh_token andUnionid:nil andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                   }
                   if (type == SSDKPlatformTypeQQ) {
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*city = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"city"]];
                       NSString*is_yellow_vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"is_yellow_vip"]];
                       NSString*vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"vip"]];
                       NSString*province = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"province"]];
                       NSString*gender = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"gender"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"figureurl_qq_2"]];
                       [handler getRBInfluenceThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:nil andProvince:province andCity:city andGender:gender andIs_vip:vip andIs_yellow_vip:is_yellow_vip];
                   }
               }
               if (state == SSDKResponseStateFail) {
                   [self.hudView showErrorWithStatus:NSLocalizedString(@"R1042", @"登录失败")];
               }
               if (state == SSDKResponseStateCancel) {
                   [self.hudView dismiss];
                   //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
               }
           }];
}

-(void)updateView:(id)jsonObject {
    datalist = [NSMutableArray new];
    datalist = [JsonService getRBThirdListEntity:jsonObject andBackArray:datalist];
    if ([datalist count]==0) {
        messageLabel.hidden = NO;
        loginView.top = messageLabel.bottom + 30.0;
    } else {
        messageLabel.hidden = YES;
        loginView.top = ScreenHeight -110.0 - 85.0 - 25.0;
    }
    [UIView performWithoutAnimation:^{
        [tableviewn reloadData];
    }];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"cal_score"]) {
        BOOL isLoading = YES;
        for (UIViewController *temp in self.navigationController.viewControllers){
            if ([temp isKindOfClass:[RBInfluenceViewController class]]) {
                if ([self.navigationController.viewControllers count]>1) {
                    [self.navigationController popViewControllerAnimated:YES];
                    return;
                }
            }
            if ([temp isKindOfClass:[RBTabUserViewController class]]) {
                isLoading = NO;
            }
        }
        if(isLoading){
            RBInfluenceLoadingResultViewController*toview = [[RBInfluenceLoadingResultViewController alloc]init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        } else {
            RBNewInfluenceViewController *toview = [[RBNewInfluenceViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
//            RBInfluenceViewController*toview = [[RBInfluenceViewController alloc]init];
//            toview.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:toview animated:YES];
        }
    }
    
    if ([sender isEqualToString:@"influences/start"]) {
        [LocalService setRBLocalDataUserKolWith:[jsonObject objectForKey:@"kol_uuid"]];
        self.uploaded_contacts = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"uploaded_contacts"]];
        [self updateView:jsonObject];
    }
    
    if ([sender isEqualToString:@"influences/identity_bind"]) {
        [self.hudView dismiss];
        [self updateView:jsonObject];
    }
    
    if ([sender isEqualToString:@"influences/bind_contacts"]) {
        if(uploadCount==2000&&[array1 count]!=0) {
            uploadCount = uploadCount+(int)[array1 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array1];
            return;
        }
        if(uploadCount==4000&&[array2 count]!=0) {
            uploadCount = uploadCount+(int)[array2 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array2];
            return;
        }
        if(uploadCount==6000&&[array3 count]!=0) {
            uploadCount = uploadCount+(int)[array3 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array3];
            return;
        }
        if(uploadCount==8000&&[array4 count]!=0) {
            uploadCount = uploadCount+(int)[array4 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array4];
            return;
        }
        [self getTestResult];
    }
    
    if ([sender isEqualToString:@"influences/unbind_identity"]) {
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R3007", @"解除绑定成功")];
        [self updateView:jsonObject];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
        [self updateView:jsonObject];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

