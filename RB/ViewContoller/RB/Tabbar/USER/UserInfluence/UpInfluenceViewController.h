//
//  UpInfluenceViewController.h
//  RB
//
//  Created by RB8 on 2017/8/2.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInfluenceView.h"
#import "RBNewInfluenceEntity.h"
@interface UpInfluenceViewController : RBBaseViewController<RBBaseVCDelegate,UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)RBNewInfluenceEntity * Entity;
@end
