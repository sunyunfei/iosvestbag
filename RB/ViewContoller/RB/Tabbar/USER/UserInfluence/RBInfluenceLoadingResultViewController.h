//
//  RBInfluenceLoadingResultViewController.h
//  RB
//
//  Created by AngusNi on 5/30/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInfluenceView.h"
#import "RBInfluenceRankingTableViewCell.h"
#import "RBInfluenceBundingViewController.h"
#import "RBInfluenceReadViewController.h"
#import "RBInfluenceIncreaseViewController.h"
#import "RBLoginViewController.h"
@interface RBInfluenceLoadingResultViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate,RBAlertViewDelegate>

@end