//
//  upInfluenceCell.m
//  RB
//
//  Created by RB8 on 2017/8/2.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "upInfluenceCell.h"
#import "Service.h"
@implementation upInfluenceCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //
        _leftImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 22, 38, 38)];
        
        [self.contentView addSubview:_leftImageView];
        //
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(_leftImageView.right+14, _leftImageView.top, 200, 15)];
        _titleLabel.textColor = [Utils getUIColorWithHexString:ccColor212121];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.font = font_cu_13;
        [self.contentView addSubview:_titleLabel];
        //
        _describeLabel = [[UILabel alloc]initWithFrame:CGRectMake(_titleLabel.left, _titleLabel.bottom+8, ScreenWidth - _leftImageView.right-14-60-15, 10)];
        _describeLabel.textColor = [Utils getUIColorWithHexString:ccColor969696];
        _describeLabel.textAlignment = NSTextAlignmentLeft;
        _describeLabel.font = font_12;
        _describeLabel.numberOfLines = 0;
        [self.contentView addSubview:_describeLabel];
        //
        _clickBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _clickBtn.frame = CGRectMake(ScreenWidth-60-15, 28, 60, 25);
        _clickBtn.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
        [_clickBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
        [_clickBtn addTarget:self action:@selector(pressBtn:) forControlEvents:UIControlEventTouchUpInside];
        _clickBtn.layer.cornerRadius = 25/2;
        _clickBtn.titleLabel.font = font_cu_12;
        [self.contentView addSubview:_clickBtn];
        //
        UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 80, ScreenWidth, 1)];
        lineView.backgroundColor = [Utils getUIColorWithHexString:ccColora4a4a4];
        lineView.alpha = 0.14;
        [self.contentView addSubview:lineView];
    }
    return self;
}
//点击button触发的方法
-(void)pressBtn:(UIButton*)sender{
    _clickBlock(sender.titleLabel.text);
}
-(void)setValueForCell:(NSArray*)arr{
    _leftImageView.image = [UIImage imageNamed:arr[0]];
    _titleLabel.text = arr[1];
    _describeLabel.text = arr[2];
    [_describeLabel sizeToFit];
    _describeLabel.frame = CGRectMake(_titleLabel.left, _titleLabel.bottom+8, ScreenWidth - _leftImageView.right-14-60-15, _describeLabel.size.height);
    [_clickBtn setTitle:arr[3] forState:UIControlStateNormal];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
