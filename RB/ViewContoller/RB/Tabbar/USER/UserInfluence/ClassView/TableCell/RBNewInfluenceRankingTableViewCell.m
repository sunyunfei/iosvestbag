//
//  RBNewInfluenceRankingTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBNewInfluenceRankingTableViewCell.h"
#import "Service.h"


@implementation RBNewInfluenceRankingTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UILabel*numLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        numLabel.font = font_30;
        numLabel.textAlignment = NSTextAlignmentCenter;
        numLabel.textColor = [Utils getUIColorWithHexString:SysColorBlue];
        numLabel.tag = 1005;
        [self.contentView addSubview:numLabel];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1001;
        [self.contentView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel.font = font_11;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        cLabel.tag = 1002;
        [self.contentView addSubview:cLabel];
        //
        UILabel*scoreLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        scoreLabel.tag = 1003;
        scoreLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:scoreLabel];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBarLine];
        lineLabel.tag = 1010;
        [self.contentView addSubview:lineLabel];
    }
    return self;
}

- (instancetype)loadRBNewInfluenceRankingTableViewCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1001];
    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1002];
    UILabel*scoreLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*numLabel=(UILabel*)[self.contentView viewWithTag:1005];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    //
    RBContactEntity*entity = datalist[indexPath.row];
    numLabel.text = [NSString stringWithFormat:@"%d",(int)indexPath.row+1];
    numLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    numLabel.hidden = YES;
    tLabel.text = entity.name;
    NSString*mobile = [NSString stringWithFormat:@"%@",entity.mobile];
    if (mobile.length!=0&&mobile.length>7) {
        cLabel.text = [NSString stringWithFormat:@"%@****%@",[mobile substringWithRange:NSMakeRange(0,3)],[mobile substringWithRange:NSMakeRange(mobile.length-4,4)]];
    } else if (mobile.length!=0&&mobile.length>4) {
        cLabel.text = [NSString stringWithFormat:@"%@****",[mobile substringWithRange:NSMakeRange(0,3)]];
    }
    scoreLabel.textColor = [Utils getUIColorWithHexString:SysColorBlue];
    scoreLabel.text = @"＋邀请加入";
    scoreLabel.font = font_13;
    NSString*score = [NSString stringWithFormat:@"%@",entity.influence_score];
    if(score.length==0){
        // 未加入
        if ([NSString stringWithFormat:@"%@",entity.invite_status].intValue==0) {
            scoreLabel.textColor = [Utils getUIColorWithHexString:SysColorBlue];
            scoreLabel.text = @"＋邀请加入";
            scoreLabel.font = font_13;
        } else {
            scoreLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            scoreLabel.text = @"已邀请";
            scoreLabel.font = font_13;
        }
        if (entity.is_send==1) {
            scoreLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            scoreLabel.text = @"已邀请";
            scoreLabel.font = font_13;
        }
    } else {
        if(score.floatValue<0) {
            // 未评分
            scoreLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            scoreLabel.text = NSLocalizedString(@"R5036", @"未评测");
            scoreLabel.font = font_13;
        } else {
            // 已评分
            scoreLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
            scoreLabel.text = [NSString stringWithFormat:@"%@",entity.influence_score];
            scoreLabel.font = font_(20.0);
            if (indexPath.row<3) {
                numLabel.textColor = [Utils getUIColorWithHexString:SysColorBlue];
            }
            numLabel.hidden = NO;
        }
    }
    numLabel.frame = CGRectMake(0, 0, 40.0, 75.0);
    tLabel.frame = CGRectMake(numLabel.right, 20.0, ScreenWidth, 18.0);
    cLabel.frame = CGRectMake(tLabel.left, tLabel.bottom+5.0, ScreenWidth, 15.0);
    scoreLabel.frame = CGRectMake(0, 0, ScreenWidth-20.0, 75.0);
    lineLabel.frame = CGRectMake(tLabel.left, 75.0-0.5f, ScreenWidth-tLabel.left, 0.5f);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, 75.0);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
