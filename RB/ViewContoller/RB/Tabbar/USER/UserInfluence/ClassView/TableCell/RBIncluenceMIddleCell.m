//
//  RBIncluenceMIddleCell.m
//  RB
//
//  Created by RB8 on 2017/8/9.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBIncluenceMIddleCell.h"
#import "Service.h"
@implementation RBIncluenceMIddleCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.leftImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 20, 26, 26)];
        self.leftImageView.image = [UIImage imageNamed:@"RBMotherIcon"];
        [self.contentView addSubview:self.leftImageView];
        //
        self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.leftImageView.right+15, 27, 40, 15) text:@"母婴" font:font_15 textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:ccColor1a1a1a] backgroundColor:[UIColor clearColor] numberOfLines:1];
        [self.contentView addSubview:self.nameLabel];
        //
        self.describeLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 100, 27, 85, 20) text:@"在微博好友中NO.2" font:font_11 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:ccColor414141] backgroundColor:[UIColor clearColor] numberOfLines:1];
        [self.describeLabel sizeToFit];
        self.describeLabel.frame = CGRectMake(ScreenWidth - 15-self.describeLabel.size.width, 27, self.describeLabel.size.width, self.describeLabel.size.height);
        [self.contentView addSubview:self.describeLabel];
        //
        self.rightImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.describeLabel.left-15,self.describeLabel.top, 15, 15)];
        self.rightImageView.backgroundColor = [UIColor yellowColor];
        [self.contentView addSubview:self.rightImageView];
        //
        self.percentLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.leftImageView.left,self.nameLabel.bottom+15, 50, 16) text:@"%60" font:font_17 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:ccColor1a1a1a] backgroundColor:[UIColor clearColor] numberOfLines:1];
        [self.contentView addSubview:self.percentLabel];
        //
        self.progressView = [[JGProgressView alloc]initWithFrame:CGRectMake(self.percentLabel.right+12, self.nameLabel.bottom+21, ScreenWidth - 2*(self.percentLabel.right+12), 10)];
        self.progressView.circle = @"circle";
        self.progressView.Tcolor = [Utils getUIColorWithHexString:@"f76262"];
        self.progressView.Ccolor = [Utils getUIColorWithHexString:ccColore4e4e4];
        self.progressView.direction = @"left";
        self.progressView.progress = 0.5;
        [self.contentView addSubview:self.progressView];
        //
        self.bottomView = [[[NSBundle mainBundle]loadNibNamed:@"RBMiddleSmallView" owner:nil options:nil]lastObject];
        self.bottomView.frame = CGRectMake(0, self.progressView.bottom+10, ScreenWidth, 61);
        [self.contentView addSubview:self.bottomView];
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
