//
//  upInfluenceCell.h
//  RB
//
//  Created by RB8 on 2017/8/2.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface upInfluenceCell : UITableViewCell
@property(nonatomic,strong)UIImageView * leftImageView;
@property(nonatomic,strong)UILabel * titleLabel;
@property(nonatomic,strong)UILabel * describeLabel;
@property(nonatomic,strong)UIButton * clickBtn;
@property(nonatomic,copy) void(^clickBlock)(NSString * text);
-(void)setValueForCell:(NSArray*)arr;
@end
