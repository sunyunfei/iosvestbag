//
//  RBInfluenceRankingTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBInfluenceRankingTableViewCell.h"
#import "constants.h"


@implementation RBInfluenceRankingTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.tag=1000;
        [self.contentView addSubview:bgView];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag=1001;
        [self.contentView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel.font = font_11;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        cLabel.tag=1002;
        [self.contentView addSubview:cLabel];
        //
        self.actionBtn = [RBButton buttonWithType:UIButtonTypeCustom];
        self.actionBtn.tag = 1003;
        [self.contentView addSubview:self.actionBtn];
        //
        TTTAttributedLabel*btnLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        btnLabel.textAlignment = NSTextAlignmentRight;
        btnLabel.tag=1004;
        [self.contentView addSubview:btnLabel];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBarLine];
        lineLabel.tag=1010;
        [self.contentView addSubview:lineLabel];
    }
    return self;
}

- (instancetype)loadRBInfluenceRankingTableViewCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1001];
    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1002];
    TTTAttributedLabel*btnLabel=(TTTAttributedLabel*)[self.contentView viewWithTag:1004];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    //
    RBContactEntity*entity = datalist[indexPath.row];
    tLabel.text = entity.name;
    NSString*mobile = [NSString stringWithFormat:@"%@",entity.mobile];
    if (mobile.length!=0&&mobile.length>7) {
        cLabel.text = [NSString stringWithFormat:@"%@****%@",[mobile substringWithRange:NSMakeRange(0,3)],[mobile substringWithRange:NSMakeRange(mobile.length-4,4)]];
    } else if (mobile.length!=0&&mobile.length>4) {
        cLabel.text = [NSString stringWithFormat:@"%@****",[mobile substringWithRange:NSMakeRange(0,3)]];
    }
    NSString*score = [NSString stringWithFormat:@"%@",entity.influence_score];
    if(score.length==0){
        // 未加入robin8
        btnLabel.text = @"";
        if ([NSString stringWithFormat:@"%@",entity.invite_status].intValue==0) {
            [self.actionBtn setBackgroundImage:[UIImage imageNamed:@"icon_influence_invitation_h.png"] forState:UIControlStateNormal];
        } else {
            [self.actionBtn setBackgroundImage:[UIImage imageNamed:@"icon_influence_invitation_l.png"] forState:UIControlStateNormal];
        }
        if (entity.is_send==1) {
            [self.actionBtn setBackgroundImage:[UIImage imageNamed:@"icon_influence_invitation_l.png"] forState:UIControlStateNormal];
        }
    }
    if(score.floatValue<0) {
        // 未评分
        btnLabel.font = font_11;
        btnLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        btnLabel.text = NSLocalizedString(@"R5036", @"未评测");
    }
    if(score.floatValue>0) {
        // 已评分
        btnLabel.font = font_(20);
        btnLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
        btnLabel.text = [NSString stringWithFormat:@"%@",entity.influence_score];
    }
    self.actionBtn.typeStr = [NSString stringWithFormat:@"%d",(int)indexPath.row];
    //
    tLabel.frame = CGRectMake(35.0, 10.0, ScreenWidth, 20.0);
    cLabel.frame = CGRectMake(tLabel.left, tLabel.bottom, ScreenWidth, 20.0);
    btnLabel.frame = CGRectMake(0, 0, ScreenWidth-35.0, 60.0);
    self.actionBtn.frame = CGRectMake(ScreenWidth-25.0-45.0, (60.0-45.0)/2.0, 45.0, 45.0);
    lineLabel.frame = CGRectMake(tLabel.left, 60.0-0.5f, ScreenWidth-tLabel.left*2.0, 0.5f);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, 60.0);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
