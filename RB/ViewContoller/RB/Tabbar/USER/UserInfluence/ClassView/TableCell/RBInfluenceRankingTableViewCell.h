//
//  RBInfluenceRankingTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "RBButton.h"

@interface RBInfluenceRankingTableViewCell : UITableViewCell
@property (nonatomic, strong) RBButton*actionBtn;
- (instancetype)loadRBInfluenceRankingTableViewCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
@end
