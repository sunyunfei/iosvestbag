//
//  RBIncluenceMIddleCell.h
//  RB
//
//  Created by RB8 on 2017/8/9.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JGProgressView.h"
#import "RBMiddleSmallView.h"
@interface RBIncluenceMIddleCell : UITableViewCell
//
@property(nonatomic,strong)UIImageView * leftImageView;
//
@property(nonatomic,strong)UILabel * nameLabel;
//
@property(nonatomic,strong)UIImageView * rightImageView;
//
@property(nonatomic,strong)UILabel * describeLabel;
//
@property(nonatomic,strong)UILabel * percentLabel;
//
@property(nonatomic,strong)JGProgressView * progressView;
//
@property(nonatomic,strong)UIButton * pinButton;
//
@property(nonatomic,strong)RBMiddleSmallView * bottomView;
@end
