//
//  RBWeiboSmallView.h
//  RB
//
//  Created by RB8 on 2017/8/9.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JGProgressView.h"
#import "RBMiddleSmallView.h"
#import "RBNewIndustriyEntity.h"
@protocol changFrame <NSObject>
-(void)changeViewFrame:(BOOL)Isfold;
@end
@interface RBWeiboSmallView : UIView
//
@property(nonatomic,strong)UIImageView * leftImageView;
//
@property(nonatomic,strong)UILabel * nameLabel;
//
@property(nonatomic,strong)UIImageView * rightImageView;
//
@property(nonatomic,strong)UILabel * describeLabel;
//
@property(nonatomic,strong)UILabel * percentLabel;
//
@property(nonatomic,strong)JGProgressView * progressView;
//
@property(nonatomic,strong)UIButton * pinButton;
//
@property(nonatomic,strong)UIImageView * pinImageVIew;
//

@property(nonatomic,strong)RBMiddleSmallView * bottomView;
@property(nonatomic,copy)void(^foldBlock)(void);
@property(nonatomic,assign)BOOL IsFold;
//
@property(nonatomic,strong)RBNewIndustriyEntity * entity;
@property(nonatomic,weak)id<changFrame>delegate;
-(void)setfoldOrUnFold:(BOOL)IsFold;
-(void)setBotoomViewFold;

@end
