//
//  RBWeiBoBigView.m
//  RB
//
//  Created by RB8 on 2017/8/9.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBWeiBoBigView.h"
#import "Service.h"
@implementation RBWeiBoBigView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.headView = [[[NSBundle mainBundle]loadNibNamed:@"RBinfluenceHeadView" owner:nil options:nil]lastObject];
        self.headView.delegate = self;
        [self addSubview:self.headView];
        UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, self.headView.bottom, ScreenWidth, 0.5)];
        lineView.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
        lineView.alpha = 0.4;
        [self addSubview:lineView];
        //
        self.smallView1 = [[RBWeiboSmallView alloc]initWithFrame:CGRectMake(0, lineView.bottom, ScreenWidth, 144)];
        self.smallView1.delegate = self;
        self.smallView1.bottomView.hidden = NO;
        self.smallView1.IsFold = NO;
        self.smallView1.progressView.Tcolor = [Utils getUIColorWithHexString:@"f76262"];
        self.smallView1.progressView.Ccolor = [Utils getUIColorWithHexString:ccColore4e4e4];
//        self.smallView1.nameLabel.text = @"母婴";
        self.smallView1.leftImageView.image = [UIImage imageNamed:@"RBMotherIcon"];
        [self addSubview:self.smallView1];
        //
        self.smallView2 = [[RBWeiboSmallView alloc]initWithFrame:CGRectMake(0, self.smallView1.bottom, ScreenWidth, 144-61)];
        self.smallView2.delegate = self;
//        self.smallView2.nameLabel.text = @"美食";
        self.smallView2.progressView.Tcolor = [Utils getUIColorWithHexString:ccColorfebc3b];
        self.smallView2.progressView.Ccolor = [Utils getUIColorWithHexString:ccColore4e4e4];
        self.smallView2.leftImageView.image = [UIImage imageNamed:@"RBFoodIcon"];
        [self addSubview:self.smallView2];
        //
        self.smallView3 = [[RBWeiboSmallView alloc]initWithFrame:CGRectMake(0, self.smallView2.bottom, ScreenWidth, 144-61)];
        self.smallView3.delegate = self;
//        self.smallView3.nameLabel.text = @"摄影";
        self.smallView3.progressView.Tcolor = [Utils getUIColorWithHexString:SysBackColorBlue];
        self.smallView3.progressView.Ccolor = [Utils getUIColorWithHexString:ccColore4e4e4];
        self.smallView3.leftImageView.image = [UIImage imageNamed:@"RBCameraIcon"];
        [self addSubview:self.smallView3];
        //
        self.BottomView = [[UIView alloc]initWithFrame:CGRectMake(0, self.smallView3.bottom, ScreenWidth, 28)];
        self.BottomView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.BottomView];
        //
        self.noenoughView = [[[NSBundle mainBundle]loadNibNamed:@"RBEnoughView" owner:nil options:nil] lastObject];
        self.noenoughView.frame = CGRectMake(0, lineView.bottom,ScreenWidth , 3*144-3*61 +28);
        self.noenoughView.hidden = YES;
        [self addSubview:self.noenoughView];

    }
    return self;
}
-(void)changeViewFrame:(BOOL)Isfold{
    [self updateConstraints];
    if (Isfold == YES) {
        if ([self.delegate respondsToSelector:@selector(changeWeiboFrame:)]) {
            [self.delegate changeWeiboFrame:YES];
        }
    }else if (Isfold == NO){
        if ([self.delegate respondsToSelector:@selector(changeWeiboFrame:)]) {
            [self.delegate changeWeiboFrame:NO];
        }
    }
}
-(void)updateConstraints{
    [super updateConstraints];
    self.headView.frame = CGRectMake(0, 0, ScreenWidth, 163);
    if (self.smallView1.IsFold == YES) {
        self.smallView1.frame = CGRectMake(0, self.headView.bottom+0.5, ScreenWidth, 144-61);
    }else{
        self.smallView1.frame = CGRectMake(0, self.headView.bottom+0.5, ScreenWidth, 144);
    }
    if (self.smallView2.IsFold == YES) {
        self.smallView2.frame = CGRectMake(0, self.smallView1.bottom+0.5, ScreenWidth, 144-61);
    }else{
        self.smallView2.frame = CGRectMake(0, self.smallView1.bottom+0.5, ScreenWidth, 144);
    }
    if (self.smallView3.IsFold == YES) {
        self.smallView3.frame = CGRectMake(0, self.smallView2.bottom+0.5, ScreenWidth, 144-61);
    }else{
        self.smallView3.frame = CGRectMake(0, self.smallView2.bottom+0.5, ScreenWidth, 144);
    }
    if (self.dataSource.count == 1) {
        self.BottomView.frame = CGRectMake(0, self.smallView1.bottom, ScreenWidth, 28);
    }else if (self.dataSource.count == 2){
        self.BottomView.frame = CGRectMake(0, self.smallView2.bottom, ScreenWidth, 28);
    }else if(self.dataSource.count>=3){
        self.BottomView.frame = CGRectMake(0, self.smallView3.bottom, ScreenWidth, 28);
    }
}
-(void)setDataSource:(NSMutableArray *)dataSource{
    _dataSource = dataSource;
    
    if (dataSource.count == 1) {
        self.smallView2.hidden = YES;
        self.smallView3.hidden = YES;
    }else if (dataSource.count == 2){
        self.smallView3.hidden = YES;
    }
    
    if (dataSource.count >= 3) {//大于3条
        for (NSInteger i = 0; i<dataSource.count; i++) {
            RBNewIndustriyEntity * entity = dataSource[i];
            if (i == 0) {
                self.smallView1.entity = entity;
            }
            if (i == 1) {
                self.smallView2.entity = entity;
            }
            if (i == 2) {
                self.smallView3.entity = entity;
            }
        }
    }else if (dataSource.count == 2){//等于两条的时候
        for (NSInteger i = 0 ; i<2; i++) {
            RBNewIndustriyEntity * entity = dataSource[i];
            if (i==0) {
                self.smallView1.entity = entity;
            }
            if (i==1) {
                self.smallView2.entity = entity;
            }
        }
    }else if (dataSource.count == 1){
        RBNewIndustriyEntity * entity = dataSource[0];
        self.smallView1.entity = entity;
    }
}
-(void)setPermit{
    _permitBlock();
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
