//
//  RBMiddleSmallView.h
//  RB
//
//  Created by RB8 on 2017/8/9.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBMiddleSmallView : UIView
@property (weak, nonatomic) IBOutlet UILabel *ShareLabel;
@property (weak, nonatomic) IBOutlet UILabel *CommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *PraiseLabel;

@end
