//
//  RBWeiBoWaitView.h
//  RB
//
//  Created by RB8 on 2017/8/10.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBWeiBoWaitView : UIView

@property (weak, nonatomic) IBOutlet UIView *smallBackView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageVIew;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *describeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@end
