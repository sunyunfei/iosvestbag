//
//  RBWeiBoWaitView.m
//  RB
//
//  Created by RB8 on 2017/8/10.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBWeiBoWaitView.h"

@implementation RBWeiBoWaitView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    self.smallBackView.layer.cornerRadius = 45/2;
    self.smallBackView.layer.borderColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.17].CGColor;
    self.smallBackView.layer.borderWidth = 1.0;
    self.smallBackView.clipsToBounds = YES;
    //
    self.headImageVIew.layer.cornerRadius = 39/2;
    self.headImageVIew.clipsToBounds = YES;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
