//
//  RBEnoughView.h
//  RB
//
//  Created by RB8 on 2017/8/22.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBEnoughView : UIView
@property (weak, nonatomic) IBOutlet UILabel *noEnoughLabel;

@end
