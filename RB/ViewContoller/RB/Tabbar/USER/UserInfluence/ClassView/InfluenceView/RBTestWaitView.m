//
//  RBTestWaitView.m
//  RB
//
//  Created by RB8 on 2017/8/17.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBTestWaitView.h"

@implementation RBTestWaitView
-(void)setIdentity:(NSString *)identity{
    _identity = identity;
    if ([identity isEqualToString:@"mine"]) {
        self.caculateLabel.text = @"引擎正在计算你几卡车的庞大数据，以最好的形式展示，让你了解自己有多么重要(全程不超过一分钟)";
    }else if ([identity isEqualToString:@"him"]){
        self.caculateLabel.text = @"这位KOL还未测试影响力哦~";
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
