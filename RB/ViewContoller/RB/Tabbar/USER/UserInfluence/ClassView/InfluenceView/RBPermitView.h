//
//  RBPermitView.h
//  RB
//
//  Created by RB8 on 2017/8/10.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBPermitView : UIView
@property (weak, nonatomic) IBOutlet UIView *smallBackView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *describeLabel;

@end
