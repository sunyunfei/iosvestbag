//
//  RBInfluenceWechatView.m
//  RB
//
//  Created by RB8 on 2017/8/14.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBInfluenceWechatView.h"
#import "Service.h"
@implementation RBInfluenceWechatView
-(void)awakeFromNib{
    [super awakeFromNib];
    self.headView.layer.cornerRadius = 45/2;
    self.headView.layer.borderColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.14].CGColor;
    self.headView.layer.borderWidth = 1;
    self.headView.clipsToBounds = YES;
    self.headImageView.layer.cornerRadius = 39/3;
    self.headImageView.clipsToBounds = YES;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
