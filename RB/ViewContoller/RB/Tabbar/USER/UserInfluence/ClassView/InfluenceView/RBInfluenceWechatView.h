//
//  RBInfluenceWechatView.h
//  RB
//
//  Created by RB8 on 2017/8/14.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBInfluenceWechatView : UIView
@property (weak, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;

@end
