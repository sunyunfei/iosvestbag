//
//  RBInfluenceView.m
//  RB
//
//  Created by AngusNi on 3/7/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBInfluenceView.h"
#import "Service.h"

@implementation RBInfluenceView
{
    CAShapeLayer*shapeLayer;
}
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.rate = 0;
        //
    }
    return self;
}
-(void)setScoreText:(NSString *)scoreText{
    _scoreText = scoreText;
    self.userLabel.text = self.scoreText;
}
-(void)setInfluenceText:(NSString *)influenceText{
    _influenceText = influenceText;
    self.textLabel.text = self.influenceText;
}
-(void)setPercent:(NSString *)percent{
    _percent = percent;
    self.percentLabel.text = self.percent;
}
-(void)setDataStr:(NSString *)dataStr{
    _dataStr = dataStr;
    self.dateLabel.text = self.dataStr;
}
-(void)setImageUrl:(NSString *)imageUrl{
    _imageUrl = imageUrl;
}
-(void)createUI1{
    self.userIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-38.0)/2.0,19.0, 38.0, 38.0)];
    self.userIV.layer.cornerRadius = self.userIV.frame.size.width/2.0;
    self.userIV.layer.masksToBounds = YES;
    self.userIV.backgroundColor = [UIColor yellowColor];
    [self.userIV sd_setImageWithURL:[NSURL URLWithString:self.imageUrl] placeholderImage:PlaceHolderUserImage];
    [self addSubview:self.userIV];
    //
    self.userLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.userIV.bottom+8.0, ScreenWidth, 20.0)];
    self.userLabel.textAlignment = NSTextAlignmentCenter;
    self.userLabel.font = font_cu_cu_(25.0);
    self.userLabel.textColor = [Utils getUIColorWithHexString:ccColor212121];
    self.userLabel.text = self.scoreText;
    [self addSubview:self.userLabel];
    //
    self.textLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.userLabel.bottom+9, ScreenWidth, 12.0)];
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    self.textLabel.font = font_cu_13;
    self.textLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    self.textLabel.text = self.influenceText;
    [self addSubview:self.textLabel];
    //
    self.percentLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.textLabel.bottom+15, ScreenWidth, 10)];
    self.percentLabel.font = font_11;
    self.percentLabel.textColor = [Utils getUIColorWithHexString:ccColor969696];
    self.percentLabel.textAlignment = NSTextAlignmentCenter;
    self.percentLabel.text = self.percent;
    [self addSubview:self.percentLabel];
    //
    self.dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.percentLabel.bottom+7, ScreenWidth, 10.0)];
    self.dateLabel.textAlignment = NSTextAlignmentCenter;
    self.dateLabel.font = font_11;
    self.dateLabel.textColor = [Utils getUIColorWithHexString:ccColor969696];
    self.dateLabel.text = self.dataStr;
    [self addSubview:self.dateLabel];
    //
    CAShapeLayer*shapeLayer0 = [CAShapeLayer layer];
    shapeLayer0.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    shapeLayer0.fillColor = [UIColor clearColor].CGColor;
    shapeLayer0.lineWidth = 7.0;
    shapeLayer0.lineCap = @"round";
    shapeLayer0.strokeColor = [Utils getUIColorWithHexString:ccColore4e4e4].CGColor;
    CGMutablePathRef path0 = CGPathCreateMutable();
    CGPathAddArc(path0, NULL, ScreenWidth/2.0,90,90, -M_PI-35.0*(M_PI/180.0), 35.0*(M_PI/180.0), 0);
    shapeLayer0.path = path0;
    [self.layer addSublayer:shapeLayer0];
}
-(void)createUI2{
    //
    self.userLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 19+50, ScreenWidth, 20.0)];
    self.userLabel.textAlignment = NSTextAlignmentCenter;
    self.userLabel.font = font_cu_cu_(25.0);
    self.userLabel.textColor = [Utils getUIColorWithHexString:ccColor212121];
    self.userLabel.text = self.scoreText;
    self.userLabel.backgroundColor = [UIColor clearColor];
    [self.userLabel sizeToFit];
    self.userLabel.center = CGPointMake(ScreenWidth/2,19+30+self.userLabel.size.height/2);
    [self addSubview:self.userLabel];
    //
//    self.upImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.userLabel.right+5, self.userLabel.top+10, 12, 15)];
//    self.upImageView.image = [UIImage imageNamed:@"upImage"];
//    [self addSubview:self.upImageView];
    //
    self.textLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.userLabel.bottom+11, ScreenWidth, 12.0)];
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    self.textLabel.font = font_cu_15;
    self.textLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    self.textLabel.text = self.influenceText;
    [self addSubview:self.textLabel];
    //
    CAShapeLayer*shapeLayer0 = [CAShapeLayer layer];
    shapeLayer0.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    shapeLayer0.fillColor = [UIColor clearColor].CGColor;
    shapeLayer0.lineWidth = 7.0;
    shapeLayer0.lineCap = @"round";
    shapeLayer0.strokeColor = [Utils getUIColorWithHexString:ccColore4e4e4].CGColor;
    CGMutablePathRef path0 = CGPathCreateMutable();
    CGPathAddArc(path0, NULL, ScreenWidth/2.0,110,80, -M_PI, 0, 0);
    shapeLayer0.path = path0;
    [self.layer addSublayer:shapeLayer0];
    //
    if ([self.category isEqualToString:@"2"]) {
        UIButton * upButton = [[UIButton alloc]initWithFrame:CGRectMake((ScreenWidth - 160)/2, self.textLabel.bottom+32, 160, 30) title:@"提升影响力" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysBackColorYellow] image:nil hlImage:nil];
        upButton.layer.cornerRadius = 15;
        upButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [upButton addTarget:self action:@selector(PressBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:upButton];
    }
}
-(void)PressBtn:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(RBInfluenceViewClick)]) {
        [self.delegate RBInfluenceViewClick];
    }
}
- (void)drawRect:(CGRect)rect {
    if ([self.category isEqualToString:@"1"]) {
        [self createUI1];
        if (self.rate!=0) {
            self.shadowIV.hidden = NO;
            //
            [shapeLayer removeFromSuperlayer];
            shapeLayer = [CAShapeLayer layer];
            shapeLayer.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
            shapeLayer.fillColor = [UIColor clearColor].CGColor;
            shapeLayer.lineWidth = 7.0;
            shapeLayer.lineCap = @"round";
            shapeLayer.strokeColor = [Utils getUIColorWithHexString:ccColorffc701].CGColor;
            CGMutablePathRef path = CGPathCreateMutable();
            CGPathAddArc(path, NULL, ScreenWidth/2.0, 90.0, 90.0, -M_PI-35.0*(M_PI/180.0), -M_PI-35.0*(M_PI/180.0)+self.rate*(210.0*M_PI/180.0), 0);
            shapeLayer.path = path;
            [self.layer addSublayer:shapeLayer];
            //
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
            animation.fromValue = @(0.0);
            animation.toValue = @(1.0);
            animation.repeatCount = 0;
            animation.duration = 1.0;
            animation.fillMode = kCAFillModeForwards;
            [shapeLayer addAnimation:animation forKey:@"animation"];
            //
            //        CGContextSetLineWidth(context, 11.0);
            //        CGContextSetStrokeColorWithColor(context, [Utils getUIColorWithHexString:SysColorYellow].CGColor);
            //        CGContextSetLineCap(context, kCGLineCapRound);
            //        CGContextAddArc(context, ScreenWidth/2.0, 110.0, 100.0, -M_PI-15.0*(M_PI/180.0), -M_PI-15.0*(M_PI/180.0)+self.rate*(210.0*M_PI/180.0), 0);
            //        CGContextStrokePath(context);
        }

    }else if ([self.category isEqualToString:@"2"]){
        [self createUI2];
        if (self.rate!=0) {
            self.shadowIV.hidden = NO;
            //
            [shapeLayer removeFromSuperlayer];
            shapeLayer = [CAShapeLayer layer];
            shapeLayer.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
            shapeLayer.fillColor = [UIColor clearColor].CGColor;
            shapeLayer.lineWidth = 7.0;
            shapeLayer.lineCap = @"round";
            shapeLayer.strokeColor = [Utils getUIColorWithHexString:ccColorffc701].CGColor;
            CGMutablePathRef path = CGPathCreateMutable();
            CGPathAddArc(path, NULL, ScreenWidth/2.0, 110.0, 80.0, -M_PI, -M_PI+self.rate*(180*M_PI/180.0), 0);
            shapeLayer.path = path;
            [self.layer addSublayer:shapeLayer];
            //
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
            animation.fromValue = @(0.0);
            animation.toValue = @(1.0);
            animation.repeatCount = 0;
            animation.duration = 1.0;
            animation.fillMode = kCAFillModeForwards;
            [shapeLayer addAnimation:animation forKey:@"animation"];
            //
            //        CGContextSetLineWidth(context, 11.0);
            //        CGContextSetStrokeColorWithColor(context, [Utils getUIColorWithHexString:SysColorYellow].CGColor);
            //        CGContextSetLineCap(context, kCGLineCapRound);
            //        CGContextAddArc(context, ScreenWidth/2.0, 110.0, 100.0, -M_PI-15.0*(M_PI/180.0), -M_PI-15.0*(M_PI/180.0)+self.rate*(210.0*M_PI/180.0), 0);
            //        CGContextStrokePath(context);
        }

    }else if([self.category isEqualToString:@"3"]){
        [self createUI2];
        if (self.rate!=0) {
            self.shadowIV.hidden = NO;
            //
            [shapeLayer removeFromSuperlayer];
            shapeLayer = [CAShapeLayer layer];
            shapeLayer.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
            shapeLayer.fillColor = [UIColor clearColor].CGColor;
            shapeLayer.lineWidth = 7.0;
            shapeLayer.lineCap = @"round";
            shapeLayer.strokeColor = [Utils getUIColorWithHexString:ccColorffc701].CGColor;
            CGMutablePathRef path = CGPathCreateMutable();
            CGPathAddArc(path, NULL, ScreenWidth/2.0, 110.0, 80.0, -M_PI, -M_PI+self.rate*(180*M_PI/180.0), 0);
            shapeLayer.path = path;
            [self.layer addSublayer:shapeLayer];
            //
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
            animation.fromValue = @(0.0);
            animation.toValue = @(1.0);
            animation.repeatCount = 0;
            animation.duration = 1.0;
            animation.fillMode = kCAFillModeForwards;
            [shapeLayer addAnimation:animation forKey:@"animation"];
        }
    }
}

@end
