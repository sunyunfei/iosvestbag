//
//  RBWeiboSmallView.m
//  RB
//
//  Created by RB8 on 2017/8/9.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBWeiboSmallView.h"
#import "Service.h"
#import "RBinfluenceHeadView.h"
@implementation RBWeiboSmallView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        self.leftImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15,20, 26, 26)];
        [self addSubview:self.leftImageView];
        //
        self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.leftImageView.right+15,27, 100, 15) text:@"" font:font_15 textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:ccColor1a1a1a] backgroundColor:[UIColor clearColor] numberOfLines:1];
        [self addSubview:self.nameLabel];
        //
//        self.describeLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 100,27, 85, 20) text:@"在微博好友中NO.2" font:font_11 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:ccColor414141] backgroundColor:[UIColor clearColor] numberOfLines:1];
//        [self.describeLabel sizeToFit];
//        self.describeLabel.frame = CGRectMake(ScreenWidth - 15-self.describeLabel.size.width,27, self.describeLabel.size.width, self.describeLabel.size.height);
//        [self addSubview:self.describeLabel];
        //
//        self.rightImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.describeLabel.left-15,self.describeLabel.top, 15, 15)];
//        self.rightImageView.image = [UIImage imageNamed:@"RBBage"];
//        [self addSubview:self.rightImageView];
        //
        self.percentLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.leftImageView.left,self.nameLabel.bottom+15, 50, 16) text:@"" font:font_17 textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:ccColor1a1a1a] backgroundColor:[UIColor clearColor] numberOfLines:1];
        [self addSubview:self.percentLabel];
        //
        self.progressView = [[JGProgressView alloc]initWithFrame:CGRectMake(self.percentLabel.right+5, self.nameLabel.bottom+21, ScreenWidth - self.percentLabel.right-12-34-10, 10)];
        self.progressView.circle = @"circle";
        self.progressView.Tcolor = [Utils getUIColorWithHexString:@"f76262"];
        self.progressView.Ccolor = [Utils getUIColorWithHexString:ccColore4e4e4];
        self.progressView.direction = @"left";
//        self.progressView.progress = 0.5;
        [self addSubview:self.progressView];
        //
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(self.progressView.right, self.percentLabel.top+4, 44, 12) title:@"" hlTitle:nil titleColor:[UIColor clearColor] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
        [btn addTarget:self action:@selector(PressBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        //
        self.pinImageVIew = [[UIImageView alloc]initWithFrame:CGRectMake(19, (btn.height-6)/2, 10, 6)];
        self.pinImageVIew.image = [UIImage imageNamed:@"RBInfluencePin"];
        [btn addSubview:self.pinImageVIew];
        //
        self.bottomView = [[[NSBundle mainBundle]loadNibNamed:@"RBMiddleSmallView" owner:nil options:nil]lastObject];
        self.bottomView.frame = CGRectMake(0, self.progressView.bottom+10, ScreenWidth, 61);
        self.bottomView.hidden = YES;
        [self addSubview:self.bottomView];
        self.IsFold = YES;
//        __weak __typeof__(self) weakSelf = self;
//        self.foldBlock = ^{
//            weakSelf.IsFold = !weakSelf.IsFold;
//        };
    }
    return self;
}
-(void)PressBtn:(id)sender{
  //  self.foldBlock();
    NSLog(@"hehe");
    self.IsFold = !self.IsFold;
    if (self.IsFold == YES) {
        self.pinImageVIew.image = [UIImage imageNamed:@"RBInfluencePin"];
    }else{
        self.pinImageVIew.image = [UIImage imageNamed:@"RBInfluencePin1"];
    }
    [self setfoldOrUnFold:self.IsFold];
}
-(void)setfoldOrUnFold:(BOOL)IsFold{
    if (IsFold == YES) {
        self.bottomView.frame = CGRectMake(0, self.progressView.bottom+10, ScreenWidth, 0);
        self.bottomView.hidden = YES;
        if ([self.delegate respondsToSelector:@selector(changeViewFrame:)]) {
            [self.delegate changeViewFrame:YES];
        }
    }else{
        self.bottomView.frame = CGRectMake(0, self.progressView.bottom+10, ScreenWidth, 61);
        self.bottomView.hidden = NO;
        if ([self.delegate respondsToSelector:@selector(changeViewFrame:)]) {
            [self.delegate changeViewFrame:NO];
        }
    }
 
}
//使bottom合上
-(void)setBotoomViewFold{
    self.bottomView.frame = CGRectMake(0, self.progressView.bottom+10, ScreenWidth, 0);
    self.bottomView.hidden = YES;
    self.pinImageVIew.image = [UIImage imageNamed:@"RBInfluencePin"];

}
-(void)setEntity:(RBNewIndustriyEntity *)entity{
    self.nameLabel.text = entity.industry_name;
    self.percentLabel.text = [NSString stringWithFormat:@"%@%%",entity.industry_score];
    self.leftImageView.image = [UIImage imageNamed:entity.industry_name];
    
    self.progressView.progress = (CGFloat)[entity.industry_score intValue]/100;

    self.bottomView.ShareLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_posts floatValue]*100)/100];
    self.bottomView.CommentLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_comments floatValue]*100)/100];
    self.bottomView.PraiseLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_likes floatValue]*100)/100];
}
//-(void)setIsFold:(BOOL)IsFold{
//    if (IsFold == YES) {
//        self.bottomView.frame = CGRectMake(0, self.progressView.bottom+10, ScreenWidth, 0);
//        self.bottomView.hidden = YES;
//        if ([self.delegate respondsToSelector:@selector(changeViewFrame:)]) {
//            [self.delegate changeViewFrame:YES];
//        }
//    }else{
//        self.bottomView.frame = CGRectMake(0, self.progressView.bottom+10, ScreenWidth, 61);
//        self.bottomView.hidden = NO;
//        if ([self.delegate respondsToSelector:@selector(changeViewFrame:)]) {
//            [self.delegate changeViewFrame:NO];
//        }
//    }
//}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
