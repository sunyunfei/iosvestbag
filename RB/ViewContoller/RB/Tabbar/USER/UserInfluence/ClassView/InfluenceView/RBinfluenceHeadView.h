//
//  RBinfluenceHeadView.h
//  RB
//
//  Created by RB8 on 2017/8/9.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RBinfluenceHeadViewDelegate <NSObject>
-(void)setPermit;
@end
@interface RBinfluenceHeadView : UIView
@property (weak, nonatomic) IBOutlet UIView *smallbackView;

@property (weak, nonatomic) IBOutlet UIButton *toOthers;
@property (weak, nonatomic) IBOutlet UILabel *describeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *shareLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UILabel *zanLabel;
@property (nonatomic,weak)id<RBinfluenceHeadViewDelegate>delegate;
- (IBAction)Permit:(id)sender;
@end
