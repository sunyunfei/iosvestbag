//
//  RBinfluenceHeadView.m
//  RB
//
//  Created by RB8 on 2017/8/9.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBinfluenceHeadView.h"
#import "Utils.h"
#import "Service.h"
@implementation RBinfluenceHeadView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
        
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    self.smallbackView.layer.cornerRadius = 45/2;
    self.smallbackView.layer.borderColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.17].CGColor;
    self.smallbackView.layer.borderWidth = 1.0;
    self.smallbackView.clipsToBounds = YES;
    //
    self.headImageView.layer.cornerRadius = 39/2;
    self.headImageView.clipsToBounds = YES;
    //
    self.toOthers.layer.cornerRadius = 18/2;
    self.toOthers.layer.borderColor = [Utils getUIColorWithHexString:ccColorffc700].CGColor;
    self.toOthers.layer.borderWidth = 1.5;
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)Permit:(id)sender {
    if ([self.delegate respondsToSelector:@selector(setPermit)]) {
        [self.delegate setPermit];
    }
}
@end
