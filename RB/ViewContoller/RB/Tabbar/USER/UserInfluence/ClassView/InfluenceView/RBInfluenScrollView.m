//
//  RBInfluenScrollView.m
//  RB
//
//  Created by RB8 on 2017/8/8.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBInfluenScrollView.h"
#import "Service.h"
#import "RBinfluenceHeadView.h"
#import "RBIncluenceMIddleCell.h"
@implementation RBInfluenScrollView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //
        self.topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth,50.5)];
        self.topView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        [self addSubview:self.topView];
        //
        UIButton*Weibobtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth/2, 48) title:NSLocalizedString(@"R1025", @"微博") hlTitle:nil titleColor:[Utils getUIColorWithHexString:ccColor202020] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
        [Weibobtn addTarget:self action:@selector(PressBtn:) forControlEvents:UIControlEventTouchUpInside];
        Weibobtn.tag = 99;
        Weibobtn.titleLabel.font = font_15;
        [self.topView addSubview:Weibobtn];
        //
        UIButton * WechatBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2, 0, ScreenWidth/2, 48) title:NSLocalizedString(@"R1023", @"微信") hlTitle:nil titleColor:[Utils getUIColorWithHexString:ccColor202020] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
        [WechatBtn addTarget:self action:@selector(PressBtn:) forControlEvents:UIControlEventTouchUpInside];
        WechatBtn.titleLabel.font = font_15;
        WechatBtn.tag = 100;
        [self.topView addSubview:WechatBtn];
        //
        _lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, WechatBtn.bottom, 55, 2) text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:ccColorffc700] numberOfLines:1];
        _lineLabel.center = CGPointMake(Weibobtn.center.x, WechatBtn.bottom+1);
        [self.topView addSubview:_lineLabel];
        //
        UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, _lineLabel.bottom, ScreenWidth, 0.5)];
        lineView.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
        lineView.alpha = 0.4;
        [_topView addSubview:lineView];
        //
        self.mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, _topView.bottom, ScreenWidth, 163.5+144*3 - 2*61 + 28)];
        self.mainScrollView.delegate = self;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        self.mainScrollView.bounces = NO;
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.scrollEnabled = YES;
        [self addSubview:self.mainScrollView];
        self.WechatView = [[[NSBundle mainBundle]loadNibNamed:@"RBInfluenceWechatView" owner:nil options:nil]lastObject];
        self.WechatView.frame = CGRectMake(ScreenWidth, 0, ScreenWidth, 163.5+3*144-3*61 + 28);
        [self.mainScrollView addSubview:self.WechatView];
//        self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, frame.size.width,frame.size.height-50 ) style:UITableViewStylePlain];
//        self.tableView.delegate = self;
//        self.tableView.dataSource = self;
//        _dataSource = @[@"1",@"2",@"3",@"4"];
//        self.tableView.scrollEnabled = NO;
//        [self.tableView registerClass:[RBIncluenceMIddleCell class] forCellReuseIdentifier:@"RBIncluenceMIddleCell"];
        
  //      [self.mainScrollView addSubview:self.tableView];
    }
    return self;
}
-(void)setCategary:(NSString *)categary{
    _categary = categary;
    if ([_categary isEqualToString:@"data"]) {
        
        self.bigView = [[RBWeiBoBigView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 163.5+_count*144-(_count-1)*61 +28)];
        self.bigView.delegate = self;
        [self.mainScrollView addSubview:self.bigView];
        //
        self.mainScrollView.contentSize = CGSizeMake(ScreenWidth*2, 163.5+_count*144-(_count-1)*61 +28);
    }else if ([_categary isEqualToString:@"wait"]){
        self.mainScrollView.frame = CGRectMake(0, 50.5, ScreenWidth, 163.5+3*144-3*61 +28);
        self.waitView = [[[NSBundle mainBundle]loadNibNamed:@"RBWeiBoWaitView" owner:nil options:nil] lastObject];
        self.waitView.frame = CGRectMake(0, 0, ScreenWidth, 163.5+3*144-3*61 +28);
        [self.mainScrollView addSubview:self.waitView];
        self.mainScrollView.contentSize = CGSizeMake(ScreenWidth*2, 163.5+3*144-3*61 +28);
        if ([self.delegate respondsToSelector:@selector(changFrameWhenClickWechat)]) {
            [self.delegate changFrameWhenClickWechat];
        }
    }else if ([_categary isEqualToString:@"nosee"]){
        self.mainScrollView.frame = CGRectMake(0, 50.5, ScreenWidth, 163.5+3*144-3*61 +28);
        self.permitView = [[[NSBundle mainBundle]loadNibNamed:@"RBPermitView" owner:nil options:nil] lastObject];
        self.permitView.frame = CGRectMake(0, 0, ScreenWidth, 163.5+3*144-3*61 +28);
        [self.mainScrollView addSubview:self.permitView];
        self.mainScrollView.contentSize = CGSizeMake(ScreenWidth*2, 163.5+3*144-3*61 +28);
        if ([self.delegate respondsToSelector:@selector(changFrameWhenClickWechat)]) {
            [self.delegate changFrameWhenClickWechat];
        }
    }else if ([_categary isEqualToString:@"noenough"]){
        self.bigView = [[RBWeiBoBigView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 163.5+3*144-3*61 +28)];
        self.bigView.delegate = self;
        self.bigView.smallView1.hidden = YES;
        self.bigView.smallView2.hidden = YES;
        self.bigView.smallView3.hidden = YES;
        self.bigView.noenoughView.hidden = NO;
        [self.mainScrollView addSubview:self.bigView];
        //
        self.mainScrollView.contentSize = CGSizeMake(ScreenWidth*2, 163.5+3*144-3*61 +28);
    }

}
-(void)setMainScrollVIewFrame:(NSString*)mark{

}
-(void)changeWeiboFrame:(BOOL)isFold{
    
    if (isFold == YES) {
        self.topView.frame = CGRectMake(0, 0, ScreenWidth, 50.5);
        self.mainScrollView.frame = CGRectMake(0, 50.5, ScreenWidth, self.mainScrollView.frame.size.height -61);
        self.mainScrollView.contentSize = CGSizeMake(ScreenWidth*2, self.mainScrollView.frame.size.height -61);
        self.bigView.frame = CGRectMake(0, 0, ScreenWidth, self.bigView.frame.size.height - 61);
        if ([self.delegate respondsToSelector:@selector(changeRBInfluenScrollViewFrame:)]) {
            [self.delegate changeRBInfluenScrollViewFrame:YES];
        }
    }else if (isFold == NO){
        self.topView.frame = CGRectMake(0, 0, ScreenWidth, 50.5);
        self.mainScrollView.frame = CGRectMake(0, 50.5, ScreenWidth, self.mainScrollView.frame.size.height+61);
        self.mainScrollView.contentSize = CGSizeMake(ScreenWidth*2, self.mainScrollView.frame.size.height -61);
        self.bigView.frame = CGRectMake(0, 0, ScreenWidth, self.bigView.frame.size.height+61);
        if ([self.delegate respondsToSelector:@selector(changeRBInfluenScrollViewFrame:)]) {
            [self.delegate changeRBInfluenScrollViewFrame:NO];
        }
    }
}
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    if (scrollView.contentOffset.x == ScreenWidth) {
//        [self.bigView.smallView1 setfoldOrUnFold:YES];
//        [self.bigView.smallView2 setfoldOrUnFold:YES];
//        [self.bigView.smallView3 setfoldOrUnFold:YES];
////        self.topView.frame = CGRectMake(0, 0, ScreenWidth, 50);
////        self.mainScrollView.frame = CGRectMake(0, 50, ScreenWidth, self.mainScrollView.frame.size.height -61);
////        self.bigView.frame = CGRectMake(0, 0, ScreenWidth, self.bigView.frame.size.height - 61);
//    }
//}
-(void)updateConstraints{
    [super updateConstraints];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([self.categary isEqualToString:@"data"]) {
    if (scrollView.contentOffset.x == ScreenWidth) {
        if ([self.delegate respondsToSelector:@selector(changFrameWhenClickWechat)]) {
            [self.delegate changFrameWhenClickWechat];
        }
        self.mainScrollView.contentSize = CGSizeMake(2*ScreenWidth, 163.5+3*144-3*61 +28);
        self.mainScrollView.frame = CGRectMake(0, 50.5,ScreenWidth, 163.5+3*144-3*61 +28);
        self.mainScrollView.contentOffset = CGPointMake(ScreenWidth, 0);
        self.bigView.smallView1.IsFold = YES;
        self.bigView.smallView2.IsFold = YES;
        self.bigView.smallView3.IsFold = YES;
        [self.bigView.smallView1 setBotoomViewFold];
        [self.bigView.smallView2 setBotoomViewFold];
        [self.bigView.smallView3 setBotoomViewFold];
        [self.bigView updateConstraints];
    }else if(scrollView.contentOffset.x == 0){
        if ([self.delegate respondsToSelector:@selector(changeFrameWhenClickWeibo)]) {
            [self.delegate changeFrameWhenClickWeibo];
        }
        self.mainScrollView.contentSize = CGSizeMake(2*ScreenWidth, 163.5+_count*144-_count*61 +28);
        self.mainScrollView.frame = CGRectMake(0, 50.5,ScreenWidth, 163.5+_count*144-_count*61 +28);
        self.mainScrollView.contentOffset = CGPointMake(0, 0);
        self.bigView.smallView1.IsFold = YES;
        self.bigView.smallView2.IsFold = YES;
        self.bigView.smallView3.IsFold = YES;
        [self.bigView.smallView1 setBotoomViewFold];
        [self.bigView.smallView2 setBotoomViewFold];
        [self.bigView.smallView3 setBotoomViewFold];
        [self.bigView updateConstraints];
    }
    }
    _lineLabel.center = CGPointMake(ScreenWidth/4+scrollView.contentOffset.x/2,49);
}
-(void)PressBtn:(id)sender{
    UIButton * btn = (UIButton*)sender;
    if (btn.tag == 99) {
        
        
        if ([self.categary isEqualToString:@"data"]) {
            //点击微博按钮，主界面也需要重新计算frame
            if ([self.delegate respondsToSelector:@selector(changeFrameWhenClickWeibo)]) {
                [self.delegate changeFrameWhenClickWeibo];
            }
            self.mainScrollView.contentSize = CGSizeMake(2*ScreenWidth, 163.5+_count*144-_count*61 +28);
            self.mainScrollView.frame = CGRectMake(0, 50.5, ScreenWidth, 163.5+_count*144-_count*61 +28);
            [UIView animateWithDuration:0.5 animations:^{
                self.mainScrollView.contentOffset = CGPointMake(0, 0);
                _lineLabel.center = CGPointMake(ScreenWidth/4, 49);
            }];
            self.bigView.smallView1.IsFold = YES;
            self.bigView.smallView2.IsFold = YES;
            self.bigView.smallView3.IsFold = YES;
            [self.bigView.smallView1 setBotoomViewFold];
            [self.bigView.smallView2 setBotoomViewFold];
            [self.bigView.smallView3 setBotoomViewFold];
            [self.bigView updateConstraints];
        }else{
            [UIView animateWithDuration:0.5 animations:^{
                self.mainScrollView.contentOffset = CGPointMake(0, 0);
                _lineLabel.center = CGPointMake(ScreenWidth/4, 49);
            }];

        }
    }else{
        if ([self.categary isEqualToString:@"data"]) {
            //点击微信按钮，微博界面需要全部收回，主界面也需要重新计算frame
            if ([self.delegate respondsToSelector:@selector(changFrameWhenClickWechat)]) {
                [self.delegate changFrameWhenClickWechat];
            }
            self.mainScrollView.contentSize = CGSizeMake(2*ScreenWidth, 163.5+3*144-3*61 +28);
            self.mainScrollView.frame = CGRectMake(0, 50,ScreenWidth, 163.5+3*144-3*61 +28);
            [UIView animateWithDuration:0.5 animations:^{
                self.mainScrollView.contentOffset = CGPointMake(ScreenWidth, 0);
                _lineLabel.center = CGPointMake(ScreenWidth*3/4, 49);
            }];
            self.bigView.smallView1.IsFold = YES;
            self.bigView.smallView2.IsFold = YES;
            self.bigView.smallView3.IsFold = YES;
            [self.bigView.smallView1 setBotoomViewFold];
            [self.bigView.smallView2 setBotoomViewFold];
            [self.bigView.smallView3 setBotoomViewFold];
            [self.bigView updateConstraints];
        }else{
            
            [UIView animateWithDuration:0.5 animations:^{
                self.mainScrollView.contentOffset = CGPointMake(ScreenWidth, 0);
                _lineLabel.center = CGPointMake(ScreenWidth*3/4, 49);
            }];
        }
       
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RBIncluenceMIddleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"RBIncluenceMIddleCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 144;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    RBinfluenceHeadView * headView = [[[NSBundle mainBundle]loadNibNamed:@"RBinfluenceHeadView" owner:nil options:nil]lastObject];
    return headView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 163;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
