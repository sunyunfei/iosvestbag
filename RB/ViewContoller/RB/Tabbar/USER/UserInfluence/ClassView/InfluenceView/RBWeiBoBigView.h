//
//  RBWeiBoBigView.h
//  RB
//
//  Created by RB8 on 2017/8/9.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBWeiboSmallView.h"
#import "RBinfluenceHeadView.h"
#import "RBEnoughView.h"
@protocol changeWeiboFrame <NSObject>
-(void)changeWeiboFrame:(BOOL)isFold;
@end
@interface RBWeiBoBigView : UIView<changFrame,RBinfluenceHeadViewDelegate>
@property(nonatomic,strong)RBinfluenceHeadView * headView;
@property(nonatomic,strong)RBWeiboSmallView * smallView1;
@property(nonatomic,strong)RBWeiboSmallView * smallView2;
@property(nonatomic,strong)RBWeiboSmallView * smallView3;
@property(nonatomic,assign)id<changeWeiboFrame>delegate;
@property(nonatomic,strong)UIView * BottomView;
@property(nonatomic,strong)RBEnoughView * noenoughView;
@property(nonatomic,strong)NSMutableArray * dataSource;
@property(nonatomic,copy)void(^permitBlock)(void);
@end
