//
//  RBInfluenScrollView.h
//  RB
//
//  Created by RB8 on 2017/8/8.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBWeiBoBigView.h"
#import "RBWeiBoWaitView.h"
#import "RBPermitView.h"
#import "RBInfluenceWechatView.h"
#import "RBEnoughView.h"
@protocol RBInfluenScrollViewDelegate <NSObject>
-(void)changeRBInfluenScrollViewFrame:(BOOL)isFold;
-(void)changFrameWhenClickWechat;
-(void)changeFrameWhenClickWeibo;
@end
@interface RBInfluenScrollView : UIView<UITableViewDataSource,UITableViewDelegate,changeWeiboFrame,UIScrollViewDelegate>
@property(nonatomic,strong)UITableView * tableView;
@property(nonatomic,strong)NSArray * dataSource;
//
@property(nonatomic,strong)UIScrollView * mainScrollView;
//
@property(nonatomic,strong)UILabel * lineLabel;
//
@property(nonatomic,assign)BOOL IsFold;
//
@property(nonatomic,strong)UIView * topView;
//
@property(nonatomic,strong)RBWeiBoBigView * bigView;
//
@property(nonatomic,strong)RBInfluenceWechatView * WechatView;
//
@property(nonatomic,strong)RBPermitView * permitView;
//
@property(nonatomic,strong)RBEnoughView * noEnoughView;
//用户的条数
@property(nonatomic,assign)NSInteger count;
@property(nonatomic,copy)NSString * categary;
@property(nonatomic,strong)RBWeiBoWaitView * waitView;
@property(nonatomic,weak)id<RBInfluenScrollViewDelegate>delegate;
@end
