//
//  RBInfluenceView.h
//  RB
//
//  Created by AngusNi on 3/7/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RBInfluenceView;
@protocol RBInfluenceViewDelegate <NSObject>

-(void)RBInfluenceViewClick;

@end
@interface RBInfluenceView : UIView
@property (nonatomic, assign) float rate;
@property (nonatomic, strong) UIImageView*shadowIV;
@property (nonatomic, strong) UIImageView*userIV;
@property (nonatomic, strong) UILabel*userLabel;
@property (nonatomic, strong) UILabel*textLabel;
@property (nonatomic, strong) UILabel*dateLabel;
@property (nonatomic,strong)  UILabel * percentLabel;
@property (nonatomic,copy)    NSString * category;
@property (nonatomic,strong)  UIImageView * upImageView;
@property(nonatomic,strong)   NSString * scoreText;
@property(nonatomic,strong)   NSString * influenceText;
@property(nonatomic,strong)   NSString * imageUrl;
@property(nonatomic,strong)   NSString * percent;
@property(nonatomic,strong)   NSString * dataStr;
@property (nonatomic,weak)    id<RBInfluenceViewDelegate>delegate;
@end
