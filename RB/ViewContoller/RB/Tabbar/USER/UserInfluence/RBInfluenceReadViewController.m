//
//  RBInfluenceReadViewController.m
//  RB
//
//  Created by AngusNi on 4/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBInfluenceReadViewController.h"

@interface RBInfluenceReadViewController () {
    NSMutableArray*titlesArray;
    NSMutableArray*dataArray;
    UIScrollView*scrollviewn;
    RBInfluenceReadEntity*readEntity;
    MCLineChartView*lineChartView;
}
@end

@implementation RBInfluenceReadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5185", @"影响力解读");
    self.bgIV.hidden = NO;
    self.navView.backgroundColor = [UIColor clearColor];
    self.navLineLabel.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    // RB-获取用户影响力解读
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBInfluenceRead];
    //
    [TalkingData trackPageBegin:@"my-influence-read"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-influence-read"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    [scrollviewn removeAllSubviews];
    scrollviewn = nil;
    scrollviewn = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    scrollviewn.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [scrollviewn setPagingEnabled:NO];
    [scrollviewn setShowsHorizontalScrollIndicator:NO];
    [scrollviewn setShowsVerticalScrollIndicator:NO];
    scrollviewn.userInteractionEnabled = YES;
    [self.view addSubview:scrollviewn];
    //
    NSArray*array = @[NSLocalizedString(@"R5056", @"身份特质"),NSLocalizedString(@"R5057", @"活跃程度"),NSLocalizedString(@"R5058", @"参与悬赏"),NSLocalizedString(@"R5059", @"佳文分享"),NSLocalizedString(@"R5060", @"人脉关系")];
    NSArray*array1 = @[[NSString stringWithFormat:@"%.2f",readEntity.feature_rate.floatValue],[NSString stringWithFormat:@"%.2f",readEntity.active_rate.floatValue],[NSString stringWithFormat:@"%.2f",readEntity.campaign_rate.floatValue],[NSString stringWithFormat:@"%.2f",readEntity.share_rate.floatValue],[NSString stringWithFormat:@"%.2f",readEntity.contact_rate.floatValue]];
    //
    JYRadarChart*radar = [[JYRadarChart alloc] initWithFrame:CGRectMake(0, -20.0, ScreenWidth, 305.0*ScreenWidth/325.0)];
    radar.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    radar.attributes = array;
    radar.dataSeries = @[array1];
    radar.r = ScreenWidth*90.0/320.0;
    [scrollviewn addSubview:radar];
    //
    UILabel*spiderLabel = [[UILabel alloc]initWithFrame:radar.bounds];
    spiderLabel.text = [NSString stringWithFormat:@"%@",self.influenceEntity.influence_score];
    spiderLabel.font = font_cu_30;
    spiderLabel.textAlignment = NSTextAlignmentCenter;
    spiderLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    [radar addSubview:spiderLabel];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, radar.bottom-20.0,ScreenWidth, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
//    [scrollviewn addSubview:lineLabel];
    //
    lineChartView = [[MCLineChartView alloc] initWithFrame:CGRectMake(0, lineLabel.top+20, ScreenWidth, 200)];
    lineChartView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.5];
    lineChartView.dataSource = self;
    lineChartView.delegate = self;
    lineChartView.minValue = @0;
    lineChartView.solidDot = YES;
    lineChartView.numberOfYAxis = 3;
    lineChartView.xFontSize = 10.0;
    lineChartView.yFontSize = 10.0;
    lineChartView.colorOfXAxis = [UIColor clearColor];
    lineChartView.colorOfXText = [Utils getUIColorWithHexString:SysColorSubGray];
    lineChartView.colorOfYAxis = [UIColor clearColor];
    lineChartView.colorOfYText = [Utils getUIColorWithHexString:SysColorSubGray];
    [scrollviewn addSubview:lineChartView];
    //
    UILabel*lineTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(18.0, lineChartView.top+7.0,ScreenWidth-36.0, 20.0)];
    lineTitleLabel.text = NSLocalizedString(@"R5061", @"近期影响力趋势");
    lineTitleLabel.font = font_13;
    lineTitleLabel.textAlignment = NSTextAlignmentCenter;
    lineTitleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [scrollviewn addSubview:lineTitleLabel];
    //
    NSString*str = [NSString stringWithFormat:@"%@",readEntity.diff_score];
    if([[str componentsSeparatedByString:@"比上周增加了"] count]>1){
        str = [str componentsSeparatedByString:@"比上周增加了"][1];
        str = [str componentsSeparatedByString:@"分"][0];
    }
    NSString*str1 = [NSString stringWithFormat:@"%@",readEntity.diff_score];
    if([[str1 componentsSeparatedByString:@"影响力分数"] count]>1){
        str1 = [str1 componentsSeparatedByString:@"影响力分数"][1];
        str1 = [str1 componentsSeparatedByString:@"分 比上周增加了"][0];
    }
    UILabel*influenceScoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, lineChartView.bottom,ScreenWidth, 30.0)];
    influenceScoreLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5187", @"影响力分数%@分 比上次增加了%@分"),str1,str];
    influenceScoreLabel.font = font_11;
    influenceScoreLabel.textAlignment = NSTextAlignmentCenter;
    influenceScoreLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [scrollviewn addSubview:influenceScoreLabel];
    //
    [scrollviewn setContentSize:CGSizeMake(ScreenWidth, influenceScoreLabel.bottom+20.0)];
}

#pragma mark - MCLineChartView Delegate
- (void)lineChartView:(MCLineChartView *)lineChartView selectedAt:(int)index {
}

- (NSUInteger)numberOfLinesInLineChartView:(MCLineChartView *)lineChartView {
    return 1;
}

- (NSUInteger)lineChartView:(MCLineChartView *)lineChartView lineCountAtLineNumber:(NSInteger)number {
    return [dataArray count];
}

- (id)lineChartView:(MCLineChartView *)lineChartView valueAtLineNumber:(NSInteger)lineNumber index:(NSInteger)index {
    return dataArray[index];
}

- (NSString *)lineChartView:(MCLineChartView *)lineChartView titleAtLineNumber:(NSInteger)number {
    return titlesArray[number];
}

- (UIColor *)lineChartView:(MCLineChartView *)lineChartView lineColorWithLineNumber:(NSInteger)lineNumber {
    return [Utils getUIColorWithHexString:SysColorYellow];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"item_detail"]) {
        [self.hudView dismiss];
        readEntity = [JsonService getRBInfluenceReadEntity:jsonObject];
        [self loadEditView];
        //
        titlesArray = [NSMutableArray new];
        dataArray = [NSMutableArray new];
        float maxScore=0;
        for (int i=0; i<[readEntity.history count]; i++) {
            RBInfluenceHistoryEntity *entity = readEntity.history[i];
            if ([[[NSString stringWithFormat:@"%@",entity.date]componentsSeparatedByString:@"-"]count]>2) {
                NSString*month = [[NSString stringWithFormat:@"%@",entity.date]componentsSeparatedByString:@"-"][1];
                NSString*day = [[NSString stringWithFormat:@"%@",entity.date]componentsSeparatedByString:@"-"][2];
                [titlesArray addObject:[NSString stringWithFormat:@"%@-%@",month,day]];
            }
            if ([NSString stringWithFormat:@"%@",entity.score].floatValue>maxScore) {
                maxScore = [NSString stringWithFormat:@"%@",entity.score].floatValue;
            }
            [dataArray addObject:[NSString stringWithFormat:@"%@",entity.score]];
        }
        if (maxScore!=0) {
            lineChartView.maxValue = [NSString stringWithFormat:@"%.2f",maxScore*1.2];
        } else {
            lineChartView.maxValue = @600;
        }
        [lineChartView reloadData];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

