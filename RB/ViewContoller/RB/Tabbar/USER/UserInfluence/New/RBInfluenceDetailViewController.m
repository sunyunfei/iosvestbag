//
//  RBInfluenceDetailViewController.m
//  RB
//
//  Created by RB8 on 2017/8/8.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBInfluenceDetailViewController.h"
#import "NewPagedFlowView.h"
#import "JYRadarChart.h"
#import "RBInfluenScrollView.h"
#import "PGIndexBannerSubiew.h"
#import "RBInfluenScrollView.h"
#import "UpInfluenceViewController.h"
#import "InfluenceController.h"
#import "RBKolSearchViewController.h"
#import "RBTestWaitView.h"
#import "RBAppDelegate.h"
#import "RBLoginViewController.h"
#import "RBInfluenceLoadingViewController.h"
@interface RBInfluenceDetailViewController ()<UIScrollViewDelegate,RBInfluenScrollViewDelegate>
{
    UIScrollView * backScroll;
    RBInfluenceView * influView;
    UIScrollView * userScroll;
    NewPagedFlowView * pageFlowView;
    RBInfluenScrollView * subScrollView;
    JYRadarChart*radarView;
    RBInfluenScrollView * subScroll;
    UIView *footView;
    UIButton * upInfluenceBtn;
    RBNewInfluenceEntity * entity;
    RBTestWaitView * waitView;
    //
    NSTimer *codeTimer;
    //记录计时器工作
    NSInteger beginTime;
    //
    NSMutableArray * dataArray;
    //记录是否需要淡隐淡出
    BOOL  hidden;
}
@end

@implementation RBInfluenceDetailViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    if ([LocalService getRBFirstInfluenceenter] == nil) {
        RBInfluenceLoadingViewController * loadVC = [[RBInfluenceLoadingViewController alloc]init];
        [self addChildViewController:loadVC];
        loadVC.view.frame = self.view.bounds;
        self.view.userInteractionEnabled = YES;
        [self.view addSubview:loadVC.view];
    }else{
        [self createUI];
    }
}
- (void)createUI{
    if ([LocalService getRBFirstInfluenceenter] == nil) {
        [LocalService setRBFirstInfluenceenter:@"yes"];
    }
    if ([_mark isEqualToString:@"mine"]) {
        [TalkingData trackPageBegin:@"My_influence"];
    }else{
        [TalkingData trackPageBegin:@"Other people_influence"];
    }
    hidden = NO;
    if ([JsonService isRBUserVisitor] || [LocalService getRBLocalDataUserPrivateToken]==nil) {
        [[NSNotificationCenter defaultCenter]postNotificationName:NotificationShowLoginView object:nil];
    }
    
    beginTime = 1;
    if(self.kolId == nil){
        [self.hudView showOverlay];
        Handler * hand = [Handler shareHandler];
        hand.delegate = self;
        [hand getRBbindSocialAccounts:self.kolId andProvider:@"weibo"];
    }else if (self.kolId.length > 0){
        [self.hudView showOverlay];
        Handler * hand = [Handler shareHandler];
        hand.delegate = self;
        [hand getOtherRBbindSocialAccounts:self.kolId andProvider:@"weibo"];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    if ([_mark isEqualToString:@"mine"]) {
        [TalkingData trackPageEnd:@"My_influence"];
    }else{
        [TalkingData trackPageEnd:@"Other people_influence"];
    }
    
    [codeTimer invalidate];
    codeTimer = nil;
    [backScroll removeFromSuperview];
    [upInfluenceBtn removeFromSuperview];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = YES;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(gotoRead) name:@"toFindVC" object:nil];
}
- (void)gotoRead{
    self.tabBarController.selectedIndex = 2;
}
#pragma mark ShowLoginViewNotification

#pragma rbInfluenceDelegate
-(void)RBInfluenceViewClick{
    UpInfluenceViewController * upInfluenceVC = [[UpInfluenceViewController alloc]init];
    upInfluenceVC.hidesBottomBarWhenPushed = YES;
    upInfluenceVC.Entity = entity;
    [self.navigationController pushViewController:upInfluenceVC animated:YES];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat h = scrollView.contentOffset.y;
    if (h>180) {
        upInfluenceBtn.hidden = NO;
    }else{
        upInfluenceBtn.hidden = YES;
    }
}
-(void)RBNavLeftBtnAction{
    if ([self.mark isEqualToString:@"first"]) {
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate loadViewController:33];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)RBNavRightBtnAction{
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.delegate = self;
    alert.shareTitle = @"快来Robin8测试你的社交账号影响力吧!";
    alert.shareContent = @"更有海量现金活动，还能和好友PK社交影响力吆";
    alert.shareImageUrl = @"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png";
    [alert showWithShareArray:nil];
}
-(void)loadtopView{
   
    backScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-self.tabBarController.tabBar.size.height)];
    backScroll.showsVerticalScrollIndicator = NO;
    backScroll.showsHorizontalScrollIndicator = NO;
    backScroll.delegate = self;
    backScroll.bounces = NO;
    if (hidden == YES) {
        backScroll.alpha = 0;
    }else{
        backScroll.alpha = 1;
    }
    [self.view addSubview:backScroll];

    if ([_mark isEqualToString:@"mine"]) {
        upInfluenceBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 68, 180+NavHeight, 79, 23) title:@"提升影响力" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysBackColorYellow] image:nil hlImage:nil];
        [upInfluenceBtn addTarget:self action:@selector(upInfluence:) forControlEvents:UIControlEventTouchUpInside];
        upInfluenceBtn.layer.cornerRadius = 23/2;
        upInfluenceBtn.titleLabel.font = font_11;
        upInfluenceBtn.hidden = YES;
//        [self.view addSubview:upInfluenceBtn];
        UIWindow * window = [[UIApplication sharedApplication].windows lastObject];
        [window addSubview:upInfluenceBtn];
    }
    //
    if ([_mark isEqualToString:@"mine"]) {
        influView = [[RBInfluenceView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 180)];
        influView.scoreText = [NSString stringWithFormat:@"%@",entity.influence_score];
        influView.influenceText = [NSString stringWithFormat:@"%@",entity.influence_level];
        influView.rate = (float)[entity.influence_score intValue]/1000;
        influView.category = @"2";
        influView.delegate = self;
        [backScroll addSubview:influView];
    }else{
        influView = [[RBInfluenceView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 150)];
        influView.scoreText = [NSString stringWithFormat:@"%@",entity.influence_score];
        influView.influenceText = [NSString stringWithFormat:@"%@",entity.influence_level];
        influView.rate = (float)[entity.influence_score intValue]/1000;
        influView.category = @"3";
        influView.delegate = self;
        [backScroll addSubview:influView];
    }
    //
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, influView.bottom, ScreenWidth, 0.5)];
    lineView.backgroundColor = [Utils getUIColorWithHexString:SysColorBlack];
    lineView.alpha = 0.1;
    [backScroll addSubview:lineView];
    //
    if ([_mark isEqualToString:@"mine"]) {
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(15, lineView.bottom+20, ScreenWidth-30, 14) text:NSLocalizedString(@"R5227", @"根据您的兴趣,推荐以下用户") font:font_cu_14 textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:ccColor1a1a1a] backgroundColor:[UIColor clearColor] numberOfLines:1];
        [backScroll addSubview:label];
        //
        userScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, label.bottom+15, ScreenWidth, 52)];
        userScroll.showsHorizontalScrollIndicator = NO;
        userScroll.showsVerticalScrollIndicator = NO;
        userScroll.bounces = NO;
        [backScroll addSubview:userScroll];
        
        
        if ([_mark isEqualToString:@"mine"]) {
            for (NSInteger i =0; i<entity.similar_kols.count+1; i++) {
                UIView * view = [[UIView alloc]initWithFrame:CGRectMake(15+i*(52+15), 0, 52, 52)];
                view.backgroundColor = [UIColor whiteColor];
                view.layer.cornerRadius = 26;
                view.layer.masksToBounds = YES;
                view.layer.borderWidth = 1;
                view.userInteractionEnabled = YES;
                view.layer.borderColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.17].CGColor;
                [userScroll addSubview:view];
                UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(3, 3, 46, 46)];
                imageView.layer.cornerRadius = 23;
                imageView.clipsToBounds = YES;
                imageView.userInteractionEnabled = YES;
                [view addSubview:imageView];
                if (i == 0) {
                    imageView.image = [UIImage imageNamed:@"AddFriend"];
                    //
                    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
                    tap.numberOfTapsRequired = 1;
                    tap.numberOfTouchesRequired = 1;
                    [imageView addGestureRecognizer:tap];
                }else{
                    RBSimilarEntity * sEntity = entity.similar_kols[i-1];
                    imageView.tag = [sEntity.iid integerValue];
                    [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",sEntity.avatar_url]] placeholderImage:PlaceHolderUserImage];
                    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(friendAction:)];
                    tap.numberOfTapsRequired = 1;
                    tap.numberOfTouchesRequired = 1;
                    [imageView addGestureRecognizer:tap];
                }
            }
        }
        userScroll.contentSize = CGSizeMake(15+(entity.similar_kols.count + 1)*(52+15), 52);
     }
    
}
-(void)friendAction:(UITapGestureRecognizer*)tap{
    UIImageView * imageView = (UIImageView*)tap.view;
    RBInfluenceDetailViewController * vc = [[RBInfluenceDetailViewController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    vc.kolId = [NSString stringWithFormat:@"%ld",(long)imageView.tag];
    //进入推荐人影响力界面，将我的影响力数据传过去，以便在PK界面做比较
    vc.myEntity = entity;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)tapAction:(UITapGestureRecognizer*)tap{
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.delegate = self;
    [alert showWithNewArray:@[@"查看网红KOL影响力",@"查看社交好友影响力",@"取消"]];
}
-(void)RBNewAlert:(RBNewAlert *)alertView clickedButtonAtIndex:(int)buttonIndex{
    if (buttonIndex == 0) {
        RBKolSearchViewController *toview = [[RBKolSearchViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }else if (buttonIndex == 1){
        [self shareSDKWithTitle:@"快来Robin8测试你的社交账号影响力吧!" Content:@"更有海量现金活动，还能和好友PK社交影响力吆" URL:[NSString stringWithFormat:@"%@invite?inviter_id=%@",ApiUrl,[LocalService getRBLocalDataUserLoginId]] ImgURL:@"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png" SSDKPlatformType:SSDKPlatformSubTypeWechatSession];
    }
}
- (void)shareSDKWithTitle:(NSString *)titleStr
                  Content:(NSString *)contentStr
                      URL:(NSString *)urlStr
                   ImgURL:(NSString *)imgUrlStr
         SSDKPlatformType:(SSDKPlatformType)type{
    [SVProgressHUD show];
    NSLog(@"title:%@,content:%@,img:%@,url:%@",titleStr,contentStr,imgUrlStr,urlStr);
    if (type==SSDKPlatformTypeSinaWeibo) {
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:[NSString stringWithFormat:@"%@ %@",titleStr,urlStr]
                                         images:imgUrlStr
                                            url:[NSURL URLWithString:urlStr]
                                          title:[NSString stringWithFormat:@"%@ %@",titleStr,urlStr]
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            if (state == SSDKResponseStateSuccess) {
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
            }
            if (state == SSDKResponseStateFail) {
                if (error.code == 204) {
                    [SVProgressHUD showErrorWithStatus:@"相同的内容不能重复分享"];
                } else {
                    if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                        [SVProgressHUD showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                    } else {
                        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
                    }
                }
            }
            if (state == SSDKResponseStateCancel) {
                [SVProgressHUD dismiss];
                //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
            }
        }];
    } else {
        [[SDWebImageDownloader sharedDownloader]downloadImageWithURL:[NSURL URLWithString:imgUrlStr] options:SDWebImageDownloaderHighPriority progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            if(finished==NO) {
                NSLog(@"图片下载失败:%@,%@",image,data);
            } else {
                image = [Utils narrowWithImage:image];
            }
            NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
            [shareParams SSDKSetupShareParamsByText:contentStr
                                             images:image
                                                url:[NSURL URLWithString:urlStr]
                                              title:titleStr
                                               type:SSDKContentTypeWebPage];
            [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
                if (state == SSDKResponseStateSuccess) {
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
                }
                if (state == SSDKResponseStateFail) {
                    if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                        [SVProgressHUD showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                    } else {
                        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
                    }
                }
                if (state == SSDKResponseStateCancel) {
                    [SVProgressHUD dismiss];
                    //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
                }
            }];
        }];
    }
    
}

-(void)loadContentView{
    NSArray * arr = @[@"airline",@"appliances",@"auto",@"babies",@"beauty",@"books",@"camera",@"ce",@"digita",@"education",@"entertainment",@"fashion",@"finance",@"fitness",@"food",@"furniture",@"games",@"health",@"hotel",@"internet",@"mobile",@"music",@"other",@"realestate",@"sports",@"travel"];
    NSArray * arr1 = @[@"航空",@"家电",@"汽车",@"母婴",@"美妆",@"图书",@"摄影",@"ce",@"digita",@"教育",@"娱乐",@"时尚",@"财经",@"健康",@"美食",@"家居",@"游戏",@"健康",@"酒店",@"互联网",@"手机",@"音乐",@"其他",@"房地产",@"健身",@"旅游"];
    //
    UIView * topView = [[UIView alloc]initWithFrame:CGRectMake(0,userScroll.bottom+19, ScreenWidth, 8)];
    if ([_mark isEqualToString:@"mine"]) {
        topView.frame = CGRectMake(0,userScroll.bottom+19, ScreenWidth, 8);
    }else{
        topView.frame = CGRectMake(0,influView.bottom+0.5, ScreenWidth, 8);
    }
    topView.backgroundColor = [Utils getUIColorWithHexString:ccColorf5f5f5];
    [backScroll addSubview:topView];
    //
    subScroll = [[RBInfluenScrollView alloc]initWithFrame:CGRectMake(0, topView.bottom, ScreenWidth, 50.5+163.5+3*144-2*61 +28)];
    subScroll.tableView.scrollEnabled = NO;
    subScroll.delegate = self;
    if ([_mark isEqualToString:@"mine"]) {
        if (entity.industries.count == 0) {
            //如果类数小于3条
            subScroll.categary = @"noenough";
            [subScroll.bigView.headView.headImageView sd_setImageWithURL:[NSURL URLWithString:entity.avatar_url] placeholderImage:[UIImage imageNamed:@"icon_user_default"]];
            subScroll.bigView.headView.nameLabel.text = entity.name;
            subScroll.bigView.headView.describeLabel.text = entity.Idescription;
            subScroll.bigView.headView.commentLabel.text = entity.avg_comments;
            subScroll.bigView.headView.shareLabel.text = entity.avg_posts;
            subScroll.bigView.headView.zanLabel.text = [NSString stringWithFormat:@"%@",entity.avg_likes];
            subScroll.bigView.headView.toOthers.hidden = YES;
            
        }else if(entity.industries.count>0){
        NSMutableArray * industryArray = [[NSMutableArray alloc]init];
        for (NSInteger i = 0; i<entity.industries.count; i++) {
            RBNewIndustriyEntity * industrityEntity = entity.industries[i];
            
            [industryArray addObject:industrityEntity];
        }
        for (NSInteger i = 0; i<industryArray.count; i++) {
            for (NSInteger j = 0; j<industryArray.count-1; j++) {
                RBNewIndustriyEntity * industrityEntity1 = industryArray[j];
                RBNewIndustriyEntity * industrityEntity2 = industryArray[j+1];
                if ([industrityEntity1.industry_score integerValue]<[industrityEntity2.industry_score integerValue]) {
                    [industryArray exchangeObjectAtIndex:j withObjectAtIndex:j+1];
                }
            }
        }
        //
        dataArray = [[NSMutableArray alloc]init];
        for (NSInteger i = 0; i<industryArray.count; i++) {
            RBNewIndustriyEntity * industryEntity = industryArray[i];
            for (NSInteger j = 0; j<arr.count; j++) {
                if ([industryEntity.industry_name isEqualToString:arr[j]]) {
                    industryEntity.industry_name = arr1[j];
                }
            }
            [dataArray addObject:industryEntity];
        }
        if (dataArray.count>0&&dataArray.count<3) {
            subScroll.frame = CGRectMake(0, topView.bottom, ScreenWidth, 50.5+163.5+dataArray.count*144-(dataArray.count-1)*61 +28);
        }else{
            subScroll.frame = CGRectMake(0, topView.bottom, ScreenWidth, 50.5+163.5+3*144-2*61 +28);
        }
        if (dataArray.count>0&&dataArray.count<3) {
            subScroll.count = dataArray.count;
        }else{
            subScroll.count = 3;
        }
        subScroll.categary = @"data";
        subScroll.bigView.dataSource = dataArray;
        [subScroll.bigView.headView.headImageView sd_setImageWithURL:[NSURL URLWithString:entity.avatar_url] placeholderImage:[UIImage imageNamed:@"icon_user_default"]];
        subScroll.bigView.headView.nameLabel.text = entity.name;
        subScroll.bigView.headView.describeLabel.text = entity.Idescription;
        subScroll.bigView.headView.commentLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_comments floatValue]*100)/100];
        subScroll.bigView.headView.shareLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_posts floatValue]*100)/100];
        subScroll.bigView.headView.zanLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_likes floatValue]*100)/100];
    }
    }else{
        if (entity.industries.count == 0) {
            //如果类数小于3条
            subScroll.categary = @"noenough";
            [subScroll.bigView.headView.headImageView sd_setImageWithURL:[NSURL URLWithString:entity.avatar_url] placeholderImage:[UIImage imageNamed:@"icon_user_default"]];
            subScroll.bigView.headView.nameLabel.text = entity.name;
            subScroll.bigView.headView.describeLabel.text = entity.Idescription;
            subScroll.bigView.headView.commentLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_comments floatValue]*100)/100];
            subScroll.bigView.headView.shareLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_posts floatValue]*100)/100];
            subScroll.bigView.headView.zanLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_likes floatValue]*100)/100];
            subScroll.bigView.headView.toOthers.hidden = YES;
            subScroll.bigView.noenoughView.noEnoughLabel.text = @"Robin8需要更多微博内容来分析影响力，目前内容不足以分析出Ta的兴趣爱好";
        }else if ([entity.influence_score_visibility integerValue] ==1){
            if (entity.industries.count>0) {
                
            NSMutableArray * industryArray = [[NSMutableArray alloc]init];
            for (NSInteger i = 0; i<entity.industries.count; i++) {
                RBNewIndustriyEntity * industrityEntity = entity.industries[i];
                
                [industryArray addObject:industrityEntity];
            }
            for (NSInteger i = 0; i<industryArray.count; i++) {
                for (NSInteger j = 0; j<industryArray.count-1; j++) {
                    RBNewIndustriyEntity * industrityEntity1 = industryArray[j];
                    RBNewIndustriyEntity * industrityEntity2 = industryArray[j+1];
                    if ([industrityEntity1.industry_score integerValue]<[industrityEntity2.industry_score integerValue]) {
                        [industryArray exchangeObjectAtIndex:j withObjectAtIndex:j+1];
                    }
                }
            }
            //
            dataArray = [[NSMutableArray alloc]init];
            for (NSInteger i = 0; i<industryArray.count; i++) {
                RBNewIndustriyEntity * industryEntity = industryArray[i];
                for (NSInteger j = 0; j<arr.count; j++) {
                    if ([industryEntity.industry_name isEqualToString:arr[j]]) {
                        industryEntity.industry_name = arr1[j];
                    }
                }
                [dataArray addObject:industryEntity];
            }
           // subScroll.frame = CGRectMake(0, topView.bottom, ScreenWidth, 50.5+163.5+dataArray.count*144-(dataArray.count-1)*61 +28);
           
            //
                if (dataArray.count>0&&dataArray.count<3) {
                    subScroll.frame = CGRectMake(0, topView.bottom, ScreenWidth, 50.5+163.5+dataArray.count*144-(dataArray.count-1)*61 +28);
                }else{
                    subScroll.frame = CGRectMake(0, topView.bottom, ScreenWidth, 50.5+163.5+3*144-2*61 +28);
                }
                if (dataArray.count>0&&dataArray.count<3) {
                    subScroll.count = dataArray.count;
                }else{
                    subScroll.count = 3;
                }
 
            //
            subScroll.categary = @"data";
            subScroll.bigView.dataSource = dataArray;
            [subScroll.bigView.headView.headImageView sd_setImageWithURL:[NSURL URLWithString:entity.avatar_url] placeholderImage:[UIImage imageNamed:@"icon_user_default"]];
            subScroll.bigView.headView.nameLabel.text = entity.name;
            subScroll.bigView.headView.describeLabel.text = entity.Idescription;
            subScroll.bigView.headView.commentLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_comments floatValue]*100)/100];
            subScroll.bigView.headView.shareLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_posts floatValue]*100)/100];
            subScroll.bigView.headView.zanLabel.text = [NSString stringWithFormat:@"%.2f",roundf([entity.avg_likes floatValue]*100)/100];
            subScroll.bigView.headView.toOthers.hidden = YES;
          }
        }else if ([entity.influence_score_visibility integerValue] == 0){
            if ([self.mark isEqualToString:@"mine"]) {
                self.navTitle = NSLocalizedString(@"R5197", @"影响力");
                self.navLeftTitle = @"空";
            }else{
                self.navTitle = NSLocalizedString(@"R5223", @"Ta的影响力");
            }
            self.navRightTitle = @"share";
            self.delegate = self;
            //设置了权限
            subScroll.categary = @"nosee";
            [subScroll.permitView.headImageView sd_setImageWithURL:[NSURL URLWithString:entity.avatar_url] placeholderImage:[UIImage imageNamed:@"icon_user_default"]];
            subScroll.permitView.nameLabel.text = entity.name;
            subScroll.permitView.describeLabel.text = entity.Idescription;
        }
    }
   
    //点击对他人可见触发的方法
    __weak __typeof(self)weakSelf = self;
    subScroll.bigView.permitBlock = ^(){
        Handler * handle = [Handler shareHandler];
        handle.delegate = weakSelf;
        if ([entity.influence_score_visibility integerValue] == 1) {
            [handle getRBPermitToothers:@"off"];
        }else{
            [handle getRBPermitToothers:@"on"];
        }
    };
    [backScroll addSubview:subScroll];
    if ([entity.influence_score_visibility integerValue] == 1) {
        [subScroll.bigView.headView.toOthers setTitle:@"对他人可见" forState:UIControlStateNormal];
    }else{
        [subScroll.bigView.headView.toOthers setTitle:@"对他人隐藏" forState:UIControlStateNormal];
    }
}
-(void)changeRBInfluenScrollViewFrame:(BOOL)isFold{
    if ([_mark isEqualToString:@"mine"]) {
    if (isFold == YES) {
        subScroll.frame = CGRectMake(0,userScroll.bottom+19+8, ScreenWidth, subScroll.frame.size.height-61);
        footView.frame = CGRectMake(0, subScroll.bottom, ScreenWidth, footView.frame.size.height);
        backScroll.contentSize = CGSizeMake(ScreenWidth, footView.bottom+10);
    }else if (isFold == NO){
        subScroll.frame = CGRectMake(0, userScroll.bottom+19+8, ScreenWidth, subScroll.frame.size.height+61);
        footView.frame = CGRectMake(0, subScroll.bottom, ScreenWidth, footView.frame.size.height);
        backScroll.contentSize = CGSizeMake(ScreenWidth, footView.bottom+10);
    }
    }else{
        influView.frame = CGRectMake(0, 0, ScreenWidth, 150);
        if (isFold == YES) {
            subScroll.frame = CGRectMake(0,influView.bottom+0.5+8, ScreenWidth, subScroll.frame.size.height-61);
            footView.frame = CGRectMake(0, subScroll.bottom, ScreenWidth, footView.frame.size.height);
            backScroll.contentSize = CGSizeMake(ScreenWidth, footView.bottom+10);
        }else if (isFold == NO){
            subScroll.frame = CGRectMake(0, influView.bottom+0.5+8, ScreenWidth, subScroll.frame.size.height+61);
            footView.frame = CGRectMake(0, subScroll.bottom, ScreenWidth, footView.frame.size.height);
            backScroll.contentSize = CGSizeMake(ScreenWidth, footView.bottom+10);
        }
    }
//    [self.view bringSubviewToFront:upInfluenceBtn];

}
//点击微信按钮
-(void)changFrameWhenClickWechat{
    if ([_mark isEqualToString:@"mine"]) {
        subScroll.frame = CGRectMake(0, userScroll.bottom+19+8, ScreenWidth, 50.5+163.5+3*144-3*61 +28);
    }else{
        influView.frame = CGRectMake(0, 0, ScreenWidth, 150);
        subScroll.frame = CGRectMake(0, influView.bottom+0.5+8, ScreenWidth, 50.5+163.5+3*144-3*61 +28);
    }
    footView.frame = CGRectMake(0, subScroll.bottom, ScreenWidth, footView.frame.size.height);
    backScroll.contentSize = CGSizeMake(ScreenWidth, footView.bottom+10);
//    [self.view bringSubviewToFront:upInfluenceBtn];
}
//点击微博按钮
-(void)changeFrameWhenClickWeibo{
    if ([_mark isEqualToString:@"mine"]) {
        if (dataArray.count>0&&dataArray.count<3) {
            subScroll.frame = CGRectMake(0, userScroll.bottom+19+8, ScreenWidth, 50.5+163.5+dataArray.count*144-dataArray.count*61 +28);
        }else{
            subScroll.frame = CGRectMake(0, userScroll.bottom+19+8, ScreenWidth, 50.5+163.5+3*144-3*61 +28);
        }
    }else{
        if (dataArray.count>0&&dataArray.count<3) {
            influView.frame = CGRectMake(0, 0, ScreenWidth, 150);
            subScroll.frame = CGRectMake(0, influView.bottom+0.5+8, ScreenWidth, 50.5+163.5+dataArray.count*144-dataArray.count*61 +28);
        }else{
            influView.frame = CGRectMake(0, 0, ScreenWidth, 150);
            subScroll.frame = CGRectMake(0, influView.bottom+0.5+8, ScreenWidth, 50.5+163.5+3*144-3*61 +28);
        }
    }
    footView.frame = CGRectMake(0, subScroll.bottom, ScreenWidth, footView.frame.size.height);
    backScroll.contentSize = CGSizeMake(ScreenWidth, footView.bottom+10);
//    [self.view bringSubviewToFront:upInfluenceBtn];

}
-(void)loadFooterView{
    
    footView = [[UIView alloc]initWithFrame:CGRectMake(0, subScroll.bottom, ScreenWidth, 0)];
    footView.backgroundColor = [UIColor whiteColor];
    [backScroll addSubview:footView];
    //
    UIView * bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 8)];
    bottomView.backgroundColor = [Utils getUIColorWithHexString:ccColorf5f5f5];
    [footView addSubview:bottomView];
    //
    UILabel * upLabel = [[UILabel alloc]initWithFrame:CGRectMake(15,bottomView.bottom+25, ScreenWidth-15, 14) text:@"" font:font_cu_14 textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:ccColor1a1a1a] backgroundColor:[UIColor clearColor] numberOfLines:1];
    if ([_mark isEqualToString:@"mine"]) {
        upLabel.text = NSLocalizedString(@"R5225", @"我的兴趣");
    }else{
        upLabel.text = NSLocalizedString(@"R5226", @"Ta的兴趣");
    }
    [footView addSubview:upLabel];
    //
    NSArray * arr = @[@"airline",@"appliances",@"auto",@"babies",@"beauty",@"books",@"camera",@"ce",@"digita",@"education",@"entertainment",@"fashion",@"finance",@"fitness",@"food",@"furniture",@"games",@"health",@"hotel",@"internet",@"mobile",@"music",@"other",@"realestate",@"sports",@"travel"];
    NSArray * arr1 = @[@"航空",@"家电",@"汽车",@"母婴",@"美妆",@"图书",@"摄影",@"消费电子",@"数码",@"教育",@"娱乐",@"时尚",@"财经",@"健康",@"美食",@"家居",@"游戏",@"健康",@"酒店",@"互联网",@"手机",@"音乐",@"其他",@"房地产",@"健身",@"旅游"];
    NSMutableArray * industryArray = [[NSMutableArray alloc]init];
    for (NSInteger i = 0; i<entity.industries.count; i++) {
        RBNewIndustriyEntity * industrityEntity = entity.industries[i];
        
        [industryArray addObject:industrityEntity];
    }
    for (NSInteger i = 0; i<industryArray.count; i++) {
        for (NSInteger j = 0; j<industryArray.count-1; j++) {
            RBNewIndustriyEntity * industrityEntity1 = industryArray[j];
            RBNewIndustriyEntity * industrityEntity2 = industryArray[j+1];
            if ([industrityEntity1.industry_score integerValue]<[industrityEntity2.industry_score integerValue]) {
                [industryArray exchangeObjectAtIndex:j withObjectAtIndex:j+1];
            }
        }
    }
    NSMutableArray * nameArray = [[NSMutableArray alloc]init];
    NSMutableArray * scoreArray = [[NSMutableArray alloc]init];
    if (industryArray.count>=8) {
        for (NSInteger i = 0; i<8; i++) {
            RBNewIndustriyEntity * industryEntity = industryArray[i];
            for (NSInteger j = 0; j<arr.count; j++) {
                if ([industryEntity.industry_name isEqualToString:arr[j]]) {
                    industryEntity.industry_name = arr1[j];
                }
            }
            [nameArray addObject:industryEntity.industry_name];
            if ([industryEntity.industry_score intValue] > 100) {
                industryEntity.industry_score = @"100";
            }
            [scoreArray addObject:industryEntity.industry_score];
        }
 
    }else{
        for (NSInteger i = 0; i<industryArray.count; i++) {
        RBNewIndustriyEntity * industryEntity = industryArray[i];
        for (NSInteger j = 0; j<arr.count; j++) {
                if ([industryEntity.industry_name isEqualToString:arr[j]]) {
                    industryEntity.industry_name = arr1[j];
                }
            }
            [nameArray addObject:industryEntity.industry_name];
            if ([industryEntity.industry_score intValue] > 100) {
                industryEntity.industry_score = @"100";
            }
            [scoreArray addObject:industryEntity.industry_score];
        }
    NSMutableArray * nameArray1 = [[NSMutableArray alloc]initWithArray:arr1];
    if (nameArray.count<8) {
        for (NSInteger i = 0; i<nameArray.count; i++) {
            [nameArray1 removeObject:nameArray[i]];
        }
        NSInteger count = nameArray.count;
        for (NSInteger z = 0; z<8-count; z++) {
            [nameArray addObject:nameArray1[z]];
            [scoreArray addObject:@"0"];
        }
    }
    }
    //
    NSArray*array = nameArray;
    NSArray*array1 = scoreArray;
    radarView= [[JYRadarChart alloc] initWithFrame:CGRectMake(0, upLabel.bottom+10, ScreenWidth, 250)];
    radarView.textColor = [Utils getUIColorWithHexString:ccColor565656];
    //[Utils getUIColorWithHexString:ccColorffc701]
    radarView.attributes = array;
    radarView.dataSeries = @[array1];
    
    radarView.r = 90;
    radarView.steps = 5;
    radarView.drawPoints = YES;
    [footView addSubview:radarView];
    footView.height = radarView.bottom+10;
    backScroll.contentSize = CGSizeMake(ScreenWidth, footView.bottom+10);
    
    //
    if (![_mark isEqualToString:@"mine"]) {
        UIButton * uploadBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, ScreenHeight-50, ScreenWidth, 50) title:NSLocalizedString(@"R5235", @"我不服，比一比") hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysBackColorBlue] image:nil hlImage:nil];
        uploadBtn.titleLabel.font = font_cu_cu_17;
        [uploadBtn addTarget:self action:@selector(PKAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:uploadBtn];
    }
    [UIView animateWithDuration:1 animations:^{
        backScroll.alpha = 1.0;
    }];
    
}
//和好友PK
-(void)PKAction:(UIButton*)sender{
    InfluenceController * influence = [[InfluenceController alloc]init];
    influence.hidesBottomBarWhenPushed = YES;
    influence.kolId = self.kolId;
    influence.myEntity = self.myEntity;
    influence.hisEntity = entity;
    [self.navigationController pushViewController:influence animated:YES];
}
//提升影响力
-(void)upInfluence:(UIButton*)sender{
    UpInfluenceViewController * upInfluenceVC = [[UpInfluenceViewController alloc]init];
    upInfluenceVC.hidesBottomBarWhenPushed = YES;
    upInfluenceVC.Entity = entity;
    [self.navigationController pushViewController:upInfluenceVC animated:YES];
}
-(void)updateData{
   
    
}
//倒计时触发的方法
-(void)beginRequestInfluence{
    if(self.kolId == nil){
        Handler * hand = [Handler shareHandler];
        hand.delegate = self;
        [hand getRBbindSocialAccounts:self.kolId andProvider:@"weibo"];
    }else if (self.kolId.length > 0){
        Handler * hand = [Handler shareHandler];
        hand.delegate = self;
        [hand getOtherRBbindSocialAccounts:self.kolId andProvider:@"weibo"];
    }
}
-(void)timeLimit{
    [self beginRequestInfluence];
}
-(void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    if ([sender isEqualToString:@"calculate_influence_score"]) {
        Handler * handle = [Handler shareHandler];
        handle.delegate = self;
        [handle getRBTestInfluence];
    }
    if ([sender isEqualToString:@"influence_score"]){
        [self.hudView dismiss];
        if ([[jsonObject objectForKey:@"calculated"]boolValue] == 0) {
            [waitView removeFromSuperview];
            hidden = YES;
            if ([self.mark isEqualToString:@"mine"]) {
                //进入我的影响力测评界面未出结果
                self.navTitle = NSLocalizedString(@"R5224", @"测评结果");
                self.navLeftTitle = @"空";
                self.navRightTitle = @"空";
                self.delegate = self;
                if (beginTime == 1) {
                    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                               target  :self
                                                               selector:@selector(timeLimit)
                                                               userInfo:nil
                                                               repeats :YES];
                    beginTime ++;
                }
            }else{
                //进入别人的影响力测评界面未出结果
                self.navTitle = NSLocalizedString(@"R5223", @"Ta的影响力");
                self.navRightTitle = @"空";
                self.delegate = self;
                if (beginTime == 1) {
                    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                               target  :self
                                                               selector:@selector(timeLimit)
                                                               userInfo:nil
                                                               repeats :YES];
                    beginTime ++;
                }
            }
            //
            //
            [backScroll removeAllSubviews];
            [upInfluenceBtn removeFromSuperview];
            waitView = [[[NSBundle mainBundle]loadNibNamed:@"RBTestWaitView" owner:nil options:nil]lastObject];
            waitView.frame = CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-49);
            if ([_mark isEqualToString:@"mine"]) {
                waitView.identity = @"mine";
            }else{
                waitView.identity = @"him";
            }
            [self.view addSubview:waitView];
        }else{
            //请求到数据停止计时器
            [codeTimer invalidate];
            codeTimer = nil;
            if ([self.mark isEqualToString:@"mine"]) {
                self.navTitle = NSLocalizedString(@"R5197", @"影响力");
                self.navLeftTitle = @"空";
            }else{
                self.navTitle = NSLocalizedString(@"R5223", @"Ta的影响力");
            }
            self.navRightTitle = @"share";
            self.delegate = self;
        entity = [JsonService getRBNewInfluenceEntity:jsonObject];
        [waitView removeFromSuperview];
        [backScroll removeFromSuperview];
        [upInfluenceBtn removeFromSuperview];
        [self loadtopView];
        [self loadContentView];
        [self loadFooterView];
        }
    }
    if ([sender isEqualToString:@"manage_influence_visibility"]) {
        if ([entity.influence_score_visibility integerValue] == 1) {
            entity.influence_score_visibility = @"0";
        }else{
            entity.influence_score_visibility = @"1";
        }
        if ([entity.influence_score_visibility integerValue] == 1) {
            [subScroll.bigView.headView.toOthers setTitle:@"对他人可见" forState:UIControlStateNormal];
        }else{
            [subScroll.bigView.headView.toOthers setTitle:@"对他人隐藏" forState:UIControlStateNormal];
        }
    }
    if ([sender isEqualToString:@"similar_kol_details"]) {
        [self.hudView dismiss];
        if ([[jsonObject objectForKey:@"calculated"]boolValue] == 0) {
            [waitView removeFromSuperview];
            [upInfluenceBtn removeFromSuperview];
            if ([self.mark isEqualToString:@"mine"]) {
                //进入我的影响力测评界面未出结果
                self.navTitle = NSLocalizedString(@"R5224", @"测评结果");
                self.navLeftTitle = @"空";
                self.navRightTitle = @"空";
                self.delegate = self;
//                if (beginTime == 1) {
//                    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1
//                                                               target  :self
//                                                               selector:@selector(timeLimit)
//                                                               userInfo:nil
//                                                               repeats :YES];
//                    beginTime ++;
//                }

            }else{
                //进入别人的影响力测评界面未出结果
                self.navTitle = NSLocalizedString(@"R5223", @"Ta的影响力");
                self.navRightTitle = @"空";
                self.delegate = self;
//                if (beginTime == 1) {
//                    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1
//                                                               target  :self
//                                                               selector:@selector(timeLimit)
//                                                               userInfo:nil
//                                                               repeats :YES];
//                    beginTime ++;
//                }

            }
            //
            //
            [backScroll removeAllSubviews];
            waitView = [[[NSBundle mainBundle]loadNibNamed:@"RBTestWaitView" owner:nil options:nil]lastObject];
            waitView.frame = CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-49);
            if ([_mark isEqualToString:@"mine"]) {
                waitView.identity = @"mine";
            }else{
                waitView.identity = @"him";
            }
            [self.view addSubview:waitView];
        }else{
            [codeTimer invalidate];
            codeTimer = nil;
            if ([self.mark isEqualToString:@"mine"]) {
                self.navTitle = NSLocalizedString(@"R5197", @"影响力");
                self.navLeftTitle = @"空";
            }else{
                self.navTitle = NSLocalizedString(@"R5223", @"Ta的影响力");
            }
            self.navRightTitle = @"share";
            self.delegate = self;
            entity = [JsonService getRBNewInfluenceEntity:jsonObject];
            [waitView removeFromSuperview];
            [backScroll removeFromSuperview];
            [upInfluenceBtn removeFromSuperview];
            [self loadtopView];
            [self loadContentView];
            [self loadFooterView];
        }

    }
}
-(void)handlerError:(NSError *)error Tag:(NSString *)sender{
    if ([_mark isEqualToString:@"mine"]) {
        self.navTitle = NSLocalizedString(@"R5224", @"测评结果");
        self.navLeftTitle = @"空";
        self.navRightTitle = @"空";
        self.delegate = self;
    }else{
        self.navTitle = NSLocalizedString(@"R5224", @"测评结果");
        self.navRightTitle = @"空";
        self.delegate = self;
    }
    
    //
    [backScroll removeAllSubviews];
    [waitView removeFromSuperview];
    
    waitView = [[[NSBundle mainBundle]loadNibNamed:@"RBTestWaitView" owner:nil options:nil]lastObject];
    waitView.frame = CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-49);
    if ([_mark isEqualToString:@"mine"]) {
        waitView.identity = @"mine";
    }else{
        waitView.identity = @"him";
    }
    [self.view addSubview:waitView];
}
-(void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender{
    if ([_mark isEqualToString:@"mine"]) {
        self.navTitle = NSLocalizedString(@"R5224", @"测评结果");
        self.navLeftTitle = @"空";
        self.navRightTitle = @"空";
        self.delegate = self;
    }else{
        self.navTitle = NSLocalizedString(@"R5224", @"测评结果");
        self.navRightTitle = @"空";
        self.delegate = self;
    }
    //
    [backScroll removeAllSubviews];
    [waitView removeFromSuperview];
    waitView = [[[NSBundle mainBundle]loadNibNamed:@"RBTestWaitView" owner:nil options:nil]lastObject];
    waitView.frame = CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-49);
    if ([_mark isEqualToString:@"mine"]) {
        waitView.identity = @"mine";
    }else{
        waitView.identity = @"him";
    }
    [self.view addSubview:waitView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
