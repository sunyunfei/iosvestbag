//
//  RBInfluenceArticleViewController.h
//  RB
//
//  Created by AngusNi on 7/7/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBArticleView.h"
#import "RBArticleDetailViewController.h"
@interface RBInfluenceArticleViewController : RBBaseViewController
<RBBaseVCDelegate,RBArticleViewDelegate>

@end
