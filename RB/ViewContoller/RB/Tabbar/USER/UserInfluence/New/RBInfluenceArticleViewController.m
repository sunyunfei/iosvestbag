//
//  RBInfluenceArticleViewController.m
//  RB
//
//  Created by AngusNi on 7/7/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBInfluenceArticleViewController.h"

@interface RBInfluenceArticleViewController () {
    RBArticleView*articleView;
}
@end

@implementation RBInfluenceArticleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5211", @"我的分享");
    //
    articleView = [[RBArticleView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight -NavHeight)];
    articleView.delegate = self;
    [self.view addSubview:articleView];
    //
    [self.hudView show];
    [articleView RBArticleViewLoadRefreshViewFirstData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-influence-share-article"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-influence-share-article"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationRefreshArticleView {
    [articleView RBArticleViewLoadRefreshViewFirstData];
}

#pragma mark - RBCampaignView Delegate
-(void)RBArticleViewLoadRefreshViewData:(RBArticleView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBArticleForwardWithPage:[NSString stringWithFormat:@"%d",articleView.pageIndex+1]];
}

- (void)RBArticleViewSelected:(RBArticleView *)view andIndex:(int)index {
    RBArticleEntity*entity = view.datalist[index];
    RBArticleDetailViewController*toview = [[RBArticleDetailViewController alloc] init];
    toview.entity = entity;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"forward"]) {
        [self.hudView dismiss];
        if (articleView.pageIndex == 0) {
            articleView.datalist = [NSMutableArray new];
        }
        articleView.datalist = [JsonService getRBArticleActionsForwardListEntity:jsonObject andBackArray:articleView.datalist];
        [articleView RBArticleViewSetRefreshViewFinish];
        //
        NSString *total_pages = [jsonObject objectForKey:@"total_pages"];
        if(total_pages.intValue <= articleView.pageIndex) {
            articleView.tableviewn.mj_footer = nil;
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [articleView RBArticleViewSetRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [articleView RBArticleViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
