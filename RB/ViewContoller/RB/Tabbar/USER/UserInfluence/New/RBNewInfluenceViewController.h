//
//  RBNewInfluenceViewController.h
//  RB
//
//  Created by AngusNi on 7/6/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInfluenceView.h"
//
#import "DXPopover.h"
//
#import "RBInfluenceBundingViewController.h"
//
#import "RBInfluenceIncreaseViewController.h"
//
#import "RBInfluenceRankingViewController.h"
#import "RBInfluenceArticleViewController.h"

@interface RBNewInfluenceViewController : RBBaseViewController
<RBBaseVCDelegate,MCLineChartViewDataSource,MCLineChartViewDelegate,MCCircleChartViewDataSource,MCCircleChartViewDelegate>

@end