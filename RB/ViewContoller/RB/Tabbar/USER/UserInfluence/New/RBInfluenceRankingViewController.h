//
//  RBInfluenceRankingViewController.h
//  RB
//
//  Created by AngusNi on 7/7/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBNewInfluenceRankingTableViewCell.h"

@interface RBInfluenceRankingViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate>
@end
