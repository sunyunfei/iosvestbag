//
//  RBNewInfluenceViewController.m
//  RB
//
//  Created by AngusNi on 7/6/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBNewInfluenceViewController.h"
#import "RBTabUserViewController.h"
#import "RBAppDelegate.h"

@interface RBNewInfluenceViewController () {
    RBInfluenceView*influenceView;
    NSMutableArray*titlesArray;
    NSMutableArray*dataArray;
    RBInfluenceReadEntity*readEntity;
    MCLineChartView*lineChartView;
    UIScrollView*scrollviewn;
    NSString*maxScore;
}
@end

@implementation RBNewInfluenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5197", @"影响力");
    self.bgIV.hidden = NO;
    self.navView.backgroundColor = [UIColor clearColor];
    self.navLineLabel.hidden = YES;
    //
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    // RB-获取用户信息
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfo];
    //
    [TalkingData trackPageBegin:@"my-influence"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-influence"];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth , 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerLeftBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerLeftBtn.frame = CGRectMake(0, 0, footerView.width/2.0, footerView.height);
    [footerLeftBtn addTarget:self
                      action:@selector(retestBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
    footerLeftBtn.titleLabel.font = font_cu_15;
    [footerLeftBtn setTitle:NSLocalizedString(@"R5198", @"重新测试") forState:UIControlStateNormal];
    [footerLeftBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerLeftBtn];
    //
    UIButton*footerRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerRightBtn.frame = CGRectMake(footerLeftBtn.right, 0, footerLeftBtn.width, footerLeftBtn.height);
    [footerRightBtn addTarget:self
                       action:@selector(shareBtnAction:)
             forControlEvents:UIControlEventTouchUpInside];
    footerRightBtn.titleLabel.font = font_cu_15;
    [footerRightBtn setTitle:NSLocalizedString(@"R2033", @"立即分享") forState:UIControlStateNormal];
    [footerRightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerRightBtn];
    //
    UIView*lineView = [[UIView alloc]initWithFrame:CGRectMake(footerView.width/2.0-0.5, 12.0, 1.0, footerView.height-24.0)];
    lineView.backgroundColor = [UIColor whiteColor];
    [footerView addSubview:lineView];
}

#pragma mark - UIButton Delegate
- (void)retestBtnAction:(UIButton *)sender {
    RBInfluenceBundingViewController *toview = [[RBInfluenceBundingViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)aboutBtnAction:(UIButton *)sender {
    self.navigationController.navigationBarHidden = NO;
    RBUserSettingChatViewController *toview = [[RBUserSettingChatViewController alloc] init];
    toview.conversationType = ConversationType_APPSERVICE;
    toview.targetId = RCIMServiceID;
    toview.title = NSLocalizedString(@"R5107", @"Robin8在线客服");
    toview.hidesBottomBarWhenPushed = YES;
    toview.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)shareBtnAction:(UIButton *)sender {
    if([NSString stringWithFormat:@"%@",readEntity.influenceEntity.influence_score].intValue==-1) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5064", @"未进行社交影响力评测")];
        return;
    }
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBInfluenceShare];
    //
    RBShareView *shareView = [RBShareView sharedRBShareView];
    [shareView showViewWithTitle:[NSString stringWithFormat:@"我在Robin8的影响力分数是%@,敢不敢和我PK一下?",readEntity.influenceEntity.influence_score] Content:@"Robin8基于大数据的社交影响力平台" URL:[NSString stringWithFormat:@"%@invite?inviter_id=%@",ApiUrl,[LocalService getRBLocalDataUserLoginId]] ImgURL:@"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png"];
}

- (void)RBNavLeftBtnAction {
    for (UIViewController *temp in self.navigationController.viewControllers){
        if ([temp isKindOfClass:[RBTabUserViewController class]]) {
            if ([self.navigationController.viewControllers count]>1) {
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
        }
    }
}

- (void)firstBtnAction:(UIButton *)sender {
    RBInfluenceIncreaseViewController *toview = [[RBInfluenceIncreaseViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)firstBtn1Action:(UIButton *)sender {
    RBInfluenceRankingViewController *toview = [[RBInfluenceRankingViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)firstBtn2Action:(UIButton *)sender {
    RBInfluenceArticleViewController *toview = [[RBInfluenceArticleViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)showContentWith:(UIButton*)sender andString:(NSString*)str {
    UIView*contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 135, 57)];
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0, 115, 57)];
    textLabel.font = font_11;
    textLabel.numberOfLines = 0;
    textLabel.text = str;
    textLabel.textColor = [UIColor whiteColor];
    [contentView addSubview:textLabel];
    DXPopover *popover = [DXPopover popover];
    popover.maskType = DXPopoverMaskTypeNone;
    [popover showAtView:sender
         popoverPostion:DXPopoverPositionDown
        withContentView:contentView inView:scrollviewn];
    
}

- (void)secondBtnAction:(UIButton *)sender {
    [self showContentWith:sender andString:NSLocalizedString(@"R5199", @"根据您的地区，社交账号情况为您的身份特征进行评分")];
}

- (void)secondBtn1Action:(UIButton *)sender {
    [self showContentWith:sender andString:NSLocalizedString(@"R5200", @"根据您的好友信息为您的人脉关系进行评分")];
}

- (void)secondBtn2Action:(UIButton *)sender {
    [self showContentWith:sender andString:NSLocalizedString(@"R5201", @"根据您的阅读和分享情况进行相关因素的评分")];
}

- (void)secondBtn3Action:(UIButton *)sender {
    [self showContentWith:sender andString:NSLocalizedString(@"R5202", @"根据您参与活动的情况为您的影响力进行评分")];
}

- (void)secondBtn4Action:(UIButton *)sender {
    [self showContentWith:sender andString:NSLocalizedString(@"R5203", @"根据您的微博活跃程度为您的影响力进行评分")];
}

- (void)weiboBtnAction:(UIButton *)sender {
    [self ThirdLoginWithTag:5000];
}

- (void)wechatBtnAction:(UIButton *)sender {
    [self ThirdLoginWithTag:5001];
}

- (void)qqBtnAction:(UIButton *)sender {
    [self ThirdLoginWithTag:5002];
}

- (void)ThirdLoginWithTag:(int)tag {
    SSDKPlatformType type;
    NSString*provider=@"";
    if (tag == 5000) {
        provider = @"weibo";
        type = SSDKPlatformTypeSinaWeibo;
    } else if (tag == 5001) {
        provider = @"wechat";
        type = SSDKPlatformTypeWechat;
    } else {
        provider = @"qq";
        type = SSDKPlatformTypeQQ;
    }
    [ShareSDK cancelAuthorize:type];
    [ShareSDK getUserInfo:type
           onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error){
               NSLog(@"user:%@,credential:%@",user,user.credential);
               if (state == SSDKResponseStateSuccess) {
                   [self.hudView show];
                   // RB-社交账号绑定
                   Handler*handler = [Handler shareHandler];
                   handler.delegate = self;
                   if (type == SSDKPlatformTypeWechat) {
                       NSString*unionid = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"unionid"]];
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:unionid andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                   }
                   if (type == SSDKPlatformTypeSinaWeibo) {
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*followers_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"followers_count"]];
                       NSString*created_at = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"created_at"]];
                       NSString*statuses_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"statuses_count"]];
                       NSString*verified = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"verified"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"avatar_large"]];
                       NSString*refresh_token = nil;
                       NSString*str = [NSString stringWithFormat:@"%@",user.credential];
                       if([[str componentsSeparatedByString:@"\"refresh_token\" = \""] count]>1) {
                           str = [str componentsSeparatedByString:@"\"refresh_token\" = \""][1];
                           if([[str componentsSeparatedByString:@"\";"] count]>1) {
                               refresh_token = [str componentsSeparatedByString:@"\";"][0];
                           }
                       }
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:followers_count andStatuses_count:statuses_count andRegistered_at:created_at andVerified:verified andRefresh_token:refresh_token andUnionid:nil andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                   }
                   if (type == SSDKPlatformTypeQQ) {
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*city = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"city"]];
                       NSString*is_yellow_vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"is_yellow_vip"]];
                       NSString*vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"vip"]];
                       NSString*province = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"province"]];
                       NSString*gender = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"gender"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"figureurl_qq_2"]];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:nil andProvince:province andCity:city andGender:gender andIs_vip:vip andIs_yellow_vip:is_yellow_vip];
                   }
               }
               if (state == SSDKResponseStateFail) {
                   [self.hudView showErrorWithStatus:NSLocalizedString(@"R1042", @"登录失败")];
               }
               if (state == SSDKResponseStateCancel) {
                   [self.hudView dismiss];
                   //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
               }
           }];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    [scrollviewn removeAllSubviews];
    [scrollviewn removeFromSuperview];
    scrollviewn = nil;
    scrollviewn = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    [scrollviewn setShowsHorizontalScrollIndicator:NO];
    [scrollviewn setShowsVerticalScrollIndicator:NO];
    scrollviewn.userInteractionEnabled = YES;
    [self.view addSubview:scrollviewn];
    //
    UIView*firstView = [[UIView alloc]initWithFrame:CGRectMake(0, 60.0, ScreenWidth, 150)];
    firstView.backgroundColor = [UIColor whiteColor];
    
    [scrollviewn addSubview:firstView];
    //
    UIImageView*userIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-80.0)/2.0, 20.0, 80.0, 80.0)];
    [userIV sd_setImageWithURL:[NSURL URLWithString:readEntity.influenceEntity.avatar_url] placeholderImage:PlaceHolderUserImage];
    userIV.layer.cornerRadius = 3.0;
    userIV.layer.masksToBounds = YES;
    [scrollviewn addSubview:userIV];
    //
    UILabel*userLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 40.0, ScreenWidth, 20.0)];
    userLabel.text = readEntity.influenceEntity.name;
    userLabel.font = font_15;
    userLabel.textAlignment = NSTextAlignmentCenter;
    userLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [firstView addSubview:userLabel];
    //
    UIButton*wechatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [wechatBtn addTarget:self action:@selector(wechatBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [wechatBtn setBackgroundImage:[UIImage imageNamed:@"icon_wechat_l.png"] forState:UIControlStateNormal];
    wechatBtn.frame = CGRectMake((ScreenWidth-30.0*2.0-42.0*3.0)/2.0, userLabel.bottom+20.0, 42.0, 42.0);
    [firstView addSubview:wechatBtn];
    //
    UIButton*weiboBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [weiboBtn addTarget:self action:@selector(weiboBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [weiboBtn setBackgroundImage:[UIImage imageNamed:@"icon_weibo_l.png"] forState:UIControlStateNormal];
    weiboBtn.frame = CGRectMake(wechatBtn.right+30.0, wechatBtn.top, wechatBtn.width, wechatBtn.height);
    [firstView addSubview:weiboBtn];
    //
    UIButton*qqBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [qqBtn addTarget:self action:@selector(qqBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [qqBtn setBackgroundImage:[UIImage imageNamed:@"icon_qq_l.png"] forState:UIControlStateNormal];
    qqBtn.frame = CGRectMake(weiboBtn.right+30.0, wechatBtn.top, wechatBtn.width, wechatBtn.height);
    [firstView addSubview:qqBtn];
    for(RBThirdEntity *entity in readEntity.identities) {
        if ([entity.provider isEqualToString:@"qq"]) {
            [qqBtn setBackgroundImage:[UIImage imageNamed:@"icon_qq.png"] forState:UIControlStateNormal];
        }
        if ([entity.provider isEqualToString:@"weibo"]) {
            [weiboBtn setBackgroundImage:[UIImage imageNamed:@"icon_weibo.png"] forState:UIControlStateNormal];
        }
        if ([entity.provider isEqualToString:@"wechat"]) {
            [wechatBtn setBackgroundImage:[UIImage imageNamed:@"icon_wechat.png"] forState:UIControlStateNormal];
        }
    }
    //
    UIView*firstLineView = [[UIView alloc]initWithFrame:CGRectMake(0, qqBtn.bottom+20.0, ScreenWidth, 0.5f)];
    firstLineView.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [firstView addSubview:firstLineView];
    //
    UILabel*firstLeftTLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, firstLineView.bottom+18.0, ScreenWidth/3.0, 20.0)];
    firstLeftTLabel.text = readEntity.influenceEntity.influence_score;
    firstLeftTLabel.font = font_(20);
    firstLeftTLabel.textAlignment = NSTextAlignmentCenter;
    firstLeftTLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [firstView addSubview:firstLeftTLabel];
    //
    UILabel*firstLeftCLabel = [[UILabel alloc]initWithFrame:CGRectMake(firstLeftTLabel.left, firstLeftTLabel.bottom, firstLeftTLabel.width, 15.0)];
    firstLeftCLabel.text = NSLocalizedString(@"R5204", @"影响力分数");
    firstLeftCLabel.font = font_11;
    firstLeftCLabel.textAlignment = NSTextAlignmentCenter;
    firstLeftCLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [firstView addSubview:firstLeftCLabel];
    //
    UILabel*firstCenterTLabel = [[UILabel alloc]initWithFrame:CGRectMake(firstLeftTLabel.right, firstLeftTLabel.top, firstLeftTLabel.width, firstLeftTLabel.height)];
    firstCenterTLabel.text = readEntity.rank_index;
    firstCenterTLabel.font = firstLeftTLabel.font;
    firstCenterTLabel.textAlignment = NSTextAlignmentCenter;
    firstCenterTLabel.textColor = firstLeftTLabel.textColor;
    [firstView addSubview:firstCenterTLabel];
    //
    UILabel*firstCenterCLabel = [[UILabel alloc]initWithFrame:CGRectMake(firstCenterTLabel.left, firstLeftCLabel.top, firstLeftCLabel.width, firstLeftCLabel.height)];
    firstCenterCLabel.text = NSLocalizedString(@"R5205", @"好友排名");
    firstCenterCLabel.font = firstLeftCLabel.font;
    firstCenterCLabel.textAlignment = NSTextAlignmentCenter;
    firstCenterCLabel.textColor = firstLeftCLabel.textColor;
    [firstView addSubview:firstCenterCLabel];
    //
    UILabel*firstRightTLabel = [[UILabel alloc]initWithFrame:CGRectMake(firstCenterTLabel.right, firstLeftTLabel.top, firstLeftTLabel.width, firstLeftTLabel.height)];
    firstRightTLabel.text = readEntity.foward_read_count;
    firstRightTLabel.font = firstLeftTLabel.font;
    firstRightTLabel.textAlignment = NSTextAlignmentCenter;
    firstRightTLabel.textColor = firstLeftTLabel.textColor;
    [firstView addSubview:firstRightTLabel];
    //
    UILabel*firstRightCLabel = [[UILabel alloc]initWithFrame:CGRectMake(firstRightTLabel.left, firstLeftCLabel.top, firstLeftCLabel.width, firstLeftCLabel.height)];
    firstRightCLabel.text = NSLocalizedString(@"R5206", @"分享被阅读数");
    firstRightCLabel.font = firstLeftCLabel.font;
    firstRightCLabel.textAlignment = NSTextAlignmentCenter;
    firstRightCLabel.textColor = firstLeftCLabel.textColor;
    [firstView addSubview:firstRightCLabel];
    //
    firstView.height = firstRightCLabel.bottom+18.0;
    //
    UIButton*firstBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [firstBtn addTarget:self action:@selector(firstBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    firstBtn.frame = CGRectMake(0, firstView.height-70.0, firstView.width/3.0, 70.0);
    [firstView addSubview:firstBtn];
    //
    UIButton*firstBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [firstBtn1 addTarget:self action:@selector(firstBtn1Action:) forControlEvents:UIControlEventTouchUpInside];
    firstBtn1.frame = CGRectMake(firstBtn.right, firstBtn.top, firstBtn.width, firstBtn.height);
    [firstView addSubview:firstBtn1];
    //
    UIButton*firstBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [firstBtn2 addTarget:self action:@selector(firstBtn2Action:) forControlEvents:UIControlEventTouchUpInside];
    firstBtn2.frame = CGRectMake(firstBtn1.right, firstBtn.top, firstBtn.width, firstBtn.height);
    [firstView addSubview:firstBtn2];
    //
    UIView*secondView = [[UIView alloc]initWithFrame:CGRectMake(0, firstView.bottom+CellBottom, ScreenWidth, 150)];
    secondView.backgroundColor = [UIColor whiteColor];
    [scrollviewn addSubview:secondView];
    //
    NSArray*array = @[NSLocalizedString(@"R5056", @"身份特质"),NSLocalizedString(@"R5057", @"活跃程度"),NSLocalizedString(@"R5058", @"参与悬赏"),NSLocalizedString(@"R5059", @"佳文分享"),NSLocalizedString(@"R5060", @"人脉关系")];
    NSArray*array1 = @[[NSString stringWithFormat:@"%.2f",readEntity.feature_rate.floatValue],[NSString stringWithFormat:@"%.2f",readEntity.active_rate.floatValue],[NSString stringWithFormat:@"%.2f",readEntity.campaign_rate.floatValue],[NSString stringWithFormat:@"%.2f",readEntity.share_rate.floatValue],[NSString stringWithFormat:@"%.2f",readEntity.contact_rate.floatValue]];
    //
    JYRadarChart*radarView= [[JYRadarChart alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 305.0*ScreenWidth/325.0-40.0)];
    radarView.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    radarView.attributes = array;
    radarView.dataSeries = @[array1];
    radarView.r = ScreenWidth*90.0/320.0;
    [secondView addSubview:radarView];
    //
    UILabel*spiderLabel = [[UILabel alloc]initWithFrame:radarView.bounds];
    spiderLabel.text = [NSString stringWithFormat:@"%@",readEntity.influenceEntity.influence_score];
    spiderLabel.font = font_cu_30;
    spiderLabel.textAlignment = NSTextAlignmentCenter;
    spiderLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    [secondView addSubview:spiderLabel];
    //
    UIView*secondLineView = [[UIView alloc]initWithFrame:CGRectMake(0, radarView.bottom, ScreenWidth, 0.5f)];
    secondLineView.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [secondView addSubview:secondLineView];
    //
    UILabel*secondTLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, secondLineView.bottom+15.0, ScreenWidth/5.0, 20.0)];
    secondTLabel.text = [NSString stringWithFormat:@"%d",(int)(readEntity.feature_score.floatValue)];
    secondTLabel.font = font_(20);
    secondTLabel.textAlignment = NSTextAlignmentCenter;
    secondTLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [secondView addSubview:secondTLabel];
    //
    UILabel*secondCLabel = [[UILabel alloc]initWithFrame:CGRectMake(secondTLabel.left, secondTLabel.bottom, secondTLabel.width, 15.0)];
    secondCLabel.text = NSLocalizedString(@"R5056", @"身份特质");
    secondCLabel.font = font_11;
    secondCLabel.textAlignment = NSTextAlignmentCenter;
    secondCLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [secondView addSubview:secondCLabel];
    //
    UILabel*secondTLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(secondTLabel.right, secondTLabel.top, secondTLabel.width, secondTLabel.height)];
    secondTLabel1.text = [NSString stringWithFormat:@"%d",(int)(readEntity.contact_score.floatValue)];
    secondTLabel1.font = secondTLabel.font;
    secondTLabel1.textAlignment = NSTextAlignmentCenter;
    secondTLabel1.textColor = secondTLabel.textColor;
    [secondView addSubview:secondTLabel1];
    //
    UILabel*secondCLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(secondTLabel1.left, secondCLabel.top, secondTLabel.width, secondCLabel.height)];
    secondCLabel1.text = NSLocalizedString(@"R5060", @"人脉关系");
    secondCLabel1.font = secondCLabel.font;
    secondCLabel1.textAlignment = NSTextAlignmentCenter;
    secondCLabel1.textColor = secondCLabel.textColor;
    [secondView addSubview:secondCLabel1];
    //
    UILabel*secondTLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(secondTLabel1.right, secondTLabel.top, secondTLabel.width, secondTLabel.height)];
    secondTLabel2.text = [NSString stringWithFormat:@"%d",(int)(readEntity.share_score.floatValue)];
    secondTLabel2.font = secondTLabel.font;
    secondTLabel2.textAlignment = NSTextAlignmentCenter;
    secondTLabel2.textColor = secondTLabel.textColor;
    [secondView addSubview:secondTLabel2];
    //
    UILabel*secondCLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(secondTLabel2.left, secondCLabel.top, secondTLabel.width, secondCLabel.height)];
    secondCLabel2.text = NSLocalizedString(@"R5059", @"佳文分享");
    secondCLabel2.font = secondCLabel.font;
    secondCLabel2.textAlignment = NSTextAlignmentCenter;
    secondCLabel2.textColor = secondCLabel.textColor;
    [secondView addSubview:secondCLabel2];
    //
    UILabel*secondTLabel3 = [[UILabel alloc]initWithFrame:CGRectMake(secondTLabel2.right, secondTLabel.top, secondTLabel.width, secondTLabel.height)];
    secondTLabel3.text = [NSString stringWithFormat:@"%d",(int)(readEntity.campaign_score.floatValue)];
    secondTLabel3.font = secondTLabel.font;
    secondTLabel3.textAlignment = NSTextAlignmentCenter;
    secondTLabel3.textColor = secondTLabel.textColor;
    [secondView addSubview:secondTLabel3];
    //
    UILabel*secondCLabel3 = [[UILabel alloc]initWithFrame:CGRectMake(secondTLabel3.left, secondCLabel.top, secondTLabel.width, secondCLabel.height)];
    secondCLabel3.text = NSLocalizedString(@"R5058", @"参与悬赏");
    secondCLabel3.font = secondCLabel.font;
    secondCLabel3.textAlignment = NSTextAlignmentCenter;
    secondCLabel3.textColor = secondCLabel.textColor;
    [secondView addSubview:secondCLabel3];
    //
    UILabel*secondTLabel4 = [[UILabel alloc]initWithFrame:CGRectMake(secondTLabel3.right, secondTLabel.top, secondTLabel.width, secondTLabel.height)];
    secondTLabel4.text = [NSString stringWithFormat:@"%d",(int)(readEntity.active_score.floatValue)];
    secondTLabel4.font = secondTLabel.font;
    secondTLabel4.textAlignment = NSTextAlignmentCenter;
    secondTLabel4.textColor = secondTLabel.textColor;
    [secondView addSubview:secondTLabel4];
    //
    UILabel*secondCLabel4 = [[UILabel alloc]initWithFrame:CGRectMake(secondTLabel4.left, secondCLabel.top, secondTLabel.width, secondCLabel.height)];
    secondCLabel4.text = NSLocalizedString(@"R5057", @"活跃程度");
    secondCLabel4.font = secondCLabel.font;
    secondCLabel4.textAlignment = NSTextAlignmentCenter;
    secondCLabel4.textColor = secondCLabel.textColor;
    [secondView addSubview:secondCLabel4];
    //
    secondView.height = secondCLabel.bottom+15.0;
    //
    UIButton*secondBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [secondBtn addTarget:self action:@selector(secondBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    secondBtn.frame = CGRectMake(0, secondView.top+secondView.height-70.0, secondView.width/5.0, 70.0);
    [scrollviewn addSubview:secondBtn];
    //
    UIButton*secondBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [secondBtn1 addTarget:self action:@selector(secondBtn1Action:) forControlEvents:UIControlEventTouchUpInside];
    secondBtn1.frame = CGRectMake(secondBtn.right, secondBtn.top, secondBtn.width, secondBtn.height);
    [scrollviewn addSubview:secondBtn1];
    //
    UIButton*secondBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [secondBtn2 addTarget:self action:@selector(secondBtn2Action:) forControlEvents:UIControlEventTouchUpInside];
    secondBtn2.frame = CGRectMake(secondBtn1.right, secondBtn.top, secondBtn.width, secondBtn.height);
    [scrollviewn addSubview:secondBtn2];
    //
    UIButton*secondBtn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [secondBtn3 addTarget:self action:@selector(secondBtn3Action:) forControlEvents:UIControlEventTouchUpInside];
    secondBtn3.frame = CGRectMake(secondBtn2.right, secondBtn.top, secondBtn.width, secondBtn.height);
    [scrollviewn addSubview:secondBtn3];
    //
    UIButton*secondBtn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    [secondBtn4 addTarget:self action:@selector(secondBtn4Action:) forControlEvents:UIControlEventTouchUpInside];
    secondBtn4.frame = CGRectMake(secondBtn3.right, secondBtn.top, secondBtn.width, secondBtn.height);
    [scrollviewn addSubview:secondBtn4];
    //
    UIView*thirdView = [[UIView alloc]initWithFrame:CGRectMake(0, secondView.bottom+CellBottom, ScreenWidth, 150)];
    thirdView.backgroundColor = [UIColor whiteColor];
    [scrollviewn addSubview:thirdView];
    //
    lineChartView = [[MCLineChartView alloc] initWithFrame:CGRectMake(0, 20, ScreenWidth, 200)];
    lineChartView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.5];
    lineChartView.dataSource = self;
    lineChartView.delegate = self;
    lineChartView.minValue = @0;
    lineChartView.solidDot = YES;
    lineChartView.numberOfYAxis = 3;
    lineChartView.xFontSize = 10.0;
    lineChartView.yFontSize = 10.0;
    lineChartView.colorOfXAxis = [UIColor clearColor];
    lineChartView.colorOfXText = [Utils getUIColorWithHexString:SysColorSubGray];
    lineChartView.colorOfYAxis = [UIColor clearColor];
    lineChartView.colorOfYText = [Utils getUIColorWithHexString:SysColorSubGray];
    [thirdView addSubview:lineChartView];
    if (maxScore.intValue!=0) {
        lineChartView.maxValue = [NSString stringWithFormat:@"%.2f",maxScore.floatValue*1.2];
    } else {
        lineChartView.maxValue = @600;
    }
    [lineChartView reloadData];
    //
    UIView*thirdTitleLineView = [[UIView alloc]initWithFrame:CGRectMake(12, 13.0, 3.0, 14.0)];
    thirdTitleLineView.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [thirdView addSubview:thirdTitleLineView];
    //
    UILabel*thirdTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(thirdTitleLineView.right+thirdTitleLineView.left, 0, ScreenWidth, 40.0)];
    thirdTitleLabel.text = NSLocalizedString(@"R5061", @"近期影响力趋势");
    thirdTitleLabel.font = font_cu_13;
    thirdTitleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [thirdView addSubview:thirdTitleLabel];
    //
    UIView*thirdLineView = [[UIView alloc]initWithFrame:CGRectMake(0, thirdTitleLabel.bottom, ScreenWidth, 0.5f)];
    thirdLineView.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [thirdView addSubview:thirdLineView];
    //
    NSString*str = [NSString stringWithFormat:@"%@",readEntity.diff_score];
    if([[str componentsSeparatedByString:@"比上周增加了"] count]>1){
        str = [str componentsSeparatedByString:@"比上周增加了"][1];
        str = [str componentsSeparatedByString:@"分"][0];
    }
    NSString*str1 = [NSString stringWithFormat:@"%@",readEntity.diff_score];
    if([[str1 componentsSeparatedByString:@"影响力分数"] count]>1){
        str1 = [str1 componentsSeparatedByString:@"影响力分数"][1];
        str1 = [str1 componentsSeparatedByString:@"分 比上周增加了"][0];
    }
    UILabel*influenceScoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, lineChartView.bottom,ScreenWidth, 30.0)];
    influenceScoreLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5187", @"影响力分数%@分 比上次增加了%@分"),str1,str];
    influenceScoreLabel.font = font_cu_13;
    influenceScoreLabel.textAlignment = NSTextAlignmentCenter;
    influenceScoreLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
    [thirdView addSubview:influenceScoreLabel];
    //
    thirdView.height = influenceScoreLabel.bottom+20.0;
    //
    UIView*fourthView = [[UIView alloc]initWithFrame:CGRectMake(0, thirdView.bottom+CellBottom, ScreenWidth, 150)];
    fourthView.backgroundColor = [UIColor whiteColor];
    [scrollviewn addSubview:fourthView];
    //
    UIView*fourthTitleLineView = [[UIView alloc]initWithFrame:CGRectMake(12, 13.0, 3.0, 14.0)];
    fourthTitleLineView.backgroundColor = thirdTitleLineView.backgroundColor;
    [fourthView addSubview:fourthTitleLineView];
    //
    UILabel*fourthTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(fourthTitleLineView.right+fourthTitleLineView.left, 0, ScreenWidth, 40.0)];
    fourthTitleLabel.text = NSLocalizedString(@"R5207", @"我的标签云");
    fourthTitleLabel.font = thirdTitleLabel.font;
    fourthTitleLabel.textColor = thirdTitleLabel.textColor;
    [fourthView addSubview:fourthTitleLabel];
    //
    UIView*fourthLineView = [[UIView alloc]initWithFrame:CGRectMake(0, fourthTitleLabel.bottom, ScreenWidth, 0.5f)];
    fourthLineView.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [fourthView addSubview:fourthLineView];
    //
    DBSphereView *sphereView = [[DBSphereView alloc] initWithFrame:CGRectMake(40.0, fourthLineView.bottom+20.0, ScreenWidth-80.0, ScreenWidth-80.0)];
    NSMutableArray *sphereArray = [[NSMutableArray alloc] initWithCapacity:0];
    for (NSInteger i = 0; i < readEntity.wordclouds.count; i ++) {
        UILabel*tagsLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 20)];
        tagsLabel.text = readEntity.wordclouds[i];
        tagsLabel.font = font_30;
        tagsLabel.textAlignment = NSTextAlignmentCenter;
        tagsLabel.textColor = [Utils getUIColorRandom];
        [sphereArray addObject:tagsLabel];
        CGSize tagsLabelSize = [Utils getUIFontSizeFitW:tagsLabel];
        tagsLabel.width = tagsLabelSize.width;
        [sphereView addSubview:tagsLabel];
    }
    [sphereView setCloudTags:sphereArray];
    [fourthView addSubview:sphereView];
    //
    UIButton*sphereBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sphereBtn addTarget:self action:@selector(weiboBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    sphereBtn.frame = CGRectMake(sphereView.left+(sphereView.width-115)/2.0, sphereView.top+(sphereView.height-35.0)/2.0, 115, 35);
    [sphereBtn setBackgroundImage:[UIImage imageNamed:@"icon_weibo_btn.png"] forState:UIControlStateNormal];
    [fourthView addSubview:sphereBtn];
    //
    UILabel*sphereLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, sphereBtn.bottom, ScreenWidth, 18)];
    sphereLabel.text = @"授权已过期，请重新绑定";
    sphereLabel.font = font_11;
    sphereLabel.textAlignment = NSTextAlignmentCenter;
    sphereLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [fourthView addSubview:sphereLabel];
    //
    fourthView.height = sphereView.bottom+20.0;
    //
    UIView*fifthView = [[UIView alloc]initWithFrame:CGRectMake(0, fourthView.bottom+CellBottom, ScreenWidth, 150)];
    fifthView.backgroundColor = [UIColor whiteColor];
    [scrollviewn addSubview:fifthView];
    //
    UIView*fifthTitleLineView = [[UIView alloc]initWithFrame:CGRectMake(12, 13.0, 3.0, 14.0)];
    fifthTitleLineView.backgroundColor = thirdTitleLineView.backgroundColor;
    [fifthView addSubview:fifthTitleLineView];
    //
    UILabel*fifthTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(fourthTitleLineView.right+fourthTitleLineView.left, 0, ScreenWidth, 40.0)];
    fifthTitleLabel.text = NSLocalizedString(@"R5208", @"我的行业偏好");
    fifthTitleLabel.font = thirdTitleLabel.font;
    fifthTitleLabel.textColor = thirdTitleLabel.textColor;
    [fifthView addSubview:fifthTitleLabel];
    //
    UIView*fifthLineView = [[UIView alloc]initWithFrame:CGRectMake(0, fifthTitleLabel.bottom, ScreenWidth, 0.5f)];
    fifthLineView.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [fifthView addSubview:fifthLineView];
    //
    MCCircleChartView*circleChartView = [[MCCircleChartView alloc] initWithFrame:CGRectMake(30.0, fifthLineView.bottom, ScreenWidth-60.0, ScreenWidth-60.0)];
    circleChartView.introduceColor = [Utils getUIColorWithHexString:SysColorGray];
    circleChartView.maxRadius = 120;
    circleChartView.circleWidth = 20;
    circleChartView.dataSource = self;
    circleChartView.delegate = self;
    [fifthView addSubview:circleChartView];
    [circleChartView reloadDataWithAnimate:YES];
    //
    UIButton*circleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [circleBtn addTarget:self action:@selector(weiboBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    circleBtn.frame = CGRectMake(circleChartView.left+(circleChartView.width-115)/2.0, circleChartView.top+(circleChartView.height-35.0)/2.0, 115, 35);
    [circleBtn setBackgroundImage:[UIImage imageNamed:@"icon_weibo_btn.png"] forState:UIControlStateNormal];
    [fifthView addSubview:circleBtn];
    //
    UILabel*circleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, circleBtn.bottom, ScreenWidth, 18)];
    circleLabel.text = @"授权已过期，请重新绑定";
    circleLabel.font = font_11;
    circleLabel.textAlignment = NSTextAlignmentCenter;
    circleLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [fifthView addSubview:circleLabel];
    //
    circleChartView.hidden = YES;
    circleBtn.hidden = NO;
    circleLabel.hidden = NO;
    sphereView.hidden = YES;
    sphereBtn.hidden = NO;
    sphereLabel.hidden = NO;
    for(RBThirdEntity *entity in readEntity.identities) {
        if ([entity.provider isEqualToString:@"weibo"]) {
            if ([readEntity.weibo_identity_status isEqualToString:@"active"]) {
                circleChartView.hidden = NO;
                circleBtn.hidden = YES;
                circleLabel.hidden = YES;
                sphereView.hidden = NO;
                sphereBtn.hidden = YES;
                sphereLabel.hidden = YES;
            } else {
                circleChartView.hidden = YES;
                circleBtn.hidden = NO;
                circleLabel.hidden = NO;
                sphereView.hidden = YES;
                sphereBtn.hidden = NO;
                sphereLabel.hidden = NO;
            }
        } else {
            sphereLabel.hidden = YES;
            circleLabel.hidden = YES;
        }
    }
    //
    fifthView.height = circleChartView.bottom;
    //
    TTTAttributedLabel *aboutLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(0, fifthView.bottom, ScreenWidth, 50.0)];
    aboutLabel.font = font_11;
    aboutLabel.textAlignment = NSTextAlignmentCenter;
    aboutLabel.textColor = [Utils getUIColorWithHexString:SysColorSubBlack];
    aboutLabel.text = NSLocalizedString(@"R5209", @"数据不准确？有什么问题 立即反馈 给我们");
    [scrollviewn addSubview:aboutLabel];
    [aboutLabel setText:aboutLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:NSLocalizedString(@"R5210", @"立即反馈") options:NSCaseInsensitiveSearch];
         [mStr
          addAttribute:(NSString *)kCTForegroundColorAttributeName
          value:(id)[[Utils getUIColorWithHexString:SysColorBlue] CGColor]
          range:range];
         [mStr
          addAttribute:(NSString *)kCTUnderlineStyleAttributeName
          value:[NSNumber numberWithBool:YES]
          range:range];
         return mStr;
     }];
    //
    UIButton *aboutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    aboutBtn.frame = aboutLabel.frame;
    [aboutBtn addTarget:self
                 action:@selector(aboutBtnAction:)
       forControlEvents:UIControlEventTouchUpInside];
    [scrollviewn addSubview:aboutBtn];
    //
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, aboutBtn.bottom)];
}


#pragma mark - MCCircleChartView Delegate
- (NSInteger)numberOfCircleInCircleChartView:(MCCircleChartView *)circleChartView {
    if([readEntity.categories count]>6) {
        return 6;
    }
    return [readEntity.categories count];
}

- (id)circleChartView:(MCCircleChartView *)circleChartView valueOfCircleAtIndex:(NSInteger)index {
    RBRankingTagsEntity*entity = readEntity.categories[index];
    return [NSString stringWithFormat:@"%f",[[NSString stringWithFormat:@"%@",entity.weight]floatValue]*100.0];
}

- (NSString *)circleChartView:(MCCircleChartView *)circleChartView introduceAtIndex:(NSInteger)index {
    RBRankingTagsEntity*entity = readEntity.categories[index];
    return [NSString stringWithFormat:@"%@:%@", entity.text,[NSString stringWithFormat:@"%.1f%%",[[NSString stringWithFormat:@"%@",entity.weight]floatValue]*100.0]];
}

- (UIColor *)circleChartView:(MCCircleChartView *)circleChartView colorOfCircleAtIndex:(NSInteger)index {
    NSArray*array = @[@"BCE784",@"5DD39E",@"348AA7",@"525174",@"513B56",@"BCE784",@"5DD39E",@"348AA7",@"525174",@"513B56"];
    return [Utils getUIColorWithHexString:array[index]];
}

#pragma mark - MCLineChartView Delegate
- (void)lineChartView:(MCLineChartView *)lineChartView selectedAt:(int)index {
}

- (NSUInteger)numberOfLinesInLineChartView:(MCLineChartView *)lineChartView {
    return 1;
}

- (NSUInteger)lineChartView:(MCLineChartView *)lineChartView lineCountAtLineNumber:(NSInteger)number {
    return [dataArray count];
}

- (id)lineChartView:(MCLineChartView *)lineChartView valueAtLineNumber:(NSInteger)lineNumber index:(NSInteger)index {
    return dataArray[index];
}

- (NSString *)lineChartView:(MCLineChartView *)lineChartView titleAtLineNumber:(NSInteger)number {
    return titlesArray[number];
}

- (UIColor *)lineChartView:(MCLineChartView *)lineChartView lineColorWithLineNumber:(NSInteger)lineNumber {
    return [Utils getUIColorWithHexString:SysColorYellow];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"my_analysis"]) {
        [self.hudView dismiss];
        readEntity = [JsonService getRBInfluenceReadEntity:jsonObject and:readEntity];
        [self loadEditView];
    }
    
    if ([sender isEqualToString:@"influences"]) {
        [self.hudView dismiss];
        readEntity = [JsonService getRBInfluenceReadEntity:jsonObject];
        //
        titlesArray = [NSMutableArray new];
        dataArray = [NSMutableArray new];
        maxScore = @"0";
        for (int i=0; i<[readEntity.history count]; i++) {
            RBInfluenceHistoryEntity *entity = readEntity.history[i];
            if ([[[NSString stringWithFormat:@"%@",entity.date]componentsSeparatedByString:@"-"]count]>2) {
                NSString*month = [[NSString stringWithFormat:@"%@",entity.date]componentsSeparatedByString:@"-"][1];
                NSString*day = [[NSString stringWithFormat:@"%@",entity.date]componentsSeparatedByString:@"-"][2];
                [titlesArray addObject:[NSString stringWithFormat:@"%@-%@",month,day]];
            }
            if ([NSString stringWithFormat:@"%@",entity.score].floatValue>maxScore.floatValue) {
                maxScore = [NSString stringWithFormat:@"%@",entity.score];
            }
            [dataArray addObject:[NSString stringWithFormat:@"%@",entity.score]];
        }
        for(RBThirdEntity *entity in readEntity.identities) {
            if ([entity.provider isEqualToString:@"weibo"]) {
                // RB-获取用户影响力详情-标签云
                [self.hudView show];
                Handler*handler = [Handler shareHandler];
                handler.delegate = self;
                [handler getRBInfluenceDetailCloud];
                return;
            }
        }
        [self loadEditView];
    }
    
    if ([sender isEqualToString:@"kols/profile"]) {
        [JsonService setRBUserEntityWith:jsonObject];
        [LocalService setRBLocalDataUserKolWith:[LocalService getRBLocalDataUserLoginKolId]];
        // RB-获取用户影响力详情
        [self.hudView show];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBInfluenceDetail];
    }
    
    if ([sender isEqualToString:@"kols/identity_bind"]) {
        // RB-获取用户影响力详情
        [self.hudView show];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBInfluenceDetail];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

