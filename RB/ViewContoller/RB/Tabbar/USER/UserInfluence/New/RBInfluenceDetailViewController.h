//
//  RBInfluenceDetailViewController.h
//  RB
//
//  Created by RB8 on 2017/8/8.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInfluenceView.h"
@interface RBInfluenceDetailViewController : RBBaseViewController<RBBaseVCDelegate,RBNewAlertViewDelegate,UIScrollViewDelegate,RBInfluenceViewDelegate>
@property(nonatomic,copy) NSString * mark;
@property(nonatomic,copy) NSString * kolId;
//我的影响力数据
@property(nonatomic,strong)RBNewInfluenceEntity * myEntity;
@end
