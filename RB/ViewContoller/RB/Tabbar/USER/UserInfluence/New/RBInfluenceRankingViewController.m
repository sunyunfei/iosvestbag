//
//  RBInfluenceRankingViewController.m
//  RB
//
//  Created by AngusNi on 7/7/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBInfluenceRankingViewController.h"
@interface RBInfluenceRankingViewController () {
    int pageIndex;
    int pageSize;
    UITableView*tableviewn;
    NSMutableArray*datalist;
    
    int contactIndex;
    RBInfluenceEntity*influenceEntity;
    RBContactEntity*contactEntity;
}
@end

@implementation RBInfluenceRankingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5184", @"影响力排名");
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight, self.view.width,(ScreenHeight -NavHeight))
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
    [self setRefreshHeaderView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    // RB-获取用户信息
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfo];
    //
    [TalkingData trackPageBegin:@"my-influence-ranking"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-influence-ranking"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationStatusBarAction {
    CGPoint off = tableviewn.contentOffset;
    off.y = 0 - tableviewn.contentInset.top;
    [tableviewn setContentOffset:off animated:YES];
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)loadRefreshViewFirstData {
    pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    pageSize = 10;
    // RB-获取社交排名
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBInfluenceRankWithPage:[NSString stringWithFormat:@"%d",pageIndex+1]];
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageIndex = pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshViewFinish {
    [tableviewn.mj_header endRefreshing];
    [tableviewn.mj_footer endRefreshing];
    if ([datalist count] == (pageIndex+1)*pageSize) {
        if (tableviewn.mj_footer == nil){
            [self setRefreshFooterView];
        }
    } else {
        [tableviewn.mj_footer removeFromSuperview];
        tableviewn.mj_footer = nil;
    }
    [UIView performWithoutAnimation:^{
        [tableviewn reloadData];
    }];
    if ([datalist count]==0) {
        self.defaultView.hidden = NO;
    } else {
        self.defaultView.hidden = YES;
    }
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBNewInfluenceRankingTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBNewInfluenceRankingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                      reuseIdentifier:cellIdentifier];
    }
    [cell loadRBNewInfluenceRankingTableViewCellWith:datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    contactIndex = (int)indexPath.row;
    contactEntity = datalist[indexPath.row];
    NSString*score = [NSString stringWithFormat:@"%@",contactEntity.influence_score];
    NSString*mobile = [Utils getNSStringPhoneNumber:[NSString stringWithFormat:@"%@",contactEntity.mobile]];
    if(score.length == 0) {
        if([NSString stringWithFormat:@"%@",mobile].length!= 11) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R5065", @"暂不支持此手机号码")];
            return;
        }
        RBAlertView *alertView = [[RBAlertView alloc]init];
        alertView.delegate = self;
        alertView.tag = 999;
        [alertView showWithArray:@[[NSString stringWithFormat:NSLocalizedString(@"R5066", @"确定邀请【%@】加入Robin8, 我们将给TA发送一条邀请短信"),contactEntity.name],NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
    }
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if(alertView.tag==999) {
        if(buttonIndex==0) {
            contactEntity.is_send = 1;
            [datalist replaceObjectAtIndex:contactIndex withObject:contactEntity];
            [UIView performWithoutAnimation:^{
                [tableviewn reloadData];
            }];
            // RB-邀请朋友
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceSmsWithMobile:contactEntity.mobile];
        }
    }
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"influences/rank"]) {
        [self.hudView dismiss];
        if(pageIndex==0){
            datalist = [NSMutableArray new];
            influenceEntity = [JsonService getRBInfluenceEntity:jsonObject];
            datalist = influenceEntity.contacts;
        } else {
            datalist = [JsonService getRBInfluenceEntity:jsonObject andBackArray:datalist];
        }
        [self setRefreshViewFinish];
    }
    
    if ([sender isEqualToString:@"kols/profile"]) {
        [JsonService setRBUserEntityWith:jsonObject];
        [LocalService setRBLocalDataUserKolWith:[LocalService getRBLocalDataUserLoginKolId]];
        [self loadRefreshViewFirstData];
    }
    
    if ([sender isEqualToString:@"influences/send_invite"]) {
        
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

