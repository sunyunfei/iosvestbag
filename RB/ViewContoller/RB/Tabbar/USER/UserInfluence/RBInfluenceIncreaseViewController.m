//
//  RBInfluenceIncreaseViewController.m
//  RB
//
//  Created by AngusNi on 4/13/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBInfluenceIncreaseViewController.h"

@interface RBInfluenceIncreaseViewController () {
    RBInfluenceView*influenceView;
    RBInfluenceUpgradeEntity*upgradeEntity;
    UILabel*leftCLabel;
    UILabel*centerCLabel;
    UILabel*rightCLabel;
}
@end

@implementation RBInfluenceIncreaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5182", @"影响力提升");
    self.bgIV.hidden = NO;
    self.navView.backgroundColor = [UIColor clearColor];
    self.navLineLabel.hidden = YES;
    //
    [self loadEditView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    // RB-获取用户影响力提升
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBInfluenceIncrease];
    //
    [TalkingData trackPageBegin:@"my-influence-increase"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-influence-increase"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIScrollView*scrollviewn = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    [scrollviewn setPagingEnabled:NO];
    [scrollviewn setShowsHorizontalScrollIndicator:NO];
    [scrollviewn setShowsVerticalScrollIndicator:NO];
    scrollviewn.userInteractionEnabled = YES;
//    scrollviewn.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self.view addSubview:scrollviewn];
    //
    UILabel*influenceBgLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth, 185.0)];
//    influenceBgLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [scrollviewn addSubview:influenceBgLabel];
    //
    UILabel*influenceLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, influenceBgLabel.bottom-0.5f,influenceBgLabel.width, 0.5f)];
    influenceLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
//    [scrollviewn addSubview:influenceLineLabel];
    //
    influenceView = [[RBInfluenceView alloc]initWithFrame:CGRectMake(0, 35.0, ScreenWidth, 150)];
    [scrollviewn addSubview:influenceView];
    //
    UILabel*desLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, influenceBgLabel.bottom+10.0, ScreenWidth, 150.0)];
    desLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.5f];
    [scrollviewn addSubview:desLabel];
    //
    UILabel*desTLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, desLabel.top, ScreenWidth, 0.5f)];
    desTLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [scrollviewn addSubview:desTLineLabel];
    //
    UILabel*desLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(desLabel.left, desLabel.top+38.0,desLabel.width, 0.5f)];
    desLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [scrollviewn addSubview:desLineLabel];
    //
    UILabel*desTLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, desLabel.top,200, 38.0)];
    desTLabel.text = NSLocalizedString(@"R5183", @"影响力提升");
    desTLabel.font = font_15;
    desTLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [scrollviewn addSubview:desTLabel];
    //
    UILabel*desCLabel = [[UILabel alloc]initWithFrame:CGRectMake(desTLabel.left, desTLabel.bottom+15.0,ScreenWidth-desTLabel.left*2.0, 18.0)];
    desCLabel.text = NSLocalizedString(@"R5046", @"1. 绑定更多的社交账号,提升你的影响力分数");
    desCLabel.numberOfLines = 0;
    desCLabel.font = font_11;
    desCLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [scrollviewn addSubview:desCLabel];
    [Utils getUILabel:desCLabel withLineSpacing:5.0];
    CGSize desCLabelSize = [Utils getUIFontSizeFitH:desCLabel withLineSpacing:5.0];
    desCLabel.height = desCLabelSize.height;
    //
    UILabel*desCLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(desTLabel.left, desCLabel.bottom+10.0,desCLabel.width, desCLabel.height)];
    desCLabel1.text = NSLocalizedString(@"R5047", @"2. 积极参与悬赏活动,增强个人账户的活跃度");
    desCLabel1.numberOfLines = 0;
    desCLabel1.font = desCLabel.font;
    desCLabel1.textColor = desCLabel.textColor;
    [scrollviewn addSubview:desCLabel1];
    [Utils getUILabel:desCLabel1 withLineSpacing:5.0];
    CGSize desCLabelSize1 = [Utils getUIFontSizeFitH:desCLabel1 withLineSpacing:5.0];
    desCLabel1.height = desCLabelSize1.height;
    //
    UILabel*desCLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(desTLabel.left, desCLabel1.bottom+10.0,desCLabel.width, desCLabel.height)];
    desCLabel2.text = NSLocalizedString(@"R5048", @"3. 邀请更多好友加入Robin8,通过通讯录建立你的朋友圈,精准分析你的影响力");
    desCLabel2.numberOfLines = 0;
    desCLabel2.font = desCLabel.font;
    desCLabel2.textColor = desCLabel.textColor;
    [scrollviewn addSubview:desCLabel2];
    [Utils getUILabel:desCLabel2 withLineSpacing:5.0];
    CGSize desCLabelSize2 = [Utils getUIFontSizeFitH:desCLabel2 withLineSpacing:5.0];
    desCLabel2.height = desCLabelSize2.height;
    //
    desLabel.height = desCLabel2.bottom +15.0 -desLabel.top;
    //
    UILabel*highLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, desLabel.bottom+10.0, ScreenWidth, 150.0)];
    highLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.5f];
    highLabel.userInteractionEnabled =YES;
    [scrollviewn addSubview:highLabel];
    //
    UILabel*highTLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, highLabel.top, ScreenWidth, 0.5f)];
    highTLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [scrollviewn addSubview:highTLineLabel];
    //
    UILabel*highLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(desLabel.left, highLabel.top+38.0,desLabel.width, 0.5f)];
    highLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [scrollviewn addSubview:highLineLabel];
    //
    UILabel*highTLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, highLabel.top, 200.0, 38.0)];
    highTLabel.text = NSLocalizedString(@"R2067", @"悬赏活动");
    highTLabel.font = font_15;
    highTLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [scrollviewn addSubview:highTLabel];
    //
    UILabel*leftTLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, highLineLabel.bottom+18.0,ScreenWidth/3.0, 20.0)];
    leftTLabel.text = NSLocalizedString(@"R5049", @"添加社交账号");
    leftTLabel.font = font_11;
    leftTLabel.textAlignment = NSTextAlignmentCenter;
    leftTLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [scrollviewn addSubview:leftTLabel];
    //
    UIButton*leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"icon_influence_account.png"] forState:UIControlStateNormal];
    leftBtn.frame = CGRectMake((ScreenWidth/3.0-40.0)/2.0, leftTLabel.bottom+5.0, 40.0, 40.0);
    [scrollviewn addSubview:leftBtn];
    //
    leftCLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, leftBtn.bottom+5.0, ScreenWidth/3.0, 20.0)];
    leftCLabel.font = font_11;
    leftCLabel.textAlignment = NSTextAlignmentCenter;
    leftCLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [scrollviewn addSubview:leftCLabel];
    //
    UILabel*leftLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/3.0, highLineLabel.bottom+30.0, 0.5f, 60.0)];
    leftLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [scrollviewn addSubview:leftLineLabel];
    //
    UILabel*centerTLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/3.0, leftTLabel.top,leftTLabel.width, leftTLabel.height)];
    centerTLabel.text = NSLocalizedString(@"R5050", @"更多悬赏活动");
    centerTLabel.font = leftTLabel.font;
    centerTLabel.textAlignment = NSTextAlignmentCenter;
    centerTLabel.textColor = leftTLabel.textColor;
    [scrollviewn addSubview:centerTLabel];
    //
    UIButton*centerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [centerBtn addTarget:self action:@selector(centerBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [centerBtn setBackgroundImage:[UIImage imageNamed:@"icon_influence_task.png"] forState:UIControlStateNormal];
    centerBtn.frame = CGRectMake(centerTLabel.left+(ScreenWidth/3.0-40.0)/2.0, leftTLabel.bottom+5.0, 40.0, 40.0);
    [scrollviewn addSubview:centerBtn];
    //
    centerCLabel = [[UILabel alloc]initWithFrame:CGRectMake(centerTLabel.left, centerBtn.bottom+5.0,ScreenWidth/3.0, 20.0)];
    centerCLabel.font = leftCLabel.font;
    centerCLabel.textAlignment = NSTextAlignmentCenter;
    centerCLabel.textColor = leftCLabel.textColor;
    [scrollviewn addSubview:centerCLabel];
    //
    UILabel*centerLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/3.0*2.0, highLineLabel.bottom+30.0, 0.5f, 60.0)];
    centerLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [scrollviewn addSubview:centerLineLabel];
    //
    UILabel*rightTLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/3.0*2.0, highLineLabel.bottom+18.0, ScreenWidth/3.0, 20.0)];
    rightTLabel.text = NSLocalizedString(@"R5051", @"通讯录授权");
    rightTLabel.font = leftTLabel.font;
    rightTLabel.textAlignment = NSTextAlignmentCenter;
    rightTLabel.textColor = leftTLabel.textColor;
    [scrollviewn addSubview:rightTLabel];
    //
    UIButton*rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"icon_influence_contact.png"] forState:UIControlStateNormal];
    rightBtn.frame = CGRectMake(rightTLabel.left+(ScreenWidth/3.0-40.0)/2.0, leftTLabel.bottom+5.0, 40.0, 40.0);
    [scrollviewn addSubview:rightBtn];
    //
    rightCLabel = [[UILabel alloc]initWithFrame:CGRectMake(rightTLabel.left, rightBtn.bottom+5.0, ScreenWidth/3.0, 20.0)];
    rightCLabel.font = leftCLabel.font;
    rightCLabel.textAlignment = NSTextAlignmentCenter;
    rightCLabel.textColor = leftCLabel.textColor;
    [scrollviewn addSubview:rightCLabel];
    //
    [scrollviewn setContentSize:CGSizeMake(ScreenWidth, highLabel.bottom+20.0)];
}

#pragma mark - UIButton Delegate
- (void)leftBtnAction:(UIButton *)sender {
    RBInfoSocialViewController *toview  =
    [[RBInfoSocialViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)centerBtnAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)rightBtnAction:(UIButton *)sender {
    if([NSString stringWithFormat:@"%@",upgradeEntity.has_contacts].intValue==0) {
        // IOS8以上系统可直接跳转到本应用的设置界面
        if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
}

-(void)updateEditView {
    influenceView.rate = ([NSString stringWithFormat:@"%@",upgradeEntity.influence_score].floatValue-350.0)/650.0; // 350 - 1000
    [influenceView.userIV sd_setImageWithURL:[NSURL URLWithString:upgradeEntity.avatar_url] placeholderImage:PlaceHolderUserImage options:SDWebImageProgressiveDownload];
    influenceView.userLabel.text = [NSString stringWithFormat:@"%@",upgradeEntity.influence_score];
    influenceView.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5054", @"第%@名"),upgradeEntity.rank_index];
    influenceView.dateLabel.text = [NSString stringWithFormat:@"%@",upgradeEntity.influence_level];
    [influenceView setNeedsDisplay];
}


#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"influences/upgrade"]) {
        [self.hudView dismiss];
        upgradeEntity = [JsonService getRBInfluenceUpgradeEntity:jsonObject];
        [self updateEditView];
        leftCLabel.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2089", @"已绑定"),upgradeEntity.identity_count];
        centerCLabel.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2006",@"已完成"),upgradeEntity.campaign_count];
        if([NSString stringWithFormat:@"%@",upgradeEntity.has_contacts].intValue==1){
            rightCLabel.text = NSLocalizedString(@"R5052", @"已授权");
        } else {
            rightCLabel.text = NSLocalizedString(@"R5053", @"未授权");
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

