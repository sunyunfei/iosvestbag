//
//  RBInfluenceReadViewController.h
//  RB
//
//  Created by AngusNi on 4/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBInfluenceReadViewController : RBBaseViewController
<RBBaseVCDelegate,MCLineChartViewDataSource,MCLineChartViewDelegate>
@property(nonatomic, strong) RBInfluenceEntity* influenceEntity;

@end