//
//  RBInfluenceIncreaseViewController.h
//  RB
//
//  Created by AngusNi on 4/13/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInfluenceView.h"
#import "RBInfoSocialViewController.h"

@interface RBInfluenceIncreaseViewController : RBBaseViewController
<RBBaseVCDelegate>

@end
