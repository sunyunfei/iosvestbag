//
//  RBInfluenceLoadingViewController.h
//  RB
//
//  Created by AngusNi on 5/30/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInfluenceBundingViewController.h"


@interface RBInfluenceLoadingViewController : RBBaseViewController<RBBaseVCDelegate,RBNewAlertViewDelegate>
@property(nonatomic,strong)NSString * isBind;
@end
