//
//  UpInfluenceViewController.m
//  RB
//
//  Created by RB8 on 2017/8/2.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "UpInfluenceViewController.h"
#import "upInfluenceCell.h"
#import "RBUserInviteViewController.h"
#import "RBUserSignViewController.h"
#import "RBAppDelegate.h"
@interface UpInfluenceViewController ()
{
    RBInfluenceView * influView;
    UITableView * upTableView;
    NSArray * arr;
    RBUserDashboardEntity*dashboardEntity;

}
@end

@implementation UpInfluenceViewController

- (void)viewDidLoad {
    // Do any additional setup after loading the view.
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5239", @"提升影响力");
    arr = @[@[@"shareIcon",@"用心参与品牌活动",@"有质量有选择的参与感兴趣的品牌活动，用心的转发语可以显著提升社交媒体的互动",NSLocalizedString(@"R5236", @"去分享")],@[@"inviteIcon",@"邀请好友",@"动员好友加入Robin8,影响力快速升级",NSLocalizedString(@"R5237", @"去邀请")],@[@"signIcon",@"签到",@"时刻查看影响力的动态变化，有助于提高影响力",NSLocalizedString(@"R5238", @"去签到")],@[@"readIcon",@"内容挖掘",@"从海量内容中挖掘优秀内容，提高您的收益和影响力",NSLocalizedString(@"R5254", @"去阅读")]];
    [self loadHeaderView];
    [self loadEditView];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoProfile];
}
-(void)loadHeaderView{
    influView = [[RBInfluenceView alloc]initWithFrame:CGRectMake(0, NavHeight+18, ScreenWidth, 170)];
    influView.imageUrl = self.Entity.avatar_url;
    influView.percent = self.Entity.influence_score_percentile;
    influView.influenceText = self.Entity.influence_level;
    influView.dataStr = self.Entity.calculated_date;
    influView.scoreText = self.Entity.influence_score;
    influView.rate = (float)[_Entity.influence_score intValue]/1000;
    influView.category = @"1";
    [self.view addSubview:influView];
}

-(void)loadEditView{
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, influView.bottom, ScreenWidth, 8)];
    lineView.backgroundColor = [Utils getUIColorWithHexString:ccColora4a4a4];
    lineView.alpha = 0.14;
    [self.view addSubview:lineView];
    upTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, lineView.bottom, ScreenWidth, ScreenHeight -lineView.bottom) style:UITableViewStyleGrouped];
    upTableView.delegate = self;
    upTableView.dataSource = self;
    upTableView.showsVerticalScrollIndicator = NO;
    upTableView.showsHorizontalScrollIndicator = NO;
    upTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:upTableView];
    [upTableView registerClass:[upInfluenceCell class] forCellReuseIdentifier:@"upInfluenceCell"];
}
#pragma mark RBBaseVCDelegate
-(void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    upInfluenceCell * cell = [tableView dequeueReusableCellWithIdentifier:@"upInfluenceCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    __weak typeof(self) weakSelf = self;
  
    cell.clickBlock = ^(NSString * text) {
        if ([text isEqualToString:NSLocalizedString(@"R5236", @"去分享")]) {
            RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate getRongIMConnect];
            [appDelegate loadViewController:22];
        }else if ([text isEqualToString:NSLocalizedString(@"R5237", @"去邀请")]){
            
            if ([dashboardEntity.detail intValue] == 19) {
                return;
            }
            RBUserInviteViewController * inviteVC = [[RBUserInviteViewController alloc]init];
            inviteVC.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:inviteVC animated:YES];
        }else if([text isEqualToString:NSLocalizedString(@"R5238", @"去签到")]){
            RBUserSignViewController * signVC = [[RBUserSignViewController alloc]init];
            signVC.hidesBottomBarWhenPushed = YES;
            signVC.avatar = _Entity.avatar_url;
            signVC.name = _Entity.name;
            [weakSelf.navigationController pushViewController:signVC animated:YES];
        }else if([text isEqualToString:NSLocalizedString(@"R5254", @"去阅读")]){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"toFindVC" object:nil];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    };
    [cell setValueForCell:arr[indexPath.row]];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 91;
    }
    return 81;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 152;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView * footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 153)];
    footView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    //
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 8)];
    lineView.backgroundColor = [Utils getUIColorWithHexString:ccColora4a4a4];
    lineView.alpha = 0.14;
    [footView addSubview:lineView];
    //
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 8+22, 25, 25)];
    imageView.image = [UIImage imageNamed:@"arithmeticIcon"];
    imageView.layer.cornerRadius = 25/2;
    [footView addSubview:imageView];
    //
    UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(imageView.right + 14, 8+28, 200, 20)];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.textColor = [Utils getUIColorWithHexString:ccColor212121];
    titleLabel.text = @"社交影响力有什么用";
    
    titleLabel.font = font_cu_12;
    [titleLabel sizeToFit];
    titleLabel.center = CGPointMake(imageView.right+14+titleLabel.size.width/2, imageView.center.y);
    [footView addSubview:titleLabel];
    //
    UILabel * describeLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, titleLabel.bottom+20,ScreenWidth - 30, 35)];
    describeLabel.numberOfLines = 0;
    describeLabel.textColor = [Utils getUIColorWithHexString:ccColor545454];
    describeLabel.textAlignment = NSTextAlignmentLeft;
    describeLabel.font = font_13;
    describeLabel.text = @"根据你的社交媒体的内容和数据,Robin8会科学的计算你的社交影响力。影响力越高，可以接受到更多高奖金的品牌邀约、福利或特权";
    [describeLabel sizeToFit];
    describeLabel.frame = CGRectMake(15, titleLabel.bottom+20,ScreenWidth - 30, describeLabel.size.height);
    [footView addSubview:describeLabel];
    //
   
    return footView;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 55)];
    headView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 45/2, 25, 25)];
    imageView.image = [UIImage imageNamed:@"upInfluenceIcon"];
    imageView.layer.cornerRadius = 25/2;
    [headView addSubview:imageView];
    UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(imageView.right+14, 23, 200, 20)];
    titleLabel.textColor = [Utils getUIColorWithHexString:ccColor212121];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = font_cu_12;
    titleLabel.text = @"以下方式可提升影响力";
    [headView addSubview:titleLabel];
    return headView;
}
#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"my/show"]) {
        [self.hudView dismiss];
        dashboardEntity = [JsonService getRBKolDashboardDetailEntity:jsonObject];

    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    dashboardEntity = nil;
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    dashboardEntity = nil;
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

-(void)pressBtn:(id)sender{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
