//
//  RBInfluenceViewController.h
//  RB
//
//  Created by AngusNi on 3/21/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInfluenceView.h"
#import "RBInfluenceRankingTableViewCell.h"
#import "RBInfluenceBundingViewController.h"
#import "RBInfluenceReadViewController.h"
#import "RBInfluenceIncreaseViewController.h"

@interface RBInfluenceViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate,RBAlertViewDelegate>

@end