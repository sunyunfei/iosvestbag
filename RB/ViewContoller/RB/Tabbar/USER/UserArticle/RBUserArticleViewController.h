//
//  RBUserArticleViewController.h
//  RB
//
//  Created by AngusNi on 3/18/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBArticleView.h"
#import "RBArticleDetailViewController.h"

@interface RBUserArticleViewController : RBBaseViewController
<RBBaseVCDelegate,RBArticleViewDelegate>

@end
