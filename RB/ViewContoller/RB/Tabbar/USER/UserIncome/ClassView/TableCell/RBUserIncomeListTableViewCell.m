//
//  RBUserIncomeListTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBUserIncomeListTableViewCell.h"
#import "Service.h"
#import "Utils.h"
@implementation RBUserIncomeListTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        TTTAttributedLabel*tLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_(10);
        tLabel.textColor = [Utils getUIColorWithHexString:@"777777"];
        tLabel.tag = 1002;
        [self.contentView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel.font = font_cu_cu_(16);
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        cLabel.tag = 1003;
        [self.contentView addSubview:cLabel];
        //
        UILabel*cLabel1 = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel1.font = font_(14);
        cLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        cLabel1.tag = 1004;
        cLabel1.numberOfLines = 0;
        [self.contentView addSubview:cLabel1];
        //
        UILabel*cLabel2 = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel2.textAlignment = NSTextAlignmentRight;
        cLabel2.font = font_11;
        cLabel2.textColor = [Utils getUIColorWithHexString:@"777777"];
        cLabel2.tag = 1005;
        [self.contentView addSubview:cLabel2];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1010;
        [self.contentView addSubview:lineLabel];
    }
    return self;
}

- (instancetype)loadRBUserIncomeListCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    TTTAttributedLabel*tLabel=(TTTAttributedLabel*)[self.contentView viewWithTag:1002];
    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*cLabel1=(UILabel*)[self.contentView viewWithTag:1004];
    UILabel*cLabel2=(UILabel*)[self.contentView viewWithTag:1005];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    //
    if(indexPath.row>=[datalist count]){
        self.frame = CGRectMake(0, 0, ScreenWidth, 60.0);
        return self;
    }
    RBIncomeEntity *entity = datalist[indexPath.row];
    if(entity.direct!=nil&&entity.direct.length!=0){
        NSString * strUrl = [entity.subject stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
        NSString * strUrl1 = [strUrl stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
        cLabel1.text = [NSString stringWithFormat:@"%@",strUrl1];
        cLabel2.text = [NSString stringWithFormat:@"%@",entity.direct];
        NSString*credits = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:entity.credits]];
        if([entity.direct isEqualToString:@"支出"]||[entity.direct isEqualToString:@"冻结"]) {
            cLabel.text = [NSString stringWithFormat:@"-%@",credits];
        } else {
            cLabel.text = [NSString stringWithFormat:@"+%@",credits];
        }
        NSString*date = [[NSString stringWithFormat:@"%@",entity.created_at]componentsSeparatedByString:@"T"][0];
        if ([[[NSString stringWithFormat:@"%@",date]componentsSeparatedByString:@"-"]count]>2) {
            NSString*month = [[NSString stringWithFormat:@"%@",date]componentsSeparatedByString:@"-"][1];
            NSString*day = [[NSString stringWithFormat:@"%@",date]componentsSeparatedByString:@"-"][2];
            tLabel.text = [NSString stringWithFormat:@"%@-%@",month,day];
            [tLabel setText:tLabel.text
    afterInheritingLabelAttributesAndConfiguringWithBlock:
             ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
                 NSRange range = [[mStr string] rangeOfString:[NSString stringWithFormat:@"/%@",month] options:NSCaseInsensitiveSearch];
                 CTFontRef font = CTFontCreateWithName((CFStringRef)font_11.fontName, font_11.pointSize, NULL);
                 [mStr addAttributes:@{(NSString *)kCTFontAttributeName:(__bridge id)font} range:range];
                 CFRelease(font);
                 return mStr;
             }];
        }
    } else {
        cLabel1.text = [NSString stringWithFormat:@"%@",entity.title];
        cLabel2.text = [NSString stringWithFormat:@"%@",entity.direct_text];
        cLabel.text = [NSString stringWithFormat:@"%@",entity.credits];
        tLabel.text = [NSString stringWithFormat:@"%@",entity.created_at];
    }
    
    //钱数
    [cLabel sizeToFit];
    cLabel.frame = CGRectMake(ScreenWidth - CellLeft - cLabel.frame.size.width, 13, cLabel.frame.size.width, cLabel.frame.size.height);
    //活动名称
    cLabel1.width = ScreenWidth - CellLeft - cLabel.width - CellLeft - 15;
    CGSize clabel1Size =  [Utils getUIFontSizeFitH:cLabel1];
    cLabel1.frame = CGRectMake(CellLeft, 13, ScreenWidth - CellLeft - cLabel.width - CellLeft - 15, clabel1Size.height);
    //日期
    [tLabel sizeToFit];
    tLabel.frame = CGRectMake(CellLeft, cLabel1.bottom + 10, tLabel.frame.size.width, tLabel.frame.size.height);
    
    //充值类型
    [cLabel2 sizeToFit];
    cLabel2.frame = CGRectMake(ScreenWidth - CellLeft - cLabel2.frame.size.width, tLabel.top, cLabel2.frame.size.width, cLabel2.frame.size.height);
    
    lineLabel.frame = CGRectMake(0, cLabel2.bottom + 13 - 0.5, ScreenWidth, 0.5);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.height);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}
- (instancetype)loadRBUserCreditListCellWith:(NSMutableArray*)dataList andIndex:(NSIndexPath*)indexPath{
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    TTTAttributedLabel*tLabel=(TTTAttributedLabel*)[self.contentView viewWithTag:1002];
    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*cLabel1=(UILabel*)[self.contentView viewWithTag:1004];
    UILabel*cLabel2=(UILabel*)[self.contentView viewWithTag:1005];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    //
    if(indexPath.row>=[dataList count]){
        self.frame = CGRectMake(0, 0, ScreenWidth, 60.0);
        return self;
    }
    RBCreditEntity *entity = dataList[indexPath.row];
    cLabel1.text = [NSString stringWithFormat:@"%@",entity.remark];
    cLabel2.text = [NSString stringWithFormat:@"%@",entity.direct];
    cLabel.text = [NSString stringWithFormat:@"%@",entity.score];
    tLabel.text = [NSString stringWithFormat:@"%@",entity.show_time];
    //钱数
    [cLabel sizeToFit];
    cLabel.frame = CGRectMake(ScreenWidth - CellLeft - cLabel.frame.size.width, 13, cLabel.frame.size.width, cLabel.frame.size.height);
    //活动名称
    cLabel1.width = ScreenWidth - CellLeft - cLabel.width - CellLeft - 15;
    CGSize clabel1Size =  [Utils getUIFontSizeFitH:cLabel1];
    cLabel1.frame = CGRectMake(CellLeft, 13, ScreenWidth - CellLeft - cLabel.width - CellLeft - 15, clabel1Size.height);
    //日期
    [tLabel sizeToFit];
    tLabel.frame = CGRectMake(CellLeft, cLabel1.bottom + 10, tLabel.frame.size.width, tLabel.frame.size.height);
    
    //充值类型
    [cLabel2 sizeToFit];
    cLabel2.frame = CGRectMake(ScreenWidth - CellLeft - cLabel2.frame.size.width, tLabel.top, cLabel2.frame.size.width, cLabel2.frame.size.height);
    
    lineLabel.frame = CGRectMake(0, cLabel2.bottom + 13 - 0.5, ScreenWidth, 0.5);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.height);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
