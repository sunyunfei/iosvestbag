//
//  RBQATableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBQATableViewCell.h"
#import "Service.h"

@implementation RBQATableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UIImageView*iconIV = [[UIImageView alloc]initWithFrame:CGRectZero];
        iconIV.tag = 1001;
        [self.contentView addSubview:iconIV];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1002;
        [self.contentView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel.font = font_11;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        cLabel.numberOfLines = 0;
        cLabel.tag = 1003;
        [self.contentView addSubview:cLabel];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1010;
        [self.contentView addSubview:lineLabel];
    }
    return self;
}

- (instancetype)loadRBQACellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UIImageView*iconIV=(UIImageView*)[self.contentView viewWithTag:1001];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1002];
    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    //
    RBQAEntity*entity = datalist[indexPath.row];
    tLabel.text = entity.question;
    cLabel.text = entity.answer;
    //
    tLabel.frame = CGRectMake(CellLeft, 20.0, ScreenWidth-2.0*CellLeft, 18.0);
    cLabel.frame = CGRectMake(tLabel.left, tLabel.bottom+5.0, tLabel.width, 15.0);
    [Utils getUILabel:cLabel withLineSpacing:4.0];
    CGSize cLabelSize = [Utils getUIFontSizeFitH:cLabel withLineSpacing:4.0];
    cLabel.height = cLabelSize.height;
    lineLabel.frame = CGRectMake(0, cLabel.bottom+20.0, ScreenWidth, 0.5);
    iconIV.frame = CGRectMake(CellLeft, (cLabel.bottom + 20.0 - 30.0)/2.0, 30.0, 30.0);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.height);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}


@end
