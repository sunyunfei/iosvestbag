//
//  RBUserIncomeListTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBUserIncomeListTableViewCell : UITableViewCell
//资金
- (instancetype)loadRBUserIncomeListCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
//积分
- (instancetype)loadRBUserCreditListCellWith:(NSMutableArray*)dataList andIndex:(NSIndexPath*)indexPath;
@end
