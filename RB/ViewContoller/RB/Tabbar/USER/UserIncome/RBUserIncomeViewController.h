//
//  RBUserIncomeViewController.h
//  RB
//
//  Created by AngusNi on 16/2/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

#import "MCLineChartView.h"
#import "RBUserIncomeCashViewController.h"
#import "RBUserIncomeQAViewController.h"
#import "RBUserIncomeListViewController.h"

@interface RBUserIncomeViewController : RBBaseViewController
<RBBaseVCDelegate,MCLineChartViewDataSource,MCLineChartViewDelegate>
@end