//
//  RBUserIncomeBundingViewController.m
//  RB
//
//  Created by AngusNi on 5/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserIncomeBundingViewController.h"

@interface RBUserIncomeBundingViewController (){
    InsetsTextField*nameTF;
    InsetsTextField*accountTF;
    InsetsTextField*idcardTF;

    UIView*editView;
}
@end

@implementation RBUserIncomeBundingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5020", @"支付宝绑定");
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-income-bunding"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-income-bunding"];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, editView.bottom+50.0, ScreenWidth , 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    footerBtn.frame = CGRectMake((footerView.width-200.0)/2.0, 0, 200.0, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080",@"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    footerBtn.layer.borderWidth = 1.0;
    footerBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    editView = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 0)];
    editView.userInteractionEnabled = YES;
    [self.view addSubview:editView];
    //
    UILabel*accountLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth-2.0*CellLeft, 50.0)];
    accountLabel.font = font_15;
    accountLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    accountLabel.text = NSLocalizedString(@"R5021", @"支付宝账号");
    accountLabel.userInteractionEnabled = YES;
    [editView addSubview:accountLabel];
    //
    accountTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(85.0, 0, accountLabel.width-85.0, accountLabel.height)];
    accountTF.font = font_15;
    accountTF.keyboardType = UIKeyboardTypeDefault;
    accountTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    accountTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5021", @"支付宝账号")] attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    accountTF.returnKeyType = UIReturnKeyDone;
    accountTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    accountTF.delegate = self;
    [accountLabel addSubview:accountTF];
    //
    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textFiledKB.delegateT = self;
    [accountTF setInputAccessoryView:textFiledKB];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, accountLabel.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editView addSubview:lineLabel];
    //
    UILabel*nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(accountLabel.left, lineLabel.bottom, accountLabel.width, accountLabel.height)];
    nameLabel.font = accountLabel.font;
    nameLabel.textColor = accountLabel.textColor;
    nameLabel.text = NSLocalizedString(@"R5022", @"支付宝姓名");
    nameLabel.userInteractionEnabled = YES;
    [editView addSubview:nameLabel];
    //
    nameTF = [[InsetsTextField alloc]initWithFrame:accountTF.frame];
    nameTF.font = accountTF.font;
    nameTF.keyboardType = UIKeyboardTypeDefault;
    nameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    nameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5022", @"支付宝姓名")] attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    nameTF.returnKeyType = UIReturnKeyDone;
    nameTF.textColor = accountTF.textColor;
    nameTF.delegate = self;
    [nameLabel addSubview:nameTF];
    [nameTF setInputAccessoryView:textFiledKB];
    //
    UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, nameLabel.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editView addSubview:lineLabel1];
    //
    UILabel*idcardLabel = [[UILabel alloc]initWithFrame:CGRectMake(accountLabel.left, lineLabel1.bottom, accountLabel.width, accountLabel.height)];
    idcardLabel.font = accountLabel.font;
    idcardLabel.textColor = accountLabel.textColor;
    idcardLabel.text = NSLocalizedString(@"R5195", @"身份证号码");
    idcardLabel.userInteractionEnabled = YES;
    [editView addSubview:idcardLabel];
    //
    idcardTF = [[InsetsTextField alloc]initWithFrame:accountTF.frame];
    idcardTF.font = accountTF.font;
    idcardTF.keyboardType = UIKeyboardTypeDefault;
    idcardTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    idcardTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5195", @"身份证号码")] attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    idcardTF.returnKeyType = UIReturnKeyDone;
    idcardTF.textColor = accountTF.textColor;
    idcardTF.delegate = self;
    [idcardLabel addSubview:idcardTF];
    [idcardTF setInputAccessoryView:textFiledKB];
    //
    UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, idcardLabel.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editView addSubview:lineLabel2];
    //
    editView.height = lineLabel2.bottom;
    //
    UITapGestureRecognizer *tgp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEdit)];
    tgp.delegate = self;
    [self.view addGestureRecognizer:tgp];
    //
    NSString*alipay_name = [JsonService checkJson:[self.jsonObjectStr objectForKey:@"alipay_name"]];
    NSString*alipay_account = [JsonService checkJson:[self.jsonObjectStr objectForKey:@"alipay_account"]];
    NSString*id_card = [JsonService checkJson:[self.jsonObjectStr objectForKey:@"id_card"]];
    accountTF.text = alipay_account;
    nameTF.text = alipay_name;
    idcardTF.text = id_card;
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    [self endEdit];
    if (accountTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5021", @"支付宝账号")]];
        return;
    }
    if (nameTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5022", @"支付宝姓名")]];
        return;
    }
    if (idcardTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5195", @"身份证号码")]];
        return;
    }
    if ( [Utils getNSStringTypeCheckIDCardNumber:idcardTF.text]==NO) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5196", @"正确的身份证号码")]];
        return;
    }
    [self.hudView show];
    // RB-支付宝绑定
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoZhiFuBaoBindingWithName:nameTF.text andAccout:accountTF.text andIdcard:idcardTF.text];
}


#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEdit];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    float keyboardTop = ScreenHeight-320;
    if (editView.bottom > keyboardTop) {
        [UIView animateWithDuration:0.2 animations:^{
            editView.top = keyboardTop-editView.height;
        } completion:^(BOOL finished) {
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.2 animations:^{
        editView.top = NavHeight;
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"bind_alipay"]) {
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5018", @"提交成功")];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self RBNavLeftBtnAction];
                       });
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

