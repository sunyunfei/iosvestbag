//
//  RBUserIncomeQAViewController.h
//  RB
//
//  Created by AngusNi on 4/5/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBQATableViewCell.h"
@interface RBUserIncomeQAViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate>

@end
