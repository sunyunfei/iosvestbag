//
//  RBUserIncomeCashViewController.h
//  RB
//
//  Created by AngusNi on 16/2/19.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBUserIncomeBundingViewController.h"

@interface RBUserIncomeCashViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>

@end
