//
//  RBUserIncomeListViewController.h
//  RB
//
//  Created by AngusNi on 5/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBUserIncomeListTableViewCell.h"

@interface RBUserIncomeListViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate>

@end
