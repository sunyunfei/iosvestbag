//
//  RBUserIncomeCashViewController.m
//  RB
//
//  Created by AngusNi on 16/2/19.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBUserIncomeCashViewController.h"

@interface RBUserIncomeCashViewController (){
    InsetsTextField*amountTF;
    UIView*editView;
    UIButton*amountBtn;
    UILabel*amountTLabel;
    UILabel*amountCLabel;
    UILabel*arrowLabel;
    id jsonStr;
}
@end

@implementation RBUserIncomeCashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5023", @"提现");
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-income-cash"];
    // RB-是否绑定支付宝
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBAccountZhiFuBao];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-income-cash"];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, editView.bottom+50.0, ScreenWidth , 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    footerBtn.frame = CGRectMake((footerView.width-200.0)/2.0, 0, 200.0, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080",@"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    footerBtn.layer.borderWidth = 1.0;
    footerBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    editView = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 0)];
    editView.userInteractionEnabled = YES;
    [self.view addSubview:editView];
    //
    amountBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    amountBtn.frame = CGRectMake(0, 0, editView.width, 75.0);
    [amountBtn addTarget:self
                  action:@selector(amountBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [editView addSubview:amountBtn];
    //
    UIImageView*amountIV = [[UIImageView alloc]initWithFrame:CGRectMake(CellLeft, (75.0-47.0)/2.0, 47.0, 47.0)];
    amountIV.image = [UIImage imageNamed:@"icon_income_alipy.png"];
    [amountBtn addSubview:amountIV];
    //
    amountTLabel = [[UILabel alloc]initWithFrame:CGRectMake(amountIV.right+CellLeft, amountIV.top, ScreenWidth-amountIV.right-CellLeft, 23.0)];
    amountTLabel.font = font_11;
    amountTLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
    amountTLabel.text = NSLocalizedString(@"R5024", @"未绑定");
    [amountBtn addSubview:amountTLabel];
    //
    amountCLabel = [[UILabel alloc]initWithFrame:CGRectMake(amountTLabel.left, amountTLabel.bottom, amountTLabel.width, amountTLabel.height)];
    amountCLabel.font = font_cu_13;
    amountCLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    amountCLabel.text = NSLocalizedString(@"R5025", @"点击绑定支付宝");
    [amountBtn addSubview:amountCLabel];
    //
    arrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-CellLeft-13.0, (75.0-13.0)/2.0, 13.0, 13.0)];
    arrowLabel.font = font_icon_(13.0);
    arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    arrowLabel.text = Icon_btn_right;
    [amountBtn addSubview:arrowLabel];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 75.0-0.5, ScreenWidth, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [amountBtn addSubview:lineLabel];
    //
    UILabel*accountLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, amountBtn.bottom, 70.0, 50.0)];
    accountLabel.font = font_15;
    accountLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    accountLabel.text = NSLocalizedString(@"R5026", @"提现金额");
    [editView addSubview:accountLabel];
    //
    amountTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(accountLabel.right, accountLabel.top, ScreenWidth-accountLabel.right-CellLeft, accountLabel.height)];
    amountTF.font = font_15;
    amountTF.keyboardType = UIKeyboardTypeNumberPad;
    amountTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    amountTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"R5027", @"满50元即可提现") attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    amountTF.returnKeyType = UIReturnKeyDone;
    amountTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    amountTF.delegate = self;
    [editView addSubview:amountTF];
    //
    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textFiledKB.backgroundColor = [UIColor clearColor];
    textFiledKB.delegateT = self;
    [amountTF setInputAccessoryView:textFiledKB];
    //
    UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, amountTF.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editView addSubview:lineLabel1];
    //
    editView.height = amountTF.bottom;
    //
    UITapGestureRecognizer *tgp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEdit)];
    tgp.delegate = self;
    [self.view addGestureRecognizer:tgp];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)amountBtnAction:(UIButton *)sender {
    RBUserIncomeBundingViewController*toview = [[RBUserIncomeBundingViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    toview.jsonObjectStr = jsonStr;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    [self endEdit];
    if (amountTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5026", @"提现金额")]];
        return;
    }
    [self.hudView showOverlay];
    // RB-用户提现申请
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBAccountCashWithCredits:amountTF.text];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEdit];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    float keyboardTop = ScreenHeight-320;
    if (editView.bottom > keyboardTop) {
        [UIView animateWithDuration:0.2 animations:^{
            editView.top = keyboardTop-editView.height;
        } completion:^(BOOL finished) {
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.2 animations:^{
        editView.top = NavHeight;
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"alipay"]) {
        jsonStr = jsonObject;
        NSString*alipay_name = [JsonService checkJson:[jsonObject objectForKey:@"alipay_name"]];
        NSString*alipay_account = [JsonService checkJson:[jsonObject objectForKey:@"alipay_account"]];
        NSString*can_update_alipay = [JsonService checkJson:[jsonObject objectForKey:@"can_update_alipay"]];
        if(can_update_alipay.intValue==1) {
//            amountBtn.enabled = YES;
            arrowLabel.hidden = NO;
        } else {
//            amountBtn.enabled = NO;
            arrowLabel.hidden = YES;
        }
        if(alipay_name.length==0){
            amountTLabel.text = NSLocalizedString(@"R5024", @"未绑定");
            amountCLabel.text = NSLocalizedString(@"R2090", @"点击绑定");
        } else {
            amountTLabel.text = alipay_name;
            amountCLabel.text = alipay_account;
        }
    }
    
    if ([sender isEqualToString:@"apply"]) {
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5018", @"提交成功")];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self RBNavLeftBtnAction];
                       });
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
