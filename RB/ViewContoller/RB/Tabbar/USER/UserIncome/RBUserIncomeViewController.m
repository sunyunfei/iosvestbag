//
//  RBUserIncomeViewController.m
//  RB
//
//  Created by AngusNi on 16/2/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBUserIncomeViewController.h"

@interface RBUserIncomeViewController (){
    UIImageView*navLeftIV;
    NSMutableArray*titlesArray;
    NSMutableArray*dataArray;
    MCLineChartView*lineChartView;
    RBAmountEntity*amountEntity;
    //
    UILabel *incomeLabel;
    UILabel *incomeLeftCLabel1;
    UILabel *incomeLeftCLabel;
    UILabel *incomeCenterCLabel;
    UILabel *incomeRightCLabel;
    //
    TTTAttributedLabel*tagLeftLabel;
    TTTAttributedLabel*tagRightLabel;
    //
    UIButton*footerBtn;
}
@end

@implementation RBUserIncomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5030", @"我的钱包");
    //
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize labelSize = [NSLocalizedString(@"R5028", @"账单") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_cu_15,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
    UIButton*rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.navView.width-labelSize.width-CellLeft, StatusHeight, labelSize.width, 44.0);
    rightBtn.titleLabel.font = font_15;
    [rightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
    [rightBtn setTitle:NSLocalizedString(@"R5028", @"账单") forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(incomeListBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:rightBtn];
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    // RB-获取用户账单
    [self.hudView show];
    Handler*handler=[Handler shareHandler];
    handler.delegate=self;
    [handler getRBAccount];
    //
    [TalkingData trackPageBegin:@"my-income"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-income"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.frame = CGRectMake(0,0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIScrollView*scrollviewn = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    [scrollviewn setShowsHorizontalScrollIndicator:NO];
    [scrollviewn setShowsVerticalScrollIndicator:NO];
    scrollviewn.userInteractionEnabled = YES;
    [self.view addSubview:scrollviewn];
    //
    UIImageView*incomeBgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 140.0)];
    incomeBgIV.image = [UIImage imageNamed:@"pic_income_bg.jpg"];
    incomeBgIV.userInteractionEnabled = YES;
    [scrollviewn addSubview:incomeBgIV];
    //
//    UIButton*incomeQABtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [incomeQABtn setBackgroundImage:[UIImage imageNamed:@"icon_income_qa_1.png"] forState:UIControlStateNormal];
//    incomeQABtn.frame = CGRectMake(incomeBgIV.width-20.0-15.0, 15.0, 20.0, 20.0);
//    [incomeQABtn addTarget:self
//                   action:@selector(incomeQABtnAction:)
//         forControlEvents:UIControlEventTouchUpInside];
//    [incomeBgIV addSubview:incomeQABtn];
    //
    UIImageView*incomeTextBgIV = [[UIImageView alloc]initWithFrame:CGRectMake((incomeBgIV.width-45.0)/2.0, 30.0, 45.0, 28.0)];
    incomeTextBgIV.image = [UIImage imageNamed:@"icon_income_text_bg.png"];
    incomeTextBgIV.userInteractionEnabled = YES;
    [incomeBgIV addSubview:incomeTextBgIV];
    //
    UILabel *incomeTextLabel = [[UILabel alloc] initWithFrame:CGRectMake((incomeBgIV.width-45.0)/2.0, 31.0, incomeTextBgIV.width, 20.0)];
    incomeTextLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    incomeTextLabel.text = NSLocalizedString(@"R5031",@"总收益>");
    incomeTextLabel.font = font_cu_cu_11;
    incomeTextLabel.textAlignment = NSTextAlignmentCenter;
    [incomeBgIV addSubview:incomeTextLabel];
    //
    incomeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, incomeTextLabel.bottom+10.0, incomeBgIV.width, 40.0)];
    incomeLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    incomeLabel.font = font_cu_(40.0);
    incomeLabel.textAlignment = NSTextAlignmentCenter;
    [incomeBgIV addSubview:incomeLabel];
    //
    UIButton*incomeListBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    incomeListBtn.frame = CGRectMake((incomeBgIV.width-60.0)/2.0, 27.0, 60.0, 60.0);
    [incomeListBtn addTarget:self
                    action:@selector(incomeListBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];
    [incomeBgIV addSubview:incomeListBtn];
    //
    UILabel *moneyLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, incomeBgIV.bottom+70.0, incomeBgIV.width, 1.0)];
    moneyLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [scrollviewn addSubview:moneyLineLabel];
    //
    UILabel *moneyCLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(incomeBgIV.width/4.0-0.5f, incomeBgIV.bottom+10.0, 1.0f, 50.0)];
    moneyCLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [scrollviewn addSubview:moneyCLineLabel];
    //
    UILabel *moneyCLineLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(incomeBgIV.width*2.0/4.0-0.5f, incomeBgIV.bottom+10.0, 1.0f, 50.0)];
    moneyCLineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [scrollviewn addSubview:moneyCLineLabel1];
    //
    UILabel *moneyCLineLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(incomeBgIV.width*3.0/4.0-0.5f, incomeBgIV.bottom+10.0, 1.0f, 50.0)];
    moneyCLineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [scrollviewn addSubview:moneyCLineLabel2];
    //
    UILabel *incomeLeftLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0,incomeBgIV.bottom+15.0,self.view.width/4.0, 15.0)];
    incomeLeftLabel1.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    incomeLeftLabel1.text = NSLocalizedString(@"R5215", @"已花费");
    incomeLeftLabel1.font = font_11;
    incomeLeftLabel1.textAlignment = NSTextAlignmentCenter;
    [scrollviewn addSubview:incomeLeftLabel1];
    //
    incomeLeftCLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0,incomeLeftLabel1.bottom,incomeLeftLabel1.width, 22.0)];
    incomeLeftCLabel1.textColor=[Utils getUIColorWithHexString:SysColorBlack];
    incomeLeftCLabel1.font = font_cu_(21.0);
    incomeLeftCLabel1.textAlignment = NSTextAlignmentCenter;
    [scrollviewn addSubview:incomeLeftCLabel1];
    //
    UILabel *incomeLeftLabel = [[UILabel alloc] initWithFrame:CGRectMake(incomeLeftLabel1.right,incomeBgIV.bottom+15.0,incomeLeftLabel1.width, 15.0)];
    incomeLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    incomeLeftLabel.text = NSLocalizedString(@"R5032", @"已提现");
    incomeLeftLabel.font = font_11;
    incomeLeftLabel.textAlignment = NSTextAlignmentCenter;
    [scrollviewn addSubview:incomeLeftLabel];
    //
    incomeLeftCLabel = [[UILabel alloc] initWithFrame:CGRectMake(incomeLeftLabel1.right,incomeLeftLabel.bottom,incomeLeftLabel.width, 22.0)];
    incomeLeftCLabel.textColor=[Utils getUIColorWithHexString:SysColorBlack];
    incomeLeftCLabel.font = font_cu_(21.0);
    incomeLeftCLabel.textAlignment = NSTextAlignmentCenter;
    [scrollviewn addSubview:incomeLeftCLabel];
    //
    UILabel *incomeCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(incomeLeftLabel.right,incomeLeftLabel.top,incomeLeftLabel.width, incomeLeftLabel.height)];
    incomeCenterLabel.textColor = incomeLeftLabel.textColor;
    incomeCenterLabel.text = NSLocalizedString(@"R5033", @"提现中");
    incomeCenterLabel.font = incomeLeftLabel.font;
    incomeCenterLabel.textAlignment = NSTextAlignmentCenter;
    [scrollviewn addSubview:incomeCenterLabel];
    //
    incomeCenterCLabel = [[UILabel alloc] initWithFrame:CGRectMake(incomeCenterLabel.left,incomeCenterLabel.bottom,incomeCenterLabel.width, incomeLeftCLabel.height)];
    incomeCenterCLabel.textColor=[Utils getUIColorWithHexString:SysColorBlack];
    incomeCenterCLabel.font = font_cu_(21.0);
    incomeCenterCLabel.textAlignment = NSTextAlignmentCenter;
    [scrollviewn addSubview:incomeCenterCLabel];
    //
    UILabel *incomeRightLabel = [[UILabel alloc] initWithFrame:CGRectMake(incomeCenterLabel.right,incomeLeftLabel.top,incomeLeftLabel.width, incomeLeftLabel.height)];
    incomeRightLabel.textColor = incomeLeftLabel.textColor;
    incomeRightLabel.text = NSLocalizedString(@"R5034", @"可提现");
    incomeRightLabel.font = incomeLeftLabel.font;
    incomeRightLabel.textAlignment = NSTextAlignmentCenter;
    [scrollviewn addSubview:incomeRightLabel];
    //
    incomeRightCLabel = [[UILabel alloc] initWithFrame:CGRectMake(incomeRightLabel.left,incomeRightLabel.bottom,incomeRightLabel.width, incomeLeftCLabel.height)];
    incomeRightCLabel.textColor=[Utils getUIColorWithHexString:SysColorBlack];
    incomeRightCLabel.font = font_cu_(21.0);
    incomeRightCLabel.textAlignment = NSTextAlignmentCenter;
    [scrollviewn addSubview:incomeRightCLabel];
    //
    UILabel*lineTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, moneyLineLabel.bottom+20.0,ScreenWidth, 16.0)];
    lineTitleLabel.text = NSLocalizedString(@"R5191", @"7日收益趋势");
    lineTitleLabel.font = font_15;
    lineTitleLabel.textAlignment = NSTextAlignmentCenter;
    lineTitleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [scrollviewn addSubview:lineTitleLabel];
    //
    lineChartView = [[MCLineChartView alloc] initWithFrame:CGRectMake(0, lineTitleLabel.bottom-5.0, ScreenWidth, 200)];
    lineChartView.dataSource = self;
    lineChartView.delegate = self;
    lineChartView.minValue = @0;
    lineChartView.solidDot = YES;
    lineChartView.numberOfYAxis = 3;
    lineChartView.xFontSize = 12.0;
    lineChartView.yFontSize = 12.0;
    lineChartView.colorOfXAxis = [UIColor clearColor];
    lineChartView.colorOfXText = [Utils getUIColorWithHexString:SysColorSubGray];
    lineChartView.colorOfYAxis = [UIColor clearColor];
    lineChartView.colorOfYText = [Utils getUIColorWithHexString:SysColorSubGray];
    [scrollviewn addSubview:lineChartView];
    //
    UILabel*lineSubTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(18.0, lineChartView.bottom+5.0,ScreenWidth-36.0, 14.0)];
    lineSubTitleLabel.text = NSLocalizedString(@"R5192", @"*图中显示金额包含预计收入(待审核收入)");
    lineSubTitleLabel.font = font_11;
    lineSubTitleLabel.textAlignment = NSTextAlignmentCenter;
    lineSubTitleLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [scrollviewn addSubview:lineSubTitleLabel];
    //
    UIImageView*tagIV = [[UIImageView alloc]initWithFrame:CGRectMake( (ScreenWidth-16.0)/2.0, lineSubTitleLabel.bottom+CellBottom, 16.0, 9.0)];
    tagIV.image = [UIImage imageNamed:@"icon_income_text_bg_tag.png"];
    [scrollviewn addSubview:tagIV];
    //
    UIImageView*textIV = [[UIImageView alloc]initWithFrame:CGRectMake(15.0, tagIV.bottom, ScreenWidth-15.0*2.0, 65.0)];
    textIV.image = [UIImage imageNamed:@"pic_income_text_bg.jpg"];
    [scrollviewn addSubview:textIV];
    //
    tagLeftLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(CellLeft, 0, textIV.width-CellLeft, textIV.height)];
    tagLeftLabel.font = font_cu_cu_17;
    tagLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [textIV addSubview:tagLeftLabel];
    //
    tagRightLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(0, 0, tagLeftLabel.width-CellLeft, tagLeftLabel.height)];
    tagRightLabel.font = font_cu_cu_17;
    tagRightLabel.textColor = tagLeftLabel.textColor;
    tagRightLabel.textAlignment = NSTextAlignmentRight;
    [textIV addSubview:tagRightLabel];
    //
    [scrollviewn setContentSize:CGSizeMake(ScreenWidth, textIV.bottom)];
}

#pragma mark - UIButton Delegate
- (void)incomeQABtnAction:(UIButton *)sender {
    RBUserIncomeQAViewController *toview  =
    [[RBUserIncomeQAViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)incomeListBtnAction:(UIButton*)sender {
    RBUserIncomeListViewController *toview  =
    [[RBUserIncomeListViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    RBUserIncomeCashViewController *toview  =
    [[RBUserIncomeCashViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)loadTextBottomView:(RBAmountEntity*)entity {
    NSString*total_amount = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:entity.total_amount]];
    tagLeftLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R6027", @"共 %@ 笔收入"),entity.count];
    tagRightLabel.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total_amount];
    //
    [tagLeftLabel setText:tagLeftLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:[NSString stringWithFormat:@"%@",entity.count] options:NSCaseInsensitiveSearch];
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range];
         return mStr;
     }];
    //
    [tagRightLabel setText:tagRightLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:[NSString stringWithFormat:@"￥%@",total_amount] options:NSCaseInsensitiveSearch];
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorWhite] CGColor]} range:range];
         return mStr;
     }];
}

#pragma mark - MCLineChartView Delegate
- (void)lineChartView:(MCLineChartView *)lineChartView selectedAt:(int)index {
    RBAmountEntity*entity=amountEntity.stats[index];
    [self loadTextBottomView:entity];
}

- (NSUInteger)numberOfLinesInLineChartView:(MCLineChartView *)lineChartView {
    return 1;
}

- (NSUInteger)lineChartView:(MCLineChartView *)lineChartView lineCountAtLineNumber:(NSInteger)number {
    return [dataArray count];
}

- (id)lineChartView:(MCLineChartView *)lineChartView valueAtLineNumber:(NSInteger)lineNumber index:(NSInteger)index {
    return dataArray[index];
}

- (NSString *)lineChartView:(MCLineChartView *)lineChartView titleAtLineNumber:(NSInteger)number {
    return titlesArray[number];
}

- (UIColor *)lineChartView:(MCLineChartView *)lineChartView lineColorWithLineNumber:(NSInteger)lineNumber {
    return [Utils getUIColorWithHexString:SysColorYellow];
}
-(NSString*)removeFloatAllZero:(NSString*)string
{
    NSString * testNumber = string;
    NSString * outNumber = [NSString stringWithFormat:@"%@",@(testNumber.floatValue)];
    
    //    价格格式化显示
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = kCFNumberFormatterDecimalStyle;
    NSString *formatterString = [formatter stringFromNumber:[NSNumber numberWithFloat:[outNumber doubleValue]]];
    
    NSRange range = [formatterString rangeOfString:@"."]; //现获取要截取的字符串位置
    NSLog(@"--------%lu",(unsigned long)range.length);
    
    if (range.length>0) {
        
        NSString * result = [formatterString substringFromIndex:range.location]; //截取字符串
        
        if (result.length>=4) {
            
            formatterString=[formatterString substringToIndex:formatterString.length-1];
        }
    }
    NSLog(@"Formatted number string:%@",formatterString);
    
    NSLog(@"Formatted number string:%@",outNumber);
    //    输出结果为：[1223:403] Formatted number string:123,456,789
    
    return formatterString;
}
#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"account"]) {
        [self.hudView dismiss];
        amountEntity = [JsonService getRBAmountEntity:jsonObject];
        if([NSString stringWithFormat:@"%@",amountEntity.avail_amount].intValue>=50){
            [footerBtn setTitle:NSLocalizedString(@"R5222", @"立即提现提现") forState:UIControlStateNormal];
            footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
            footerBtn.enabled = YES;
        } else {
           // [footerBtn setTitle:NSLocalizedString(@"R5027", @"满50元即可提现") forState:UIControlStateNormal];
            NSString * str = [NSString stringWithFormat:@"%.2f",50.0 - [amountEntity.avail_amount doubleValue]];
            
            [footerBtn setTitle:[NSString stringWithFormat:@"还差%@元即可提现",[self removeFloatAllZero:str]] forState:UIControlStateNormal];
            footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
            footerBtn.enabled = NO;
        }
        NSString*total_withdraw = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:amountEntity.total_withdraw]];
        NSString*frozen_amount = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:amountEntity.frozen_amount]];
        NSString*avail_amount = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:amountEntity.avail_amount]];
        NSString*total_expense = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:amountEntity.total_expense]];
        NSString*total_income = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:amountEntity.total_income]];
        incomeLeftCLabel1.text = [NSString stringWithFormat:@"%@",total_expense];
        incomeLabel.text = [NSString stringWithFormat:@"%@",total_income];
        incomeLeftCLabel.text = [NSString stringWithFormat:@"%@",total_withdraw];
        incomeCenterCLabel.text = [NSString stringWithFormat:@"%@",frozen_amount];
        incomeRightCLabel.text = [NSString stringWithFormat:@"%@",avail_amount];
        //
        titlesArray = [NSMutableArray new];
        dataArray = [NSMutableArray new];
        float max=0;
        for (int i=0; i<[amountEntity.stats count]; i++) {
            RBAmountEntity*entity = amountEntity.stats[i];
            if ([[[NSString stringWithFormat:@"%@",entity.date]componentsSeparatedByString:@"-"]count]>2) {
                NSString*month = [[NSString stringWithFormat:@"%@",entity.date]componentsSeparatedByString:@"-"][1];
                NSString*day = [[NSString stringWithFormat:@"%@",entity.date]componentsSeparatedByString:@"-"][2];
                [titlesArray addObject:[NSString stringWithFormat:@"%@/%@",month,day]];
            }
            if ([NSString stringWithFormat:@"%@",entity.total_amount].floatValue>max) {
                max = [NSString stringWithFormat:@"%@",entity.total_amount].floatValue;
            }
            [dataArray addObject:[NSString stringWithFormat:@"%@",entity.total_amount]];
            if (i==[amountEntity.stats count]-1) {
                [self loadTextBottomView:entity];
            }
        }
        if (max!=0) {
            lineChartView.maxValue = [NSString stringWithFormat:@"%.2f",max*1.2];
        } else {
            lineChartView.maxValue = @30;
        }
        [lineChartView reloadData];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
@end
