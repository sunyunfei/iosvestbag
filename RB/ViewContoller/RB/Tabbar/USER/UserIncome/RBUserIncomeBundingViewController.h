//
//  RBUserIncomeBundingViewController.h
//  RB
//
//  Created by AngusNi on 5/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBUserIncomeBundingViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
@property(nonatomic, strong) id jsonObjectStr;
@end
