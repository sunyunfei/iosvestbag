//
//  RBKolSearchListViewController.h
//  RB
//
//  Created by AngusNi on 16/7/27.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolListView.h"
#import "RBKolDetailViewController.h"
#import "RBEngineViewController.h"

@interface RBUserFollowersViewController : RBBaseViewController
<RBBaseVCDelegate,RBKolListViewDelegate>
@property(nonatomic, strong) RBKolListView *kolListView;
@end
