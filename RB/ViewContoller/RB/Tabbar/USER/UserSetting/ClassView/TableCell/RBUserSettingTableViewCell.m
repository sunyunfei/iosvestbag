//
//  RBUserSettingTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBUserSettingTableViewCell.h"

@implementation RBUserSettingTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1001;
        [self.contentView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel.font = font_13;
        cLabel.textAlignment = NSTextAlignmentRight;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        cLabel.tag = 1002;
        [self.contentView addSubview:cLabel];
        //
        UILabel*arrowLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        arrowLabel.font = font_icon_(13.0);
        arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        arrowLabel.text = Icon_btn_right;
        arrowLabel.tag = 1003;
        [self.contentView addSubview:arrowLabel];
        //
        self.sharkeSwitch =
        [[RBSwitch alloc] initWithFrame:CGRectMake(ScreenWidth-50.0-CellLeft, (50.0-30.0)/2.0, 50, 30)];
        self.sharkeSwitch.tag = 1004;
        self.sharkeSwitch.onTintColor = [Utils getUIColorWithHexString:SysColorBlue];
        self.sharkeSwitch.tintColor = [Utils getUIColorWithHexString:SysColorBlue];
        [self.contentView addSubview:self.sharkeSwitch];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1010;
        [self.contentView addSubview:lineLabel];
    }
    return self;
}

- (instancetype)loadRBUserSettingCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath andCacheCount:(NSString*)cacheCount {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1001];
    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1002];
    UILabel*arrowLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    //
    tLabel.text = datalist[indexPath.row];
    cLabel.text = @"";
    self.sharkeSwitch.hidden = YES;
    cLabel.frame=CGRectMake(ScreenWidth-90.0-CellLeft-13.0-5.0, 0, 90.0, 50.0);
    if (indexPath.row == 0) {
        cLabel.text = @"10:00 - 17:00";
    }
    if (indexPath.row == 1) {
        cLabel.text = @"";
    }
    if (indexPath.row == 2) {
        arrowLabel.hidden = YES;
        self.sharkeSwitch.hidden = NO;
        self.sharkeSwitch.typeStr = @"infopush";
        if([LocalService getRBLocalDataUserPush]==nil||[LocalService getRBLocalDataUserPush].length==0){
            self.sharkeSwitch.on = YES;
        } else {
            self.sharkeSwitch.on = NO;
        }
    }
    if (indexPath.row == 3) {
        cLabel.text = cacheCount;
    }
    if (indexPath.row == 4) {
        arrowLabel.hidden = YES;
        self.sharkeSwitch.hidden = NO;
        self.sharkeSwitch.typeStr = @"feedback";
        if([LocalService getRBLocalDataUserShake]==nil||[LocalService getRBLocalDataUserShake].length==0){
            self.sharkeSwitch.on = YES;
        } else {
            self.sharkeSwitch.on = NO;
        }
    }
    if (indexPath.row == 5) {
        cLabel.text = @"";
    }
    if (indexPath.row == [datalist count]-1) {
        cLabel.text = [Utils getCurrentBundleShortVersion];
    }
    tLabel.frame=CGRectMake(CellLeft, 0, ScreenWidth-CellLeft*2.0, 50.0);
    arrowLabel.frame=CGRectMake(ScreenWidth-CellLeft-13.0, (tLabel.height-13.0)/2.0, 13.0, 13.0);
    lineLabel.frame=CGRectMake(0, tLabel.height-0.5, ScreenWidth, 0.5);
    bgView.frame=CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame=CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}


@end
