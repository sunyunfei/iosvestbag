//
//  RBInfoTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "RBButton.h"
#import "RBSwitch.h"

@interface RBUserSettingTableViewCell : UITableViewCell
- (instancetype)loadRBUserSettingCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath andCacheCount:(NSString*)cacheCount;
@property(nonatomic, strong) RBSwitch*sharkeSwitch;

@end
