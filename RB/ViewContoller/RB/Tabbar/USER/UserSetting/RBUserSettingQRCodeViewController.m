//
//  RBUserSettingQRCodeViewController.m
//
//  Created by angusni on 15/6/4.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBUserSettingQRCodeViewController.h"

@interface RBUserSettingQRCodeViewController () {
    UIImageView                 *animationIV;
    AVCaptureSession            *session;
    AVCaptureVideoPreviewLayer  *preview;
    NSString                    *userID;
    NSString                    *userName;
}
@end

@implementation RBUserSettingQRCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5213", @"扫一扫登录官网");
    self.navView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    self.view.backgroundColor = [UIColor clearColor];
    //
    UIImageView *scanBgIV = [[UIImageView alloc]initWithFrame:CGRectMake(50, 100 + NavHeight, self.view.width - 100, self.view.width - 100)];
    scanBgIV.backgroundColor = [UIColor clearColor];
    scanBgIV.image = [UIImage imageNamed:@"pic_user_code_bg.png"];
    [self.view addSubview:scanBgIV];
    //
    animationIV = [[UIImageView alloc] initWithFrame:CGRectMake(50, 100 + NavHeight, self.view.width - 100, (self.view.width - 100) / 600.0 * 8.0)];
    animationIV.image = [UIImage imageNamed:@"pic_user_code_line.png"];
    [self.view addSubview:animationIV];
    //
    UILabel *aboutLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, scanBgIV.bottom, self.view.width, 25)];
    aboutLabel.backgroundColor = [UIColor clearColor];
    aboutLabel.textAlignment = NSTextAlignmentCenter;
    aboutLabel.font = [UIFont systemFontOfSize:15.0f];
    aboutLabel.textColor = [Utils getUIColorWithHexString:@"d2d2d2"];
    aboutLabel.text = @"将二维码放入框内，即可自动扫描";
    [self.view addSubview:aboutLabel];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupCamera];
    [self timerAnimation];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [animationIV.layer removeAllAnimations];
    [session stopRunning];
    session = nil;
    [preview removeFromSuperlayer];
    preview = nil;
}

#pragma mark - UINavigationBar Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - animations Delegate
- (void)timerAnimation {
    [UIView animateWithDuration:2 animations:^{
        NSLog(@"动画开始");
        // 路径曲线
        CGPoint fromPoint = animationIV.center;
        UIBezierPath *movePath = [UIBezierPath bezierPath];
        [movePath moveToPoint:fromPoint];
        CGPoint toPoint = CGPointMake(fromPoint.x, fromPoint.y + animationIV.frame.size.width);
        [movePath addLineToPoint:toPoint];
        // 关键帧位置变化
        CAKeyframeAnimation *moveAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        moveAnim.path = movePath.CGPath;
        // 关键帧，旋转，透明度组合起来执行
        CAAnimationGroup *animGroup = [CAAnimationGroup animation];
        animGroup.animations = @[moveAnim];
        animGroup.removedOnCompletion = YES;
        animGroup.duration = 2;         // 动画的持续时间
        animGroup.repeatCount = 100000; // 动画的重复次数
        [animationIV.layer addAnimation:animGroup forKey:nil];
    } completion:^(BOOL finished) {
        NSLog(@"动画结束");
    }];
}

#pragma mark - Camera Delegate
- (void)setupCamera {
    // Device
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    // Input
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    // Output
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc]init];

    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    // Session
    session = [[AVCaptureSession alloc]init];
    [session setSessionPreset:AVCaptureSessionPresetHigh];

    if ([session canAddInput:input]) {
        [session addInput:input];
    }

    if ([session canAddOutput:output]) {
        [session addOutput:output];
    }

    // 条码类型
    if ([[output availableMetadataObjectTypes]count] == 0) {
        return;
    }

    output.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];
    // Preview
    preview = [AVCaptureVideoPreviewLayer layerWithSession:session];
    preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
    preview.frame = CGRectMake(0, [Utils getIOS7TabBarHeight], self.view.frame.size.width, self.view.frame.size.height - [Utils getIOS7TabBarHeight]);
    [self.view.layer insertSublayer:preview atIndex:0];
    // Start
    [session startRunning];
}

#pragma mark - AVCaptureOutput Delegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    NSString *stringValue = @"";
    if ([metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObject = [metadataObjects objectAtIndex:0];
        stringValue = metadataObject.stringValue;
    }
    NSLog(@"stringValue:%@", stringValue);
    if (stringValue.length != 0) {
        [animationIV.layer removeAllAnimations];
        [session stopRunning];
        session = nil;
        [preview removeFromSuperlayer];
        preview = nil;
        [self.hudView show];
        // RB-获取扫码登录
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBUserHelpScanCode:stringValue];
    }
}


#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"scan_qr_code_and_login"]) {
        [self.hudView showSuccessWithStatus:@"登录成功"];
        [self RBNavLeftBtnAction];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
    [self setupCamera];
    [self timerAnimation];
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
    [self setupCamera];
    [self timerAnimation];
}

@end
