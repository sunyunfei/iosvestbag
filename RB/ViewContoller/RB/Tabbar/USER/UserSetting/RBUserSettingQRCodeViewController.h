//
//  ASUserQRCodeViewController.h
//
//  Created by angusni on 15/6/4.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
@interface RBUserSettingQRCodeViewController : RBBaseViewController
<RBBaseVCDelegate, AVCaptureMetadataOutputObjectsDelegate>
@end
