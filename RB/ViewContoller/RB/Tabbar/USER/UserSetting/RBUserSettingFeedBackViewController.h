//
//  RBUserSettingFeedBackViewController.h
//  RB
//
//  Created by AngusNi on 16/2/19.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBUserSettingFeedBackViewController : RBBaseViewController
<RBBaseVCDelegate,TextViewKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
@property(nonatomic, strong) UIImage *feedbackImage;

@end
