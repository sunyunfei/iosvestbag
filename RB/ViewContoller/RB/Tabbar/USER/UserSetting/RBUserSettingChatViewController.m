//
//  RBUserSettingChatViewController.m
//  RB
//
//  Created by AngusNi on 4/22/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserSettingChatViewController.h"

@interface RBUserSettingChatViewController ()

@end

@implementation RBUserSettingChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Utils getIOS7UINavigationController:self];
    // 禁用 iOS7 返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    [TalkingData trackPageBegin:@"my-service"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-service"];
}

@end
