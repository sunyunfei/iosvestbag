//
//  RBUserSettingChatViewController.h
//  RB
//
//  Created by AngusNi on 4/22/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>
#import "RBBaseViewController.h"

@interface RBUserSettingChatViewController : RCPublicServiceChatViewController
<UIGestureRecognizerDelegate>
@end
