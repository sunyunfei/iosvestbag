//
//  RBUserSettingFeedBackViewController.m
//  RB
//
//  Created by AngusNi on 16/2/19.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBUserSettingFeedBackViewController.h"
@interface RBUserSettingFeedBackViewController () {
    UIView*editView;
    PlaceholderTextView*contentTV;
}
@end

@implementation RBUserSettingFeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5103", @"意见反馈");
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-feedback"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-feedback"];
}

#pragma mark- NSNotification Delegate
- (void)RBNotificationShowFeedbackView {
    
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    editView = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    editView.userInteractionEnabled = YES;
    [self.view addSubview:editView];
    //
    contentTV = [[PlaceholderTextView alloc]initWithFrame:CGRectMake(CellLeft, CellLeft, ScreenWidth-CellLeft*2.0, 100)];
    contentTV.font = font_15;
    contentTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    contentTV.keyboardType = UIKeyboardTypeDefault;
    contentTV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
    contentTV.layer.borderWidth = 0.5f;
    contentTV.layer.masksToBounds = YES;
    contentTV.placeholderColor = [Utils getUIColorWithHexString:SysColorSubGray];
    contentTV.placeholderFont = font_13;
    contentTV.placeholder = NSLocalizedString(@"R5104", @"请描述您遇到的问题");
    [editView addSubview:contentTV];
    TextViewKeyBoard*textViewKB = [[TextViewKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textViewKB.delegateP = self;
    [contentTV setInputAccessoryView:textViewKB];
    //
    if(self.feedbackImage!=nil) {
        UIImageView*contentIV = [[UIImageView alloc]initWithFrame:CGRectMake(contentTV.left, contentTV.bottom+CellLeft, contentTV.width, editView.height-contentTV.bottom-CellLeft*2.0)];
        contentIV.contentMode = UIViewContentModeScaleToFill;
        contentIV.layer.masksToBounds = YES;
        contentIV.layer.borderWidth = 0.5f;
        contentIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
        contentIV.image = self.feedbackImage;
        [editView addSubview:contentIV];
    }
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self dismissVC];
}

- (void)footerBtnAction:(UIButton *)sender {
    if (contentTV.text.length>=200) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R6066", @"输入内容过长")];
        return;
    }
    if (contentTV.text.length==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5104", @"请描述您遇到的问题")];
        return;
    }
    [self.hudView showOverlay];
    if(self.feedbackImage!=nil) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *tempimg = [Utils
                                getUIImageScalingFromSourceImage:self.feedbackImage
                                targetSize:self.feedbackImage.size];
            dispatch_async(dispatch_get_main_queue(), ^{
                Handler*handler = [Handler shareHandler];
                handler.delegate = self;
                [handler getRBUserHelpFeedBackWithContent:contentTV.text andScreen:tempimg];
            });
        });
    } else {
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBUserHelpFeedBackWithContent:contentTV.text andScreen:nil];
    }
}

#pragma mark - TextViewKeyBoard Delegate
- (void)TextViewKeyBoardFinish:(TextViewKeyBoard *)textViewKeyBoardView {
    [self.view endEditing:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"feedbacks"]) {
        [self.hudView showSuccessWithStatus:@"提交成功,更多问题请联系在线客服"];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self RBNavLeftBtnAction];
                       });
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
