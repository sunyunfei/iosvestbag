//
//  RBUserSettingViewController.m
//  RB
//
//  Created by AngusNi on 16/2/18.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBUserSettingViewController.h"
#import "RBAppDelegate.h"
#import "RegularHelp.h"
@interface RBUserSettingViewController (){
    UITableView*tableviewn;
    NSMutableArray*datalist;    
    NSString*cacheCount;
}
@end

@implementation RBUserSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5105", @"设置");
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height - NavHeight)
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
    //
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth , 45.0*2.0)];
    [tableviewn setTableFooterView:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    footerBtn.frame = CGRectMake((footerView.width-200.0)/2.0, footerView.height-45.0, 200.0, 45.0);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R5106", @"退出登录") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    footerBtn.layer.borderWidth = 1.0;
    footerBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
    [footerView addSubview:footerBtn];
    //
    datalist = [[NSMutableArray alloc]initWithArray:@[NSLocalizedString(@"R5107", @"在线客服"),NSLocalizedString(@"R5213", @"扫一扫登录官网"),NSLocalizedString(@"R5108", @"开启消息提醒"),NSLocalizedString(@"R5109", @"清除图片缓存"),NSLocalizedString(@"R5110", @"开启摇一摇反馈"),NSLocalizedString(@"R5162",@"好评鼓励"),NSLocalizedString(@"R5111", @"关于Robin8")]];
    //
    [self getCacheSize];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-setting"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-setting"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton*)sender {
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    actionSheet.tag = 6000;
    [actionSheet showWithArray:@[NSLocalizedString(@"R5106", @"退出登录"),NSLocalizedString(@"R1011", @"取消")]];
}

- (void)getCacheSize {
    dispatch_async(
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                       cacheCount = [NSString stringWithFormat:@"%.2fM",([[SDImageCache sharedImageCache]getSize]+[YYWebImageManager sharedManager].cache.diskCache.totalCost)/1024.0/1024.0];
                       dispatch_async(dispatch_get_main_queue(), ^{
                           [UIView performWithoutAnimation:^{
                               [tableviewn reloadData];
                           }];
                       });
                   });
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if (actionSheet.tag == 6000) {
        if (buttonIndex == 0) {
            [GeTuiSdk setPushModeForOff:YES];
            [LocalService setRBFirstInfluenceenter:nil];
            [Utils getFileDeleteWithFileName:@"Login" andFilePath:@"RB"];
            [LocalService setRBLocalDataUserGuideWith:nil];
            [LocalService setRBLocalDataUserPrivateTokenWith:nil];
            [LocalService setRBLocalDataUserRongTokenWith:nil];
            [LocalService setRBLocalDataUserLoginPhoneWith:nil];
            [LocalService setRBLocalEmail:nil];
            // 离线数据
            [Utils getFileDeleteWithFileName:@"RBCampaign" andFilePath:@"RB"];
            [Utils getFileDeleteWithFileName:@"RBKol" andFilePath:@"RB"];
            [Utils getFileDeleteWithFileName:@"RBKolNew" andFilePath:@"RB"];
            [Utils getFileDeleteWithFileName:@"RBUser" andFilePath:@"RB"];
            //去掉保存的微信的UID
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UID"];
            //去除红包
            [[NSNotificationCenter defaultCenter]postNotificationName:@"addRedBag" object:nil userInfo:nil];
            //
            [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5112", @"退出登录成功")];
            dispatch_after(
                           dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(), ^{
                               RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
                               [appDelegate loadViewController:0];
                           });
        }
    }
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBUserSettingTableViewCell*cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBUserSettingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:cellIdentifier];
    }
    [cell loadRBUserSettingCellWith:datalist andIndex:indexPath andCacheCount:cacheCount];
    [cell.sharkeSwitch addTarget:self
                action          :@selector(sharkeSwitchAction:)
                forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

#pragma mark - RBSwitch Delegate
- (void)sharkeSwitchAction:(RBSwitch *)sender {
    if([sender.typeStr isEqualToString:@"infopush"]) {
        if([LocalService getRBLocalDataUserPush]==nil || [LocalService getRBLocalDataUserPush].length==0){
            [LocalService setRBLocalDataUserPushWith:@"NO"];
            sender.on = NO;
            [GeTuiSdk setPushModeForOff:YES];
        } else {
            if ([[UIApplication sharedApplication] currentUserNotificationSettings].types == UIUserNotificationTypeNone) {
                UILocalNotification *notification = [[UILocalNotification alloc] init];
                notification.alertBody = @"通知未打开,请设置打开";
                [[UIApplication sharedApplication]
                 presentLocalNotificationNow:notification];
                return;
            }
            [LocalService setRBLocalDataUserPushWith:nil];
            sender.on = YES;
            [GeTuiSdk setPushModeForOff:NO];
        }
    }
    
    if([sender.typeStr isEqualToString:@"feedback"]){
        if([LocalService getRBLocalDataUserShake]==nil||[LocalService getRBLocalDataUserShake].length==0){
            [LocalService setRBLocalDataUserShakeWith:@"NO"];;
            sender.on = NO;
            [[UIApplication sharedApplication] setApplicationSupportsShakeToEdit:NO];
        } else {
            [LocalService setRBLocalDataUserShakeWith:nil];
            sender.on = YES;
            [[UIApplication sharedApplication] setApplicationSupportsShakeToEdit:YES];
        }
    }
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        self.navigationController.navigationBarHidden = NO;
        // 在线客服
        RBUserSettingChatViewController *toview = [[RBUserSettingChatViewController alloc] init];
        toview.conversationType = ConversationType_APPSERVICE;
        toview.targetId = RCIMServiceID;
        toview.title = NSLocalizedString(@"R5107", @"在线客服");
        toview.hidesBottomBarWhenPushed = YES;
        toview.navigationController.navigationBarHidden = NO;
        [self.navigationController pushViewController:toview animated:YES];
        // 在线聊天
        //        RCConversationViewController *toview  = [[RCConversationViewController alloc]init];
        //        toview.targetId = @"15221779999";
        //        toview.title = @"15221779999";
        //        toview.conversationType = ConversationType_PRIVATE;
        //        toview.hidesBottomBarWhenPushed = YES;
        //        [self.navigationController pushViewController:toview animated:YES];
    }
    
    if (indexPath.row == 1) {
        RBUserSettingQRCodeViewController *toview = [[RBUserSettingQRCodeViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
    
    if (indexPath.row == 2) {
        
    }
    
    if (indexPath.row == 3) {
        [Service clearImageCache];
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5113", @"清除缓存成功")];
        [self getCacheSize];
    }
    
    if(indexPath.row == 4) {
        RBUserSettingFeedBackViewController *toview = [[RBUserSettingFeedBackViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self presentVC:toview];
    }
    
    if (indexPath.row == 5) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:AppStoreUrl]];
    }
    
    if (indexPath.row == 6) {
        TOWebViewController *toview = [[TOWebViewController alloc] init];
        toview.url = [NSURL URLWithString:@"http://www.robin8.net"];
        toview.title = NSLocalizedString(@"R5111", @"关于Robin8");
        toview.showPageTitles = NO;
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
}

@end
