//
//  RBInfoViewController.m
//  RB
//
//  Created by AngusNi on 16/2/5.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBInfoViewController.h"

@interface RBInfoViewController (){
    UITableView*tableviewn;
    NSMutableArray*datalist;
    NSMutableArray*thirdlist;
    UIView*headerView;
    RBUserEntity*userEntity;
    BOOL isNeedSend;
    TTTAttributedLabel*headerLabel;
    NSString*can_receive_complete_reward;
    NSString*had_complete_reward;
}
@end

@implementation RBInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5159",@"个人资料");
    self.bgIV.hidden = NO;
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height-NavHeight)
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
//    [self setRefreshHeaderView];
    //
    datalist = [NSMutableArray new];
    [datalist addObject:@[NSLocalizedString(@"R5188", @"头像"),NSLocalizedString(@"R5079", @"昵称"),NSLocalizedString(@"R5189", @"性别"),NSLocalizedString(@"R5070", @"年龄"),NSLocalizedString(@"R5085", @"所在地区"),NSLocalizedString(@"R5082", @"关注行业"),NSLocalizedString(@"R2087", @"微信好友数")]];
    [datalist addObject:@[NSLocalizedString(@"R1003", @"手机号码"),NSLocalizedString(@"R5081", @"社交账号")]];
    //
    isNeedSend = NO;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    //
    [self.hudView show];
    [self loadRefreshViewFirstData];
    //
    [TalkingData trackPageBegin:@"my-list"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-list"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)headerBtnAction:(UIButton*)sender {
    if(had_complete_reward.integerValue==0&&can_receive_complete_reward.intValue==1) {
        [self.hudView show];
        // RB-奖励模块-领取奖励
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBUserRewardCompleteInfo];
    } else {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5086", @"100%完善个人资料才可以获得现金奖励哦!")];
    }
}

#pragma mark - loadTableHeaderView Delegate
- (void)loadTableHeaderView {
    [headerView removeAllSubviews];
    [headerView removeFromSuperview];
    headerView = nil;
    if (headerView == nil) {
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 40.0)];
        headerView.backgroundColor = [Utils getUIColorWithHexString:SysColorSubBlack];
        //
        headerLabel = [[TTTAttributedLabel alloc] initWithFrame:headerView.bounds];
        headerLabel.font = font_cu_13;
        headerLabel.text = NSLocalizedString(@"R5087", @"100%完善个人资料 获得现金奖励!  立即领取");
        headerLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.8];
        headerLabel.textAlignment = NSTextAlignmentCenter;
        [headerView addSubview:headerLabel];
        //
        UIButton *headerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        headerBtn.frame = headerView.bounds;
        [headerBtn addTarget:self
                      action:@selector(headerBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:headerBtn];
        //
        if(had_complete_reward.integerValue==1){
            headerLabel.text = NSLocalizedString(@"R5088", @"100%完善个人资料 获得现金奖励!  已领取");
            headerBtn.enabled = NO;
        } else {
            headerBtn.enabled = YES;
            [headerLabel setText:headerLabel.text
    afterInheritingLabelAttributesAndConfiguringWithBlock:
             ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
                 NSRange range = [[mStr string] rangeOfString:NSLocalizedString(@"R5089", @"立即领取") options:NSCaseInsensitiveSearch];
                 [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorWhite] CGColor]} range:range];
                 return mStr;
             }];
        }
        [tableviewn setTableHeaderView:headerView];
    }
}

#pragma mark - loadRefreshView Delegate
- (void)loadRefreshViewFirstData {
    [self.hudView show];
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    // RB-获取用户信息
    [handler getRBUserInfo];
    // RB-获取用户第三方信息
    [handler getRBUserInfoThirdAccountList];
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshViewFinish {
    [tableviewn.mj_header endRefreshing];
    [UIView performWithoutAnimation:^{
        [tableviewn reloadData];
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    NSArray*array = datalist[section];
    return [array count];
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView*sectionView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 8.0)];
//    sectionView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    return sectionView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 8.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBInfoTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:cellIdentifier];
    }
    [cell loadRBInfoCellWith:datalist andIndex:indexPath andUserEntity:userEntity andThirdList:thirdlist];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section==0 && indexPath.row==0) {
        // 头像
        RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
        actionSheet.delegate = self;
        actionSheet.tag = 1000;
        [actionSheet showWithArray:@[NSLocalizedString(@"R2051", @"打开相册"),NSLocalizedString(@"R1011", @"取消")]];
    }
    if(indexPath.section==0 && indexPath.row==1) {
        // 昵称
        RBInfoNameViewController *toview  =
        [[RBInfoNameViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
    if(indexPath.section==0 && indexPath.row==2) {
        // 性别
        RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
        actionSheet.delegate = self;
        actionSheet.tag = 2000;
        [actionSheet showWithArray:@[NSLocalizedString(@"R5068",  @"男"),NSLocalizedString(@"R5069",  @"女"),NSLocalizedString(@"R1011", @"取消")]];
    }
    if(indexPath.section==0 && indexPath.row==3) {
        // 年龄
        RBInfoAgeViewController *toview = [[RBInfoAgeViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
    if(indexPath.section==0 && indexPath.row==4) {
        // 地区
        RBInfoCitysViewController *toview  =
        [[RBInfoCitysViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
    if(indexPath.section==0 && indexPath.row==5) {
        // 关注行业
        RBInfoTagsViewController *toview  =
        [[RBInfoTagsViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
    if(indexPath.section==0 && indexPath.row==6) {
        // 微信好友数
        RBInfoWexinCountViewController *toview  =
        [[RBInfoWexinCountViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
    if(indexPath.section==1 && indexPath.row==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5090", @"手机号码不可修改")];
    }
    if(indexPath.section==1 && indexPath.row==1) {
        // 社交账号
        RBInfoSocialViewController *toview  =
        [[RBInfoSocialViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
}

#pragma mark - RBPictureUpload Delegate
- (void)RBPictureUpload:(RBPictureUpload *)pictureUpload selectImage:(UIImage *)img {
    // RB-上传用户头像
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoAvatarUpdate:img];
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if (actionSheet.tag == 1000) {
        RBPictureUpload*picUpload = [RBPictureUpload sharedRBPictureUpload];
        picUpload.fromVC = self.navigationController;
        picUpload.delegate = self;
        if (buttonIndex == 0) {
            ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
            if (author == ALAuthorizationStatusDenied) {
                UILocalNotification *notification = [[UILocalNotification alloc] init];
                notification.alertBody = @"照片不可用,请设置打开";
                [[UIApplication sharedApplication]
                 presentLocalNotificationNow:notification];
                return;
            }
            [picUpload RBPictureUploadClickedButtonAtIndex:1];
            return;
        }
    }
    if (actionSheet.tag == 2000) {
        if (buttonIndex == 0) {
            userEntity.gender = @"1";
            NSIndexSet *indexSet = [[NSIndexSet alloc]initWithIndex:0];
            [tableviewn reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        if (buttonIndex == 1) {
            userEntity.gender = @"2";
            NSIndexSet *indexSet = [[NSIndexSet alloc]initWithIndex:0];
            [tableviewn reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        if (buttonIndex == 2) {
            return;
        }
        // RB-用户信息保存
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBUserInfoUpdateWithEmail:nil andName:nil andWeixin_friend_count:nil andAge:nil andGender:userEntity.gender andBirthday:nil andCountry:nil andCity:nil andDesc:nil andTags:nil andAlipay:nil];
    }
}


#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"complete_info"]) {
        RBSignView*signView = [[RBSignView alloc]init];
        [signView show];
        [self loadRefreshViewFirstData];
    }
    
    if ([sender isEqualToString:@"upload_avatar"]) {
        isNeedSend = YES;
        [self loadRefreshViewFirstData];
    }
    
    if ([sender isEqualToString:@"update_profile"]) {
        [JsonService setRBUserEntityWith:jsonObject];
        userEntity = [JsonService getRBUserEntity:jsonObject];
        [self loadRefreshViewFirstData];
    }
    
    if ([sender isEqualToString:@"kols/profile"]) {
        [self.hudView dismiss];
        // 是否领取
        had_complete_reward = [JsonService checkJson:[jsonObject objectForKey:@"had_complete_reward"]];
        // 是否可以领取
        can_receive_complete_reward = [JsonService checkJson:[jsonObject objectForKey:@"can_receive_complete_reward"]];
        if(had_complete_reward.integerValue==1){
            [tableviewn setTableHeaderView:nil];
        } else {
            [self loadTableHeaderView];
        }
        [JsonService setRBUserEntityWith:jsonObject];
        userEntity = [JsonService getRBUserEntity:jsonObject];
        [self setRefreshViewFinish];
        //
        if(isNeedSend) {
            isNeedSend = NO;
            [[NSNotificationQueue defaultQueue] enqueueNotification:[NSNotification notificationWithName:NotificationRefreshUserInfo object:nil] postingStyle:NSPostNow];
        }
    }
    
    if ([sender isEqualToString:@"identities"]) {
        thirdlist = [NSMutableArray new];
        thirdlist = [JsonService getRBThirdListEntity:jsonObject andBackArray:thirdlist];
        [self setRefreshViewFinish];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
