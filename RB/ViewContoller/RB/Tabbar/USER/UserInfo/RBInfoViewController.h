//
//  RBInfoViewController.h
//  RB
//
//  Created by AngusNi on 16/2/5.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInfoTableViewCell.h"
#import "RBSignView.h"
//
#import "RBInfoAgeViewController.h"
#import "RBInfoNameViewController.h"
#import "RBInfoTagsViewController.h"
#import "RBInfoSocialViewController.h"
#import "RBInfoCitysViewController.h"
#import "RBInfoWexinCountViewController.h"

@interface RBInfoViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate,RBActionSheetDelegate,RBPictureUploadDelegate>
@end
