//
//  RBInfoWexinCountViewController.m
//  RB
//
//  Created by AngusNi on 16/2/5.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBInfoWexinCountViewController.h"

@interface RBInfoWexinCountViewController (){
    InsetsTextField*wechatTF;
}
@end

@implementation RBInfoWexinCountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R2087", @"微信好友数");
    self.bgIV.hidden = NO;
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIView*editView = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 50.0)];
    editView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:editView];
    //
    wechatTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, NavHeight, ScreenWidth-CellLeft*2.0, 50.0)];
    wechatTF.font = font_15;
    wechatTF.keyboardType =  UIKeyboardTypeNumberPad;
    wechatTF.clearButtonMode = UITextFieldViewModeNever;
    wechatTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R2087", @"微信好友数")] attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    wechatTF.returnKeyType = UIReturnKeyDone;
    wechatTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [self.view addSubview:wechatTF];
    //
    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textFiledKB.delegateT = self;
    [wechatTF setInputAccessoryView:textFiledKB];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, wechatTF.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineLabel];
    //
    RBUserEntity*userEntity = [JsonService getRBUserEntity];
    wechatTF.text = userEntity.weixin_friend_count;
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    if (wechatTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R2087", @"微信好友数")]];
        return;
    }
    if (wechatTF.text.intValue>5000) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5073", @"微信好友数不能超过5000")];
        return;
    }
    [self.hudView showOverlay];
    // RB-用户信息保存
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoUpdateWithEmail:nil andName:nil andWeixin_friend_count:wechatTF.text andAge:nil andGender:nil andBirthday:nil andCountry:nil andCity:nil andDesc:nil andTags:nil andAlipay:nil];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"update_profile"]) {
        [JsonService setRBUserEntityWith:jsonObject];
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5018", @"提交成功")];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self RBNavLeftBtnAction];
                       });
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
