//
//  RBInfoSocialViewController.m
//  RB
//
//  Created by AngusNi on 16/2/6.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBInfoSocialViewController.h"


@interface RBInfoSocialViewController (){
    int pageIndex;
    int pageSize;
    UITableView*tableviewn;
    NSMutableArray*datalist;
    NSString*uid;
}
@end

@implementation RBInfoSocialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5081", @"社交账号");
    self.bgIV.hidden = NO;
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height-NavHeight)
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
    [self setRefreshHeaderView];
    //
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [self loadRefreshViewFirstData];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 115)];
    footerView.userInteractionEnabled = YES;
    [tableviewn setTableFooterView:footerView];
    //
    UILabel *thirdLabel = [[UILabel alloc]
                           initWithFrame:CGRectMake(0, 30.0, ScreenWidth, 18)];
    thirdLabel.font = font_13;
    thirdLabel.textAlignment = NSTextAlignmentCenter;
    thirdLabel.text = NSLocalizedString(@"R5039", @"添加更多社交账号提升影响力");
    thirdLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [footerView addSubview:thirdLabel];
    CGSize thirdLabelSize = [Utils getUIFontSizeFitW:thirdLabel];
    thirdLabel.frame = CGRectMake((self.view.width - thirdLabelSize.width-20.0) / 2.0,
                                  thirdLabel.top, thirdLabelSize.width+20.0, 18);
    //
    UIView *leftLineView = [[UIView alloc]
                            initWithFrame:CGRectMake(20, thirdLabel.bottom - 9.0,
                                                     (self.view.width - thirdLabelSize.width-20.0 - 40.0) /
                                                     2.0,
                                                     0.5f)];
    leftLineView.backgroundColor = thirdLabel.textColor;
    [footerView addSubview:leftLineView];
    //
    UIView *rightLineView = [[UIView alloc]
                             initWithFrame:CGRectMake(thirdLabel.right, leftLineView.top,
                                                      leftLineView.width, 0.5f)];
    rightLineView.backgroundColor = leftLineView.backgroundColor;
    [footerView addSubview:rightLineView];
    //
    UIButton *weixinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    weixinBtn.frame = CGRectMake((self.view.width - 50.0 * 3.0 - 50.0) / 3.0, thirdLabel.bottom + 15.0, 50, 50);
    [weixinBtn addTarget:self
                  action:@selector(weixinBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [weixinBtn setBackgroundImage:[UIImage imageNamed:@"icon_wechat.png"] forState:UIControlStateNormal];
    [footerView addSubview:weixinBtn];
    //
    UIButton *weiboBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    weiboBtn.frame = CGRectMake(weixinBtn.right + 50.0, weixinBtn.top, weixinBtn.width,weixinBtn.height);
    [weiboBtn addTarget:self
                 action:@selector(weiboBtnAction:)
       forControlEvents:UIControlEventTouchUpInside];
    [weiboBtn setBackgroundImage:[UIImage imageNamed:@"icon_weibo.png"] forState:UIControlStateNormal];
    [footerView addSubview:weiboBtn];
    //
    UIButton *qqBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    qqBtn.frame = CGRectMake(weiboBtn.right + 50.0, weixinBtn.top, weixinBtn.width,weixinBtn.height);
    [qqBtn addTarget:self
              action:@selector(qqBtnAction:)
    forControlEvents:UIControlEventTouchUpInside];
    [qqBtn setBackgroundImage:[UIImage imageNamed:@"icon_qq.png"] forState:UIControlStateNormal];
    [footerView addSubview:qqBtn];
    //
    footerView.height = qqBtn.bottom;
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)loadRefreshViewFirstData {
    [self.hudView show];
    pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    pageSize = 10;
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoThirdAccountList];
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageIndex = pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshViewFinish {
    [tableviewn.mj_header endRefreshing];
    [tableviewn.mj_footer endRefreshing];
    if ([datalist count] == (pageIndex+1)*pageSize) {
        if (tableviewn.mj_footer == nil){
            [self setRefreshFooterView];
        }
    } else {
        [tableviewn.mj_footer removeFromSuperview];
        tableviewn.mj_footer = nil;
    }
    [UIView performWithoutAnimation:^{
        [tableviewn reloadData];
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBInfoSocialTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBInfoSocialTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    [cell loadRBInfoSocialCellWith:datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RBThirdEntity*entity = datalist[indexPath.row];
    uid = [NSString stringWithFormat:@"%@",entity.uid];
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    [actionSheet showWithArray:@[NSLocalizedString(@"R5043", @"解除绑定"),NSLocalizedString(@"R1011", @"取消")]];
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if (buttonIndex == 0) {
        // RB-获取第三方解除绑定
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBUserInfoThirdAccountUnbind:uid];
    }
}

#pragma mark - UIButton Delegate
- (void)weiboBtnAction:(UIButton *)sender {
    [self ThirdLoginWithTag:5000];
}

- (void)weixinBtnAction:(UIButton *)sender {
    [self ThirdLoginWithTag:5001];
}

- (void)qqBtnAction:(UIButton *)sender {
    [self ThirdLoginWithTag:5002];
}

- (void)ThirdLoginWithTag:(int)tag {
    SSDKPlatformType type;
    NSString*provider=@"";
    if (tag == 5000) {
        // 微博
        provider = @"weibo";
        type = SSDKPlatformTypeSinaWeibo;
    } else if (tag == 5001) {
        // 微信
        provider = @"wechat";
        type = SSDKPlatformTypeWechat;
    } else {
        // QQ
        provider = @"qq";
        type = SSDKPlatformTypeQQ;
    }
    // 
    [ShareSDK cancelAuthorize:type];
    [ShareSDK getUserInfo:type
           onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error){
               NSLog(@"user:%@,credential:%@",user,user.credential);
               if (state == SSDKResponseStateSuccess) {
                   [self.hudView show];
                   // RB-社交账号绑定
                   Handler*handler = [Handler shareHandler];
                   handler.delegate = self;
                   if (type == SSDKPlatformTypeWechat) {
                       NSString*unionid = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"unionid"]];
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:unionid andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                   }
                   if (type == SSDKPlatformTypeSinaWeibo) {
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*followers_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"followers_count"]];
                       NSString*created_at = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"created_at"]];
                       NSString*statuses_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"statuses_count"]];
                       NSString*verified = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"verified"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"avatar_large"]];
                       NSString*refresh_token = nil;
                       NSString*str = [NSString stringWithFormat:@"%@",user.credential];
                       if([[str componentsSeparatedByString:@"\"refresh_token\" = \""] count]>1) {
                           str = [str componentsSeparatedByString:@"\"refresh_token\" = \""][1];
                           if([[str componentsSeparatedByString:@"\";"] count]>1) {
                               refresh_token = [str componentsSeparatedByString:@"\";"][0];
                           }
                       }
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:followers_count andStatuses_count:statuses_count andRegistered_at:created_at andVerified:verified andRefresh_token:refresh_token andUnionid:nil andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                   }
                   if (type == SSDKPlatformTypeQQ) {
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*city = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"city"]];
                       NSString*is_yellow_vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"is_yellow_vip"]];
                       NSString*vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"vip"]];
                       NSString*province = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"province"]];
                       NSString*gender = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"gender"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"figureurl_qq_2"]];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:nil andProvince:province andCity:city andGender:gender andIs_vip:vip andIs_yellow_vip:is_yellow_vip];
                   }
               }
               if (state == SSDKResponseStateFail) {
                   [self.hudView showErrorWithStatus:NSLocalizedString(@"R1042", @"登录失败")];
               }
               if (state == SSDKResponseStateCancel) {
                   [self.hudView dismiss];
                   //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
               }
           }];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"kols/identity_bind_v2"]) {
        [self.hudView dismiss];
        [self loadRefreshViewFirstData];
    }
    
    if ([sender isEqualToString:@"identities"]) {
        [self.hudView dismiss];
        datalist = [NSMutableArray new];
        datalist = [JsonService getRBThirdListEntity:jsonObject andBackArray:datalist];
        [self setRefreshViewFinish];
    }
    
    if ([sender isEqualToString:@"identity_unbind"]) {
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R3007", @"解除绑定成功")];
        [self loadRefreshViewFirstData];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
