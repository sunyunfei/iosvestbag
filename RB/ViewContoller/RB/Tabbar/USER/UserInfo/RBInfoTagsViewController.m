//
//  RBInfoTagsViewController.m
//  RB
//
//  Created by AngusNi on 16/2/5.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBInfoTagsViewController.h"

@interface RBInfoTagsViewController (){
    UIScrollView*scrollviewn;
    NSMutableArray*selectedArray;
    NSMutableArray*datalist;
}
@end

@implementation RBInfoTagsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5082", @"关注行业");
    self.bgIV.hidden = NO;
    //
    [self loadFooterView];
    // RB-获取用户标签
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoTags];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-tags"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-tags"];
}

#pragma mark - loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    scrollviewn = nil;
    [scrollviewn removeAllSubviews];
    scrollviewn = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    [scrollviewn setPagingEnabled:NO];
    [scrollviewn setShowsHorizontalScrollIndicator:NO];
    [scrollviewn setShowsVerticalScrollIndicator:NO];
    [self.view addSubview:scrollviewn];
    //
    float tempG = 15.0;
    float tempM = 8.0;
    float tempW = (ScreenWidth-tempG*2.0-tempM*2.0)/3.0;
    float tempH = (tempW*128.0)/220.0;
    float tempT = 110.0;
    if ([datalist count]%5==0) {
        tempT = (scrollviewn.height-(int)([datalist count]/5.0)*(tempH+15.0)*2.0)/2.0;
        [scrollviewn setContentSize:CGSizeMake(ScreenWidth, (int)([datalist count]/5.0)*(tempH+8.0))];
    } else if ([datalist count]%5<=3&&[datalist count]%5>0) {
        tempT = (scrollviewn.height-(int)([datalist count]/5.0)*(tempH+15.0)*2.0-(tempH+8.0))/2.0;
        [scrollviewn setContentSize:CGSizeMake(ScreenWidth, (int)([datalist count]/5.0)*(tempH+8.0))];
    } else {
        tempT = (scrollviewn.height-(int)([datalist count]/5.0+1)*(tempH+15.0)*2.0)/2.0;
        [scrollviewn setContentSize:CGSizeMake(ScreenWidth, (int)([datalist count]/5.0+1)*(tempH+8.0))];
    }
    if (tempT <= 0) {
        tempT = 20;
    }
    for (int i=0; i<[datalist count]; i++) {
        UIButton*tempBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        tempBtn.tag = 2000+i;
        [tempBtn addTarget:self action:@selector(tempBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        tempBtn.imageView.contentMode = UIViewContentModeScaleToFill;
        [scrollviewn addSubview:tempBtn];
        //
        UILabel*tempLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tempLabel.font = font_icon_(22);
        tempLabel.text = Icon_btn_select_h;
        tempLabel.textColor = [Utils getUIColorWithHexString:SysColorGreen];
        tempLabel.tag = 100;
        [tempBtn addSubview:tempLabel];
        //
        if (i>=0 && i<3) {
            tempBtn.frame = CGRectMake(tempG+(tempW+tempM)*i, tempT, tempW,tempH);
            if(i==1){
                tempBtn.top=tempBtn.top+10.0;
            }
        }
        if (i>=3 && i<5) {
            tempM = 25.0;
            tempG = (ScreenWidth-(tempW*2.0+tempM))/2.0;
            tempBtn.frame = CGRectMake(tempG+(tempW+tempM)*(i-3), tempT+(tempH+20.0), tempW,tempH);
        }
        if (i>=5 && i<8) {
            tempBtn.frame = CGRectMake(15.0+(tempW+8.0)*(i-5), tempT+(tempH+20.0)*2.0, tempW,tempH);
            if(i==6){
                tempBtn.top=tempBtn.top-10.0;
            }
        }
        if (i>=8 && i<10) {
            tempM = 25.0;
            tempG = (ScreenWidth-(tempW*2.0+tempM))/2.0;
            tempBtn.frame = CGRectMake(tempG+(tempW+tempM)*(i-8), tempT+(tempH+20.0)*3.0-10.0, tempW,tempH);
        }
        if (i>=10 && i<13) {
            tempBtn.frame = CGRectMake(15.0+(tempW+8.0)*(i-10), tempT+(tempH+20.0)*4.0-20.0, tempW,tempH);
            if(i==11){
                tempBtn.top=tempBtn.top+10.0;
            }
        }
        if (i>=13 && i<15) {
            tempM = 25.0;
            tempG = (ScreenWidth-(tempW*2.0+tempM))/2.0;
            tempBtn.frame = CGRectMake(tempG+(tempW+tempM)*(i-13), tempT+(tempH+20.0)*5.0-10.0, tempW,tempH);
        }
        tempLabel.frame = CGRectMake(tempBtn.width-22.0, 0, 22,22);
        //
        RBTagEntity*tagEntity = datalist[i];
        [tempBtn sd_setImageWithURL:[NSURL URLWithString:tagEntity.cover_url] forState:UIControlStateNormal completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
        //
        tempLabel.hidden = YES;
        //
        if ([selectedArray containsObject:tagEntity.name]) {
            [self shakeStart:tempBtn];
        }
    }
}


#pragma mark - UIButton Delegate
- (void)footerBtnAction:(UIButton *)sender {
    if ([selectedArray count]<2) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5083", @"最少选择2个")];
        return;
    }
    if ([selectedArray count]>5) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5084", @"最多选择5个")];
        return;
    }
    // RB-用户信息保存
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoUpdateWithEmail:nil andName:nil andWeixin_friend_count:nil andAge:nil andGender:nil andBirthday:nil andCountry:nil andCity:nil andDesc:nil andTags:selectedArray andAlipay:nil];
}

- (void)tempBtnAction:(UIButton *)sender {
    RBTagEntity*temp = datalist[sender.tag-2000];
    if ([selectedArray containsObject:temp.name]) {
        [selectedArray removeObject:temp.name];
        [self shakeEnd:sender];
    } else {
        [self shakeStart:sender];
        [selectedArray addObject:temp.name];
    }
}

-(void)shakeStart:(UIButton*)sender {
    CGAffineTransform fromTransform = sender.transform;
    CGAffineTransform toTransform = CGAffineTransformScale(sender.transform, 1.2, 1.2);
    sender.transform = toTransform;
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8f initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        sender.transform = fromTransform;
        UIImageView*tempIV = (UIImageView*)[sender viewWithTag:100];
        tempIV.hidden = NO;
    } completion:^(BOOL finished) {
    }];
}

-(void)shakeEnd:(UIButton*)sender {
    CGAffineTransform fromTransform = sender.transform;
    CGAffineTransform toTransform = CGAffineTransformScale(sender.transform, 0.8, 0.8);
    sender.transform = toTransform;
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8f initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        sender.transform = fromTransform;
        UIImageView*tempIV = (UIImageView*)[sender viewWithTag:100];
        tempIV.hidden = YES;
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"tags"]) {
        [self.hudView dismiss];
        datalist = [NSMutableArray new];
        datalist = [JsonService getRBTagListEntity:jsonObject andBackArray:datalist];
        RBUserEntity*userEntity=[JsonService getRBUserEntity];
        selectedArray = [NSMutableArray new];
        for (RBTagEntity*temp in userEntity.tags) {
            [selectedArray addObject:temp.name];
        }
        [self loadEditView];
    }
    
    if ([sender isEqualToString:@"update_profile"]) {
        [JsonService setRBUserEntityWith:jsonObject];
        [[NSNotificationQueue defaultQueue] enqueueNotification:[NSNotification notificationWithName:NotificationRefreshUserInfo object:nil] postingStyle:NSPostNow];
        scrollviewn = nil;
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5018", @"提交成功")];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self RBNavLeftBtnAction];
                       });
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
