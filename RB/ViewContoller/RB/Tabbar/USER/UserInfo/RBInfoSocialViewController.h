//
//  RBInfoSocialViewController.h
//  RB
//
//  Created by AngusNi on 16/2/6.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInfoSocialTableViewCell.h"

@interface RBInfoSocialViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate,RBActionSheetDelegate>

@end
