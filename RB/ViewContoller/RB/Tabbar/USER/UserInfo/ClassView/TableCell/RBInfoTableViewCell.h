//
//  RBInfoTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBUserEntity.h"

@interface RBInfoTableViewCell : UITableViewCell

- (instancetype)loadRBInfoCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath andUserEntity:(RBUserEntity*)userEntity andThirdList:(NSMutableArray*)thirdlist;
@end
