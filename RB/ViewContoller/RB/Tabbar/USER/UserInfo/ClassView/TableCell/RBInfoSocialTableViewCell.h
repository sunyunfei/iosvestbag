//
//  RBInfoSocialTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBInfoSocialTableViewCell : UITableViewCell

- (instancetype)loadRBInfoSocialCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
@end
