//
//  RBInfoTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBInfoTableViewCell.h"
#import "Service.h"

@implementation RBInfoTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1001;
        [self.contentView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel.font = font_11;
        cLabel.textAlignment = NSTextAlignmentRight;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        cLabel.tag = 1002;
        [self.contentView addSubview:cLabel];
        //
        UILabel*arrowLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        arrowLabel.font = font_icon_(13.0);
        arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        arrowLabel.text = Icon_btn_right;
        arrowLabel.tag = 1003;
        [self.contentView addSubview:arrowLabel];
        //
        UIImageView*sectionUserIV = [[UIImageView alloc]initWithFrame:CGRectZero];
        sectionUserIV.tag = 1004;
        [self.contentView addSubview:sectionUserIV];
        //
        UILabel*lineLabel=[[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1010;
        [self.contentView addSubview:lineLabel];

    }
    return self;
}

- (instancetype)loadRBInfoCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath andUserEntity:(RBUserEntity*)userEntity andThirdList:(NSMutableArray*)thirdlist {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1001];
    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1002];
    UILabel*arrowLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    UIImageView*sectionUserIV=(UIImageView*)[self.contentView viewWithTag:1004];
    tLabel.hidden=NO;
    cLabel.hidden=NO;
    sectionUserIV.hidden=YES;
    tLabel.text = datalist[indexPath.section][indexPath.row];
    if(indexPath.section==0 && indexPath.row==0) {
        sectionUserIV.hidden=NO;
        [sectionUserIV sd_setImageWithURL:[NSURL URLWithString:[LocalService getRBLocalDataUserLoginAvatar]] placeholderImage:PlaceHolderUserImage];
        tLabel.frame = CGRectMake(CellLeft, 0, ScreenWidth-CellLeft*2.0, 75.0);
        sectionUserIV.frame = CGRectMake(ScreenWidth-50.0-13.0-CellLeft, (tLabel.height-50.0)/2.0, 50.0, 50.0);
        sectionUserIV.layer.cornerRadius = sectionUserIV.width/2.0;
        sectionUserIV.layer.masksToBounds = YES;
        sectionUserIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorGray]CGColor];
        sectionUserIV.layer.borderWidth = 1.0f;
        arrowLabel.frame = CGRectMake(ScreenWidth-CellLeft-8.0, (tLabel.height-13.0)/2.0, 13.0, 13.0);
        lineLabel.frame = CGRectMake(0, tLabel.height-0.5f, ScreenWidth, 0.5f);
        bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
        self.frame = CGRectMake(0, 0, ScreenWidth, bgView.bottom);
        UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
        cellBgView.backgroundColor = [UIColor clearColor];
        self.backgroundView = cellBgView;
        return self;
    }
    if (indexPath.section==0) {
        if (indexPath.row==1) {
            if ([NSString stringWithFormat:@"%@",userEntity.name].length==0) {
                cLabel.text = NSLocalizedString(@"R5067", @"未填写");
            } else {
                cLabel.text = [NSString stringWithFormat:@"%@",userEntity.name];
            }
        }
        if (indexPath.row==2) {
            if ([NSString stringWithFormat:@"%@",userEntity.gender].intValue==1){
                cLabel.text = NSLocalizedString(@"R5068",  @"男");
            } else if ([NSString stringWithFormat:@"%@",userEntity.gender].intValue==2){
                cLabel.text = NSLocalizedString(@"R5069",  @"女");
            } else {
                cLabel.text = NSLocalizedString(@"R5067", @"未填写");
            }
        }
        if (indexPath.row==3) {
            // 年龄
            if ([NSString stringWithFormat:@"%@",userEntity.age].length==0){
                cLabel.text = NSLocalizedString(@"R5067", @"未填写");
            } else {
                cLabel.text = [NSString stringWithFormat:@"%@",userEntity.age];
            }
        }
        if (indexPath.row==4) {
            // 所在地区
            if ([NSString stringWithFormat:@"%@",userEntity.app_city_label].length==0){
                cLabel.text = NSLocalizedString(@"R5067", @"未填写");
            } else {
                cLabel.text = [NSString stringWithFormat:@"%@",userEntity.app_city_label];
            }
        }
        if (indexPath.row==5) {
            // 关注行业
            if ([userEntity.tags count]==0){
                cLabel.text = NSLocalizedString(@"R5067", @"未填写");
            } else {
                NSString*tags=@"";
                for(int i=0;i<[userEntity.tags count];i++){
                    RBTagEntity*tagEntity = userEntity.tags[i];
                    if (i>4) {
                        break;
                    }
                    if (i==0) {
                        tags = tagEntity.label;
                    } else {
                        tags = [NSString stringWithFormat:@"%@/%@",tags,tagEntity.label];
                    }
                }
                cLabel.text = [NSString stringWithFormat:@"%@",tags];
            }
        }
        if (indexPath.row==6) {
            // 微信好友数
            if ([NSString stringWithFormat:@"%@",userEntity.weixin_friend_count].length==0){
                cLabel.text = NSLocalizedString(@"R5067", @"未填写");
            } else {
                cLabel.text = [NSString stringWithFormat:@"%@",userEntity.weixin_friend_count];
            }
        }
    }
    if (indexPath.section==1) {
        // 手机号码
        if (indexPath.row==0) {
            if ([NSString stringWithFormat:@"%@",userEntity.mobile_number].length==0) {
                cLabel.text = NSLocalizedString(@"R5067", @"未填写");
            } else {
                cLabel.text = [NSString stringWithFormat:@"%@",userEntity.mobile_number];
            }
        }
        // 第三方账号
        if (indexPath.row==1) {
            if (thirdlist==nil || thirdlist.count==0) {
                cLabel.text = NSLocalizedString(@"R5024", @"未绑定");
            } else {
                cLabel.text = [NSString stringWithFormat:@"%d",(int)thirdlist.count];
            }
        }
    }
    tLabel.frame = CGRectMake(CellLeft, 0, ScreenWidth-CellLeft*2.0, 50.0);
    cLabel.frame = CGRectMake(110.0, 0, ScreenWidth-CellLeft-13.0-110.0, tLabel.height);
    arrowLabel.frame = CGRectMake(ScreenWidth-CellLeft-8.0, (tLabel.height-13.0)/2.0, 13.0, 13.0);
    lineLabel.frame = CGRectMake(0, tLabel.height-0.5f, ScreenWidth, 0.5f);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
