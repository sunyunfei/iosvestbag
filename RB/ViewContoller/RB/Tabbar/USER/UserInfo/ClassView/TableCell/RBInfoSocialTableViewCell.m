//
//  RBInfoSocialTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBInfoSocialTableViewCell.h"
#import "Service.h"

@implementation RBInfoSocialTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UIView*iconView = [[UIView alloc]initWithFrame:CGRectZero];
        iconView.tag = 1002;
        [self.contentView addSubview:iconView];
        //
        UIImageView*avatarIV = [[UIImageView alloc] initWithFrame:iconView.bounds];
        avatarIV.tag = 1011;
        [iconView addSubview:avatarIV];
        //
        UIImageView*tagIV = [[UIImageView alloc]initWithFrame:CGRectZero];
        tagIV.tag = 1001;
        [self.contentView addSubview:tagIV];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_13;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1003;
        [self.contentView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        cLabel.font = font_11;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        cLabel.tag = 1004;
        [self.contentView addSubview:cLabel];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1010;
        [self.contentView addSubview:lineLabel];
    }
    return self;
}

- (instancetype)loadRBInfoSocialCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UIImageView*tagIV=(UIImageView*)[self.contentView viewWithTag:1001];
    UIView*iconView=(UIView*)[self.contentView viewWithTag:1002];
    UIImageView*avatarIV=(UIImageView*)[iconView viewWithTag:1011];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*cLabel=(UILabel*)[self.contentView viewWithTag:1004];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    //
    RBThirdEntity*entity = datalist[indexPath.row];
    tLabel.text = entity.name;
    [avatarIV sd_setImageWithURL:[NSURL URLWithString:entity.avatar_url] placeholderImage:PlaceHolderUserImage];
    if ([[NSString stringWithFormat:@"%@",entity.provider]isEqualToString:@"weibo"]) {
        cLabel.text = @"weibo";
        tagIV.image=[UIImage imageNamed:@"icon_weibo.png"];
    }
    if ([[NSString stringWithFormat:@"%@",entity.provider]isEqualToString:@"wechat"]) {
        cLabel.text = @"wechat";
        tagIV.image=[UIImage imageNamed:@"icon_wechat.png"];
    }
    if ([[NSString stringWithFormat:@"%@",entity.provider]isEqualToString:@"qq"]) {
        cLabel.text = @"qq";
        tagIV.image=[UIImage imageNamed:@"icon_qq.png"];
    }
    lineLabel.hidden=NO;
    if (indexPath.row==[datalist count]-1) {
        lineLabel.hidden=YES;
    }
    //
    iconView.frame = CGRectMake(50.0, 15.0, 50.0, 50.0);
    iconView.layer.cornerRadius = iconView.width/2.0;
    iconView.clipsToBounds = NO;
    iconView.layer.shadowColor = [[UIColor blackColor]CGColor];
    iconView.layer.shadowOffset = CGSizeMake(10,10);
    iconView.layer.shadowOpacity = 0.1;
    iconView.layer.shadowRadius = 6;
    avatarIV.frame = iconView.bounds;
    avatarIV.layer.masksToBounds = YES;
    avatarIV.layer.cornerRadius =iconView.width/2.0;
    tagIV.frame = CGRectMake(iconView.left, 15.0-2.0, 20.0, 20.0);
    tLabel.frame = CGRectMake(iconView.right+15.0, 25.0, ScreenWidth, 15.0);
    cLabel.frame = CGRectMake(iconView.right+15.0, tLabel.bottom, ScreenWidth, 13.0);
    lineLabel.frame = CGRectMake(iconView.left, 80.0-1.0f, ScreenWidth-iconView.left*2.0, 1.0);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, 80.0);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}


@end
