//
//  RBInfoAgeViewController.m
//  RB
//
//  Created by AngusNi on 16/2/5.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBInfoAgeViewController.h"

@interface RBInfoAgeViewController (){
    InsetsTextField*ageTF;
    UILabel*numberLabel;
}
@end

@implementation RBInfoAgeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5070", @"年龄");
    self.bgIV.hidden = NO;
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIView*editView = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 50.0)];
    editView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:editView];
    //
    ageTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, NavHeight, ScreenWidth-CellLeft*2.0, 50.0)];
    ageTF.font = font_15;
    ageTF.keyboardType =  UIKeyboardTypeNumberPad;
    ageTF.clearButtonMode = UITextFieldViewModeNever;
    ageTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5070", @"年龄")] attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    ageTF.returnKeyType = UIReturnKeyDone;
    ageTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNotification:) name:UITextFieldTextDidChangeNotification object:nil];
    [self.view addSubview:ageTF];
    //
    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textFiledKB.delegateT = self;
    [ageTF setInputAccessoryView:textFiledKB];
    //
    numberLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, ageTF.top, ScreenWidth-CellLeft, ageTF.height)];
    numberLabel.font = font_13;
    numberLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    numberLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:numberLabel];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, numberLabel.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineLabel];
    //
    RBUserEntity*userEntity = [JsonService getRBUserEntity];
    ageTF.text = userEntity.age;
    numberLabel.text = [NSString stringWithFormat:@"%d/3",(int)ageTF.text.length];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    if (ageTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5070", @"年龄")]];
        return;
    }
    if (ageTF.text.intValue<16) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5071", @"年龄不能小于16岁")];
        return;
    }
    if (ageTF.text.intValue>100) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5072", @"年龄不能超过100岁")];
        return;
    }
    [self.hudView showOverlay];
    // RB-用户信息保存
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoUpdateWithEmail:nil andName:nil andWeixin_friend_count:nil andAge:ageTF.text andGender:nil andBirthday:nil andCountry:nil andCity:nil andDesc:nil andTags:nil andAlipay:nil];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - NSNotification Delegate
-(void)didChangeNotification:(NSNotification*)notification{
    numberLabel.text = [NSString stringWithFormat:@"%d/3",(int)ageTF.text.length];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"update_profile"]) {
        [JsonService setRBUserEntityWith:jsonObject];
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5018", @"提交成功")];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self RBNavLeftBtnAction];
                       });
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
