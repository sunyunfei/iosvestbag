//
//  RBInfoCitysViewController.m
//  RB
//
//  Created by AngusNi on 16/2/17.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBInfoCitysViewController.h"

@interface RBInfoCitysViewController (){
    UITableView*tableviewn;
    
    NSMutableDictionary*citysDic;
    NSMutableArray*keysArray;
    
    NSMutableDictionary*rowCitysDic;
    NSMutableArray*rowKeysArray;
    
    CLLocationManager*locationManager;
    NSString*cityStr;
}
@end

@implementation RBInfoCitysViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.bgIV.hidden = NO;
    //
    [self loadNavView];
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height-NavHeight)
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    tableviewn.sectionIndexTrackingBackgroundColor = [UIColor clearColor];
    tableviewn.sectionIndexBackgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
    [self setRefreshHeaderView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [self loadRefreshViewFirstData];
    [self getGpsData];
}

#pragma mark - UIButton Delegate
- (void)loadNavView {
    RBArticleSearchView*searchView = [[RBArticleSearchView alloc]initWithFrame:CGRectZero];
    searchView.delegate = self;
    [self.view addSubview:searchView];
    //
    UIScreenEdgePanGestureRecognizer *edgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(edgePanGesture:)];
    edgePanGestureRecognizer.delegate = self;
    edgePanGestureRecognizer.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:edgePanGestureRecognizer];
}

#pragma mark- UIScreenEdgePanGestureRecognizer Delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (void)edgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender {
    [self RBNavLeftBtnAction];
    [self.view removeGestureRecognizer:sender];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - RBArticleSearchView Delegate
-(void)RBArticleSearchViewSearchCancel:(RBArticleSearchView *)view {
    [self RBNavLeftBtnAction];
}

-(void)RBArticleSearchViewSearchClear:(RBArticleSearchView *)view {
    [self.view endEditing:YES];
    [self initCityData];
}

-(void)RBArticleSearchViewSearch:(RBArticleSearchView *)view andText:(NSString *)text {
    [self.view endEditing:YES];
    if (text.length==0) {
        return;
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
                   {
                       NSMutableArray*searchArray=[NSMutableArray new];
                       citysDic = [NSMutableDictionary new];
                       for (NSString*keystr in rowKeysArray) {
                           NSMutableArray*citysarray=[rowCitysDic objectForKey:keystr];
                           for (RBCityEntity*cityEntity in citysarray) {
                               if ([text isEqualToString:cityEntity.name]||[text isEqualToString:cityEntity.name_en]||([[NSString stringWithFormat:@"%@", cityEntity.name] rangeOfString:text].length > 0) ||([[NSString stringWithFormat:@"%@", cityEntity.name_en] rangeOfString:text].length > 0)) {
                                   [searchArray addObject:cityEntity];
                                   keysArray = [NSMutableArray arrayWithArray:@[@"搜"]];
                                   [citysDic setObject:searchArray forKey:@"搜"];
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       [self setRefreshViewFinish];
                                   });
                               }
                           }
                       }
                       if ([searchArray count] == 0) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [self.hudView showErrorWithStatus:NSLocalizedString(@"R2024", @"无相关搜索结果")];
                               [self initCityData];
                           });
                       }
                   });
}

#pragma mark - initData Delegate
- (void)initCityData {
    citysDic = rowCitysDic;
    keysArray = rowKeysArray;
    RBCityEntity*temp = [[RBCityEntity alloc]init];
    temp.name = cityStr;
    temp.name_en = @"";
    if (![[NSString stringWithFormat:@"%@",[keysArray objectAtIndex:0]] isEqualToString:@"当"]) {
        [keysArray insertObject:@"当" atIndex:0];
    } else {
        for (NSString*key in keysArray) {
            NSMutableArray*citys = [citysDic objectForKey:key];
            for (RBCityEntity*cityEntity in citys) {
                if ([[[NSString stringWithFormat:@"%@",cityEntity.name]componentsSeparatedByString:cityStr]count]>1) {
                    temp = cityEntity;
                }
            }
        }
    }
    if (![[NSString stringWithFormat:@"%@",temp.name] isEqualToString:cityStr] ) {
        temp.name = NSLocalizedString(@"R5074", @"不支持当前城市");
    }
    [citysDic setObject:@[temp] forKey:@"当"];
    [self setRefreshViewFinish];
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)loadRefreshViewFirstData {
    [self.hudView show];
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoCitys];
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshViewFinish {
    [tableviewn.mj_header endRefreshing];
    [UIView performWithoutAnimation:^{
        [tableviewn reloadData];
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [keysArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *key = keysArray[section];
    NSMutableArray *citySection = [citysDic objectForKey:key];
    return [citySection count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableviewn.width, 25.0)];
//    headView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, tableviewn.width - CellLeft*2.0, headView.height)];
    contentLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    contentLabel.font = font_15;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    [headView addSubview:contentLabel];
    //
    NSString *key = [keysArray objectAtIndex:section];
    if ([key rangeOfString:@"搜"].location != NSNotFound) {
        contentLabel.text = NSLocalizedString(@"R5075", @"搜索城市");
    } else if ([key rangeOfString:@"当"].location != NSNotFound) {
        contentLabel.text = NSLocalizedString(@"R5076", @"当前城市");
    } else {
        contentLabel.text = key;
    }
    return headView;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return keysArray;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    UITableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        cell.backgroundView = nil;
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        bgView.userInteractionEnabled = YES;
        bgView.tag = 1000;
        [cell.contentView addSubview:bgView];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_13;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1001;
        [cell.contentView addSubview:tLabel];
        //
        UILabel*arrowLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        arrowLabel.font = font_icon_(13.0);
        arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        arrowLabel.text = Icon_btn_right;
        arrowLabel.tag = 1003;
        [cell.contentView addSubview:arrowLabel];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1010;
        [cell.contentView addSubview:lineLabel];
    }
    UIView*bgView=(UIView*)[cell.contentView viewWithTag:1000];
    UILabel*tLabel=(UILabel*)[cell.contentView viewWithTag:1001];
    UILabel*arrowLabel=(UILabel*)[cell.contentView viewWithTag:1003];
    UILabel*lineLabel=(UILabel*)[cell.contentView viewWithTag:1010];
    //
    NSString *key = keysArray[indexPath.section];
    NSMutableArray*citys = [citysDic objectForKey:key];
    RBCityEntity*cityEntity = citys[indexPath.row];
    tLabel.text = cityEntity.name;
    //
    tLabel.frame=CGRectMake(CellLeft, 0, ScreenWidth-CellLeft*2.0, 50.0);
    arrowLabel.frame=CGRectMake(ScreenWidth-CellLeft-13.0, (tLabel.height-13.0)/2.0, 13.0, 13.0);
    lineLabel.frame=CGRectMake(0, tLabel.height-0.5f, ScreenWidth, 0.5f);
    bgView.frame=CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    cell.frame=CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [keysArray objectAtIndex:indexPath.section];
    RBCityEntity *cityEntity = [[citysDic objectForKey:key] objectAtIndex:indexPath.row];
    if (cityEntity.name_en.length!=0) {
        // RB-用户信息保存
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBUserInfoUpdateWithEmail:nil andName:nil andWeixin_friend_count:nil andAge:nil andGender:nil andBirthday:nil andCountry:nil andCity:cityEntity.name_en andDesc:nil andTags:nil andAlipay:nil];
    }
}

#pragma mark - CLLocationManager Delegate
-(void)getGpsData {
    if ([CLLocationManager locationServicesEnabled]) {
        locationManager = [[CLLocationManager alloc] init];
        [locationManager setDelegate:self];
        [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        locationManager.distanceFilter = 100.0f;
        [locationManager startUpdatingLocation];
        if ([[[UIDevice currentDevice] systemVersion]floatValue]>=8.0) {
            [locationManager requestWhenInUseAuthorization];
        }
    }
    cityStr = NSLocalizedString(@"R5077", @"定位中...");
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    // 中国火星坐标换算
    CLLocationCoordinate2D loc = [locationManager.location coordinate];
    NSString*urlstring= [NSString stringWithFormat:@"http://api.map.baidu.com/ag/coord/convert?from=0&to=2&x=%f&y=%f",loc.longitude,loc.latitude];
    NSData*listData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]];
    NSString*x=[[listData JSONValue]objectForKey:@"x"];
    NSString*y=[[listData JSONValue]objectForKey:@"y"];
    x=[[NSString alloc] initWithData:[GGTMBase64 decodeString:x] encoding:NSUTF8StringEncoding];
    y=[[NSString alloc] initWithData:[GGTMBase64 decodeString:y] encoding:NSUTF8StringEncoding];
    CLLocation*chinaLocation =[[CLLocation alloc]initWithLatitude:[y doubleValue] longitude:[x doubleValue]];
    CLGeocoder*geocoder=[[CLGeocoder alloc]init];
    [geocoder reverseGeocodeLocation:chinaLocation completionHandler:^(NSArray*placemarks,NSError*error) {
        [locationManager stopUpdatingLocation];
        if(placemarks && placemarks.count>0) {
            [self.hudView dismiss];
            CLPlacemark*placemark=[placemarks objectAtIndex:0];
            cityStr = [placemark.addressDictionary objectForKey:@"City"];
        } else {
            cityStr = NSLocalizedString(@"R5078", @"定位失败");
        }
        [self initCityData];
    }];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [locationManager stopUpdatingLocation];
    cityStr = NSLocalizedString(@"R5078", @"定位失败");
    [self initCityData];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"cities"]) {
        [self.hudView dismiss];
        NSMutableArray*datalist = [NSMutableArray new];
        datalist = [JsonService getRBCityListEntity:jsonObject andBackArray:datalist];
        rowCitysDic = [NSMutableDictionary new];
        rowKeysArray = [NSMutableArray arrayWithArray:@[@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z"]];
        for (NSString*keystr in rowKeysArray) {
            NSMutableArray*citysarray=[NSMutableArray new];
            for (RBCityEntity*cityEntity in datalist) {
                if ([[[NSString stringWithFormat:@"%@",cityEntity.name_en]substringToIndex:1].uppercaseString isEqualToString:keystr]) {
                    [citysarray addObject:cityEntity];
                }
            }
            [rowCitysDic setObject:citysarray forKey:keystr];
        }
        [self initCityData];
    }
    
    if ([sender isEqualToString:@"update_profile"]) {
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5018", @"提交成功")];
        [JsonService setRBUserEntityWith:jsonObject];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self RBNavLeftBtnAction];
                       });
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
