//
//  RBInfoAgeViewController.h
//  RB
//
//  Created by AngusNi on 16/2/5.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBInfoAgeViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>

@end
