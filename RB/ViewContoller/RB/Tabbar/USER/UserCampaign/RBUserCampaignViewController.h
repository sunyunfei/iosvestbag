//
//  RBUserCampaignViewController.h
//  RB
//
//  Created by AngusNi on 4/5/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
//
#import "RBUserCampaignView.h"
#import "RBRankingSwitchView.h"
#import "RBCampaignDetailViewController.h"
#import "RBCampaignRecruitViewController.h"
#import "RBCampaignInviteViewController.h"

@interface RBUserCampaignViewController : RBBaseViewController
<RBBaseVCDelegate,RBUserCampaignViewDelegate,RBRankingSwitchViewDelegate,UIScrollViewDelegate,RBNewAlertViewDelegate>
@end
