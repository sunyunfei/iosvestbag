//
//  RBUserCampaignViewController.m
//  RB
//
//  Created by AngusNi on 4/5/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserCampaignViewController.h"
#import "RBCampaignRecruitAutoViewController.h"
@interface RBUserCampaignViewController (){
    UIScrollView*scrollviewn;
    NSArray*statusArray;
    RBRankingSwitchView*switchView;
    RBMyActityEntity * activityEntity;
}
@end

@implementation RBUserCampaignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5120",@"我的活动");
    //
    switchView = [[RBRankingSwitchView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 38.0)];
    switchView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [switchView RBRankingSwitchViewShow:@[NSLocalizedString(@"R2040",@"审核中"),NSLocalizedString(@"R2039",@"审核通过"),NSLocalizedString(@"R2041", @"审核拒绝")] andSelected:0];
    switchView.delegate = self;
    [self.view addSubview:switchView];
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, switchView.bottom, ScreenWidth, ScreenHeight-switchView.bottom)];
    scrollviewn.bounces = YES;
    scrollviewn.pagingEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.delegate = self;
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width*4.0, scrollviewn.height)];
    [self.view addSubview:scrollviewn];
    //
    statusArray = @[@"pending",@"passed",@"rejected"];
    for (int i=0; i<3; i++) {
        RBUserCampaignView*campaignView = [[RBUserCampaignView alloc]initWithFrame:CGRectMake(scrollviewn.width*i, 0, scrollviewn.width, scrollviewn.height)];
        campaignView.delegate = self;
        campaignView.tag = 10000+i;
        [scrollviewn addSubview:campaignView];
        if(i==0) {
            [self.hudView show];
            [campaignView RBUserCampaignViewLoadRefreshViewFirstData];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-task"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-task"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- UIScrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView==scrollviewn){
        CGPoint offset = scrollView.contentOffset;
        int currentPage = offset.x / (self.view.bounds.size.width);
        [switchView RBRankingSwitchViewShow:@[NSLocalizedString(@"R2004",@"进行中"),NSLocalizedString(@"R5121",@"待上传"),NSLocalizedString(@"R2040", @"审核中"),NSLocalizedString(@"R2006",@"已完成")] andSelected:currentPage];
        [self RBRankingSwitchViewSelected:switchView andIndex:currentPage];
    }
}

#pragma mark- RBCampaignSwitchView Delegate
- (void)RBRankingSwitchViewSelected:(RBRankingSwitchView *)view andIndex:(int)index {
    RBUserCampaignView*campaignView = (RBUserCampaignView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView show];
    [campaignView RBUserCampaignViewLoadRefreshViewFirstData];
    [scrollviewn scrollRectToVisible:CGRectMake(scrollviewn.width*index, 0, scrollviewn.width, scrollviewn.height) animated:YES];
}

#pragma mark- RBUserCampaignView Delegate
-(void)RBUserCampaignViewLoadRefreshViewData:(RBUserCampaignView *)view {
    self.defaultView.hidden = YES;
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityWithStatus:statusArray[switchView.selectedTag] andPage:[NSString stringWithFormat:@"%d",view.pageIndex+1]];
}

-(void)RBUserCampaignViewSelected:(RBUserCampaignView *)view andIndex:(int)index andTag:(NSString *)btnAction{
    RBMyActityEntity*entity = view.datalist[index];
    activityEntity = entity;
    NSString*per_budget_type = [NSString stringWithFormat:@"%@",entity.per_budget_type];
    NSString * per_action_type = [NSString stringWithFormat:@"%@",entity.per_action_type];
    if ([per_budget_type isEqualToString:@"recruit"]) {
        if([per_action_type isEqualToString:@"weibo"]||[per_action_type isEqualToString:@"wechat"]||[per_action_type isEqualToString:@"qq"]) {
            RBCampaignRecruitAutoViewController*toview = [[RBCampaignRecruitAutoViewController alloc] init];
            toview.campaignId = entity.campaign_id;
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
            return;
        } else {
            RBCampaignRecruitViewController*toview = [[RBCampaignRecruitViewController alloc] init];
            toview.campaignId = entity.campaign_id;
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
            return;
        }
    }
    if ([per_budget_type isEqualToString:@"invite"]) {
        RBCampaignInviteViewController*toview = [[RBCampaignInviteViewController alloc] init];
        toview.campaignId = entity.campaign_id;
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
        return;
    }
    //   if ([entity.status isEqualToString:@"rejected"]) {
    //        RBNewAlert * alertView = [[RBNewAlert alloc]init];
    //        alertView.tag = 13000;
    //        alertView.delegate = self;
    //        if (entity.reject_reason.length <= 0 && entity.screenshot.length>0) {
    //            [alertView showWithArray:@[[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2050", @"图片审核不通过:\n"),@"图片上传不规范"],NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R2042", @"查看截图")] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
    //        }else if (entity.screenshot.length <= 0 && entity.reject_reason.length <= 0){
    //            [alertView showWithArray:@[[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2050", @"图片审核不通过:\n"),@"图片上传不规范"],NSLocalizedString(@"R2052", @"截图参考")] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
    //        }else if(entity.reject_reason.length >0 && entity.screenshot.length > 0){
    //            [alertView showWithArray:@[[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2050", @"图片审核不通过:\n"),entity.reject_reason],NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R2042", @"查看截图")] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
    //        }else if (entity.reject_reason.length > 0 && entity.screenshot.length <= 0){
    //            [alertView showWithArray:@[[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2050", @"图片审核不通过:\n"),entity.reject_reason],NSLocalizedString(@"R2052", @"截图参考")] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
    //        }
    //        [alertView showWithArray:@[[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2050", @"图片审核不通过:s\n"),activityEntity.reject_reason],NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R2042", @"查看截图")] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
    //        return;
    //    }
    //        }
    RBCampaignDetailViewController*toview = [[RBCampaignDetailViewController alloc] init];
    toview.campaignId = entity.campaign_id;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}
#pragma mark - alertDelegate
//-(void)RBNewAlert:(RBNewAlert *)alertView clickedButtonAtIndex:(int)buttonIndex{
//    if (buttonIndex==0) {
//        if ([NSString stringWithFormat:@"%@",activityEntity.cpi_example_screenshot].length!=0) {
//            TOWebViewController *toview = [[TOWebViewController alloc] init];
//            toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.cpi_example_screenshot]];
//            toview.showPageTitles = NO;
//            toview.title = NSLocalizedString(@"R2052", @"截图参考");
//            toview.hidesBottomBarWhenPushed = YES;
//            toview.navigationButtonsHidden = YES;
//            toview.campainID = activityEntity.iid;
//            [self.navigationController pushViewController:toview animated:YES];
//        } else {
//            RBPictureView*pictureView = [[RBPictureView alloc]init];
//            [pictureView showWithPic:nil];
//        }
//    }
//    if (buttonIndex==1) {
//        TOWebViewController *toview = [[TOWebViewController alloc] init];
//        toview.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",activityEntity.screenshot]];
//        toview.title = NSLocalizedString(@"R2042", @"查看截图");
//        toview.showPageTitles = NO;
//        toview.hidesBottomBarWhenPushed = YES;
//        toview.navigationButtonsHidden = YES;
//        toview.campainID = activityEntity.iid;
//        [self.navigationController pushViewController:toview animated:YES];
//    }
//}
#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"my_campaigns"]) {
        RBUserCampaignView*campaignView = (RBUserCampaignView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
        if (campaignView.pageIndex == 0) {
            campaignView.datalist = [NSMutableArray new];
        }
        campaignView.datalist = [JsonService getRBMyActivityListEntity:jsonObject andBackArray:campaignView.datalist];
        campaignView.defaultView.hidden = YES;
        if ([campaignView.datalist count] == 0) {
            campaignView.defaultView.hidden = NO;
        }
        [self.hudView dismiss];
        [campaignView RBUserCampaignViewSetRefreshViewFinish];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    RBUserCampaignView*campaignView = (RBUserCampaignView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView dismiss];
    [campaignView RBUserCampaignViewSetRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    RBUserCampaignView*campaignView = (RBUserCampaignView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView dismiss];
    [campaignView RBUserCampaignViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
