//
//  RBUserCampaignSwitchView.h
//  RB
//
//  Created by AngusNi on 3/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"

@class RBUserCampaignSwitchView;
@protocol RBUserCampaignSwitchViewDelegate <NSObject>
@optional
- (void)RBUserCampaignSwitchViewSelected:(RBUserCampaignSwitchView *)view andIndex:(int)index;
@end

@interface RBUserCampaignSwitchView : UIView
@property(nonatomic, weak) id<RBUserCampaignSwitchViewDelegate> delegate;
@property(nonatomic, strong) UILabel*tipLabel;
@property(nonatomic, strong) UILabel*lineLabel;
@property(nonatomic, strong) NSArray*dataArray;
@property(nonatomic, assign) int selectedTag;
-(void)RBUserCampaignSwitchViewShow:(NSArray*)data andSelected:(int)tag;

@end