//
//  RBUserCampaignSwitchView.m
//  RB
//
//  Created by AngusNi on 3/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserCampaignSwitchView.h"
@implementation RBUserCampaignSwitchView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        //
        for (int i=0; i<4; i++) {
            UILabel*tabLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth/4.0)*i,0, ScreenWidth/4.0, 38.0)];
            tabLabel.font = font_(14.0);
            tabLabel.tag = 3000+i;
            tabLabel.textAlignment = NSTextAlignmentCenter;
            tabLabel.userInteractionEnabled = YES;
            [self addSubview:tabLabel];
            //
            UIButton*tabBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            tabBtn.frame = tabLabel.frame;
            tabBtn.tag = 5000+i;
            tabBtn.backgroundColor = [UIColor clearColor];
            [tabBtn addTarget:self
                             action:@selector(tabBtnAction:)
                   forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:tabBtn];
        }
        //
        self.tipLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.tipLabel.backgroundColor = [Utils getUIColorWithHexString:@"edac13"];
        [self addSubview:self.tipLabel];
        //
        self.lineLabel = [[UILabel alloc]initWithFrame:CGRectMake( 0, 37.5f, ScreenWidth, 0.5f)];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self addSubview:self.lineLabel];
    }
    return self;
}

#pragma mark - RBUserCampaignSwitchView Delegate
- (void)RBUserCampaignSwitchViewShow:(NSArray *)data andSelected:(int)tag {
    self.dataArray = data;
    self.selectedTag = tag;
    UILabel*tabLabel0 = (UILabel*)[self viewWithTag:3000];
    UILabel*tabLabel1 = (UILabel*)[self viewWithTag:3001];
    UILabel*tabLabel2 = (UILabel*)[self viewWithTag:3002];
    UILabel*tabLabel3 = (UILabel*)[self viewWithTag:3003];
    tabLabel0.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.6];
    tabLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.6];
    tabLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.6];
    tabLabel3.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.6];
    tabLabel0.text = self.dataArray[0];
    tabLabel1.text = self.dataArray[1];
    tabLabel2.text = self.dataArray[2];
    tabLabel3.text = self.dataArray[3];
    UILabel*tabLabel = (UILabel*)[self viewWithTag:3000+tag];
    CGSize tabLabelSize = [Utils getUIFontSizeFitW:tabLabel0];
    self.tipLabel.frame = CGRectMake((ScreenWidth/4.0)*tag+(ScreenWidth/4.0-tabLabelSize.width-20.0)/2.0,36.0, tabLabelSize.width+20.0, 2.0);
    tabLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
}

#pragma mark - UIButton Delegate
- (void)tabBtnAction:(UIButton *)sender {
    self.selectedTag = (int)sender.tag-5000;
    [self RBUserCampaignSwitchViewShow:self.dataArray andSelected:self.selectedTag];
    if ([self.delegate
         respondsToSelector:@selector(RBUserCampaignSwitchViewSelected:andIndex:)]) {
        [self.delegate RBUserCampaignSwitchViewSelected:self andIndex:self.selectedTag];
    }
}

@end
