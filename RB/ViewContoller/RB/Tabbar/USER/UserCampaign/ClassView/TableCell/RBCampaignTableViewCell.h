//
//  RBCampaignTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface RBCampaignTableViewCell : UITableViewCell

- (void)loadRBCampaignCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
@property(nonatomic,copy)void(^rejectBlock)(void);
@end
