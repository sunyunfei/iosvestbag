//
//  RBCampaignTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBCampaignTableViewCell.h"
#import "Service.h"

@implementation RBCampaignTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        self.contentView.userInteractionEnabled = YES;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1003;
        [self.contentView addSubview:tLabel];
        //
        UILabel*typeLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        typeLabel.font = font_11;
        typeLabel.textAlignment = NSTextAlignmentRight;
        typeLabel.textColor = [Utils getUIColorWithHexString:ccColor111111];
        typeLabel.tag = 1004;
        typeLabel.userInteractionEnabled = YES;
        [self.contentView addSubview:typeLabel];
        //
        UILabel*priceLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        priceLabel.font = font_(11);
        priceLabel.textAlignment = NSTextAlignmentRight;
        priceLabel.textColor = [Utils getUIColorWithHexString:ccColor111111];
        priceLabel.tag = 1005;
        priceLabel.userInteractionEnabled = YES;
        [self.contentView addSubview:priceLabel];
        //
        UILabel*lineLabel=[[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1010;
        [self.contentView addSubview:lineLabel];
       
    }
    return self;
}
-(void)rejectedAction:(id)sender{
    self.rejectBlock();
}
- (void)loadRBCampaignCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1003];
    UILabel*typeLabel=(UILabel*)[self.contentView viewWithTag:1004];
    UILabel*priceLabel=(UILabel*)[self.contentView viewWithTag:1005];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    if ([LocalService getRBIsIncheck] == YES) {
        priceLabel.hidden = YES;
        typeLabel.hidden = YES;
    }else{
        priceLabel.hidden = NO;
        typeLabel.hidden = NO;
    }
    //
    RBMyActityEntity*entity = datalist[indexPath.row];
    tLabel.text = entity.campaign_name;
    priceLabel.text = [NSString stringWithFormat:@"￥%.2f",entity.earn_money.floatValue];
    NSString*img_status = [NSString stringWithFormat:@"%@",entity.img_status];
    NSString * status = [NSString stringWithFormat:@"%@",entity.status];
    typeLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
//    if ([status isEqualToString:@"approved"]) {
//        typeLabel.text = NSLocalizedString(@"R2019",@"即将赚");
//    }else if ([status isEqualToString:@"passed"]){
//        typeLabel.text = NSLocalizedString(@"R2018",@"已赚");
//    }else if ([status isEqualToString:@"rejected"]){
//        if (entity.reject_reason.length >0) {
//            priceLabel.text = [NSString stringWithFormat:@"拒绝原因:%@",entity.reject_reason];
//        }else{
//            if (entity.screenshot.length>0) {
//                priceLabel.text = @"该截图不符合要求";
//            }else{
//                priceLabel.text = @"没有上传截图";
//            }
//        }
//    }
     if ([img_status isEqualToString:@"passed"] && [status isEqualToString:@"settled"]) {
        typeLabel.text = NSLocalizedString(@"R2018",@"已赚");
    } else if ([status isEqualToString:@"approved"] || [status isEqualToString:@"finished"]){
        if ([entity.img_status isEqualToString:@"passed"]) {
            typeLabel.text = NSLocalizedString(@"R2515",@"正在结算");
        }else if([entity.img_status isEqualToString:@"rejected"]){
          //  typeLabel.text = NSLocalizedString(@"R2019",@"即将赚");
            if (entity.reject_reason.length >0) {
                priceLabel.text = [NSString stringWithFormat:@"拒绝原因:%@",entity.reject_reason];
            }else{
                if (entity.screenshot.length>0) {
                    priceLabel.text = @"该截图不符合要求";
                }else{
                    priceLabel.text = @"没有上传截图";
                }
            }
        }else{
            typeLabel.text = NSLocalizedString(@"R2019",@"即将赚");
        }
    }else if ([img_status isEqualToString:@"rejected"]) {
        if (entity.reject_reason.length >0) {
            priceLabel.text = [NSString stringWithFormat:@"拒绝原因:%@",entity.reject_reason];
        }else{
            if (entity.screenshot.length>0) {
                priceLabel.text = @"该截图不符合要求";
            }else{
                priceLabel.text = @"没有上传截图";
            }
        }
    }else{
        typeLabel.text = NSLocalizedString(@"R2019",@"即将赚");
    }
    //
    tLabel.frame=CGRectMake(CellLeft, 0, ScreenWidth-CellLeft*2.0-90.0, 67);
    typeLabel.frame=CGRectMake(0, 19.0, ScreenWidth-CellLeft, 10);
    priceLabel.frame=CGRectMake(0, 29+8, ScreenWidth-CellLeft, 10);
    priceLabel.textAlignment = NSTextAlignmentRight;
    priceLabel.textColor = [Utils getUIColorWithHexString:ccColor111111];
    lineLabel.frame=CGRectMake(0, tLabel.bottom-0.5, ScreenWidth, 0.5);
    if ([img_status isEqualToString:@"rejected"]) {
        tLabel.frame=CGRectMake(CellLeft, 10, ScreenWidth-CellLeft*2.0, 30);
        priceLabel.frame=CGRectMake(CellLeft, 40, ScreenWidth-CellLeft, 20);
        lineLabel.frame=CGRectMake(0, priceLabel.bottom+9.5, ScreenWidth, 0.5);
        priceLabel.textAlignment = NSTextAlignmentLeft;
        priceLabel.textColor = [Utils getUIColorWithHexString:SysColorRed];
    }
    bgView.frame=CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame=CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
//    return self;
}


@end
