//
//  RBUserCampaignView.m
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserCampaignView.h"

@interface RBUserCampaignView () {
}
@end

@implementation RBUserCampaignView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        //
        self.tableviewn = [[UITableView alloc]
                           initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)
                           style:UITableViewStylePlain];
        self.tableviewn.backgroundView = nil;
        self.tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableviewn.backgroundColor = [UIColor clearColor];
        self.tableviewn.delegate = self;
        self.tableviewn.dataSource = self;
        [self addSubview:self.tableviewn];
        [self setRefreshHeaderView];
    }
    return self;
}

- (UIView*)defaultView {
    if (!_defaultView) {
        _defaultView = [[UIView alloc]initWithFrame:CGRectMake(0, (self.height-120.0)/2.0, ScreenWidth, 120.0)];
        _defaultView.tag = 9999;
        _defaultView.hidden = YES;
        [self addSubview:_defaultView];
        //
        UIImageView*defaultIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-100.0)/2.0, 0, 100.0, 100.0)];
        defaultIV.image = [UIImage imageNamed:@"icon_task_default.png"];
        [_defaultView addSubview:defaultIV];
        //
        UILabel*defaultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, defaultIV.bottom, ScreenWidth, 20.0)];
        defaultLabel.font = font_17;
        defaultLabel.textAlignment = NSTextAlignmentCenter;
        defaultLabel.text = NSLocalizedString(@"R1028", @"当前页面无数据,试试别的页面吧");
        defaultLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [_defaultView addSubview:defaultLabel];
    }
    return _defaultView;
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)RBUserCampaignViewLoadRefreshViewFirstData {
    self.pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    self.pageSize = 10;
    if ([self.delegate
         respondsToSelector:@selector(RBUserCampaignViewLoadRefreshViewData:)]) {
        [self.delegate RBUserCampaignViewLoadRefreshViewData:self];
    }
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    self.tableviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    self.tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.pageIndex = self.pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)RBUserCampaignViewSetRefreshViewFinish {
    [self.tableviewn.mj_header endRefreshing];
    [self.tableviewn.mj_footer endRefreshing];
    if ([self.datalist count] == (self.pageIndex+1)*self.pageSize) {
        if (self.tableviewn.mj_footer == nil){
            [self setRefreshFooterView];
        }
    } else {
        [self.tableviewn.mj_footer removeFromSuperview];
        self.tableviewn.mj_footer = nil;
    }
    [UIView performWithoutAnimation:^{
        [self.tableviewn reloadData];
    }];
    if ([self.datalist count]==0) {
        self.defaultView.hidden = NO;
    } else {
        self.defaultView.hidden = YES;
    }
}


#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self.datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  //  NSString *cellIdentifier = [NSString stringWithFormat:@"Cell%d%d",(int)[indexPath section],(int)[indexPath row]];
    static NSString *cellIdentifier = @"CellIdentifier";
    RBCampaignTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBCampaignTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:cellIdentifier];
    }
//    __weak typeof(self) weakSelf = self;
//    cell.rejectBlock = ^{
//        if ([weakSelf.delegate respondsToSelector:@selector(RBUserCampaignViewSelected:andIndex:andTag:)]) {
//            [weakSelf.delegate RBUserCampaignViewSelected:self andIndex:(int)indexPath.row andTag:@"btn"];
//        }
//    };
    if (self.datalist.count > 0) {
        [cell loadRBCampaignCellWith:self.datalist andIndex:indexPath];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell =
//    [self tableView:tableView cellForRowAtIndexPath:indexPath];
//    return cell.frame.size.height;
    if (self.datalist.count > 0) {
        RBMyActityEntity*entity = self.datalist[indexPath.row];
        if ([entity.img_status isEqualToString:@"rejected"]) {
            return 70;
        }else{
            return 67;
        }
    }
    return 70;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate
         respondsToSelector:@selector(RBUserCampaignViewSelected:andIndex:andTag:)]) {
        [self.delegate RBUserCampaignViewSelected:self andIndex:(int)indexPath.row andTag:nil];
    }
}

@end
