//
//  RBUserCampaignView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBCampaignTableViewCell.h"

@class RBUserCampaignView;
@protocol RBUserCampaignViewDelegate <NSObject>
@optional
- (void)RBUserCampaignViewLoadRefreshViewData:(RBUserCampaignView *)view;
- (void)RBUserCampaignViewSelected:(RBUserCampaignView *)view andIndex:(int)index andTag:(NSString*)btnAction;

@end


@interface RBUserCampaignView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBUserCampaignViewDelegate> delegate;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) UIView *defaultView;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
- (void)RBUserCampaignViewLoadRefreshViewFirstData;
- (void)RBUserCampaignViewSetRefreshViewFinish;
@end
