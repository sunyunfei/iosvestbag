//
//  RBUserIndianaDetailViewController.m
//  RB
//
//  Created by AngusNi on 5/26/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserIndianaDetailViewController.h"


@interface RBUserIndianaDetailViewController () {
    UITableView*tableviewn;
    NSMutableArray*datalist;
    int pageIndex;
    int pageSize;
    UIView*tableHeadView;
    RBIndianaEntity*indianaEntity;
}
@end

@implementation RBUserIndianaDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5134",@"夺宝详情");
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-indiana-detail"];
    // RB-夺宝-详情
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBIndianaDetailWithCode:self.code];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-indiana-detail"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton*)sender {
    NSLog(@"kol_amount:%@",indianaEntity.kol_amount);
    RBIndianaInputView*inputView = [[RBIndianaInputView alloc]init];
    inputView.delegate = self;
    [inputView showWithMoney:indianaEntity.kol_amount];
    
}

- (void)detailBtnAction:(UIButton*)sender {
    RBUserIndianaDetailDesViewController *toview = [[RBUserIndianaDetailDesViewController alloc] init];
    toview.code = self.code;
    toview.titleStr = indianaEntity.name;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)RBIndianaInputView:(RBIndianaInputView *)view selectValue:(int)value{
    if (value>=1) {
        RBUserIndianaPayViewController *toview = [[RBUserIndianaPayViewController alloc] init];
        toview.value = [NSString stringWithFormat:@"%d",value];
        toview.indianaEntity = indianaEntity;
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
}

#pragma mark - loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R5122",@"立即夺宝") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)loadTableHeaderView {
    [tableHeadView removeAllSubviews];
    [tableHeadView removeFromSuperview];
    tableHeadView = nil;
    //
    tableHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth)];
    tableHeadView.userInteractionEnabled = YES;
    tableHeadView.backgroundColor = [UIColor whiteColor];
    //
    NSArray*bannerlist = indianaEntity.pictures;
    if ([bannerlist count]<=1) {
        self.slideView = [[AutoSlideScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth*674.0/1202.0) animationDuration:-1];
    } else{
        self.slideView = [[AutoSlideScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth*674.0/1202.0) animationDuration:5];
    }
    [tableHeadView addSubview:self.slideView];
    self.slideView.totalPagesCount = ^NSInteger(void) {
        return [bannerlist count];
    };
    self.slideView.fetchContentViewAtIndex = ^UIView *(NSInteger page) {
        UIImageView *picIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth*674.0/1202.0)];
        picIV.contentMode = UIViewContentModeScaleAspectFill;
        picIV.layer.masksToBounds = YES;
        [picIV sd_setImageWithURL:[NSURL URLWithString:bannerlist[page]] placeholderImage:PlaceHolderImage];
        return picIV;
    };
    self.slideView.TapActionBlock = ^(NSInteger pageIndex) {
    };
    [self.slideView restart];
    //
    UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, self.slideView.bottom+CellLeft, ScreenWidth-2*CellLeft, 50.0)];
    tLabel.font = font_cu_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tLabel.numberOfLines = 0;
    tLabel.textAlignment = NSTextAlignmentCenter;
    tLabel.text = indianaEntity.name;
    [tableHeadView addSubview:tLabel];
    CGSize tLabelSize = [Utils getUIFontSizeFitH:tLabel];
    tLabel.height = tLabelSize.height;
    //
    UILabel*progressBgLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    progressBgLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    progressBgLabel.frame = CGRectMake(CellLeft, tLabel.bottom+CellLeft, tLabel.width, 8.0);
    progressBgLabel.layer.borderWidth = 1.0;
    progressBgLabel.layer.borderColor = [[Utils getUIColorWithHexString:SysColorYellow]CGColor];
    [tableHeadView addSubview:progressBgLabel];
    //
    NSString*actual_number = [NSString stringWithFormat:@"%@",indianaEntity.actual_number];
    NSString*total_number = [NSString stringWithFormat:@"%@",indianaEntity.total_number];
    float percent = actual_number.floatValue/total_number.floatValue;
    //
    UILabel*progressLabel = [[UILabel alloc]initWithFrame:CGRectMake(progressBgLabel.left, progressBgLabel.top, progressBgLabel.width*percent, progressBgLabel.height)];
    progressLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [tableHeadView addSubview:progressLabel];
    //
    TTTAttributedLabel*leftLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(progressBgLabel.left, progressLabel.bottom, ScreenWidth/2.0-progressBgLabel.left, 20.0)];
    leftLabel.font = font_13;
    leftLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    leftLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5135",@"总需人次: %@"),total_number];
    [tableHeadView addSubview:leftLabel];
    //
    TTTAttributedLabel*rightLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(ScreenWidth/2.0, leftLabel.top, ScreenWidth/2.0-progressBgLabel.left, leftLabel.height)];
    rightLabel.textAlignment = NSTextAlignmentRight;
    rightLabel.font = font_13;
    rightLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    rightLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5136",@"剩余人次: %d"),total_number.intValue-actual_number.intValue];
    [tableHeadView addSubview:rightLabel];
    //
    [leftLabel setText:leftLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:total_number options:NSCaseInsensitiveSearch];
         [mStr
          addAttribute:(NSString *)kCTForegroundColorAttributeName
          value:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]
          range:range];
         return mStr;
     }];
    [rightLabel setText:rightLabel.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:[NSString stringWithFormat:@"%d",total_number.intValue-actual_number.intValue] options:NSCaseInsensitiveSearch];
         [mStr
          addAttribute:(NSString *)kCTForegroundColorAttributeName
          value:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]
          range:range];
         return mStr;
     }];
    //
    UILabel*userBgLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, rightLabel.bottom+CellLeft, ScreenWidth-2*CellLeft, 50.0)];
    userBgLabel.backgroundColor = [Utils getUIColorWithHexString:@"f1f1f1"];
    [tableHeadView addSubview:userBgLabel];
    //
    UILabel*userLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft*2.0, userBgLabel.top, ScreenWidth-4.0*CellLeft, 50.0)];
    userLabel.numberOfLines = 0;
    userLabel.font = font_11;
    userLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
    [tableHeadView addSubview:userLabel];
    //
    NSString*token_number = [NSString stringWithFormat:@"%@",indianaEntity.token_number];
    if(token_number.intValue>0) {
        NSString*temp;
        for (int i=0; i< [indianaEntity.tickets count]; i++) {
            if (i==0) {
                temp = [NSString stringWithFormat:@"%@",indianaEntity.tickets[0]];
            } else {
                temp = [NSString stringWithFormat:@"%@ %@",temp,indianaEntity.tickets[i]];
            }
        }
        userLabel.textAlignment = NSTextAlignmentLeft;
        userLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5137",@"您参与了: %@人次\n夺宝号码: %@"),token_number,temp];
    } else {
        userLabel.textAlignment = NSTextAlignmentCenter;
        userLabel.text = NSLocalizedString(@"R5138",@"你没有参加本次夺宝！");
    }
    [Utils getUILabel:userLabel withLineSpacing:4.0];
    CGSize userLabelSize = [Utils getUIFontSizeFitH:userLabel withLineSpacing:4.0];
    userLabel.height = userLabelSize.height+CellLeft*2.0;
    userBgLabel.height = userLabel.height;
    //
    UILabel*userGapLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, userBgLabel.bottom+CellLeft, ScreenWidth, CellBottom)];
    userGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [tableHeadView addSubview:userGapLabel];
    //
    float tempH = userGapLabel.bottom;
    NSString*winner_token_number = [NSString stringWithFormat:@"%@",indianaEntity.winner_token_number];
    if(winner_token_number.intValue>0) {
        UIImageView*userWinIV = [[UIImageView alloc] initWithFrame:CGRectMake(CellLeft+(78.0-50.0)/2.0, userGapLabel.bottom+CellLeft, 50.0, 50.0)];
        userWinIV.layer.cornerRadius = userWinIV.width/2.0;
        userWinIV.layer.masksToBounds = YES;
        [userWinIV sd_setImageWithURL:[NSURL URLWithString:indianaEntity.winner_avatar_url] placeholderImage:PlaceHolderUserImage];
        [tableHeadView addSubview:userWinIV];
        //
        UIImageView*userWinTagIV = [[UIImageView alloc] initWithFrame:CGRectMake(CellLeft, userWinIV.bottom-6.0, 78.0, 22.0)];
        userWinTagIV.image = [UIImage imageNamed:@"pic_user_winner.png"];
        [tableHeadView addSubview:userWinTagIV];
        //
        UILabel*userWinLabel = [[UILabel alloc]initWithFrame:CGRectMake(userWinTagIV.right+CellLeft, userWinIV.top, ScreenWidth-userWinTagIV.right-2*CellLeft, 66.0)];
        userWinLabel.numberOfLines = 0;
        userWinLabel.font = font_11;
        userWinLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        userWinLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5139",@"获 奖 者：%@\n本期参与: %@人次\n期       号: %@\n揭晓时间: %@"),indianaEntity.winner_name,indianaEntity.winner_token_number,indianaEntity.code,indianaEntity.draw_at];
        [tableHeadView addSubview:userWinLabel];
        [Utils getUILabel:userWinLabel withLineSpacing:4.0];
        //
        UILabel*userWinBgLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, userWinTagIV.bottom+CellLeft, ScreenWidth, 45.0)];
        userWinBgLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorRed];
        [tableHeadView addSubview:userWinBgLabel];
        //
        UILabel*userWinNumberLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, userWinBgLabel.top, ScreenWidth-2*CellLeft, userWinBgLabel.height)];
        userWinNumberLabel.font = font_15;
        userWinNumberLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        userWinNumberLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5140",@"幸运号码: %@"),indianaEntity.lucky_number];
        [tableHeadView addSubview:userWinNumberLabel];
        //
        UIButton*userWinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        userWinBtn.titleLabel.font = font_13;
        [userWinBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
        [userWinBtn setTitle:NSLocalizedString(@"R5141",@"计算详情") forState:UIControlStateNormal];
        userWinBtn.frame = CGRectMake(ScreenWidth-98.0-CellLeft, userWinNumberLabel.top+7.5, 98.0, 30.0);
        userWinBtn.layer.borderWidth = 0.5;
        userWinBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorWhite andAlpha:0.8]CGColor];
        [userWinBtn addTarget:self action:@selector(userWinBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [tableHeadView addSubview:userWinBtn];
        //
        UILabel*userWinGapLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, userWinBgLabel.bottom, ScreenWidth, CellBottom)];
        userWinGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        [tableHeadView addSubview:userWinGapLabel];
        //
        tempH = userWinGapLabel.bottom;
    }
    //
    UIButton*detailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    detailBtn.frame = CGRectMake(0, tempH, ScreenWidth, 50.0);
    [detailBtn addTarget:self action:@selector(detailBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [tableHeadView addSubview:detailBtn];
    //
    UILabel*detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 0, detailBtn.width-2.0*CellLeft, detailBtn.height)];
    detailLabel.font = font_15;
    detailLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    detailLabel.text = NSLocalizedString(@"R5142",@"商品详情");
    [detailBtn addSubview:detailLabel];
    //
    UILabel*detailArrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(detailBtn.width-CellLeft-13.0, (detailBtn.height-13.0)/2.0, 13.0, 13.0)];
    detailArrowLabel.font = font_icon_(13.0);
    detailArrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    detailArrowLabel.text = Icon_btn_right;
    [detailBtn addSubview:detailArrowLabel];
    //
    UILabel*detailBtnLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, detailBtn.top, ScreenWidth, 0.5f)];
    detailBtnLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [tableHeadView addSubview:detailBtnLineLabel];
    //
    UILabel*detailBtnLineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, detailBtn.bottom-0.5f, ScreenWidth, 0.5f)];
    detailBtnLineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [tableHeadView addSubview:detailBtnLineLabel1];
    //
    UILabel*userDetailGapLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, detailBtn.bottom, ScreenWidth, CellBottom)];
    userDetailGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [tableHeadView addSubview:userDetailGapLabel];
    //
    UILabel*cellLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, userDetailGapLabel.bottom, ScreenWidth-2.0*CellLeft, 30.0)];
    cellLabel.font = font_15;
    cellLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    cellLabel.text = NSLocalizedString(@"R5143",@"参与记录");
    [tableHeadView addSubview:cellLabel];
    //
    UILabel*cellLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, cellLabel.bottom, ScreenWidth, 0.5f)];
    cellLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [tableHeadView addSubview:cellLineLabel];
    //
    tableHeadView.height = cellLineLabel.bottom;
    [tableviewn setTableHeaderView:tableHeadView];
}

#pragma mark - UIButton Delegate
- (void)userWinBtnAction:(UIButton*)sender {
    RBUserIndianaCalculationViewController*toview = [[RBUserIndianaCalculationViewController alloc] init];
    toview.code = self.code;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - LoadRefreshView Delegate
- (void)loadRefreshViewFirstData {
    [self.hudView show];
    pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    pageSize = 10;
    // RB-夺宝-详情-购买人
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBIndianaDetailListWithCode:self.code andPage:[NSString stringWithFormat:@"%d",pageIndex+1]];
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageIndex = pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshViewFinish {
    [tableviewn.mj_header endRefreshing];
    [tableviewn.mj_footer endRefreshing];
    if ([datalist count] == (pageIndex+1)*pageSize) {
        if (tableviewn.mj_footer == nil){
            [self setRefreshFooterView];
        }
    } else {
        [tableviewn.mj_footer removeFromSuperview];
        tableviewn.mj_footer = nil;
    }
    [UIView performWithoutAnimation:^{
        [tableviewn reloadData];
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBUserIndianaDetailTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBUserIndianaDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                 reuseIdentifier:cellIdentifier];
    }
    [cell loadRBUserIndianaDetailCellWith:datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"lottery_activities/code"]) {
        indianaEntity = [JsonService getRBIndianaDetailEntity:jsonObject];
        //
        [tableviewn removeAllSubviews];
        [tableviewn removeFromSuperview];
        //
        NSString*winner_token_number = [NSString stringWithFormat:@"%@",indianaEntity.winner_token_number];
        if(winner_token_number.intValue>0) {
            tableviewn = [[UITableView alloc]
                          initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, self.view.height-NavHeight)
                          style:UITableViewStylePlain];
        } else {
            tableviewn = [[UITableView alloc]
                          initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, self.view.height-NavHeight-45.0)
                          style:UITableViewStylePlain];
            [self loadFooterView];
        }
        tableviewn.dataSource = self;
        tableviewn.delegate = self;
        tableviewn.backgroundView = nil;
        tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableviewn.backgroundColor = [UIColor clearColor];
        [self.view addSubview:tableviewn];
        [self setRefreshHeaderView];
        [self loadRefreshViewFirstData];
        [self loadTableHeaderView];
    }
    
    if ([sender isEqualToString:@"lottery_activities/code/orders"]) {
        [self.hudView dismiss];
        if (pageIndex==0) {
            datalist=[NSMutableArray new];
        }
        datalist = [JsonService getRBIndianaDetailOrderEntity:jsonObject andBackArray:datalist];
        [self setRefreshViewFinish];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
