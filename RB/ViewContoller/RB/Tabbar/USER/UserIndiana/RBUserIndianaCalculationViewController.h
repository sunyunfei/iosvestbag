//
//  RBUserIndianaCalculationViewController.h
//  RB
//
//  Created by AngusNi on 5/26/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBUserIndianaCalculationViewController : RBBaseViewController
<RBBaseVCDelegate>
@property(nonatomic, strong) NSString *code;

@end
