//
//  RBUserIndianaDetailViewController.h
//  RB
//
//  Created by AngusNi on 5/26/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBUserIndianaDetailTableViewCell.h"
#import "RBIndianaInputView.h"
//
#import "RBUserIndianaCalculationViewController.h"
#import "RBUserIndianaPayViewController.h"
#import "RBUserIndianaAddressViewController.h"
#import "RBUserIndianaDetailDesViewController.h"

@interface RBUserIndianaDetailViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate,RBIndianaInputViewDelegate>
@property(nonatomic, strong) AutoSlideScrollView *slideView;
@property(nonatomic, strong) NSString *code;

@end
