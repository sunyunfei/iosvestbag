//
//  RBUserIndianaViewController.m
//  RB
//
//  Created by AngusNi on 5/26/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserIndianaViewController.h"

@interface RBUserIndianaViewController () {
    UITableView*tableviewn;
    NSMutableArray*datalist;
    int pageIndex;
    int pageSize;
}
@end

@implementation RBUserIndianaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5132",@"一元购");
    //
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize labelSize = [NSLocalizedString(@"R5133",@"我的夺宝") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_cu_15,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
    UIButton*rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.navView.width-labelSize.width-CellLeft, 20.0, labelSize.width, 44.0);
    rightBtn.titleLabel.font = font_15;
    [rightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
    [rightBtn setTitle:NSLocalizedString(@"R5133",@"我的夺宝") forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:rightBtn];
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, self.view.height-NavHeight)
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
    [self setRefreshHeaderView];
    //
    UIImageView*bannerIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth*674.0/1202.0)];
    bannerIV.image = [UIImage imageNamed:@"pic_user_indiana.jpg"];
    [tableviewn setTableHeaderView:bannerIV];
    //
    [self loadRefreshViewFirstData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-indiana"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-indiana"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationRefreshIndianaView {
    if([self isCurrentVCVisible:self]==NO) {
        [self loadRefreshViewFirstData];
    }
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnAction:(UIButton*)sender {
    RBUserIndianaMyViewController *toview = [[RBUserIndianaMyViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)loadRefreshViewFirstData {
    [self.hudView show];
    pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    pageSize = 10;
    // RB-夺宝-列表
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBIndianaListWithPage:pageIndex+1];
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageIndex = pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshViewFinish {
    [tableviewn.mj_header endRefreshing];
    [tableviewn.mj_footer endRefreshing];
    if ([datalist count] == (pageIndex+1)*pageSize) {
        if (tableviewn.mj_footer == nil){
            [self setRefreshFooterView];
        }
    } else {
        [tableviewn.mj_footer removeFromSuperview];
        tableviewn.mj_footer = nil;
    }
    [UIView performWithoutAnimation:^{
        [tableviewn reloadData];
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBUserIndianaTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBUserIndianaTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:cellIdentifier];
    }
    [cell loadRBUserIndianaCellWith:datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RBIndianaEntity*entity = datalist[indexPath.row];
    RBUserIndianaDetailViewController*toview = [[RBUserIndianaDetailViewController alloc] init];
    toview.code = entity.code;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}


#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"lottery_activities"]) {
        [self.hudView dismiss];
        if (pageIndex==0) {
            datalist=[NSMutableArray new];
        }
        datalist = [JsonService getRBIndianaListEntity:jsonObject andBackArray:datalist];
        [self setRefreshViewFinish];
        self.defaultView.hidden = YES;
        if ([datalist count] == 0) {
            self.defaultView.hidden = NO;
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
