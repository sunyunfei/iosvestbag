//
//  RBUserIndianaDetailDesViewController.h
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBUserIndianaDetailDesViewController : RBBaseViewController
<RBBaseVCDelegate>
@property(nonatomic, strong) NSString *code;
@property(nonatomic, strong) NSString *titleStr;

@end
