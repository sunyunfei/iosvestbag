//
//  RBIndianaMyViewController.m
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserIndianaMyViewController.h"

@interface RBUserIndianaMyViewController (){
    UIScrollView*scrollviewn;
    NSArray*statusArray;
    int statusIndex;
    RBRankingSwitchView *switchView;
}
@end

@implementation RBUserIndianaMyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5133",@"我的夺宝");
    //
    NSMutableParagraphStyle *paragraphStyle  =
    [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize labelSize  =
    [NSLocalizedString(@"R5151",@"收货地址") boundingRectWithSize:CGSizeMake(1000, 20)
                                                      options:NSStringDrawingUsesLineFragmentOrigin |
     NSStringDrawingUsesFontLeading
                                                   attributes:@{
                                                                NSFontAttributeName : font_cu_15,
                                                                NSParagraphStyleAttributeName : paragraphStyle
                                                                } context:nil]
    .size;
    UIButton*rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.navView.width-labelSize.width-CellLeft, 20.0, labelSize.width, 44.0);
    rightBtn.titleLabel.font = font_15;
    [rightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
    [rightBtn setTitle:NSLocalizedString(@"R5151",@"收货地址") forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:rightBtn];
    //
    switchView = [[RBRankingSwitchView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 38.0)];
    switchView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [switchView RBRankingSwitchViewShow:@[NSLocalizedString(@"R5156",@"进行中"),NSLocalizedString(@"R5157",@"已揭晓"),NSLocalizedString(@"R5158",@"中奖记录")] andSelected:0];
    switchView.delegate = self;
    [self.view addSubview:switchView];
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, switchView.bottom, ScreenWidth, ScreenHeight-switchView.bottom)];
    scrollviewn.bounces = YES;
    scrollviewn.pagingEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.delegate = self;
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width*4.0, scrollviewn.height)];
    [self.view addSubview:scrollviewn];
    //
    statusArray = @[@"executing",@"finished",@"win"];
    for (int i=0; i<3; i++) {
        RBIndianaView*indianaView = [[RBIndianaView alloc]initWithFrame:CGRectMake(scrollviewn.width*i, 0, scrollviewn.width, scrollviewn.height)];
        indianaView.delegate = self;
        indianaView.tag = 10000+i;
        [scrollviewn addSubview:indianaView];
        if(i==0) {
            statusIndex = 0;
            [self.hudView show];
            [indianaView RBIndianaViewLoadRefreshViewFirstData];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-indiana-all"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-indiana-all"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnAction:(UIButton*)sender {
    RBUserIndianaAddressViewController *toview = [[RBUserIndianaAddressViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark- UIScrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView==scrollviewn){
        CGPoint offset = scrollView.contentOffset;
        int currentPage = offset.x / (self.view.bounds.size.width);
        [switchView RBRankingSwitchViewShow:@[NSLocalizedString(@"R5156",@"进行中"),NSLocalizedString(@"R5157",@"已揭晓"),NSLocalizedString(@"R5158",@"中奖记录")] andSelected:currentPage];
        [self RBRankingSwitchViewSelected:switchView andIndex:currentPage];
    }
}

#pragma mark- RBCampaignSwitchView Delegate
- (void)RBRankingSwitchViewSelected:(RBRankingSwitchView *)view andIndex:(int)index {
    statusIndex = index;
    RBIndianaView*indianaView = (RBIndianaView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView show];
    [indianaView RBIndianaViewLoadRefreshViewFirstData];
    [scrollviewn scrollRectToVisible:CGRectMake(scrollviewn.width*index, 0, scrollviewn.width, scrollviewn.height) animated:YES];
}

#pragma mark- RBIndianaView Delegate
-(void)RBIndianaViewLoadRefreshViewData:(RBIndianaView *)view {
    // RB-夺宝-列表-我的
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBIndianaMyWithStatus:statusArray[statusIndex] andPage:view.pageIndex+1];
}

-(void)RBIndianaViewSelected:(RBIndianaView *)view andIndex:(int)index {
    RBIndianaEntity*entity = view.datalist[index];
    RBUserIndianaDetailViewController*toview = [[RBUserIndianaDetailViewController alloc] init];
    toview.code = entity.code;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"my/lottery_activities"]) {
        RBIndianaView*indianaView = (RBIndianaView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
        if (indianaView.pageIndex == 0) {
            indianaView.datalist = [NSMutableArray new];
        }
        indianaView.datalist = [JsonService getRBIndianaDetailMyEntity:jsonObject andBackArray:indianaView.datalist];
        indianaView.defaultView.hidden = YES;
        if ([indianaView.datalist count] == 0) {
            indianaView.defaultView.hidden = NO;
        }
        [self.hudView dismiss];
        [indianaView RBIndianaViewSetRefreshViewFinish];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    RBIndianaView*indianaView = (RBIndianaView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView dismiss];
    [indianaView RBIndianaViewSetRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    RBIndianaView*indianaView = (RBIndianaView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView dismiss];
    [indianaView RBIndianaViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
