//
//  RBUserIndianaCalculationViewController.m
//  RB
//
//  Created by AngusNi on 5/26/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserIndianaCalculationViewController.h"

@interface RBUserIndianaCalculationViewController () {
    NSString*lucky_number;
    NSString*order_sum;
    NSString*lottery_number;
    NSString*lottery_issue;
}
@end

@implementation RBUserIndianaCalculationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5141",@"计算详情");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-indiana-detail-calculation"];
    // RB-夺宝-详情-计算
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBIndianaDetailCalculationWithCode:self.code];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-indiana-detail-calculation"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - LoadEditView Delegate
- (void)loadEditView {
    UIView*editView = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    editView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self.view addSubview:editView];
    //
    UIView*tapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, CellLeft*2.0+33.0)];
    tapView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editView addSubview:tapView];
    //
    UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, CellLeft, ScreenWidth-2*CellLeft, 18.0)];
    tLabel.font = font_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tLabel.text = NSLocalizedString(@"R5144",@"计算公式");
    [tapView addSubview:tLabel];
    //
    UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectMake(tLabel.left, tLabel.bottom, tLabel.width, 15.0)];
    cLabel.font = font_13;
    cLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
    cLabel.text = NSLocalizedString(@"R5145",@"[(数值A+数值B)/商品所需人次]取余数+10000001");
    [tapView addSubview:cLabel];
    //
    UIView*tapView1 = [[UIView alloc]initWithFrame:CGRectMake(0, tapView.bottom+CellBottom, ScreenWidth, CellLeft*2.0+48.0)];
    tapView1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editView addSubview:tapView1];
    //
    UILabel*tLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, CellLeft, ScreenWidth-2*CellLeft, 18.0)];
    tLabel1.font = font_15;
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tLabel1.text = NSLocalizedString(@"R5146",@"数据A");
    [tapView1 addSubview:tLabel1];
    //
    UILabel*cLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(tLabel.left, tLabel.bottom, tLabel.width, 15.0)];
    cLabel1.font = font_13;
    cLabel1.textColor = [Utils getUIColorWithHexString:SysColorGray];
    cLabel1.text = NSLocalizedString(@"R5147",@"=截止该商品最后夺宝时间最后10条全站参与纪录");
    [tapView1 addSubview:cLabel1];
    //
    UILabel*cLabel11 = [[UILabel alloc]initWithFrame:CGRectMake(tLabel.left, cLabel1.bottom, tLabel.width, 15.0)];
    cLabel11.font = font_13;
    cLabel11.textColor = [Utils getUIColorWithHexString:SysColorGray];
    cLabel11.text = [NSString stringWithFormat:@"=%@",order_sum];
    [tapView1 addSubview:cLabel11];
    //
    UIView*tapView2 = [[UIView alloc]initWithFrame:CGRectMake(0, tapView1.bottom+CellBottom, ScreenWidth, CellLeft*2.0+48.0)];
    tapView2.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editView addSubview:tapView2];
    //
    UILabel*tLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, CellLeft, ScreenWidth-2*CellLeft, 18.0)];
    tLabel2.font = font_15;
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tLabel2.text = NSLocalizedString(@"R5148",@"数据B");
    [tapView2 addSubview:tLabel2];
    //
    UILabel*cLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(tLabel.left, tLabel.bottom, tLabel.width, 15.0)];
    cLabel2.font = font_13;
    cLabel2.textColor = [Utils getUIColorWithHexString:SysColorGray];
    cLabel2.text = NSLocalizedString(@"R5149",@"=最近新一期中国福利彩票“老时时彩”的揭晓结果");
    [tapView2 addSubview:cLabel2];
    //
    UILabel*cLabel21 = [[UILabel alloc]initWithFrame:CGRectMake(tLabel.left, cLabel2.bottom, tLabel.width, 15.0)];
    cLabel21.font = font_13;
    cLabel21.textColor = [Utils getUIColorWithHexString:SysColorGray];
    cLabel21.text = [NSString stringWithFormat:NSLocalizedString(@"R5150",@"=%@(第%@期)"),lottery_number,lottery_issue];
    [tapView2 addSubview:cLabel21];
    //
    UIButton*numberBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    numberBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    numberBtn.frame = CGRectMake(CellLeft, tapView2.bottom+CellBottom, ScreenWidth-2.0*CellLeft, 40.0);
    numberBtn.titleLabel.font = font_15;
    [numberBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [numberBtn setTitle:[NSString stringWithFormat:NSLocalizedString(@"R5140",@"幸运号码: %@"),lucky_number] forState:UIControlStateNormal];
    [editView addSubview:numberBtn];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"lottery_activities/code/formula"]) {
        lucky_number = [jsonObject objectForKey:@"lucky_number"];
        order_sum = [jsonObject objectForKey:@"order_sum"];
        lottery_number = [jsonObject objectForKey:@"lottery_number"];
        lottery_issue = [jsonObject objectForKey:@"lottery_issue"];
        //
        [self loadEditView];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
