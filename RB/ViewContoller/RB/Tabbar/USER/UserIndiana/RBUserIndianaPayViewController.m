//
//  RBUserIndianaPayViewController.m
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserIndianaPayViewController.h"

@interface RBUserIndianaPayViewController (){
    InsetsTextField*nameTF;
    InsetsTextField*accountTF;
    UIView*editView;
}
@end

@implementation RBUserIndianaPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5152",@"订单支付");
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-indiana-pay"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-indiana-pay"];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, editView.bottom+50.0, ScreenWidth , 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    footerBtn.frame = CGRectMake((footerView.width-200.0)/2.0, 0, 200.0, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R5153",@"立即支付") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    footerBtn.layer.borderWidth = 1.0;
    footerBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    editView = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 0)];
    editView.userInteractionEnabled = YES;
    [self.view addSubview:editView];
    //
    UIImageView*iconIV = [[UIImageView alloc] initWithFrame:CGRectZero];
    iconIV.layer.borderWidth = 0.5f;
    iconIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorLine]CGColor];
    [editView addSubview:iconIV];
    //
    UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    tLabel.numberOfLines = 0;
    tLabel.font = font_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editView addSubview:tLabel];
    //
    UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    cLabel.font = font_11;
    cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [editView addSubview:cLabel];
    //
    TTTAttributedLabel*cLabel2 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
    cLabel2.font = font_15;
    cLabel2.textColor = [Utils getUIColorWithHexString:SysColorGray];
    [editView addSubview:cLabel2];
    //
    TTTAttributedLabel*cLabel3 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
    cLabel3.font = font_15;
    cLabel3.textAlignment = NSTextAlignmentRight;
    cLabel3.textColor = [Utils getUIColorWithHexString:SysColorGray];
    [editView addSubview:cLabel3];
    //
    UILabel*lineLabel=[[UILabel alloc]initWithFrame:CGRectZero];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editView addSubview:lineLabel];
    //
    [iconIV sd_setImageWithURL:[NSURL URLWithString:self.indianaEntity.poster_url] placeholderImage:PlaceHolderImage];
    tLabel.text = [NSString stringWithFormat:@"%@",self.indianaEntity.name];
    cLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5126",@"期号: %@"),self.indianaEntity.code];
    cLabel2.text = [NSString stringWithFormat:NSLocalizedString(@"R5154",@"本期参与: %@人次"),self.value];
    cLabel3.text = [NSString stringWithFormat:NSLocalizedString(@"R5155",@"合计消耗余额: %@元"),self.value];
    //
    [cLabel2 setText:cLabel2.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:[NSString stringWithFormat:@"%@",self.value] options:NSCaseInsensitiveSearch];
         [mStr
          addAttribute:(NSString *)kCTForegroundColorAttributeName
          value:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]
          range:range];
         return mStr;
     }];
    [cLabel3 setText:cLabel3.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:[NSString stringWithFormat:@"%@",self.value] options:NSCaseInsensitiveSearch];
         [mStr
          addAttribute:(NSString *)kCTForegroundColorAttributeName
          value:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]
          range:range];
         return mStr;
     }];
    //
    iconIV.frame = CGRectMake(CellLeft, CellLeft, 110.0, 110.0);
    tLabel.frame = CGRectMake(iconIV.right+CellLeft, iconIV.top, ScreenWidth-(iconIV.right+CellLeft*2.0), 36.0);
    CGSize tLabelSize = [Utils getUIFontSizeFitH:tLabel];
    tLabel.height = tLabelSize.height;
    cLabel.frame = CGRectMake(tLabel.left, iconIV.bottom-15.0, tLabel.width, 15.0);
    cLabel2.frame = CGRectMake(CellLeft, iconIV.bottom+CellLeft, ScreenWidth-CellLeft, 20.0);
    cLabel3.frame = CGRectMake(0, iconIV.bottom+CellLeft, ScreenWidth-CellLeft, 20.0);
    lineLabel.frame = CGRectMake(0, cLabel3.bottom+CellLeft-0.5, ScreenWidth, 0.5);
    //
    editView.height = lineLabel.bottom;
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    [self.hudView showOverlay];
    // RB-夺宝-详情-购买
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBIndianaDetailBugWithCode:self.indianaEntity.code andNum:[NSString stringWithFormat:@"%@",self.value]];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"lottery_orders"]) {
        NSArray*order = [jsonObject objectForKey:@"order"];
        NSString*order_id = [order valueForKey:@"code"];
        // RB-夺宝-详情-支付
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBIndianaDetailBugPayWithCode:order_id];
    }
    
    if ([sender isEqualToString:@"lottery_orders/code/checkout"]) {
        [[NSNotificationQueue defaultQueue] enqueueNotification:[NSNotification notificationWithName:NotificationRefreshIndianaView object:nil] postingStyle:NSPostNow];
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5018", @"提交成功")];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self RBNavLeftBtnAction];
                       });
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

