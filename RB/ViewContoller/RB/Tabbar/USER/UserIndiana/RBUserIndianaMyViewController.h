//
//  RBUserIndianaMyViewController.h
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBIndianaView.h"
#import "RBUserIndianaAddressViewController.h"
#import "RBUserIndianaDetailViewController.h"

@interface RBUserIndianaMyViewController : RBBaseViewController
<RBBaseVCDelegate,RBIndianaViewDelegate,RBRankingSwitchViewDelegate,UIScrollViewDelegate>
@property(nonatomic, strong) NSString *code;

@end
