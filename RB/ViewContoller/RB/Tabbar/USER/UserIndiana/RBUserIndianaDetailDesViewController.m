//
//  RBUserIndianaDetailDesViewController.m
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserIndianaDetailDesViewController.h"

@interface RBUserIndianaDetailDesViewController () {
    UIWebView*webviewn;
}
@end

@implementation RBUserIndianaDetailDesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = self.titleStr;
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    webviewn = [[UIWebView alloc] initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height-NavHeight)];
    webviewn.scalesPageToFit = YES;
    webviewn.contentMode = UIViewContentModeRedraw;
    webviewn.opaque = YES;
    [self.view addSubview:webviewn];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-indiana-detail-des"];
    [self.hudView show];
    // RB-夺宝-详情-图片
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBIndianaDetailInfoWithCode:self.code];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-indiana-detail-des"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"lottery_activities/code/desc"]) {
        [self.hudView dismiss];
        NSArray*pictures = [jsonObject objectForKey:@"pictures"];
        NSString*temp = @"";
        for (int i=0; i<[pictures count]; i++) {
            temp = [NSString stringWithFormat:@"%@<img src=\"%@\"></>",temp,pictures[i]];
        }
        NSString *html = [NSString stringWithFormat:@"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><meta content=\"width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0,user-scalable=no\" name=\"viewport\" id=\"viewport\" /><style>body { text-align: left; }a { color:#666666; text-decoration: none; }#content { font-size:12.0pt; line-height:1.45em; font-family:'宋体'; color:#666666; }#content p { padding:0; margin:15pt 0px; display: block; }#content span, #content span[style], #content font, #content font[style] { color:#666666 !important; font-family:'宋体' !important; font-size:12.0pt !important;}#content img, #content table { width:100%% !important;}#content table, #content table td, #content table th { border:1px solid #cccccc; border-collapse:collapse; }   </style></head><body><form><div id=\"content\" font-size:12.0pt; font-family:'宋体'; color:#000000;><center style=\" font-size:14pt; font-weight:700; \">%@</center>%@</div> </form></body></html>", @"", temp];
        //
        NSLog(@"html:%@",html);
        [webviewn loadHTMLString:html baseURL:nil];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
