//
//  RBUserIndianaAddressViewController.m
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserIndianaAddressViewController.h"

@interface RBUserIndianaAddressViewController (){
    UITextField*inputTF;
    UITextField*inputTF1;
    UITextField*inputTF2;
}
@end

@implementation RBUserIndianaAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5151",@"收货地址");
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-indiana-address"];
    // RB-夺宝-收货地址
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBIndianaMyAddress];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-indiana-address"];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIScrollView*editSV = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    editSV.bounces = YES;
    editSV.pagingEnabled = NO;
    editSV.userInteractionEnabled = YES;
    editSV.showsHorizontalScrollIndicator = NO;
    editSV.showsVerticalScrollIndicator = NO;
    [self.view addSubview:editSV];
    //
    UILabel *inputTLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, 150.0, 50.0)];
    inputTLabel.text = NSLocalizedString(@"R2084", @"姓名");
    inputTLabel.font = font_15;
    inputTLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editSV addSubview:inputTLabel];
    //
    UIView*inputLineView = [[UIView alloc]initWithFrame:CGRectMake(0, inputTLabel.bottom, ScreenWidth, 0.5)];
    inputLineView.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editSV addSubview:inputLineView];
    //
    UILabel *inputTLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(inputTLabel.left,inputTLabel.bottom, inputTLabel.width, inputTLabel.height)];
    inputTLabel1.text = NSLocalizedString(@"R2085", @"电话");
    inputTLabel1.font = inputTLabel.font;
    inputTLabel1.textColor = inputTLabel.textColor;
    [editSV addSubview:inputTLabel1];
    //
    UIView*inputLineView1 = [[UIView alloc]initWithFrame:CGRectMake(inputLineView.left, inputTLabel1.bottom, inputLineView.width, inputLineView.height)];
    inputLineView1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editSV addSubview:inputLineView1];
    //
    UILabel *inputTLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(inputTLabel.left,inputTLabel1.bottom, inputTLabel.width, inputTLabel.height)];
    inputTLabel2.text = NSLocalizedString(@"R5151",@"收货地址");
    inputTLabel2.font = inputTLabel.font;
    inputTLabel2.textColor = inputTLabel.textColor;
    [editSV addSubview:inputTLabel2];
    //
    UIView*inputLineView2 = [[UIView alloc]initWithFrame:CGRectMake(inputLineView.left, inputTLabel2.bottom, inputLineView.width, inputLineView.height)];
    inputLineView2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editSV addSubview:inputLineView2];
    //
    inputTF = [[UITextField alloc]initWithFrame:CGRectMake(0, inputTLabel.top, editSV.width-CellLeft, inputTLabel.height)];
    inputTF.delegate = self;
    inputTF.font = inputTLabel.font;
    inputTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    inputTF.textAlignment = NSTextAlignmentRight;
    inputTF.keyboardType =  UIKeyboardTypeDefault;
    inputTF.clearButtonMode = UITextFieldViewModeNever;
    inputTF.returnKeyType = UIReturnKeyDone;
    inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    [editSV addSubview:inputTF];
    //
    inputTF1 = [[UITextField alloc]initWithFrame:CGRectMake(inputTF.left, inputTLabel1.top, inputTF.width, inputTLabel.height)];
    inputTF1.delegate = self;
    inputTF1.font = inputTLabel.font;
    inputTF1.textColor = inputTF.textColor;
    inputTF1.textAlignment = inputTF.textAlignment;
    inputTF1.keyboardType =  UIKeyboardTypeNumberPad;
    inputTF1.clearButtonMode = inputTF.clearButtonMode;
    inputTF1.returnKeyType = inputTF.returnKeyType;
    inputTF1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    [editSV addSubview:inputTF1];
    //
    inputTF2 = [[UITextField alloc]initWithFrame:CGRectMake(inputTF.left, inputTLabel2.top, inputTF.width, inputTLabel.height)];
    inputTF2.delegate = self;
    inputTF2.font = inputTLabel.font;
    inputTF2.textColor = inputTF.textColor;
    inputTF2.textAlignment = inputTF.textAlignment;
    inputTF2.keyboardType =  UIKeyboardTypeDefault;
    inputTF2.clearButtonMode = inputTF.clearButtonMode;
    inputTF2.returnKeyType = inputTF.returnKeyType;
    inputTF2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    [editSV addSubview:inputTF2];
    //
    TextFiledKeyBoard *textKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textKB.delegateT = self;
    [inputTF setInputAccessoryView:textKB];
    [inputTF1 setInputAccessoryView:textKB];
    [inputTF2 setInputAccessoryView:textKB];
    if(inputTF2.bottom> editSV.height){
        [editSV setContentSize:CGSizeMake(editSV.width, inputTF2.bottom)];
    }
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    if (inputTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R2084", @"姓名")]];
        return;
    }
    if (inputTF1.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R2085", @"电话")]];
        return;
    }
    if (inputTF2.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5151",@"收货地址")]];
        return;
    }
    [self.hudView showOverlay];
    // RB-夺宝-填写
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBIndianaMyAddressEditWithName:inputTF.text andPhone:inputTF1.text andLocation:inputTF2.text];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"my/address"]) {
        NSArray*address = [jsonObject objectForKey:@"address"];
        NSString*name = [JsonService checkJson:[address valueForKey:@"name"]];
        NSString*phone = [JsonService checkJson:[address valueForKey:@"phone"]];
        NSString*location = [JsonService checkJson:[address valueForKey:@"location"]];
        inputTF.text = name;
        inputTF1.text = phone;
        inputTF2.text = location;
    }
    
    if ([sender isEqualToString:@"my/address/put"]) {
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5018", @"提交成功")];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self RBNavLeftBtnAction];
                       });
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
