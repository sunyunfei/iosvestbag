//
//  RBUserIndianaViewController.h
//  RB
//
//  Created by AngusNi on 5/26/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBUserIndianaTableViewCell.h"
#import "RBUserIndianaDetailViewController.h"
#import "RBUserIndianaMyViewController.h"


@interface RBUserIndianaViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate>


@end
