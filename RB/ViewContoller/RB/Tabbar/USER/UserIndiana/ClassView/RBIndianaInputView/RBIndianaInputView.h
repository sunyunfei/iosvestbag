//
//  RBIndianaInputView.h
//  RB
//
//  Created by AngusNi on 16/3/10.
//  Copyright © 2016年 AngusNi. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "TextFiledKeyBoard.h"

@class RBIndianaInputView;
@protocol RBIndianaInputViewDelegate <NSObject>
@optional
- (void)RBIndianaInputView:(RBIndianaInputView *)view selectValue:(int)value;
@end

@interface RBIndianaInputView : UIView
<UITextFieldDelegate>
@property (nonatomic, weak)  id<RBIndianaInputViewDelegate> delegate;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UIView *bgView;
- (void)showWithMoney:(NSString*)money;

@end
