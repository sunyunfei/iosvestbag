//
//  RBIndianaInputView.m
//  RB
//
//  Created by AngusNi on 16/3/10.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBIndianaInputView.h"
#import "Service.h"

@interface RBIndianaInputView () {
    UITextField*centerTF;
    UILabel*numLabel;
    NSString*moneyStr;

}
@end

@implementation RBIndianaInputView

- (instancetype)init {
    if (self = [super init]) {
        self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
        id<UIApplicationDelegate> delegate  =
        [[UIApplication sharedApplication] delegate];
        if ([delegate respondsToSelector:@selector(window)]) {
            self.window = [delegate performSelector:@selector(window)];
        } else {
            self.window = [[UIApplication sharedApplication] keyWindow];
        }
        //
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.backgroundColor = SysColorCoverDeep;
        //
        UIView*topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, self.height-245.0)];
        [self addSubview:topView];
        //
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
        [topView addGestureRecognizer:tapGesture];
    }
    return self;
}

- (void)showWithMoney:(NSString*)money {
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(0, self.height-245.0, ScreenWidth, 245.0)];
    self.bgView.backgroundColor = [UIColor whiteColor];
    self.bgView.userInteractionEnabled = YES;
    [self addSubview:self.bgView];
    //
    UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 35.0)];
    tLabel.text = NSLocalizedString(@"R5193",@"参与人次");
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
    tLabel.font = font_15;
    tLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:tLabel];
    //
    UIButton*leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setBackgroundColor:[Utils getUIColorWithHexString:SysColorBlue]];
    leftBtn.frame = CGRectMake(CellLeft, 35.0, 60.0, 35.0);
    [leftBtn addTarget:self
                action:@selector(leftBtnAction:)
      forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setTitle:@"-" forState:UIControlStateNormal];
    [leftBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    leftBtn.titleLabel.font = font_17;
    [self.bgView addSubview:leftBtn];
    //
    centerTF = [[UITextField alloc]initWithFrame:CGRectMake(leftBtn.right, self.bgView.top+leftBtn.top, ScreenWidth-2*leftBtn.right, leftBtn.height)];
    centerTF.delegate = self;
    centerTF.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    centerTF.font = font_17;
    centerTF.textAlignment = NSTextAlignmentCenter;
    centerTF.keyboardType = UIKeyboardTypeNumberPad;
    centerTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    centerTF.returnKeyType = UIReturnKeyDone;
    [self addSubview:centerTF];
    //
    UIButton*rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setBackgroundColor:[Utils getUIColorWithHexString:SysColorBlue]];
    rightBtn.frame = CGRectMake(centerTF.right, 35.0, 60.0, 35.0);
    [rightBtn addTarget:self
                action:@selector(rightBtnAction:)
      forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setTitle:@"+" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    rightBtn.titleLabel.font = font_17;
    [self.bgView addSubview:rightBtn];
    //
    UIButton*numBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [numBtn1 setBackgroundColor:[Utils getUIColorWithHexString:SysColorBkg]];
    numBtn1.frame = CGRectMake((ScreenWidth-4*50.0-3*20.0)/2.0, rightBtn.bottom+35.0, 50.0, 25.0);
    [numBtn1 addTarget:self
                action:@selector(numBtn1Action:)
      forControlEvents:UIControlEventTouchUpInside];
    [numBtn1 setTitle:@"5" forState:UIControlStateNormal];
    [numBtn1 setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
    numBtn1.titleLabel.font = font_17;
    [self.bgView addSubview:numBtn1];
    //
    UIButton*numBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [numBtn2 setBackgroundColor:[Utils getUIColorWithHexString:SysColorBkg]];
    numBtn2.frame = CGRectMake(numBtn1.right+20.0, rightBtn.bottom+35.0, 50.0, 25.0);
    [numBtn2 addTarget:self
                action:@selector(numBtn2Action:)
      forControlEvents:UIControlEventTouchUpInside];
    [numBtn2 setTitle:@"10" forState:UIControlStateNormal];
    [numBtn2 setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
    numBtn2.titleLabel.font = font_17;
    [self.bgView addSubview:numBtn2];
    //
    UIButton*numBtn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [numBtn3 setBackgroundColor:[Utils getUIColorWithHexString:SysColorBkg]];
    numBtn3.frame = CGRectMake(numBtn2.right+20.0, rightBtn.bottom+35.0, 50.0, 25.0);
    [numBtn3 addTarget:self
                action:@selector(numBtn3Action:)
      forControlEvents:UIControlEventTouchUpInside];
    [numBtn3 setTitle:@"20" forState:UIControlStateNormal];
    [numBtn3 setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
    numBtn3.titleLabel.font = font_17;
    [self.bgView addSubview:numBtn3];
    //
    UIButton*numBtn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    [numBtn4 setBackgroundColor:[Utils getUIColorWithHexString:SysColorBkg]];
    numBtn4.frame = CGRectMake(numBtn3.right+20.0, rightBtn.bottom+35.0, 50.0, 25.0);
    [numBtn4 addTarget:self
                action:@selector(numBtn4Action:)
      forControlEvents:UIControlEventTouchUpInside];
    [numBtn4 setTitle:@"50" forState:UIControlStateNormal];
    [numBtn4 setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
    numBtn4.titleLabel.font = font_17;
    [self.bgView addSubview:numBtn4];
    //
    numLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, numBtn4.bottom+40.0, ScreenWidth, 18.0)];
    numLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
    numLabel.font = font_15;
    numLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:numLabel];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, self.bgView.height-45.0, ScreenWidth, 45.0);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R5122",@"立即夺宝") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [self.bgView addSubview:footerBtn];
    //
    centerTF.text = @"5";
    moneyStr = money;
    [self showText];
    //
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = self.bgView.transform;
        self.bgView.transform  =
        CGAffineTransformScale(self.bgView.transform, 0.5, 0.5);
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.bgView.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self endEditing:YES];
    [self showText];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEdit];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.2 animations:^{
        self.top = -216;
    } completion:^(BOOL finished) {
    }];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.2 animations:^{
        self.top = 0;
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)tapGesture {
    [self removeFromSuperview];
}

#pragma mark - UIButton Delegate
- (void)leftBtnAction:(UIButton *)sender {
    if (![Utils getNSStringTypeCheckNumber:centerTF.text]) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"R5123",@"请填写正确的数字")];
    } else if (centerTF.text.intValue<=1) {
    } else {
        centerTF.text = [NSString stringWithFormat:@"%d",centerTF.text.intValue-1];
    }
    [self showText];
}

- (void)rightBtnAction:(UIButton *)sender {
    if (![Utils getNSStringTypeCheckNumber:centerTF.text]) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"R5123",@"请填写正确的数字")];
    } else {
        centerTF.text = [NSString stringWithFormat:@"%d",centerTF.text.intValue+1];
    }
    [self showText];
}

- (void)numBtn1Action:(UIButton *)sender {
    centerTF.text = @"5";
    [self showText];
}

- (void)numBtn2Action:(UIButton *)sender {
    centerTF.text = @"10";
    [self showText];
}

- (void)numBtn3Action:(UIButton *)sender {
    centerTF.text = @"20";
    [self showText];
}

- (void)numBtn4Action:(UIButton *)sender {
    centerTF.text = @"50";
    [self showText];
}

- (void)showText {
    numLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5124",@"共消耗余额%@元(当前余额:￥%@)"),centerTF.text,moneyStr];
}

- (void)footerBtnAction:(UIButton *)sender {
    [self endEdit];
    if (![Utils getNSStringTypeCheckNumber:centerTF.text]) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"R5123",@"请填写正确的数字")];
        return;
    }
    if ([self.delegate respondsToSelector:@selector(RBIndianaInputView:selectValue:)]) {
        [self.delegate RBIndianaInputView:self selectValue:centerTF.text.intValue];
    }
    [self removeFromSuperview];
}

@end
