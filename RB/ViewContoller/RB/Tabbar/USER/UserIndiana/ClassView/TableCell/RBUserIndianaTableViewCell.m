//
//  RBUserIndianaTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBUserIndianaTableViewCell.h"
#import "Service.h"

@implementation RBUserIndianaTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
        self.bgView.userInteractionEnabled = YES;
        [self.contentView addSubview:self.bgView];
        //
        self.iconIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:self.iconIV];
        //
        self.tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.tLabel.numberOfLines = 0;
        self.tLabel.font = font_15;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self.contentView addSubview:self.tLabel];
        //
        self.cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.cLabel.font = font_11;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [self.contentView addSubview:self.cLabel];
        //
        self.cLabel1 = [[UILabel alloc]initWithFrame:CGRectZero];
        self.cLabel1.font = font_11;
        self.cLabel1.textColor = [Utils getUIColorWithHexString:SysColorGray];
        [self.contentView addSubview:self.cLabel1];
        //
        self.progressBgLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.progressBgLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.progressBgLabel.layer.borderWidth = 1.0;
        self.progressBgLabel.layer.borderColor = [[Utils getUIColorWithHexString:SysColorYellow]CGColor];
        [self.contentView addSubview:self.progressBgLabel];
        //
        self.progressLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.progressLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
        [self.contentView addSubview:self.progressLabel];
        //
        self.cellBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.cellBtn.titleLabel.font = font_15;
        [self.cellBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        self.cellBtn.layer.borderWidth = 1.0;
        self.cellBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
        self.cellBtn.enabled = NO;
        [self.contentView addSubview:self.cellBtn];
        //
        self.lineLabel=[[UILabel alloc]initWithFrame:CGRectZero];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.contentView addSubview:self.lineLabel];
    }
    return self;
}

- (instancetype)loadRBUserIndianaCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    //
    RBIndianaEntity*entity = datalist[indexPath.row];
    [self.iconIV sd_setImageWithURL:[NSURL URLWithString:entity.poster_url] placeholderImage:PlaceHolderImage];
    self.tLabel.text = entity.name;
    NSString*actual_number = [NSString stringWithFormat:@"%@",entity.actual_number];
    NSString*total_number = [NSString stringWithFormat:@"%@",entity.total_number];
    self.cLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5135",@"总需人次: %@"),total_number];
    self.cLabel1.text = [NSString stringWithFormat:NSLocalizedString(@"R5128",@"参与进度: %.2f%%"),100.0*actual_number.floatValue/total_number.floatValue];
    [self.cellBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    self.cellBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
    if ([entity.status isEqualToString:@"finished"]) {
        [self.cellBtn setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        self.cellBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
        [self.cellBtn setTitle:NSLocalizedString(@"R5178",@"已经揭晓") forState:UIControlStateNormal];
    } else if ([entity.status isEqualToString:@"drawing"]) {
        [self.cellBtn setTitle:NSLocalizedString(@"R5179",@"正在开奖") forState:UIControlStateNormal];
    } else {
        [self.cellBtn setTitle:NSLocalizedString(@"R5122",@"立即夺宝") forState:UIControlStateNormal];
    }
    //
    self.iconIV.frame = CGRectMake(CellLeft, CellLeft, 110.0, 110.0);
    self.iconIV.layer.borderWidth = 0.5f;
    self.iconIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorLine]CGColor];
    self.iconIV.contentMode = UIViewContentModeScaleAspectFill;
    self.iconIV.layer.masksToBounds = YES;
    self.tLabel.frame = CGRectMake(self.iconIV.right+CellLeft, self.iconIV.top, ScreenWidth-(self.iconIV.right+CellLeft*2.0), 36.0);
    CGSize tLabelSize = [Utils getUIFontSizeFitH:self.tLabel];
    self.tLabel.height = tLabelSize.height;
    self.cLabel.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom+5.0, self.tLabel.width, 15.0);
    self.cLabel1.frame = CGRectMake(self.tLabel.left, self.cLabel.bottom+5.0, self.tLabel.width, 15.0);
    self.progressBgLabel.frame = CGRectMake(self.tLabel.left, self.cLabel1.bottom+5.0, self.tLabel.width, 8.0);
    self.progressLabel.frame = CGRectMake(self.progressBgLabel.left, self.progressBgLabel.top, self.progressBgLabel.width*(actual_number.floatValue/total_number.floatValue), self.progressBgLabel.height);
    self.cellBtn.frame = CGRectMake(ScreenWidth-98.0-CellLeft, self.iconIV.bottom-30.0, 98.0, 30.0);
    self.lineLabel.frame = CGRectMake(0, self.iconIV.bottom+self.iconIV.top-0.5, ScreenWidth, 0.5);
    self.bgView.frame = CGRectMake(0, 0, ScreenWidth, self.lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, self.bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
