//
//  RBUserIndianaDetailTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBUserIndianaDetailTableViewCell.h"
#import "Service.h"

@implementation RBUserIndianaDetailTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
        self.bgView.userInteractionEnabled = YES;
        [self.contentView addSubview:self.bgView];
        //
        self.iconIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:self.iconIV];
        //
        self.tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.tLabel.font = font_13;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self.contentView addSubview:self.tLabel];
        //
        self.cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.cLabel.font = font_11;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [self.contentView addSubview:self.cLabel];
        //
        self.cLabel1 = [[UILabel alloc]initWithFrame:CGRectZero];
        self.cLabel1.font = font_11;
        self.cLabel1.textAlignment = NSTextAlignmentRight;
        self.cLabel1.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [self.contentView addSubview:self.cLabel1];
        //
        self.lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.contentView addSubview:self.lineLabel];
    }
    return self;
}

- (instancetype)loadRBUserIndianaDetailCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    RBIndianaOrderEntity*entity = datalist[indexPath.row];
    self.bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.iconIV sd_setImageWithURL:[NSURL URLWithString:entity.avatar_url] placeholderImage:PlaceHolderUserImage];
    self.tLabel.text = entity.name;
    self.cLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5125",@"参与了%@人次"),entity.credits];
    //
    NSString *inputDateStr  =
    [[entity.created_at componentsSeparatedByString:@"T"] objectAtIndex:0];
    NSString *inputTimeStr  =
    [[entity.created_at componentsSeparatedByString:@"T"] objectAtIndex:1];
    inputTimeStr =
    [[inputTimeStr componentsSeparatedByString:@"+"] objectAtIndex:0];
    inputTimeStr =
    [[inputTimeStr componentsSeparatedByString:@"."] objectAtIndex:0];
    self.cLabel1.text = [NSString stringWithFormat:@"%@ %@", inputDateStr,
                         inputTimeStr];
    //
    self.iconIV.frame = CGRectMake(CellLeft, CellLeft, 35.0, 35.0);
    self.iconIV.layer.cornerRadius = self.iconIV.width/2.0;
    self.iconIV.layer.masksToBounds = YES;
    self.tLabel.frame = CGRectMake(self.iconIV.right+CellLeft, self.iconIV.top, ScreenWidth-(self.iconIV.right+CellLeft)-CellLeft, 35.0/2.0);
    self.cLabel.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom, self.tLabel.width, self.tLabel.height);
    self.cLabel1.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom, self.tLabel.width, self.tLabel.height);
    self.lineLabel.frame = CGRectMake(0, self.iconIV.bottom+self.iconIV.top-0.5, ScreenWidth, 0.5);
    self.bgView.frame = CGRectMake(0, 0, ScreenWidth, self.lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, self.bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
