//
//  RBUserIndianaMyTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBUserIndianaMyTableViewCell.h"
#import "Service.h"

@implementation RBUserIndianaMyTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
        self.bgView.userInteractionEnabled = YES;
        [self.contentView addSubview:self.bgView];
        //
        self.iconIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:self.iconIV];
        //
        self.tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.tLabel.font = font_15;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self.contentView addSubview:self.tLabel];
        //
        self.cLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.cLabel.font = font_11;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [self.contentView addSubview:self.cLabel];
        //
        self.cLabel1 = [[UILabel alloc]initWithFrame:CGRectZero];
        self.cLabel1.font = font_11;
        self.cLabel1.textColor = [Utils getUIColorWithHexString:SysColorGray];
        [self.contentView addSubview:self.cLabel1];
        //
        self.cLabel2 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        self.cLabel2.font = font_11;
        self.cLabel2.textColor = [Utils getUIColorWithHexString:SysColorGray];
        [self.contentView addSubview:self.cLabel2];
        //
        self.cLabel3 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        self.cLabel3.font = font_11;
        self.cLabel3.textAlignment = NSTextAlignmentRight;
        self.cLabel3.textColor = [Utils getUIColorWithHexString:SysColorGray];
        [self.contentView addSubview:self.cLabel3];
        //
        self.progressBgLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.progressBgLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.progressBgLabel.layer.borderWidth = 1.0;
        self.progressBgLabel.layer.borderColor = [[Utils getUIColorWithHexString:SysColorYellow]CGColor];
        [self.contentView addSubview:self.progressBgLabel];
        //
        self.progressLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.progressLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
        [self.contentView addSubview:self.progressLabel];
        //
        self.cellBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.cellBtn.titleLabel.font = font_15;
        [self.cellBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        self.cellBtn.layer.borderWidth = 1.0;
        self.cellBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
        self.cellBtn.enabled = NO;
        [self.contentView addSubview:self.cellBtn];
        //
        self.lineLabel=[[UILabel alloc]initWithFrame:CGRectZero];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.contentView addSubview:self.lineLabel];
    }
    return self;
}

- (instancetype)loadRBUserIndianaMyCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    RBIndianaEntity*entity = datalist[indexPath.row];
    [self.iconIV sd_setImageWithURL:[NSURL URLWithString:entity.poster_url] placeholderImage:PlaceHolderImage];
    self.tLabel.text = entity.name;
    if(![entity.status isEqualToString:@"finished"]){
        self.cLabel2.font = font_11;
        self.cLabel3.font = font_11;
        self.cLabel2.textColor = [Utils getUIColorWithHexString:SysColorGray];
        self.cLabel3.textColor = [Utils getUIColorWithHexString:SysColorGray];
        //
        NSString*actual_number = [NSString stringWithFormat:@"%@",entity.actual_number];
        NSString*total_number = [NSString stringWithFormat:@"%@",entity.total_number];
        self.cLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5126",@"期号: %@"),entity.code];
        self.cLabel1.text = [NSString stringWithFormat:NSLocalizedString(@"R5127",@"我已参与: %@人次"),entity.token_number];
        self.cLabel2.text = [NSString stringWithFormat:NSLocalizedString(@"R5128",@"参与进度: %.2f%%"),100.0*actual_number.floatValue/total_number.floatValue];
        [self.cellBtn setTitle:NSLocalizedString(@"R5129",@"追加") forState:UIControlStateNormal];
        //
        self.iconIV.frame = CGRectMake(CellLeft, CellLeft, 80.0, 80.0);
        self.iconIV.layer.borderWidth = 0.5f;
        self.iconIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorLine]CGColor];
        self.tLabel.frame = CGRectMake(self.iconIV.right+CellLeft, self.iconIV.top+5.0, ScreenWidth-(self.iconIV.right+CellLeft*2.0), 18.0);
        self.cLabel.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom+10.0, self.tLabel.width, 15.0);
        self.cLabel1.frame = CGRectMake(self.tLabel.left, self.iconIV.bottom-20.0, self.tLabel.width, 15.0);
        self.cLabel2.frame = CGRectMake(CellLeft, self.iconIV.bottom+CellLeft, ScreenWidth-98.0-CellLeft*4.0, 15.0);
        self.progressBgLabel.frame = CGRectMake(self.cLabel2.left, self.cLabel2.bottom+5.0, self.cLabel2.width, 8.0);
        self.progressLabel.frame = CGRectMake(self.progressBgLabel.left, self.progressBgLabel.top, self.progressBgLabel.width*(actual_number.floatValue/total_number.floatValue), self.progressBgLabel.height);
        self.cellBtn.frame = CGRectMake(ScreenWidth-98.0-CellLeft, self.cLabel2.top, 98.0, 30.0);
        self.lineLabel.frame = CGRectMake(0, self.cellBtn.bottom+CellLeft-0.5, ScreenWidth, 0.5);
    } else {
        self.cLabel2.font = font_13;
        self.cLabel3.font = font_13;
        self.cLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.cLabel3.textColor = [Utils getUIColorWithHexString:SysColorBlack];

        self.cLabel.text = [NSString stringWithFormat:NSLocalizedString(@"R5126",@"期号: %@"),entity.code];
        self.cLabel1.text = [NSString stringWithFormat:NSLocalizedString(@"R5127",@"我已参与: %@人次"),entity.token_number];
        self.cLabel2.text = [NSString stringWithFormat:NSLocalizedString(@"R5130",@"获奖者: %@"),entity.winner_name];
        self.cLabel3.text = [NSString stringWithFormat:NSLocalizedString(@"R5131",@"TA已参与: %@人次"),entity.winner_token_number];
        //
        [self.cLabel2 setText:self.cLabel2.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
         ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
             NSRange range = [[mStr string] rangeOfString:entity.winner_name options:NSCaseInsensitiveSearch];
             [mStr
              addAttribute:(NSString *)kCTForegroundColorAttributeName
              value:(id)[[Utils getUIColorWithHexString:SysColorRed] CGColor]
              range:range];
             return mStr;
         }];
        [self.cLabel3 setText:self.cLabel3.text
afterInheritingLabelAttributesAndConfiguringWithBlock:
         ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
             NSRange range = [[mStr string] rangeOfString:entity.winner_token_number options:NSCaseInsensitiveSearch];
             [mStr
              addAttribute:(NSString *)kCTForegroundColorAttributeName
              value:(id)[[Utils getUIColorWithHexString:SysColorRed] CGColor]
              range:range];
             return mStr;
         }];
        self.iconIV.frame = CGRectMake(CellLeft, CellLeft, 80.0, 80.0);
        self.iconIV.layer.borderWidth = 0.5f;
        self.iconIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorLine]CGColor];
        self.iconIV.contentMode = UIViewContentModeScaleAspectFill;
        self.iconIV.layer.masksToBounds = YES;
        self.tLabel.frame = CGRectMake(self.iconIV.right+CellLeft, self.iconIV.top+5.0, ScreenWidth-(self.iconIV.right+CellLeft*2.0), 18.0);
        self.cLabel.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom+10.0, self.tLabel.width, 15.0);
        self.cLabel1.frame = CGRectMake(self.tLabel.left, self.iconIV.bottom-20.0, self.tLabel.width, 15.0);
        self.cLabel2.frame = CGRectMake(CellLeft, self.iconIV.bottom+CellLeft, ScreenWidth-CellLeft, 20.0);
        self.cLabel3.frame = CGRectMake(0, self.iconIV.bottom+CellLeft, ScreenWidth-CellLeft, 20.0);
        self.lineLabel.frame = CGRectMake(0, self.cLabel3.bottom+CellLeft-0.5, ScreenWidth, 0.5);
    }
    self.bgView.frame = CGRectMake(0, 0, ScreenWidth, self.lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, self.bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
