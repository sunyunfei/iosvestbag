//
//  RBUserIndianaMyTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface RBUserIndianaMyTableViewCell : UITableViewCell
- (instancetype)loadRBUserIndianaMyCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
@property(nonatomic, strong) UIView*bgView;
//
@property(nonatomic, strong) UIImageView*iconIV;
//
@property(nonatomic, strong) UILabel*tLabel;
//
@property(nonatomic, strong) UILabel*cLabel;
//
@property(nonatomic, strong) UILabel*cLabel1;
//
@property(nonatomic, strong) TTTAttributedLabel*cLabel2;
//
@property(nonatomic, strong) TTTAttributedLabel*cLabel3;
//
@property(nonatomic, strong) UILabel*progressBgLabel;
//
@property(nonatomic, strong) UILabel*progressLabel;
//
@property(nonatomic, strong) UIButton*cellBtn;
//
@property(nonatomic, strong) UILabel*lineLabel;

@end
