//
//  RBIndianaView.m
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBIndianaView.h"

@interface RBIndianaView () {
}
@end

@implementation RBIndianaView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        //
        self.tableviewn = [[UITableView alloc]
                           initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)
                           style:UITableViewStylePlain];
        self.tableviewn.backgroundView = nil;
        self.tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableviewn.backgroundColor = [UIColor clearColor];
        self.tableviewn.delegate = self;
        self.tableviewn.dataSource = self;
        [self addSubview:self.tableviewn];
        [self setRefreshHeaderView];
    }
    return self;
}

- (UIView*)defaultView {
    if (!_defaultView) {
        _defaultView = [[UIView alloc]initWithFrame:CGRectMake(0, (self.height-120.0)/2.0, ScreenWidth, 120.0)];
        _defaultView.tag = 9999;
        _defaultView.hidden = YES;
        [self addSubview:_defaultView];
        //
        UIImageView*defaultIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-100.0)/2.0, 0, 100.0, 100.0)];
        defaultIV.image = [UIImage imageNamed:@"icon_task_default.png"];
        [_defaultView addSubview:defaultIV];
        //
        UILabel*defaultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, defaultIV.bottom, ScreenWidth, 20.0)];
        defaultLabel.font = font_17;
        defaultLabel.textAlignment = NSTextAlignmentCenter;
        defaultLabel.text = NSLocalizedString(@"R1028", @"当前页面无数据,试试别的页面吧");
        defaultLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [_defaultView addSubview:defaultLabel];
    }
    return _defaultView;
}


#pragma mark - LoadTableHeadFootView Delegate
- (void)RBIndianaViewLoadRefreshViewFirstData {
    self.pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    self.pageSize = 10;
    if ([self.delegate
         respondsToSelector:@selector(RBIndianaViewLoadRefreshViewData:)]) {
        [self.delegate RBIndianaViewLoadRefreshViewData:self];
    }
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    self.tableviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    self.tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.pageIndex = self.pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)RBIndianaViewSetRefreshViewFinish {
    [self.tableviewn.mj_header endRefreshing];
    [self.tableviewn.mj_footer endRefreshing];
    if ([self.datalist count] == (self.pageIndex+1)*self.pageSize) {
        if (self.tableviewn.mj_footer == nil){
            [self setRefreshFooterView];
        }
    } else {
        [self.tableviewn.mj_footer removeFromSuperview];
        self.tableviewn.mj_footer = nil;
    }
    [UIView performWithoutAnimation:^{
        [self.tableviewn reloadData];
    }];
    if ([self.datalist count]==0) {
        self.defaultView.hidden = NO;
    } else {
        self.defaultView.hidden = YES;
    }
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self.datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"Cell%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBUserIndianaMyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBUserIndianaMyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                 reuseIdentifier:cellIdentifier];
    }
    [cell loadRBUserIndianaMyCellWith:self.datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate
         respondsToSelector:@selector(RBIndianaViewSelected:andIndex:)]) {
        [self.delegate RBIndianaViewSelected:self andIndex:(int)indexPath.row];
    }
}

@end


