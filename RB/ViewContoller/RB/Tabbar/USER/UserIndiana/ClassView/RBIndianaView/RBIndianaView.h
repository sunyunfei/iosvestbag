//
//  RBIndianaView.h
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBUserIndianaMyTableViewCell.h"

@class RBIndianaView;
@protocol RBIndianaViewDelegate <NSObject>
@optional
- (void)RBIndianaViewLoadRefreshViewData:(RBIndianaView *)view;
- (void)RBIndianaViewSelected:(RBIndianaView *)view andIndex:(int)index;
@end

@interface RBIndianaView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBIndianaViewDelegate> delegate;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) UIView *defaultView;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
- (void)RBIndianaViewLoadRefreshViewFirstData;
- (void)RBIndianaViewSetRefreshViewFinish;

@end