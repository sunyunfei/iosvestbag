//
//  RBUserIndianaPayViewController.h
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBUserIndianaPayViewController : RBBaseViewController
<RBBaseVCDelegate>
@property(nonatomic, strong) RBIndianaEntity*indianaEntity;
@property(nonatomic, strong) NSString*value;
@end
