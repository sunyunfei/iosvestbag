//
//  RBUserIndianaAddressViewController.h
//  RB
//
//  Created by AngusNi on 5/27/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBUserIndianaAddressViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>

@end
