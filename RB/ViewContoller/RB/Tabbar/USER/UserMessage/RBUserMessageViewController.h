//
//  RBUserMessageViewController.h
//  RB
//
//  Created by AngusNi on 16/1/30.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

#import "RBUserMessageTableViewCell.h"
#import "RBCampaignDetailViewController.h"
#import "RBCampaignRecruitViewController.h"
#import "RBCampaignInviteViewController.h"
#import "RBCampaignRecruitAutoViewController.h"

@interface RBUserMessageViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate,RBAlertViewDelegate>
@property(nonatomic, strong) NSString *status;
@end
