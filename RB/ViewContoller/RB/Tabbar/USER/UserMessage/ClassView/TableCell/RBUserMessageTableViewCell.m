//
//  RBUserMessageTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBUserMessageTableViewCell.h"
#import "Service.h"

@implementation RBUserMessageTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.tag=1000;
        [self.contentView addSubview:bgView];
        //
        UIImageView*iconIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        iconIV.contentMode = UIViewContentModeScaleAspectFill;
        iconIV.layer.masksToBounds = YES;
        iconIV.tag=1001;
        [self.contentView addSubview:iconIV];
        //
        UILabel*timeLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        timeLabel.font = font_11;
        timeLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        timeLabel.textAlignment = NSTextAlignmentRight;
        timeLabel.tag=1002;
        [self.contentView addSubview:timeLabel];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_11;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        tLabel.tag=1003;
        [self.contentView addSubview:tLabel];
        //
        TTTAttributedLabel*cLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        cLabel.font = font_15;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        cLabel.tag=1004;
        [self.contentView addSubview:cLabel];
        //
        UILabel*lineLabel=[[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag=1005;
        [self.contentView addSubview:lineLabel];
        //
        UILabel*newLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        newLabel.backgroundColor = [Utils getUIColorWithHexString:@"ff0000"];
        newLabel.tag=1006;
        [self.contentView addSubview:newLabel];
    }
    return self;
}

- (instancetype)loadRBUserMessageCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
//    UIImageView*iconIV=(UIImageView*)[self.contentView viewWithTag:1001];
    UILabel*timeLabel=(UILabel*)[self.contentView viewWithTag:1002];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1003];
    TTTAttributedLabel*cLabel=(TTTAttributedLabel*)[self.contentView viewWithTag:1004];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1005];
    UILabel*newLabel=(UILabel*)[self.contentView viewWithTag:1006];
    //
    RBNotificationEntity*entity = datalist[indexPath.row];
    NSString*type = [NSString stringWithFormat:@"%@",entity.message_type];
    if ([type isEqualToString:@"income"]) {
        cLabel.text = [NSString stringWithFormat:@"%@",entity.name];
        tLabel.text = [NSString stringWithFormat:@"%@",entity.title];
    } else if ([type isEqualToString:@"announcement"]) {
        cLabel.text = [NSString stringWithFormat:@"%@",entity.name];
        tLabel.text = [NSString stringWithFormat:@"%@",entity.title];
    } else if ([type isEqualToString:@"campaign"]||[type isEqualToString:@"screenshot_passed"]||[type isEqualToString:@"screenshot_rejected"]||[type isEqualToString:@"remind_upload"]) {
        cLabel.text = [NSString stringWithFormat:@"%@",entity.name];
        tLabel.text = [NSString stringWithFormat:@"%@",entity.title];
    } else if ([type isEqualToString:@"common"]) {
        cLabel.text = [NSString stringWithFormat:@"%@",entity.name];
        tLabel.text = [NSString stringWithFormat:@"%@",entity.title];
    } else {
        cLabel.text = @"Robin8 其它消息";
        tLabel.text = [NSString stringWithFormat:@"%@",entity.title];
    }
    //'income','announcement','campaign','screenshot_passed','screenshot_rejected','remind_upload','common' 分别代表收入、公告、活动、审核通过、截图审核失败、提醒上传截图、内容通知 类型
    timeLabel.text = [Utils getUIDateCompare:entity.created_at];
    timeLabel.frame = CGRectMake(ScreenWidth-100-CellLeft, CellLeft, 100, 50.0);
    CGSize timeLabelSize = [Utils getUIFontSizeFitW:timeLabel];
    timeLabel.frame = CGRectMake(ScreenWidth-timeLabelSize.width-CellLeft, CellLeft, timeLabelSize.width, 50.0);
//    [iconIV sd_setImageWithURL:[NSURL URLWithString:entity.logo_url] placeholderImage:PlaceHolderImage options:SDWebImageProgressiveDownload];
    newLabel.hidden = YES;
    cLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
    if ([NSString stringWithFormat:@"%@",entity.is_read].intValue==0) {
        newLabel.hidden = NO;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    }
    newLabel.frame = CGRectMake(5.0, (50.0+CellLeft*2.0-10.0)/2.0, 10.0, 10.0);
    newLabel.layer.cornerRadius = 5.0;
    newLabel.layer.masksToBounds = YES;
    //
    tLabel.frame = CGRectMake(20.0, CellLeft+5.0, ScreenWidth-20.0-timeLabelSize.width-CellLeft, 18.0);
    cLabel.frame = CGRectMake(tLabel.left, tLabel.bottom+5.0, tLabel.width, 18.0);
    lineLabel.frame = CGRectMake(0, 50.0+CellLeft*2.0-0.5, ScreenWidth, 0.5);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
