//
//  RBUserMessageViewController.m
//  RB
//
//  Created by AngusNi on 16/1/30.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBUserMessageViewController.h"

@interface RBUserMessageViewController () {
    int pageIndex;
    int pageSize;
    UITableView*tableviewn;
    NSMutableArray*datalist;
    RBNotificationEntity*notificationEntity;
}
@end

@implementation RBUserMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    if (self.status == nil) {
        self.status = @"all";
        self.navTitle = NSLocalizedString(@"R5100", @"消息中心");
    } else {
        self.navTitle = NSLocalizedString(@"R5101", @"未读消息");
    }
    self.navRightTitle = Icon_bar_read;
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height - NavHeight)
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
    [self setRefreshHeaderView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [self loadRefreshViewFirstData];
    //
    [TalkingData trackPageBegin:@"message-list"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"message-list"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationShowMessageView {
    if([self isCurrentVCVisible:self]==YES) {
        [self loadRefreshViewFirstData];
    }
}

- (void)RBNotificationStatusBarAction {
    CGPoint off = tableviewn.contentOffset;
    off.y = 0 - tableviewn.contentInset.top;
    [tableviewn setContentOffset:off animated:YES];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)RBNavRightBtnAction {
    RBAlertView *alertView = [[RBAlertView alloc]init];
    alertView.delegate = self;
    alertView.tag = 7000;
    [alertView showWithArray:@[NSLocalizedString(@"R5102", @"所有消息设置已读"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if(alertView.tag == 7000) {
        if (buttonIndex==0) {
            [self.hudView show];
            // RB-获取所有消息设置已读
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBMessageAllRead];
        }
    }
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)loadRefreshViewFirstData {
    [self.hudView show];
    pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    pageSize = 10;
    // RB-获取消息列表
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBMessageListWithStatus:self.status andPage:[NSString stringWithFormat:@"%d",pageIndex+1]];
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageIndex = pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshViewFinish {
    [tableviewn.mj_header endRefreshing];
    [tableviewn.mj_footer endRefreshing];
    if ([datalist count] == (pageIndex+1)*pageSize) {
        if (tableviewn.mj_footer == nil){
            [self setRefreshFooterView];
        }
    } else {
        [tableviewn.mj_footer removeFromSuperview];
        tableviewn.mj_footer = nil;
    }
    [UIView performWithoutAnimation:^{
        [tableviewn reloadData];
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBUserMessageTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBUserMessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:cellIdentifier];
    }
    [cell loadRBUserMessageCellWith:datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    notificationEntity = datalist[indexPath.row];
    if ([NSString stringWithFormat:@"%@",notificationEntity.is_read].intValue==0) {
        // RB-获取消息已读
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBMessageRead:notificationEntity.iid];
    }
    NSString*type = [NSString stringWithFormat:@"%@",notificationEntity.message_type];
    if ([type isEqualToString:@"income"] || [type isEqualToString:@"screenshot_passed"] || [type isEqualToString:@"screenshot_rejected"]||[type isEqualToString:@"campaign"] || [type isEqualToString:@"remind_upload"]) {
        // 收入消息 & 截图审核消息 & 新活动消息
        NSString*type = [NSString stringWithFormat:@"%@",notificationEntity.sub_message_type];
        NSString*sub_type = [NSString stringWithFormat:@"%@",notificationEntity.sub_sub_message_type];
        if ([type isEqualToString:@"recruit"]) {
            if([sub_type isEqualToString:@"weibo"]||[sub_type isEqualToString:@"wechat"]||[sub_type isEqualToString:@"qq"]) {
                RBCampaignRecruitAutoViewController*toview = [[RBCampaignRecruitAutoViewController alloc] init];
                toview.campaignId = notificationEntity.item_id;
                toview.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:toview animated:YES];
            } else {
                RBCampaignRecruitViewController*toview = [[RBCampaignRecruitViewController alloc] init];
                toview.campaignId = notificationEntity.item_id;
                toview.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:toview animated:YES];
            }
            return;
        }
        if ([type isEqualToString:@"invite"]) {
            RBCampaignInviteViewController*toview = [[RBCampaignInviteViewController alloc] init];
            toview.campaignId = notificationEntity.item_id;
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
            return;
        }
        RBCampaignDetailViewController *toview = [[RBCampaignDetailViewController alloc] init];
        toview.campaignId = notificationEntity.item_id;
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    } else if ([type isEqualToString:@"announcement"]) {
        // 系统消息
        TOWebViewController *toview = [[TOWebViewController alloc] init];
        toview.url = [NSURL URLWithString:notificationEntity.url];
        toview.title = notificationEntity.title;
        toview.showPageTitles = NO;
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    } else if ([type isEqualToString:@"common"]) {
        // 通知消息
        notificationEntity.is_read=@"1";
        [datalist replaceObjectAtIndex:indexPath.row withObject:notificationEntity];
        [tableviewn reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    } else {
        notificationEntity.is_read=@"1";
        [datalist replaceObjectAtIndex:indexPath.row withObject:notificationEntity];
        [tableviewn reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"messages_read"]) {
    }
    
    if ([sender isEqualToString:@"messages_read_all"]) {
        [self loadRefreshViewFirstData];
    }
    
    if ([sender isEqualToString:@"messages"]) {
        [self.hudView dismiss];
        if (pageIndex==0) {
            datalist=[NSMutableArray new];
        }
        datalist = [JsonService getRBNotificationListEntity:jsonObject andBackArray:datalist];
        [self setRefreshViewFinish];
        //
        self.defaultView.hidden = YES;
        if ([datalist count] == 0) {
            self.defaultView.hidden = NO;
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
