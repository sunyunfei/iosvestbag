//
//  RBUserContactBigVViewController.h
//  RB
//
//  Created by AngusNi on 5/19/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInputRightView.h"
@interface RBUserContactBigVViewController : RBBaseViewController
<RBBaseVCDelegate,RBRankingSwitchViewDelegate,UIScrollViewDelegate>

@end
