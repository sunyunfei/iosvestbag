//
//  RBUserContactCpsViewController.m
//  RB
//
//  Created by AngusNi on 5/19/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserContactCpsViewController.h"

@interface RBUserContactCpsViewController (){
    UITextField*inputTF;
    UITextField*inputTF1;
    UITextField*inputTF2;
    UITextField*inputTF3;
}
@end

@implementation RBUserContactCpsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R6080", @"商务合作");
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-contact-cps"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-contact-cps"];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIScrollView*editSV = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    editSV.bounces = YES;
    editSV.pagingEnabled = NO;
    editSV.userInteractionEnabled = YES;
    editSV.showsHorizontalScrollIndicator = NO;
    editSV.showsVerticalScrollIndicator = NO;
    [self.view addSubview:editSV];
    //
    UILabel *inputTLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, 150.0, 50.0)];
    inputTLabel.text = NSLocalizedString(@"R2084", @"姓名");
    inputTLabel.font = font_15;
    inputTLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
    [editSV addSubview:inputTLabel];
    //
    UIView*inputLineView = [[UIView alloc]initWithFrame:CGRectMake(0, inputTLabel.bottom, ScreenWidth, 0.5)];
    inputLineView.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editSV addSubview:inputLineView];
    //
    UILabel *inputTLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(inputTLabel.left,inputTLabel.bottom, inputTLabel.width, inputTLabel.height)];
    inputTLabel1.text = NSLocalizedString(@"R2085", @"电话");
    inputTLabel1.font = inputTLabel.font;
    inputTLabel1.textColor = inputTLabel.textColor;
    [editSV addSubview:inputTLabel1];
    //
    UIView*inputLineView1 = [[UIView alloc]initWithFrame:CGRectMake(inputLineView.left, inputTLabel1.bottom, inputLineView.width, inputLineView.height)];
    inputLineView1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editSV addSubview:inputLineView1];
    //
    UILabel *inputTLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(inputTLabel.left,inputTLabel1.bottom, inputTLabel.width, inputTLabel.height)];
    inputTLabel2.text = NSLocalizedString(@"R5180", @"公司");
    inputTLabel2.font = inputTLabel.font;
    inputTLabel2.textColor = inputTLabel.textColor;
    [editSV addSubview:inputTLabel2];
    //
    UIView*inputLineView2 = [[UIView alloc]initWithFrame:CGRectMake(inputLineView.left, inputTLabel2.bottom, inputLineView.width, inputLineView.height)];
    inputLineView2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editSV addSubview:inputLineView2];
    //
    UILabel *inputTLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(inputTLabel.left,inputTLabel2.bottom, inputTLabel.width, inputTLabel.height)];
    inputTLabel3.text = NSLocalizedString(@"R5181", @"邮箱");
    inputTLabel3.font = inputTLabel.font;
    inputTLabel3.textColor = inputTLabel.textColor;
    [editSV addSubview:inputTLabel3];
    //
    UIView*inputLineView3 = [[UIView alloc]initWithFrame:CGRectMake(inputLineView.left, inputTLabel3.bottom, inputLineView.width, inputLineView.height)];
    inputLineView3.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editSV addSubview:inputLineView3];
    //
    inputTF = [[UITextField alloc]initWithFrame:CGRectMake(0, inputTLabel.top, editSV.width-CellLeft, inputTLabel.height)];
    inputTF.delegate = self;
    inputTF.font = inputTLabel.font;
    inputTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    inputTF.textAlignment = NSTextAlignmentRight;
    inputTF.keyboardType =  UIKeyboardTypeDefault;
    inputTF.clearButtonMode = UITextFieldViewModeNever;
    inputTF.returnKeyType = UIReturnKeyDone;
    inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R2084", @"姓名")] attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    [editSV addSubview:inputTF];
    //
    inputTF1 = [[UITextField alloc]initWithFrame:CGRectMake(inputTF.left, inputTLabel1.top, inputTF.width, inputTLabel.height)];
    inputTF1.delegate = self;
    inputTF1.font = inputTLabel.font;
    inputTF1.textColor = inputTF.textColor;
    inputTF1.textAlignment = inputTF.textAlignment;
    inputTF1.keyboardType =  UIKeyboardTypeNumberPad;
    inputTF1.clearButtonMode = inputTF.clearButtonMode;
    inputTF1.returnKeyType = inputTF.returnKeyType;
    inputTF1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R2085", @"电话")] attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    [editSV addSubview:inputTF1];
    //
    inputTF2 = [[UITextField alloc]initWithFrame:CGRectMake(inputTF.left, inputTLabel2.top, inputTF.width, inputTLabel.height)];
    inputTF2.delegate = self;
    inputTF2.font = inputTLabel.font;
    inputTF2.textColor = inputTF.textColor;
    inputTF2.textAlignment = inputTF.textAlignment;
    inputTF2.keyboardType =  UIKeyboardTypeDefault;
    inputTF2.clearButtonMode = inputTF.clearButtonMode;
    inputTF2.returnKeyType = inputTF.returnKeyType;
    inputTF2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5180", @"公司")] attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    [editSV addSubview:inputTF2];
    //
    inputTF3 = [[UITextField alloc]initWithFrame:CGRectMake(inputTF.left, inputTLabel3.top, inputTF.width, inputTLabel.height)];
    inputTF3.delegate = self;
    inputTF3.font = inputTLabel.font;
    inputTF3.textColor = inputTF.textColor;
    inputTF3.textAlignment = inputTF.textAlignment;
    inputTF3.keyboardType =  UIKeyboardTypeDefault;
    inputTF3.clearButtonMode = inputTF.clearButtonMode;
    inputTF3.returnKeyType = inputTF.returnKeyType;
    inputTF3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5181", @"邮箱")] attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    [editSV addSubview:inputTF3];
    //
    TextFiledKeyBoard *textKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textKB.delegateT = self;
    [inputTF setInputAccessoryView:textKB];
    [inputTF1 setInputAccessoryView:textKB];
    [inputTF2 setInputAccessoryView:textKB];
    [inputTF3 setInputAccessoryView:textKB];
    if(inputTF3.bottom> editSV.height) {
        [editSV setContentSize:CGSizeMake(editSV.width, inputTF3.bottom)];
    }
    //
    inputTF.text = [LocalService getRBLocalDataUserLoginName];
    inputTF1.text = [LocalService getRBLocalDataUserLoginPhone];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    if (inputTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R2084", @"姓名")]];
        return;
    }
    if (inputTF1.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R2085", @"电话")]];
        return;
    }
    [self.hudView showOverlay];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserHelpFeedBackWithContent:[NSString stringWithFormat:@"我是品牌主,我要CPS合作\n姓名:%@,电话:%@,公司:%@,邮箱:%@",inputTF.text,inputTF1.text,inputTF2.text,inputTF3.text] andScreen:nil];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"feedbacks"]) {
        [self.hudView showSuccessWithStatus:@"提交成功，我们会尽快与您联系，请保持电话畅通！"];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self RBNavLeftBtnAction];
                       });
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
