//
//  RBUserContactRankingViewController.h
//  RB
//
//  Created by AngusNi on 5/19/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBUserContactRankingViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>

@end
