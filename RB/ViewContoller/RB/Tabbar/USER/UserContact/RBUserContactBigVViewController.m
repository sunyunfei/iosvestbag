//
//  RBUserContactBigVViewController.m
//  RB
//
//  Created by AngusNi on 5/19/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserContactBigVViewController.h"

@interface RBUserContactBigVViewController () {
    RBRankingSwitchView*switchView;
    NSArray*tArray;
    NSArray*tArray1;
    NSArray*tArray2;
    UIScrollView*scrollviewn;
    //
    RBInputRightView*inputView;
    RBInputRightView*inputView1;
    RBInputRightView*inputView2;
    RBInputRightView*inputView3;
    RBInputRightView*inputView4;
    //
    RBInputRightView*inputView10;
    RBInputRightView*inputView11;
    RBInputRightView*inputView12;
    RBInputRightView*inputView13;
    //
    RBInputRightView*inputView20;
    RBInputRightView*inputView21;
    RBInputRightView*inputView22;
    RBInputRightView*inputView23;
}
@end

@implementation RBUserContactBigVViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5005", @"大V报价");
    //
    switchView = [[RBRankingSwitchView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 38.0)];
    switchView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [switchView RBRankingSwitchViewShow:@[NSLocalizedString(@"R3002", @"微信公众号"),NSLocalizedString(@"R5006", @"微信朋友圈"),NSLocalizedString(@"R3001", @"新浪微博")] andSelected:0];
    switchView.delegate = self;
    [self.view addSubview:switchView];
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, switchView.bottom, ScreenWidth, ScreenHeight-switchView.bottom-45.0)];
    scrollviewn.bounces = YES;
    scrollviewn.pagingEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.delegate = self;
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width*3.0, scrollviewn.height)];
    [self.view addSubview:scrollviewn];
    //
    tArray = @[NSLocalizedString(@"R5007", @"公众号名称"),NSLocalizedString(@"R5008", @"粉丝数量"),NSLocalizedString(@"R5009", @"所属领域"),NSLocalizedString(@"R5010", @"头条价格"),NSLocalizedString(@"R5011", @"次条价格")];
    //
    inputView = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 50.0)];
    [inputView loadInputRightViewWithText:nil andTitle:tArray[0] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView];
    inputView.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray[0]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    //
    inputView1 = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, inputView.bottom, ScreenWidth, inputView.height)];
    [inputView1 loadInputRightViewWithText:nil andTitle:tArray[1] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView1.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView1];
    inputView1.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray[1]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    inputView1.inputTF.keyboardType = UIKeyboardTypeNumberPad;
    //
    inputView2 = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, inputView1.bottom, ScreenWidth, inputView.height)];
    [inputView2 loadInputRightViewWithText:nil andTitle:tArray[2] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView2.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView2];
    inputView2.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray[2]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    //
    inputView3 = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, inputView2.bottom, ScreenWidth, inputView.height)];
    [inputView3 loadInputRightViewWithText:nil andTitle:tArray[3] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView3.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView3];
    inputView3.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray[3]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    inputView3.inputTF.keyboardType = UIKeyboardTypeNumberPad;
    //
    inputView4 = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, inputView3.bottom, ScreenWidth, inputView.height)];
    [inputView4 loadInputRightViewWithText:nil andTitle:tArray[4] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView4.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView3];
    inputView4.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray[4]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    inputView4.inputTF.keyboardType = UIKeyboardTypeNumberPad;
    //
    tArray1 = @[NSLocalizedString(@"R2086", @"微信号"),NSLocalizedString(@"R5012", @"好友数量"),NSLocalizedString(@"R5009", @"所属领域"),NSLocalizedString(@"R5013", @"单条价格")];
    //
    inputView10 = [[RBInputRightView alloc]initWithFrame:CGRectMake(ScreenWidth, 0, ScreenWidth, 50.0)];
    [inputView10 loadInputRightViewWithText:nil andTitle:tArray1[0] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView10.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView10];
    inputView10.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray1[0]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    //
    inputView11 = [[RBInputRightView alloc]initWithFrame:CGRectMake(inputView10.left, inputView10.bottom, ScreenWidth, inputView10.height)];
    [inputView11 loadInputRightViewWithText:nil andTitle:tArray1[1] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView11.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView11];
    inputView11.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray1[1]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    inputView11.inputTF.keyboardType = UIKeyboardTypeNumberPad;
    //
    inputView12 = [[RBInputRightView alloc]initWithFrame:CGRectMake(inputView10.left, inputView11.bottom, ScreenWidth, inputView10.height)];
    [inputView12 loadInputRightViewWithText:nil andTitle:tArray1[2] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView12.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView12];
    inputView12.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray1[2]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    //
    inputView13 = [[RBInputRightView alloc]initWithFrame:CGRectMake(inputView10.left, inputView12.bottom, ScreenWidth, inputView10.height)];
    [inputView13 loadInputRightViewWithText:nil andTitle:tArray1[3] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView13.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView13];
    inputView13.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray1[3]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    inputView13.inputTF.keyboardType = UIKeyboardTypeNumberPad;
    //
    tArray2 = @[NSLocalizedString(@"R5014", @"微博名称"),NSLocalizedString(@"R5008", @"粉丝数量"),NSLocalizedString(@"R5009", @"所属领域"),NSLocalizedString(@"R5013", @"单条价格")];
    //
    inputView20 = [[RBInputRightView alloc]initWithFrame:CGRectMake(ScreenWidth*2.0, 0, ScreenWidth, 50.0)];
    [inputView20 loadInputRightViewWithText:nil andTitle:tArray2[0] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView20.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView20];
    inputView20.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray2[0]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    //
    inputView21 = [[RBInputRightView alloc]initWithFrame:CGRectMake(inputView20.left, inputView20.bottom, ScreenWidth, inputView10.height)];
    [inputView21 loadInputRightViewWithText:nil andTitle:tArray2[1] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView21.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView21];
    inputView21.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray2[1]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    inputView21.inputTF.keyboardType = UIKeyboardTypeNumberPad;
    //
    inputView22 = [[RBInputRightView alloc]initWithFrame:CGRectMake(inputView20.left, inputView21.bottom, ScreenWidth, inputView10.height)];
    [inputView22 loadInputRightViewWithText:nil andTitle:tArray2[2] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView22.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView22];
    inputView22.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray2[2]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    //
    inputView23 = [[RBInputRightView alloc]initWithFrame:CGRectMake(inputView20.left, inputView22.bottom, ScreenWidth, inputView10.height)];
    [inputView23 loadInputRightViewWithText:nil andTitle:tArray2[3] andIsDate:NO andIsSelect:NO andValues:nil];
    inputView23.userInteractionEnabled = YES;
    [scrollviewn addSubview:inputView23];
    inputView23.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),tArray2[3]] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    inputView23.inputTF.keyboardType = UIKeyboardTypeNumberPad;
    //
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-contact-bigV"];
    [self.hudView show];
    // RB-获取BigV列表
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoBigV];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-contact-bigV"];
}

#pragma mark- UIScrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView==scrollviewn) {
        CGPoint offset = scrollView.contentOffset;
        int currentPage = offset.x / (self.view.bounds.size.width);
        [switchView RBRankingSwitchViewShow:@[NSLocalizedString(@"R3002", @"微信公众号"),NSLocalizedString(@"R5006", @"微信朋友圈"),NSLocalizedString(@"R3001", @"新浪微博")] andSelected:currentPage];
        [self RBRankingSwitchViewSelected:switchView andIndex:currentPage];
    }
}

#pragma mark- RBCampaignSwitchView Delegate
- (void)RBRankingSwitchViewSelected:(RBRankingSwitchView *)view andIndex:(int)index {
    [scrollviewn scrollRectToVisible:CGRectMake(scrollviewn.width*index, 0, scrollviewn.width, scrollviewn.height) animated:YES];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    [self.hudView show];
    // RB-获取BigV-填写
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    if (switchView.selectedTag==0) {
        [handler getRBUserInfoBigVWithProvider:@"public_wechat" andName:inputView.inputTF.text andFollower_count:inputView1.inputTF.text andBelong_field:inputView2.inputTF.text andHeadline_price:inputView3.inputTF.text andSecond_price:inputView4.inputTF.text andSingle_price:nil];
    }
    if (switchView.selectedTag==1) {
        [handler getRBUserInfoBigVWithProvider:@"wechat" andName:inputView10.inputTF.text andFollower_count:inputView11.inputTF.text andBelong_field:inputView12.inputTF.text andHeadline_price:nil andSecond_price:nil andSingle_price:inputView13.inputTF.text];
    }
    if (switchView.selectedTag==2) {
        [handler getRBUserInfoBigVWithProvider:@"weibo" andName:inputView20.inputTF.text andFollower_count:inputView21.inputTF.text andBelong_field:inputView22.inputTF.text andHeadline_price:nil andSecond_price:nil andSingle_price:inputView23.inputTF.text];
    }
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"kol_identity_prices/set_price"]) {
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R5018", @"提交成功")];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self RBNavLeftBtnAction];
                       });
    }
    
    if ([sender isEqualToString:@"kol_identity_prices/list"]) {
        [self.hudView dismiss];
        NSMutableArray*list = [NSMutableArray new];
        list = [JsonService getRBUserInfoBigVListEntity:jsonObject andBackArray:list];
        for (RBUserBigVEntity *entity in list) {
            if ([entity.provider isEqualToString:@"public_wechat"]) {
                inputView.inputTF.text = entity.name;
                inputView1.inputTF.text = entity.follower_count;
                inputView2.inputTF.text = entity.belong_field;
                inputView3.inputTF.text = entity.headline_price;
                inputView4.inputTF.text = entity.second_price;
            }
            if ([entity.provider isEqualToString:@"wechat"]) {
                inputView10.inputTF.text = entity.name;
                inputView11.inputTF.text = entity.follower_count;
                inputView12.inputTF.text = entity.belong_field;
                inputView13.inputTF.text = entity.single_price;
            }
            if ([entity.provider isEqualToString:@"weibo"]) {
                inputView20.inputTF.text = entity.name;
                inputView21.inputTF.text = entity.follower_count;
                inputView22.inputTF.text = entity.belong_field;
                inputView23.inputTF.text = entity.single_price;
            }
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}


@end
