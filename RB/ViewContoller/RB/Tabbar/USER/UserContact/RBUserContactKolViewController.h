//
//  RBUserContactKolViewController.h
//  RB
//
//  Created by AngusNi on 5/19/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBUserContactKolViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
@property(nonatomic, strong) NSString* kolName;
@property(nonatomic, strong) NSString* kolId;

@end
