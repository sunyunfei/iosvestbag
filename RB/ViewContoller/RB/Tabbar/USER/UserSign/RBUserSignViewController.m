//
//  RBUserSignViewController.m
//  RB
//
//  Created by AngusNi on 5/17/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBUserSignViewController.h"
#import "LXCalender.h"
#import "RBWeekView.h"
#import "RBFirstDoCampaignController.h"
#import "RBAppDelegate.h"
#import "RBPhoneInviteViewController.h"
#import "RBUserInviteViewController.h"
@interface RBUserSignViewController () {
    UIView*editView;
    UILabel*userDesLabel;
    UILabel * allEarnLabel;
    UILabel * todayEarnLabel;
    BOOL isSign;
    RBWeekView * signView;
    RBNewMissionView * missionView;
    //
    RBcompleteSignView * bottomView1;
    RBcompleteSignView * bottomView2;
    RBcompleteSignView * bottomView3;
    //
    UIView * bootomBigView;
    //
    RBUserSignEntity*signEntity;
}
@end

@implementation RBUserSignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    isSign = NO;
    //
//    [self loadEditView];
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R2301", @"任务");
    [self getSignHistory];
    
}
//签到奖励规则
-(void)ruleAction:(id)sender{
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    [alert showWithSign];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"my-sign"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"my-sign"];
}

- (void)getSignHistory {
    [self.hudView showOverlay];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserRewardCheckInNewHistory];
//    [handler getRBWeekSignHistory];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIScrollView*scrollviewn = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    [scrollviewn setPagingEnabled:NO];
    scrollviewn.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [scrollviewn setShowsHorizontalScrollIndicator:NO];
    [scrollviewn setShowsVerticalScrollIndicator:NO];
    scrollviewn.userInteractionEnabled = YES;
    [self.view addSubview:scrollviewn];
    //
    if (@available(iOS 11.0,*)) {
        scrollviewn.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    //
    editView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 0)];
    editView.userInteractionEnabled = YES;
    editView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [scrollviewn addSubview:editView];
    //
    UIImageView*editViewBgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 165)];
    editViewBgIV.userInteractionEnabled = YES;
    editViewBgIV.image = [UIImage imageNamed:@"RBheadBack"];
//    editViewBgIV.contentMode = UIViewContentModeScaleAspectFill;
//    editViewBgIV.autoresizesSubviews = YES;
    [editView addSubview:editViewBgIV];
    //
    UIImageView*userBgIV = [[UIImageView alloc]initWithFrame:CGRectMake((editView.width-63.0)/2.0, 26.0, 63.0, 63.0)];
    userBgIV.layer.cornerRadius = userBgIV.width/2.0;
    userBgIV.layer.masksToBounds = YES;
    [editViewBgIV addSubview:userBgIV];
    //
    UIButton * signBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 71 - 15, 29, 71.0, 18) title:@"签到奖励规则" hlTitle:nil titleColor:[Utils getUIColorWithHexString:@"bbbbbb"] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    signBtn.layer.cornerRadius = 9.0;
    signBtn.layer.borderColor = [Utils getUIColorWithHexString:@"bbbbbb"].CGColor;
    signBtn.layer.borderWidth = 0.5;
    signBtn.layer.masksToBounds = YES;
    [signBtn addTarget:self action:@selector(ruleAction:) forControlEvents:UIControlEventTouchUpInside];
    signBtn.titleLabel.font = font_(9);
    [editViewBgIV addSubview:signBtn];
    
    editView.userInteractionEnabled = YES;
    //
    UILabel*userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, userBgIV.bottom+13.0, editView.width, 18.0)];
    userNameLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    userNameLabel.font = font_cu_cu_(16);
    userNameLabel.textAlignment = NSTextAlignmentCenter;
    [editViewBgIV addSubview:userNameLabel];
    //
    userDesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, userNameLabel.bottom+9.0, editView.width, 11.0)];
    userDesLabel.textColor = [Utils getUIColorWithHexString:@"bbbbbb"];
    userDesLabel.font = font_cu_13;
    userDesLabel.textAlignment = NSTextAlignmentCenter;
    [editViewBgIV addSubview:userDesLabel];
    //
    UILabel * lineLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth - 0.5)/2, editViewBgIV.bottom + 15, 0.5, 22) text:@"" font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorGray2] numberOfLines:1];
    
    [editView addSubview:lineLabel];
    //
    UILabel * yuanLabel = [[UILabel alloc]initWithFrame:CGRectMake(lineLabel.left - 25 * ScaleWidth - 12,0, 12.0, 12.0) text:@"元" font:font_cu_(13) textAlignment:NSTextAlignmentRight textColor:[Utils getUIColorWithHexString:@"777777"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    yuanLabel.centerY = lineLabel.centerY;
    [editView addSubview:yuanLabel];
    //
    UILabel * label1 = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 0,0, 20) text:@"累计收益" font:font_cu_(13) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"777777"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [label1 sizeToFit];
    label1.frame = CGRectMake(11, lineLabel.centerY - 6, label1.width, label1.height);
    [editView addSubview:label1];
    //
    allEarnLabel = [[UILabel alloc]initWithFrame:CGRectMake(label1.right, lineLabel.centerY - 8.0,yuanLabel.left - label1.right, 16) text:@"0" font:font_cu_cu_(18) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"ffad4c"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [editView addSubview:allEarnLabel];
    //
    UILabel * label2 = [[UILabel alloc]initWithFrame:CGRectMake(lineLabel.right + 25 * ScaleWidth, yuanLabel.top, label1.width, label1.height) text:@"今日收益" font:font_cu_(13) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"777777"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [editView addSubview:label2];
    //
    UILabel * yuanlabel1 = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - CellLeft - 12, yuanLabel.top, 12, 12) text:@"元" font:font_cu_(13) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"777777"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [editView addSubview:yuanlabel1];
    //
    todayEarnLabel = [[UILabel alloc]initWithFrame:CGRectMake(label2.right, allEarnLabel.top, yuanlabel1.left - label2.right, allEarnLabel.height) text:@"0.0" font:font_cu_cu_(18) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"ffad4c"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [editView addSubview:todayEarnLabel];
    //
    editView.height = lineLabel.bottom;
    //
    [userBgIV sd_setImageWithURL:[NSURL URLWithString:self.avatar] placeholderImage:PlaceHolderUserImage];
    userNameLabel.text = [NSString stringWithFormat:@"%@",self.name];
    //
    signView = [[RBWeekView alloc]initWithFrame:CGRectMake(0,editView.bottom, ScreenWidth, 162)];
    signView.delegate = self;
    [scrollviewn addSubview:signView];
    //
    if ([LocalService getRBIsNewMember] == YES && [signEntity.is_show_newbie isEqualToString:@"0"]) {
        missionView = [[[NSBundle mainBundle]loadNibNamed:@"RBNewMissionView" owner:self options:nil]objectAtIndex:0];
        missionView.frame = CGRectMake(0, signView.bottom + 8, ScreenWidth, 214);
        missionView.delegate = self;
        [scrollviewn addSubview:missionView];
    }
    //
    
    bootomBigView = [[UIView alloc]initWithFrame:CGRectMake(0, signView.bottom + 8, ScreenWidth, 0)];
    if ([LocalService getRBIsNewMember] == YES && [signEntity.is_show_newbie isEqualToString:@"0"]) {
        bootomBigView.frame = CGRectMake(0, signView.bottom + 8 + 214 + 8, ScreenWidth, 0);
    }
    bootomBigView.backgroundColor = [UIColor whiteColor];
    [scrollviewn addSubview:bootomBigView];
    //
    UIImageView * smallImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 20, 12, 12)];
    smallImageView.image = [UIImage imageNamed:@"RBdayMission"];
    [bootomBigView addSubview:smallImageView];
    //
    UILabel * smallLabel = [[UILabel alloc]initWithFrame:CGRectMake(smallImageView.right + 6, 20, 100, 12) text:@"每日任务" font:font_(11) textAlignment:NSTextAlignmentLeft textColor:[UIColor blackColor] backgroundColor:[UIColor clearColor] numberOfLines:1];
    smallLabel.font = [UIFont systemFontOfSize:11];
    [bootomBigView addSubview:smallLabel];
    //
    bottomView1 = [[RBcompleteSignView alloc]initWithFrame:CGRectMake(0, smallLabel.bottom + 10, ScreenWidth, 63)];
    bottomView1.delegate = self;
    bottomView1.headImageView.image = [UIImage imageNamed:@"RBShareInvite"];
    bottomView1.bigLabel.text = [NSString stringWithFormat:@"活动分享 (%@/4)",signEntity.campaign_invites_count];
    bottomView1.smallLabel.text = @"奖励:视活动而定";
    bottomView1.goBtn.tag = 100;
    [bootomBigView addSubview:bottomView1];
    //
    UILabel * seprateLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, bottomView1.bottom, ScreenWidth, 0.5)];
    seprateLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];

    [bootomBigView addSubview:seprateLabel];
    //
    bottomView2 = [[RBcompleteSignView alloc]initWithFrame:CGRectMake(0, seprateLabel.bottom, ScreenWidth, 63)];
    bottomView2.delegate = self;
    bottomView2.headImageView.image = [UIImage imageNamed:@"readIcon"];
    bottomView2.bigLabel.text = [NSString stringWithFormat:@"内容挖掘 (%@/10)",signEntity.red_money_count];
    bottomView2.smallLabel.text = @"奖励:海量现金红包砸中你";
    [bottomView2.goBtn setTitle:@"去阅读" forState:UIControlStateNormal];
    bottomView2.goBtn.tag = 101;
    [bootomBigView addSubview:bottomView2];
    //
    UILabel * seprateLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, bottomView2.bottom, ScreenWidth, 0.5)];
    seprateLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [bootomBigView addSubview:seprateLabel1];
    //
    bottomView3 = [[RBcompleteSignView alloc]initWithFrame:CGRectMake(0, seprateLabel1.bottom, ScreenWidth, 63)];
    bottomView3.delegate = self;
    bottomView3.headImageView.image = [UIImage imageNamed:@"RBInviteFriend"];
    bottomView3.bigLabel.text = [NSString stringWithFormat:@"邀请好友 (%@/10)",signEntity.invite_friends];
    bottomView3.smallLabel.text = @"奖励:￥2.00";
    bottomView3.goBtn.tag = 102;
    [bootomBigView addSubview:bottomView3];
    bootomBigView.height = bottomView3.bottom;
    [scrollviewn setContentSize:CGSizeMake(ScreenWidth, bootomBigView.bottom)];
}
#pragma RBNewMissionViewDelegate
- (void)firstDoCampaign{
//    RBFirstDoCampaignController * campaignVC = [[RBFirstDoCampaignController alloc]init];
//    [self.navigationController pushViewController:campaignVC animated:YES];
//    RBCampainMissionViewController * campaignVC = [[RBCampainMissionViewController alloc]init];
//    [self.navigationController pushViewController:campaignVC animated:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:NotificationBackToHome object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:NotificationCampainAddCoverView object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma RBcompletesignViewDelegate
- (void)goToWork:(NSInteger)tag{
    if (tag == 100) {
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:22];
    }else if(tag == 101){
        [[NSNotificationCenter defaultCenter]postNotificationName:@"jumpToFind" object:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }else if (tag == 102){
        RBUserInviteViewController * userSign = [[RBUserInviteViewController alloc]init];
        [self.navigationController pushViewController:userSign animated:YES];
        //
//        RBPhoneInviteViewController * inviteVC = [[RBPhoneInviteViewController alloc]init];
//        [self.navigationController pushViewController:inviteVC animated:YES];
    }
}
#pragma mark - RBWeekViewDelegate
- (void)RBSignAction{
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserRewardNewCheckIn];
}
#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"check_in_history"]) {
        [self.hudView dismiss];
        signEntity = [JsonService getRBUserSignEntity:jsonObject];
        if (isSign == YES) {
            RBNewAlert * alert = [[RBNewAlert alloc]init];
            NSString * str = [NSString stringWithFormat:@"签到成功,%@元奖励已放入您的钱包!\n您已连续签到%@天,不要间断哦",signEntity.today_already_amount,signEntity.continuous_checkin_count];
            NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:str];
            NSRange range = [str rangeOfString:signEntity.today_already_amount];
            NSRange range1 = [str rangeOfString:signEntity.continuous_checkin_count];
            [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorBlue] range:range];
            [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorBlue] range:range1];
            [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, attStr.length)];
            [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:range];
            [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:range1];
            NSMutableParagraphStyle * style = [[NSMutableParagraphStyle alloc]init];
            style.lineSpacing = 10;
            [attStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, attStr.length)];
            [alert successSignIn:attStr AndTitle:@"签到成功"];
        }
        //
        
        //
        
        //
        [self loadEditView];
        
        allEarnLabel.text = [NSString stringWithFormat:@"%@",signEntity.total_check_in_amount];
        todayEarnLabel.text = [NSString stringWithFormat:@"%@",signEntity.today_already_amount];
        //
        signView.signArray = jsonObject[@"check_in_7"];
        [signView initUI];
        //
        if (signEntity.today_had_check_in.intValue==1) {
            userDesLabel.text = [NSString stringWithFormat:@"您已连续签到%@天,明日签到可领%@元",signEntity.continuous_checkin_count,signEntity.tomorrow_can_amount];
            signView.signBtn.backgroundColor = [Utils getUIColorWithHexString:SysWhiteGray];
            signView.signBtn.enabled = NO;
            [signView.signBtn setTitle:@"已签到" forState:UIControlStateNormal];
        } else {
            userDesLabel.text = [NSString stringWithFormat:@"您已连续签到%@天,今日签到可领%@元",signEntity.continuous_checkin_count,signEntity.today_can_amount];
            signView.signBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
            signView.signBtn.enabled = YES;
        }
    }
    if ([sender isEqualToString:@"check_in"]) {
        isSign = YES;
        [self getSignHistory];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
