//
//  RBSignView.h
//  RB
//
//  Created by AngusNi on 16/3/10.
//  Copyright © 2016年 AngusNi. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Service.h"

@class RBSignView;
@protocol RBSignViewDelegate <NSObject>
@optional
- (void)RBSignView:(RBSignView *)actionSheet clickedButtonAtIndex:(int)buttonIndex;
@end

@interface RBSignView : UIView

@property (nonatomic, weak)  id<RBSignViewDelegate> delegate;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UIImageView*bgIV;
- (void)show;
@end
