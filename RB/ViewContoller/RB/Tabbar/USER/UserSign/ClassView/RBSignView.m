//
//  RBSignView.m
//  RB
//
//  Created by AngusNi on 16/3/10.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBSignView.h"

@interface RBSignView () {
}
@end

@implementation RBSignView

- (instancetype)init {
    if (self = [super init]) {
        self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
        id<UIApplicationDelegate> delegate  =
        [[UIApplication sharedApplication] delegate];
        if ([delegate respondsToSelector:@selector(window)]) {
            self.window = [delegate performSelector:@selector(window)];
        } else {
            self.window = [[UIApplication sharedApplication] keyWindow];
        }
        //
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.bgIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-150.0)/2.0, (ScreenHeight-150.0)/2.0, 150.0, 150.0)];
        self.bgIV.image = [UIImage imageNamed:@"pic_user_sign.png"];
        [self addSubview:self.bgIV];
        //
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}
- (void)show {
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = self.bgIV.transform;
        self.bgIV.transform  =
        CGAffineTransformScale(self.bgIV.transform, 0.5, 0.5);
        [UIView animateWithDuration:1.5 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.bgIV.transform = oldTransform;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)tapGesture {
    [self removeFromSuperview];
}

@end
