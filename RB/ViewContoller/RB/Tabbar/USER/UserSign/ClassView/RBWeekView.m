//
//  RBWeekView.m
//  RB
//
//  Created by RB8 on 2018/4/23.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBWeekView.h"
#import "Service.h"
@implementation RBWeekView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        
    }
    return self;
}
- (void)initUI{
    self.backgroundColor = [UIColor whiteColor];
    self.userInteractionEnabled = YES;
    UILabel * lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(28, 37.5,ScreenWidth - 2 * 28,5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:@"f0f3f4" andAlpha:1.0];
    [self addSubview:lineLabel];
    //
    CGFloat a = (ScreenWidth - 2 * 15 - 7 * 30)/6;
    NSArray * arr = @[@"1天",@"2天",@"3天",@"4天",@"5天",@"6天",@"7天"];
    NSArray * arr1 = @[@"0.10",@"0.20",@"0.25",@"0.30",@"0.35",@"0.40",@"0.50"];
    if (_signArray.count == 7) {
        for (NSInteger i = 0; i<7; i ++) {
            UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(15 + (30 + a)*i, lineLabel.center.y - 15, 30, 30) title:arr1[i] hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorBlue] image:nil hlImage:nil];
           
            
            btn.layer.cornerRadius = 15.0;
            btn.layer.masksToBounds = YES;
            btn.titleLabel.font = font_(10);
            btn.layer.borderColor = [Utils getUIColorWithHexString:@"f0f3f4" andAlpha:1.0].CGColor;
            btn.layer.borderWidth = 0.5;
            [self addSubview:btn];
            if ([_signArray[i] boolValue] == 0) {
                btn.backgroundColor = [Utils getUIColorWithHexString:@"f0f3f4"];
                [btn setTitleColor:[Utils getUIColorWithHexString:@"777777"] forState:UIControlStateNormal];
            }else if ([_signArray[i] boolValue] == 1){
                btn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
                [btn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
            }
            //
            if (i == 6) {
                if ([_signArray[i] boolValue] == 0) {
                    [btn setImage:[UIImage imageNamed:@"RBWeekBag"] forState:UIControlStateNormal];
                    [btn setTitle:@"" forState:UIControlStateNormal];
                }else{
                    [btn setImage:nil forState:UIControlStateNormal];
                    [btn setTitle:arr1[i] forState:UIControlStateNormal];
                }
            }
            
            UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, btn.bottom + 5, (ScreenWidth - 2 * 15)/7, 30) text:arr[i] font:font_(12) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"777777"] backgroundColor:[UIColor clearColor] numberOfLines:1];
            CGFloat b = btn.center.x;
            label.centerX = b;
            [self addSubview:label];
        }
    }
    //
    _signBtn = [[UIButton alloc]initWithFrame:CGRectMake((ScreenWidth - 130)/2, lineLabel.center.y + 15 + 5 + 30 + 20, 130, 38) title:@"签到领奖励" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorBlue] image:nil hlImage:nil];
    _signBtn.titleLabel.font = font_cu_(16);
    [_signBtn addTarget:self action:@selector(signAction:) forControlEvents:UIControlEventTouchUpInside];
    _signBtn.layer.cornerRadius = 19;
    _signBtn.layer.masksToBounds = YES;
    [self addSubview:_signBtn];
}
- (void)signAction:(id)sender{
    if ([self.delegate respondsToSelector:@selector(RBSignAction)]) {
        [self.delegate RBSignAction];
    }
}
@end
