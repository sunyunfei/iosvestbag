//
//  RBNewMissionView.m
//  RB
//
//  Created by RB8 on 2018/4/23.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBNewMissionView.h"

@implementation RBNewMissionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)doMission:(id)sender {
    if ([self.delegate respondsToSelector:@selector(firstDoCampaign)]) {
        [self.delegate firstDoCampaign];
    }
}
@end
