//
//  RBWeekView.h
//  RB
//
//  Created by RB8 on 2018/4/23.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RBWeekViewDelegate <NSObject>
- (void)RBSignAction;
@end
@interface RBWeekView : UIView
@property(nonatomic,weak)id<RBWeekViewDelegate>delegate;
@property(nonatomic,strong)UIButton * signBtn;
- (void)initUI;
@property(nonatomic,strong)NSArray * signArray;
@end
