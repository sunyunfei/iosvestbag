//
//  RBcompleteSignView.h
//  RB
//
//  Created by RB8 on 2018/4/24.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RBcompleteSignViewDelegate <NSObject>
- (void)goToWork:(NSInteger)tag;
@end
@interface RBcompleteSignView : UIView
@property(nonatomic,strong)UIButton * goBtn;
@property(nonatomic,strong)UILabel * smallLabel;
@property(nonatomic,weak)id<RBcompleteSignViewDelegate>delegate;
@property(nonatomic,strong)UIImageView * headImageView;
@property(nonatomic,strong)UILabel * bigLabel;
@end
