//
//  RBNewMissionView.h
//  RB
//
//  Created by RB8 on 2018/4/23.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RBNewMissionViewDelegate <NSObject>
- (void)firstDoCampaign;
@end
@interface RBNewMissionView : UIView

- (IBAction)doMission:(id)sender;
@property(nonatomic,weak)id<RBNewMissionViewDelegate>delegate;
@end
