//
//  RBcompleteSignView.m
//  RB
//
//  Created by RB8 on 2018/4/24.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBcompleteSignView.h"
#import "Service.h"
@implementation RBcompleteSignView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        _headImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, (63-33)/2, 33, 33)];
        _headImageView.layer.cornerRadius = 16.5;
        _headImageView.backgroundColor = [UIColor yellowColor];
        [self addSubview:_headImageView];
        //
        _bigLabel = [[UILabel alloc]initWithFrame:CGRectMake(_headImageView.right + 9, _headImageView.top, 120, 16) text:@"活动分享 (0/4)" font:font_(14) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:@"000000"] backgroundColor:[UIColor clearColor] numberOfLines:1];
        _bigLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_bigLabel];
        //
        _smallLabel = [[UILabel alloc]initWithFrame:CGRectMake(_bigLabel.left,_bigLabel.bottom +  4, ScreenWidth - _bigLabel.left -75, 14) text:@"奖励:视活动而定" font:font_(12) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:@"777777"] backgroundColor:[UIColor clearColor] numberOfLines:1];
        [self addSubview:_smallLabel];
        //
        _goBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 75, (63 - 25)/2, 60, 25) title:@"去完成" hlTitle:nil titleColor:[UIColor whiteColor] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysBackColorBlue] image:nil hlImage:nil];
        [_goBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        _goBtn.layer.cornerRadius = 12.5;
        _goBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [self addSubview:_goBtn];
        
    }
    return self;
}
- (void)btnAction:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(goToWork:)]) {
        [self.delegate goToWork:_goBtn.tag];
    }
}
@end
