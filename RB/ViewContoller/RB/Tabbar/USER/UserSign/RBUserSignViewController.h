//
//  RBUserSignViewController.h
//  RB
//
//  Created by AngusNi on 5/17/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "FYCalendarView.h"
#import "RBSignView.h"
#import "RBcompleteSignView.h"
#import "RBNewMissionView.h"
#import "RBCampainMissionViewController.h"
@interface RBUserSignViewController : RBBaseViewController
<RBBaseVCDelegate,RBWeekViewDelegate,RBcompleteSignViewDelegate,RBNewMissionViewDelegate>
@property(nonatomic, strong) NSString *avatar;
@property(nonatomic, strong) NSString *name;

@end
