//
//  RBCampainMissionViewController.m
//  RB
//
//  Created by RB8 on 2018/5/4.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBCampainMissionViewController.h"

@interface RBCampainMissionViewController ()

@end

@implementation RBCampainMissionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = YES;
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    imageView.image = [UIImage imageNamed:@"RBClickMission"];
    imageView.userInteractionEnabled = YES;
    [self.view addSubview:imageView];
    //
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ClickMission:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [imageView addGestureRecognizer:tap];
}
- (void)ClickMission:(UITapGestureRecognizer*)tap{
    RBFirstDoCampaignController * campaignVC = [[RBFirstDoCampaignController alloc]init];
    [self.navigationController pushViewController:campaignVC animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
