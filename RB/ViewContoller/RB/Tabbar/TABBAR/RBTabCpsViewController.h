//
//  RBTabCpsViewController.h
//  RB
//
//  Created by AngusNi on 16/8/25.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBArticleViewController.h"
#import "RBCpsEditViewController.h"
#import "RBCpsProductViewController.h"
#import "RBCpsMyViewController.h"
#import "RBCpsDetailViewController.h"
#import "RBCpsProductSearchViewController.h"
//
#import "RBCpsView.h"
//
@interface RBTabCpsViewController : RBBaseViewController
<RBBaseVCDelegate,RBCpsViewDelegate,SDCycleScrollViewDelegate>
@property(nonatomic, strong) RBCpsView *cpsView;

@end
