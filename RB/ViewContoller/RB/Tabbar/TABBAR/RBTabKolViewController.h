//
//  RBTabKolViewController.h
//  RB
//
//  Created by AngusNi on 7/11/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolView.h"
#import "RBGuideView.h"

#import "TOWebViewController.h"
#import "RBEngineViewController.h"
#import "RBKolDetailViewController.h"
#import "RBKolSearchListViewController.h"
#import "RBKolSearchViewController.h"
#import "RBAdvertiserAddViewController.h"
@interface RBTabKolViewController : RBBaseViewController
<RBBaseVCDelegate,RBKolViewDelegate,SDCycleScrollViewDelegate,CLLocationManagerDelegate>
@property(nonatomic, strong) RBKolView *kolView;

@end
