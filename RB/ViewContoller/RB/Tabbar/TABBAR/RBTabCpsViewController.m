//
//  RBTabCpsViewController.m
//  RB
//
//  Created by AngusNi on 16/8/25.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBTabCpsViewController.h"

@interface RBTabCpsViewController () {
}
@end

@implementation RBTabCpsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R0007",@"创作");
    self.navRightTitle = Icon_bar_cps_ark;
    self.navLeftTitle = Icon_bar_write_ark;
    self.bgIV.hidden = NO;
    self.edgePGR.enabled = NO;
    //
    self.cpsView = [[RBCpsView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-self.tabBarController.tabBar.height)];
    self.cpsView.delegate = self;
    [self.view addSubview:self.cpsView];
    [self loadHeaderView];
    //
    [self.hudView show];
    [self.cpsView RBCpsViewLoadRefreshViewFirstData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageBegin:@"cps-list"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"cps-list"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    if([self isVisitorLogin]==YES) {
        return;
    };
    RBCpsEditViewController *toview = [[RBCpsEditViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)RBNavRightBtnAction {
    if([self isVisitorLogin]==YES) {
        return;
    };
    RBCpsMyViewController *toview = [[RBCpsMyViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationStatusBarAction {
    CGPoint off = self.cpsView.tableviewn.contentOffset;
    off.y = 0 - self.cpsView.tableviewn.contentInset.top;
    [self.cpsView.tableviewn setContentOffset:off animated:YES];
}

#pragma mark - RBCpsView Delegate
-(void)RBCpsViewLoadRefreshViewData:(RBCpsView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBCpsCreateListWithPage:[NSString stringWithFormat:@"%d",self.cpsView.pageIndex+1]];
}

- (void)RBCpsViewSelected:(RBCpsView *)view andIndex:(int)index {
    RBCpsCreateEntity*entity = view.datalist[index];
    RBCpsDetailViewController *toview = [[RBCpsDetailViewController alloc] init];
    toview.createEntity = entity;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - SDCycleScrollView Delegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    TOWebViewController*toview = [[TOWebViewController alloc] init];
    toview.url = [NSURL URLWithString:@"http://7xuw3n.com1.z0.glb.clouddn.com/Robin8_cps_banner_0@2x.png"];
    toview.title = NSLocalizedString(@"R0007",@"创作");
    toview.showPageTitles = NO;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)loadHeaderView {
    UIView*bannerBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth, (ScreenWidth*137.0)/375.0 +74.0 )];
    bannerBgView.backgroundColor = [UIColor whiteColor];
    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0,ScreenWidth, (ScreenWidth*137.0)/375.0) shouldInfiniteLoop:YES imageNamesGroup:@[@"pic_create_banner_0.jpg",@"pic_create_banner_1.jpg"]];
    cycleScrollView.delegate = self;
    cycleScrollView.autoScrollTimeInterval = 5.0;
    cycleScrollView.currentPageDotImage = [UIImage imageNamed:@"pageControlCurrentDot.png"];
    cycleScrollView.pageDotImage = [UIImage imageNamed:@"pageControlDot.png"];
    [bannerBgView addSubview:cycleScrollView];
    //
    UIImageView*leftIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth/2.0-71.0)/2.0, cycleScrollView.bottom+(74.0-35.0)/2.0, 71.0, 35.0)];
    leftIV.image = [UIImage imageNamed:@"icon_create_product.png"];
    [bannerBgView addSubview:leftIV];
    //
    UIImageView*rightIV = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2.0+(ScreenWidth/2.0-71.0)/2.0, leftIV.top, leftIV.width, leftIV.height)];
    rightIV.image = [UIImage imageNamed:@"icon_create_article.png"];
    [bannerBgView addSubview:rightIV];
    //
    UIView*lineView = [[UIView alloc]initWithFrame:CGRectMake(ScreenWidth/2.0, cycleScrollView.bottom, 0.5, 74.0)];
    lineView.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [bannerBgView addSubview:lineView];
    //
    UIButton*bannerLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    bannerLeftBtn.frame = CGRectMake(0, cycleScrollView.bottom, ScreenWidth/2.0, 74.0);
    [bannerLeftBtn addTarget:self
                       action:@selector(bannerLeftBtnAction)
             forControlEvents:UIControlEventTouchUpInside];
    [bannerBgView addSubview:bannerLeftBtn];
    //
    UIButton*bannerRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    bannerRightBtn.frame = CGRectMake(bannerLeftBtn.right, bannerLeftBtn.top, bannerLeftBtn.width, bannerLeftBtn.height);
    [bannerRightBtn addTarget:self
                    action:@selector(bannerRightBtnAction)
          forControlEvents:UIControlEventTouchUpInside];
    [bannerBgView addSubview:bannerRightBtn];
    //
    [self.cpsView.tableviewn setTableHeaderView:bannerBgView];
}

#pragma mark - UIButton Delegate
- (void)bannerLeftBtnAction {
    RBCpsProductSearchViewController*toview = [[RBCpsProductSearchViewController alloc] init];
    toview.isNew = @"YES";
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)bannerRightBtnAction {
    RBArticleViewController *toview = [[RBArticleViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"cps_articles"]) {
        [self.hudView dismiss];
        if (self.cpsView.pageIndex == 0) {
            self.cpsView.datalist = [NSMutableArray new];
            self.cpsView.jsonObject = jsonObject;
        }
        if ([self.cpsView.datalist count]%40==0) {
            YYImageCache *cache = [YYWebImageManager sharedManager].cache;
            [cache.memoryCache removeAllObjects];
            [cache.diskCache removeAllObjects];
        }
        self.cpsView.datalist = [JsonService getRBCpsCreateListEntity:jsonObject andBackArray:self.cpsView.datalist];
        [self.cpsView RBCpsViewSetRefreshViewFinish];
        if ([self.cpsView.datalist count] == 0) {
            self.cpsView.defaultView.hidden = NO;
        } else {
            self.cpsView.defaultView.hidden = YES;
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self.cpsView RBCpsViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.cpsView RBCpsViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

