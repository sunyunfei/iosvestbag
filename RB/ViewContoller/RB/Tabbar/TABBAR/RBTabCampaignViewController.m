/////
//  RBCampaignViewController.m
//  RB
//
//  Created by AngusNi on 5/9/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBTabCampaignViewController.h"
#import "RBUserInviteViewController.h"
#import "RBUserSignViewController.h"
#import "NewPagedFlowView.h"
#import "RBFirstDoCampaignController.h"
#import "RBBrandMessageController.h"
@interface RBTabCampaignViewController ()<NewPagedFlowViewDelegate,NewPagedFlowViewDataSource> {
    NSArray*statusArray;
    NSArray*titlesArray;
    //
    RBUserDashboardEntity*dashboardEntity;
    HDSlideBannerView *bannerView;
    NSMutableArray*bannerlist;
    UIView*bannerBgView;
    NewPagedFlowView *pageFlowView;
    NSTimer * timer;
    //记录是否有倒计时的活动
    BOOL isbegin;
    //
    UIView * coverView;
}
@end

@implementation RBTabCampaignViewController
//- (UIView*)initCoverView{
//    if (coverView == nil) {
//
//    }
//}
-(void)removeImageView:(UITapGestureRecognizer*)tap{
    UIView * view = (UIView*)tap.view;
    [view removeFromSuperview];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([LocalService getRBfirstEnterApp] == NO) {
        UIView * backView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        backView.userInteractionEnabled = YES;
        backView.tag = 1000;
        backView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        UIImageView * campaignBtn = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth/4 -42)/2, ScreenHeight - self.tabBarController.tabBar.height + 4, 42, 42)];
        campaignBtn.image = [UIImage imageNamed:@"campain1"];
        [backView addSubview: campaignBtn];
        UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/8 - 11 , ScreenHeight - self.tabBarController.tabBar.height-202, 122, 202)];
        imageView.image = [UIImage imageNamed:@"campain2"];
        [backView addSubview:imageView];
        //
        UIImageView * influenceBtn = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/4 + ScreenWidth/8 - 21, ScreenHeight - self.tabBarController.tabBar.height + 4 , 42, 42)];
        influenceBtn.image = [UIImage imageNamed:@"Influence1"];
        [backView addSubview:influenceBtn];
        //
        UIImageView * imageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth*3/8 - 15, ScreenHeight - self.tabBarController.tabBar.height - 262, 191, 262)];
        imageView1.image = [UIImage imageNamed:@"Influence2"];
        [backView addSubview:imageView1];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeImageView:)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        [backView addGestureRecognizer:tap];
        UIWindow * window = [[UIApplication sharedApplication].windows lastObject];
        [window addSubview:backView];
        [LocalService setRBfirstEnterAPP];
    }


    self.navigationController.navigationBarHidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.delegate = self;
    
    self.navTitle = NSLocalizedString(@"R0004",@"活动");
    self.navRightTitle = Icon_bar_filter_ark;
    self.navLeftTitle = Icon_bar_add_ark;
    self.bgIV.hidden = NO;
    self.edgePGR.enabled = NO;
    //
    self.campaignView = [[RBCampaignView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-self.tabBarController.tabBar.height)];
    self.campaignView.delegate = self;
    [self.view addSubview:self.campaignView];
    //
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserVersionUpdate];
    //
    [self.hudView show];
    statusArray = @[@"all",@"running",@"approved",@"verifying",@"completed",@"missed"];
    titlesArray = @[NSLocalizedString(@"R2002",@"全部"),NSLocalizedString(@"R2003",@"新活动"),NSLocalizedString(@"R2004",@"进行中"),NSLocalizedString(@"R2005",@"待审核"),NSLocalizedString(@"R2006",@"已完成"),NSLocalizedString(@"R2007",@"已错失")];
    self.campaignView.status = @"all";
    if ([LocalService getRBLocalEmail].length > 0 && [LocalService getRBLocalDataUserPrivateToken]!=nil && [LocalService getRBLocalDataUserLoginId] != nil) {
        [self.campaignView RBCampaignViewLoadRefreshViewFirstData];
    }else if ([LocalService getRBLocalDataUserPrivateToken]!=nil && [LocalService getRBLocalDataUserLoginPhone].length>0 && [LocalService getRBLocalDataUserLoginId] != nil) {
        [self.campaignView RBCampaignViewLoadRefreshViewFirstData];
    }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(jumpToFindVC) name:@"jumpToFind" object:nil];
}
- (void)jumpToFindVC{
    self.tabBarController.selectedIndex = 2;
}
- (void)RBNotificationCampaignAddCover{
        UIView * backView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        backView.userInteractionEnabled = YES;
        backView.tag = 2000;
        backView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        //
        UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, NavHeight + ScreenWidth * 7/15, ScreenWidth, (ScreenWidth*9.0)/16.0+5.0)];
        imageView.image = [UIImage imageNamed:@"RBCampaignCover"];
        [backView addSubview:imageView];
        imageView.userInteractionEnabled = YES;
        //
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(NewCampaginAction:)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTapsRequired = 1;
        [imageView addGestureRecognizer:tap];
        //
        UIImageView * markImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth - 94)/2 - 30 , imageView.top - 89, 94, 89)];
        markImageView.image = [UIImage imageNamed:@"RBcover5"];
        [backView addSubview:markImageView];
        //
        UIImageView * wordImageView = [[UIImageView alloc]initWithFrame:CGRectMake(markImageView.right, markImageView.top - 28, 75, 56)];
        wordImageView.image = [UIImage imageNamed:@"RBCoverClick"];
        [backView addSubview:wordImageView];
        //
        UIWindow * window = [[UIApplication sharedApplication].windows lastObject];
        if ([window viewWithTag:2000]) {
            UIView * coverView = [window viewWithTag:2000];
            [coverView removeFromSuperview];
        }
        [window addSubview:backView];
}
- (void)NewCampaginAction:(UITapGestureRecognizer*)tap{
    //
    RBFirstDoCampaignController * toview = [[RBFirstDoCampaignController alloc]init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
    //
    UIWindow * window = [[UIApplication sharedApplication].windows lastObject];
    UIView * backView = [window viewWithTag:2000];
    [backView removeFromSuperview];
    
}
- (NSTimer *)shareTimer{
    if (timer == nil) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:UITrackingRunLoopMode];
    }
    return timer;
}
- (void)timeChange{
    [self.campaignView.tableviewn reloadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
 //    判断是否申请成为KOL
    if (isbegin == YES) {
        [self shareTimer];
    }
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
   
    if ([LocalService getRBLocalDataUserPrivateToken]!=nil &&[LocalService getRBLocalDataUserLoginId] != nil) {
        [handler getRBUserInfoProfile];
    }else{
        [handler getRBUserLoginWithPhone:@"13000000000" andCode:@"123456" andInvteCode:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageBegin:@"campaign-list"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [pageFlowView stopTimer];
    [timer invalidate];
    timer = nil;
    [TalkingData trackPageEnd:@"campaign-list"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationStatusBarAction {
    CGPoint off = self.campaignView.tableviewn.contentOffset;
    off.y = 0 - self.campaignView.tableviewn.contentInset.top;
    [self.campaignView.tableviewn setContentOffset:off animated:YES];
}

- (void)RBNotificationRefreshHomeView {
    if([self isCurrentVCVisible:self]==NO) {
        [self RBCampaignViewLoadRefreshViewData:nil];
    }
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    if([self isVisitorLogin]==YES){
        return;
    };
    //
    if ([LocalService getRBIsIncheck] == NO) {
        if (dashboardEntity.brand_campany_name.length == 0) {
            RBNewAlert * alert = [[RBNewAlert alloc]init];
            alert.delegate = self;
            [alert showWithArray:@[@"请先完善品牌主信息",@"取消",@"去完善"] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
            UIButton * btn = [alert.bgView viewWithTag:1000];
            btn.backgroundColor = [Utils getUIColorWithHexString:SysColorbackgray];
            [btn setTitleColor:[Utils getUIColorWithHexString: @"777777"] forState:UIControlStateNormal];
            UIButton * btn1 = [alert.bgView viewWithTag:1001];
            btn1.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
            [btn1 setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
            return;
        }
        RBAdvertiserAddViewController *toview = [[RBAdvertiserAddViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
    
}
#pragma mark - RBNewAlertDelegate
- (void)RBNewAlert:(RBNewAlert *)alertView clickedButtonAtIndex:(int)buttonIndex{
    if (buttonIndex == 1) {
        RBBrandMessageController*toview = [[RBBrandMessageController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
}
- (void)RBNavRightBtnAction {
    int temp = 0;
    for (int i=0; i<[statusArray count]; i++) {
        if ([[NSString stringWithFormat:@"%@",self.campaignView.status] isEqualToString:[NSString stringWithFormat:@"%@",statusArray[i]]]) {
            temp = i;
            break;
        }
    }
    RBSelectView*selectView = [[RBSelectView alloc]init];
    selectView.delegate = self;
    [selectView showWithData:titlesArray andHeight:44.0*[statusArray count] andSelected:temp];
}

#pragma mark - RBSelectView Delegate
- (void)RBSelectViewSelected:(RBSelectView *)view andIndex:(int)index {
    self.navTitle = titlesArray[index];
    self.campaignView.status = statusArray[index];
    [self.hudView show];
    [self.campaignView RBCampaignViewLoadRefreshViewFirstData];
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if (alertView.tag==8000) {
        if (buttonIndex==0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1085946339"]];
        }
    }
}

#pragma mark - RBCampaignView Delegate
-(void)RBCampaignViewLoadRefreshViewData:(RBCampaignView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityWithStatus:self.campaignView.status andTitle:nil andWith_message:nil andPage:[NSString stringWithFormat:@"%d",self.campaignView.pageIndex+1]];
}

- (void)RBCampaignViewSelected:(RBCampaignView *)view andIndex:(int)index {
    RBActivityEntity*entity = view.datalist[index];
    NSString*per_budget_type = [NSString stringWithFormat:@"%@",entity.campaign.per_budget_type];
    NSString*per_action_type = [NSString stringWithFormat:@"%@",entity.campaign.per_action_type];
    if ([per_budget_type isEqualToString:@"recruit"]) {
        if([per_action_type isEqualToString:@"weibo"]||[per_action_type isEqualToString:@"wechat"]||[per_action_type isEqualToString:@"qq"]) {
            RBCampaignRecruitAutoViewController*toview = [[RBCampaignRecruitAutoViewController alloc] init];
            toview.campaignId = entity.campaign.iid;
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        } else {
            RBCampaignRecruitViewController*toview = [[RBCampaignRecruitViewController alloc] init];
            toview.campaignId = entity.campaign.iid;
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }
    } else if ([per_budget_type isEqualToString:@"invite"]) {
        RBCampaignInviteViewController*toview = [[RBCampaignInviteViewController alloc] init];
        toview.campaignId = entity.campaign.iid;
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    } else {
        RBCampaignDetailViewController*toview = [[RBCampaignDetailViewController alloc] init];
        toview.campaignId = entity.campaign.iid;
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
}
- (void)RBCampaignViewBannerSelected:(NSInteger)index{
    if ([LocalService getRBIsIncheck] == YES) {
        return;
    }
    if([self isVisitorLogin]==YES) {
        return;
    };
    RBActivityBannerEntity * entity = bannerlist[index];
    if ([entity.detail_type isEqualToString:@"invite_friend"]) {
        RBUserInviteViewController*toview = [[RBUserInviteViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }else if ([entity.detail_type isEqualToString:@"check_in"]){
        RBUserSignViewController*toview = [[RBUserSignViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        toview.avatar = dashboardEntity.avatar_url;
        toview.name = dashboardEntity.name;
        [self.navigationController pushViewController:toview animated:YES];
    }else if ([entity.detail_type isEqualToString:@"complete_info"]){
        RBKolApplyInfoViewController *toview = [[RBKolApplyInfoViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }else if ([entity.detail_type isEqualToString:@"indiana"]){
        RBUserIndianaViewController*toview = [[RBUserIndianaViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
    Handler * handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBBannerClick:entity.bannerID andparams_json:nil];
}
#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==alertView.cancelButtonIndex) {
        return;
    } else {
        RBKolApplyInfoViewController *toview = [[RBKolApplyInfoViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
        return;
    }
}

#pragma mark - SDCycleScrollView Delegate
//- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
//    if ([LocalService getRBIsIncheck] == YES) {
//        return;
//    }
//    if([self isVisitorLogin]==YES) {
//        return;
//    };
//    RBActivityBannerEntity * entity = bannerlist[index];
//    if ([entity.detail_type isEqualToString:@"invite_friend"]) {
//        RBUserInviteViewController*toview = [[RBUserInviteViewController alloc] init];
//        toview.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:toview animated:YES];
//    }else if ([entity.detail_type isEqualToString:@"check_in"]){
//        RBUserSignViewController*toview = [[RBUserSignViewController alloc] init];
//        toview.hidesBottomBarWhenPushed = YES;
//        toview.avatar = dashboardEntity.avatar_url;
//        toview.name = dashboardEntity.name;
//        [self.navigationController pushViewController:toview animated:YES];
//    }else if ([entity.detail_type isEqualToString:@"complete_info"]){
//        RBKolApplyInfoViewController *toview = [[RBKolApplyInfoViewController alloc] init];
//        toview.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:toview animated:YES];
//    }else if ([entity.detail_type isEqualToString:@"indiana"]){
//        RBUserIndianaViewController*toview = [[RBUserIndianaViewController alloc] init];
//        toview.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:toview animated:YES];
//    }
//    Handler * handler = [Handler shareHandler];
//    handler.delegate = self;
//    [handler getRBBannerClick:entity.bannerID andparams_json:nil];
//}
#pragma mark NewPagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView{
    return CGSizeMake(316*ScaleWidth, 179*ScaleHeight);
}
-(void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex{
    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
}
-(void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView{
    NSLog(@"ViewController 滚动到了第%ld页",(long)pageNumber);
}
#pragma mark NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    return bannerlist.count;
}

- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    PGIndexBannerSubiew * bannerSubView = [flowView dequeueReusableCell];
    if (!bannerSubView) {
        bannerSubView = [[PGIndexBannerSubiew alloc] init];
        bannerSubView.tag = index;
        bannerSubView.layer.cornerRadius = 4;
        bannerSubView.layer.masksToBounds = YES;
    }
    //在这里下载网络图片
    //  [bannerView.mainImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:hostUrlsImg,imageDict[@"img"]]] placeholderImage:[UIImage imageNamed:@""]];
    bannerSubView.mainImageView.image = [UIImage imageNamed:bannerlist[index]];
    
    return bannerSubView;
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"my/show"]) {
        dashboardEntity = [JsonService getRBKolDashboardDetailEntity:jsonObject];
        if (dashboardEntity.detail.intValue == 22) {
            [LocalService setRBIsInCheck:@"yes"];
            self.navLeftTitle = @"";
            self.navLeftBtn.enabled = NO;
            [self.navLeftLabel removeFromSuperview];
        }else{
            [LocalService setRBIsInCheck:nil];
        }
    }
    if ([sender isEqualToString:@"announcements/click"]) {
        [self.hudView dismiss];
    }
    if ([sender isEqualToString:@"campaign_invites"]) {
        [self.hudView dismiss];
        NSArray * arr = [jsonObject objectForKey:@"announcements"];
       
        for (NSInteger i = 0; i<arr.count; i ++) {
            NSLog(@"%@",[arr[i] objectForKey:@"title"]);
        }
        bannerlist = [NSMutableArray new];
        bannerlist = [JsonService getRBActivityBannerEntity:jsonObject andBackArray:bannerlist];
        NSMutableArray * imgArr = [[NSMutableArray alloc]init];
        for (NSInteger i = 0; i < bannerlist.count; i ++) {
            RBActivityBannerEntity * entity = bannerlist[i];
            [imgArr addObject:entity.banner_url];
        }
        self.campaignView.imgList = imgArr;
        if (self.campaignView.pageIndex == 0) {
            self.campaignView.datalist = [NSMutableArray new];
            self.campaignView.jsonObject = jsonObject;
        }
        if ([self.campaignView.datalist count]%40==0) {
            YYImageCache *cache = [YYWebImageManager sharedManager].cache;
            [cache.memoryCache removeAllObjects];
            [cache.diskCache removeAllObjects];
        }
        self.campaignView.datalist = [JsonService getRBActivityListEntity:jsonObject andBackArray:self.campaignView.datalist];
        isbegin = NO;
        for (int i = 0; i < self.campaignView.datalist.count; i ++) {
            RBActivityEntity * entity = self.campaignView.datalist[i];
            if ([entity.campaign.status isEqualToString:@"countdown"]) {
                isbegin = YES;
            }
        }
        if (isbegin == YES) {
            [self shareTimer];
        }
        [self.campaignView RBCampaignViewSetRefreshViewFinish];
        if ([self.campaignView.datalist count] == 0) {
            self.campaignView.defaultView.hidden = NO;
        } else {
            self.campaignView.defaultView.hidden = YES;
        }
    }
    if ([sender isEqualToString:@"kols/sign_in"]) {
        [self.hudView dismiss];
        [GeTuiSdk setPushModeForOff:NO];
        [JsonService setRBUserEntityWith:jsonObject];
        RBUserEntity*userEntity = [JsonService getRBUserEntity:jsonObject];
        [LocalService setRBLocalDataUserLoginIdWith:userEntity.iid];
        [LocalService setRBLocalDataUserLoginNameWith:userEntity.name];
        [LocalService setRBLocalDataUserLoginPhoneWith:userEntity.mobile_number];
        [LocalService setRBLocalDataUserLoginAvatarWith:userEntity.avatar_url];
        [LocalService setRBLocalDataUserPrivateTokenWith:userEntity.issue_token];
        [LocalService setRBLocalDataUserLoginKolIdWith:userEntity.kol_uuid];
        // 离线数据
        [Utils getFileDeleteWithFileName:@"RBCampaign" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBKol" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBKolNew" andFilePath:@"RB"];
        [Utils getFileDeleteWithFileName:@"RBUser" andFilePath:@"RB"];
//        bannerlist = [NSMutableArray new];
//        //申请成为KOL
//        [bannerlist addObject:[NSString stringWithFormat:@"pic_kol_banner_01.jpg"]];
//        //发布悬赏活动
//        [bannerlist addObject:[NSString stringWithFormat:@"pic_kol_banner_1.jpg"]];
//        bannerBgView = nil;
//        bannerBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth * 2/5 )];
//        //
//        SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:bannerBgView.bounds shouldInfiniteLoop:YES imageNamesGroup:bannerlist];
//        cycleScrollView.delegate = self;
//        cycleScrollView.autoScrollTimeInterval = 5.0;
//        cycleScrollView.currentPageDotImage = [UIImage imageNamed:@"RBCircleSelec"];
//        cycleScrollView.pageDotImage = [UIImage imageNamed:@"RBCircleUnSelect"];
//        [bannerBgView addSubview:cycleScrollView];
//        [self.campaignView.tableviewn setTableHeaderView:bannerBgView];
        [self.campaignView RBCampaignViewLoadRefreshViewFirstData];
    }
    //
    if ([sender isEqualToString:@"check"]) {
        if ([[jsonObject objectForKey:@"had_upgrade"]intValue] == 1) {
            NSString * str = [NSString stringWithFormat:@"%@",[[jsonObject objectForKey:@"newest_version"]objectForKey:@"release_note"]];
//            UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//            }];
            UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/cn/app/you-meng-you/id1085946339?mt=8"]];
            }];
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"提示" message:str preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:action1];
//            [alert addAction:action2];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }

}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self.campaignView RBCampaignViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.campaignView RBCampaignViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
