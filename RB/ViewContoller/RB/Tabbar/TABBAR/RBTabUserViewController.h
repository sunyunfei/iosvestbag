//
//  RBTabUserViewController.h
//  RB
//
//  Created by AngusNi on 5/9/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
//
#import "RBUserBtn.h"
#import "RBTabUserTableViewCell.h"
//
#import "RBUserCampaignViewController.h"
#import "RBUserIncomeViewController.h"
#import "RBUserMessageViewController.h"
#import "RBUserSettingViewController.h"
#import "RBUserIndianaViewController.h"
#import "RBUserInviteViewController.h"
#import "RBUserSignViewController.h"
#import "RBAdvertiserMyViewController.h"
#import "RBUserHelpViewController.h"
#import "RBKolApplyInfoViewController.h"
#import "RBUserFollowersViewController.h"

@interface RBTabUserViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
@property(nonatomic, strong) id jsonObject;

@end
