//
//  RBTabUserViewController.m
//  RB
//
//  Created by AngusNi on 5/9/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBTabUserViewController.h"
#import "RBLoginViewController.h"
#import "RBInviteCodeViewController.h"
#import "RBMyCollectViewController.h"
@interface RBTabUserViewController (){
    UITableView*tableviewn;
    NSMutableArray*datalist;
    UIImageView*headBgIV;
    RBUserDashboardEntity*dashboardEntity;
    UILabel * newMsgLabel;
    int reviewValue;
}
@end

@implementation RBTabUserViewController
-(void)removeImageView:(UITapGestureRecognizer*)tap{
    UIImageView * imageView = (UIImageView*)tap.view;
    [imageView removeFromSuperview];
}
- (void)RBNotificationBackToHome{
    self.tabBarController.selectedIndex = 0;
}
- (void)jumpToFindVC{
    self.tabBarController.selectedIndex = 2;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
    //
    reviewValue = 22;
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-self.tabBarController.tabBar.height)
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    tableviewn.showsHorizontalScrollIndicator = NO;
    tableviewn.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableviewn];
    [self loadTableHeaderView];
    if (@available(iOS 11.0,*)) {
        tableviewn.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    //
    self.navTitle = NSLocalizedString(@"R0003",@"我的");
    
    self.navRightTitle = Icon_bar_setting;
    self.edgePGR.enabled = NO;
    self.bgIV.hidden = NO;
//    if ([LocalService getRBIsIncheck] == YES) {
//        self.navLeftTitle = @"";
//        [self.navLeftLabel removeFromSuperview];
//        self.navLeftBtn.enabled = NO;
//    }else{
        self.navLeftTitle = Icon_bar_msg;
//    }
    self.navTitleLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.9];
    self.navLeftLabel.textColor = self.navTitleLabel.textColor;
    self.navRightLabel.textColor = self.navTitleLabel.textColor;
    self.navView.backgroundColor = [UIColor clearColor];
    self.navLineLabel.backgroundColor = [UIColor clearColor];
    newMsgLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.navLeftLabel.right - 5, self.navLeftLabel.top - 1, 8, 8) text:@"" font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[UIColor redColor] numberOfLines:1];
    newMsgLabel.hidden = YES;
    newMsgLabel.layer.cornerRadius = 5.0;
    newMsgLabel.layer.masksToBounds = YES;
    [self.navLeftBtn addSubview:newMsgLabel];
    
    datalist = [NSMutableArray arrayWithArray: @[@[NSLocalizedString(@"R5030", @"我的钱包"),NSLocalizedString(@"R5120",@"我的活动"),NSLocalizedString(@"R5251",@"我的收藏")],@[NSLocalizedString(@"R5019", @"品牌主")],@[NSLocalizedString(@"R5132",@"一元夺宝"),NSLocalizedString(@"R5253", @"每日任务"),NSLocalizedString(@"R5243", @"收徒赚收益"),NSLocalizedString(@"R5218", @"输入邀请码")],@[NSLocalizedString(@"R5107", @"在线客服"),NSLocalizedString(@"R7036",@"帮助中心")]]];
    //
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(jumpToFindVC) name:@"jumpToFind" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
    //
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    if ([JsonService isRBUserVisitor]) {
        [handler getRBUserInfoProfile];
        [handler getRBUserInfoUpdateLongitudeLatitude];
        return;
    }else if ([LocalService getRBLocalDataUserPrivateToken]!=nil && [LocalService getRBLocalDataUserLoginPhone].length>0){
        [handler getRBUserInfoProfile];
        [handler getRBUserInfoUpdateLongitudeLatitude];
        return;
    }else if([LocalService getRBLocalEmail].length > 0 && [LocalService getRBLocalDataUserPrivateToken] != nil){
        [handler getRBUserInfoProfile];
        [handler getRBUserInfoUpdateLongitudeLatitude];
         //   [self loadTableHeaderView];
        return;
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationShowLoginView
                                                            object:self
                                                          userInfo:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:@"my"];
    //
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"my"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationDismissView {
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    if([self isVisitorLogin]==YES) {
        return;
    };
    //
    RBUserMessageViewController *toview = [[RBUserMessageViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)RBNavRightBtnAction {
    if([self isVisitorLogin]==YES){
        return;
    };
    //
    RBUserSettingViewController *toview = [[RBUserSettingViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)applyBtnAction:(UIButton *)sender {
    if([self isVisitorLogin]==YES){
        return;
    };
    if([LocalService getRBLocalDataUserPrivateToken] == nil && [JsonService isRBUserVisitor] == NO){
        RBLoginViewController*toview = [[RBLoginViewController alloc]init];
        //        toview.hidesBottomBarWhenPushed = YES;
        //        [self.navigationController pushViewController:toview animated:YES];
        toview.isPush = NO;
        [self presentVC:toview];
        return;
    }
    if ([dashboardEntity.role_apply_status isEqualToString:@"rejected"]) {
        UIAlertView*alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"R7034",@"KOL资质审核拒绝") message:dashboardEntity.role_check_remark delegate:self cancelButtonTitle:NSLocalizedString(@"R1011", @"取消") otherButtonTitles:NSLocalizedString(@"R7035",@"重新申请成为KOL"), nil];
        [alertView show];
        return;
    }
    RBKolApplyInfoViewController *toview = [[RBKolApplyInfoViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat y = scrollView.contentOffset.y;
    if (y < -(ScreenWidth/375.0)*248.0) {
        CGRect frame = headBgIV.frame;
        frame.origin.y = y;
        frame.size.height = -y;
        headBgIV.frame = frame;
    }
    if(scrollView.contentOffset.y < 60-(ScreenWidth/375.0)*248.0) {
        self.navView.backgroundColor = [UIColor clearColor];
    } else {
        self.navView.backgroundColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.3];
    }
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==alertView.cancelButtonIndex) {
        return;
    } else {
        RBKolApplyInfoViewController *toview = [[RBKolApplyInfoViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
        return;
    }
}

#pragma mark - loadTableHeaderView Delegate
- (void)loadTableHeaderView {
    if (dashboardEntity==nil) {
        self.jsonObject = [Utils getFileReadWithFileName:@"RBUser" andFilePath:@"RB"];
        dashboardEntity = [JsonService getRBKolDashboardDetailEntity:self.jsonObject];
        if ([NSString stringWithFormat:@"%@",dashboardEntity.max_campaign_click].length==0) {
            dashboardEntity.max_campaign_click = @"0";
        }
        if ([NSString stringWithFormat:@"%@",dashboardEntity.campaign_total_income].length==0) {
            dashboardEntity.campaign_total_income = @"0";
        }
        if ([NSString stringWithFormat:@"%@",dashboardEntity.max_campaign_earn_money].length==0) {
            dashboardEntity.max_campaign_earn_money = @"0";
        }
        if ([NSString stringWithFormat:@"%@",dashboardEntity.avg_campaign_credit].length==0) {
            dashboardEntity.avg_campaign_credit = @"0";
        }
        dashboardEntity.max_campaign_click = [Utils getNSStringTwoFloat:dashboardEntity.max_campaign_click];
        dashboardEntity.campaign_total_income = [Utils getNSStringTwoFloat:dashboardEntity.campaign_total_income];
        dashboardEntity.max_campaign_earn_money = [Utils getNSStringTwoFloat:dashboardEntity.max_campaign_earn_money];
        dashboardEntity.avg_campaign_credit = [Utils getNSStringTwoFloat:dashboardEntity.avg_campaign_credit];
    } else if(self.jsonObject){
        [Utils getFileWrite:self.jsonObject andFileName:@"RBUser" andFilePath:@"RB"];
    }else{
        
    }
    if ([dashboardEntity.has_any_unread_message isEqualToString:@"1"]) {
            newMsgLabel.hidden = NO;
    }else{
        newMsgLabel.hidden = YES;
    }
    [headBgIV removeFromSuperview];

    tableviewn.contentInset = UIEdgeInsetsMake((ScreenWidth/375.0)*248.0, 0, 0, 0);
    headBgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, -(ScreenWidth/375.0)*248.0, ScreenWidth, (ScreenWidth/375.0)*248.0)];
    headBgIV.image = [UIImage imageNamed:@"pic_user_bg.jpg"];
    headBgIV.userInteractionEnabled = YES;
    headBgIV.contentMode = UIViewContentModeScaleAspectFill;
    headBgIV.autoresizesSubviews = YES;
    [tableviewn addSubview:headBgIV];
    //
    UIButton*userBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [userBtn addTarget:self
                action:@selector(applyBtnAction:)
      forControlEvents:UIControlEventTouchUpInside];
    userBtn.frame = CGRectMake(0, NavHeight, ScreenWidth, 100.0);
    [headBgIV addSubview:userBtn];
    //
    float gap = ((ScreenWidth/375.0)*248.0-75.0*(ScreenWidth/375.0)-60.0-60.0)/2.0;
    //
    UIImageView*userIV = [[UIImageView alloc]initWithFrame:CGRectMake(CellLeft, 60.0+gap, 60.0, 60.0)];
    userIV.layer.masksToBounds = YES;
    userIV.layer.cornerRadius = userIV.width/2.0;
    [headBgIV addSubview:userIV];
    //
    UILabel*userLabel = [[UILabel alloc]initWithFrame:CGRectMake(userIV.right+CellBottom, userIV.top - 2.5, 150.0, 30)];
    userLabel.font = font_cu_17;
    userLabel.textColor = [Utils getUIColorWithHexString:@"ffffff" andAlpha:0.9];
    [headBgIV addSubview:userLabel];
    //
    UILabel*userCLabel = [[UILabel alloc]initWithFrame:CGRectMake(userLabel.left, userLabel.bottom + 2.5, 170.0, 30)];
    userCLabel.font = font_cu_13;
    userCLabel.textColor = [Utils getUIColorWithHexString:@"ffffff" andAlpha:0.9];
    [headBgIV addSubview:userCLabel];
    //
    
    if ([JsonService isRBUserVisitor] && [LocalService getRBLocalEmail].length <= 0) {
        userIV.image = PlaceHolderUserImage;
        userLabel.text = NSLocalizedString(@"R7038",@"点击登录");
        userCLabel.text = NSLocalizedString(@"R7039",@"登录获取更多活动~");
    } else if(dashboardEntity != nil && dashboardEntity.name.length>0){
        [userIV sd_setImageWithURL:[NSURL URLWithString:dashboardEntity.avatar_url] placeholderImage:PlaceHolderUserImage];
        userCLabel.text = @"";
        for (int i=0; i< [dashboardEntity.tags count]; i++) {
            RBTagEntity*entity = dashboardEntity.tags[i];
            if(i==0) {
                userCLabel.text = [NSString stringWithFormat:@"%@",entity.label];
            } else {
                userCLabel.text = [NSString stringWithFormat:@"%@/%@",userCLabel.text,entity.label];
            }
        }
        if(userCLabel.text.length==0) {
            userCLabel.text = NSLocalizedString(@"R7037",@"申请成为KOL获取更多活动~");
        }
        userLabel.text = dashboardEntity.name;
    }else{
        userIV.image = PlaceHolderUserImage;
        userLabel.text = NSLocalizedString(@"R7038",@"点击登录");
        userCLabel.text = NSLocalizedString(@"R7039",@"登录获取更多活动~");
    }
    
    //
    CGSize userLabelSize = [Utils getUIFontSizeFitW:userLabel];
    if(userLabelSize.width>ScreenWidth-userLabel.left-50.0-17){
        userLabel.width = ScreenWidth-userLabel.left-50.0-17;
    } else {
        userLabel.width = userLabelSize.width;
    }
    userCLabel.width = ScreenWidth-80.0-CellLeft-userLabel.left;
    //
    UIImageView*userVIV = [[UIImageView alloc]initWithFrame:CGRectMake(userLabel.right+2.0, userLabel.top+5.0, 50.0, 19.0)];
    userVIV.image = [UIImage imageNamed:@"icon_user_v.png"];
    [headBgIV addSubview:userVIV];
    //
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(userVIV.right + 5, userVIV.top, 17, 19)];
    if (dashboardEntity.logo.length > 0) {
        [logoImageView sd_setImageWithURL:[NSURL URLWithString:dashboardEntity.logo]];
        logoImageView.hidden = NO;
    }else{
        logoImageView.hidden = YES;
    }
    [headBgIV addSubview:logoImageView];
    //
    UIButton*applyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [applyBtn addTarget:self
                 action:@selector(applyBtnAction:)
       forControlEvents:UIControlEventTouchUpInside];
    applyBtn.frame = CGRectMake(ScreenWidth-80.0-CellLeft, userCLabel.top, 80.0, 25.0);
    applyBtn.titleLabel.font = font_11;
    applyBtn.layer.cornerRadius = applyBtn.height/2.0;
    applyBtn.layer.borderWidth = 1;
    applyBtn.layer.masksToBounds = YES;
    applyBtn.centerY = userCLabel.centerY;
    [headBgIV addSubview:applyBtn];
    //
    [applyBtn setTitleColor:[Utils getUIColorWithHexString:SysColorYellow] forState:UIControlStateNormal];
    applyBtn.layer.borderColor = [Utils getUIColorWithHexString:SysColorYellow].CGColor;
    [applyBtn setTitle:NSLocalizedString(@"R7048",@"申请成为KOL") forState:UIControlStateNormal];
    
    if ([dashboardEntity.role_apply_status isEqualToString:@"passed"]) {
         userVIV.image = [UIImage imageNamed:@"icon_user_v_on.png"];
        if([dashboardEntity.kol_role isEqualToString:@"big_v"]) {
            [applyBtn setTitle:NSLocalizedString(@"R7040",@"编辑KOL资料") forState:UIControlStateNormal];
        }
    }
    if ([dashboardEntity.role_apply_status isEqualToString:@"applying"]) {
        [applyBtn setTitle:NSLocalizedString(@"R7041",@"资料审核中") forState:UIControlStateNormal];
        if([dashboardEntity.kol_role isEqualToString:@"big_v"]) {
            userVIV.image = [UIImage imageNamed:@"icon_user_v_on.png"];
            [applyBtn setTitle:NSLocalizedString(@"R7042",@"重新审核中") forState:UIControlStateNormal];
        }
    }
    if ([dashboardEntity.role_apply_status isEqualToString:@"rejected"]) {
        [applyBtn setTitle:NSLocalizedString(@"R7043",@"资料审核拒绝") forState:UIControlStateNormal];
        [applyBtn setTitleColor:[Utils getUIColorWithHexString:SysColorRed] forState:UIControlStateNormal];
        applyBtn.layer.borderColor = [Utils getUIColorWithHexString:SysColorRed].CGColor;
    }
    if ([JsonService isRBUserVisitor]) {
        [applyBtn setTitle:NSLocalizedString(@"R7048",@"申请成为KOL") forState:UIControlStateNormal];
        userVIV.image = [UIImage imageNamed:@"icon_user_v.png"];
    }
    //
    UILabel*userLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, userIV.bottom+gap, headBgIV.width, 0.5f)];
    userLineLabel.backgroundColor = [Utils getUIColorWithHexString:@"505050"];
    [headBgIV addSubview:userLineLabel];
    //
    UIView*userDataView = [[UIView alloc]initWithFrame:CGRectMake(0, userLineLabel.bottom, ScreenWidth, 75.0*(ScreenWidth/375.0))];
    [headBgIV addSubview:userDataView];
    NSArray*dataArray = @[[NSString stringWithFormat:@"%@",dashboardEntity.max_campaign_click],[NSString stringWithFormat:@"%@",dashboardEntity.campaign_total_income],[NSString stringWithFormat:@"%@",dashboardEntity.max_campaign_earn_money],[NSString stringWithFormat:@"%@",dashboardEntity.avg_campaign_credit]];
    if(dashboardEntity==nil) {
        dataArray = @[@"",@"",@"",@""];
    }
    NSArray*tipsArray = @[NSLocalizedString(@"R7044",@"最大点击数"),NSLocalizedString(@"R7045",@"活动总收益"),NSLocalizedString(@"R7046",@"最高单笔收益"),NSLocalizedString(@"R7047",@"平均收益")];
    for (int i=0; i<[tipsArray count]; i++) {
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectMake(i*ScreenWidth/4.0, (userDataView.height-36.0)/2.0, ScreenWidth/4.0, 18.0)];
        tLabel.font = font_(20.0);
        tLabel.text = dataArray[i];
        tLabel.textAlignment = NSTextAlignmentCenter;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.9];
        [userDataView addSubview:tLabel];
        //
        UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectMake(tLabel.left, tLabel.bottom+5.0, tLabel.width, 13.0)];
        cLabel.font = font_cu_11;
        cLabel.text = tipsArray[i];
        cLabel.textAlignment = NSTextAlignmentCenter;
        cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [userDataView addSubview:cLabel];
        if ([LocalService getRBIsIncheck] == YES) {
            tLabel.hidden = YES;
            cLabel.hidden = YES;
        }
    }
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [datalist count];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    NSArray*array = datalist[section];
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CellBottom;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView*tempView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, CellBottom)];
    tempView.backgroundColor = [UIColor clearColor];
    return tempView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBTabUserTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[RBTabUserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:cellIdentifier];
    }
    [cell loadRBTabUserCellWith:datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section==3) {
        if (indexPath.row == 0) {
            self.navigationController.navigationBarHidden = NO;

            RBUserSettingChatViewController *toview = [[RBUserSettingChatViewController alloc] init];
            toview.conversationType = ConversationType_APPSERVICE;
            toview.targetId = RCIMServiceID;
            toview.title = NSLocalizedString(@"R5107", @"在线客服");
            toview.hidesBottomBarWhenPushed = YES;
            toview.navigationController.navigationBarHidden = NO;
            [self.navigationController pushViewController:toview animated:YES];
            return;
        }else{
            RBUserHelpViewController*toview = [[RBUserHelpViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
            return;
        }
    }
    if([self isVisitorLogin]==YES){
        return;
    };
    if (indexPath.section==0) {
        if(indexPath.row==0) {
            if (dashboardEntity.detail.intValue<=reviewValue) {
                RBUserCampaignViewController*toview = [[RBUserCampaignViewController alloc] init];
                toview.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:toview animated:YES];
                return;
            }
            RBUserIncomeViewController*toview = [[RBUserIncomeViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }
        if(indexPath.row==1) {
            if (dashboardEntity.detail.intValue<=reviewValue) {
                RBMyCollectViewController*toview = [[RBMyCollectViewController alloc] init];
                toview.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:toview animated:YES];
                return;
            }
            RBUserCampaignViewController*toview = [[RBUserCampaignViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }
        if(indexPath.row==2) {
            RBMyCollectViewController*toview = [[RBMyCollectViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }
    }
    if(indexPath.section==1) {
        RBAdvertiserMyViewController*toview = [[RBAdvertiserMyViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
    if (indexPath.section==2) {
        // if ([LocalService getRBLocalDataUserAgree].length!=0) {
        if (dashboardEntity.detail.intValue<=reviewValue) {
            RBUserSignViewController*toview = [[RBUserSignViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            toview.avatar = dashboardEntity.avatar_url;
            toview.name = dashboardEntity.name;
            [self.navigationController pushViewController:toview animated:YES];
            return;
        }
        if(indexPath.row==0) {
            RBUserIndianaViewController*toview = [[RBUserIndianaViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }
        if(indexPath.row==1) {
            RBUserSignViewController*toview = [[RBUserSignViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            toview.avatar = dashboardEntity.avatar_url;
            toview.name = dashboardEntity.name;
            [self.navigationController pushViewController:toview animated:YES];
        }
        if(indexPath.row==2) {
            RBUserInviteViewController*toview = [[RBUserInviteViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }
        if (indexPath.row == 3) {
            RBInviteCodeViewController * codeVC = [[RBInviteCodeViewController alloc]init];
            codeVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:codeVC animated:YES];
        }
    }
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"my/show"]) {
        [self.hudView dismiss];
        self.jsonObject = jsonObject;
        dashboardEntity = [JsonService getRBKolDashboardDetailEntity:self.jsonObject];
        if ([NSString stringWithFormat:@"%@",dashboardEntity.max_campaign_click].length==0) {
            dashboardEntity.max_campaign_click = @"0";
        }
        if ([NSString stringWithFormat:@"%@",dashboardEntity.campaign_total_income].length==0) {
            dashboardEntity.campaign_total_income = @"0";
        }
        if ([NSString stringWithFormat:@"%@",dashboardEntity.max_campaign_earn_money].length==0) {
            dashboardEntity.max_campaign_earn_money = @"0";
        }
        if ([NSString stringWithFormat:@"%@",dashboardEntity.avg_campaign_credit].length==0) {
            dashboardEntity.avg_campaign_credit = @"0";
        }
        dashboardEntity.max_campaign_click = [Utils getNSStringTwoFloat:dashboardEntity.max_campaign_click];
        dashboardEntity.campaign_total_income = [Utils getNSStringTwoFloat:dashboardEntity.campaign_total_income];
        dashboardEntity.max_campaign_earn_money = [Utils getNSStringTwoFloat:dashboardEntity.max_campaign_earn_money];
        dashboardEntity.avg_campaign_credit = [Utils getNSStringTwoFloat:dashboardEntity.avg_campaign_credit];
        if (dashboardEntity.detail.intValue<=reviewValue) {
//            [LocalService setRBLocalDataUserHideWith:@"YES"];
            datalist = [NSMutableArray arrayWithArray: @[@[NSLocalizedString(@"R5120",@"我的活动"),NSLocalizedString(@"R5251",@"我的收藏")],@[NSLocalizedString(@"R5019", @"品牌主")],@[NSLocalizedString(@"R5253", @"每日任务")],@[]]];
            
            [tableviewn reloadData];
        }else if(dashboardEntity.detail.intValue>reviewValue){
            if ([dashboardEntity.is_show_invite_code isEqualToString:@"0"]) {
                datalist = [NSMutableArray arrayWithArray: @[@[NSLocalizedString(@"R5030", @"我的钱包"),NSLocalizedString(@"R5120",@"我的活动"),NSLocalizedString(@"R5251",@"我的收藏")],@[NSLocalizedString(@"R5019", @"品牌主")],@[NSLocalizedString(@"R5132",@"一元购"),NSLocalizedString(@"R5253", @"每日任务"),NSLocalizedString(@"R5243", @"收徒赚收益")],@[NSLocalizedString(@"R5107", @"在线客服"),NSLocalizedString(@"R7036",@"帮助中心")]]];
            }else{
                datalist = [NSMutableArray arrayWithArray: @[@[NSLocalizedString(@"R5030", @"我的钱包"),NSLocalizedString(@"R5120",@"我的活动"),NSLocalizedString(@"R5251",@"我的收藏")],@[NSLocalizedString(@"R5019", @"品牌主")],@[NSLocalizedString(@"R5132",@"一元购"),NSLocalizedString(@"R5253", @"每日任务"),NSLocalizedString(@"R5243", @"收徒赚收益"),NSLocalizedString(@"R5218", @"输入邀请码")],@[NSLocalizedString(@"R5107", @"在线客服"),NSLocalizedString(@"R7036",@"帮助中心")]]];
            }
            [tableviewn reloadData];
        }
        [self loadTableHeaderView];
        if ([LocalService getRBfirstEnterMyInterface] == NO && [LocalService getRBIsIncheck] == NO) {
            UIImageView * imageView = [[UIImageView alloc]initWithFrame:[UIScreen mainScreen].bounds];
            imageView.image = [UIImage imageNamed:@"RBMyInterface"];
            imageView.userInteractionEnabled = YES;
            UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeImageView:)];
            tap.numberOfTapsRequired = 1;
            tap.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:tap];
            UIWindow * window = [[UIApplication sharedApplication].windows lastObject];
            [window addSubview:imageView];
            [LocalService setRBfirstEnterMyInterface];
        }

    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    dashboardEntity = nil;
    [self loadTableHeaderView];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    dashboardEntity = nil;
    [self loadTableHeaderView];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
