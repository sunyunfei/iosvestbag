//
//  RBTabKolViewController.m
//  RB
//
//  Created by AngusNi on 7/11/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBTabKolViewController.h"

@interface RBTabKolViewController () {
    CLLocationManager*locationManager;
}
@end

@implementation RBTabKolViewController
-(void)removeImageView:(UITapGestureRecognizer*)tap{
    UIImageView * imageView = (UIImageView*)tap.view;
    [imageView removeFromSuperview];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.edgePGR.enabled = NO;
    self.navRightTitle = Icon_bar_search_ark;
    self.navLeftTitle = Icon_bar_add_ark;
    self.navTitleLabel.hidden = YES;
    self.bgIV.hidden = NO;
    //
    UIImageView*logoIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-76.0)/2.0, StatusHeight+(44.0-17.0)/2.0, 76, 17)];
    logoIV.image = [UIImage imageNamed:@"icon_kol_logo.png"];
    [self.navView addSubview:logoIV];
    //
    self.kolView = [[RBKolView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-self.tabBarController.tabBar.height)];
    self.kolView.delegate = self;
    [self.view addSubview:self.kolView];
    [self.hudView show];
    [self.kolView RBKolViewLoadRefreshViewFirstData];
    //
    [self getGPS];
    // RB-检查更新
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserVersionUpdate];
    // RB-引导页
    if ([[Utils getCurrentBundleShortVersion] isEqualToString:@"1.7.0"]&&[LocalService getRBLocalDataUserVersionShow].length==0) {
        RBGuideView *guideView = [[RBGuideView alloc]init];
        [guideView show];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageBegin:@"kol-list"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"kol-list"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationStatusBarAction {
    [self.kolView.collectionviewn scrollRectToVisible:self.kolView.collectionviewn.bounds animated:YES];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    if([self isVisitorLogin]==YES){
        return;
    };
    RBAdvertiserAddViewController *toview = [[RBAdvertiserAddViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
    
//    NSURL * myURL_APP_A = [NSURL URLWithString:@"weixin://"];
//    if ([[UIApplication sharedApplication] canOpenURL:myURL_APP_A]) {
//        // 跳转朋友圈
////        NSURL * url = [NSURL URLWithString:@"weixin://dl/moments"];
////        [[UIApplication sharedApplication] openURL:url];
//        // 跳转扫一扫
//        NSURL * url2 = [NSURL URLWithString:@"weixin://dl"];
//                        [[UIApplication sharedApplication] openURL:url2];
//                        }
}

- (void)RBNavRightBtnAction {
    [self typeBtnAction:nil];
}

#pragma mark - UIButton Delegate
-(void)typeBtnAction:(UIButton*)sender {
    [KxMenu setTintColor:[Utils getUIColorWithHexString:SysColorWhite]];
    [KxMenu setTitleFont:font_cu_13];
    NSMutableArray *menuItems = [[NSMutableArray alloc]initWithCapacity:20];
    KxMenuItem *item0 = [KxMenuItem menuItem:NSLocalizedString(@"R7052",@"APP用户找KOL") image:nil target:self action:@selector(menuItem0:)];
    item0.alignment = NSTextAlignmentCenter;
    item0.foreColor = [Utils getUIColorWithHexString:SysColorBlack];
    [menuItems addObject:item0];
    KxMenuItem *item1 = [KxMenuItem menuItem:NSLocalizedString(@"R7053",@"搜索引擎找KOL") image:nil target:self action:@selector(menuItem1:)];
    item1.alignment = item0.alignment;
    item1.foreColor = item0.foreColor;
    [menuItems addObject:item1];
    KxMenuItem *item2 = [KxMenuItem menuItem:NSLocalizedString(@"R7054",@"微信公众号榜单") image:nil target:self action:@selector(menuItem2:)];
    item2.alignment = item0.alignment;
    item2.foreColor = item0.foreColor;
    [menuItems addObject:item2];
    [KxMenu showMenuInView:self.view fromRect:CGRectMake(ScreenWidth-65, 0, 85, NavHeight - 9) menuItems:menuItems];
}

#pragma mark - UIButton Delegate
- (void)menuItem0:(KxMenuItem *)sender {
    [self dismissMenu];
    RBKolSearchViewController *toview = [[RBKolSearchViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)menuItem1:(KxMenuItem *)sender {
    [self dismissMenu];
    RBEngineSearchViewController *toview = [[RBEngineSearchViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)menuItem2:(KxMenuItem *)sender {
    [self dismissMenu];
    RBEngineViewController *toview = [[RBEngineViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)dismissMenu {
    [KxMenu dismissMenu];
}

#pragma mark - RBkolView Delegate
-(void)RBKolViewLoadRefreshViewData:(RBKolView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBKOLListWithPage:self.kolView.pageIndex+1 andBanner:@"Y" andCategory:nil andSearch:nil andOrder:@"order_by_hot"];
    [handler getRBKOLListWithPage:1 andBanner:@"Y" andCategory:nil andSearch:nil andOrder:@"order_by_created"];
}

- (void)RBKolViewSelected:(RBKolView *)view andIndex:(int)index {
    RBKOLEntity *entity = view.datalist[index];
    RBKolDetailViewController *toview = [[RBKolDetailViewController alloc] init];
    toview.kolId = entity.iid;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)RBKolViewKolSelected :(RBKolView *)view andIndex:(int)index {
    RBKOLEntity*entity = view.newlist[index];
    RBKolDetailViewController *toview = [[RBKolDetailViewController alloc] init];
    toview.kolId = entity.iid;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)RBKolViewHeadSelected:(RBKolView *)view andIndex:(int)index {
    if (index>=10000) {
        RBKolSearchListViewController *toview = [[RBKolSearchListViewController alloc] init];
        toview.categoryName = view.nameArray[index-10000];
        toview.categoryLabel = view.labelArray[index-10000];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    } else {
        RBKOLBannerEntity*entity = view.headlist[index];
        if([entity.category isEqualToString:@"link"]) {
            TOWebViewController*toview = [[TOWebViewController alloc] init];
            toview.url = [NSURL URLWithString:entity.link];
            toview.title = entity.title;
            toview.showPageTitles = NO;
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }
        if([entity.category isEqualToString:@"kol"]) {
            RBKolDetailViewController *toview = [[RBKolDetailViewController alloc] init];
            toview.kolId = entity.kol_id;
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }
    }
}

#pragma mark - CLLocationManager Delegate
-(void)getGPS {
    if([LocalService getRBLocalDataUserLongitude]==nil) {
        if ([CLLocationManager locationServicesEnabled]) {
            locationManager = [[CLLocationManager alloc] init];
            [locationManager setDelegate:self];
            [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
            locationManager.distanceFilter = 1000.0f;
            [locationManager startUpdatingLocation];
            [locationManager requestWhenInUseAuthorization];
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    CLLocationCoordinate2D loc = [locationManager.location coordinate];
    [LocalService setRBLocalDataUserLatitudeWith:[NSString stringWithFormat:@"%f",loc.latitude] andLongitude:[NSString stringWithFormat:@"%f",loc.longitude]];
    NSLog(@"Latitude:%f,Longitude:%f",loc.latitude,loc.longitude);
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [locationManager stopUpdatingLocation];
    NSLog(@"Location Error");
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"big_v/order_by_created"]) {
        self.kolView.newlist = [NSMutableArray new];
        self.kolView.newlist = [JsonService getRBKolListEntity:jsonObject andBackArray:self.kolView.newlist];
        self.kolView.jsonObjectNew = jsonObject;
        [self.kolView RBKolViewSetRefreshViewFinish];
    }
    
    if ([sender isEqualToString:@"big_v/order_by_hot"]) {
        [self.hudView dismiss];
        if (self.kolView.pageIndex == 0) {
            self.kolView.datalist = [NSMutableArray new];
            self.kolView.jsonObject = jsonObject;
        }
        self.kolView.datalist = [JsonService getRBKolListEntity:jsonObject andBackArray:self.kolView.datalist];
        if ([self.kolView.datalist count]%80==0) {
            YYImageCache *cache = [YYWebImageManager sharedManager].cache;
            [cache.memoryCache removeAllObjects];
            [cache.diskCache removeAllObjects];
        }
        self.kolView.headlist = [NSMutableArray new];
        self.kolView.headlist = [JsonService getRBKolBannerListEntity:jsonObject andBackArray:self.kolView.headlist];
        [self.kolView RBKolViewSetRefreshViewFinish];
    }
    if ([sender isEqualToString:@"check"]) {
        if ([[jsonObject objectForKey:@"had_upgrade"]intValue] == 1) {
            NSString * str = [NSString stringWithFormat:@"%@",[[jsonObject objectForKey:@"newest_version"]objectForKey:@"release_note"]];
            UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            UIAlertAction * action2 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/cn/app/you-meng-you/id1085946339?mt=8"]];
            }];
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"提示" message:str preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:action1];
            [alert addAction:action2];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self.kolView RBKolViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.kolView RBKolViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
