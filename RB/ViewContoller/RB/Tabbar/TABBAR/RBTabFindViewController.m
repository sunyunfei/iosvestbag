//
//  RBTabFindViewController.m
//  RB
//
//  Created by RB8 on 2018/3/27.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBTabFindViewController.h"
#import "NewDynamicsTableViewCell.h"
#import "RBFindDetailViewController.h"
@interface RBTabFindViewController ()<RBBaseVCDelegate,HandlerDelegate,UITableViewDelegate,UITableViewDataSource,NewDynamicsCellDelegate,HandlerDelegate,RBShareViewDelegate,RBFindDetailControllerDelegate,RBRedBagViewDelegate>
@property(nonatomic,strong)UIView * downView;
@property(nonatomic,assign)BOOL isFold;
@property(nonatomic,strong)UITableView * MyTable;
@property(nonatomic,strong)UIButton * navBtn;
@property(nonatomic,strong)UIImageView * pinImageView;
@property(nonatomic,strong)NSArray * labelArray;
//记录最顶部选择的类目
@property(nonatomic,strong)NSString * selectName;
//记录选择的按钮index

@property(nonatomic,assign)NSInteger index;
@property(nonatomic,assign)int pageIndex;
@property(nonatomic,assign)NSIndexPath * KCIndexPath;
@property(nonatomic,strong)NSString * KCType;
@property(nonatomic,strong)UIButton * redBagButton;
@property(nonatomic,strong)NSTimer * timer;
@property(nonatomic,strong)NSString * readmoney;
@property(nonatomic,strong)RBRedBagView * alertBag;
@end

@implementation RBTabFindViewController
- (NSMutableArray *)layoutsArr{
    if (!_layoutsArr) {
        _layoutsArr = [NSMutableArray array];
    }
    return _layoutsArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.hidden = YES;
    self.delegate = self;
    //
    _index = 0;
    _pageIndex = 1;
    _selectName = @"";
    //
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(popRedBag:) name:@"addRedBag" object:nil];
    [self loadData:@""];
    //
    [self loadEditView];
    [self.hudView show];
}
//弹出红包
- (void)popRedBag:(NSNotification*)notification{
    if (notification.userInfo == nil && _redBagButton != nil) {
        [_redBagButton removeFromSuperview];
        _redBagButton = nil;
        [_timer invalidate];
        _timer = nil;
    }else if(notification.userInfo != nil){
        if (!_redBagButton) {
            NSNumber * redMoney = [notification.userInfo objectForKey:@"readmoney"];
            _readmoney = [NSString stringWithFormat:@"%.2f",[redMoney floatValue]];
            //红包按钮
            _redBagButton = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth, self.tabBarController.tabBar.top - 12 - 51, 55, 51) title:nil hlTitle:nil titleColor:nil hlTitleColor:nil backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"RBredBag"] hlImage:nil];
            [_redBagButton addTarget:self action:@selector(getRedBag:) forControlEvents:UIControlEventTouchUpInside];
            
            UIWindow * window = [[UIApplication sharedApplication].windows lastObject];
            [window addSubview:_redBagButton];
            //
            [UIView animateWithDuration:0.5 animations:^{
                _redBagButton.frame = CGRectMake(ScreenWidth - 23 - 55, self.tabBarController.tabBar.top - 12 -51, 55, 51);
            }];
            _timer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(pauseAnimate) userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        }
    }
}
//
- (void)loadData:(NSString*)type{
    Handler * hand = [Handler shareHandler];
    hand.delegate = self;
    [hand getRBArticleslist:type andPage:self.pageIndex];
}
//
- (void)setRefreshHeaderView{
    __weak typeof(self) weakSelf = self;
    self.MyTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.pageIndex = 1;
        [weakSelf loadData:weakSelf.selectName];
    }];
}
- (void)setRefreshFooterView{
    __weak typeof(self) weakSelf = self;
    self.MyTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.pageIndex = weakSelf.pageIndex + 1;
        [weakSelf loadData:weakSelf.selectName];
    }];
}
//
- (void)loadEditView{
    _MyTable = [[UITableView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight - NavHeight - self.tabBarController.tabBar.height) style:UITableViewStylePlain];
    _MyTable.delegate = self;
    _MyTable.dataSource = self;
    _MyTable.showsVerticalScrollIndicator = NO;
    _MyTable.showsHorizontalScrollIndicator = NO;
    _MyTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self setRefreshHeaderView];
    [self setRefreshFooterView];
    self.MyTable.estimatedRowHeight = 0;
    self.MyTable.estimatedSectionFooterHeight = 0;
    self.MyTable.estimatedSectionHeaderHeight = 0;
    [_MyTable registerClass:[NewDynamicsTableViewCell class] forCellReuseIdentifier:@"NewDynamicsTableViewCell"];
    [self.view addSubview:_MyTable];
    //
    _navBtn = [[UIButton alloc]initWithFrame:CGRectMake((ScreenWidth - 100)/2,(NavHeight - StatusHeight - 35)/2 + StatusHeight, 100, 35) title:@"新鲜事" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorBlack] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    _navBtn.titleLabel.font = font_cu_cu_17;
//    [_navBtn setImage:[UIImage imageNamed:@"findDown"] forState:UIControlStateNormal];
    [_navBtn sizeToFit];
    _navBtn.frame = CGRectMake((ScreenWidth - _navBtn.frame.size.width)/2, (NavHeight - StatusHeight - _navBtn.frame.size.height)/2 + StatusHeight, _navBtn.frame.size.width, _navBtn.frame.size.height);
    [_navBtn addTarget:self action:@selector(downAction:) forControlEvents:UIControlEventTouchUpInside];
    //
    _pinImageView = [[UIImageView alloc]initWithFrame:CGRectMake(_navBtn.right + 5,(NavHeight - StatusHeight - 7)/2 + StatusHeight, 7, 4)];
    _pinImageView.image = [UIImage imageNamed:@"findDown"];
    [self.view addSubview:_pinImageView];
    [self.view addSubview:_navBtn];
    //
    UILabel * lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, NavHeight - 0.5, ScreenWidth, 0.5) text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorLightGray] numberOfLines:0];
    [self.view addSubview: lineLabel];
    //
    _downView = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 0)];
    _downView.backgroundColor = [Utils getUIColorWithHexString:@"f2f2f2"];
    _downView.layer.masksToBounds = YES;
    _isFold = YES;
    [self.view addSubview:_downView];
    
    /*
     CAKeyframeAnimation *animation = (CAKeyframeAnimation *)[socialBtn.layer animationForKey:@"shakeAnimation"];
     if (animation == nil) {
     CAKeyframeAnimation *anima = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation"];
     //在这里@"transform.rotation"==@"transform.rotation.z"
     NSValue *value1 = [NSNumber numberWithFloat:-M_PI/180*4];
     NSValue *value2 = [NSNumber numberWithFloat:M_PI/180*4];
     NSValue *value3 = [NSNumber numberWithFloat:-M_PI/180*4];
     anima.values = @[value1,value2,value3];
     anima.repeatCount = MAXFLOAT;  // 次数为不限
     //anima.duration = 0.25;
     //恢复原样
     //anima.autoreverses = YES;
     
     anima.fillMode = kCAFillModeForwards;
     anima.removedOnCompletion = NO;
     [socialBtn.layer addAnimation:anima forKey:@"shakeAnimation"];
     }else{
     socialBtn.layer.speed = 1.0;
     }
     */
//    CABasicAnimation * animation2 = [CABasicAnimation animationWithKeyPath:@"position"];
//    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    
}
//点击红包按钮
- (void)getRedBag:(UIButton*)btn{
    _redBagButton.hidden = YES;
    [_timer setFireDate:[NSDate distantFuture]];
    _alertBag = [[RBRedBagView alloc]init];
    _alertBag.delegate = self;
    _alertBag.isApart = NO;
    [_alertBag showRedBagWithMoney:_readmoney];
}
#pragma mark 拆开红包 RBRedBagViewDelegate
- (void)DidClickapartBtnAction{
    Handler * hand = [Handler shareHandler];
    hand.delegate = self;
    [hand getRBApartRedBag:_readmoney];
}
//没有点击拆开红包情况下，红包消失后触发方法
- (void)showRedBagButton{
    _redBagButton.hidden = NO;
    [_timer setFireDate:[NSDate distantPast]];
}
- (void)pauseAnimate{
    [_redBagButton.layer removeAllAnimations];
    CAKeyframeAnimation *anima = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation"];
    //在这里@"transform.rotation"==@"transform.rotation.z"
    NSValue *value1 = [NSNumber numberWithFloat:-M_PI/180*4];
    NSValue *value2 = [NSNumber numberWithFloat:M_PI/180*4];
    NSValue *value3 = [NSNumber numberWithFloat:-M_PI/180*4];
    anima.values = @[value1,value2,value3];
    anima.repeatCount = 2;  // 次数为不限
    //anima.duration = 0.25;
    //恢复原样
    anima.autoreverses = YES;
    
    anima.fillMode = kCAFillModeForwards;
    anima.removedOnCompletion = NO;
    [_redBagButton.layer addAnimation:anima forKey:@"shakeAnimation"];
}
- (void)createBtn{
    [_downView removeAllSubviews];
    for (NSInteger i = 0; i<_labelArray.count; i ++ ) {
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(16 + i * (77 + 12), (62 - 32)/2, 77, 32) title:[_labelArray[i] objectAtIndex:1] hlTitle:nil titleColor:nil hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:@"e6e6e7"] image:nil hlImage:nil];
        btn.titleLabel.font = font_cu_cu_(14);
        btn.layer.cornerRadius = 1;
        [btn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 100 + i;
        if (btn.tag - 100 == _index) {
            [btn setTitleColor:[Utils getUIColorWithHexString:SysColorYellow] forState:UIControlStateNormal];
        }else{
            [btn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
        }
        [_downView addSubview:btn];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.layoutsArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewDynamicsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NewDynamicsTableViewCell"];
    cell.layout = self.layoutsArr[indexPath.row];
    cell.delegate = self;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewDynamicsLayout * layout = self.layoutsArr[indexPath.row];
    return layout.height;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RBFindDetailViewController * vc = [[RBFindDetailViewController alloc]init];
    vc.findDelegate = self;
    NewDynamicsLayout * layout = self.layoutsArr[indexPath.row];
    vc.layout = layout;
    vc.hidesBottomBarWhenPushed = YES;
    vc.postId = layout.model.post_id;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)DynamicsCell:(NewDynamicsTableViewCell *)cell didClicktext:(NSString *)text{
    NSIndexPath * indexpath = [self.MyTable indexPathForCell:cell];
    RBFindDetailViewController * vc = [[RBFindDetailViewController alloc]init];
    vc.findDelegate = self;
    NewDynamicsLayout * layout = self.layoutsArr[indexpath.row];
    vc.layout = layout;
    vc.hidesBottomBarWhenPushed = YES;
    vc.postId = layout.model.post_id;
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (void)reloadAction{
    [self.MyTable reloadData];
}
- (void)DidClickCollectDynamicsCell:(NewDynamicsTableViewCell *)cell{
    if ([self isLogin]) {
        _KCIndexPath = [self.MyTable indexPathForCell:cell];
        
        NewDynamicsLayout * layout = self.layoutsArr[_KCIndexPath.row];
        _KCType = @"collect";
        if ([layout.model.is_collected isEqualToString:@"0"]) {
            Handler * handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBAddOrCancellike:@"collect" andpost_id:layout.model.post_id andType:@"add"];
        }else{
            Handler * handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBAddOrCancellike:@"collect" andpost_id:layout.model.post_id andType:@"cancel"];
        }
        NSLog(@"点击了收藏");
    }
}
- (void)DidClickZanBtnCell:(NewDynamicsTableViewCell *)cell{
    if ([self isLogin]){
        NSLog(@"点击了点赞");
        _KCIndexPath = [self.MyTable indexPathForCell:cell];
        _KCType = @"like";
        NewDynamicsLayout * layout = self.layoutsArr[_KCIndexPath.row];
        
        if ([layout.model.is_liked isEqualToString:@"0"]) {
            Handler * handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBAddOrCancellike:@"like" andpost_id:layout.model.post_id andType:@"add"];
        }else{
            Handler * handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBAddOrCancellike:@"like" andpost_id:layout.model.post_id andType:@"cancel"];
        }
    }
}
- (void)DidClickShareInDynamicsCell:(NewDynamicsTableViewCell *)cell{
    if ([self isLogin]){
        NSLog(@"点击了分享");
        _KCIndexPath = [self.MyTable indexPathForCell:cell];
        _KCType = @"forward";
        
        NewDynamicsLayout * layout = self.layoutsArr[_KCIndexPath.row];
        
        Handler * handler = [Handler shareHandler];
        RBShareView *shareView = [RBShareView sharedRBShareView];
        shareView.delegate = self;
        NSString * titleStr;
        if (layout.model.title.length > 20) {
            titleStr = [layout.model.title substringToIndex:20];
        }
        [shareView showViewWithTitle:[NSString stringWithFormat:@"%@",titleStr] Content:[NSString stringWithFormat:@"%@",layout.model.title] URL:[NSString stringWithFormat:@"%@%@",handler.postUrl,layout.model.forward_url] ImgURL:@"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png"];
    }
}

-(void)DidClickMoreLessInDynamicsCell:(NewDynamicsTableViewCell *)cell
{
    NSIndexPath * indexPath = [self.MyTable indexPathForCell:cell];
    NewDynamicsLayout * layout = self.layoutsArr[indexPath.row];
    layout.model.isOpening = !layout.model.isOpening;
    [layout resetLayout];
    CGRect cellRect = [self.MyTable rectForRowAtIndexPath:indexPath];
    
    [self.MyTable reloadData];
    
    if (cellRect.origin.y < self.MyTable.contentOffset.y + 64) {
        [self.MyTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}
-(BOOL)isLogin{
    if ([JsonService isRBUserVisitor]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationShowLoginView
                                                            object:self
                                                          userInfo:nil];
        return NO;
    }else if ([LocalService getRBLocalDataUserPrivateToken] == nil){
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationShowLoginView
                                                            object:self
                                                          userInfo:nil];
        return NO;
    }
    return YES;
}
- (void)DynamicsCell:(NewDynamicsTableViewCell *)cell didClickUrl:(NSString *)url PhoneNum:(NSString *)phoneNum{
    if (url) {
        if ([url rangeOfString:@"wemall"].length != 0 || [url rangeOfString:@"t.cn"].length != 0) {
            if (![url hasPrefix:@"http://"]) {
                url = [NSString stringWithFormat:@"http://%@",url];
            }
            NSLog(@"点击了链接:%@",url);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }else{
            [SVProgressHUD showErrorWithStatus:@"暂不支持打开外部链接"];
        }
    }
    if (phoneNum) {
        NSLog(@"点击了电话:%@",phoneNum);
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"即将拨打电话" message:phoneNum preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNum]]];
        }];
        [alert addAction:action];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
- (void)selectAction:(id)sender{
    UIButton * btn = (UIButton*)sender;
    self.pageIndex = 1;
    _isFold = !_isFold;
    _selectName = _labelArray[btn.tag - 100][0];
    _pinImageView.image = [UIImage imageNamed:@"findDown"];
    [UIView animateWithDuration:0.1 animations:^{
        _downView.frame = CGRectMake(0, NavHeight, ScreenWidth, 0);
    }];
    //
    _index = btn.tag - 100;
    [_navBtn setTitle:_labelArray[btn.tag - 100][1] forState:UIControlStateNormal];
    [_navBtn sizeToFit];
    _navBtn.frame = CGRectMake((ScreenWidth - _navBtn.frame.size.width)/2, (NavHeight - StatusHeight - _navBtn.frame.size.height)/2 + StatusHeight, _navBtn.frame.size.width, _navBtn.frame.size.height);
    _pinImageView.frame = CGRectMake(_navBtn.right + 5,(NavHeight - StatusHeight - 7)/2 + StatusHeight, 7, 4);
    [_pinImageView setImage:[UIImage imageNamed:@"findDown"]];
    NSArray * btnArray = _downView.subviews;
    for (UIButton * button in btnArray) {
        if (btn == button) {
            [button setTitleColor:[Utils getUIColorWithHexString:SysColorYellow] forState:UIControlStateNormal];
        }else{
            [button setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
        }
    }
    
    [self.MyTable.mj_header beginRefreshing];
    
    //
}
- (void)downAction:(id)sender{
    _isFold = !_isFold;
    if (_isFold == NO) {
        _pinImageView.image = [UIImage imageNamed:@"findBack"];
        [UIView animateWithDuration:0.1 animations:^{
            _downView.frame = CGRectMake(0, NavHeight, ScreenWidth, 62);
        }];
    }else{
        _pinImageView.image = [UIImage imageNamed:@"findDown"];
        [UIView animateWithDuration:0.1 animations:^{
            _downView.frame = CGRectMake(0, NavHeight, ScreenWidth, 0);
        }];
    }
}
//判断是否是纯数字
- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}
//分享成功
-(void)shareSuccess{
    NewDynamicsLayout * layout = self.layoutsArr[_KCIndexPath.row];
    
    Handler * handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBAddOrCancellike:@"forward" andpost_id:layout.model.post_id andType:@"add"];
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    if ([sender isEqualToString:@"articles"]) {
        [_MyTable.mj_header endRefreshing];
        [_MyTable.mj_footer endRefreshing];
        NSArray * dataArray = [jsonObject objectForKey:@"list"];
        if (_pageIndex == 1) {
            [_layoutsArr removeAllObjects];
            [self.MyTable setContentOffset:CGPointMake(0, 0)];
        }
        for (id dict in dataArray) {
            DynamicsModel * model = [DynamicsModel modelWithDictionary:dict];
            NewDynamicsLayout * layout = [[NewDynamicsLayout alloc]initWithModel:model];
            
            [self.layoutsArr addObject:layout];
        }
        _labelArray = [jsonObject objectForKey:@"labels"];
        [self createBtn];
        [self.MyTable reloadData];
        [self.hudView dismiss];
        return;
    }
    
    if ([sender isEqualToString:@"articles/set"]) {
        NewDynamicsLayout * layout = self.layoutsArr[_KCIndexPath.row];
        if ([_KCType isEqualToString:@"like"]) {
            if ([layout.model.is_liked isEqualToString:@"0"]) {
                layout.model.is_liked = @"1";
                if ([self isPureInt:layout.model.likes_count]) {
                    int likeCount = [layout.model.likes_count intValue];
                    likeCount = likeCount + 1;
                    layout.model.likes_count = [NSString stringWithFormat:@"%d",likeCount];
                }
            }else{
                layout.model.is_liked = @"0";
                if ([self isPureInt:layout.model.likes_count]) {
                    int likeCount = [layout.model.likes_count intValue];
                    likeCount = likeCount - 1;
                    layout.model.likes_count = [NSString stringWithFormat:@"%d",likeCount];
                }
            }
        }else if([_KCType isEqualToString:@"collect"]){
            if ([layout.model.is_collected isEqualToString:@"0"]) {
                layout.model.is_collected = @"1";
            }else{
                layout.model.is_collected = @"0";
            }
        }else if([_KCType isEqualToString:@"forward"]){
            if ([self isPureInt:layout.model.forwards_count]) {
                int likeCount = [layout.model.forwards_count intValue];
                likeCount = likeCount + 1;
                layout.model.forwards_count = [NSString stringWithFormat:@"%d",likeCount];
                [self.hudView showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
            }
        }
        
        [self.MyTable reloadData];
        return;
    }
    if ([sender isEqualToString:@"split_red"]) {
        //拆红包
        [_redBagButton removeFromSuperview];
        _redBagButton = nil;
        [_timer invalidate];
        _timer = nil;
        _alertBag.isApart = YES;
        [_alertBag apartRedBagSuccess];
        return;
    }
}
- (void)handlerError:(NSError *)error Tag:(NSString *)sender{
    [self.MyTable.mj_header endRefreshing];
    [self.MyTable.mj_footer endRefreshing];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender{
    [self.MyTable.mj_header endRefreshing];
    [self.MyTable.mj_footer endRefreshing];
    [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end
