//
//  RBTabCampaignViewController.h
//  RB
//
//  Created by AngusNi on 5/9/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
//
#import "RBSelectView.h"

#import "RBCampaignView.h"
//
#import "RBCampaignRecruitViewController.h"
#import "RBCampaignInviteViewController.h"
#import "RBCampaignDetailViewController.h"
#import "RBCampaignRecruitAutoViewController.h"
//
#import "RBUserInviteViewController.h"
#import "RBUserSignViewController.h"
#import "RBUserIndianaViewController.h"
#import "RBUserMessageViewController.h"
#import "RBAdvertiserAddViewController.h"
#import "RBKolApplyInfoViewController.h"
@interface RBTabCampaignViewController : RBBaseViewController
<RBBaseVCDelegate,RBSelectViewDelegate,RBCampaignViewDelegate,ASHUDViewDelegate,UIScrollViewDelegate,HDSlideBannerViewDelegate,SDCycleScrollViewDelegate,RBNewAlertViewDelegate>
@property(nonatomic, strong) RBCampaignView *campaignView;
@end
