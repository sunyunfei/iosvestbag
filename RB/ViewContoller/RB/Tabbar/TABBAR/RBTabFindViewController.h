//
//  RBTabFindViewController.h
//  RB
//
//  Created by RB8 on 2018/3/27.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBRedBagView.h"
@interface RBTabFindViewController : RBBaseViewController<RBBaseVCDelegate>
@property(nonatomic,strong)NSMutableArray * layoutsArr;
@end
