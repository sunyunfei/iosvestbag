//
//  InfluenceController.m
//  RB
//
//  Created by RB8 on 2017/7/31.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "InfluenceController.h"
#import "JGProgressView.h"
#import "PKLineView.h"
#import "UpInfluenceViewController.h"
#import "RBInfluenceDetailViewController.h"
#import "RBUserInviteViewController.h"
#import "RBAppDelegate.h"
@interface InfluenceController ()
{
    UIScrollView * backScroView;
    UIImageView * VImageView;
    UIImageView * SImageView;
    JGProgressView * winProgressView;
    JGProgressView * failProgressView;
    UIImageView * kingImageView;
    UIImageView * rightKingImageView;
    UILabel * leftLabel;
    UILabel * rightLabel;
    UIImageView * leftHeadImageView;
    UIImageView * rightHeadImageView;
    //分享按钮
    UIButton * shareButton;
    int count;
    NSTimer * timer;
    NSArray * imageViewArr;
    NSArray * rightImageViewArr;
    NSString * failOrSuccess;
   
}
@end

@implementation InfluenceController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [TalkingData trackPageBegin:@"Pk_influence"];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:@"Pk_influence"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    failOrSuccess = @"right";
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5228", @"影响力对决");
    self.navRightTitle = @"share";
    backScroView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight - NavHeight-50)];
    backScroView.backgroundColor = [UIColor whiteColor];
    backScroView.bounces = NO;
    backScroView.showsVerticalScrollIndicator = NO;
    backScroView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:backScroView];
    count = 0;
    
//   [self.hudView showOverlay];
//    Handler * hand = [Handler shareHandler];
//    hand.delegate = self;
//    [hand getRBbindSocialAccounts:self.kolId andProvider:@"weibo"];
    if (!_myEntity) {
        [self.hudView showOverlay];
        Handler * hand = [Handler shareHandler];
        hand.delegate = self;
        [hand getRBbindSocialAccounts:nil andProvider:@"weibo"];

    }else{
    [self loadHeaderView];
    [self loadContentView];
    [self loadFooterView];
    [NSTimer  scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delayMethod) userInfo:nil repeats:NO];
    }
}
#pragma RBVCDelegate
-(void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)RBNavRightBtnAction{
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.delegate = self;
    alert.shareTitle = @"要和我比拼影响力么？快来Robin8一决高下！";
    alert.shareContent = @"更有海量现金活动，还能和好友PK社交影响力吆";
    alert.shareImageUrl = @"http://pp.myapp.com/ma_icon/0/icon_42248507_1505726963/256";
    alert.shareurl = [NSString stringWithFormat:@"%@kol/%@/kol_pk",ApiUrl,[LocalService getRBLocalDataUserLoginId]];
    [alert showWithShareArray:nil];
}
-(void)RBNewAlert:(RBNewAlert *)alertView clickedButtonAtIndex:(int)buttonIndex{
    
}
-(void)delayMethod{
    NSArray * arr1 = @[_myEntity.influence_score,_myEntity.avg_posts,_myEntity.avg_comments,_myEntity.avg_likes];
    NSArray * arr2 = @[_hisEntity.influence_score,_hisEntity.avg_posts,_hisEntity.avg_comments,_hisEntity.avg_likes];
    PKLineView * lineView1 = [backScroView viewWithTag:100];
    PKLineView * lineView2 = [backScroView viewWithTag:101];
    PKLineView * lineView3 = [backScroView viewWithTag:102];
    PKLineView * lineView4 = [backScroView viewWithTag:103];
//    PKLineView * lineView5 = [backScroView viewWithTag:104];
    winProgressView.progress = (CGFloat)[_myEntity.influence_score intValue]/1000;
    failProgressView.progress = (CGFloat)[_hisEntity.influence_score intValue]/1000;
    lineView1.leftProgressView.progress = (CGFloat)[arr1[0] intValue]/1000;
    lineView1.rightProgressView.progress = (CGFloat)[arr2[0] intValue]/1000;
    lineView2.leftProgressView.progress = (CGFloat)[arr1[1] floatValue]/100;
    lineView2.rightProgressView.progress = (CGFloat)[arr2[1] floatValue]/100;
    lineView3.leftProgressView.progress = (CGFloat)[arr1[2] floatValue]/100;
    lineView3.rightProgressView.progress = (CGFloat)[arr1[2] floatValue]/100;
    lineView4.leftProgressView.progress = (CGFloat)[arr1[3] floatValue]/100;
    lineView4.rightProgressView.progress = (CGFloat)[arr2[3] floatValue]/100;
   
//    lineView1.leftProgressView.progress = 0.3;
//    lineView2.leftProgressView.progress = 0.3;
//    lineView3.leftProgressView.progress = 0.3;
//    lineView4.leftProgressView.progress = 0.3;
//    lineView5.leftProgressView.progress = 0.3;
//    lineView1.rightProgressView.progress = 0.3;
//    lineView2.rightProgressView.progress = 0.3;
//    lineView3.rightProgressView.progress = 0.3;
//    lineView4.rightProgressView.progress = 0.3;
//    lineView5.rightProgressView.progress = 0.3;
    [NSTimer  scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delayMethod1) userInfo:nil repeats:NO];
}
-(void)delayMethod1{
    timer = [NSTimer scheduledTimerWithTimeInterval:0.2
                                           target  :self
                                           selector:@selector(timeLimit)
                                           userInfo:nil
                                           repeats :YES];
}
-(void)loadHeaderView{
    
    UIImageView * headView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 175)];
    headView.image = [UIImage imageNamed:@"PKHead"];
    [backScroView addSubview:headView];
    //
    //
    VImageView = [[UIImageView alloc]initWithFrame:CGRectMake(75,0,30,43)];
    VImageView.image = [UIImage imageNamed:@"RbStarV"];
    [headView addSubview:VImageView];
    //
    SImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth-75-38,headView.height-41, 38, 41)];
    SImageView.image = [UIImage imageNamed:@"RbStar5s"];
    [headView addSubview:SImageView];
    [self breathAnimate:VImageView.layer andTime:1.0];
    [self breathAnimate:SImageView.layer andTime:1.0];
    [UIView animateWithDuration:0.5 delay:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
        VImageView.frame = CGRectMake(headView.center.x - 28, 62,30, 43);
        SImageView.frame = CGRectMake(headView.center.x-24, 71, 38, 41);

    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            VImageView.frame = CGRectMake(headView.center.x - 30, 59,30, 43);
            SImageView.frame = CGRectMake(headView.center.x-23, 72, 38, 41);
        }];
    }];
    //
    winProgressView = [[JGProgressView alloc]initWithFrame:CGRectMake(77-9, headView.height/2 -9, 63, 18)];
    
    winProgressView.Ccolor = [Utils getUIColorWithHexString:@"ffffff"];
    winProgressView.Tcolor = [Utils getUIColorWithHexString:@"ff3030"];
    winProgressView.borderColor = [Utils getUIColorWithHexString:@"ffc701"];
//    winProgressView.progress = 0.8;
    [headView addSubview:winProgressView];
    //
    kingImageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 35, 55, 33)];
    kingImageView.image = [UIImage imageNamed:@"RbKing"];
    kingImageView.center = CGPointMake(20+57/2, 51);
    kingImageView.hidden = YES;
    [headView addSubview:kingImageView];
    //
    leftHeadImageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, (headView.height - 57)/2, 57, 57)  ];
   // winImageView.image = [UIImage imageNamed:@"icon_qzone"];
    leftHeadImageView.layer.cornerRadius = 57/2;
    leftHeadImageView.layer.borderWidth = 2.0;
    leftHeadImageView.layer.borderColor = [Utils getUIColorWithHexString:@"ffc701"].CGColor;
    leftHeadImageView.clipsToBounds = YES;
    [leftHeadImageView sd_setImageWithURL:[NSURL URLWithString:_myEntity.avatar_url] placeholderImage:PlaceHolderUserImage];
    [headView addSubview:leftHeadImageView];
    //

    //
    UILabel * leftNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, leftHeadImageView.bottom+20, 0, 0)];
    leftNameLabel.text = [NSString stringWithFormat:@"%@",_myEntity.name];
    leftNameLabel.font = font_cu_13;
    leftNameLabel.textColor = [UIColor whiteColor];
    [leftNameLabel sizeToFit];
    leftNameLabel.center = CGPointMake(leftHeadImageView.center.x, leftHeadImageView.bottom+10);
    [headView addSubview:leftNameLabel];
    //
    leftLabel = [[UILabel alloc]initWithFrame:CGRectMake(leftNameLabel.right+11, leftNameLabel.top-5, 25, 25)];
    leftLabel.textAlignment = NSTextAlignmentCenter;
    leftLabel.text = @"";
//    leftLabel.backgroundColor = [Utils getUIColorWithHexString:@"ffc701"];
    leftLabel.layer.cornerRadius = 5.0;
    leftLabel.clipsToBounds = YES;
    leftLabel.textColor = [Utils getUIColorWithHexString:@"181818"];
    leftLabel.font = font_cu_13;
    leftLabel.hidden = YES;
    [headView addSubview:leftLabel];
    //
    failProgressView = [[JGProgressView alloc]initWithFrame:CGRectMake(ScreenWidth - 77 -63+9, winProgressView.top, winProgressView.width, winProgressView.height)];
    failProgressView.direction = @"right";
    failProgressView.Ccolor = [Utils getUIColorWithHexString:@"ffffff"];
    failProgressView.Tcolor = [Utils getUIColorWithHexString:@"ff3030"];
    failProgressView.borderColor = [Utils getUIColorWithHexString:@"ffc701"];
    [headView addSubview:failProgressView];
    //20, 35, 55, 33
    rightKingImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 75, kingImageView.top, 55, 33)];
    rightKingImageView.image = [UIImage imageNamed:@"RbKing"];
    rightKingImageView.center = CGPointMake(ScreenWidth-20-57/2, 51);
    rightKingImageView.hidden = YES;
    [headView addSubview:rightKingImageView];
    //
    rightHeadImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 20-57, (headView.height - 57)/2, 57, 57)];
    rightHeadImageView.backgroundColor = [UIColor grayColor];
    rightHeadImageView.layer.cornerRadius = 57/2;
    rightHeadImageView.clipsToBounds = YES;
    rightHeadImageView.layer.borderColor = [Utils getUIColorWithHexString:@"ffc701"].CGColor;
    rightHeadImageView.layer.borderWidth = 2.0;
    [rightHeadImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_hisEntity.avatar_url]] placeholderImage:PlaceHolderImage];
    [headView addSubview:rightHeadImageView];
    //
    UILabel * rightNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, leftNameLabel.top, 0,0)];
    rightNameLabel.text = [NSString stringWithFormat:@"%@",_hisEntity.name];
    rightNameLabel.font = font_cu_13;
    rightNameLabel.textColor = [UIColor whiteColor];
    [rightNameLabel sizeToFit];
    rightNameLabel.center = CGPointMake(rightHeadImageView.center.x, rightHeadImageView.bottom+10);
    [headView addSubview:rightNameLabel];
    //
    rightLabel = [[UILabel alloc]initWithFrame:CGRectMake(rightNameLabel.left-11-25, rightNameLabel.top-5, 25, 25)];
    rightLabel.textAlignment = NSTextAlignmentCenter;
    rightLabel.text = @"";
//    rightLabel.backgroundColor = [Utils getUIColorWithHexString:@"a2a2a2"];
    rightLabel.layer.cornerRadius = 5.0;
    rightLabel.clipsToBounds = YES;
    rightLabel.textColor = [Utils getUIColorWithHexString:@"181818"];
    rightLabel.font = font_cu_13;
    rightLabel.hidden = YES;
    [headView addSubview:rightLabel];
    
    //
    //
    UIImageView * starImageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(19, 60, 5, 5)];
    starImageView1.image = [UIImage imageNamed:@"RbStar1"];
    starImageView1.hidden = YES;
    [headView addSubview:starImageView1];
    //
    UIImageView * starImageView2 = [[UIImageView alloc]initWithFrame:CGRectMake(32, 42, 8, 8)];
    starImageView2.image = [UIImage imageNamed:@"RbStar2"];
    starImageView2.hidden = YES;
    [headView addSubview:starImageView2];
    //
    UIImageView * starImageView3 = [[UIImageView alloc]initWithFrame:CGRectMake(67, 31, 8, 8)];
    starImageView3.image = [UIImage imageNamed:@"RbStar4"];
    starImageView3.hidden = YES;
    [headView addSubview:starImageView3];
    //
    UIImageView * starImageView4 = [[UIImageView alloc]initWithFrame:CGRectMake(60, 45, 8, 8)];
    starImageView4.image = [UIImage imageNamed:@"RbStar6"];
    starImageView4.hidden = YES;
    [headView addSubview:starImageView4];
    //
    UIImageView * starImageView5 = [[UIImageView alloc]initWithFrame:CGRectMake(82, 33, 13, 13)];
    starImageView5.image = [UIImage imageNamed:@"RbStar3"];
    starImageView5.hidden = YES;
    [headView addSubview:starImageView5];
    //
    UIImageView * starImageView6 = [[UIImageView alloc]initWithFrame:CGRectMake(77, 55, 5, 5)];
    starImageView6.image = [UIImage imageNamed:@"RbStar2"];
    starImageView6.hidden = YES;
    [headView addSubview:starImageView6];
    //
    UIImageView * starImageView7 = [[UIImageView alloc]initWithFrame:CGRectMake(95, 55, 4, 4)];
    starImageView7.image = [UIImage imageNamed:@"RbStar7"];
    starImageView7.hidden = YES;
    [headView addSubview:starImageView7];
    //
    UIImageView * failstarImageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth-19-54-5, 60, 5, 5)];
    failstarImageView1.image = [UIImage imageNamed:@"RbStar1"];
    failstarImageView1.hidden = YES;
    [headView addSubview:failstarImageView1];
    //
    UIImageView * failstarImageView2 = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth-19-8-39, 42, 8, 8)];
    failstarImageView2.image = [UIImage imageNamed:@"RbStar2"];
    failstarImageView2.hidden = YES;
    [headView addSubview:failstarImageView2];
    //
    UIImageView * failstarImageView3 = [[UIImageView alloc]initWithFrame:CGRectMake( ScreenWidth-19-2-8, 31, 8, 8)];
    failstarImageView3.image = [UIImage imageNamed:@"RbStar4"];
    failstarImageView3.hidden = YES;
    [headView addSubview:failstarImageView3];
    //
    UIImageView * failstarImageView4 = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth-19-9-8, 45, 8, 8)];
    failstarImageView4.image = [UIImage imageNamed:@"RbStar6"];
    failstarImageView4.hidden = YES;
    [headView addSubview:failstarImageView4];
    //
    UIImageView * failstarImageView5 = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth-19+13-13, 33, 13, 13)];
    failstarImageView5.image = [UIImage imageNamed:@"RbStar3"];
    failstarImageView5.hidden = YES;
    [headView addSubview:failstarImageView5];
    //
    UIImageView * failstarImageView6 = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth-19+8-5, 55, 5, 5)];
    failstarImageView6.image = [UIImage imageNamed:@"RbStar2"];
    failstarImageView6.hidden = YES;
    [headView addSubview:failstarImageView6];
    //
    UIImageView * failstarImageView7 = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth-19+26-4, 55, 4, 4)];
    failstarImageView7.image = [UIImage imageNamed:@"RbStar7"];
    failstarImageView7.hidden = YES;
    [headView addSubview:failstarImageView7];
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, headView.bottom, ScreenWidth, 5)];
    lineView.backgroundColor = [UIColor colorWithRed:236/255.0 green:237/255.0 blue:238/255.0 alpha:1];
    [backScroView addSubview:lineView];
    
    imageViewArr = @[starImageView7,starImageView6,starImageView5,starImageView4,starImageView3,starImageView2,starImageView1];
    rightImageViewArr = @[failstarImageView1,failstarImageView2,failstarImageView3,failstarImageView4,failstarImageView5,failstarImageView6,failstarImageView7];
}
-(void)timeLimit{
    UIImageView * imageView;
    PKLineView * lineView1 = [backScroView viewWithTag:100];
    PKLineView * lineView2 = [backScroView viewWithTag:101];
    PKLineView * lineView3 = [backScroView viewWithTag:102];
    PKLineView * lineView4 = [backScroView viewWithTag:103];
    PKLineView * lineView5 = [backScroView viewWithTag:104];
    if ([_myEntity.influence_score intValue]>[_hisEntity.influence_score intValue]) {
        //左边胜利
        failOrSuccess = @"left";
    }else if([_myEntity.influence_score intValue]<[_hisEntity.influence_score intValue]){
        //右边胜利
        failOrSuccess = @"right";
    }else{
        //平局
        failOrSuccess = @"ping";
    }
    if ([failOrSuccess isEqualToString:@"left"]) {
        imageView = imageViewArr[count];
        imageView.hidden = NO;
        if (count == 0) {
            //
            kingImageView.hidden = NO;
            [self breathAnimate:kingImageView.layer andTime:0.5];
            //
            leftLabel.hidden = NO;
            leftLabel.text = @"胜";
            leftLabel.backgroundColor = [Utils getUIColorWithHexString:SysBackColorYellow];
            rightLabel.hidden = NO;
            rightLabel.text = @"败";
            rightLabel.backgroundColor = [Utils getUIColorWithHexString:@"a2a2a2"];
            //
            lineView1.rightProgressView.Tcolor = [Utils getUIColorWithHexString:@"b4b4b4"];
            lineView2.rightProgressView.Tcolor = [Utils getUIColorWithHexString:@"b4b4b4"];
            lineView3.rightProgressView.Tcolor = [Utils getUIColorWithHexString:@"b4b4b4"];
            lineView4.rightProgressView.Tcolor = [Utils getUIColorWithHexString:@"b4b4b4"];
            lineView5.rightProgressView.Tcolor = [Utils getUIColorWithHexString:@"b4b4b4"];
            //
            failProgressView.borderColor = [Utils getUIColorWithHexString:@"ececec"];
            failProgressView.Tcolor = [Utils getUIColorWithHexString:@"949494"];
            //
            rightHeadImageView.layer.borderColor = [Utils getUIColorWithHexString:@"ececec"].CGColor;
        }
    }else if([failOrSuccess isEqualToString:@"right"]){
        if (count == 0) {
            rightKingImageView.hidden = NO;
            [self breathAnimate:rightKingImageView.layer andTime:0.5];
            //
            leftLabel.hidden = NO;
            leftLabel.text = @"败";
            leftLabel.backgroundColor = [Utils getUIColorWithHexString:@"a2a2a2"];
            rightLabel.hidden = NO;
            rightLabel.text = @"胜";
            rightLabel.backgroundColor = [Utils getUIColorWithHexString:SysBackColorYellow];
            //
            lineView1.leftProgressView.Tcolor = [Utils getUIColorWithHexString:@"b4b4b4"];
            lineView2.leftProgressView.Tcolor = [Utils getUIColorWithHexString:@"b4b4b4"];
            lineView3.leftProgressView.Tcolor = [Utils getUIColorWithHexString:@"b4b4b4"];
            lineView4.leftProgressView.Tcolor = [Utils getUIColorWithHexString:@"b4b4b4"];
            lineView5.leftProgressView.Tcolor = [Utils getUIColorWithHexString:@"b4b4b4"];
            //
            winProgressView.borderColor = [Utils getUIColorWithHexString:@"ececec"];
            winProgressView.Tcolor = [Utils getUIColorWithHexString:@"949494"];
            //
            leftHeadImageView.layer.borderColor = [Utils getUIColorWithHexString:@"ececec"].CGColor;
        }
        imageView = rightImageViewArr[count];
        imageView.hidden = NO;
    }else if([failOrSuccess isEqualToString:@"ping"]){
        leftLabel.hidden = NO;
        leftLabel.text = @"平";
        leftLabel.backgroundColor = [Utils getUIColorWithHexString:SysBackColorYellow];
        rightLabel.hidden = NO;
        rightLabel.text = @"平";
        rightLabel.backgroundColor = [Utils getUIColorWithHexString:SysBackColorYellow];
        
    }
    count++;
    if (count == 7) {
        [timer invalidate];
        timer = nil;
    }
}

//动画顺序执行
-(void)animateWithOrder:(double)time AndLayer:(CALayer*)layer{
    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.fromValue = [NSNumber numberWithFloat:0.0f];
    animation.toValue = [NSNumber numberWithFloat:1.0f];
    animation.duration = 0.2f;
    animation.repeatCount = 1;
    animation.beginTime = time;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;//removeOnCompletion,fillMode配合使用保持动画完成效果
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    [layer addAnimation:animation forKey:@"alpha"];
}
//透明度动画
-(void)breathAnimate:(CALayer*)layer andTime:(CGFloat)time{
    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.fromValue = [NSNumber numberWithFloat:0.0f];
    animation.toValue = [NSNumber numberWithFloat:1.0f];
    animation.duration = time;
    animation.repeatCount = 1;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;//removeOnCompletion,fillMode配合使用保持动画完成效果
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    [layer addAnimation:animation forKey:@"aAlpha"];
}
-(void)loadContentView{
    NSArray * arr = @[NSLocalizedString(@"R5229", @"影响力分数"),NSLocalizedString(@"R5230", @"平均转发"),NSLocalizedString(@"R5231", @"平均互动"),NSLocalizedString(@"R5232", @"平均点赞")];
    NSArray * arr1 = @[_myEntity.influence_score,_myEntity.avg_posts,_myEntity.avg_comments,_myEntity.avg_likes];
    NSArray * arr2 = @[_hisEntity.influence_score,_hisEntity.avg_posts,_hisEntity.avg_comments,_hisEntity.avg_likes];
    for (NSInteger i = 0; i<4; i++) {
        PKLineView * lineView = [[PKLineView alloc]initWithFrame:CGRectMake(0, 30+180+i*(43+32), ScreenWidth, 43)];
        [lineView getdata:arr[i]];
        lineView.tag = 100+i;
        
        lineView.leftScoreLabel.text = [NSString stringWithFormat:@"%.2f",roundf([arr1[i] floatValue]*100)/100];
        lineView.rightScoreLabel.text = [NSString stringWithFormat:@"%.2f",roundf([arr2[i] floatValue]*100)/100];
        [backScroView addSubview:lineView];
        if (i == 3) {
            backScroView.contentSize = CGSizeMake(ScreenWidth, lineView.bottom+20);
        }
    }
}
-(void)loadFooterView{
    UIView * footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-50, ScreenWidth, 50)];
    footerView.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
    [self.view addSubview:footerView];
    //
    UIButton * InfluenceButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth/2, 50) title:NSLocalizedString(@"R5233", @"影响力变现") hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    InfluenceButton.titleLabel.font = font_cu_17;
    [InfluenceButton addTarget:self action:@selector(PressBtn:) forControlEvents:UIControlEventTouchUpInside];
    InfluenceButton.tag = 998;
    [footerView addSubview:InfluenceButton];
    //
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(ScreenWidth/2, 0, 0.5, 50)];
    lineView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [footerView addSubview:lineView];
    //
    UIButton * PKButton = [[UIButton alloc]initWithFrame:CGRectMake(lineView.right,0, ScreenWidth/2, 50) title:NSLocalizedString(@"R5234", @"和微信好友PK") hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    PKButton.titleLabel.font = font_cu_17;
    [PKButton addTarget:self action:@selector(PressBtn:) forControlEvents:UIControlEventTouchUpInside];
    PKButton.tag = 999;
    [footerView addSubview:PKButton];

}
-(void)PressBtn:(UIButton*)sender{
    if (sender.tag == 998) {
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:22];
        
    }else{
        //和微信好友PK
//        RBInfluenceDetailViewController * detailVc = [[RBInfluenceDetailViewController alloc]init];
//        detailVc.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:detailVc animated:YES];
         [self shareSDKWithTitle:@"要和我比拼影响力么？快来Robin8一决高下！" Content:@"Robin8基于大数据的社交影响力平台" URL:[NSString stringWithFormat:@"%@kol/%@/kol_pk",ApiUrl,[LocalService getRBLocalDataUserLoginId]] ImgURL:@"http://pp.myapp.com/ma_icon/0/icon_42248507_1505726963/256" SSDKPlatformType:SSDKPlatformSubTypeWechatSession];
    }
}
//和微信好友PK
- (void)shareSDKWithTitle:(NSString *)titleStr
                  Content:(NSString *)contentStr
                      URL:(NSString *)urlStr
                   ImgURL:(NSString *)imgUrlStr
         SSDKPlatformType:(SSDKPlatformType)type{
    [SVProgressHUD show];
    NSLog(@"title:%@,content:%@,img:%@,url:%@",titleStr,contentStr,imgUrlStr,urlStr);
    if (type==SSDKPlatformTypeSinaWeibo) {
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:[NSString stringWithFormat:@"%@ %@",titleStr,urlStr]
                                         images:imgUrlStr
                                            url:[NSURL URLWithString:urlStr]
                                          title:[NSString stringWithFormat:@"%@ %@",titleStr,urlStr]
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            if (state == SSDKResponseStateSuccess) {
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
            }
            if (state == SSDKResponseStateFail) {
                if (error.code == 204) {
                    [SVProgressHUD showErrorWithStatus:@"相同的内容不能重复分享"];
                } else {
                    if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                        [SVProgressHUD showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                    } else {
                        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
                    }
                }
            }
            if (state == SSDKResponseStateCancel) {
                [SVProgressHUD dismiss];
                //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
            }
        }];
    } else {
        [[SDWebImageDownloader sharedDownloader]downloadImageWithURL:[NSURL URLWithString:imgUrlStr] options:SDWebImageDownloaderHighPriority progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            if(finished==NO) {
                NSLog(@"图片下载失败:%@,%@",image,data);
            } else {
                image = [Utils narrowWithImage:image];
            }
            NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
            [shareParams SSDKSetupShareParamsByText:contentStr
                                             images:image
                                                url:[NSURL URLWithString:urlStr]
                                              title:titleStr
                                               type:SSDKContentTypeWebPage];
            [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
                if (state == SSDKResponseStateSuccess) {
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
                }
                if (state == SSDKResponseStateFail) {
                    if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                        [SVProgressHUD showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                    } else {
                        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
                    }
                }
                if (state == SSDKResponseStateCancel) {
                    [SVProgressHUD dismiss];
                    //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
                }
            }];
        }];
    }
    
}

-(void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    if ([sender isEqualToString:@"calculate_influence_score"]) {
        Handler * handle = [Handler shareHandler];
        handle.delegate = self;
        [handle getRBTestInfluence];
    }
   
    if ([sender isEqualToString:@"influence_score"]){
            [self.hudView dismiss];
            if ([[jsonObject objectForKey:@"calculated"]boolValue] == 0) {
                _myEntity = [[RBNewInfluenceEntity alloc]init];
                _myEntity.avg_likes = @"0";
                _myEntity.avg_posts = @"0";
                _myEntity.avg_comments = @"0";
                _myEntity.influence_score = @"0";
                _myEntity.name = @"我";
                [self loadHeaderView];
                [self loadContentView];
                [self loadFooterView];
                [NSTimer  scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delayMethod) userInfo:nil repeats:NO];
            }else{
                _myEntity = [JsonService getRBNewInfluenceEntity:jsonObject];
                [self loadHeaderView];
                [self loadContentView];
                [self loadFooterView];
                [NSTimer  scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delayMethod) userInfo:nil repeats:NO];
            }
    }

    
}
-(void)handlerError:(NSError *)error Tag:(NSString *)sender{
    [self.hudView dismiss];
    if (!_myEntity) {
        _myEntity = [[RBNewInfluenceEntity alloc]init];
        _myEntity.influence_score = @"0";
        _myEntity.name = @"我";
        _myEntity.avg_likes = @"0";
        _myEntity.avg_posts = @"0";
        _myEntity.avg_comments = @"0";
        [self loadHeaderView];
        [self loadContentView];
        [self loadFooterView];
        [NSTimer  scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delayMethod) userInfo:nil repeats:NO];
    }
}
-(void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender{
    [self.hudView dismiss];
    if (!_myEntity) {
        _myEntity = [[RBNewInfluenceEntity alloc]init];
        _myEntity.influence_score = @"0";
        _myEntity.name = @"我";
        _myEntity.avg_likes = @"0";
        _myEntity.avg_posts = @"0";
        _myEntity.avg_comments = @"0";
        [self loadHeaderView];
        [self loadContentView];
        [self loadFooterView];
        [NSTimer  scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delayMethod) userInfo:nil repeats:NO];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
