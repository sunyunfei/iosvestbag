//
//  RBTabInfluenceController.m
//  RB
//
//  Created by RB8 on 2017/7/10.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBTabInfluenceController.h"
#import "RBInfluenceViewController.h"
#import "RBInfluenceBundingViewController.h"
#import "RBNewInfluenceViewController.h"
#import "RBInfluenceView.h"
@interface RBTabInfluenceController ()<UIScrollViewDelegate>
{
    UIScrollView * backScroll;
    RBInfluenceView * influView;
    UIScrollView * userScroll;
    UIScrollView * campainScroll;
}
@end

@implementation RBTabInfluenceController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.navTitle = @"测评结果";
    backScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-49)];
    backScroll.delegate = self;
    backScroll.showsVerticalScrollIndicator = NO;
    backScroll.showsHorizontalScrollIndicator = NO;
    backScroll.bounces = NO;
    [self.view addSubview:backScroll];
    //
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.delegate = self;
    [alert showWithArray:@[@"稍等片刻，30分钟后我们会给你提供更详细的数据哦",@"知道了"] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
    [self loadHeadView];
    [self loadEditView];
}
-(void)loadEditView{
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, influView.bottom, ScreenWidth, 8)];
    lineView.backgroundColor = [Utils getUIColorWithHexString:ccColora4a4a4];
    lineView.alpha = 0.14;
    [backScroll addSubview:lineView];
    //
    UILabel * middleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, ScreenWidth, 20) text:@"我们发现你跟下面的几位用户兴趣相投" font:font_13 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:SysColorBlack] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [backScroll addSubview:middleLabel];
    //
    userScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, middleLabel.bottom+10, ScreenWidth, 52)];
    userScroll.showsHorizontalScrollIndicator = NO;
    userScroll.showsVerticalScrollIndicator = NO;
    userScroll.bounces = NO;
    [backScroll addSubview:userScroll];
    for (NSInteger i =0; i<10; i++) {
        UIView * view = [[UIView alloc]initWithFrame:CGRectMake(15+i*(52+15), 0, 52, 52)];
        view.backgroundColor = [UIColor whiteColor];
        view.layer.cornerRadius = 26;
        view.layer.masksToBounds = YES;
        view.layer.borderWidth = 1;
        view.layer.borderColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.17].CGColor;
        [userScroll addSubview:view];
        UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(3, 3, 46, 46)];
        imageView.layer.cornerRadius = 23;
        imageView.clipsToBounds = YES;
        imageView.image = [UIImage imageNamed:@"RbWeibo"];
        [view addSubview:imageView];
    }
    userScroll.contentSize = CGSizeMake(15+10*(52+15), 52);
    //
    UILabel * recommentLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, userScroll.bottom+10, ScreenWidth, 13) text:@"为你推荐的品牌福利活动" font:font_13 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:SysColorBlack] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [userScroll addSubview:recommentLabel];
    //
    UILabel * shareLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, recommentLabel.bottom+5, ScreenWidth, 11) text:@"转发有奖金，累计50元即可提现" font:font_11 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:SysColorBlack] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [userScroll addSubview:shareLabel];
    //
}
-(void)loadHeadView{
    influView = [[RBInfluenceView alloc]initWithFrame:CGRectMake(0, 18, ScreenWidth, 170)];
    influView.rate = 0.8;
    influView.category = @"1";
    [backScroll addSubview:influView];
}
-(void)PressBtn:(id)sender{
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
