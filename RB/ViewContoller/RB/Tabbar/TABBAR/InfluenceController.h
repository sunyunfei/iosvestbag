//
//  InfluenceController.h
//  RB
//
//  Created by RB8 on 2017/7/31.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBNewInfluenceEntity.h"
#import "RBNewInfluenceEntity.h"
@interface InfluenceController : RBBaseViewController<RBBaseVCDelegate,UIScrollViewDelegate,RBNewAlertViewDelegate>
@property(nonatomic,copy)NSString * kolId;
@property(nonatomic,strong) RBNewInfluenceEntity * myEntity;
@property(nonatomic,strong) RBNewInfluenceEntity * hisEntity;
@end
