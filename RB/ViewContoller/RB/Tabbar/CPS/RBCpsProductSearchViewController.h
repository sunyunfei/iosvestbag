//
//  RBCpsProductSearchViewController.h
//  RB
//
//  Created by AngusNi on 16/9/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBCpsProductView.h"
#import "RBUserContactCpsViewController.h"
#import "RBCpsProductDetailViewController.h"
#import "DOPDropDownMenu.h"


@class RBCpsProductSearchViewController;
@protocol RBCpsProductSearchViewControllerDelegate <NSObject>
@optional
- (void)RBCpsProductSearchViewController:(RBCpsProductSearchViewController *)vc select:(RBCpsProductEntity*)productEntity;
@end

@interface RBCpsProductSearchViewController : RBBaseViewController
<RBBaseVCDelegate,RBCpsProductViewDelegate,RBCpsProductDetailViewControllerDelegate,UITextFieldDelegate,DOPDropDownMenuDataSource,DOPDropDownMenuDelegate>
@property(nonatomic, strong) RBCpsProductView *cpsProductView;
@property(nonatomic, weak)  id<RBCpsProductSearchViewControllerDelegate> delegateT;
@property(nonatomic, strong) NSString *isNew;

@end
