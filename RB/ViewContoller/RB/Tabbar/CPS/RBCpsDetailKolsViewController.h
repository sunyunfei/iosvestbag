//
//  RBCpsDetailKolsViewController.h
//  RB
//
//  Created by AngusNi on 7/5/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolDetailViewController.h"

@interface RBCpsDetailKolsViewController : RBBaseViewController
<RBBaseVCDelegate>
@property(nonatomic, strong) RBCpsCreateEntity*cpsEntity;

@end
