//
//  RBFindDetailViewController.h
//  RB
//
//  Created by RB8 on 2018/4/24.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "NewDynamicsLayout.h"
#import "RBShareView.h"
@protocol RBFindDetailControllerDelegate <NSObject>
- (void)reloadAction;
@end
@interface RBFindDetailViewController : RBBaseViewController<HandlerDelegate,RBBaseVCDelegate,RBShareViewDelegate>
@property(nonatomic,strong)NewDynamicsLayout * layout;
@property(nonatomic,strong)NSString * postId;
@property(nonatomic,weak)id<RBFindDetailControllerDelegate>findDelegate;
@property(nonatomic,assign)BOOL isHaveMore;
@end
