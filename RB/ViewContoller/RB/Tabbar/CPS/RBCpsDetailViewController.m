//
//  RBCpsDetailViewController.m
//  RB
//
//  Created by AngusNi on 16/8/25.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBCpsDetailViewController.h"

@interface RBCpsDetailViewController () {
    UIWebView*webviewn;
    UIView*footerView;
}
@end

@implementation RBCpsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R8002",@"详情");
    //
    if([self.createEntity.status isEqualToString:@"passed"]) {
        webviewn = [[UIWebView alloc] initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    } else {
        webviewn = [[UIWebView alloc] initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    }
    webviewn.delegate = self;
    webviewn.scalesPageToFit = YES;
    [self.view addSubview:webviewn];
    [webviewn loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.createEntity.show_url]]]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"cps-detail"];
    if([self.createEntity.status isEqualToString:@"passed"]) {
        [self.hudView show];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBCpsCreateDetailWithID:self.createEntity.iid];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"cps-detail"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton*)sender {
    if([self isVisitorLogin]==YES) {
        return;
    };
    if(self.createEntity.material_total_price.intValue==0) {
        [self.hudView showOverlay];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBCpsCreateShareWithId:self.createEntity.iid];
    } else {
        RBAlertView *alertView = [[RBAlertView alloc]init];
        alertView.delegate = self;
        alertView.tag = 20000;
        [alertView showWithArray:@[NSLocalizedString(@"R8040", @"文章分享后，您的朋友通过购买了文章中的商品，您可以获得高额提成！"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
    }
}

- (void)kolBtnAction:(UIButton*)sender {
    RBCpsDetailKolsViewController*toview = [[RBCpsDetailKolsViewController alloc] init];
    toview.cpsEntity = self.createEntity;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if (buttonIndex==0) {
        [self.hudView showOverlay];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBCpsCreateShareWithId:self.createEntity.iid];
        return;
    }
}

#pragma mark - UIButton Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self.hudView show];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.hudView dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self.hudView dismiss];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    [footerView removeAllSubviews];
    [footerView removeFromSuperview];
    footerView = nil;
    footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    footerView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkgGray];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(ScreenWidth-107, 0, 107, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    if(self.createEntity.material_total_price.intValue==0) {
        [footerBtn setTitle:NSLocalizedString(@"R1022", @"分享") forState:UIControlStateNormal];
    } else {
        [footerBtn setTitle:NSLocalizedString(@"R8022",@"分享赚钱") forState:UIControlStateNormal];
    }
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
    //
    UIButton*kolBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    kolBtn.frame = CGRectMake(0, 0, ScreenWidth-107, footerView.height);
    [kolBtn addTarget:self
               action:@selector(kolBtnAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:kolBtn];
    //
    float temw = 35.0;
    float temg = 15.0;
    int num = (int)self.createEntity.cps_article_shares.count;
    num = num+1;
    if(self.view.width>320){
        if (num>5) {
            num=5;
        }
    } else {
        if (num>4) {
            num=4;
        }
    }
    float temleft = (kolBtn.width-temw*num-temg*(num-1))/2.0;
    for (int i=0; i<num; i++) {
        if(i==(num-1)) {
            UILabel*kolIVLabel = [[UILabel alloc] initWithFrame:CGRectMake(temleft+(temw+temg)*i, (kolBtn.height - temw)/2.0, temw, temw)];
            kolIVLabel.font = font_icon_(temw-1);
            kolIVLabel.text = Icon_btn_more;
            kolIVLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            [kolBtn addSubview:kolIVLabel];
        } else {
            RBKOLEntity*userEntity = self.createEntity.cps_article_shares[i];
            UIImageView*kolIV = [[UIImageView alloc]initWithFrame:CGRectMake(temleft+(temw+temg)*i, (kolBtn.height - temw)/2.0, temw, temw)];
            [kolIV sd_setImageWithURL:[NSURL URLWithString:userEntity.avatar_url] placeholderImage:PlaceHolderUserImage];
            if (userEntity.avatar_url.length==0) {
                kolIV.layer.borderWidth = 1.0f;
                kolIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
            }
            kolIV.layer.cornerRadius = kolIV.width/2.0;
            kolIV.layer.masksToBounds = YES;
            [kolBtn addSubview:kolIV];
        }
    }
}

- (void)RBCpsShareViewDetailAction:(RBCpsShareView *)view {
    RBCpsProductViewController*toview = [[RBCpsProductViewController alloc] init];
    toview.isNew = @"YES";
    toview.createId = self.createEntity.iid;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"show"]) {
        [self.hudView dismiss];
        self.createEntity = [JsonService getRBCpsCreateDetailEntity:jsonObject];
        [self loadFooterView];
    }
    
    if ([sender isEqualToString:@"cps_articles/share_article"]) {
        NSArray*shareArray = [jsonObject objectForKey:@"cps_article_share"];
        NSString*share_url = [JsonService checkJson:[shareArray valueForKey:@"share_url"]];
        if (share_url.length!=0) {
            [self.hudView dismiss];
            RBCpsShareView *shareView = [RBCpsShareView sharedRBCpsShareView];
            shareView.delegate = self;
            [shareView showViewWithTitle:[NSString stringWithFormat:@"%@",self.createEntity.title] Content:[NSString stringWithFormat:@"%@",self.createEntity.title] URL:[NSString stringWithFormat:@"%@",share_url] ImgURL:[NSString stringWithFormat:@"%@",self.createEntity.cover]];
        } else {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R8035",@"此文章不可分享！")];
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}


@end
