//
//  RBCpsProductDetailViewController.h
//  RB
//
//  Created by AngusNi on 16/9/6.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBArticleSearchViewController.h"

@class RBCpsProductDetailViewController;
@protocol RBCpsProductDetailViewControllerDelegate <NSObject>
@optional
- (void)RBCpsProductDetailViewController:(RBCpsProductDetailViewController *)vc select:(RBCpsProductEntity*)productEntity;
@end

@interface RBCpsProductDetailViewController : RBBaseViewController
<RBBaseVCDelegate,UIWebViewDelegate>
@property(nonatomic, strong) RBCpsProductEntity*productEntity;
@property(nonatomic, weak)  id<RBCpsProductDetailViewControllerDelegate> delegateP;
@property(nonatomic, strong) NSString *isNew;

@end
