//
//  RBCpsProductViewController.m
//  RB
//
//  Created by AngusNi on 16/9/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBCpsProductViewController.h"
#import "RBCpsEditViewController.h"

@interface RBCpsProductViewController () {
}
@end

@implementation RBCpsProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R8005",@"商品库");
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize labelSize = [NSLocalizedString(@"R8006",@"入驻") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_cu_15,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
    UIButton*rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.navView.width-labelSize.width-CellLeft, 20.0, labelSize.width, 44.0);
    rightBtn.titleLabel.font = font_15;
    [rightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
    [rightBtn setTitle:NSLocalizedString(@"R8006",@"入驻") forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(RBNavRightBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:rightBtn];
    //
    self.bgIV.hidden = NO;
    //
    self.cpsProductView = [[RBCpsProductView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    self.cpsProductView.delegate = self;
    [self.view addSubview:self.cpsProductView];
//    [self loadHeaderView];
    //
    [self.hudView show];
    [self.cpsProductView RBCpsProductViewLoadRefreshViewFirstData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageBegin:@"cps-product-list"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"cps-product-list"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)RBNavRightBtnAction {
    if([self isVisitorLogin]==YES){
        return;
    };
    //
    RBUserContactCpsViewController*toview = [[RBUserContactCpsViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)bannerBtnAction {
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationStatusBarAction {
    CGPoint off = self.cpsProductView.tableviewn.contentOffset;
    off.y = 0 - self.cpsProductView.tableviewn.contentInset.top;
    [self.cpsProductView.tableviewn setContentOffset:off animated:YES];
}

#pragma mark - RBCpsView Delegate
-(void)RBCpsProductViewLoadRefreshViewData:(RBCpsProductView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    if (self.createId) {
        [handler getRBCpsCreateProductWithId:self.createId];
    } else {
        [handler getRBCpsProductListWithGoods:nil andCategory:nil andOrder:nil andPage:[NSString stringWithFormat:@"%d",self.cpsProductView.pageIndex+1]];
    }
}

- (void)RBCpsProductViewSelected:(RBCpsProductView *)view andIndex:(int)index {
    RBCpsProductEntity *entity = view.datalist[index];
    RBCpsProductDetailViewController*toview = [[RBCpsProductDetailViewController alloc] init];
    toview.productEntity = entity;
    toview.hidesBottomBarWhenPushed = YES;
    toview.delegateP = self;
    toview.isNew = self.isNew;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - RBCpsProductDetailViewControllerDelegate Delegate
- (void)RBCpsProductDetailViewController:(RBCpsProductDetailViewController *)vc select:(RBCpsProductEntity*)productEntity {
    if([self isVisitorLogin]==YES) {
        return;
    };
    if (self.isNew!=nil) {
        RBCpsEditViewController *toview = [[RBCpsEditViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        toview.productEntity = productEntity;
        [self.navigationController pushViewController:toview animated:YES];
    } else {
        if ([self.delegateT respondsToSelector:@selector(RBCpsProductViewController:select:)]) {
            [self.delegateT RBCpsProductViewController:self select:productEntity];
        }
        for (UIViewController *temp in self.navigationController.viewControllers){
            if ([temp isKindOfClass:[RBCpsEditViewController class]]) {
                RBCpsEditViewController*toview = (RBCpsEditViewController*)temp;
                [self.navigationController popToViewController:toview animated:YES];
                return;
            }
        }
    }
}

#pragma mark - loadHeaderView Delegate
- (void)loadHeaderView {
    UIView*bannerBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 50.0)];
    bannerBgView.backgroundColor = [UIColor whiteColor];
    //
    UIImageView*bannerIV = [[UIImageView alloc]initWithFrame:CGRectMake(CellLeft, (50.0-((ScreenWidth-2*CellLeft)/700.0)* 60.0)/2.0, ScreenWidth-2*CellLeft, ((ScreenWidth-2*CellLeft)/700.0)* 60.0)];
    bannerIV.image = [UIImage imageNamed:@"icon_create_search.png"];
    [bannerBgView addSubview:bannerIV];
    //
    UIButton*bannerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    bannerBtn.frame = bannerBgView.bounds;
    [bannerBtn addTarget:self
                       action:@selector(bannerBtnAction)
             forControlEvents:UIControlEventTouchUpInside];
    [bannerBgView addSubview:bannerBtn];
    //
    [self.cpsProductView.tableviewn setTableHeaderView:bannerBgView];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"cps_materials"]||[sender isEqualToString:@"cps_articles/materials"]) {
        [self.hudView dismiss];
        if (self.cpsProductView.pageIndex == 0) {
            self.cpsProductView.datalist = [NSMutableArray new];
            self.cpsProductView.jsonObject = jsonObject;
        }
        self.cpsProductView.datalist = [JsonService getRBCpsProductListEntity:jsonObject andBackArray:self.cpsProductView.datalist];
        [self.cpsProductView RBCpsProductViewSetRefreshViewFinish];
        if ([self.cpsProductView.datalist count] == 0) {
            self.cpsProductView.defaultView.hidden = NO;
        } else {
            self.cpsProductView.defaultView.hidden = YES;
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self.cpsProductView RBCpsProductViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.cpsProductView RBCpsProductViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

