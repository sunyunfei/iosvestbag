//
//  RBCpsProductSearchViewController.m
//  RB
//
//  Created by AngusNi on 16/9/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBCpsProductSearchViewController.h"
#import "RBCpsEditViewController.h"

@interface RBCpsProductSearchViewController () {
    InsetsTextField*searchTF;
    UIButton*cancelBtn;
    UIButton*searchBgBtn;
    NSArray*orderArray;
    NSArray*categoryArray;
    NSArray*orderenArray;

    NSString*category;
    NSString*order;
}
@end

@implementation RBCpsProductSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    NSMutableParagraphStyle *paragraphStyle  =
    [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize labelSize  =
    [NSLocalizedString(@"R1011", @"取消") boundingRectWithSize:CGSizeMake(1000, 20)
                                                     options:NSStringDrawingUsesLineFragmentOrigin |
     NSStringDrawingUsesFontLeading
                                                  attributes:@{
                                                               NSFontAttributeName : font_cu_15,
                                                               NSParagraphStyleAttributeName : paragraphStyle
                                                               } context:nil]
    .size;
    //
    UIView*bkgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, NavHeight)];
    bkgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:bkgView];
    //
    UIView*bgView = [[UIView alloc]initWithFrame:CGRectMake(CellLeft, StatusHeight+(44.0-30.0)/2.0, ScreenWidth-CellLeft*3.0-labelSize.width, 30.0)];
    bgView.layer.borderWidth = 0.5;
    bgView.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlack]CGColor];
    bgView.layer.masksToBounds = YES;
    bgView.userInteractionEnabled = YES;
    [self.view addSubview:bgView];
    //
    searchTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(5.0, 0, bgView.width, bgView.height)];
    searchTF.font = font_cu_15;
    searchTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    searchTF.placeholder = NSLocalizedString(@"R2010",@"搜索");
    searchTF.keyboardType = UIKeyboardTypeDefault;
    searchTF.returnKeyType = UIReturnKeySearch;
    searchTF.delegate = self;
    searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    [bgView addSubview:searchTF];
    //
    cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(bgView.right, bgView.top, labelSize.width+2*CellLeft, bgView.height);
    cancelBtn.titleLabel.font = font_cu_15;
    [cancelBtn setTitle:NSLocalizedString(@"R1011", @"取消") forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(RBNavLeftBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBtn];
    //
    UILabel*navLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NavHeight-0.5, ScreenWidth, 0.5)];
    navLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBarLine];
    [self.view addSubview:navLineLabel];
    //
    orderArray = @[@"综合排序",@"按价格从高到低",@"按价格从低到高",@"按提成从高到低",@"按提成从低到高"];
    orderenArray = @[@"order_overall",@"order_price_desc",@"order_price_asc",@"order_commission_desc",@"order_commission_asc"];
    categoryArray = @[@"全部分类",@"手机", @"家具", @"珠宝首饰", @"影视", @"厨具", @"音乐", @"玩具乐器", @"家用电器", @"礼品箱包", @"服饰内衣", @"家居家装", @"运动户外", @"养生保健",
                 @"医药保健", @"宠物生活", @"鞋靴", @"教育音像", @"电脑和办公", @"性福生活", @"酒类", @"食品饮料", @"汽车用品", @"家用器械", @"个护化妆", @"母婴", @"图书", @"钟表"];
    DOPDropDownMenu *menu = [[DOPDropDownMenu alloc]
                             initWithOrigin:CGPointMake(0, NavHeight)
                             andHeight:40.0];
    menu.dataSource = self;
    menu.delegate = self;
    [self.view addSubview:menu];
    //
    self.cpsProductView = [[RBCpsProductView alloc]initWithFrame:CGRectMake(0, 104.0, ScreenWidth, ScreenHeight-104)];
    self.cpsProductView.delegate = self;
    [self.view addSubview:self.cpsProductView];
    //
    [self.hudView show];
    [self.cpsProductView RBCpsProductViewLoadRefreshViewFirstData];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageBegin:@"cps-product-list"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"cps-product-list"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self searchBgBtnAction:nil];
    //
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (searchBgBtn == nil) {
        searchBgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        searchBgBtn.frame  =
        CGRectMake(0, 104, self.view.bounds.size.width,
                   self.view.bounds.size.height);
        searchBgBtn.backgroundColor  = [UIColor clearColor];
        [searchBgBtn addTarget:self
              action          :@selector(searchBgBtnAction:)
              forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:searchBgBtn];
    }
    searchBgBtn.hidden = NO;
}

- (void)searchBgBtnAction:(UIButton *)sender{
    [self.view endEditing:YES];
    searchBgBtn.hidden = YES;
    //
    [self.hudView show];
    [self.cpsProductView RBCpsProductViewLoadRefreshViewFirstData];
}
#pragma mark - DropDownMenuView Delegate
- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column {
    if(column==0){
        return [categoryArray count];
    }
    return [orderArray count];
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForRowAtIndexPath:(DOPIndexPath *)indexPath {
    if(indexPath.column==0){
        return categoryArray[indexPath.row];
    }
    return orderArray[indexPath.row];
}

- (NSInteger)numberOfColumnsInMenu:(DOPDropDownMenu *)menu {
    return 2;
}

- (void)menu:(DOPDropDownMenu *)menu didSelectRowAtIndexPath:(DOPIndexPath *)indexPath {
    if(indexPath.column==0) {
        category = categoryArray[indexPath.row];
        if (indexPath.row==0) {
            category = nil;
        }
    } else {
        order = orderenArray[indexPath.row];
    }
    //
    [self.hudView show];
    [self.cpsProductView RBCpsProductViewLoadRefreshViewFirstData];
}


#pragma mark - NSNotification Delegate
- (void)RBNotificationStatusBarAction {
    CGPoint off = self.cpsProductView.tableviewn.contentOffset;
    off.y = 0 - self.cpsProductView.tableviewn.contentInset.top;
    [self.cpsProductView.tableviewn setContentOffset:off animated:YES];
}

#pragma mark - RBCpsView Delegate
-(void)RBCpsProductViewLoadRefreshViewData:(RBCpsProductView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBCpsProductListWithGoods:searchTF.text andCategory:category andOrder:order andPage:[NSString stringWithFormat:@"%d",self.cpsProductView.pageIndex+1]];
}

- (void)RBCpsProductViewSelected:(RBCpsProductView *)view andIndex:(int)index {
    RBCpsProductEntity *entity = view.datalist[index];
    RBCpsProductDetailViewController*toview = [[RBCpsProductDetailViewController alloc] init];
    toview.productEntity = entity;
    toview.hidesBottomBarWhenPushed = YES;
    toview.delegateP = self;
    toview.isNew = self.isNew;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - RBCpsProductDetailViewControllerDelegate Delegate
- (void)RBCpsProductDetailViewController:(RBCpsProductDetailViewController *)vc select:(RBCpsProductEntity*)productEntity {
    if([self isVisitorLogin]==YES) {
        return;
    };
    if (self.isNew!=nil) {
        RBCpsEditViewController *toview = [[RBCpsEditViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        toview.productEntity = productEntity;
        [self.navigationController pushViewController:toview animated:YES];
    } else {
        if ([self.delegateT respondsToSelector:@selector(RBCpsProductSearchViewController:select:)]) {
            [self.delegateT RBCpsProductSearchViewController:self select:productEntity];
        }
        for (UIViewController *temp in self.navigationController.viewControllers){
            if ([temp isKindOfClass:[RBCpsEditViewController class]]) {
                RBCpsEditViewController*toview = (RBCpsEditViewController*)temp;
                [self.navigationController popToViewController:toview animated:YES];
                return;
            }
        }
    }
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"cps_materials"]) {
        [self.hudView dismiss];
        if (self.cpsProductView.pageIndex == 0) {
            self.cpsProductView.datalist = [NSMutableArray new];
            self.cpsProductView.jsonObject = jsonObject;
        }
        self.cpsProductView.datalist = [JsonService getRBCpsProductListEntity:jsonObject andBackArray:self.cpsProductView.datalist];
        [self.cpsProductView RBCpsProductViewSetRefreshViewFinish];
        if ([self.cpsProductView.datalist count] == 0) {
            self.cpsProductView.defaultView.hidden = NO;
        } else {
            self.cpsProductView.defaultView.hidden = YES;
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self.cpsProductView RBCpsProductViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.cpsProductView RBCpsProductViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

