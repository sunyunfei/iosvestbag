//
//  RBCpsProductDetailViewController.m
//  RB
//
//  Created by AngusNi on 16/9/6.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBCpsProductDetailViewController.h"

@interface RBCpsProductDetailViewController () {
    UIWebView*webviewn;
}
@end

@implementation RBCpsProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R8002",@"详情");
    self.navRightTitle = Icon_btn_write;
    //
    [self loadFooterView];
    //
    webviewn = [[UIWebView alloc] initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    webviewn.delegate = self;
    webviewn.scalesPageToFit = YES;
    [self.view addSubview:webviewn];
    [webviewn loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.productEntity.material_url]]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"cps-product-detail"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"cps-product-detail"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)RBNavRightBtnAction {
    if ([self.delegateP respondsToSelector:@selector(RBCpsProductDetailViewController:select:)]) {
        [self.delegateP RBCpsProductDetailViewController:self select:self.productEntity];
    }
}

- (void)footerBtnAction:(UIButton*)sender {
    if ([self.delegateP respondsToSelector:@selector(RBCpsProductDetailViewController:select:)]) {
        [self.delegateP RBCpsProductDetailViewController:self select:self.productEntity];
    }
}

- (void)footerBtn1Action:(UIButton*)sender {
    RBArticleSearchViewController*toview = [[RBArticleSearchViewController alloc] init];
    toview.str = self.productEntity.goods_name;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - UIButton Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.hudView show];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.hudView dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.hudView dismiss];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width/2.0, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
    //
    if (self.isNew!=nil) {
        [footerBtn setTitle:NSLocalizedString(@"R8007", @"开始创作") forState:UIControlStateNormal];
    } else {
        [footerBtn setTitle:NSLocalizedString(@"R8031", @"插入商品") forState:UIControlStateNormal];
    }
    //
    UIButton*footerBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn1.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn1.frame = CGRectMake(footerBtn.width, 0, footerBtn.width, footerView.height);
    [footerBtn1 addTarget:self
                  action:@selector(footerBtn1Action:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn1.titleLabel.font = font_cu_15;
    [footerBtn1 setTitle:NSLocalizedString(@"R8021", @"参考文章") forState:UIControlStateNormal];
    [footerBtn1 setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn1];
    //
    UILabel*footerLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(footerView.width/2.0-0.5f, 10.0, 1, 45.0-20.0)];
    footerLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [footerView addSubview:footerLineLabel];
}

@end
