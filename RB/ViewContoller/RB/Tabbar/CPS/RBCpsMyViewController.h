//
//  RBCpsMyViewController.h
//  RB
//
//  Created by AngusNi on 16/9/6.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBCpsMyView.h"
#import "RBRankingSwitchView.h"
#import "RBCpsDetailViewController.h"
//
@interface RBCpsMyViewController : RBBaseViewController
<RBBaseVCDelegate,RBCpsMyViewDelegate,RBRankingSwitchViewDelegate,UIScrollViewDelegate>
@end
