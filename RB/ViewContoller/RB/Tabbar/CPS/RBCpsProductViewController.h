//
//  RBCpsProductViewController.h
//  RB
//
//  Created by AngusNi on 16/9/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBCpsProductView.h"
#import "RBUserContactCpsViewController.h"
#import "RBCpsProductDetailViewController.h"

@class RBCpsProductViewController;
@protocol RBCpsProductViewControllerDelegate <NSObject>
@optional
- (void)RBCpsProductViewController:(RBCpsProductViewController *)vc select:(RBCpsProductEntity*)productEntity;
@end

@interface RBCpsProductViewController : RBBaseViewController
<RBBaseVCDelegate,RBCpsProductViewDelegate,RBCpsProductDetailViewControllerDelegate>
@property(nonatomic, strong) RBCpsProductView *cpsProductView;
@property(nonatomic, weak)  id<RBCpsProductViewControllerDelegate> delegateT;
@property(nonatomic, strong) NSString *isNew;

@property(nonatomic, strong) NSString *createId;

@end
