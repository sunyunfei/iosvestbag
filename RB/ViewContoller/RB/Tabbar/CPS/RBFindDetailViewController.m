//
//  RBFindDetailViewController.m
//  RB
//
//  Created by RB8 on 2018/4/24.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBFindDetailViewController.h"
#import "RBFindDetailView.h"
@interface RBFindDetailViewController ()<RBFindDetailViewDelegate>
@property(nonatomic,strong)RBFindDetailView * detailView;
@property(nonatomic,strong)NSMutableArray * dataArray;
@property(nonatomic,strong)NSString * KCType;
@property(nonatomic,strong)NSDate * startDate;
@end

@implementation RBFindDetailViewController
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (void)viewWillAppear:(BOOL)animated{
    
}
- (void)RBNavLeftBtnAction{
//    NSDateFormatter * formater = [[NSDateFormatter alloc]init];
//    [formater setDateFormat:@"mm:ss"];
    NSDate * endDate = [NSDate date];
    NSTimeInterval delta = [endDate timeIntervalSinceDate:_startDate];
    NSLog(@"%.0fSSSSSSSSSSSS",delta);
    NSString * timeStr = [NSString stringWithFormat:@"%.0f",delta];
    if ([JsonService isRBUserVisitor] || [LocalService getRBLocalDataUserPrivateToken] == nil) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        Handler * handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBRecommendPost_id:self.postId AndStayTime:timeStr];
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    if ([self.findDelegate respondsToSelector:@selector(reloadAction)]) {
        self.layout.model.isShowAll = NO;
        self.layout.model.shouldShowMoreButton = _isHaveMore;
        [self.layout resetLayout];
        [self.findDelegate reloadAction];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.navigationController.navigationBar.hidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [Utils getUIColorWithHexString:@"e9e9e9"];
    self.navTitle = @"标题";
    self.delegate = self;
    self.layout.model.isShowAll = YES;
    _isHaveMore = self.layout.model.shouldShowMoreButton;
    [self.layout resetLayout];
    _detailView = [[RBFindDetailView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight - NavHeight)];
    _detailView.detailLayout = self.layout;
    _detailView.delegate = self;
    [self.view addSubview:_detailView];
    [_detailView.loadTable reloadData];
    [self loadData];
}
-(BOOL)isLogin{
    if ([JsonService isRBUserVisitor]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationShowLoginView
                                                            object:self
                                                          userInfo:nil];
        return NO;
    }else if ([LocalService getRBLocalDataUserPrivateToken] == nil){
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationShowLoginView
                                                            object:self
                                                          userInfo:nil];
        return NO;
    }
    return YES;
}
- (void)DidClickButton:(NSString *)act{
    if ([self isLogin]){
        _KCType = act;
        if ([act isEqualToString:@"like"]) {
            //点赞
            if ([_layout.model.is_liked isEqualToString:@"0"]) {
                Handler * handler = [Handler shareHandler];
                handler.delegate = self;
                [handler getRBAddOrCancellike:@"like" andpost_id:_layout.model.post_id andType:@"add"];
            }else{
                Handler * handler = [Handler shareHandler];
                handler.delegate = self;
                [handler getRBAddOrCancellike:@"like" andpost_id:_layout.model.post_id andType:@"cancel"];
            }
        }else if ([act isEqualToString:@"collect"]){
            //收藏
            if ([_layout.model.is_collected isEqualToString:@"0"]) {
                Handler * handler = [Handler shareHandler];
                handler.delegate = self;
                [handler getRBAddOrCancellike:@"collect" andpost_id:_layout.model.post_id andType:@"add"];
            }else{
                Handler * handler = [Handler shareHandler];
                handler.delegate = self;
                [handler getRBAddOrCancellike:@"collect" andpost_id:_layout.model.post_id andType:@"cancel"];
            }
        }else if([act isEqualToString:@"forward"]){
            //分享
            Handler * handler = [Handler shareHandler];
            RBShareView *shareView = [RBShareView sharedRBShareView];
            shareView.delegate = self;
            [shareView showViewWithTitle:[NSString stringWithFormat:@"%@",_layout.model.title] Content:[NSString stringWithFormat:@"%@",_layout.model.title] URL:[NSString stringWithFormat:@"%@%@",handler.postUrl,_layout.model.forward_url] ImgURL:@"http://7xq4sa.com1.z0.glb.clouddn.com/robin8_icon.png"];
        }
    }
}
- (void)loadData{
    [self.hudView show];
    Handler * hand = [Handler shareHandler];
    hand.delegate = self;
    [hand getRBRecommendTag:@"books" andPost_id:self.postId];
}
//分享成功
-(void)shareSuccess{
    Handler * handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBAddOrCancellike:@"forward" andpost_id:_layout.model.post_id andType:@"add"];
}
//判断是否是纯数字
- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    if ([sender isEqualToString:@"articles/recommends"]) {
        _startDate = [NSDate date];
        [self.hudView dismiss];
        NSArray * arr = [jsonObject objectForKey:@"list"];
        for (NSInteger i = 0; i < arr.count; i ++) {
            DynamicsModel * model = [DynamicsModel modelWithDictionary:arr[i]];
            model.isShowAll = YES;
            NewDynamicsLayout * layout = [[NewDynamicsLayout alloc]initWithModel:model];
            [self.dataArray addObject:layout];
        }
        _detailView.dataArray = self.dataArray;
        [_detailView.loadTable reloadData];
    }
    if ([sender isEqualToString:@"articles/set"]) {
        if ([_KCType isEqualToString:@"like"]) {
            if ([_layout.model.is_liked isEqualToString:@"0"]) {
                _layout.model.is_liked = @"1";
                if ([self isPureInt:_layout.model.likes_count]) {
                    int likeCount = [_layout.model.likes_count intValue];
                    likeCount = likeCount + 1;
                    _layout.model.likes_count = [NSString stringWithFormat:@"%d",likeCount];
                }
            }else{
                _layout.model.is_liked = @"0";
                if ([self isPureInt:_layout.model.likes_count]) {
                    int likeCount = [_layout.model.likes_count intValue];
                    likeCount = likeCount - 1;
                    _layout.model.likes_count = [NSString stringWithFormat:@"%d",likeCount];
                }
            }
        }else if([_KCType isEqualToString:@"collect"]){
            if ([_layout.model.is_collected isEqualToString:@"0"]) {
                _layout.model.is_collected = @"1";
            }else{
                _layout.model.is_collected = @"0";
            }
        }else if([_KCType isEqualToString:@"forward"]){
            if ([self isPureInt:_layout.model.forwards_count]) {
                int likeCount = [_layout.model.forwards_count intValue];
                likeCount = likeCount + 1;
                _layout.model.forwards_count = [NSString stringWithFormat:@"%d",likeCount];
                [self.hudView showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
            }
        }
        self.layout.model.isShowAll = YES;
        //分享时调用_layout.model.title会使model.shouldShowMoreButton变为YES
        _layout.model.shouldShowMoreButton = NO;
        [_layout resetLayout];
        self.detailView.detailLayout = _layout;
        [self.detailView.loadTable reloadData];
        return;
    }
    if ([sender isEqualToString:@"articles/read"]) {
        if ([[jsonObject objectForKey:@"red_money"]floatValue] > 0) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"addRedBag" object:nil userInfo:@{@"readmoney":[jsonObject objectForKey:@"red_money"]}];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender{
    if ([sender isEqualToString:@"articles/read"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)handlerError:(NSError *)error Tag:(NSString *)sender{
    if ([sender isEqualToString:@"articles/read"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
