//
//  RBCpsProductView.h
//  RB
//
//  Created by AngusNi on 16/9/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBCpsProductTableViewCell.h"

@class RBCpsProductView;
@protocol RBCpsProductViewDelegate <NSObject>
@optional
- (void)RBCpsProductViewLoadRefreshViewData:(RBCpsProductView *)view;
- (void)RBCpsProductViewSelected:(RBCpsProductView *)view andIndex:(int)index;
@end


@interface RBCpsProductView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak)   id<RBCpsProductViewDelegate> delegate;
@property(nonatomic, strong) UIView*defaultView;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) id jsonObject;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
- (void)RBCpsProductViewLoadRefreshViewFirstData;
- (void)RBCpsProductViewSetRefreshViewFinish;

@end
