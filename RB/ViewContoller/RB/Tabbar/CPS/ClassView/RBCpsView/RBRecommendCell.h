//
//  RBRecommendCell.h
//  RB
//
//  Created by RB8 on 2018/4/24.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
@interface RBfindImageView : UIView
@property (nonatomic,strong)UIImageView * headImageView;
@property (nonatomic,strong)UILabel * TitleLabel;
@property (nonatomic,strong)UILabel * SmallLabel;
//@property(nonatomic,strong)UIImageView * rightImageView;
@end
@interface RBRecommendCell : UITableViewCell<UIScrollViewDelegate>
@property(nonatomic,strong)UIScrollView * scrollviewn;
@property(nonatomic,strong)NSArray * recommendArray;
- (void)setValueForCell:(NSArray*)arr;
@end
