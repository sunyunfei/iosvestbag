//
//  RBRecommendCell.m
//  RB
//
//  Created by RB8 on 2018/4/24.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBRecommendCell.h"
#import "DynamicsModel.h"
#import "RBFindDetailViewController.h"
#import "NewDynamicsLayout.h"
@implementation RBfindImageView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        _headImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CellLeft, CellLeft, 70, 70)];
        [self addSubview:_headImageView];
        //
        _TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(_headImageView.right + 10, _headImageView.top + 7, 170*ScaleWidth, 38) text:@"" font:font_15 textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorBlack] backgroundColor:[UIColor clearColor] numberOfLines:2];
        [self addSubview:_TitleLabel];
        //
        _SmallLabel = [[UILabel alloc]initWithFrame:CGRectMake(_TitleLabel.left, _TitleLabel.bottom+3, 170*ScaleWidth, 12) text:@"" font:font_(12) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorGray] backgroundColor:[UIColor clearColor] numberOfLines:1];
        
        [self addSubview:_SmallLabel];
        //
//        _rightImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 71/2, _headImageView.top, 71, 71)];
//        [self addSubview:_rightImageView];
    }
    return self;
}
@end
@implementation RBRecommendCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
- (void)initUI{
    //
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth - CellLeft, 35) text:@"相关推荐" font:font_(14) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:@"777777"] backgroundColor:[UIColor whiteColor] numberOfLines:1];
    [self.contentView addSubview:label];
    //
    UILabel * lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, label.bottom, ScreenWidth, 0.5) text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorBkg] numberOfLines:1];
    [self.contentView addSubview:lineLabel1];
    //
//    [self.contentView addSubview:self.scrollviewn];
}
- (void)tapAction:(UITapGestureRecognizer*)tap{
    RBfindImageView * imageView = (RBfindImageView*)tap.view;
    NewDynamicsLayout * layout = _recommendArray[imageView.tag - 100];
    RBFindDetailViewController * find = [[RBFindDetailViewController alloc] init];
    find.postId = layout.model.post_id;
    find.layout = layout;
    id object = [self nextResponder];
    while (![object isKindOfClass:[UIViewController class]] && object != nil) {
        object = [object nextResponder];
    }
    UIViewController *superController = (UIViewController*)object;
    /**
     *  这里声明所要push的页面（Mycontroller）
     */
    [superController.navigationController pushViewController:find animated:YES];

}
-(UIScrollView*)scrollviewn{
    if (!_scrollviewn) {
        _scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 35.5,ScreenWidth - 70/2 - CellLeft, 95)];
        _scrollviewn.showsVerticalScrollIndicator = NO;
        _scrollviewn.showsHorizontalScrollIndicator = NO;
        _scrollviewn.bounces = NO;
        _scrollviewn.pagingEnabled = YES;
        _scrollviewn.delegate = self;
        _scrollviewn.contentSize = CGSizeMake(_recommendArray.count * (ScreenWidth - 70/2 - CellLeft), 95);
        _scrollviewn.clipsToBounds = NO;
        //        [self.contentView addSubview:_scrollviewn];
        for (NSInteger i = 0; i < _recommendArray.count; i ++) {
            NewDynamicsLayout * layout = _recommendArray[i];
            RBfindImageView * imageView = [[RBfindImageView alloc]initWithFrame:CGRectMake(i * (ScreenWidth - 70/2 - CellLeft), 0, (ScreenWidth - 70/2 - CellLeft), 95)];
            NSArray * arr = layout.model.pics[0];
            if (arr.count > 0) {
                [imageView.headImageView sd_setImageWithURL:[NSURL URLWithString:layout.model.pics[0][0]] placeholderImage:PlaceHolderImage];
            }else{
                imageView.headImageView.image = PlaceHolderUserImage;
            }
            
            imageView.TitleLabel.text = layout.model.title;
            imageView.SmallLabel.text = layout.model.user_name;
            imageView.tag = 100 + i;
            imageView.userInteractionEnabled = YES;
            UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
            tap.numberOfTouchesRequired = 1;
            tap.numberOfTapsRequired = 1;
            [imageView addGestureRecognizer:tap];
            [_scrollviewn addSubview:imageView];
        }
    }
    return _scrollviewn;
}
- (void)setValueForCell:(NSArray*)arr{
    _recommendArray = arr;
    [self.contentView addSubview:self.scrollviewn];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
