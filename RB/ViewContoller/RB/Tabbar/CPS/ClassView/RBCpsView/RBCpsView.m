//
//  RBCpsView.m
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBCpsView.h"
@interface RBCpsView () {
}
@end

@implementation RBCpsView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        //
        self.tableviewn = [[UITableView alloc]
                           initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)
                           style:UITableViewStylePlain];
        self.tableviewn.backgroundView = nil;
        self.tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableviewn.backgroundColor = [UIColor clearColor];
        self.tableviewn.delegate = self;
        self.tableviewn.dataSource = self;
        [self addSubview:self.tableviewn];
        [self setRefreshHeaderView];
    }
    return self;
}

- (UIView*)defaultView {
    if (!_defaultView) {
        _defaultView = [[UIView alloc]initWithFrame:CGRectMake(0, (self.height-120.0)/2.0, ScreenWidth, 120.0)];
        _defaultView.tag = 9999;
        _defaultView.hidden = YES;
        [self addSubview:_defaultView];
        //
        UIImageView*defaultIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-100.0)/2.0, 0, 100.0, 100.0)];
        defaultIV.image = [UIImage imageNamed:@"icon_task_default.png"];
        [_defaultView addSubview:defaultIV];
        //
        UILabel*defaultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, defaultIV.bottom, ScreenWidth, 20.0)];
        defaultLabel.font = font_17;
        defaultLabel.textAlignment = NSTextAlignmentCenter;
        defaultLabel.text = NSLocalizedString(@"R1028", @"当前页面无数据,试试别的页面吧");
        defaultLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [_defaultView addSubview:defaultLabel];
    }
    return _defaultView;
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)RBCpsViewLoadRefreshViewFirstData {
    self.pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    self.pageSize = 10;
    if ([self.delegate
         respondsToSelector:@selector(RBCpsViewLoadRefreshViewData:)]) {
        [self.delegate RBCpsViewLoadRefreshViewData:self];
    }
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    self.tableviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    self.tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.pageIndex = self.pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)RBCpsViewSetRefreshViewFinish {
    [self.tableviewn.mj_header endRefreshing];
    [self.tableviewn.mj_footer endRefreshing];
    if ([self.datalist count] == (self.pageIndex+1)*self.pageSize) {
        if (self.tableviewn.mj_footer == nil) {
            [self setRefreshFooterView];
        }
    } else {
        [self.tableviewn.mj_footer removeFromSuperview];
        self.tableviewn.mj_footer = nil;
    }
    if ([self.datalist count]==0) {
        self.jsonObject = [Utils getFileReadWithFileName:@"RBCps" andFilePath:@"RB"];
        self.datalist = [NSMutableArray new];
        self.datalist = [JsonService getRBActivityListEntity:self.jsonObject andBackArray:self.datalist];
    } else {
        [Utils getFileWrite:self.jsonObject andFileName:@"RBCps" andFilePath:@"RB"];
    }
    [UIView performWithoutAnimation:^{
        [self.tableviewn reloadData];
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self.datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBCpsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[RBCpsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:CellIdentifier];
    }
    [cell loadRBCpsCellWith:self.datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (ScreenWidth*9.0)/16.0+5.0;
    //    UITableViewCell *cell =
    //    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    //    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate
         respondsToSelector:@selector(RBCpsViewSelected:andIndex:)]) {
        [self.delegate RBCpsViewSelected:self andIndex:(int)indexPath.row];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat viewHeight = tableView.height + tableView.contentInset.top;
    CGFloat y = cell.centerY - tableView.contentOffset.y;
    CGFloat p = y - viewHeight / 2;
    if(p>0) {
        cell.layer.transform = CATransform3DMakeScale(0.9, 0.9, 1);
        [UIView animateWithDuration:1 animations:^{
            cell.layer.transform = CATransform3DMakeScale(1, 1, 1);
        }];
    }
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat viewHeight = scrollView.height + scrollView.contentInset.top;
//    for (RBHomeCampaignTableViewCell *cell in [self.tableviewn visibleCells]) {
//        CGFloat y = cell.centerY - scrollView.contentOffset.y;
//        CGFloat p = y - viewHeight / 2;
//        NSLog(@"p：%f",p);
//
//        if(p>=30) {
//            CGFloat scale = p/375.0;
//            NSLog(@"scale：%f",scale);
//            [UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:^{
//                cell.transform = CGAffineTransformMakeScale(scale, scale);
//            } completion:NULL];
//        }
//    }
//}


@end
