//
//  RBImageCollectionView.h
//  RB
//
//  Created by RB8 on 2018/3/28.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBImageCollectionView : UIView<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property(nonatomic,strong)UICollectionView * imageCollection;
@property(nonatomic,strong)NSArray * imgArr;
@end
