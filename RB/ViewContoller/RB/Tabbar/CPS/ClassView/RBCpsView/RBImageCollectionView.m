//
//  RBImageCollectionView.m
//  RB
//
//  Created by RB8 on 2018/3/28.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBImageCollectionView.h"
#import "Service.h"
@implementation RBImageCollectionView
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 5;
        layout.minimumInteritemSpacing = 5;
        _imageCollection = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _imageCollection.delegate = self;
        _imageCollection.dataSource = self;
        _imageCollection.backgroundColor = [UIColor whiteColor];
        [self addSubview:_imageCollection];
    }
    return self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _imgArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * str = @"reuse";
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:str forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UICollectionViewCell alloc]init];
        UIImageView * imageView = [[UIImageView alloc]init];
        if (_imgArr.count == 1) {
            imageView.frame = CGRectMake(0, 0, 155, 155);
        }else{
            imageView.frame = CGRectMake(0, 0, (ScreenWidth - 10 - 2*CellLeft)/3, (ScreenWidth - 10 - 2*CellLeft)/3);
        }
        [cell.contentView addSubview:imageView];
    }
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_imgArr.count == 1) {
        return CGSizeMake(155, 155);
    }else{
        return CGSizeMake((ScreenWidth - 10 - 2*CellLeft)/3, (ScreenWidth - 10 - 2*CellLeft)/3);
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end