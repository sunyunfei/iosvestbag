//
//  RBCpsView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBCpsTableViewCell.h"

@class RBCpsView;
@protocol RBCpsViewDelegate <NSObject>
@optional
- (void)RBCpsViewLoadRefreshViewData:(RBCpsView *)view;
- (void)RBCpsViewSelected:(RBCpsView *)view andIndex:(int)index;
@end


@interface RBCpsView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBCpsViewDelegate> delegate;
@property(nonatomic, strong) UIView*defaultView;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) id jsonObject;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
- (void)RBCpsViewLoadRefreshViewFirstData;
- (void)RBCpsViewSetRefreshViewFinish;

@end
