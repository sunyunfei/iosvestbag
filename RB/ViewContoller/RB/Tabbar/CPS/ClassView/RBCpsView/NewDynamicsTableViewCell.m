//
//  NewDynamicsTableViewCell.m
//  LooyuEasyBuy
//
//  Created by Andy on 2017/9/27.
//  Copyright © 2017年 Doyoo. All rights reserved.
//

#import "NewDynamicsTableViewCell.h"

@implementation NewDynamicsGrayView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = ScreenWidth - kDynamicsNormalPadding * 2 - kDynamicsPortraitNamePadding - kDynamicsPortraitWidthAndHeight;;
        frame.size.height = kDynamicsGrayBgHeight;
    }
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
- (void)setup
{
    [self addSubview:self.grayBtn];
    [self addSubview:self.thumbImg];
    [self addSubview:self.dspLabel];
    
    [self layout];
}
- (void)layout
{
    _grayBtn.frame = self.frame;
    
    _thumbImg.left = kDynamicsGrayPicPadding;
    _thumbImg.top = kDynamicsGrayPicPadding;
    _thumbImg.width = kDynamicsGrayPicHeight;
    _thumbImg.height = kDynamicsGrayPicHeight;

    _dspLabel.left = _thumbImg.right + kDynamicsNameDetailPadding;
    _dspLabel.width = self.right - kDynamicsNameDetailPadding - _dspLabel.left;
}

//-(UIButton *)grayBtn
//{
//    if (!_grayBtn) {
//        _grayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        _grayBtn.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:242/255.0 alpha:1];
//        WS(weakSelf);
//        [_grayBtn bk_addEventHandler:^(id sender) {
//            if (weakSelf.cell.delegate != nil && [weakSelf.cell.delegate respondsToSelector:@selector(DidClickGrayViewInDynamicsCell:)]) {
//                [weakSelf.cell.delegate DidClickGrayViewInDynamicsCell:weakSelf.cell];
//            }
//        } forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _grayBtn;
//}
-(UIImageView *)thumbImg
{
    if (!_thumbImg) {
        _thumbImg = [UIImageView new];
        _thumbImg.userInteractionEnabled = NO;
        _thumbImg.backgroundColor = [UIColor grayColor];
    }
    return _thumbImg;
}
-(YYLabel *)dspLabel
{
    if (!_dspLabel) {
        _dspLabel = [YYLabel new];
        _dspLabel.userInteractionEnabled = NO;
    }
    return _dspLabel;
}

@end

@implementation NewDynamicsThumbCommentView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = ScreenWidth - kDynamicsNormalPadding * 2 - kDynamicsPortraitNamePadding - kDynamicsPortraitWidthAndHeight;;
        frame.size.height = 0;
    }
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
- (void)setup
{
    [self addSubview:self.bgImgView];
    [self addSubview:self.thumbLabel];
    [self addSubview:self.dividingLine];
    
}
- (void)setWithLikeArr:(NSMutableArray *)likeArr CommentArr:(NSMutableArray *)commentArr DynamicsLayout:(NewDynamicsLayout *)layout
{
    _likeArray = likeArr;
    self.commentArray = layout.commentLayoutArr;
    _layout = layout;
    [self layoutView];
}
- (void)layoutView
{
    _bgImgView.top = 0;
    _bgImgView.left = 0;
    _bgImgView.width = self.frame.size.width;
    _bgImgView.height = _layout.thumbCommentHeight;
    
    UIView * lastView = _bgImgView;
    
    if (_likeArray.count != 0) {
        _thumbLabel.hidden = NO;
        _thumbLabel.top = 10;
        _thumbLabel.left = kDynamicsNameDetailPadding;
        _thumbLabel.width = self.frame.size.width - kDynamicsNameDetailPadding*2;
        _thumbLabel.height = _layout.thumbLayout.textBoundingSize.height;
        _thumbLabel.textLayout = _layout.thumbLayout;
        lastView = _thumbLabel;
    }else{
        _thumbLabel.hidden = YES;
    }
    
    
    if (_likeArray.count != 0 && _commentArray.count != 0) {
        _dividingLine.hidden = NO;
        _dividingLine.top = _thumbLabel.bottom;
        _dividingLine.left = 0;
        _dividingLine.width = self.frame.size.width;
        _dividingLine.height = .5;
        lastView = _dividingLine;
    }else{
        _dividingLine.hidden = YES;
    }
    
    
}

-(UIImageView *)bgImgView
{
    if (!_bgImgView) {
        _bgImgView = [UIImageView new];
        UIImage *bgImage = [[[UIImage imageNamed:@"LikeCmtBg"] stretchableImageWithLeftCapWidth:40 topCapHeight:30] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        _bgImgView.image = bgImage;
        _bgImgView.backgroundColor = [UIColor clearColor];
    }
    return _bgImgView;
}
-(YYLabel *)thumbLabel
{
    if (!_thumbLabel) {
        _thumbLabel = [YYLabel new];
    }
    return _thumbLabel;
}
-(UIView *)dividingLine
{
    if (!_dividingLine) {
        _dividingLine = [UIView new];
        _dividingLine.backgroundColor = [UIColor colorWithRed:210/255.0 green:210/255.0 blue:210/255.0 alpha:1];
//        RGBA_COLOR(210, 210, 210, 1);
    }
    return _dividingLine;
}

@end

@implementation NewDynamicsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}
- (void)setup
{
    [self.contentView addSubview:self.portrait];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.dateLabel];
    [self.contentView addSubview:self.collectBtn];
    [self.contentView addSubview:self.detailLabel];
    [self.contentView addSubview:self.moreLessDetailBtn];
    [self.contentView addSubview:self.picContainerView];
    [self.contentView addSubview:self.watchLabel];
    [self.contentView addSubview:self.zanBtn];
    [self.contentView addSubview:self.dividingLine];
    [self.contentView addSubview:self.shareBtn];
    [self.contentView addSubview:self.seperateLabel];
}
- (NSString*)changeToMillion:(NSString*)mount{
    if ([mount intValue] < 10000) {
        return mount;
    }else{
        int a = [mount intValue];
        NSString * b = [NSString stringWithFormat:@"%.1f万",a/10000.0];
        return b;
    }
}
-(void)setLayout:(NewDynamicsLayout *)layout
{
    UIView * lastView;
    _layout = layout;
    DynamicsModel * model = layout.model;
    
    //头像
    _portrait.left = kDynamicsNormalPadding;
    _portrait.top = KDynamicsPortraittopPadding;
    _portrait.size = CGSizeMake(kDynamicsPortraitWidthAndHeight, kDynamicsPortraitWidthAndHeight);
    _portrait.layer.cornerRadius = kDynamicsPortraitWidthAndHeight/2;
    _portrait.layer.masksToBounds = YES;
    [_portrait sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.avatar_url]]];
    //昵称
    _nameLabel.text = model.user_name;
    _nameLabel.top = KDynamicsNameTopPadding;
    _nameLabel.left = _portrait.right + kDynamicsPortraitNamePadding;
    CGSize nameSize = [_nameLabel sizeThatFits:CGSizeZero];
    _nameLabel.width = nameSize.width;
    _nameLabel.height = kDynamicsNameHeight;
    //时间
    _dateLabel.left = _nameLabel.left;
    _dateLabel.top = _nameLabel.bottom + 6;
    NSString * newTime = [self formateDate:model.post_date withFormate:@"yyyy-MM-dd HH:mm:ss"];
    _dateLabel.text = newTime;
    CGSize dateSize = [_dateLabel sizeThatFits:CGSizeMake(100, kDynamicsNameHeight)];
    _dateLabel.width = dateSize.width;
    _dateLabel.height = kDynamicsNameHeight;
    //收藏
    _collectBtn.layer.cornerRadius = 2;
    _collectBtn.layer.borderWidth = 0.5;
    _collectBtn.layer.borderColor = [Utils getUIColorWithHexString:SysColorYellow].CGColor;
    _collectBtn.frame = CGRectMake(ScreenWidth - kDynamicsNormalPadding - 50, 15+20.5-11.5, 50, 23);
    if ([[NSString stringWithFormat:@"%@",model.is_collected] isEqualToString:@"0"]) {
        [_collectBtn setTitle:@"收藏" forState:UIControlStateNormal];
        [_collectBtn setTitleColor:[Utils getUIColorWithHexString:SysColorYellow] forState:UIControlStateNormal];
        _collectBtn.layer.borderColor = [Utils getUIColorWithHexString:SysColorYellow].CGColor;
    }else{
        [_collectBtn setTitle:@"已收藏" forState:UIControlStateNormal];
        _collectBtn.layer.borderColor = [Utils getUIColorWithHexString:@"777777"].CGColor;
        [_collectBtn setTitleColor:[Utils getUIColorWithHexString:@"777777"] forState:UIControlStateNormal];
    }
    
//    _deleteBtn.left = _dateLabel.right + kDynamicsPortraitNamePadding;
//    _deleteBtn.top = _dateLabel.top;
//    CGSize deleteSize = [_deleteBtn sizeThatFits:CGSizeMake(100, kDynamicsNameHeight)];
//    _deleteBtn.width = deleteSize.width;
//    _deleteBtn.height = kDynamicsNameHeight;
    
   
    
    //描述
    _detailLabel.left = _portrait.left;
    _detailLabel.top = _portrait.bottom + KDynamicsPortraitDsbPadding;
    _detailLabel.width = ScreenWidth - kDynamicsNormalPadding * 2;
//    YYTextLinePositionSimpleModifier *mod = [YYTextLinePositionSimpleModifier new];
//    mod.fixedLineHeight = 22;
//    _detailLabel.linePositionModifier = mod;
    _detailLabel.height = layout.detailLayout.textBoundingSize.height;
    _detailLabel.textLayout = layout.detailLayout;    
    lastView = _detailLabel;
    
    //展开/收起按钮
    _moreLessDetailBtn.left = _portrait.left;
    _moreLessDetailBtn.top = _detailLabel.bottom + kDynamicsNameDetailPadding;
    _moreLessDetailBtn.height = kDynamicsMoreLessButtonHeight;
    [_moreLessDetailBtn sizeToFit];
    
    if (model.shouldShowMoreButton) {
        _moreLessDetailBtn.hidden = NO;
        
        if (model.isOpening) {
            [_moreLessDetailBtn setTitle:@"收起" forState:UIControlStateNormal];
        }else{
            [_moreLessDetailBtn setTitle:@"全文" forState:UIControlStateNormal];
        }
        
        lastView = _moreLessDetailBtn;
    }else{
        _moreLessDetailBtn.hidden = YES;
    }
    //图片集
    NSArray * smallArray = model.pics[0];
    if (smallArray.count != 0) {
        NSArray * bigArray = model.pics[1];
        _picContainerView.hidden = NO;
        _picContainerView.left = _portrait.left;
        _picContainerView.top = lastView.bottom + kDynamicsNormalPadding;
        _picContainerView.width = layout.photoContainerSize.width;
        _picContainerView.height = layout.photoContainerSize.height;
        _picContainerView.picPathStringsArray = smallArray;
        _picContainerView.picPathBigArray = bigArray;
        lastView = _picContainerView;
    }else{
        _picContainerView.hidden = YES;
    }
//    //头条
//    if (model.pagetype == 1) {
//        _grayView.hidden = NO;
//
//        _grayView.left = _nameLabel.left;
//        _grayView.top = lastView.bottom + kDynamicsNameDetailPadding;
//        _grayView.width = _detailLabel.right - _grayView.left;
//        _grayView.height = kDynamicsGrayBgHeight;
//
//        [_grayView.thumbImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"http://static.soperson.com",model.thumb]]];
//        _grayView.dspLabel.height = layout.dspLayout.textBoundingSize.height;
//        _grayView.dspLabel.centerY = _grayView.thumbImg.centerY;
//        _grayView.dspLabel.textLayout = layout.dspLayout;
//
//        lastView = _grayView;
//    }else{
//        _grayView.hidden = YES;
//    }
    
    //推广
//    _spreadBtn.left = _nameLabel.left;
//    _spreadBtn.top = lastView.bottom + kDynamicsNameDetailPadding;;
//
//    if (model.spreadparams.count != 0) {
//        _spreadBtn.hidden = NO;
//        [_spreadBtn setTitle:model.spreadparams[@"name"] forState:UIControlStateNormal];
//        CGSize fitSize = [_spreadBtn sizeThatFits:CGSizeZero];
//        _spreadBtn.width = fitSize.width > _detailLabel.size.width ? _detailLabel.size.width : fitSize.width;
//        _spreadBtn.height = kDynamicsSpreadButtonHeight;
//
//        lastView = _spreadBtn;
//    }else if (model.companyparams.count != 0){
//        _spreadBtn.hidden = NO;
//        [_spreadBtn setTitle:model.companyparams[@"name"] forState:UIControlStateNormal];
//        CGSize fitSize = [_spreadBtn sizeThatFits:CGSizeZero];
//        _spreadBtn.width = fitSize.width > _detailLabel.size.width ? _detailLabel.size.width : fitSize.width;
//        _spreadBtn.height = kDynamicsSpreadButtonHeight;
//
//        lastView = _spreadBtn;
//    }else{
//        _spreadBtn.hidden = YES;
//    }
    
   
    
    //更多
//    _menuBtn.left = _detailLabel.right - 30 + 5;
//    _menuBtn.top = lastView.bottom + kDynamicsPortraitNamePadding - 8;
//    _menuBtn.size = CGSizeMake(30, 30);
    //分割线
    _dividingLine.left = _portrait.left;
    _dividingLine.height = .5;
    _dividingLine.width = ScreenWidth - 2 * kDynamicsNormalPadding;
    _dividingLine.bottom = layout.height - 39 - 8 - .5;
    //
//    _watchLabel.left = _portrait.left;
//    _watchLabel.top = _dividingLine.bottom + 13;
    _watchLabel.text = [NSString stringWithFormat:@"%@次观看",[self changeToMillion:model.reads_count]];
    [_watchLabel sizeToFit];
    _watchLabel.frame = CGRectMake(_portrait.left, _dividingLine.bottom + 13, 100, _watchLabel.frame.size.height);
    //
    _shareBtn.top = _dividingLine.bottom + 10;
    _shareBtn.right = ScreenWidth - kDynamicsNormalPadding;
    if ([model.forwards_count isEqual:[NSNull null]] || model.forwards_count == nil) {
        model.forwards_count = @"0";
    }
    [_shareBtn setTitle:[NSString stringWithFormat:@"%@",[self changeToMillion:model.forwards_count]] forState:UIControlStateNormal];
    [_shareBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
    _shareBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_shareBtn sizeToFit];
    _shareBtn.frame = CGRectMake(ScreenWidth - kDynamicsNormalPadding - 60, _dividingLine.bottom + 10,72, _shareBtn.frame.size.height );
    //
    _zanBtn.right = _shareBtn.left - 35;
    _zanBtn.top = _shareBtn.top;
    if ([model.likes_count isEqual:[NSNull null]] || model.likes_count == nil) {
        model.likes_count = @"0";
    }
    
    [_zanBtn setTitle:[NSString stringWithFormat:@"%@",[self changeToMillion:model.likes_count]] forState:UIControlStateNormal];
    [_zanBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
    _zanBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_zanBtn sizeToFit];
    if ([[NSString stringWithFormat:@"%@",model.is_liked] isEqualToString:@"1"]) {
        [_zanBtn setImage:[UIImage imageNamed:@"zanHeart"] forState:UIControlStateNormal];
    }else{
        [_zanBtn setImage:[UIImage imageNamed:@"noZanHeart"] forState:UIControlStateNormal];
    }
    _zanBtn.frame = CGRectMake(_shareBtn.left - 20 - 75, _shareBtn.top + 1, 75, _zanBtn.frame.size.height);
    //
    _seperateLabel.left = 0;
    _seperateLabel.top = _dividingLine.bottom + 39;
    _seperateLabel.size = CGSizeMake(ScreenWidth, 8);
//    if (model.likeArr.count != 0 || model.commentArr.count != 0) {
//        _thumbCommentView.hidden = NO;
//        //点赞/评论
//        _thumbCommentView.left = _detailLabel.left;
//        _thumbCommentView.top = _dateLabel.bottom + kDynamicsPortraitNamePadding;
//        _thumbCommentView.width = _detailLabel.width;
//        _thumbCommentView.height = layout.thumbCommentHeight;
//
//        [_thumbCommentView setWithLikeArr:model.likeArr CommentArr:model.commentArr DynamicsLayout:layout];
//    }else{
//        _thumbCommentView.hidden = YES;
//    }
    
    
    
    
    WS(weakSelf);
//    layout.clickUserBlock = ^(NSString *userID) {//点赞评论区域点击用户昵称操作
//        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(DynamicsCell:didClickUser:)]) {
//            [weakSelf.delegate DynamicsCell:weakSelf didClickUser:userID];
//        }
//    };
    
    layout.clickUrlBlock = ^(NSString *url) {
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(DynamicsCell:didClickUrl:PhoneNum:)]) {
            [weakSelf.delegate DynamicsCell:weakSelf didClickUrl:url PhoneNum:nil];
        }
    };
    
    layout.clickPhoneNumBlock = ^(NSString *phoneNum) {
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(DynamicsCell:didClickUrl:PhoneNum:)]) {
            [weakSelf.delegate DynamicsCell:weakSelf didClickUrl:nil PhoneNum:phoneNum];
        }
    };
}
#pragma mark - 弹出JRMenu
//- (void)presentMenuController
//{
//    DynamicsModel * model = _layout.model;
//    if (!model.isThumb) {//点赞
//        if (!_jrMenuView) {
//            _jrMenuView = [[JRMenuView alloc] init];
//        }
//        [_jrMenuView setTargetView:_menuBtn InView:self.contentView];
//        _jrMenuView.delegate = self;
//        [_jrMenuView setTitleArray:@[@"点赞",@"评论"]];
//        [self.contentView addSubview:_jrMenuView];
//        [_jrMenuView show];
//    }else{//取消点赞
//        if (!_jrMenuView) {
//            _jrMenuView = [[JRMenuView alloc] init];
//        }
//        [_jrMenuView setTargetView:_menuBtn InView:self.contentView];
//        _jrMenuView.delegate = self;
//        [_jrMenuView setTitleArray:@[@"取消点赞",@"评论"]];
//        [self.contentView addSubview:_jrMenuView];
//        [_jrMenuView show];
//    }
//}
//#pragma mark - 点击JRMenu上的Btn
//-(void)hasSelectedJRMenuIndex:(NSInteger)jrMenuIndex
//{
//    DynamicsModel * model = _layout.model;
//    if (jrMenuIndex == 0) {
//        if (!model.isThumb) {
//            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(DidClickThunmbInDynamicsCell:)]) {
//                [_delegate DidClickThunmbInDynamicsCell:self];
//            }
//        }else{
//            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(DidClickCancelThunmbInDynamicsCell:)]) {
//                [_delegate DidClickCancelThunmbInDynamicsCell:self];
//            }
//        }
//    }else{
//        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(DidClickCommentInDynamicsCell:)]) {
//            [_delegate DidClickCommentInDynamicsCell:self];
//        }
//    }
//}
#pragma mark - getter
-(UIImageView *)portrait
{
    if(!_portrait){
        _portrait = [UIImageView new];
        _portrait.userInteractionEnabled = YES;
        _portrait.backgroundColor = [UIColor grayColor];
        
    }
    return _portrait;
}
-(YYLabel *)nameLabel
{
    if (!_nameLabel) {
        _nameLabel = [YYLabel new];
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];

    }
    return _nameLabel;
}
-(YYLabel *)detailLabel
{
    if (!_detailLabel) {
        _detailLabel = [YYLabel new];
//        _detailLabel.font = font_(15);
        _detailLabel.textLongPressAction = ^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
//            containerView.backgroundColor = RGBA_COLOR(1, 1, 1, .2);
            [SVProgressHUD showSuccessWithStatus:@"文字复制成功!"];
            UIPasteboard * board = [UIPasteboard generalPasteboard];
            board.string = text.string;
//            [Utils delayTime:.5 TimeOverBlock:^{
//                containerView.backgroundColor = [UIColor clearColor];
//            }];
        };
        WS(weakSelf);
        _detailLabel.textTapAction = ^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            NSLog(@"点击了文字");
            if ([weakSelf.delegate respondsToSelector:@selector(DynamicsCell:didClicktext:)]) {
                [weakSelf.delegate DynamicsCell:weakSelf didClicktext:nil];
            }
        };
       
    }
    return _detailLabel;
}
-(UIButton *)moreLessDetailBtn
{
    if (!_moreLessDetailBtn) {
        _moreLessDetailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _moreLessDetailBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_moreLessDetailBtn setTitleColor:[UIColor colorWithRed:74/255.0 green:90/255.0 blue:133/255.0 alpha:1] forState:UIControlStateNormal];
        _moreLessDetailBtn.hidden = YES;
     //   [_moreLessDetailBtn bk_addEventHandler:^(id sender) {
        
     //   } forControlEvents:UIControlEventTouchUpInside];
        [_moreLessDetailBtn addTarget:self action:@selector(foldAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreLessDetailBtn;
}
- (void)foldAction:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(DidClickMoreLessInDynamicsCell:)]) {
        [self.delegate DidClickMoreLessInDynamicsCell:self];
    }
}
-(SDWeiXinPhotoContainerView *)picContainerView
{
    if (!_picContainerView) {
        _picContainerView = [SDWeiXinPhotoContainerView new];
        _picContainerView.hidden = YES;
    }
    return _picContainerView;
}
//-(UIButton *)spreadBtn
//{
//    if (!_spreadBtn) {
//        _spreadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        _spreadBtn.titleLabel.font = [UIFont systemFontOfSize:12];
//        _spreadBtn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
//        [_spreadBtn setTitleColor:[UIColor colorWithRed:74/255.0 green:90/255.0 blue:133/255.0 alpha:1] forState:UIControlStateNormal];
//        WS(weakSelf);
//        [_spreadBtn bk_addEventHandler:^(id sender) {
//            if (weakSelf.delegate != nil && [weakSelf.delegate respondsToSelector:@selector(DidClickSpreadInDynamicsCell:)]) {
//                [weakSelf.delegate DidClickSpreadInDynamicsCell:weakSelf];
//            }
//        } forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _spreadBtn;
//}
-(NewDynamicsGrayView *)grayView
{
    if (!_grayView) {
        _grayView = [NewDynamicsGrayView new];
        _grayView.cell = self;
    }
    return _grayView;
}
- (UILabel*)seperateLabel{
    if (!_seperateLabel) {
        _seperateLabel = [[UILabel alloc]init];
        _seperateLabel.backgroundColor = [Utils getUIColorWithHexString:@"e9e9e9"];
        _seperateLabel.alpha = 0.3;
    }
    return _seperateLabel;
}
-(YYLabel *)dateLabel
{
    if (!_dateLabel) {
        _dateLabel = [YYLabel new];
        _dateLabel.textColor = [Utils getUIColorWithHexString:@"bbbbbb"];
        _dateLabel.font = [UIFont systemFontOfSize:12];
    }
    return _dateLabel;
}
-(YYLabel *)watchLabel{
    if (!_watchLabel) {
        _watchLabel = [YYLabel new];
        _watchLabel.textColor = [Utils getUIColorWithHexString:@"777777"];
        _watchLabel.font = font_(13);
    }
    return _watchLabel;
}
-(UIButton*)zanBtn{
    if (!_zanBtn) {
        _zanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_zanBtn setTitle:@"1532" forState:UIControlStateNormal];
        [_zanBtn setImage:[UIImage imageNamed:@"zanHeart"] forState:UIControlStateNormal];
        _zanBtn.titleLabel.font = font_(13);
        [_zanBtn setTitleColor:[Utils getUIColorWithHexString:@"777777"] forState:UIControlStateNormal];
        [_zanBtn addTarget:self action:@selector(zanAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _zanBtn;
}
- (void)zanAction:(id)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(DidClickZanBtnCell:)]) {
        [self.delegate DidClickZanBtnCell:self];
    }
}
-(UIButton*)shareBtn{
    if (!_shareBtn) {
        _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_shareBtn setTitle:@"3453" forState:UIControlStateNormal];
        [_shareBtn setImage:[UIImage imageNamed:@"shareCPS"] forState:UIControlStateNormal];
        _shareBtn.titleLabel.font = font_(13);
        [_shareBtn setTitleColor:[Utils getUIColorWithHexString:@"777777"] forState:UIControlStateNormal];
        [_shareBtn addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareBtn;
}
- (void)shareAction:(id)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(DidClickShareInDynamicsCell:)]) {
        [self.delegate DidClickShareInDynamicsCell:self];
    }
}
- (UIButton*)collectBtn{
    if (!_collectBtn) {
        _collectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_collectBtn setTitle:@"收藏" forState:UIControlStateNormal];
        [_collectBtn addTarget:self action:@selector(collectAction:) forControlEvents:UIControlEventTouchUpInside];
        [_collectBtn setTitleColor:[Utils getUIColorWithHexString:SysColorYellow] forState:UIControlStateNormal];
        _collectBtn.backgroundColor = [UIColor clearColor];
        _collectBtn.titleLabel.font = font_(13);
    }
    return _collectBtn;
}
- (void)collectAction:(id)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(DidClickCollectDynamicsCell:)]) {
        [self.delegate DidClickCollectDynamicsCell:self];
    }
}
//-(UIButton *)deleteBtn
//{
//    if (!_deleteBtn) {
//        _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
//        _deleteBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
//        _deleteBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//        _deleteBtn.titleLabel.font = [UIFont systemFontOfSize:13];
//        [_deleteBtn setTitleColor:[UIColor colorWithRed:74/255.0 green:90/255.0 blue:133/255.0 alpha:1] forState:UIControlStateNormal];
//        WS(weakSelf);
//        [_deleteBtn bk_addEventHandler:^(id sender) {
//            if (weakSelf.delegate != nil && [weakSelf.delegate respondsToSelector:@selector(DidClickDeleteInDynamicsCell:)]) {
//                [weakSelf.delegate DidClickDeleteInDynamicsCell:weakSelf];
//            }
//        } forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _deleteBtn;
//}
//-(UIButton *)menuBtn
//{
//    if (!_menuBtn) {
//        _menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        _menuBtn.contentMode = UIViewContentModeScaleAspectFit;
//        [_menuBtn setImage:[UIImage imageNamed:@"AlbumOperateMore"] forState:UIControlStateNormal];
//        WS(weakSelf);
//        [_menuBtn bk_addEventHandler:^(id sender) {
//            [weakSelf presentMenuController];
//        } forControlEvents:UIControlEventTouchUpInside];
//    }
//    return _menuBtn;
//}
//-(NewDynamicsThumbCommentView *)thumbCommentView
//{
//    if (!_thumbCommentView) {
//        _thumbCommentView = [NewDynamicsThumbCommentView new];
//        _thumbCommentView.cell = self;
//    }
//    return _thumbCommentView;
//}
-(UIView *)dividingLine
{
    if (!_dividingLine) {
        _dividingLine = [UIView new];
        _dividingLine.backgroundColor = [Utils getUIColorWithHexString:@"e9e9e9"];
    }
    return _dividingLine;
}

- (NSString *)formateDate:(NSString *)dateString withFormate:(NSString *) formate
{
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formate];
    NSDate * nowDate = [NSDate date];
    
    /////  将需要转换的时间转换成 NSDate 对象
    NSDate * needFormatDate = [dateFormatter dateFromString:dateString
                               ];
    /////  取当前时间和转换时间两个日期对象的时间间隔
    /////  这里的NSTimeInterval 并不是对象，是基本型，其实是double类型，是由c定义的:  typedef double NSTimeInterval;
    NSTimeInterval time = [nowDate timeIntervalSinceDate:needFormatDate];
    
    //// 再然后，把间隔的秒数折算成天数和小时数：
    
    NSString *dateStr = @"";
    
    if (time<=60) {  //// 1分钟以内的
        dateStr = @"刚刚";
    }else if(time<=60*60){  ////  一个小时以内的
        
        int mins = time/60;
        dateStr = [NSString stringWithFormat:@"%d分钟前",mins];
        
    }else if(time<=60*60*24){   //// 在两天内的
        
        [dateFormatter setDateFormat:@"YYYY/MM/dd"];
        NSString * need_yMd = [dateFormatter stringFromDate:needFormatDate];
        NSString *now_yMd = [dateFormatter stringFromDate:nowDate];
        
        [dateFormatter setDateFormat:@"HH:mm"];
        if ([need_yMd isEqualToString:now_yMd]) {
            //// 在同一天
            dateStr = [NSString stringWithFormat:@"今天 %@",[dateFormatter stringFromDate:needFormatDate]];
        }else{
            ////  昨天
            dateStr = [NSString stringWithFormat:@"昨天 %@",[dateFormatter stringFromDate:needFormatDate]];
        }
    }else {
        
        [dateFormatter setDateFormat:@"yyyy"];
        NSString * yearStr = [dateFormatter stringFromDate:needFormatDate];
        NSString *nowYear = [dateFormatter stringFromDate:nowDate];
        
        if ([yearStr isEqualToString:nowYear]) {
            ////  在同一年
            [dateFormatter setDateFormat:@"MM月dd日"];
            dateStr = [dateFormatter stringFromDate:needFormatDate];
        }else{
            [dateFormatter setDateFormat:@"yyyy/MM/dd"];
            dateStr = [dateFormatter stringFromDate:needFormatDate];
        }
    }
    
    return dateStr;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
