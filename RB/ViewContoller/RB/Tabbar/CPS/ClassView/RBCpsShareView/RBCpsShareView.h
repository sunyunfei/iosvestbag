//
//  RBCpsShareView.h
//  RB
//
//  Created by AngusNi on 15/6/2.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constants.h"

@class RBCpsShareView;
@protocol RBCpsShareViewDelegate <NSObject>
@optional
- (void)RBCpsShareViewDetailAction:(RBCpsShareView *)view;
@end


@interface RBCpsShareView : UIView
DEFINE_SINGLETON_FOR_HEADER(RBCpsShareView);
@property(nonatomic, weak) id<RBCpsShareViewDelegate> delegate;

- (void)showViewWithTitle:(NSString *)titleStr
                  Content:(NSString *)contentStr
                      URL:(NSString *)urlStr
                   ImgURL:(NSString *)imgUrlStr;
@property(nonatomic, strong) UIWindow*window;
@end
