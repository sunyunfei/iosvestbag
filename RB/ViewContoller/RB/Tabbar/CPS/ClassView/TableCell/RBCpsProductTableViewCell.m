//
//  RBCpsProductTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBCpsProductTableViewCell.h"
#import "Service.h"

@implementation RBCpsProductTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
        self.bgView.userInteractionEnabled = YES;
        self.bgView.tag = 1000;
        [self.contentView addSubview:self.bgView];
        //
        self.bgIV = [[YYAnimatedImageView alloc] initWithFrame:CGRectZero];
        self.bgIV.contentMode = UIViewContentModeScaleAspectFill;
        self.bgIV.clipsToBounds = YES;
        self.bgIV.tag = 1001;
        [self.contentView addSubview:self.bgIV];
        //
        self.tLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.tLabel.font = font_15;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.tLabel.numberOfLines = 0;
        self.tLabel.tag = 1002;
        [self.contentView addSubview:self.tLabel];
        //
        self.cLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectZero];
        self.cLabel.font = font_cu_13;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.cLabel.tag = 1003;
        [self.contentView addSubview:self.cLabel];
        //
        self.cLabel1 = [[TTTAttributedLabel alloc] initWithFrame:CGRectZero];
        self.cLabel1.font = font_13;
        self.cLabel1.textColor = [Utils getUIColorWithHexString:SysColorGray];
        self.cLabel1.tag = 1004;
        [self.contentView addSubview:self.cLabel1];
        //
        self.lineLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        self.lineLabel.tag = 1005;
        [self.contentView addSubview:self.lineLabel];
    }
    return self;
}

- (instancetype)loadRBCpsProductCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    RBCpsProductEntity *entity = datalist[indexPath.row];
    self.tLabel.text = [NSString stringWithFormat:@"%@",entity.goods_name];
    self.cLabel1.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"R2401", @"商品有效期"),entity.end_date];
    self.cLabel.text = [NSString stringWithFormat:@"%@￥%@    %@￥%@",NSLocalizedString(@"R8013", @"售价"),[Utils getNSStringTwoFloat:entity.unit_price],NSLocalizedString(@"R8014", @"提成"),[Utils getNSStringTwoFloat:entity.kol_commision_wl]];
    self.bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.cLabel setText:self.cLabel.text afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:[NSString stringWithFormat:@"￥%@",[Utils getNSStringTwoFloat:entity.kol_commision_wl]] options:NSCaseInsensitiveSearch];
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range];
         return mStr;
     }];
   // [self.bgIV yy_setImageWithURL:[NSURL URLWithString:entity.img_url] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
    [self.bgIV setImageWithURL:[NSURL URLWithString:entity.img_url] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
    //
    self.bgIV.frame = CGRectMake(0, 0, 115.0-1.0, 115.0-1.0);
    self.lineLabel.frame = CGRectMake(self.bgIV.right, 0, 0.5, self.bgIV.height);
    self.tLabel.frame = CGRectMake(self.bgIV.right+10.0, self.bgIV.top+10.0, ScreenWidth-self.bgIV.right-20.0, 18.0);
    CGSize tLabelSize = [Utils getUIFontSizeFitH:self.tLabel withLineSpacing:2.0];
    if (tLabelSize.height > 37) {
        self.tLabel.height = 38;
    } else {
        self.tLabel.height = tLabelSize.height;
    }
    [Utils getUILabel:self.tLabel withLineSpacing:2.0];
    self.cLabel1.frame = CGRectMake(self.tLabel.left, self.bgIV.bottom-43.0, self.tLabel.width, 13.0);
    self.cLabel.frame = CGRectMake(self.tLabel.left, self.bgIV.bottom-23.0, self.tLabel.width, 13.0);
    self.bgView.frame = CGRectMake(0, 0, ScreenWidth, self.bgIV.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, self.bgView.bottom+5.0);
    return self;
}

@end
