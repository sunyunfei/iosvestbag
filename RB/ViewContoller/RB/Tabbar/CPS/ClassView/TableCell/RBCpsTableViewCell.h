//
//  RBCpsTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYAnimatedImageView.h"
#import "TTTAttributedLabel.h"

@interface RBCpsTableViewCell : UITableViewCell
@property(nonatomic, strong)  UIView *bgView;
//
@property(nonatomic, strong)  YYAnimatedImageView *bgIV;
//
@property(nonatomic, strong)  UILabel*bgIVLabel;
//
@property(nonatomic, strong)  UILabel *tLabel;
//
@property(nonatomic, strong)  TTTAttributedLabel *cLabel;
//
@property(nonatomic, strong)  UILabel*userLabel;
//
@property(nonatomic, strong)  YYAnimatedImageView*userIV;
//
@property(nonatomic, strong)  UILabel*shareIV;
//
@property(nonatomic, strong)  UILabel*shareLabel;

- (instancetype)loadRBCpsCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
@end
