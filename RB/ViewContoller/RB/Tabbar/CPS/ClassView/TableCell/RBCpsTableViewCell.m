//
//  RBCpsTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBCpsTableViewCell.h"
#import "Service.h"
@implementation RBCpsTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
        self.bgView.userInteractionEnabled = YES;
        self.bgView.tag = 1000;
        [self.contentView addSubview:self.bgView];
        //
        self.bgIV = [[YYAnimatedImageView alloc] initWithFrame:CGRectZero];
        self.bgIV.contentMode = UIViewContentModeScaleAspectFill;
        self.bgIV.clipsToBounds = YES;
        self.bgIV.tag = 1001;
        [self.contentView addSubview:self.bgIV];
        //
        self.bgIVLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        self.bgIVLabel.tag = 1002;
        [self.contentView addSubview:self.bgIVLabel];
        //
        self.tLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.tLabel.font = font_cu_cu_17;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.tLabel.textAlignment = NSTextAlignmentCenter;
        self.tLabel.tag = 1003;
        [self.contentView addSubview:self.tLabel];
        //
        self.cLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectZero];
        self.cLabel.font = font_cu_cu_13;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.cLabel.textAlignment = NSTextAlignmentCenter;
        self.cLabel.tag = 1004;
        [self.contentView addSubview:self.cLabel];
        //
        self.userLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.userLabel.font = font_cu_13;
        self.userLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.userLabel.tag = 1005;
        [self.contentView addSubview:self.userLabel];
        //
        self.userIV = [[YYAnimatedImageView alloc] initWithFrame:CGRectZero];
        self.userIV.contentMode = UIViewContentModeScaleAspectFill;
        self.userIV.clipsToBounds = YES;
        self.userIV.tag = 1006;
        [self.contentView addSubview:self.userIV];
        //
        self.shareIV = [[UILabel alloc] initWithFrame:CGRectZero];
        self.shareIV.text = Icon_btn_share;
        self.shareIV.font = font_icon_(15);
        self.shareIV.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.shareIV.tag = 1007;
        [self.contentView addSubview:self.shareIV];
        //
        self.shareLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.shareLabel.font = font_cu_13;
        self.shareLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.shareLabel.tag = 1008;
        [self.contentView addSubview:self.shareLabel];
    }
    return self;
}

- (instancetype)loadRBCpsCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    if ([datalist count]>0) {
        RBCpsCreateEntity*entity = datalist[indexPath.row];
        self.tLabel.text = [NSString stringWithFormat:@"%@",entity.title];
        if([NSString stringWithFormat:@"%@",entity.material_total_price].intValue!=0) {
            self.cLabel.text = [NSString stringWithFormat:@"%@￥%@",NSLocalizedString(@"R8017",@"单笔预计提成"),[Utils getNSStringTwoFloat:entity.material_total_price]];
            [self.cLabel setText:self.cLabel.text afterInheritingLabelAttributesAndConfiguringWithBlock:
             ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
                 NSRange range = [[mStr string] rangeOfString:[NSString stringWithFormat:@"￥%@",[Utils getNSStringTwoFloat:entity.material_total_price]] options:NSCaseInsensitiveSearch];
                 [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range];
                 return mStr;
             }];
        }
        self.bgIVLabel.backgroundColor = SysColorCoverDeep;
//        [self.bgIV yy_setImageWithURL:[NSURL URLWithString:entity.cover] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
        [self.bgIV setImageWithURL:[NSURL URLWithString:entity.cover] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
        self.shareLabel.text = [NSString stringWithFormat:@"%@",entity.cps_article_share_count];
        self.userLabel.text = [NSString stringWithFormat:@"%@",entity.author_name];
//        if([NSString stringWithFormat:@"%@",entity.end_date].length!=0){
//            self.userLabel.text = [NSString stringWithFormat:@"%@  [%@]",entity.author_name,entity.end_date];
//        }
        //[self.userIV yy_setImageWithURL:[NSURL URLWithString:entity.author_avatar_url] placeholder:PlaceHolderUserImage];
        [self.userIV setImageWithURL:[NSURL URLWithString:entity.author_avatar_url] placeholder:PlaceHolderUserImage];
    }
    //
    self.bgIV.frame = CGRectMake(0, 0, ScreenWidth, (ScreenWidth*9.0)/16.0);
    self.bgIVLabel.frame = self.bgIV.frame;
    self.tLabel.frame = CGRectMake(self.bgIV.left+10.0, self.bgIV.top+(self.bgIV.height-31.0)/2.0-30.0, self.bgIV.width-20.0, 18.0);
    self.cLabel.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom+15.0, self.tLabel.width, 13.0);
    self.userIV.frame = CGRectMake(CellBottom, self.bgIV.height-CellBottom-30.0, 30.0, 30.0);
    self.userIV.layer.cornerRadius = self.userIV.width/2.0;
    self.userIV.layer.masksToBounds = YES;
    self.userLabel.frame = CGRectMake(CellBottom+self.userIV.right, self.userIV.top, 200.0, self.userIV.height);
    self.shareIV.frame = CGRectMake(ScreenWidth, self.userIV.top+7.5, 15.0, 15.0);
    self.shareLabel.frame = CGRectMake(CellBottom+self.userIV.right, self.userIV.top, 200.0, self.userIV.height);
    CGSize shareLabelSize = [Utils getUIFontSizeFitW:self.shareLabel];
    self.shareLabel.width = shareLabelSize.width;
    self.shareLabel.left = ScreenWidth - shareLabelSize.width -CellBottom;
    self.shareIV.left = self.shareLabel.left-15.0-5.0;
    self.bgView.frame = CGRectMake(0, 0, ScreenWidth, self.bgIV.bottom+5.0);
    self.frame = CGRectMake(0, 0, ScreenWidth, self.bgView.bottom);
    //
    UIView*cellBgView = [[UIView alloc]initWithFrame:self.frame];
    cellBgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.1];
    self.backgroundView = cellBgView;
    return self;
}

@end
