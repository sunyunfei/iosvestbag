//
//  RBFindTableViewCell.h
//  RB
//
//  Created by RB8 on 2018/3/28.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBImageCollectionView.h"
@interface RBFindTableViewCell : UITableViewCell
@property(nonatomic,strong)UIImageView * headImageView;
@property(nonatomic,strong)UILabel * nameLabel;
@property(nonatomic,strong)UILabel * timeLabel;
@property(nonatomic,strong)UIButton * collectBtn;
@property(nonatomic,strong)UILabel * describeLabel;
@property(nonatomic,strong)RBImageCollectionView * imgListView;
@property(nonatomic,strong)UILabel * watchLabel;
@property(nonatomic,strong)UIButton * zanBtn;
@property(nonatomic,strong)UIButton * shareBtn;
@property(nonatomic,strong)UILabel * lineLabel;
@property(nonatomic,strong)UIButton * foldBtn;
@end
