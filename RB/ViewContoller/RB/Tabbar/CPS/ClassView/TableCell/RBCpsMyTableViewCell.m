//
//  RBCpsMyTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBCpsMyTableViewCell.h"
#import "Service.h"

@implementation RBCpsMyTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UIImageView*tIV = [[UIImageView alloc]initWithFrame:CGRectZero];
        tIV.tag = 1001;
        [self.contentView addSubview:tIV];
        //
        UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_cu_15;
        tLabel.numberOfLines = 2;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1002;
        [self.contentView addSubview:tLabel];
        //
        UILabel*tLabel1 = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel1.font = font_13;
        tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel1.tag = 1003;
        //        [self.contentView addSubview:tLabel1];
        //
        TTTAttributedLabel*tLabel2 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        tLabel2.font = font_13;
        tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel2.tag = 1005;
        [self.contentView addSubview:tLabel2];
        //
        UILabel*tLabel3 = [[UILabel alloc]initWithFrame:CGRectZero];
        tLabel3.font = font_cu_13;
        tLabel3.textAlignment = NSTextAlignmentCenter;
        tLabel3.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        tLabel3.tag = 1006;
        [self.contentView addSubview:tLabel3];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1010;
        [self.contentView addSubview:lineLabel];
        //
        UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel1.tag = 1011;
        [self.contentView addSubview:lineLabel1];
        //
        UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        lineLabel2.tag = 1012;
        [self.contentView addSubview:lineLabel2];
    }
    return self;
}

- (instancetype)loadRBCpsMyCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath andStatus:(NSString*)status {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UIImageView*tIV=(UIImageView*)[self.contentView viewWithTag:1001];
    UILabel*tLabel=(UILabel*)[self.contentView viewWithTag:1002];
    UILabel*tLabel1=(UILabel*)[self.contentView viewWithTag:1003];
    TTTAttributedLabel*tLabel2=(TTTAttributedLabel*)[self.contentView viewWithTag:1005];
    UILabel*tLabel3=(UILabel*)[self.contentView viewWithTag:1006];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    UILabel*lineLabel1=(UILabel*)[self.contentView viewWithTag:1011];
    UILabel*lineLabel2=(UILabel*)[self.contentView viewWithTag:1012];
    //
    RBCpsCreateEntity *entity = datalist[indexPath.row];
    [tIV sd_setImageWithURL:[NSURL URLWithString:entity.cover] placeholderImage:PlaceHolderImage];
    tLabel.text = [NSString stringWithFormat:@"%@",entity.title];
    if([NSString stringWithFormat:@"%@",entity.end_date].length!=0) {
        tLabel.text = [NSString stringWithFormat:@"%@ [%@]",entity.title,entity.end_date];
    }
    tLabel1.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"R8018",@"转发人数"),entity.cps_article_share_count];
    tLabel2.text = [NSString stringWithFormat:@"%@:￥%@  %@:￥%@",NSLocalizedString(@"R2019",@"即将赚"),@"0.0",NSLocalizedString(@"R2018",@"已赚"),@"0.0"];
    if ([status isEqualToString:@"shares"]) {
        // 转发者
        NSString*share_forecast_commission = [Utils getNSStringTwoFloat:entity.share_forecast_commission];
        NSString*share_settled_commission = [Utils getNSStringTwoFloat:entity.share_settled_commission];
        tLabel2.text = [NSString stringWithFormat:@"%@:￥%@  %@:￥%@",NSLocalizedString(@"R2019",@"即将赚"),share_forecast_commission,NSLocalizedString(@"R2018",@"已赚"),share_settled_commission];
        [tLabel2 setText:tLabel2.text afterInheritingLabelAttributesAndConfiguringWithBlock:
         ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
             NSRange range_0 = [[mStr string] rangeOfString:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"R2019",@"即将赚")] options:NSCaseInsensitiveSearch];
             NSRange range0 = NSMakeRange(range_0.location+range_0.length,[NSString stringWithFormat:@"￥%@",share_forecast_commission].length);
             NSRange range_1 = [[mStr string] rangeOfString:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"R2018",@"已赚")] options:NSCaseInsensitiveSearch];
             NSRange range1 = NSMakeRange(range_1.location+range_1.length,[NSString stringWithFormat:@"￥%@",share_settled_commission].length);
             [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range0];
             [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range1];
             return mStr;
         }];
    } else {
        // 作者
        NSString*writing_forecast_commission = [Utils getNSStringTwoFloat:entity.writing_forecast_commission];
        NSString*writing_settled_commission = [Utils getNSStringTwoFloat:entity.writing_settled_commission];
        tLabel2.text = [NSString stringWithFormat:@"%@:￥%@  %@:￥%@",NSLocalizedString(@"R2019",@"即将赚"),writing_forecast_commission,NSLocalizedString(@"R2018",@"已赚"),writing_settled_commission];
        [tLabel2 setText:tLabel2.text afterInheritingLabelAttributesAndConfiguringWithBlock:
         ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
             NSRange range_0 = [[mStr string] rangeOfString:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"R2019",@"即将赚")] options:NSCaseInsensitiveSearch];
             NSRange range0 = NSMakeRange(range_0.location+range_0.length,[NSString stringWithFormat:@"￥%@",writing_forecast_commission].length);
             NSRange range_1 = [[mStr string] rangeOfString:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"R2018",@"已赚")] options:NSCaseInsensitiveSearch];
             NSRange range1 = NSMakeRange(range_1.location+range_1.length,[NSString stringWithFormat:@"￥%@",writing_settled_commission].length);
             [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range0];
             [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range1];
             return mStr;
         }];
    }
    tLabel3.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    if ([entity.status isEqualToString:@"pending"]) {
        tLabel3.text = NSLocalizedString(@"R2040", @"审核中");
        tLabel3.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    }
    if ([entity.status isEqualToString:@"rejected"]) {
        tLabel3.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"R8020",@"审核拒绝理由"),entity.check_remark];
        tLabel3.textColor = [Utils getUIColorWithHexString:SysColorRed];
    }
    //
    lineLabel2.frame = CGRectMake(0, 0, ScreenWidth, CellBottom);
    tIV.frame = CGRectMake(CellLeft, lineLabel2.bottom+CellLeft, (73.0/9.0)*16.0, 73.0);
    tIV.layer.borderWidth = 0.5;
    tIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
    tIV.contentMode = UIViewContentModeScaleAspectFill;
    tIV.layer.masksToBounds = YES;
    tLabel.frame = CGRectMake(tIV.right+CellBottom, tIV.top, ScreenWidth-CellLeft-(tIV.right+CellBottom), 1000.0);
    CGSize tLabelSize = [Utils getUIFontSizeFitH:tLabel];
    tLabel.height = tLabelSize.height;
    if (tLabelSize.height > 37) {
        tLabel.height = 38;
    }
    tLabel1.frame = CGRectMake(tLabel.left, tIV.bottom-40.0, tLabel.width, 16.0);
    tLabel2.frame = CGRectMake(tLabel.left, tIV.bottom-16.0, tLabel.width, 16.0);
    lineLabel1.frame = CGRectMake(0, tIV.bottom+CellLeft-0.5, ScreenWidth, 0.5);
    if ([entity.status isEqualToString:@"pending"]||[entity.status isEqualToString:@"rejected"]){
        tLabel3.frame = CGRectMake(0, lineLabel1.bottom, ScreenWidth, 37.0);
    } else {
        tLabel3.frame = CGRectMake(0, lineLabel1.bottom, ScreenWidth, 0);
    }
    lineLabel.frame = CGRectMake(0, tLabel3.bottom-0.5, ScreenWidth, 0.5);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, tLabel3.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    return self;
}

@end
