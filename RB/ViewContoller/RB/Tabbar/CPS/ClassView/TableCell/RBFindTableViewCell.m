//
//  RBFindTableViewCell.m
//  RB
//
//  Created by RB8 on 2018/3/28.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBFindTableViewCell.h"
#import "Service.h"
@implementation RBFindTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _headImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
        [self.contentView addSubview:_headImageView];
        //
        _nameLabel = [[UILabel alloc]initWithFrame:CGRectZero text:@"" font:font_(14) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorBlack] backgroundColor:[UIColor clearColor] numberOfLines:1];
        [self.contentView addSubview:_nameLabel];
        //
        _timeLabel = [[UILabel alloc]initWithFrame:CGRectZero text:@"" font:font_(12) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:@"bbbbbb"] backgroundColor:[UIColor clearColor] numberOfLines:1];
        [self.contentView addSubview:_timeLabel];
        //
        _collectBtn = [[UIButton alloc]initWithFrame:CGRectZero title:@"收藏" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorYellow] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
        _collectBtn.layer.cornerRadius = 2;
        _collectBtn.layer.borderColor = [Utils getUIColorWithHexString:SysColorYellow].CGColor;
        _collectBtn.layer.borderWidth = 0.5;
        [self.contentView addSubview:_collectBtn];
        //
        _describeLabel = [[UILabel alloc]initWithFrame:CGRectZero text:@"" font:font_(15) textAlignment:NSTextAlignmentLeft textColor:[UIColor blackColor] backgroundColor:[UIColor clearColor] numberOfLines:0];
        [self.contentView addSubview:_describeLabel];
        //
        _imgListView = [[RBImageCollectionView alloc]initWithFrame:CGRectZero];
        [self.contentView addSubview:_imgListView];
        //
        _watchLabel = [[UILabel alloc]initWithFrame:CGRectZero text:@"" font:font_(13) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:@"777777"] backgroundColor:[UIColor clearColor] numberOfLines:1];
        [self.contentView addSubview:_watchLabel];
        //
        _zanBtn = [[UIButton alloc]initWithFrame:CGRectZero title:nil hlTitle:nil titleColor:[Utils getUIColorWithHexString:@"777777"] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
        [self.contentView addSubview:_zanBtn];
        //
        _shareBtn = [[UIButton alloc]initWithFrame:CGRectZero title:nil hlTitle:nil titleColor:[Utils getUIColorWithHexString:@"777777"] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
        [self.contentView addSubview:_shareBtn];
        //
        _lineLabel = [[UILabel alloc]initWithFrame:CGRectZero text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:@"e9e9e9"] numberOfLines:1];
        [self.contentView addSubview:_lineLabel];
        //
        _foldBtn = [[UIButton alloc]initWithFrame:CGRectZero title:nil hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorYellow] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
        [self.contentView addSubview:_foldBtn];
    }
    return self;
}
- (void)setValueForCell:(id)model{
    _describeLabel.text = @"我大姐夫IE窝囊废哦啊见佛额外if绝对是咖啡金额我if就噢诶解放路水电费我发大师傅我诶副经理的哭声围殴if将是多么佛为辅打飞机为佛山科目分为哦的访问封口膜am时空裂缝饿哦飞机孔明灯开了房么我贷款方面为of莱克斯顿发饿哦无法";
    
    
    //
    _headImageView.frame = CGRectMake(CellLeft, 15, 41, 41);
    _headImageView.layer.cornerRadius = 41/2;
    _headImageView.layer.masksToBounds = YES;
    //
    _nameLabel.frame = CGRectMake(_headImageView.right + 10, 18, 100, 14);
    _timeLabel.frame = CGRectMake(_nameLabel.left, _nameLabel.bottom + 7, 100, 12);
    _collectBtn.frame = CGRectMake(ScreenWidth - CellLeft - 50, _headImageView.centerY - 11.5, 50, 23);
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
