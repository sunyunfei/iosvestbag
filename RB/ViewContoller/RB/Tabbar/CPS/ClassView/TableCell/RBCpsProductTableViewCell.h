//
//  RBCpsProductTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYAnimatedImageView.h"
#import "TTTAttributedLabel.h"

@interface RBCpsProductTableViewCell : UITableViewCell
@property(nonatomic, strong)  UIView *bgView;
@property(nonatomic, strong)  YYAnimatedImageView *bgIV;
@property(nonatomic, strong)  UILabel *tLabel;
@property(nonatomic, strong)  TTTAttributedLabel *cLabel;
@property(nonatomic, strong)  TTTAttributedLabel *cLabel1;
@property(nonatomic, strong)  UILabel *lineLabel;

- (instancetype)loadRBCpsProductCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
@end
