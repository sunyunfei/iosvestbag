//
//  RBCpsMyTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBButton.h"
@interface RBCpsMyTableViewCell : UITableViewCell
- (instancetype)loadRBCpsMyCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath  andStatus:(NSString*)status;
@end
