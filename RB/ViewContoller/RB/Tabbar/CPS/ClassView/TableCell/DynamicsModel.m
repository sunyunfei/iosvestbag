//
//  DynamicsModel.m
//  LooyuEasyBuy
//
//  Created by Andy on 15/11/20.
//  Copyright © 2015年 Doyoo. All rights reserved.
//

#import "DynamicsModel.h"


extern CGFloat maxContentLabelHeight;

@implementation DynamicsModel
{
    CGFloat _lastContentWidth;
}
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
    NSLog(@"DynamicsModel找不到Key----------------------------%@",key);
}

//-(void)setOptthumb:(NSMutableArray *)optthumb
//{
//    _optthumb = optthumb;
//    self.likeArr = optthumb;
//
//    if (optthumb.count != 0 && optthumb != nil) {
//        __block BOOL hasUserID = NO;
//        [optthumb enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            //这里加入if判断如果存在于点赞列表则显示取消点赞
//            //  if(){
//                _isThumb = YES;
//                hasUserID = YES;
//            //        }
//        }];
//        if (!hasUserID) {
//            _isThumb = NO;
//        }
//    }else{
//        _isThumb = NO;
//    }
//
//}
//- (void)setLikeArr:(NSMutableArray<DynamicsLikeItemModel *> *)likeArr
//{
//    NSMutableArray *tempLikes = [NSMutableArray new];
//    for (id thumbDic in likeArr) {
//        DynamicsLikeItemModel * likeModel = [DynamicsLikeItemModel new];
//        [likeModel setValuesForKeysWithDictionary:thumbDic];
//        [tempLikes addObject:likeModel];
//    }
//    likeArr = [tempLikes copy];
//
//    _likeArr = likeArr;
//}
//- (void)setOptcomment:(NSMutableArray *)optcomment
//{
//    _optcomment = optcomment;
//    self.commentArr = optcomment;
//
//}
//-(void)setCommentArr:(NSMutableArray<DynamicsCommentItemModel *> *)commentArr
//{
//    NSMutableArray *tempComments = [NSMutableArray new];
//    for (id commentDic in commentArr) {
//        DynamicsCommentItemModel * commentModel = [DynamicsCommentItemModel new];
//        [commentModel setValuesForKeysWithDictionary:commentDic];
//        [tempComments addObject:commentModel];
//    }
//    commentArr = [tempComments copy];
//
//    _commentArr = commentArr;
//}
-(NSString *)title
{
    if (_title == nil) {
        _title = @"";
    }
//    _title = [self converStrEmoji:_title];
 //   _title = [self disable_EmojiString:_title];
    _title = [self converStrEmoji:_title];
    NSMutableAttributedString * text = [[NSMutableAttributedString alloc] initWithString:_title];
    text.font = [UIFont systemFontOfSize:15];
    
    YYTextContainer * container = [YYTextContainer containerWithSize:CGSizeMake(ScreenWidth - 2*12, CGFLOAT_MAX)];
    
    YYTextLayout * layout = [YYTextLayout layoutWithContainer:container text:text];
    if (layout.rowCount <= 3) {
        _shouldShowMoreButton = NO;
    }else{
        _shouldShowMoreButton = YES;
    }
    return _title;
}

- (BOOL)stringContainsEmoji:(NSString *)string
{
    __block BOOL returnValue = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         // surrogate pair
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     returnValue = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 returnValue = YES;
             }
         } else {
             // non surrogate
             if (0x2100 <= hs && hs <= 0x27ff) {
                 returnValue = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 returnValue = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 returnValue = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 returnValue = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                 returnValue = YES;
             }
         }
     }];
    return returnValue;
}
- (NSString*)disable_EmojiString:(NSString *)text
{
    //去除表情规则
    //  \u0020-\\u007E  标点符号，大小写字母，数字
    //  \u00A0-\\u00BE  特殊标点  (¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾)
    //  \u2E80-\\uA4CF  繁简中文,日文，韩文 彝族文字
    //  \uF900-\\uFAFF  部分汉字
    //  \uFE30-\\uFE4F  特殊标点(︴︵︶︷︸︹)
    //  \uFF00-\\uFFEF  日文  (ｵｶｷｸｹｺｻ)
    //  \u2000-\\u201f  特殊字符(‐‑‒–—―‖‗‘’‚‛“”„‟)
    // 注：对照表 http://blog.csdn.net/hherima/article/details/9045765
    
    NSRegularExpression* expression = [NSRegularExpression regularExpressionWithPattern:@"[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u2000-\\u201f\r\n]" options:NSRegularExpressionCaseInsensitive error:nil];
    
    
    NSString* result = [expression stringByReplacingMatchesInString:text options:0 range:NSMakeRange(0, text.length) withTemplate:@""];
    
    return result;
}
- (NSString*)converStrEmoji:(NSString *)emojiStr{
    NSString *tempStr = [[NSString alloc]init];
    NSMutableString *kksstr = [[NSMutableString alloc]init];
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:10];
    NSMutableString *strMu = [[NSMutableString alloc]init];
    //记录是含有不能解析的表情符号
    BOOL hasEmoji = NO;
    for(int i =0; i < [emojiStr length]; i++)
    {
        tempStr = [emojiStr substringWithRange:NSMakeRange(i, 1)];
//        [strMu appendString:tempStr];
//        if ([self stringContainsEmoji:strMu]) {
//            if (strMu.length >= 2) {
//              strMu = [[strMu substringToIndex:([strMu length]-2)] mutableCopy];
//            }
//            if (strMu.length != array.count) {
//                [array removeLastObject];
//            }
//            continue;
//        }else{
            if ([tempStr isEqualToString:@"["]) {
                hasEmoji = YES;
                continue;
            }else{
                if (hasEmoji == YES) {
                    if ([tempStr isEqualToString:@"]"]) {
                        hasEmoji = NO;
                    }else{
                        continue;
                    }
                }else{
                    [array addObject:tempStr];
                }
            }
        }
 //   }
    for (NSString *strs in array) {

        [kksstr appendString:strs];
    }
  return kksstr;
}

//- (void) deleteEmojiStringAction
//
//{
//
//    NSString *souceText = self.textView.text;
//
//    NSRange range = self.textView.selectedRange;
//
//    if (range.location == NSNotFound) {
//
//        range.location = self.textView.text.length;
//
//    }
//
//    if (range.length > 0) {
//
//        [self.textView deleteBackward];
//
//        return;
//
//    }else
//
//    {
//
//        //正则匹配要替换的文字的范围
//
//        //正则表达式
//
//        NSString * pattern = @"\\[[a-zA-Z0-9\\u4e00-\\u9fa5]+\\]";
//
//        NSError *error = nil;
//
//        NSRegularExpression * re = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
//
//        if (!re) {
//
//            NSLog(@"%@", [error localizedDescription]);
//
//        }
//
//        //通过正则表达式来匹配字符串
//
//        NSArray *resultArray = [re matchesInString:souceText options:0 range:NSMakeRange(0, souceText.length)];
//
//        NSTextCheckingResult *checkingResult = resultArray.lastObject;
//
//        for (NSString *faceName in self.plistFaces) {
//
//            if ([souceText hasSuffix:@"]"]) {
//
//                if ([[souceText substringWithRange:checkingResult.range] isEqualToString:faceName]) {
//
//                    NSLog(@"faceName %@", faceName);
//
//                    NSString *newText = [souceText substringToIndex:souceText.length - checkingResult.range.length];
//
//                    self.textView.text = newText;                    return;
//
//                }
//
//            }else
//
//            {
//
//                [self.textView deleteBackward];
//
//                return;
//
//            }
//
//        }
//
//    }
//
//}

//- (void)setPic:(NSArray *)pic{
//    _pic = pic;
//    _smallArray = pic[0];
//    _BigArray = pic[1];
//}

- (void)setIsOpening:(BOOL)isOpening
{
    if (!_shouldShowMoreButton) {
        _isOpening = NO;
    } else {
        _isOpening = isOpening;
    }
}
@end

//@implementation DynamicsLikeItemModel
//
//-(void)setValue:(id)value forUndefinedKey:(NSString *)key
//{
//    NSLog(@"DynamicsLikeItemModel找不到Key----------------------------%@",key);
//}
//
//
//@end
//
//@implementation DynamicsCommentItemModel
//
//-(void)setValue:(id)value forUndefinedKey:(NSString *)key
//{
//    NSLog(@"DynamicsCommentItemModel找不到Key----------------------------%@",key);
//}
//
//@end

