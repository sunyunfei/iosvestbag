//
//  RBFindDetailView.m
//  RB
//
//  Created by RB8 on 2018/4/24.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBFindDetailView.h"
#import "Service.h"

@implementation RBFindDetailView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}
- (void)initUI{
    _loadTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth,ScreenHeight - NavHeight) style:UITableViewStylePlain];
    _loadTable.delegate = self;
    _loadTable.dataSource = self;
    _loadTable.showsVerticalScrollIndicator = NO;
    _loadTable.showsHorizontalScrollIndicator = NO;
    _loadTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_loadTable];
    [_loadTable registerClass:[NewDynamicsTableViewCell class] forCellReuseIdentifier:@"NewDynamicsTableViewCell"];
    [_loadTable registerClass:[RBRecommendCell class] forCellReuseIdentifier:@"RBRecommendCell"];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        NewDynamicsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NewDynamicsTableViewCell"];
        cell.layout = _detailLayout;
        cell.delegate = self;
        return cell;
    }else{
        RBRecommendCell * cell = [tableView dequeueReusableCellWithIdentifier:@"RBRecommendCell"];
        if (self.dataArray.count > 0) {
            [cell setValueForCell:self.dataArray];
        }
        return cell;
    }
   
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
       // NewDynamicsLayout * layout = self.dataArray[0];
        return _detailLayout.height;
    }else{
        return 35 + 0.5 + 95;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
//
- (void)DidClickCollectDynamicsCell:(NewDynamicsTableViewCell *)cell{
    if ([self.delegate respondsToSelector:@selector(DidClickButton:)]) {
        [self.delegate DidClickButton:@"collect"];
    }
}
- (void)DidClickZanBtnCell:(NewDynamicsTableViewCell *)cell{
    if ([self.delegate respondsToSelector:@selector(DidClickButton:)]) {
        [self.delegate DidClickButton:@"like"];
    }
}
- (void)DidClickShareInDynamicsCell:(NewDynamicsTableViewCell *)cell{
    if ([self.delegate respondsToSelector:@selector(DidClickButton:)]) {
        [self.delegate DidClickButton:@"forward"];
    }
}
- (void)DynamicsCell:(NewDynamicsTableViewCell *)cell didClickUrl:(NSString *)url PhoneNum:(NSString *)phoneNum{
    if (url) {
        if ([url rangeOfString:@"wemall"].length != 0 || [url rangeOfString:@"t.cn"].length != 0) {
            if (![url hasPrefix:@"http://"]) {
                url = [NSString stringWithFormat:@"http://%@",url];
            }
            NSLog(@"点击了链接:%@",url);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }else{
            [SVProgressHUD showErrorWithStatus:@"暂不支持打开外部链接"];
        }
    }
    if (phoneNum) {
//        NSLog(@"点击了电话:%@",phoneNum);
//        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"即将拨打电话" message:phoneNum preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction * action = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//            
//        }];
//        UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNum]]];
//        }];
//        [alert addAction:action];
//        [alert addAction:action1];
//        [self presentViewController:alert animated:YES completion:nil];
        
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 Rect)rect {
    // Drawing code
}
*/

@end
