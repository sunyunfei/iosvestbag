//
//  RBCpsMyView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBCpsMyTableViewCell.h"

@class RBCpsMyView;
@protocol RBCpsMyViewDelegate <NSObject>
@optional
- (void)RBCpsMyViewLoadRefreshViewData:(RBCpsMyView *)view;
- (void)RBCpsMyViewSelected:(RBCpsMyView *)view andIndex:(int)index;
@end


@interface RBCpsMyView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBCpsMyViewDelegate> delegate;
@property(nonatomic, strong) NSString*status;
@property(nonatomic, strong) UIView*defaultView;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) id jsonObject;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
- (void)RBCpsMyViewLoadRefreshViewFirstData;
- (void)RBCpsMyViewSetRefreshViewFinish;

@end
