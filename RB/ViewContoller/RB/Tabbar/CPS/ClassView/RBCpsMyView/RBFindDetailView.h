//
//  RBFindDetailView.h
//  RB
//
//  Created by RB8 on 2018/4/24.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewDynamicsTableViewCell.h"
#import "RBRecommendCell.h"
@protocol RBFindDetailViewDelegate <NSObject>
- (void)DidClickButton:(NSString*)act;
@end
@interface RBFindDetailView : UIView<UITableViewDelegate,UITableViewDataSource,NewDynamicsCellDelegate>
@property(nonatomic,strong)UITableView * loadTable;
@property(nonatomic,strong)NSArray * dataArray;
@property(nonatomic,strong)NewDynamicsLayout * detailLayout;
@property(nonatomic,strong)NSIndexPath * KCIndexPath;
@property(nonatomic,strong)NSString * KCType;
@property(nonatomic,weak)id<RBFindDetailViewDelegate>delegate;
@end
