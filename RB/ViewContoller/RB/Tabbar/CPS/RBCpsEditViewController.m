//
//  RBCpsEditViewController.m
//  RB
//
//  Created by AngusNi on 16/8/25.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBCpsEditViewController.h"
@implementation RBImgView
@end

@implementation RBProView
@end

@interface RBCpsEditViewController () {
    InsetsTextField*editTF;
    PlaceholderTextView*editTV;
    UIScrollView*scrollviewn;
    TextViewCpsKeyBoard*textViewKB;
    UITextView*inputTV;
    NSMutableArray*imgArray;
    NSMutableArray*proArray;
    UIImage *tempimg;
}
@end

@implementation RBCpsEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R8007",@"开始创作");
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize labelSize = [NSLocalizedString(@"R8008",@"发布") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_cu_15,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
    UIButton*rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.navView.width-labelSize.width-CellLeft, StatusHeight, labelSize.width, 44.0);
    rightBtn.titleLabel.font = font_15;
    [rightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
    [rightBtn setTitle:NSLocalizedString(@"R8008",@"发布") forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:rightBtn];
    //
    [self loadContentView];
    imgArray = [NSMutableArray new];
    proArray = [NSMutableArray new];
    //
    if (self.productEntity!=nil) {
        inputTV = editTV;
        [self updateScrollview];
        [self addEditProduct:self.productEntity];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    //
    [TalkingData trackPageBegin:@"cps-edit"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [TalkingData trackPageEnd:@"cps-edit"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self endEdit];
    
    RBAlertView *alertView = [[RBAlertView alloc]init];
    alertView.delegate = self;
    alertView.tag = 20000;
    [alertView showWithArray:@[NSLocalizedString(@"R8039",@"当前文章尚未发布，确定退出编辑？"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
}

- (void)rightBtnAction:(UIButton*)sender {
    [self endEdit];
    
    RBAlertView *alertView = [[RBAlertView alloc]init];
    alertView.delegate = self;
    alertView.tag = 30000;
    [alertView showWithArray:@[NSLocalizedString(@"R8041",@"文章发布后，不可以修改，文章中的商品被购买后，您可以获得高额提成！"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if(alertView.tag==20000) {
        if (buttonIndex==0) {
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        if (buttonIndex==1) {
            return;
        }
    }
    
    if(alertView.tag==30000) {
        if (buttonIndex==0) {
            NSString* temStr = @"";
            NSString* imgStr = @"";
            for (UIView *tempV in scrollviewn.subviews) {
                if ([tempV isKindOfClass:[UITextView class]]) {
                    UITextView* temp = (UITextView*)tempV;
                    if(temp.text.length!=0) {
                        temStr = [NSString stringWithFormat:@"%@<text>%@",temStr,temp.text];
                    }
                }
                if ([tempV isKindOfClass:[RBImgView class]]) {
                    RBImgView* temp = (RBImgView*)tempV;
                    temStr = [NSString stringWithFormat:@"%@<img>%@",temStr,temp.imgStr];
                    if(imgStr.length==0){
                        imgStr = temp.imgStr;
                    }
                }
                if ([tempV isKindOfClass:[RBProView class]]) {
                    RBProView* temp = (RBProView*)tempV;
                    temStr = [NSString stringWithFormat:@"%@<product>%@",temStr,temp.pEntity.iid];
                }
            }
            NSLog(@"temStr:%@",temStr);
            if (editTF.text.length==0) {
                [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092",@"请填写"),NSLocalizedString(@"R8032",@"标题")]];
                return;
            }
            if (imgStr.length==0) {
                [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R8034",@"至少上传1张图片")]];
                return;
            }
            [self.hudView showOverlay];
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBCpsCreateSubmitWithContent:temStr andTitle:editTF.text andCover:imgStr];
            return;
        }
        if (buttonIndex==1) {
            return;
        }
    }
}

#pragma mark - loadContentView Delegate
-(void)loadContentView {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(endEdit)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height-NavHeight)];
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.userInteractionEnabled = YES;
    [self.view addSubview:scrollviewn];
    //
    editTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth-CellLeft*2.0, 50.0)];
    editTF.font = font_cu_17;
    editTF.keyboardType =  UIKeyboardTypeDefault;
    editTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    editTF.returnKeyType = UIReturnKeyNext;
    editTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"R8032",@"标题") attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    editTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    editTF.delegate = self;
    [scrollviewn addSubview:editTF];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(editTF.left, editTF.bottom, editTF.width, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [scrollviewn addSubview:lineLabel];
    //
    editTV = [[PlaceholderTextView alloc]initWithFrame:CGRectMake(editTF.left, lineLabel.bottom, editTF.width, scrollviewn.height-lineLabel.bottom)];
    editTV.font = font_15;
    editTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    editTV.keyboardType = UIKeyboardTypeDefault;
    editTV.placeholderColor = [Utils getUIColorWithHexString:SysColorSubGray];
    editTV.placeholderFont = font_15;
    editTV.placeholder = NSLocalizedString(@"R8033",@"内容");
    editTV.delegate = self;
    editTV.scrollEnabled = NO;
    [scrollviewn addSubview:editTV];
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, editTV.bottom)];
    //
    textViewKB = [[TextViewCpsKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textViewKB.delegateP = self;
    [editTV setInputAccessoryView:textViewKB];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
    scrollviewn.height = self.view.height-NavHeight;
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [editTV becomeFirstResponder];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    scrollviewn.height = self.view.height-NavHeight -44.0 -258.0 -40.0;
    inputTV = textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    scrollviewn.height = self.view.height-NavHeight;
}

- (void)textViewDidChange:(UITextView *)textView {
    CGSize constraintSize = CGSizeMake(textView.width, MAXFLOAT);
    CGSize size = [textView sizeThatFits:constraintSize];
    textView.height = size.height;
    [self updateScrollview];
}

#pragma mark - TextViewCpsKeyBoard Delegate
-(void)TextViewCpsKeyBoardLeft:(TextViewCpsKeyBoard *)sender {
    RBPictureUpload*picUpload = [RBPictureUpload sharedRBPictureUpload];
    picUpload.scaleFrame = CGRectZero;
    picUpload.fromVC = self.navigationController;
    picUpload.delegate = self;
    [picUpload RBPictureUploadClickedButtonAtIndex:1];
}

-(void)TextViewCpsKeyBoardRight:(TextViewCpsKeyBoard *)sender {
    RBCpsProductViewController*toview = [[RBCpsProductViewController alloc] init];
    toview.delegateT = self;
    toview.isNew = nil;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - RBCpsProductViewControllerDelegate Delegate
- (void)RBCpsProductViewController:(RBCpsProductViewController *)vc select:(RBCpsProductEntity*)productEntity {
    [self addEditProduct:productEntity];
}

#pragma mark - UIImageUpload Delegate
-(void)addEditImg:(UIImage*)img andImgStr:(NSString*)str {
    CGSize constraintSize = CGSizeMake(editTV.width, MAXFLOAT);
    CGSize size = [editTV sizeThatFits:constraintSize];
    editTV.height = size.height;
    //
    RBImgView*imgView = [[RBImgView alloc]initWithFrame:CGRectMake(editTV.left, inputTV.bottom, editTV.width, (editTV.width/img.size.width)*img.size.height)];
    imgView.imgStr = str;
    [scrollviewn insertSubview:imgView aboveSubview:inputTV];
    //
    UIImageView*imgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, imgView.width, (imgView.width/img.size.width)*img.size.height)];
    imgIV.contentMode = UIViewContentModeScaleAspectFit;
    imgIV.image = img;
    imgIV.tag = 1000;
    [imgView addSubview:imgIV];
    //
    PlaceholderTextView*temTV = [[PlaceholderTextView alloc]initWithFrame:CGRectMake(editTF.left, imgView.bottom, editTF.width, 34.0)];
    temTV.font = font_15;
    temTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    temTV.keyboardType = UIKeyboardTypeDefault;
    temTV.delegate = self;
    temTV.scrollEnabled = NO;
    [temTV setInputAccessoryView:textViewKB];
    [scrollviewn insertSubview:temTV aboveSubview:imgView];
    //
    [temTV becomeFirstResponder];
    //
    RBButton*deleteBtn = [RBButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.frame = CGRectMake(imgView.width-30.0, imgIV.height-30.0, 30.0, 30.0);
    [deleteBtn addTarget:self action:@selector(deleteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.5];
    deleteBtn.titleLabel.font = font_icon_(15.0);
    [deleteBtn setTitle:Icon_btn_delete forState:UIControlStateNormal];
    deleteBtn.btnView = imgView;
    deleteBtn.btnTV = temTV;
    [imgView addSubview:deleteBtn];
    //
    [self updateScrollview];
}

-(void)deleteBtnAction:(RBButton *)sender {
    [sender.btnView removeAllSubviews];
    [sender.btnView removeFromSuperview];
    if(sender.btnTV.text.length==0) {
        [sender.btnTV removeAllSubviews];
        [sender.btnTV removeFromSuperview];
    }
    [self updateScrollview];
}

#pragma mark - ProductUpload Delegate
-(void)addEditProduct:(RBCpsProductEntity*)productEntity {
    CGSize constraintSize = CGSizeMake(editTV.width, MAXFLOAT);
    CGSize size = [editTV sizeThatFits:constraintSize];
    editTV.height = size.height;
    //
    RBProView*productView = [[RBProView alloc]initWithFrame:CGRectMake(editTV.left, inputTV.bottom, editTV.width, 74.0)];
    productView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkgGray];
    productView.pEntity = productEntity;
    [scrollviewn insertSubview:productView aboveSubview:inputTV];
    //
    UIImageView*imgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 74.0, 74.0)];
    [imgIV sd_setImageWithURL:[NSURL URLWithString:productEntity.img_url] placeholderImage:PlaceHolderImage];
    [productView addSubview:imgIV];
    //
    UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectMake(imgIV.right+10.0, 15.0, productView.width-imgIV.right-10.0-30.0, 20.0)];
    tLabel.font = font_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tLabel.text = productEntity.goods_name;
    [productView addSubview:tLabel];
    //
    TTTAttributedLabel*cLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(tLabel.left, tLabel.bottom+10.0, tLabel.width, 15.0)];
    cLabel.font = font_13;
    cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    cLabel.text = [NSString stringWithFormat:@"%@￥%@    %@￥%@",NSLocalizedString(@"R8013", @"售价"),[Utils getNSStringTwoFloat:productEntity.unit_price],NSLocalizedString(@"R8014", @"提成"),[Utils getNSStringTwoFloat:productEntity.kol_commision_wl]];
    [productView addSubview:cLabel];
    [cLabel setText:cLabel.text afterInheritingLabelAttributesAndConfiguringWithBlock:
     ^NSMutableAttributedString *(NSMutableAttributedString *mStr) {
         NSRange range = [[mStr string] rangeOfString:[NSString stringWithFormat:@"￥%@",[Utils getNSStringTwoFloat:productEntity.kol_commision_wl]] options:NSCaseInsensitiveSearch];
         [mStr addAttributes:@{(NSString *)kCTForegroundColorAttributeName:(id)[[Utils getUIColorWithHexString:SysColorYellow] CGColor]} range:range];
         return mStr;
     }];
    //
    PlaceholderTextView*temTV = [[PlaceholderTextView alloc]initWithFrame:CGRectMake(editTF.left, productView.bottom, editTF.width, 34.0)];
    temTV.font = font_15;
    temTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    temTV.keyboardType = UIKeyboardTypeDefault;
    temTV.delegate = self;
    temTV.scrollEnabled = NO;
    [temTV setInputAccessoryView:textViewKB];
    [scrollviewn insertSubview:temTV aboveSubview:productView];
    //
    [temTV becomeFirstResponder];
    //
    UIView*rightBgView = [[UIView alloc]initWithFrame:CGRectMake(productView.width-30.0, 0, 30.0, productView.height)];
    rightBgView.backgroundColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.5];
    [productView addSubview:rightBgView];
    //
    RBButton*deleteBtn = [RBButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.frame = CGRectMake(productView.width-22.5, (productView.height-15.0)/2.0, 15.0, 15.0);
    [deleteBtn addTarget:self action:@selector(deleteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.titleLabel.font = font_icon_(15.0);
    [deleteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [deleteBtn setTitle:Icon_btn_delete forState:UIControlStateNormal];
    deleteBtn.btnView = productView;
    deleteBtn.btnTV = temTV;
    [productView addSubview:deleteBtn];
    //
    [self updateScrollview];
}

-(void)updateScrollview {
    float temH = 0;
    for (UIView *temp in scrollviewn.subviews) {
        temp.top = temH;
        temH = temH + temp.height;
    }
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, temH)];
}

#pragma mark - RBPictureUpload Delegate
- (void)RBPictureUpload:(RBPictureUpload *)pictureUpload selectImage:(UIImage *)img {
    [self.hudView showOverlay];
    tempimg = [Utils getUIImageScalingFromSourceImage:img targetSize:img.size];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBCpsUploadImg:tempimg];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"upload"]) {
        [self.hudView dismiss];
        NSString*url = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"url"]];
        [self addEditImg:tempimg andImgStr:url];
    }
    
    if ([sender isEqualToString:@"cps_articles/create"]) {
        RBCpsEditSubmitViewController*toview = [[RBCpsEditSubmitViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}


@end
