//
//  RBCpsDetailKolsViewController.m
//  RB
//
//  Created by AngusNi on 7/5/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBCpsDetailKolsViewController.h"

@interface RBCpsDetailKolsViewController () {
    UIScrollView*scrollviewn;
}
@end

@implementation RBCpsDetailKolsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R8036", @"转发人员列表");
    self.bgIV.hidden = NO;
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    scrollviewn.bounces = YES;
    scrollviewn.userInteractionEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    [self.view addSubview:scrollviewn];
    //
    [self loadContent];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //
    [TalkingData trackPageBegin:@"cps-detail-kols-list"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //
    [TalkingData trackPageEnd:@"cps-detail-kols-list"];
}

- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadContent {
    float gap = (ScreenWidth-3*60.0)/4.0;
    for (int i=0; i<[self.cpsEntity.cps_article_shares count]; i++) {
        RBKOLEntity*userEntity = self.cpsEntity.cps_article_shares[i];
        //
        UIButton*kolBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        kolBtn.frame = CGRectMake(gap+(60.0+gap)*(i%3), gap+(60.0+gap)*(i/3), 60.0, 60.0);
        [kolBtn addTarget:self action:@selector(kolBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        kolBtn.tag = 5000+i;
        [scrollviewn addSubview:kolBtn];
        //
        UIImageView*kolIV = [[UIImageView alloc]initWithFrame:kolBtn.bounds];
        kolIV.image = PlaceHolderUserImage;
        [kolIV sd_setImageWithURL:[NSURL URLWithString:userEntity.avatar_url] placeholderImage:PlaceHolderUserImage];
        if (userEntity.avatar_url.length==0) {
            kolIV.layer.borderWidth = 1.0f;
            kolIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
        }
        kolIV.layer.cornerRadius = kolIV.width/2.0;
        kolIV.layer.masksToBounds = YES;
        [kolBtn addSubview:kolIV];
        //
        UILabel*kolLabel = [[UILabel alloc] initWithFrame:CGRectMake(kolBtn.left-15.0, kolBtn.bottom, kolBtn.width+30.0, 20.0)];
        kolLabel.text = userEntity.name;
        kolLabel.font = font_13;
        kolLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        kolLabel.textAlignment = NSTextAlignmentCenter;
        [scrollviewn addSubview:kolLabel];
        //
        if(kolLabel.bottom>scrollviewn.height){
            [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, kolLabel.bottom+gap)];
        }
    }
}

- (void)kolBtnAction:(UIButton *)sender {
    RBKOLEntity*userEntity = self.cpsEntity.cps_article_shares[sender.tag-5000];
    RBKolDetailViewController *toview = [[RBKolDetailViewController alloc] init];
    toview.kolId = userEntity.iid;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

@end
