//
//  RBCpsEditViewController.h
//  RB
//
//  Created by AngusNi on 16/8/25.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBCpsEditSubmitViewController.h"
#import "RBCpsProductViewController.h"

@interface RBImgView : UIView
@property(nonatomic, strong) UIImage *img;
@property(nonatomic, strong) NSString *imgStr;
@end

@interface RBProView : UIView
@property(nonatomic, strong) RBCpsProductEntity *pEntity;
@end

@interface RBCpsEditViewController : RBBaseViewController
<RBBaseVCDelegate,UITextViewDelegate,UITextFieldDelegate,TextViewCpsKeyBoardDelegate,RBPictureUploadDelegate,UIGestureRecognizerDelegate,RBCpsProductViewControllerDelegate,RBAlertViewDelegate>
@property(nonatomic, strong) RBCpsProductEntity*productEntity;

@end
