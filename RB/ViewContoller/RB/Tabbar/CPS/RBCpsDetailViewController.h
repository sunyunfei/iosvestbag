//
//  RBCpsDetailViewController.h
//  RB
//
//  Created by AngusNi on 16/8/25.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBCpsShareView.h"
#import "RBCpsProductViewController.h"
#import "RBCpsDetailKolsViewController.h"

@interface RBCpsDetailViewController : RBBaseViewController
<RBBaseVCDelegate,UIWebViewDelegate,RBCpsShareViewDelegate>
@property(nonatomic, strong) RBCpsCreateEntity *createEntity;

@end
