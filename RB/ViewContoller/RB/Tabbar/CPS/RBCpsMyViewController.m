//
//  RBCpsMyViewController.m
//  RB
//
//  Created by AngusNi on 16/9/6.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBCpsMyViewController.h"

@interface RBCpsMyViewController (){
    UIScrollView*scrollviewn;
    NSArray*statusArray;
    RBRankingSwitchView*switchView;
}
@end

@implementation RBCpsMyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R8012", @"我的创作");
    //
    switchView = [[RBRankingSwitchView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 38.0)];
    switchView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [switchView RBRankingSwitchViewShow:@[NSLocalizedString(@"R8015", @"我的转发"),NSLocalizedString(@"R8012", @"我的创作"),NSLocalizedString(@"R2040", @"审核中"),NSLocalizedString(@"R8016",@"审核拒绝")] andSelected:0];
    switchView.delegate = self;
    [self.view addSubview:switchView];
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, switchView.bottom, ScreenWidth, ScreenHeight-switchView.bottom)];
    scrollviewn.bounces = YES;
    scrollviewn.pagingEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.delegate = self;
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width*4.0, scrollviewn.height)];
    [self.view addSubview:scrollviewn];
    //
    statusArray = @[@"shares",@"passed",@"pending",@"rejected"];
    for (int i=0; i<4; i++) {
        RBCpsMyView*cpsMyView = [[RBCpsMyView alloc]initWithFrame:CGRectMake(scrollviewn.width*i, 0, scrollviewn.width, scrollviewn.height)];
        cpsMyView.status = statusArray[i];
        cpsMyView.delegate = self;
        cpsMyView.tag = 10000+i;
        cpsMyView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        [scrollviewn addSubview:cpsMyView];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"cps-my"];
    //
    [self.hudView show];
    RBCpsMyView*cpsMyView = (RBCpsMyView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [cpsMyView RBCpsMyViewLoadRefreshViewFirstData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"cps-my"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark- UIScrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView==scrollviewn){
        CGPoint offset = scrollView.contentOffset;
        int currentPage = offset.x / (self.view.bounds.size.width);
        [switchView RBRankingSwitchViewShow:@[NSLocalizedString(@"R8015", @"我的转发"),NSLocalizedString(@"R8012", @"我的创作"),NSLocalizedString(@"R2040", @"审核中"),NSLocalizedString(@"R8016",@"审核拒绝")] andSelected:currentPage];
        [self RBRankingSwitchViewSelected:switchView andIndex:currentPage];
    }
}

#pragma mark- RBCpsMySwitchView Delegate
- (void)RBRankingSwitchViewSelected:(RBRankingSwitchView *)view andIndex:(int)index {
    RBCpsMyView*advertiserView = (RBCpsMyView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView show];
    [advertiserView RBCpsMyViewLoadRefreshViewFirstData];
    [scrollviewn scrollRectToVisible:CGRectMake(scrollviewn.width*index, 0, scrollviewn.width, scrollviewn.height) animated:YES];
}

#pragma mark- RBCpsMyView Delegate
-(void)RBCpsMyViewLoadRefreshViewData:(RBCpsMyView *)view {
    self.defaultView.hidden = YES;
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBCpsMyCreateListWithPage:[NSString stringWithFormat:@"%d",view.pageIndex+1] andStatus:statusArray[switchView.selectedTag]];
}

-(void)RBCpsMyViewSelected:(RBCpsMyView *)view andIndex:(int)index {
    RBCpsCreateEntity*entity = view.datalist[index];
    RBCpsDetailViewController *toview = [[RBCpsDetailViewController alloc] init];
    toview.createEntity = entity;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"cps_articles/my"]) {
        RBCpsMyView*cpsMyView = (RBCpsMyView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
        if (cpsMyView.pageIndex == 0) {
            cpsMyView.datalist = [NSMutableArray new];
        }
        cpsMyView.datalist = [JsonService getRBCpsCreateListEntity:jsonObject andBackArray:cpsMyView.datalist];
        cpsMyView.defaultView.hidden = YES;
        if ([cpsMyView.datalist count] == 0) {
            cpsMyView.defaultView.hidden = NO;
        }
        [self.hudView dismiss];
        [cpsMyView RBCpsMyViewSetRefreshViewFinish];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    RBCpsMyView*cpsMyView = (RBCpsMyView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView dismiss];
    [cpsMyView RBCpsMyViewSetRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    RBCpsMyView*cpsMyView = (RBCpsMyView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView dismiss];
    [cpsMyView RBCpsMyViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

