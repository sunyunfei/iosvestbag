//
//  RBCpsEditSubmitViewController.m
//  RB
//
//  Created by AngusNi on 16/9/6.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBCpsEditSubmitViewController.h"

@interface RBCpsEditSubmitViewController () {
}
@end

@implementation RBCpsEditSubmitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R6028", @"订单支付成功");
    //
    [self loadEditView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"cps-edit-successed"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"cps-edit-successed"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    RBCpsMyViewController*toview = [[RBCpsMyViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    RBCpsMyViewController*toview = [[RBCpsMyViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIScrollView*editScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:editScrollView];
    //
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake((ScreenWidth-80.0)/2.0, (editScrollView.height-80.0-40.0-50.0-45.0-10.0)/2.0-40.0, 80.0, 80.0)];
    tLabel.font = font_icon_(80.0);
    tLabel.text = Icon_btn_select_h;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlue];
    [editScrollView addSubview:tLabel];
    //
    UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, tLabel.bottom+25.0, ScreenWidth, 20.0)];
    tLabel1.text = NSLocalizedString(@"R8009", @"发布成功");
    tLabel1.font = font_cu_17;
    tLabel1.textAlignment = NSTextAlignmentCenter;
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel1];
    //
    UILabel*tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(0, tLabel1.bottom+5.0, ScreenWidth, 20.0)];
    tLabel2.text = NSLocalizedString(@"R8010", @"我们将在24小时内审核您的活动");
    tLabel2.font = font_13;
    tLabel2.textAlignment = NSTextAlignmentCenter;
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [editScrollView addSubview:tLabel2];
    CGSize tLabel2Size = [Utils getUIFontSizeFitW:tLabel2];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake((ScreenWidth-tLabel2Size.width)/2.0, tLabel2.bottom+25.0, tLabel2Size.width, 45.0);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R8011", @"查看我的创作") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [editScrollView addSubview:footerBtn];
    //
    [editScrollView setContentSize:CGSizeMake(editScrollView.width, footerBtn.bottom)];
}


@end

