//
//  RBKolSearchViewController.m
//  RB
//
//  Created by AngusNi on 16/7/27.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolSearchViewController.h"

@interface RBKolSearchViewController (){
    RBKolListView*kolListView;
    RBArticleSearchView*searchView;
    RBArticleSearchHistoryView*historyView;
    NSString*searchStr;
}
@end

@implementation RBKolSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    [self loadNavView];
    [self showSearchHistory];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    //
    [TalkingData trackPageBegin:@"kol-search"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"kol-search"];
}

#pragma mark - UIButton Delegate
- (void)loadNavView {
    searchView = [[RBArticleSearchView alloc]initWithFrame:CGRectZero];
    searchView.delegate = self;
    [self.view addSubview:searchView];
    //
    historyView = [[RBArticleSearchHistoryView alloc]initWithFrame:CGRectMake(searchView.left, searchView.bottom, searchView.width, ScreenHeight - (searchView.bottom))];
    historyView.delegate = self;
    historyView.type = @"KOL";
    [self.view addSubview:historyView];
    //
    kolListView = [[RBKolListView alloc]initWithFrame:CGRectMake(0, searchView.bottom, ScreenWidth, ScreenHeight - searchView.bottom)];
    kolListView.delegate = self;
    [self.view addSubview:kolListView];
    kolListView.collectionviewn.mj_header=nil;
    //
    UIScreenEdgePanGestureRecognizer *edgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(edgePanGesture:)];
    edgePanGestureRecognizer.delegate = self;
    edgePanGestureRecognizer.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:edgePanGestureRecognizer];
}

-(void)showSearchHistory {
    historyView.hidden = NO;
    kolListView.hidden = YES;
    historyView.datalist = [NSMutableArray arrayWithArray:[[LocalService getRBLocalDataUserKolSearchList] componentsSeparatedByString:@"*^*"]];
    [historyView.tableviewn reloadData];
    [searchView.searchTF becomeFirstResponder];
}

- (BOOL)becomeFirstResponder{
    return YES;
}

#pragma mark- UIScreenEdgePanGestureRecognizer Delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (void)edgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self.view removeGestureRecognizer:sender];
}

#pragma mark - RBArticleSearchView Delegate
-(void)RBArticleSearchHistoryViewSelected:(RBArticleSearchHistoryView *)view andIndex:(int)index {
    searchStr = view.datalist[index];
    searchView.searchTF.text = searchStr;
    [kolListView RBKolListViewLoadRefreshViewFirstData];
}

#pragma mark - RBArticleSearchView Delegate
-(void)RBArticleSearchViewSearchCancel:(RBArticleSearchView *)view {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)RBArticleSearchViewSearchClear:(RBArticleSearchView *)view {
    [self showSearchHistory];
}

-(void)RBArticleSearchViewSearch:(RBArticleSearchView *)view andText:(NSString *)text {
    [self.view endEditing:YES];
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([text isEqualToString:searchStr]) {
        return;
    }
    if (text.length==0) {
        return;
    }
    searchStr = text;
    NSMutableArray*tempList = [NSMutableArray arrayWithArray:[[LocalService getRBLocalDataUserKolSearchList] componentsSeparatedByString:@"*^*"]];
    if (![tempList containsObject:searchStr]) {
        if ([tempList count]>=5) {
            [tempList removeLastObject];
        }
        [tempList insertObject:searchStr atIndex:0];
    }
    NSString*temp=@"";
    for (int i =0; i <[tempList count]; i++) {
        if([NSString stringWithFormat:@"%@",tempList[i]].length!=0){
            if (i==0) {
                temp = tempList[i];
            } else {
                temp = [NSString stringWithFormat:@"%@*^*%@",temp,tempList[i]];
            }
        }
    }
    if([NSString stringWithFormat:@"%@",temp].length!=0) {
        [LocalService setRBLocalDataUserKolSearchListWith:temp];
        [kolListView RBKolListViewLoadRefreshViewFirstData];
    }
}

#pragma mark - RBKolListView Delegate
-(void)RBKolListViewLoadRefreshViewData:(RBKolListView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBKOLListWithPage:view.pageIndex+1 andBanner:nil andCategory:nil andSearch:searchStr andOrder:@"order_by_hot"];
}

- (void)RBKolListViewSelected:(RBKolListView *)view andIndex:(int)index {
    RBKOLEntity *entity = view.datalist[index];
    RBKolDetailViewController *toview = [[RBKolDetailViewController alloc] init];
    toview.kolId = entity.iid;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)RBKolListViewMore:(RBKolListView *)view {
    RBEngineViewController *toview = [[RBEngineViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"big_v/order_by_hot"]) {
        [self.hudView dismiss];
        if (kolListView.pageIndex == 0) {
            kolListView.datalist = [NSMutableArray new];
        }
        kolListView.datalist = [JsonService getRBKolListEntity:jsonObject andBackArray:kolListView.datalist];
        if (kolListView.pageIndex == 0) {
//                [self.hudView showSuccessWithStatus:NSLocalizedString(@"R2024", @"无相关搜索结果")];
//                [self showSearchHistory];
//                [searchView.searchTF becomeFirstResponder];
//            } else {
//                [searchView.searchTF resignFirstResponder];
//                historyView.hidden = YES;
//                kolListView.hidden = NO;
//            }
            [searchView.searchTF resignFirstResponder];
            historyView.hidden = YES;
            kolListView.hidden = NO;
            [kolListView RBKolListViewSetRefreshViewFinish];
            //
            if ([kolListView.datalist count] == 0) {
                kolListView.defaultView.hidden = NO;
            } else {
                kolListView.defaultView.hidden = YES;
            }
        } else {
            [kolListView RBKolListViewSetRefreshViewFinish];
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [kolListView RBKolListViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [kolListView RBKolListViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
