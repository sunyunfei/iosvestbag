//
//  RBKolApplySocialViewController.h
//  RB
//
//  Created by AngusNi on 16/8/2.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolSocialButton.h"
#import "RBKolEditView.h"
#import "RBKolApplySubmitViewController.h"

#import "RBKolApplySocialInputViewController.h"
#import "RBKolApplySocialBundingInputViewController.h"
#import "RBKolApplySocialShowInputViewController.h"
#import "RBKolApplySocialPublicInputViewController.h"
@protocol RefreshSocial <NSObject>
-(void)updataSocial:(NSString*)index;
@end
@interface RBKolApplySocialViewController : RBBaseViewController
<RBBaseVCDelegate>
@property(nonatomic,weak)id<RefreshSocial>KolApplyDelegate;
@end
