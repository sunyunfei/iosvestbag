//
//  RBKolApplyInfoViewController.h
//  RB
//
//  Created by AngusNi on 16/8/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolEditView.h"
#import "RBKolApplyInfoInputViewController.h"
#import "RBKolApplyInfoInputTextViewController.h"
#import "RBKolApplyInfoInterestViewController.h"
#import "RBKolApplySocialViewController.h"
@interface RBKolApplyInfoViewController : RBBaseViewController
<RBBaseVCDelegate,RBActionSheetDelegate,RBPictureUploadDelegate,RefreshSocial>
//判断
@property(nonatomic,copy)NSString * path;
@property(nonatomic,strong)NSMutableDictionary * dataDic;
@end
