//
//  RBModifyPhoneViewController.h
//  RB
//
//  Created by RB8 on 2017/7/14.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBModifyPhoneViewController : RBBaseViewController<TextFiledKeyBoardDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,RBBaseVCDelegate>

@end
