//
//  RBKolApplySocialBundingInputViewController.h
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolSocialButton.h"
#import "RBKolEditView.h"
#import "RBPictureView.h"
@interface RBKolApplySocialBundingInputViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,RBPictureUploadDelegate,RBActionSheetDelegate>
@property(nonatomic, strong) NSString*editString;
@property(nonatomic, strong) RBKolSocialButton*editButton;
@property(nonatomic, strong) RBKOLSocialEntity*socialEntity;
@end
