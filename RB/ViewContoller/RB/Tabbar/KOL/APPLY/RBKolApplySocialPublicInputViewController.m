//
//  RBKolApplySocialPublicInputViewController.m
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolApplySocialPublicInputViewController.h"

@interface RBKolApplySocialPublicInputViewController () {
    InsetsTextField*editTF;
    InsetsTextField*editTF1;
}
@end

@implementation RBKolApplySocialPublicInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString];
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, NavHeight, ScreenWidth-CellLeft*2.0, 40.0)];
    tLabel.font = font_15;
    tLabel.text = [NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString,NSLocalizedString(@"R7027",@"对应的微信号")];
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [self.view addSubview:tLabel];
    //
    UIView*tBgView = [[UIView alloc] initWithFrame:CGRectMake(0, tLabel.bottom, ScreenWidth, 50.0)];
    tBgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:tBgView];
    //
    editTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, tLabel.bottom, ScreenWidth-CellLeft*2.0, 50.0)];
    editTF.font = font_15;
    editTF.keyboardType =  UIKeyboardTypeWebSearch;
    editTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    editTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"例如: Robin8China" attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    editTF.returnKeyType = UIReturnKeyDone;
    editTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    editTF.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    editTF.delegate = self;
    [self.view addSubview:editTF];
    //
    UILabel*lineTLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF.top, ScreenWidth, 0.5f)];
    lineTLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineTLabel];
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineLabel];
    //
    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textFiledKB.delegateT = self;
    [editTF setInputAccessoryView:textFiledKB];
    //
    UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineLabel.bottom, ScreenWidth-CellLeft*2.0, tLabel.height)];
    tLabel1.font = font_15;
    tLabel1.text = [NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString,NSLocalizedString(@"R7021",@"报价(元/条)")];
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [self.view addSubview:tLabel1];
    //
    UIView*tBgView1 = [[UIView alloc] initWithFrame:CGRectMake(0, tLabel1.bottom, ScreenWidth, 50.0)];
    tBgView1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:tBgView1];
    //
    editTF1 = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, tLabel1.bottom, ScreenWidth-CellLeft*2.0, 50.0)];
    editTF1.font = font_15;
    editTF1.keyboardType =  UIKeyboardTypeNumberPad;
    editTF1.clearButtonMode = UITextFieldViewModeWhileEditing;
    editTF1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"例如: 1000" attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    editTF1.returnKeyType = UIReturnKeyDone;
    editTF1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    editTF1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    editTF1.delegate = self;
    [self.view addSubview:editTF1];
    [editTF1 setInputAccessoryView:textFiledKB];
    //
    UILabel*lineTLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF1.top, ScreenWidth, 0.5f)];
    lineTLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineTLabel1];
    UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF1.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineLabel1];
    //
    if (self.socialEntity) {
        editTF.text = self.socialEntity.uid;
        editTF1.text = self.socialEntity.price;
    }
}

- (void)footerBtnAction:(UIButton *)sender {
    if (editTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString,NSLocalizedString(@"R7027",@"对应的微信号")]];
        return;
    }
    if (editTF1.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString,NSLocalizedString(@"R7021",@"报价(元/条)")]];
        return;
    }
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBKOLApplyWithProvider:self.editString andHomepage:nil andUsername:nil andPrice:editTF1.text andFollowers:nil andScreenshot:nil andUid:editTF.text];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"update_social_v2"]) {
        [self.hudView dismiss];
        self.editButton.iconIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_ON.png",self.editString]];
        self.editButton.is_ON = @"YES";
        [self RBNavLeftBtnAction];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
@end
