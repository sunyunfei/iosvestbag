//
//  RBSetPhoneViewController.h
//  RB
//
//  Created by RB8 on 2017/7/14.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBSetPhoneViewController : RBBaseViewController<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,RBActionSheetDelegate,UIAlertViewDelegate>

@end
