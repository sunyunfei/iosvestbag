//
//  RBKolApplySocialShowInputViewController.m
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolApplySocialShowInputViewController.h"

@interface RBKolApplySocialShowInputViewController () {
    InsetsTextField*editTF;
    InsetsTextField*editTF1;
    InsetsTextField*editTF2;
}
@end

@implementation RBKolApplySocialShowInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R7028",@"个人精彩展示")];
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, NavHeight, ScreenWidth-CellLeft*2.0, 40.0)];
    tLabel.font = font_15;
    tLabel.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R7028",@"个人精彩展示")];
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [self.view addSubview:tLabel];
    //
    UIView*tBgView = [[UIView alloc] initWithFrame:CGRectMake(0, tLabel.bottom, ScreenWidth, 50.0)];
    tBgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:tBgView];
    //
    editTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, tLabel.bottom, ScreenWidth-CellLeft*2.0, 50.0)];
    editTF.font = font_15;
    editTF.keyboardType =  UIKeyboardTypeWebSearch;
    editTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    editTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"例如: http://mp.weixin.qq.com/s?__biz=MzAwNTcxNTg5MA==&mid=2650710052&idx=1&sn=f3ec65f73c433980c60fd0bb6885e6ed" attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    editTF.returnKeyType = UIReturnKeyDone;
    editTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    editTF.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    editTF.delegate = self;
    [self.view addSubview:editTF];
    //
    UILabel*lineTLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF.top, ScreenWidth, 0.5f)];
    lineTLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineTLabel];
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineLabel];
    //
    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textFiledKB.delegateT = self;
    [editTF setInputAccessoryView:textFiledKB];
    //
    UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineLabel.bottom, ScreenWidth-CellLeft*2.0, tLabel.height)];
    tLabel1.font = font_15;
    tLabel1.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R7028",@"个人精彩展示")];
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [self.view addSubview:tLabel1];
    //
    UIView*tBgView1 = [[UIView alloc] initWithFrame:CGRectMake(0, tLabel1.bottom, ScreenWidth, 50.0)];
    tBgView1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:tBgView1];
    //
    editTF1 = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, tLabel1.bottom, ScreenWidth-CellLeft*2.0, 50.0)];
    editTF1.font = font_15;
    editTF1.keyboardType =  UIKeyboardTypeNumberPad;
    editTF1.clearButtonMode = UITextFieldViewModeWhileEditing;
    editTF1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"例如: http://mp.weixin.qq.com/s?__biz=MzAwNTcxNTg5MA==&mid=2650710052&idx=1&sn=f3ec65f73c433980c60fd0bb6885e6ed" attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    editTF1.returnKeyType = UIReturnKeyDone;
    editTF1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    editTF1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    editTF1.delegate = self;
    [self.view addSubview:editTF1];
    [editTF1 setInputAccessoryView:textFiledKB];
    //
    UILabel*lineTLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF1.top, ScreenWidth, 0.5f)];
    lineTLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineTLabel1];
    UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF1.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineLabel1];
    //
    UILabel*tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineLabel1.bottom, ScreenWidth-CellLeft*2.0, tLabel.height)];
    tLabel2.font = font_15;
    tLabel2.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R7028",@"个人精彩展示")];
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [self.view addSubview:tLabel2];
    //
    UIView*tBgView2 = [[UIView alloc] initWithFrame:CGRectMake(0, tLabel2.bottom, ScreenWidth, 50.0)];
    tBgView2.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:tBgView2];
    //
    editTF2 = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, tLabel2.bottom, ScreenWidth-CellLeft*2.0, 50.0)];
    editTF2.font = font_15;
    editTF2.keyboardType =  UIKeyboardTypeNumberPad;
    editTF2.clearButtonMode = UITextFieldViewModeWhileEditing;
    editTF2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"例如: http://mp.weixin.qq.com/s?__biz=MzAwNTcxNTg5MA==&mid=2650710052&idx=1&sn=f3ec65f73c433980c60fd0bb6885e6ed" attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    editTF2.returnKeyType = UIReturnKeyDone;
    editTF2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    editTF2.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    editTF2.delegate = self;
    [self.view addSubview:editTF2];
    [editTF2 setInputAccessoryView:textFiledKB];
    //
    UILabel*lineTLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF2.top, ScreenWidth, 0.5f)];
    lineTLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineTLabel2];
    UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF2.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineLabel2];
    //
    for (int i=0; i< [[self.kolshows componentsSeparatedByString:@","] count]; i++) {
        if (i==0) {
            editTF.text = [self.kolshows componentsSeparatedByString:@","][0];
        }
        if (i==1) {
            editTF1.text = [self.kolshows componentsSeparatedByString:@","][1];
        }
        if (i==2) {
            editTF2.text = [self.kolshows componentsSeparatedByString:@","][2];
        }
    }
}

- (void)footerBtnAction:(UIButton *)sender {
    if(editTF.text.length!=0&&[[editTF.text componentsSeparatedByString:@"http://"]count]<=1){
        editTF.text = [NSString stringWithFormat:@"http://%@",editTF.text];
    }
    if(editTF1.text.length!=0&&[[editTF1.text componentsSeparatedByString:@"http://"]count]<=1){
        editTF1.text = [NSString stringWithFormat:@"http://%@",editTF1.text];
    }
    if(editTF2.text.length!=0&&[[editTF2.text componentsSeparatedByString:@"http://"]count]<=1){
        editTF2.text = [NSString stringWithFormat:@"http://%@",editTF2.text];
    }
    if (editTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R7028",@"个人精彩展示")]];
        return;
    }
    NSMutableArray *array = [NSMutableArray new];
    NSString*str = editTF.text;
    if(editTF1.text.length!=0) {
        [array addObject:editTF1.text];
    }
    if(editTF2.text.length!=0) {
        [array addObject:editTF2.text];
    }
    for (NSString*temp in array) {
        str = [NSString stringWithFormat:@"%@,%@",str,temp];
    }
    self.editButton.cLabel.text = [NSString stringWithFormat:@"%d",(int)[array count]+1];
    self.editButton.shows = str;
    [self RBNavLeftBtnAction];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

@end

