//
//  RBKolApplyInfoViewController.m
//  RB
//
//  Created by AngusNi on 16/8/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolApplyInfoViewController.h"
#import "Utils.h"
#import "RBAppDelegate.h"
#import "RBModifyPhoneViewController.h"
@interface RBKolApplyInfoViewController () {
    UIScrollView*scrollviewn;
    UIView*headerView;
    UIButton*headButton;
    UIImage*uploadImg;
    RBKOLEntity*kolEntity;
    UIButton * SocialBtn;
    //
    RBKolEditView*nameView;
    RBKolEditView*ageView;
    RBKolEditView*professionView;
    RBKolEditView*interestView;
    RBKolEditView*descriptionView;
    RBKolEditView*genderView;
    RBKolEditView*phoneView;
    RBKolEditView*emailView;
}
@end

@implementation RBKolApplyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _dataDic = [[NSMutableDictionary alloc]init];
    [_dataDic setValue:@"0" forKey:@"wechat"];
    [_dataDic setValue:@"0" forKey:@"qq"];
    [_dataDic setValue:@"0" forKey:@"weibo"];
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R7048",@"申请成为KOL");
    if ([self.path isEqualToString:@"first"]) {
        self.navLeftTitle = NSLocalizedString(@"R7059", @"跳过");
    }else{
        self.navRightTitle = NSLocalizedString(@"R7057", @"编辑");
    }
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height-NavHeight-45.0)];
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.userInteractionEnabled = YES;
    [self.view addSubview:scrollviewn];
    //
    [self loadHeaderView];
    [self loadContentView];
    [self loadFooterView];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissToUser) name:@"dismiss" object:nil];
    //
    if(![JsonService isRBUserVisitor]) {
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBKOLDetailWithKolId:[LocalService getRBLocalDataUserLoginId]];
        [handler getRBInfluenceCityName];
    }
}
//回到个人中心页面
-(void)dismissToUser{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageBegin:@"kol-apply-info"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"kol-apply-info"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    if ([self.path isEqualToString:@"first"]) {
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:1];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)RBNavRightBtnAction{
    UILabel * arrowLabel = (UILabel*)[SocialBtn viewWithTag:998];
    UIView * view = (UIView*)[self.view viewWithTag:999];
    UIButton * bigHeadButton = (UIButton*)[headerView viewWithTag:1001];
    
    if ([self.navRightTitle isEqualToString:NSLocalizedString(@"R7057", @"编辑")]) {
        self.navRightTitle = NSLocalizedString(@"R7058", @"取消");
        nameView.arrowLabel.hidden = NO;
        genderView.arrowLabel.hidden = NO;
        ageView.arrowLabel.hidden = NO;
        interestView.arrowLabel.hidden = NO;
        professionView.arrowLabel.hidden = NO;
        arrowLabel.hidden = NO;
        descriptionView.arrowLabel.hidden = NO;
        view.hidden = NO;

        
        nameView.userInteractionEnabled = YES;
        genderView.userInteractionEnabled = YES;
        ageView.userInteractionEnabled = YES;
        interestView.userInteractionEnabled = YES;
        professionView.userInteractionEnabled = YES;
        SocialBtn.userInteractionEnabled = YES;
        descriptionView.userInteractionEnabled = YES;
        
        view.userInteractionEnabled = YES;
        bigHeadButton.userInteractionEnabled = YES;
    }else if ([self.navRightTitle isEqualToString:NSLocalizedString(@"R7058", @"取消")]){
        self.navRightTitle = NSLocalizedString(@"R7057", @"编辑");
        nameView.arrowLabel.hidden = YES;
        genderView.arrowLabel.hidden = YES;
        ageView.arrowLabel.hidden = YES;
        interestView.arrowLabel.hidden = YES;
        professionView.arrowLabel.hidden = YES;
        arrowLabel.hidden = YES;
        descriptionView.arrowLabel.hidden = YES;
        view.hidden = YES;
        
        
        nameView.userInteractionEnabled = NO;
        genderView.userInteractionEnabled = NO;
        ageView.userInteractionEnabled = NO;
        interestView.userInteractionEnabled = NO;
        professionView.userInteractionEnabled = NO;
        SocialBtn.userInteractionEnabled = NO;
        descriptionView.userInteractionEnabled = NO;
        
        view.userInteractionEnabled = NO;
        bigHeadButton.userInteractionEnabled = NO;
    }

}
- (void)footerBtnAction:(UIButton*)sender {
    if (nameView.cLabel.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5079", @"昵称")]];
        return;
    }
    if (genderView.cLabel.text.length==0||[genderView.cLabel.text isEqualToString:NSLocalizedString(@"R5067", @"未填写")]) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5189", @"性别")]];
        return;
    }
    if (ageView.cLabel.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R5070", @"年龄")]];
        return;
    }
    if (professionView.cLabel.text.length==0) {
         professionView.cLabel.text = @"";
//        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R7016",@"职业")]];
//        return;
    }
    if (interestView.interest.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R7017",@"兴趣")]];
        return;
    }
    if (descriptionView.cLabel.text.length==0) {
        descriptionView.cLabel.text = @"";
//        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R7018",@"简介")]];
//        return;
    }
    if (uploadImg==nil) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R7019",@"请上传封面图片")];
        return;
    }
    if (![[_dataDic objectForKey:@"wechat"] isEqualToString:@"1"]) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R7056",@"请先绑定微信")];
        return;
    }
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBKOLApplyWithAvatar:uploadImg andName:nameView.cLabel.text andAge:ageView.cLabel.text andGender:genderView.cLabel.text  andApp_city:[LocalService getRBLocalDataUserKolCity] andJob:professionView.cLabel.text andTags:interestView.interest andDesc:descriptionView.cLabel.text];
}
-(void)phoneBtnAction:(UIButton*)sender{
    RBModifyPhoneViewController * modifyVC = [[RBModifyPhoneViewController alloc]init];
    [self.navigationController pushViewController:modifyVC animated:YES];
}
- (void)uploadBtnAction:(UIButton*)sender {
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    actionSheet.tag = 1000;
    [actionSheet showWithArray:@[NSLocalizedString(@"R2051", @"打开相册"),NSLocalizedString(@"R1011",@"取消")]];
}

- (void)nameBtnAction:(RBKolEditView*)sender {
    RBKolApplyInfoInputViewController *toview = [[RBKolApplyInfoInputViewController alloc] init];
    toview.maxCount = 10;
    toview.editString = NSLocalizedString(@"R5079", @"昵称");
    toview.editText = sender.cLabel.text;
    toview.editView = sender;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)ageBtnAction:(RBKolEditView*)sender {
    RBKolApplyInfoInputViewController *toview = [[RBKolApplyInfoInputViewController alloc] init];
    toview.maxCount = 3;
    toview.editString = NSLocalizedString(@"R5070", @"年龄");
    toview.editText = sender.cLabel.text;
    toview.editView = sender;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)genderBtnAction:(RBKolEditView*)sender {
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    actionSheet.tag = 2000;
    [actionSheet showWithArray:@[NSLocalizedString(@"R5068",@"男"),NSLocalizedString(@"R5069",@"女"),NSLocalizedString(@"R1011", @"取消")]];
}

- (void)professionBtnAction:(RBKolEditView*)sender {
    RBKolApplyInfoInputViewController *toview = [[RBKolApplyInfoInputViewController alloc] init];
    toview.maxCount = 10;
    toview.editString = NSLocalizedString(@"R7016",@"职业");
    toview.editView = sender;
    toview.editText = sender.cLabel.text;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)interestBtnAction:(RBKolEditView*)sender {
    RBKolApplyInfoInterestViewController *toview = [[RBKolApplyInfoInterestViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    toview.editView = sender;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)descriptionBtnAction:(RBKolEditView*)sender {
    RBKolApplyInfoInputTextViewController *toview = [[RBKolApplyInfoInputTextViewController alloc] init];
    toview.maxCount = 100;
    toview.editString = NSLocalizedString(@"R7018",@"简介");
    toview.editView = sender;
    toview.editText = sender.cLabel.text;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - RBPictureUpload Delegate
- (void)RBPictureUpload:(RBPictureUpload *)pictureUpload selectImage:(UIImage *)img {
    NSLog(@"%f,%f-%f,%f",img.size.width,img.size.height,headButton.width,headButton.height);
    img = [Utils getUIImageScalingFromSourceImage:img targetSize:headButton.size];
    [headButton setBackgroundImage:img forState:UIControlStateNormal];
    [headButton setImage:img forState:UIControlStateNormal];
    uploadImg = img;
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if (actionSheet.tag == 1000) {
        if (buttonIndex == 0) {
            RBPictureUpload*picUpload = [RBPictureUpload sharedRBPictureUpload];
            picUpload.scaleFrame = CGRectMake(CellLeft, (ScreenHeight-ScreenWidth+2*CellLeft)/2.0, ScreenWidth - 2*CellLeft, ScreenWidth-2*CellLeft);
            picUpload.fromVC = self.navigationController;
            picUpload.delegate = self;
            [picUpload RBPictureUploadClickedButtonAtIndex:1];
            return;
        }
    }
    if (actionSheet.tag == 2000) {
        if (buttonIndex == 0) {
            genderView.cLabel.text = NSLocalizedString(@"R5068",@"男");
        }
        if (buttonIndex == 1) {
            genderView.cLabel.text = NSLocalizedString(@"R5069",@"女");
        }
        if (buttonIndex == 2) {
            return;
        }
    }
}

#pragma mark- loadHeaderView Delegate
- (void)loadHeaderView {
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 0)];
    headerView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    //
    UILabel*headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40.0)];
    headerLabel.font = font_cu_13;
    headerLabel.text = NSLocalizedString(@"R7020",@"资料完善度越高，接受的活动价格越高");
    headerLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.8];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorSubBlack];
    [headerView addSubview:headerLabel];
    //
//    UIImageView*headerIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, headerLabel.bottom+24.0, ScreenWidth, 30.0)];
//    headerIV.image = [UIImage imageNamed:@"pic_kol_step_0.png"];
//    [headerView addSubview:headerIV];
    
    
    //头像
    UIButton * view = [UIButton buttonWithType:UIButtonTypeCustom];
    view.frame = CGRectMake(0, headerLabel.bottom+14, ScreenWidth, 50.0);
    view.backgroundColor = [UIColor whiteColor];
    [view addTarget:self action:@selector(uploadBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    view.tag = 1001;
    [headerView addSubview:view];
    //
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft,10, 38, 30)];
    label.text = @"头像";
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    label.font = font_15;
    [view addSubview:label];
    //头像
    headButton = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - CellLeft - 50,0 , 50,50)];
    headButton.layer.cornerRadius = 25.0;
    headButton.clipsToBounds = YES;
    headButton.layer.borderWidth = 0.5;
    headButton.layer.borderColor = [UIColor blackColor].CGColor;
    [headButton addTarget:self action:@selector(uploadBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    //[headButton setImage:PlaceHolderUserImage forState:UIControlStateNormal];
    [headButton setBackgroundImage:PlaceHolderUserImage forState:UIControlStateNormal];
    
    [view addSubview:headButton];
    
    
    UILabel*headerGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,view.bottom+16.0, ScreenWidth, CellBottom)];
    headerGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [headerView addSubview:headerGapLabel];
    //
    headerView.height = headerGapLabel.bottom;
    [scrollviewn addSubview:headerView];
    if (self.navRightTitle.length>0) {
        view.userInteractionEnabled = NO;
        headButton.userInteractionEnabled = NO;
    }else{
        view.userInteractionEnabled = YES;
        headButton.userInteractionEnabled = YES;
    }
    
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    footerView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    footerView.tag = 999;
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R7030",@"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
    if (self.navRightTitle.length>0) {
        footerView.hidden = YES;
    }else{
        footerView.hidden = NO;
    }
    
    
    //
    [self.view addSubview:footerView];
}

#pragma mark- loadContentView Delegate
- (void)loadContentView {
    nameView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, headerView.bottom, ScreenWidth, 50.0)];
    nameView.tLabel.text = NSLocalizedString(@"R5079", @"昵称");
    nameView.cLabel.text = @"";
    
    [nameView addTarget:self
                  action:@selector(nameBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [scrollviewn addSubview:nameView];
    //
//    phoneView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, nameView.bottom, nameView.width, nameView.height)];
//    phoneView.tLabel.text = NSLocalizedString(@"R5216", @"手机");
//    phoneView.cLabel.text = @"";
//    [phoneView addTarget:self action:@selector(phoneBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//    [scrollviewn addSubview:phoneView];
    phoneView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, nameView.bottom, nameView.width, nameView.height)];
    phoneView.tLabel.text = NSLocalizedString(@"R5216", @"手机");
    phoneView.cLabel.textColor = [UIColor lightGrayColor];
    if ([LocalService getRBLocalDataUserLoginPhone] == nil) {
        phoneView.cLabel.text = @"";
    }else{
        phoneView.cLabel.text = [NSString stringWithFormat:@"%@",[LocalService getRBLocalDataUserLoginPhone]];
    }
    phoneView.arrowLabel.hidden = YES;
    [scrollviewn addSubview:phoneView];
    //
    emailView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, phoneView.bottom, nameView.width, nameView.height)];
    emailView.tLabel.text = @"E-mail";
    emailView.cLabel.textColor = [UIColor lightGrayColor];
    emailView.arrowLabel.hidden = YES;
    [scrollviewn addSubview:emailView];
    //
    genderView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, emailView.bottom, nameView.width, nameView.height)];
    genderView.tLabel.text = NSLocalizedString(@"R5189", @"性别");
    genderView.cLabel.text = @"";
    [genderView addTarget:self
                         action:@selector(genderBtnAction:)
               forControlEvents:UIControlEventTouchUpInside];
    [scrollviewn addSubview:genderView];
    //
    ageView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, genderView.bottom, nameView.width, nameView.height)];
    ageView.tLabel.text = NSLocalizedString(@"R5070", @"年龄");
    ageView.cLabel.text = @"";
    [ageView addTarget:self
                   action:@selector(ageBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    [scrollviewn addSubview:ageView];
    //
    interestView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, ageView.bottom, nameView.width, nameView.height)];
    interestView.tLabel.text = NSLocalizedString(@"R7017",@"兴趣");
    interestView.cLabel.text = @"";
    
    [interestView addTarget:self
                     action:@selector(interestBtnAction:)
           forControlEvents:UIControlEventTouchUpInside];
    [scrollviewn addSubview:interestView];
    //
    //分割label
    UILabel*headerGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,interestView.bottom, ScreenWidth, 10)];
    headerGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [scrollviewn addSubview:headerGapLabel];
    professionView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, headerGapLabel.bottom, nameView.width, nameView.height)];
    professionView.tLabel.text = NSLocalizedString(@"R7016",@"职业");
    professionView.cLabel.text = @"";
    [professionView addTarget:self
                         action:@selector(professionBtnAction:)
               forControlEvents:UIControlEventTouchUpInside];
    [scrollviewn addSubview:professionView];
    
   
    //社交账号
    SocialBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    SocialBtn.frame = CGRectMake(0, professionView.bottom, ScreenWidth, 70);
    [SocialBtn addTarget:self action:@selector(SocialAction:) forControlEvents:UIControlEventTouchUpInside];
    [scrollviewn addSubview:SocialBtn];
    UILabel * socialLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 10, 70, 50)];
    socialLabel.font = font_15;
    socialLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    socialLabel.text = @"社交账号";
    [SocialBtn addSubview:socialLabel];
    NSArray * arr = @[@"icon-weibo-off",@"icon-qq-off",@"icon-wechat-off"];
    for (NSInteger i = 0; i<3; i++) {
        UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth - CellLeft-13-44-5)-i*(44+5), 13, 44, 44)];
        imageView.image = [UIImage imageNamed:arr[i]];
        imageView.tag = 1000+i;
        [SocialBtn addSubview:imageView];
    }
    UILabel*arrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-CellLeft-13.0, (70-13.0)/2.0, 13.0, 13.0)];
    arrowLabel.font = font_icon_(13.0);
    arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    arrowLabel.text = Icon_btn_right;
    arrowLabel.tag = 998;
    [SocialBtn addSubview:arrowLabel];
    
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 70-0.5, ScreenWidth, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [SocialBtn addSubview:lineLabel];
    //简介
    descriptionView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, SocialBtn.bottom, nameView.width, nameView.height*2.0)];
    descriptionView.tLabel.text = NSLocalizedString(@"R7018",@"简介");
    descriptionView.cLabel.text = @"";
    descriptionView.cLabel.numberOfLines = 0;
    [descriptionView addTarget:self
                             action:@selector(descriptionBtnAction:)
                   forControlEvents:UIControlEventTouchUpInside];
    [scrollviewn addSubview:descriptionView];
    if (self.navRightTitle.length>0) {
        nameView.arrowLabel.hidden = YES;
        genderView.arrowLabel.hidden = YES;
        ageView.arrowLabel.hidden = YES;
        interestView.arrowLabel.hidden = YES;
        professionView.arrowLabel.hidden = YES;
        arrowLabel.hidden = YES;
        descriptionView.arrowLabel.hidden = YES;
        
        
        nameView.userInteractionEnabled = NO;
        genderView.userInteractionEnabled = NO;
        ageView.userInteractionEnabled = NO;
        interestView.userInteractionEnabled = NO;
        professionView.userInteractionEnabled = NO;
        SocialBtn.userInteractionEnabled = NO;
        descriptionView.userInteractionEnabled = NO;
    }else{
        nameView.arrowLabel.hidden = NO;
        genderView.arrowLabel.hidden = NO;
        ageView.arrowLabel.hidden = NO;
        interestView.arrowLabel.hidden = NO;
        professionView.arrowLabel.hidden = NO;
        arrowLabel.hidden = NO;
        descriptionView.arrowLabel.hidden = NO;
        
        
        nameView.userInteractionEnabled = YES;
        genderView.userInteractionEnabled = YES;
        ageView.userInteractionEnabled = YES;
        interestView.userInteractionEnabled = YES;
        professionView.userInteractionEnabled = YES;
        SocialBtn.userInteractionEnabled = YES;
        descriptionView.userInteractionEnabled = YES;
    }
    
    
    //
//    uploadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    uploadBtn.frame = CGRectMake(CellLeft, gapLabel.bottom+CellLeft, ScreenWidth-2*CellLeft, ScreenWidth*450.0/375.0);
//    [uploadBtn setBackgroundImage:[UIImage imageNamed:@"pic_kol_upload.png"] forState:UIControlStateNormal];
//    [uploadBtn addTarget:self
//                  action:@selector(uploadBtnAction:)
//        forControlEvents:UIControlEventTouchUpInside];
//    [scrollviewn addSubview:uploadBtn];
    //
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, descriptionView.bottom+CellLeft)];
}
//点击社交账号触发的方法
-(void)SocialAction:(id)sender
{
    RBKolApplySocialViewController *toview = [[RBKolApplySocialViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    toview.KolApplyDelegate = self;
    [self.navigationController pushViewController:toview animated:YES];
}
-(void)updataSocial:(NSString *)index{
    if ([index isEqualToString:@"1"]) {
        [_dataDic setValue:@"1" forKey:@"wechat"];
        UIImageView * imageView = [SocialBtn viewWithTag:1002];
        imageView.image = [UIImage imageNamed:@"icon-wechat-on"];
    }else if ([index isEqualToString:@"2"]){
        UIImageView * imageView = [SocialBtn viewWithTag:1001];
        imageView.image = [UIImage imageNamed:@"icon-qq-on"];
        [_dataDic setValue:@"1" forKey:@"qq"];
    }else if([index isEqualToString:@"3"]){
        UIImageView * imageView = [SocialBtn viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon-weibo-on"];
        [_dataDic setValue:@"1" forKey:@"weibo"];
    }else if ([index isEqualToString:@"4"]){
        [_dataDic setValue:@"0" forKey:@"wechat"];
        UIImageView * imageView = [SocialBtn viewWithTag:1002];
        imageView.image = [UIImage imageNamed:@"icon-wechat-off"];
    }else if ([index isEqualToString:@"5"]){
        UIImageView * imageView = [SocialBtn viewWithTag:1001];
        imageView.image = [UIImage imageNamed:@"icon-qq-off"];
        [_dataDic setValue:@"0" forKey:@"qq"];
    }else{
        UIImageView * imageView = [SocialBtn viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon-weibo-off"];
        [_dataDic setValue:@"0" forKey:@"weibo"];
    }
    
}
-(void)RBNotificationRefreshApply
{
    
    if(![JsonService isRBUserVisitor]) {
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBKOLDetailWithKolId:[LocalService getRBLocalDataUserLoginId]];
        [handler getRBInfluenceCityName];
    }
}
#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"big_v/detail"]) {
        kolEntity = [JsonService getRBKolDetailEntity:jsonObject];
        for (RBKOLSocialEntity * socialEntity in kolEntity.social_accounts) {
            
            if ([socialEntity.provider isEqualToString:@"wechat"]) {
                UIImageView * imageView = [SocialBtn viewWithTag:1002];
                imageView.image = [UIImage imageNamed:@"icon-wechat-on"];
                [self.dataDic setValue:@"1" forKey:@"wechat"];
            }else if ([socialEntity.provider isEqualToString:@"qq"]){
                UIImageView * imageView = [SocialBtn viewWithTag:1001];
                imageView.image = [UIImage imageNamed:@"icon-qq-on"];
                [self.dataDic setValue:@"1" forKey:@"qq"];
            }else if ([socialEntity.provider isEqualToString:@"weibo"]){
                UIImageView * imageView = [SocialBtn viewWithTag:1000];
                imageView.image = [UIImage imageNamed:@"icon-weibo-on"];
                [self.dataDic setValue:@"1" forKey:@"weibo"];
            }
            
        }
        nameView.cLabel.text = kolEntity.name;
        if ([NSString stringWithFormat:@"%@",kolEntity.gender].intValue==1){
            genderView.cLabel.text = NSLocalizedString(@"R5068",  @"男");
        } else if ([NSString stringWithFormat:@"%@",kolEntity.gender].intValue==2){
            genderView.cLabel.text = NSLocalizedString(@"R5069",  @"女");
        } else {
            genderView.cLabel.text = NSLocalizedString(@"R5067", @"未填写");
        }
        emailView.cLabel.text = kolEntity.email;
        ageView.cLabel.text = kolEntity.age;
        professionView.cLabel.text = kolEntity.job_info;
        NSString*str = @"";
        NSString*strEn = @"";
        for (int i=0; i< [kolEntity.tags count];i++) {
            RBTagEntity*entity = kolEntity.tags[i];
            if(i==0) {
                str = [NSString stringWithFormat:@"%@",entity.label];
                strEn = [NSString stringWithFormat:@"%@",entity.name];
            } else {
                str = [NSString stringWithFormat:@"%@,%@",str,entity.label];
                strEn = [NSString stringWithFormat:@"%@,%@",strEn,entity.name];
            }
        }
        interestView.interest = strEn;
        interestView.cLabel.text = str;
        descriptionView.cLabel.text = kolEntity.desc;
        [headButton sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",kolEntity.avatar_url]] forState:UIControlStateNormal placeholderImage:PlaceHolderUserImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            uploadImg = image;
        }];
    }
    
    if ([sender isEqualToString:@"update_profile"]) {
        
        [self.hudView dismiss];
        RBKolApplySubmitViewController *toview = [[RBKolApplySubmitViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        if(_path.length>0){
            toview.comePath = _path;
        }
        [self.navigationController pushViewController:toview animated:YES];
//        RBKolApplySocialViewController *toview = [[RBKolApplySocialViewController alloc] init];
//        toview.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:toview animated:YES];
       // RBKolSocialButton *socialBtn  = (RBKolSocialButton*)[scrollviewn viewWithTag:2000];
        //判断微信是否绑定
//        BOOL IsOn = NO;
//        for (RBKOLSocialEntity * socialEntity in kolEntity.social_accounts) {
//            if ([socialEntity.provider isEqualToString:@"wechat"]) {
//                IsOn = YES;
//            }
//            
//        }
//        if(IsOn) {
//            [self.hudView show];
//            Handler*handler = [Handler shareHandler];
//            handler.delegate = self;
//            [handler getRBKOLApplyWithShows:@""];
//        } else {
//            [self.hudView showErrorWithStatus:NSLocalizedString(@"R7029",@"必须绑定微信才能接受活动")];
//        }

    }
    if ([sender isEqualToString:@"submit_apply"]) {
        [self.hudView dismiss];
        
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
