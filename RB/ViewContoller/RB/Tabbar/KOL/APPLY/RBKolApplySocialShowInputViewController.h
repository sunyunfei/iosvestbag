//
//  RBKolApplySocialShowInputViewController.h
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolEditView.h"

@interface RBKolApplySocialShowInputViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,RBPictureUploadDelegate,RBActionSheetDelegate>
@property(nonatomic, strong) RBKolEditView*editButton;
@property(nonatomic, strong) NSString*kolshows;

@end
