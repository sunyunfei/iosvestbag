//
//  RBModifyPhoneViewController.m
//  RB
//
//  Created by RB8 on 2017/7/14.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBModifyPhoneViewController.h"
#import "RBSetPhoneViewController.h"
@interface RBModifyPhoneViewController ()
{
    InsetsTextField * FirstPassTF;
}
@end

@implementation RBModifyPhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = YES;
    self.delegate = self;
    self.navTitle = @"修改手机号";
    [self loadEditView];
}
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)loadEditView{
    FirstPassTF = [[InsetsTextField alloc]
                   initWithFrame:CGRectMake(15, (ScreenHeight-3*50)/2, ScreenWidth-30.0, 50.0)];
    FirstPassTF.font = font_(16.0);
    FirstPassTF.backgroundColor = [UIColor clearColor];
    FirstPassTF.keyboardType = UIKeyboardTypeNumberPad;
    FirstPassTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    FirstPassTF.attributedPlaceholder = [[NSAttributedString alloc]
                                         initWithString:@"输入密码(不少于6位)"
                                         attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
    FirstPassTF.returnKeyType = UIReturnKeyDone;
    FirstPassTF.delegate = self;
    [self.view addSubview:FirstPassTF];
    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]
                                      initWithFrame:CGRectMake(0, ScreenHeight - 216 - 44,
                                                               ScreenWidth, 216 + 44)];
    textFiledKB.backgroundColor = [UIColor clearColor];
    textFiledKB.delegateT = self;
    [FirstPassTF setInputAccessoryView:textFiledKB];
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(FirstPassTF.left,FirstPassTF.bottom-0.5f, FirstPassTF.width, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    [self.view addSubview:lineLabel];
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake((ScreenWidth -200)/2, lineLabel.bottom+100, 200, 40);
    btn.layer.cornerRadius = 4;
    btn.layer.borderColor = [UIColor blackColor].CGColor;
    btn.layer.borderWidth = 0.5;
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTitle:@"下一步" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(NextAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
}
-(void)NextAction:(id)sender{
    RBSetPhoneViewController * SetPhone = [[RBSetPhoneViewController alloc]init];
    [self.navigationController pushViewController:SetPhone animated:YES];
}
#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}
#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEdit];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}
#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
