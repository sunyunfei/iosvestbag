//
//  RBKolApplySocialViewController.m
//  RB
//
//  Created by AngusNi on 16/8/2.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolApplySocialViewController.h"

@interface RBKolApplySocialViewController () {
    UIScrollView*scrollviewn;
    UIView*headerView;
    RBKolEditView*showView;
    NSArray*socialArray;
    RBKOLEntity*kolEntity;
    NSString * userName;
    NSString * editString;
    NSString * relieveString;
    NSString * socialKolid;
    NSString * socialProvider;
    NSInteger  buttonTag;
    NSString * socialKolid1;
    //
    NSMutableArray*array;
    NSMutableArray*array1;
    NSMutableArray*array2;
    NSMutableArray*array3;
    NSMutableArray*array4;
    int uploadCount;
    NSMutableArray * datalist;
    //
    NSArray * providerArray;
    void(^jumpblock)(void);
}
@end

@implementation RBKolApplySocialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
   // self.navTitle = NSLocalizedString(@"R7048",@"申请成为KOL");
    self.navTitle = NSLocalizedString(@"R7055", @"社交账号绑定");
    self.navRightTitle = NSLocalizedString(@"R7057", @"编辑");
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height-NavHeight-45.0)];
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.userInteractionEnabled = YES;
    [self.view addSubview:scrollviewn];
    //
    [self loadHeaderView];
    [self loadContentView];
 //   [self loadFooterView];
    //
    // 获取通讯录
    [self getAddressBookInfo];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    if(![JsonService isRBUserVisitor]) {
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBKOLDetailWithKolId:[LocalService getRBLocalDataUserLoginId]];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageBegin:@"kol-apply-social"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"kol-apply-social"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)navRightBtnAction {
    if ([self.navRightTitle isEqualToString:NSLocalizedString(@"R7057", @"编辑")]) {
        self.navRightTitle = NSLocalizedString(@"R7058", @"取消");
        for (int i=0; i<[socialArray count]; i++) {
            RBKolSocialButton *socialBtn  = (RBKolSocialButton*)[scrollviewn viewWithTag:2000+i];
            for (RBKOLSocialEntity*social in kolEntity.social_accounts) {
                if([social.provider_name isEqualToString:socialArray[i]]) {
                   CAKeyframeAnimation *animation = (CAKeyframeAnimation *)[socialBtn.layer animationForKey:@"shakeAnimation"];
                    if (animation == nil) {
                        CAKeyframeAnimation *anima = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation"];
                        //在这里@"transform.rotation"==@"transform.rotation.z"
                        NSValue *value1 = [NSNumber numberWithFloat:-M_PI/180*4];
                        NSValue *value2 = [NSNumber numberWithFloat:M_PI/180*4];
                        NSValue *value3 = [NSNumber numberWithFloat:-M_PI/180*4];
                        anima.values = @[value1,value2,value3];
                        anima.repeatCount = MAXFLOAT;  // 次数为不限
                        //anima.duration = 0.25;
                        //恢复原样
                        //anima.autoreverses = YES;
                        
                        anima.fillMode = kCAFillModeForwards;
                        anima.removedOnCompletion = NO;
                        [socialBtn.layer addAnimation:anima forKey:@"shakeAnimation"];
                    }else{
                        socialBtn.layer.speed = 1.0;
                    }
                    socialBtn.deleteButton.hidden = NO;
                    __weak typeof(self) weakSelf = self;
                    //点击删除按键触发解绑
                    socialBtn.deleteShakeBlock = ^(){
                        if(socialBtn.tag == 2000){
                            relieveString = @"wechat";
                        }else if (socialBtn.tag == 2001){
                            relieveString = @"qq";
                        }else if(socialBtn.tag == 2002){
                            relieveString = @"weibo";
                        }
                        NSLog(@"删除了");
//                        RBAlertView *alertView = [[RBAlertView alloc]init];
//                        alertView.delegate = self;
//                        alertView.tag = 1000;
//                        [alertView showWithArray:@[NSLocalizedString(@"R5044", @"是否允许ROBIN8\n获取您的手机通讯录？"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
                        
                        socialKolid = social.kol_id;
                        socialProvider = social.provider;
                        buttonTag = socialBtn.tag;
                        [weakSelf clearTimes:[LocalService getRBLocalDataUserLoginId] andProvider:social.provider];
                        
//                        if (buttonTag == 2000||buttonTag == 2001||buttonTag == 2002) {
//                            [self.hudView show];
//                            Handler*handler = [Handler shareHandler];
//                            handler.delegate = self;
//                            [handler getRBUserInfoThirdAccountList];
//                             [handler relieveRBKOLAccount:[LocalService getRBLocalDataUserLoginId] andProvider:socialProvider andId:socialKolid];
//                        }else{
//                            [self.hudView show];
//                            Handler*handler = [Handler shareHandler];
//                            handler.delegate = self;
//                            [handler relieveRBKOLAccount:[LocalService getRBLocalDataUserLoginId] andProvider:socialProvider andId:socialKolid];
//                        }

                        
                        
//                        if (socialBtn.tag == 2000 || socialBtn.tag == 2001 || socialBtn.tag == 2002) {
//                            RBAlertView *alertView = [[RBAlertView alloc]init];
//                            alertView.delegate = weakSelf;
//                            alertView.tag = 14000;
//                            [alertView showWithArray:@[NSLocalizedString(@"R5217", @"确定要解绑这个账号吗？"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
//                            
//                        }else{
//                            RBAlertView *alertView = [[RBAlertView alloc]init];
//                            alertView.delegate = weakSelf;
//                            alertView.tag = 14001;
//                            [alertView showWithArray:@[NSLocalizedString(@"R5217", @"确定要解绑这个账号吗？"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
//                            
//                        }
                        
                    };
                }
            }
        }
    }else if ([self.navRightTitle isEqualToString:NSLocalizedString(@"R7058", @"取消")]){
        self.navRightTitle = NSLocalizedString(@"R7057", @"编辑");
        for (int i=0; i<[socialArray count]; i++) {
            RBKolSocialButton *socialBtn  = (RBKolSocialButton*)[scrollviewn viewWithTag:2000+i];
            for (RBKOLSocialEntity*social in kolEntity.social_accounts) {
                if([social.provider_name isEqualToString:socialArray[i]]) {
//                    socialBtn.iconIV.userInteractionEnabled = YES;
                    CAKeyframeAnimation *animation = (CAKeyframeAnimation *)[socialBtn.layer animationForKey:@"shakeAnimation"];
                    if (animation) {
                        socialBtn.layer.speed = 0.0;
                    }
                    socialBtn.deleteButton.hidden = YES;
                }
            }
        }

    }
}
- (void)footerBtnAction:(UIButton*)sender {
    RBKolSocialButton *socialBtn  = (RBKolSocialButton*)[scrollviewn viewWithTag:2000];
    if([socialBtn.is_ON isEqualToString:@"YES"]) {
        [self.hudView show];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBKOLApplyWithShows:showView.shows];
    } else {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R7029",@"必须绑定微信才能接受活动")];
    }
}

- (void)showBtnAction:(RBKolEditView*)sender {
    RBKolApplySocialShowInputViewController *toview = [[RBKolApplySocialShowInputViewController alloc] init];
    toview.editButton = sender;
    toview.kolshows = showView.shows;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)socialBtnAction:(RBKolSocialButton*)sender {
    NSString*str = socialArray[sender.tag-2000];
    RBKolSocialButton * socialBtn = (RBKolSocialButton*)sender;
    if ([socialBtn.is_ON isEqualToString:@"YES"]) {
        RBAlertView * alertView = [[RBAlertView alloc]init];
        [alertView showWithArray:@[[NSString stringWithFormat:@"已绑定%@",socialArray[sender.tag-2000]],@"确定"]];
        return;
    }
    RBKOLSocialEntity*socialEntity = nil;
    for (RBKOLSocialEntity*social in kolEntity.social_accounts) {
        if([social.provider_name isEqualToString:str]) {
            socialEntity = social;
        }
    }
    for (int i=0; i<[socialArray count]; i++) {
        RBKolSocialButton *socialBtn  = (RBKolSocialButton*)[scrollviewn viewWithTag:2000+i];
        socialBtn.deleteButton.hidden = YES;
        CAKeyframeAnimation *animation = (CAKeyframeAnimation *)[socialBtn.layer animationForKey:@"shakeAnimation"];
        if (animation) {
            socialBtn.layer.speed = 0.0;
        }
    }
    buttonTag = sender.tag;
    self.navRightTitle = NSLocalizedString(@"R7057", @"编辑");
    
   // [weakSelf.navigationController pushViewController:toview animated:YES];
  //  [self searchRelieveTimes:[LocalService getRBLocalDataUserLoginId] andProvider:providerArray[sender.tag-2000]];
    if (sender.tag-2000==0||sender.tag-2000==1||sender.tag-2000==2) {
        //微信、微博、qq
        //查询绑定次数
        __weak typeof(self) weakSelf = self;
        jumpblock = ^(void){
            [weakSelf ThirdLoginWithTag:(int)sender.tag +3000];
        };
    } else if (sender.tag-2000==3) {
        __weak NSArray * socialArr = socialArray;
        __weak typeof(self) weakSelf = self;
        jumpblock = ^(void){
            RBKolApplySocialPublicInputViewController *toview = [[RBKolApplySocialPublicInputViewController alloc] init];
            toview.editButton = sender;
            toview.editString = socialArr[sender.tag-2000];
            toview.socialEntity = socialEntity;
            toview.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:toview animated:YES];
        };
        
    } else {
        __weak NSArray * socialArr = socialArray;
        __weak typeof(self) weakSelf = self;
        jumpblock = ^(void){
            RBKolApplySocialInputViewController *toview = [[RBKolApplySocialInputViewController alloc] init];
            toview.editString = socialArr[sender.tag-2000];
            toview.editButton = sender;
            toview.socialEntity = socialEntity;
            toview.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:toview animated:YES];
        };
    }
    jumpblock();
}
#pragma 查询解绑次数
-(void)clearTimes:(NSString*)kolid andProvider:(NSString*)provider{
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler searchRBKOLClearTimes:kolid AndProvider:provider];
}
#pragma 查询绑定次数
-(void)searchRelieveTimes:(NSString*)kolid andProvider:(NSString*)provider{
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler seeaRBKOLrelieveTimes:kolid AndProvider:provider];
}
- (void)endEdit {
    [self.view endEditing:YES];
}
- (void)ThirdLoginWithTag:(int)tag {
    SSDKPlatformType type;
    RBKolSocialButton *socialBtn  = (RBKolSocialButton*)[scrollviewn viewWithTag:tag-3000];
    NSString*provider=@"";
    if (tag == 5002) {
        provider = @"weibo";
        type = SSDKPlatformTypeSinaWeibo;
    } else if (tag == 5000) {
        provider = @"wechat";
        type = SSDKPlatformTypeWechat;
    } else {
        provider = @"qq";
        type = SSDKPlatformTypeQQ;
    }
    [ShareSDK cancelAuthorize:type];
    [ShareSDK getUserInfo:type
           onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error){
               NSLog(@"user:%@,credential:%@,rawData:%@",user,user.credential,user.rawData);
               if (state == SSDKResponseStateSuccess) {
                   userName = [NSString stringWithFormat:@"%@",user.nickname];
                   socialBtn.is_ON = @"YES";
                   //screenView.tLabel.text = [NSString stringWithFormat:@"%@已绑定 %@",self.editString,userName];
                   // RB-社交账号绑定
                   Handler*handler = [Handler shareHandler];
                   //handler.delegate = self;
                   if (type == SSDKPlatformTypeWechat) {
                       NSString*unionid = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"unionid"]];
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       editString = @"微信";
                      // socialBtn.iconIV.image = [UIImage imageNamed:@"微信_ON.png"];
//                       [handler getV2RBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:unionid andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:unionid andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                       [self.hudView show];
                       
                   }
                   if (type == SSDKPlatformTypeSinaWeibo) {
                       editString = @"微博";
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*followers_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"followers_count"]];
                       NSString*created_at = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"created_at"]];
                       NSString*statuses_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"statuses_count"]];
                       NSString*verified = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"verified"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"avatar_large"]];
                       NSString*refresh_token = nil;
                       NSString*str = [NSString stringWithFormat:@"%@",user.credential];
                       if([[str componentsSeparatedByString:@"\"refresh_token\" = \""] count]>1) {
                           str = [str componentsSeparatedByString:@"\"refresh_token\" = \""][1];
                           if([[str componentsSeparatedByString:@"\";"] count]>1) {
                               refresh_token = [str componentsSeparatedByString:@"\";"][0];
                           }
                       }
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:followers_count andStatuses_count:statuses_count andRegistered_at:created_at andVerified:verified andRefresh_token:refresh_token andUnionid:nil andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                       //socialBtn.iconIV.image = [UIImage imageNamed:@"微博_ON.png"];
//                       [handler getV2RBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:followers_count andStatuses_count:statuses_count andRegistered_at:created_at andVerified:verified andRefresh_token:refresh_token andUnionid:nil andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                       
                       [self.hudView show];
                        }
                   if (type == SSDKPlatformTypeQQ) {
                       editString = @"QQ";
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*city = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"city"]];
                       NSString*is_yellow_vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"is_yellow_vip"]];
                       NSString*vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"vip"]];
                       NSString*province = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"province"]];
                       NSString*gender = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"gender"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"figureurl_qq_2"]];
                     //  socialBtn.iconIV.image = [UIImage imageNamed:@"QQ_ON.png"];
//                       [handler getV2RBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:nil andProvince:province andCity:city andGender:gender andIs_vip:vip andIs_yellow_vip:is_yellow_vip];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:nil andProvince:province andCity:city andGender:gender andIs_vip:vip andIs_yellow_vip:is_yellow_vip];
                       [self.hudView show];
                      
                   }
               }
               if (state == SSDKResponseStateFail) {
                   if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                       [self.hudView showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                   } else {
                       [self.hudView showErrorWithStatus:NSLocalizedString(@"R1042", @"登录失败")];
                   }
               }
               if (state == SSDKResponseStateCancel) {
                   [self.hudView dismiss];
                   //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
               }
           }];
}

//- (void)screenBtnAction:(UIButton *)sender {
//    [self endEdit];
//    //
//    if ([self.editString isEqualToString:@"微信"]) {
//        [self ThirdLoginWithTag:5001];
//    } else if ([self.editString isEqualToString:@"微博"]) {
//        [self ThirdLoginWithTag:5000];
//    } else if ([self.editString isEqualToString:@"QQ"]) {
//        [self ThirdLoginWithTag:5002];
//    }
//}
#pragma mark- loadHeaderView Delegate
- (void)loadHeaderView {
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 0)];
    headerView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    //
    UILabel*headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40.0)];
    headerLabel.font = font_cu_13;
    headerLabel.text = NSLocalizedString(@"R7020",@"资料完善度越高，接受的活动价格越高");
    headerLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.8];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorSubBlack];
    [headerView addSubview:headerLabel];
    //
//    UIImageView*headerIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, headerLabel.bottom+24.0, ScreenWidth, 30.0)];
//    headerIV.image = [UIImage imageNamed:@"pic_kol_step_1.png"];
//    [headerView addSubview:headerIV];
    //
    UILabel*headerGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, headerLabel.bottom+24.0, ScreenWidth, CellBottom)];
    headerGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [headerView addSubview:headerGapLabel];
    //
    headerView.height = headerGapLabel.bottom;
    [scrollviewn addSubview:headerView];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    footerView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R7030",@"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
    //
    [self.view addSubview:footerView];
}

#pragma mark- loadContentView Delegate
- (void)loadContentView {
    UILabel*contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, headerView.bottom, ScreenWidth, 50.0)];
    contentLabel.font = font_15;
    contentLabel.text = NSLocalizedString(@"R7031",@"社交账号信息");
    contentLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [scrollviewn addSubview:contentLabel];
    //
    socialArray = @[@"微信",@"QQ",@"微博",@"公众号",@"美拍",@"秒拍",@"知乎",@"斗鱼",@"映客",@"贴吧",@"天涯",@"淘宝",@"花椒", @"NICE", @"豆瓣", @"小红书", @"一直播", @"美啦",@"其它"];
    providerArray = @[@"wechat",@"qq",@"weibo",@"public_wechat",@"meipai",@"miaopai",@"zhihu",@"douyu",@"yingke",@"tieba",@"tianya",@"taobao",@"huajiao",@"nice",@"douban",@"xiaohongshu",@"yizhibo",@"meila",@"other"];
    float tempGap = (scrollviewn.width-2.0*CellLeft-4.0*45.0)/3.0;
    for (int i=0; i<[socialArray count]; i++) {
        RBKolSocialButton *socialBtn = [[RBKolSocialButton alloc]initWithFrame:CGRectMake(CellLeft+(45.0+tempGap)*(i%4), contentLabel.bottom+(60.0+CellLeft)*(i/4), 45.0, 60.0)];
        socialBtn.iconIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",socialArray[i]]];
        socialBtn.tLabel.text = socialArray[i];
        socialBtn.is_ON = @"NO";
        socialBtn.tag = 2000+i;
        [socialBtn addTarget:self
                      action:@selector(socialBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        [scrollviewn addSubview:socialBtn];
    }
    //
//    UILabel*contentGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, contentLabel.bottom+([socialArray count]/4+1)*(60.0+CellLeft)+CellLeft, ScreenWidth, CellBottom)];
//    contentGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
//    [scrollviewn addSubview:contentGapLabel];
//    //
//    showView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, contentGapLabel.bottom, ScreenWidth, 50.0)];
//    showView.tLabel.text = NSLocalizedString(@"R7028",@"个人精彩展示");
//    showView.cLabel.text = @"0";
//    [showView addTarget:self
//                 action:@selector(showBtnAction:)
//       forControlEvents:UIControlEventTouchUpInside];
//    showView.backgroundColor = [UIColor whiteColor];
//    [scrollviewn addSubview:showView];
//    //
//    UILabel*showGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, showView.bottom, ScreenWidth, CellBottom)];
//    showGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
//    [scrollviewn addSubview:showGapLabel];
    //
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, contentLabel.bottom+([socialArray count]/4+1)*(60.0+CellLeft)+CellLeft)];
}

// 获取本机通讯录信息
- (void)getAddressBookInfo {
    CFErrorRef *error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    __block BOOL accessGranted = NO;
    if (&ABAddressBookRequestAccessWithCompletion != NULL) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    if (accessGranted == NO) {
//        RBAlertView *alertView = [[RBAlertView alloc]init];
//        alertView.delegate = self;
//        alertView.tag = 1000;
//        [alertView showWithArray:@[NSLocalizedString(@"R5044", @"是否允许ROBIN8\n获取您的手机通讯录？"),NSLocalizedString(@"R1020", @"确定"),NSLocalizedString(@"R1011", @"取消")]];
        return;
    } else {
        array = [NSMutableArray new];
        array1 = [NSMutableArray new];
        array2 = [NSMutableArray new];
        array3 = [NSMutableArray new];
        array4 = [NSMutableArray new];
        NSArray*listContacts = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
        for (int i=0; i<[listContacts count]; i++) {
            ABRecordRef thisPerson = CFBridgingRetain(listContacts[i]);
            NSString *firstName = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonFirstNameProperty));
            NSString *lastName = CFBridgingRelease(ABRecordCopyValue(thisPerson, kABPersonLastNameProperty));
            ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(thisPerson, kABPersonPhoneProperty);
            NSArray* phoneNumberArray = CFBridgingRelease(ABMultiValueCopyArrayOfAllValues(phoneNumberProperty));
            for(int index = 0; index<[phoneNumberArray count]; index++) {
                NSMutableDictionary*diclist = [NSMutableDictionary new];
                NSString*fName = @"";
                NSString*sName = @"";
                if (firstName!=nil&&![firstName isEqual:[NSNull null]]&&firstName.length!=0) {
                    fName = firstName;
                }
                if (lastName!=nil&&![lastName isEqual:[NSNull null]]&&lastName.length!=0) {
                    sName = lastName;
                }
                NSString*mobile = [NSString stringWithFormat:@"%@",phoneNumberArray[index]];
                mobile = [mobile stringByReplacingOccurrencesOfString:@" (" withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@") " withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
                mobile = [mobile stringByReplacingOccurrencesOfString:@"+86" withString:@""];
                if (mobile.length==11 && [Utils getIsValidateNumber:mobile]) {
                    [diclist setObject:[NSString stringWithFormat:@"%@%@",sName,fName] forKey:@"name"];
                    [diclist setObject:[NSString stringWithFormat:@"%@",mobile] forKey:@"mobile"];
                    if([array count]<2000) {
                        [array addObject:diclist];
                    } else {
                        if ([array1 count]<2000) {
                            [array1 addObject:diclist];
                        } else {
                            if ([array2 count]<2000) {
                                [array2 addObject:diclist];
                            } else {
                                if ([array3 count]<2000) {
                                    [array3 addObject:diclist];
                                } else {
                                    [array4 addObject:diclist];
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if ([array count]!=0) {
            uploadCount = (int)[array count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBSocialInfluenceContactWithArray:array];
        }
    }
}

#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if(alertView.tag==1000) {
        if (buttonIndex==0) {
            if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
        }
    }else if (alertView.tag == 14000){
        if (buttonIndex==0) {
            [self.hudView show];
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBUserInfoThirdAccountList];
            }else{
            
            
        }
    }else if (alertView.tag == 14001){
        if (buttonIndex == 0) {
            [self.hudView show];
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler relieveRBKOLAccount:[LocalService getRBLocalDataUserLoginId] andProvider:socialProvider andId:socialKolid];
        }else{
            
        }
    }
}
#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"influences/bind_contacts"]) {
//        Handler*handler = [Handler shareHandler];
//        handler.delegate = self;
//        [handler getRBInfluenceSmsWithMobile:@"18817774982"];
        if(uploadCount==2000&&[array1 count]!=0) {
            uploadCount = uploadCount+(int)[array1 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array1];
            return;
        }
        if(uploadCount==4000&&[array2 count]!=0) {
            uploadCount = uploadCount+(int)[array2 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array2];
            return;
        }
        if(uploadCount==6000&&[array3 count]!=0) {
            uploadCount = uploadCount+(int)[array3 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array3];
            return;
        }
        if(uploadCount==8000&&[array4 count]!=0) {
            uploadCount = uploadCount+(int)[array4 count];
            // RB-上传通讯录
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBInfluenceContactWithArray:array4];
            return;
        }
    }

    if ([sender isEqualToString:@"big_v/detail"]) {
        kolEntity = [JsonService getRBKolDetailEntity:jsonObject];
        for (int i=0; i<[socialArray count]; i++) {
            RBKolSocialButton *socialBtn  = (RBKolSocialButton*)[scrollviewn viewWithTag:2000+i];
            socialBtn.deleteButton.hidden = YES;
            CAKeyframeAnimation *animation = (CAKeyframeAnimation *)[socialBtn.layer animationForKey:@"shakeAnimation"];
            if (animation) {
                socialBtn.layer.speed = 0.0;
            }
            for (RBKOLSocialEntity*social in kolEntity.social_accounts) {
                if([social.provider_name isEqualToString:socialArray[i]]) {
                    socialBtn.iconIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_ON.png",socialArray[i]]];
                    socialBtn.is_ON = @"YES";
                }
            }
        }
        showView.cLabel.text = [NSString stringWithFormat:@"%d",(int)[kolEntity.kol_shows count]];
        NSString*str = @"";
        for ( int i=0; i<[kolEntity.kol_shows count]; i++) {
            RBKOLShowEntity*showEntity = kolEntity.kol_shows[i];
            if(i==0){
                str = [NSString stringWithFormat:@"%@",showEntity.link];
            } else {
                str = [NSString stringWithFormat:@"%@,%@",str,showEntity.link];
            }
        }
        showView.shows = str;
    }
    //解除账户
    if ([sender isEqualToString:@"unbind_social_account"]) {
        [self.hudView dismiss];
        self.navRightTitle = NSLocalizedString(@"R7057", @"编辑");
        kolEntity.social_accounts = [JsonService getRBAllSocialAccounts:jsonObject andBackArray:kolEntity.social_accounts];
        for (int i=0; i<[socialArray count]; i++) {
            RBKolSocialButton *socialBtn  = (RBKolSocialButton*)[scrollviewn viewWithTag:2000+i];
            socialBtn.iconIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",socialArray[i]]];
            socialBtn.is_ON = @"NO";
            socialBtn.deleteButton.hidden = YES;
            CAKeyframeAnimation *animation = (CAKeyframeAnimation *)[socialBtn.layer animationForKey:@"shakeAnimation"];
            if (animation) {
                socialBtn.layer.speed = 0.0;
            }
            for (RBKOLSocialEntity*social in kolEntity.social_accounts) {
                if([social.provider_name isEqualToString:socialArray[i]]) {
                    socialBtn.iconIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_ON.png",socialArray[i]]];
                    socialBtn.is_ON = @"YES";
                }
                
//                socialBtn.deleteButton.hidden = YES;
            }
        }
        [self.hudView showWithStatus:@"解除绑定成功"];
        if (relieveString.length>0) {
            NSString * str = @"";
            if ([relieveString isEqualToString:@"wechat"]) {
                str = @"4";
            }else if ([relieveString isEqualToString:@"qq"]){
                str = @"5";
            }else if ([relieveString isEqualToString:@"weibo"]){
                str= @"6";
            }
            if ([self.KolApplyDelegate respondsToSelector:@selector(updataSocial:)]) {
                [self.KolApplyDelegate updataSocial:str];
            }
        }
        
    }
    if ([sender isEqualToString:@"submit_apply"]) {
        [self.hudView dismiss];
        //
        
    }
    //获取第三方账号列表
    if ([sender isEqualToString:@"identities"]) {
        [self.hudView dismiss];
        datalist = [NSMutableArray new];
        datalist = [JsonService getRBThirdListEntity:jsonObject andBackArray:datalist];
        [self.hudView show];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        NSString * str =@"";
        for (RBThirdEntity * entity in datalist) {
            if ([entity.provider isEqualToString:relieveString]) {
                str = entity.uid;
            }
        }
        //解绑第三方账号
        [handler getRBUserInfoThirdAccountUnbind:str];
    }
    if ([sender isEqualToString:@"identity_unbind"]) {
        [self.hudView dismiss];
//        Handler*handler = [Handler shareHandler];
//        handler.delegate = self;
//        [handler relieveRBKOLAccount:[LocalService getRBLocalDataUserLoginId] andProvider:socialProvider andId:socialKolid];
    }
    if ([sender isEqualToString:@"kols/identity_bind"]) {
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
//        [handler getV2RBKOLApplyWithProvider:editString andHomepage:nil andUsername:userName andPrice:@"1" andFollowers:@"10" andScreenshot:nil andUid:nil];
        [handler getRBKOLApplyWithProvider:editString andHomepage:nil andUsername:userName andPrice:@"1" andFollowers:@"10" andScreenshot:nil andUid:nil];
    }
    if ([sender isEqualToString:@"update_social"]) {
        [self.hudView dismiss];
        NSString * str = @"";
        if ([editString isEqualToString:@"微信"]) {
            str = @"1";
        }else if ([editString isEqualToString:@"QQ"]){

            str = @"2";
        }else if ([editString isEqualToString:@"微博"]){
            str= @"3";
        }
        if ([self.KolApplyDelegate respondsToSelector:@selector(updataSocial:)]) {
            [self.KolApplyDelegate updataSocial:str];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    if ([sender isEqualToString:@"bind_count"]) {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"确定绑定当前%@?",socialArray[buttonTag-2000]] message:[jsonObject objectForKey:@"detail"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            jumpblock();
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        UIAlertAction * action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:action1];
        [alert addAction:action2];
        [self presentViewController:alert animated:YES completion:nil];
    }
    if ([sender isEqualToString:@"unbind_count"]) {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"确定解绑当前%@?",socialArray[buttonTag-2000]] message:[jsonObject objectForKey:@"detail"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (buttonTag == 2000||buttonTag == 2001||buttonTag == 2002) {
                [self.hudView show];
                Handler*handler = [Handler shareHandler];
                handler.delegate = self;
                [handler getRBUserInfoThirdAccountList];
                [handler relieveRBKOLAccount:[LocalService getRBLocalDataUserLoginId] andProvider:socialProvider andId:socialKolid];
            }else{
                [self.hudView show];
                Handler*handler = [Handler shareHandler];
                handler.delegate = self;
                [handler relieveRBKOLAccount:[LocalService getRBLocalDataUserLoginId] andProvider:socialProvider andId:socialKolid];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        UIAlertAction * action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:action1];
        [alert addAction:action2];
        [self presentViewController:alert animated:YES completion:nil];

    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
