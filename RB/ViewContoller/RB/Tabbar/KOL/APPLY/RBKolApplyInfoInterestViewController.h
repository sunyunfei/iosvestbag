//
//  RBKolApplyInfoInterestViewController.h
//  RB
//
//  Created by AngusNi on 16/8/2.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolEditView.h"

@interface RBKolApplyInfoInterestViewController : RBBaseViewController
<RBBaseVCDelegate>
@property(nonatomic, strong) RBKolEditView*editView;
@end
