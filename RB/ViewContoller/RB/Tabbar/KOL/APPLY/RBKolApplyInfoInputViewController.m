//
//  RBKolApplyInfoInputViewController.m
//  RB
//
//  Created by AngusNi on 16/8/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolApplyInfoInputViewController.h"

@interface RBKolApplyInfoInputViewController () {
    UILabel*numberLabel;
}
@end

@implementation RBKolApplyInfoInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString];
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIView*editView = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight+CellBottom, ScreenWidth, 50.0)];
    editView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:editView];
    //
    self.editTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, NavHeight+CellBottom, ScreenWidth-CellLeft*2.0, 50.0)];
    self.editTF.font = font_15;
    self.editTF.keyboardType =  UIKeyboardTypeDefault;
    self.editTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.editTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString] attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    self.editTF.returnKeyType = UIReturnKeyDone;
    self.editTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    self.editTF.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNotification:) name:UITextFieldTextDidChangeNotification object:nil];
    [self.view addSubview:self.editTF];
    //
    numberLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.editTF.top, ScreenWidth-CellLeft, self.editTF.height)];
    numberLabel.font = font_13;
    numberLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    numberLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:numberLabel];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, numberLabel.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineLabel];
    //
    if ([self.editString isEqualToString:NSLocalizedString(@"R5070",@"年龄")]) {
        DatePickerView *datePickerView = [[DatePickerView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - 216 - 44, self.view.frame.size.width, 216 + 44)];
        datePickerView.backgroundColor = [UIColor clearColor];
        datePickerView.delegate = self;
        [datePickerView ageWithDateOfBirth];
        self.editTF.inputView = datePickerView;
        numberLabel.hidden = YES;
    } else {
        TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
        textFiledKB.delegateT = self;
        [self.editTF setInputAccessoryView:textFiledKB];
        numberLabel.hidden = NO;
    }
    //
    self.editTF.text = self.editText;
    numberLabel.text = [NSString stringWithFormat:@"%d/%d",(int)self.editTF.text.length,self.maxCount];
}

#pragma mark - DatePickerView Delegate
- (void)DatePickerViewFinish:(DatePickerView *)sender {
    self.editTF.text = [NSString stringWithFormat:@"%@", [Utils getUIDateAge:sender.datePicker.date]];
    [self endEdit];
}

- (void)footerBtnAction:(UIButton *)sender {
    if (self.editTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString]];
        return;
    }
    if (self.editTF.text.length>self.maxCount) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R7014",@"输入的字符太长了")];
        return;
    }
    self.editView.cLabel.text = self.editTF.text;
    [self RBNavLeftBtnAction];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
    [self footerBtnAction:nil];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - NSNotification Delegate
-(void)didChangeNotification:(NSNotification*)notification {
    numberLabel.text = [NSString stringWithFormat:@"%d/%d",(int)self.editTF.text.length,self.maxCount];
    if (self.editTF.text.length>self.maxCount) {
        numberLabel.textColor = [UIColor redColor];
    } else {
        numberLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    }
}

@end
