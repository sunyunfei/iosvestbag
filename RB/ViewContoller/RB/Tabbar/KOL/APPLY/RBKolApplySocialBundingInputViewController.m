//
//  RBKolApplySocialBundingInputViewController.m
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolApplySocialBundingInputViewController.h"

@interface RBKolApplySocialBundingInputViewController () {
    InsetsTextField*editTF;
    NSString*userName;
    InsetsTextField*editTF1;
    RBKolEditView*screenView;
    UIImage*uploadImg;
}
@end

@implementation RBKolApplySocialBundingInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString];
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, NavHeight, ScreenWidth-CellLeft*2.0, 40.0)];
    tLabel.font = font_15;
    tLabel.text = [NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString,NSLocalizedString(@"R7022",@"好友人数")];
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [self.view addSubview:tLabel];
    //
    UIView*tBgView = [[UIView alloc] initWithFrame:CGRectMake(0, tLabel.bottom, ScreenWidth, 50.0)];
    tBgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:tBgView];
    //
    editTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, tLabel.bottom, ScreenWidth-CellLeft*2.0, 50.0)];
    editTF.font = font_15;
    editTF.keyboardType =  UIKeyboardTypeNumberPad;
    editTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    editTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"例如: 1000" attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    editTF.returnKeyType = UIReturnKeyDone;
    editTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    editTF.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    editTF.delegate = self;
    [self.view addSubview:editTF];
    //
    UILabel*lineTLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF.top, ScreenWidth, 0.5f)];
    lineTLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineTLabel];
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineLabel];
    //
    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textFiledKB.delegateT = self;
    [editTF setInputAccessoryView:textFiledKB];
    //
    UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineLabel.bottom, ScreenWidth-CellLeft*2.0, tLabel.height)];
    tLabel1.font = font_15;
    tLabel1.text = [NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString,NSLocalizedString(@"R7021",@"报价(元/条)")];
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [self.view addSubview:tLabel1];
    //
    UIView*tBgView1 = [[UIView alloc] initWithFrame:CGRectMake(0, tLabel1.bottom, ScreenWidth, 50.0)];
    tBgView1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:tBgView1];
    //
    editTF1 = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, tLabel1.bottom, ScreenWidth-CellLeft*2.0, 50.0)];
    editTF1.font = font_15;
    editTF1.keyboardType =  UIKeyboardTypeNumberPad;
    editTF1.clearButtonMode = UITextFieldViewModeWhileEditing;
    editTF1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"例如: 500" attributes:@{NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    editTF1.returnKeyType = UIReturnKeyDone;
    editTF1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    editTF1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    editTF1.delegate = self;
    [self.view addSubview:editTF1];
    [editTF1 setInputAccessoryView:textFiledKB];
    //
    UILabel*lineTLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF1.top, ScreenWidth, 0.5f)];
    lineTLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineTLabel1];
    UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF1.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineLabel1];
    //
    UILabel*tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineLabel1.bottom, ScreenWidth-CellLeft*2.0, tLabel.height)];
    tLabel2.font = font_15;
    tLabel2.text = [NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"R7023",@"请绑定"),self.editString,NSLocalizedString(@"R7024",@"账号")];
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [self.view addSubview:tLabel2];
    //
    UIView*editBgView2 = [[UIView alloc]initWithFrame:CGRectMake(0, tLabel2.bottom, ScreenWidth, 50.0)];
    editBgView2.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:editBgView2];
    //
    screenView = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, editBgView2.top, ScreenWidth, 50.0)];
    screenView.tLabel.text = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R7025",@"点击打开"),self.editString];
    screenView.cLabel.text = @"";
    [screenView addTarget:self
                        action:@selector(screenBtnAction:)
              forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:screenView];
    //
    UILabel*lineTLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, editBgView2.top, ScreenWidth, 0.5f)];
    lineTLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineTLabel2];
    //
    if (self.socialEntity) {
        editTF.text = self.socialEntity.followers_count;
        editTF1.text = self.socialEntity.price;
        screenView.tLabel.text = [NSString stringWithFormat:@"%@%@ %@",NSLocalizedString(@"R7026",@"已绑定"),self.editString,self.socialEntity.username];
    }
}

- (void)footerBtnAction:(UIButton *)sender {
    if (editTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString,NSLocalizedString(@"R7022",@"好友人数")]];
        return;
    }
    if (editTF1.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString,NSLocalizedString(@"R7021",@"报价(元/条)")]];
        return;
    }
    if ([screenView.tLabel.text isEqualToString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R7025",@"点击打开"),self.editString]]) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R7025",@"点击打开"),self.editString]];
        return;
    }
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBKOLApplyWithProvider:self.editString andHomepage:nil andUsername:userName andPrice:editTF1.text andFollowers:editTF.text andScreenshot:nil andUid:nil];
}

- (void)screenBtnAction:(UIButton *)sender {
    [self endEdit];
    //
    if ([self.editString isEqualToString:@"微信"]) {
        [self ThirdLoginWithTag:5001];
    } else if ([self.editString isEqualToString:@"微博"]) {
        [self ThirdLoginWithTag:5000];
    } else if ([self.editString isEqualToString:@"QQ"]) {
        [self ThirdLoginWithTag:5002];
    }
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

- (void)ThirdLoginWithTag:(int)tag {
    SSDKPlatformType type;
    NSString*provider=@"";
    if (tag == 5000) {
        provider = @"weibo";
        type = SSDKPlatformTypeSinaWeibo;
    } else if (tag == 5001) {
        provider = @"wechat";
        type = SSDKPlatformTypeWechat;
    } else {
        provider = @"qq";
        type = SSDKPlatformTypeQQ;
    }
    [ShareSDK cancelAuthorize:type];
    [ShareSDK getUserInfo:type
           onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error){
               NSLog(@"user:%@,credential:%@,rawData:%@",user,user.credential,user.rawData);
               if (state == SSDKResponseStateSuccess) {
                   userName = [NSString stringWithFormat:@"%@",user.nickname];
                   screenView.tLabel.text = [NSString stringWithFormat:@"%@已绑定 %@",self.editString,userName];
                   // RB-社交账号绑定
                   Handler*handler = [Handler shareHandler];
//                   handler.delegate = self;
                   if (type == SSDKPlatformTypeWechat) {
                       NSString*unionid = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"unionid"]];
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:unionid andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                   }
                   if (type == SSDKPlatformTypeSinaWeibo) {
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*followers_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"followers_count"]];
                       NSString*created_at = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"created_at"]];
                       NSString*statuses_count = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"statuses_count"]];
                       NSString*verified = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"verified"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"avatar_large"]];
                       NSString*refresh_token = nil;
                       NSString*str = [NSString stringWithFormat:@"%@",user.credential];
                       if([[str componentsSeparatedByString:@"\"refresh_token\" = \""] count]>1) {
                           str = [str componentsSeparatedByString:@"\"refresh_token\" = \""][1];
                           if([[str componentsSeparatedByString:@"\";"] count]>1) {
                               refresh_token = [str componentsSeparatedByString:@"\";"][0];
                           }
                       }
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:followers_count andStatuses_count:statuses_count andRegistered_at:created_at andVerified:verified andRefresh_token:refresh_token andUnionid:nil andProvince:nil andCity:nil andGender:nil andIs_vip:nil andIs_yellow_vip:nil];
                   }
                   if (type == SSDKPlatformTypeQQ) {
                       NSString*serial_params = [NSString stringWithFormat:@"%@",[user.rawData JSONRepresentation]];
                       NSString*city = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"city"]];
                       NSString*is_yellow_vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"is_yellow_vip"]];
                       NSString*vip = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"vip"]];
                       NSString*province = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"province"]];
                       NSString*gender = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"gender"]];
                       user.icon = [NSString stringWithFormat:@"%@",[user.rawData objectForKey:@"figureurl_qq_2"]];
                       [handler getRBUserInfoThirdAccountBindingWithProvider:provider andUid:[NSString stringWithFormat:@"%@",user.uid] andToken:[NSString stringWithFormat:@"%@",user.credential.token] andName:[NSString stringWithFormat:@"%@",user.nickname] andUrl:[NSString stringWithFormat:@"%@",user.url] andAvatar_url:[NSString stringWithFormat:@"%@",user.icon] andDesc:[NSString stringWithFormat:@"%@",user.aboutMe] andSerial_params:serial_params andFollowers_count:nil andStatuses_count:nil andRegistered_at:nil andVerified:nil andRefresh_token:nil andUnionid:nil andProvince:province andCity:city andGender:gender andIs_vip:vip andIs_yellow_vip:is_yellow_vip];
                   }
               }
               if (state == SSDKResponseStateFail) {
                   if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                       [self.hudView showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                   } else {
                       [self.hudView showErrorWithStatus:NSLocalizedString(@"R1042", @"登录失败")];
                   }
               }
               if (state == SSDKResponseStateCancel) {
                   [self.hudView dismiss];
                   //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
               }
           }];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"kols/identity_bind_v2"]) {
        [self.hudView dismiss];
        screenView.tLabel.text = [NSString stringWithFormat:@"%@%@ %@",NSLocalizedString(@"R7026",@"已绑定"),self.editString,userName];
    }
    
    if ([sender isEqualToString:@"update_social_v2"]) {
        [self.hudView dismiss];
        self.editButton.iconIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_ON.png",self.editString]];
        self.editButton.is_ON = @"YES";
        [self RBNavLeftBtnAction];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

