//
//  RBKolApplySocialInputViewController.h
//  RB
//
//  Created by AngusNi on 16/8/2.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolSocialButton.h"
@interface RBKolApplySocialInputViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
@property(nonatomic, strong) NSString*editString;
@property(nonatomic, strong) RBKolSocialButton*editButton;
@property(nonatomic, strong) RBKOLSocialEntity*socialEntity;

@end
