//
//  RBKolApplyInfoInputTextViewController.h
//  RB
//
//  Created by AngusNi on 16/8/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolEditView.h"

@interface RBKolApplyInfoInputTextViewController : RBBaseViewController
<RBBaseVCDelegate,TextViewKeyBoardDelegate,UIGestureRecognizerDelegate,UITextViewDelegate>
@property(nonatomic, assign) int maxCount;
@property(nonatomic, strong) NSString*editString;
@property(nonatomic, strong) NSString*editText;
@property(nonatomic, strong) RBKolEditView*editView;
@property(nonatomic, strong) PlaceholderTextView*editTV;
@end
