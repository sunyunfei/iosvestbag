//
//  RBKolApplyInfoInputTextViewController.m
//  RB
//
//  Created by AngusNi on 16/8/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolApplyInfoInputTextViewController.h"

@interface RBKolApplyInfoInputTextViewController () {
    UILabel*numberLabel;
}
@end

@implementation RBKolApplyInfoInputTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString];
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIView*editView = [[UIView alloc]initWithFrame:CGRectMake(0, NavHeight+CellBottom, ScreenWidth, 100.0+25.0)];
    editView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:editView];
    //
    self.editTV = [[PlaceholderTextView alloc]initWithFrame:CGRectMake(0, NavHeight+CellBottom, ScreenWidth, 100)];
    self.editTV.font = font_15;
    self.editTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    self.editTV.keyboardType = UIKeyboardTypeDefault;
    self.editTV.placeholderColor = [Utils getUIColorWithHexString:SysColorSubGray];
    self.editTV.placeholderFont = font_15;
    self.editTV.placeholder = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString];
    [self.view addSubview:self.editTV];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNotification:) name:UITextViewTextDidChangeNotification object:nil];
    TextViewKeyBoard*textViewKB = [[TextViewKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textViewKB.delegateP = self;
    [self.editTV setInputAccessoryView:textViewKB];
    //
    numberLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.editTV.bottom, ScreenWidth-CellLeft, 25.0)];
    numberLabel.font = font_13;
    numberLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    numberLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:numberLabel];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, numberLabel.bottom-0.5, ScreenWidth, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [self.view addSubview:lineLabel];
    //
    self.editTV.text = self.editText;
    if(self.editText!=nil&&self.editText.length!=0) {
        self.editTV.placeholder = @"";
    }
    numberLabel.text = [NSString stringWithFormat:@"%d/%d",(int)self.editTV.text.length,self.maxCount];
}

- (void)footerBtnAction:(UIButton *)sender {
    if (self.editTV.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),self.editString]];
        return;
    }
    if (self.editTV.text.length>self.maxCount) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R7014",@"输入的字符太长了")];
        return;
    }
    self.editView.cLabel.text = self.editTV.text;
    [self RBNavLeftBtnAction];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
    [self footerBtnAction:nil];
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextViewKeyBoardFinish:(TextViewKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - NSNotification Delegate
-(void)didChangeNotification:(NSNotification*)notification {
    numberLabel.text = [NSString stringWithFormat:@"%d/%d",(int)self.editTV.text.length,self.maxCount];
    if (self.editTV.text.length>self.maxCount) {
        numberLabel.textColor = [UIColor redColor];
    } else {
        numberLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    }
}

@end
