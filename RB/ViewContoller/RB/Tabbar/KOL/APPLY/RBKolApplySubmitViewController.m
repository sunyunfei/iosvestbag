//
//  RBKolApplySubmitViewController.m
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolApplySubmitViewController.h"
#import "RBAppDelegate.h"
#import "RBKolApplyInfoViewController.h"
@interface RBKolApplySubmitViewController () {
    UIView*headerView;
}
@end

@implementation RBKolApplySubmitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = @"";
    [self loadNavigationView];
    //
    [self loadHeaderView];
    [self loadContentView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageBegin:@"kol-apply-submit"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"kol-apply-submit"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton*)sender {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBKOLApplyWithShows:@""];
    
}

#pragma mark- loadHeaderView Delegate
- (void)loadHeaderView {
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, NavHeight, self.view.width, 0)];
    headerView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    //
    UILabel*headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40.0)];
    headerLabel.font = font_cu_13;
    headerLabel.text = NSLocalizedString(@"R7020",@"资料完善度越高，接受的活动价格越高");
    headerLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.8];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorSubBlack];
    [headerView addSubview:headerLabel];
    //
//    UIImageView*headerIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, headerLabel.bottom+24.0, ScreenWidth, 30.0)];
//    headerIV.image = [UIImage imageNamed:@"pic_kol_step_2.png"];
//    [headerView addSubview:headerIV];
    //
    UILabel*headerGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, headerLabel.bottom+24.0, ScreenWidth, CellBottom)];
    headerGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [headerView addSubview:headerGapLabel];
    //
    headerView.height = headerGapLabel.bottom;
    [self.view addSubview:headerView];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    footerView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:@"确定" forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
    //
    [self.view addSubview:footerView];
}

#pragma mark- loadContentView Delegate
- (void)loadContentView {
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake((ScreenWidth-80.0)/2.0, headerView.bottom+ (ScreenHeight-NavHeight-45.0-headerView.height-80.0-70.0)/2.0, 80.0, 80.0)];
    tLabel.font = font_icon_(80.0);
    tLabel.text = Icon_btn_select_h;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlue];
    [self.view addSubview:tLabel];
    //
    UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, tLabel.bottom+25.0, ScreenWidth, 20.0)];
    tLabel1.text = NSLocalizedString(@"R7032",@"提交成功，KOL资质审核中！");
    tLabel1.font = font_cu_17;
    tLabel1.textAlignment = NSTextAlignmentCenter;
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [self.view addSubview:tLabel1];
    //
    UILabel*tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(0, tLabel1.bottom+5.0, ScreenWidth, 20.0)];
    tLabel2.text = NSLocalizedString(@"R7033",@"Robin8会在1个工作日内审核您的KOL资质，请耐心等待");
    tLabel2.font = font_13;
    tLabel2.textAlignment = NSTextAlignmentCenter;
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [self.view addSubview:tLabel2];
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    if ([sender isEqualToString:@"submit_apply"]) {
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate getRongIMConnect];
        [appDelegate loadViewController:22];
    }
}
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}
- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
@end
