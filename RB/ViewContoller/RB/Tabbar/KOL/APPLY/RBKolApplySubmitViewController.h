//
//  RBKolApplySubmitViewController.h
//  RB
//
//  Created by AngusNi on 16/8/3.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBKolApplySubmitViewController : RBBaseViewController
<RBBaseVCDelegate>
@property(nonatomic,copy)NSString * comePath;
@end
