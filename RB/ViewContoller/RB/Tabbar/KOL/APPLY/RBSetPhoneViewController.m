//
//  RBSetPhoneViewController.m
//  RB
//
//  Created by RB8 on 2017/7/14.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBSetPhoneViewController.h"

@interface RBSetPhoneViewController ()
{
    InsetsTextField*phoneTF;
    InsetsTextField*codeTF;
    UIButton *codeBtn;
    NSTimer *codeTimer;
    int codeTimerStr;
    UIView*thirdView;
    UIView*editView;
}
@end

@implementation RBSetPhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
    self.navigationController.navigationBar.hidden = YES;
    self.navTitle = @"修改手机号";
    [self loadEditView];
}
-(void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)loadEditView {
    editView = [[UIView alloc]initWithFrame:CGRectMake(0, (ScreenHeight-50.0*3.0)/2.0,ScreenWidth, 0)];
    editView.userInteractionEnabled = YES;
    [self.view addSubview:editView];
    //
    phoneTF = [[InsetsTextField alloc]
               initWithFrame:CGRectMake(15, 0, ScreenWidth-30.0, 50.0)];
    phoneTF.font = font_(16.0);
    phoneTF.backgroundColor = [UIColor clearColor];
    phoneTF.keyboardType = UIKeyboardTypeNumberPad;
    phoneTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    phoneTF.attributedPlaceholder = [[NSAttributedString alloc]
                                     initWithString:NSLocalizedString(@"R1003", @"手机号")
                                     attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
    phoneTF.returnKeyType = UIReturnKeyDone;
    phoneTF.delegate = self;
    [editView addSubview:phoneTF];
    TextFiledKeyBoard *textFiledKB = [[TextFiledKeyBoard alloc]
                                      initWithFrame:CGRectMake(0, ScreenHeight - 216 - 44,
                                                               ScreenWidth, 216 + 44)];
    textFiledKB.backgroundColor = [UIColor clearColor];
    textFiledKB.delegateT = self;
    [phoneTF setInputAccessoryView:textFiledKB];
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(phoneTF.left,phoneTF.bottom-0.5f, phoneTF.width, 0.5f)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    [editView addSubview:lineLabel];
    //
    codeTF = [[InsetsTextField alloc]
              initWithFrame:CGRectMake(15, phoneTF.bottom+10.0,ScreenWidth-30.0-95.0, 50.0)];
    codeTF.font = font_(16.0);
    codeTF.delegate = self;
    codeTF.backgroundColor = [UIColor clearColor];
    codeTF.keyboardType = UIKeyboardTypeNumberPad;
    codeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    codeTF.attributedPlaceholder = [[NSAttributedString alloc]
                                    initWithString:NSLocalizedString(@"R1004", @"验证码")
                                    attributes:@{ NSForegroundColorAttributeName : [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55]}];
    codeTF.returnKeyType = UIReturnKeyDone;
    [editView addSubview:codeTF];
    [codeTF setInputAccessoryView:textFiledKB];
    UILabel *lineLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(phoneTF.left,codeTF.bottom-0.5f, phoneTF.width, 0.5f)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    [editView addSubview:lineLabel1];
    //
    UILabel *lineLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-15.0-100.0,lineLabel.bottom+20.0, 0.5f, 30)];
    lineLabel2.backgroundColor = [Utils getUIColorWithHexString:@"636363" andAlpha:0.35];
    [editView addSubview:lineLabel2];
    codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    [codeBtn setTitle:NSLocalizedString(@"R1005", @"获取验证码") forState:UIControlStateNormal];
    codeBtn.titleLabel.font = font_(14);
    codeBtn.frame = CGRectMake(ScreenWidth-15.0-100.0, codeTF.top, 100.0, codeTF.height);
    [codeBtn addTarget:self
                action:@selector(codeBtnAction:)
      forControlEvents:UIControlEventTouchUpInside];
    codeBtn.backgroundColor = [UIColor clearColor];
    [editView addSubview:codeBtn];
    //
    editView.height = codeBtn.bottom;
    //
//    UIButton *visitorBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    visitorBtn.frame = CGRectMake(0, ScreenHeight-220.0, ScreenWidth, 16);
//    visitorBtn.titleLabel.font = font_cu_13;
//    [visitorBtn setTitle:NSLocalizedString(@"R5172",@"游客 >") forState:UIControlStateNormal];
//    [visitorBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
//    [visitorBtn addTarget:self
//                   action:@selector(visitorBtnAction:)
//         forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:visitorBtn];
    //
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setTitleColor:[Utils getUIColorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [loginBtn setTitle:NSLocalizedString(@"R1101", @"完成") forState:UIControlStateNormal];
    loginBtn.titleLabel.font=font_cu_cu_15;
    loginBtn.frame = CGRectMake(15.0, ScreenHeight-220, ScreenWidth-30.0, 44.0);
    [loginBtn addTarget:self
                 action:@selector(loginBtnAction:)
       forControlEvents:UIControlEventTouchUpInside];
    loginBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    [self.view addSubview:loginBtn];
}
-(void)loginBtnAction:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - UIButton Delegate
- (void)codeBtnAction:(UIButton *)sender {
    if (phoneTF.text.length==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1006", @"请输入手机号码")];
        return;
    }
    if (phoneTF.text.length!=11) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R1007", @"手机号码格式错误")];
        return;
    }
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"R1009", @"收不到验证码?")]){
        RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
        actionSheet.delegate = self;
        actionSheet.tag = 1000;
        [actionSheet showWithArray:@[NSLocalizedString(@"R1010", @"获取语音验证码"),NSLocalizedString(@"R1011", @"取消")]];
        return;
    }
    // RB-获取短信验证码
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserLoginPhoneCodeWith:phoneTF.text];
    //
    [self codeBtnTime];
}
-(void)codeBtnTime {
    codeTimerStr = 59;
    [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55] forState:UIControlStateNormal];
    [codeBtn setTitle:[NSString stringWithFormat:@"%@%ds",NSLocalizedString(@"R1012", @"重新获取"),codeTimerStr] forState:UIControlStateNormal];
    codeBtn.enabled = NO;
    codeTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                               target  :self
                                               selector:@selector(timeLimit)
                                               userInfo:nil
                                               repeats :YES];
}
- (void)timeLimit {
    if (codeTimerStr == 0) {
        [codeTimer invalidate];
        codeTimer = nil;
        [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        [codeBtn setTitle:NSLocalizedString(@"R1009", @"收不到验证码?") forState:UIControlStateNormal];
        codeBtn.enabled = YES;
    } else {
        codeTimerStr = codeTimerStr - 1;
        [codeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack andAlpha:0.55] forState:UIControlStateNormal];
        [codeBtn setTitle:[NSString stringWithFormat:@"%@%ds",NSLocalizedString(@"R1012", @"重新获取"),codeTimerStr] forState:UIControlStateNormal];
        codeBtn.enabled = NO;
    }
}
#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEdit];
    return YES;
}
#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    float keyboardTop = ScreenHeight-320;
    if (editView.bottom > keyboardTop) {
        [UIView animateWithDuration:0.2 animations:^{
            editView.top = keyboardTop-editView.height;
        } completion:^(BOOL finished) {
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.2 animations:^{
        editView.top = (ScreenHeight-50.0*3.0)/2.0;
    } completion:^(BOOL finished) {
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
