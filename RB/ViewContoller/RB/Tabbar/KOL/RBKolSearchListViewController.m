//
//  RBKolSearchListViewController.m
//  RB
//
//  Created by AngusNi on 16/7/27.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolSearchListViewController.h"

@interface RBKolSearchListViewController ()

@end

@implementation RBKolSearchListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.edgePGR.enabled = NO;
    self.navTitle = self.categoryLabel;
    //
    self.kolListView = [[RBKolListView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    self.kolListView.delegate = self;
    [self.view addSubview:self.kolListView];
    //
    [self.hudView show];
    [self.kolListView RBKolListViewLoadRefreshViewFirstData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageBegin:@"kol-list-category"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.hudView dismiss];
    [TalkingData trackPageEnd:@"kol-list-category"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationStatusBarAction {
    [self.kolListView.collectionviewn scrollRectToVisible:self.kolListView.collectionviewn.bounds animated:YES];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - RBkolView Delegate
-(void)RBKolListViewLoadRefreshViewData:(RBKolListView *)view {
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBKOLListWithPage:view.pageIndex+1 andBanner:nil andCategory:self.categoryName andSearch:nil andOrder:@"order_by_hot"];
}

- (void)RBKolListViewSelected:(RBKolListView *)view andIndex:(int)index {
    RBKOLEntity *entity = view.datalist[index];
    RBKolDetailViewController *toview = [[RBKolDetailViewController alloc] init];
    toview.kolId = entity.iid;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)RBKolListViewMore:(RBKolListView *)view {
    RBEngineViewController *toview = [[RBEngineViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"big_v/order_by_hot"]) {
        [self.hudView dismiss];
        if (self.kolListView.pageIndex == 0) {
            self.kolListView.datalist = [NSMutableArray new];
        }
        self.kolListView.datalist = [JsonService getRBKolListEntity:jsonObject andBackArray:self.kolListView.datalist];
        [self.kolListView RBKolListViewSetRefreshViewFinish];
        //
        if ([self.kolListView.datalist count] == 0) {
            self.kolListView.defaultView.hidden = NO;
        } else {
            self.kolListView.defaultView.hidden = YES;
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self.kolListView RBKolListViewSetRefreshViewFinish];
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.kolListView RBKolListViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end


