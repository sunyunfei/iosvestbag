//
//  RBKolDetailViewController.m
//  RB
//
//  Created by AngusNi on 7/11/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBKolDetailViewController.h"
#import "RBInfluenceDetailViewController.h"
#import "RBLoginViewController.h"
@interface RBKolDetailViewController () {
    UIScrollView*scrollviewn;
    UIImageView*bgIV;
    UIImageView*userIV;
    UILabel*nameLabel;
    UILabel*jobLabel;
    UILabel*categoryLabel;
    RBKolSocialTableView*kolTableView;
    UIView*desView;
    UIView*showView;
    UIView*keywordView;
    UIView*footerView;
    RBKOLEntity*kolEntity;
    NSString*is_follow;
    UIButton * InfluenceBtn;
}
@end

@implementation RBKolDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    //
    [self loadKOLView];
    [self loadFooterView];
    //
    self.navTitle = NSLocalizedString(@"R7001",@"KOL详情");
    self.navRightTitle = Icon_btn_care_l;
    self.navLeftLabel.textColor = [UIColor whiteColor];
    self.navTitleLabel.textColor = [UIColor whiteColor];
    self.navRightLabel.textColor = [UIColor whiteColor];
    self.navView.backgroundColor = [UIColor clearColor];
    self.navLineLabel.backgroundColor = [UIColor clearColor];
    //
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBKOLDetailWithKolId:self.kolId];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:@"kol-detail"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //
    [TalkingData trackPageEnd:@"kol-detail"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)RBNavRightBtnAction {
    if([self isVisitorLogin]==YES) {
        return;
    };
    //
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBKOLCareWithKolId:self.kolId];
    //
    if([self.navRightTitle isEqualToString:Icon_btn_care_l]) {
        self.navRightLabel.textColor = [Utils getUIColorWithHexString:SysColorRed];
        self.navRightTitle = Icon_btn_care_h;
    } else {
        self.navRightLabel.textColor = [UIColor whiteColor];
        self.navRightTitle = Icon_btn_care_l;
    }
}

#pragma mark - LoadContentView Delegate
- (void)loadKOLView {
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-45.0)];
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.delegate = self;
    [self.view addSubview:scrollviewn];
    //
    bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth*730.0/750.0)];
    bgIV.contentMode = UIViewContentModeScaleAspectFill;
    bgIV.clipsToBounds = YES;
    [scrollviewn addSubview:bgIV];
    //
    userIV = [[UIImageView alloc]initWithFrame:CGRectMake((bgIV.width-60.0)/2.0, (bgIV.height-60.0)/2.0, 60, 60)];
    userIV.contentMode = UIViewContentModeScaleAspectFill;
    userIV.clipsToBounds = YES;
    [bgIV addSubview:userIV];
    //
    UIImageView*bgCoverIV = [[UIImageView alloc]initWithFrame:bgIV.bounds];
    bgCoverIV.backgroundColor = [Utils getUIColorWithHexString:@"000000" andAlpha:0.3];
    [bgIV addSubview:bgCoverIV];
    //
    nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, bgIV.height-100.0, ScreenWidth, 18.0)];
    nameLabel.font = font_cu_(24);
    nameLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    [bgIV addSubview:nameLabel];
    //
    jobLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, bgIV.height-100.0, ScreenWidth, 18.0)];
    jobLabel.font = font_cu_13;
    jobLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    [bgIV addSubview:jobLabel];
    //
    UILabel*tipLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, nameLabel.bottom+16.0, 40.0, 3.0)];
    tipLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [bgIV addSubview:tipLabel];
    //
    categoryLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, tipLabel.bottom+16.0, ScreenWidth-2*CellLeft, 16.0)];
    categoryLabel.font = font_cu_13;
    categoryLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    [bgIV addSubview:categoryLabel];
    
    //
    
    InfluenceBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, bgIV.bottom, ScreenWidth, 67) title:@"查看Ta的影响力" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorBlack] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    InfluenceBtn.titleLabel.font = font_cu_15;
    [InfluenceBtn addTarget:self action:@selector(LookInfluence:) forControlEvents:UIControlEventTouchUpInside];
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(CellLeft, 15, 37, 37)];
    imageView.image = [UIImage imageNamed:@"RBInfluenceLook"];
    [InfluenceBtn addSubview:imageView];
    UIImageView * pinImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth-18-11, 28, 6, 11)];
    pinImageView.image = [UIImage imageNamed:@"RBKOLPin"];
    [InfluenceBtn addSubview:pinImageView];
    [scrollviewn addSubview:InfluenceBtn];
    if ([self.kolId isEqualToString:[LocalService getRBLocalDataUserLoginId]]) {
        InfluenceBtn.height = 0;
        InfluenceBtn.hidden = YES;
    }
    //
}
-(void)LookInfluence:(id)sender{
    if([LocalService getRBLocalDataUserPrivateToken] == nil && [JsonService isRBUserVisitor] == NO){
        RBLoginViewController*toview = [[RBLoginViewController alloc]init];
        //        toview.hidesBottomBarWhenPushed = YES;
        //        [self.navigationController pushViewController:toview animated:YES];
        toview.isPush = NO;
        [self presentVC:toview];
        return;
    }
    if ([self isVisitorLogin] == YES) {
        return;
    }
    RBInfluenceDetailViewController * detailInfluence = [[RBInfluenceDetailViewController alloc]init];
    detailInfluence.kolId = self.kolId;
    [self.navigationController pushViewController:detailInfluence animated:YES];
}
#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(scrollviewn.contentOffset.y<60) {
        self.navView.backgroundColor = [UIColor clearColor];
    } else {
        self.navView.backgroundColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.3];
    }
}

#pragma mark - LoadSocialView Delegate
- (void)loadSocialView {
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, InfluenceBtn.bottom, ScreenWidth, 0.5)];
    lineView.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
    lineView.alpha = 0.4;
    [scrollviewn addSubview:lineView];
    kolTableView = [[RBKolSocialTableView alloc]initWithFrame:CGRectMake(0, lineView.bottom, ScreenWidth, 75.0*[kolEntity.social_accounts count])];
    kolTableView.delegate = self;
    [scrollviewn addSubview:kolTableView];
    [kolTableView RBKolSocialTableViewLoadRefreshViewFirstData];
}

#pragma mark - RBKolSocialTableView Delegate
- (void)RBKolSocialTableViewLoadRefreshViewData:(RBKolSocialTableView *)view {
    view.datalist = kolEntity.social_accounts;
    [view RBKolSocialTableViewSetRefreshViewFinish];
}

- (void)RBKolSocialTableViewSelected:(RBKolSocialTableView *)view andIndex:(int)index {
    RBKOLSocialEntity*entity = view.datalist[index];
    if (entity.uid.length!=0||[entity.provider isEqualToString:@"zhihu"]||[entity.provider isEqualToString:@"meipai"]||[entity.provider isEqualToString:@"miaopai"]) {
        RBEngineDetailViewController*toview = [[RBEngineDetailViewController alloc]init];
        toview.socialEntity = entity;
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
        return;
    }
    if(entity.homepage.length==0) {
        return;
    }
    TOWebViewController*toview = [[TOWebViewController alloc] init];
    toview.url = [NSURL URLWithString:entity.homepage];
    toview.title = entity.username;
    toview.showPageTitles = NO;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - LoadDesView Delegate
- (void)loadDesView {
    desView = [[UIView alloc]initWithFrame:CGRectMake(0, kolTableView.bottom, ScreenWidth, 75.0*3.0)];
    [scrollviewn addSubview:desView];
    //
    UIView*gapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, CellBottom)];
    gapView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [desView addSubview:gapView];
    //
    UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, gapView.bottom+18.0, ScreenWidth, 18.0)];
    tLabel.font = font_cu_cu_15;
    tLabel.textAlignment = NSTextAlignmentCenter;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [desView addSubview:tLabel];
    //
    UILabel*tipLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth-20.0)/2.0, tLabel.bottom+10.0, 20.0, 3.0)];
    tipLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [desView addSubview:tipLabel];
    //
    UILabel*cLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, tipLabel.bottom+10.0, ScreenWidth-2*CellLeft, 50.0)];
    cLabel.numberOfLines = 0;
    cLabel.font = font_15;
    cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [desView addSubview:cLabel];
    //
    tLabel.text = NSLocalizedString(@"R7002",@"KOL简介");
    cLabel.text = kolEntity.desc;
    if(cLabel.text.length==0){
        cLabel.text = NSLocalizedString(@"R7051",@"未填写个人简介");
    }
    [Utils getUILabel:cLabel withLineSpacing:5.0];
    CGSize cLabelSize = [Utils getUIFontSizeFitH:cLabel withLineSpacing:5.0];
    cLabel.height = cLabelSize.height;
    desView.height = cLabel.bottom+20.0;
}

#pragma mark - LoadDesView Delegate
- (void)loadShowView {
    showView = [[UIView alloc]initWithFrame:CGRectMake(0, desView.bottom, ScreenWidth, 75.0*3.0)];
    [scrollviewn addSubview:showView];
    //
    UIView*gapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, CellBottom)];
    gapView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [showView addSubview:gapView];
    //
    UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, gapView.bottom+18.0, ScreenWidth, 18.0)];
    tLabel.font = font_cu_cu_15;
    tLabel.textAlignment = NSTextAlignmentCenter;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [showView addSubview:tLabel];
    //
    UILabel*tipLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth-20.0)/2.0, tLabel.bottom+10.0, 20.0, 3.0)];
    tipLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [showView addSubview:tipLabel];
    //
    iCarousel*carousel = [[iCarousel alloc]initWithFrame:CGRectMake(0, tipLabel.bottom+10.0, ScreenWidth, 210.0)];
    carousel.delegate = self;
    carousel.dataSource = self;
    carousel.type = iCarouselTypeRotary;
    [showView addSubview:carousel];
    //
    showView.height = carousel.bottom+20.0;
    //
    [carousel reloadData];
    tLabel.text = NSLocalizedString(@"R7003",@"KOL展示");
}

#pragma mark - iCarousel Delegate
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return [kolEntity.kol_shows count]*2.0;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view {
    if (view == nil) {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
        ((UIImageView *)view).contentMode = UIViewContentModeScaleAspectFill;
        ((UIImageView *)view).clipsToBounds = YES;
        UILabel*btnLabel = [[UILabel alloc] initWithFrame:CGRectMake((view.width-50.0)/2.0, (view.height-50.0)/2.0, 50.0, 50.0)];
        btnLabel.font = font_icon_(50.0);
        btnLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        btnLabel.tag = 1000;
        btnLabel.text = Icon_btn_play;
        [view addSubview:btnLabel];
    }
    if(index>= [kolEntity.kol_shows count]) {
        index = index-[kolEntity.kol_shows count];
    }
    RBKOLShowEntity*entity = kolEntity.kol_shows[index];
    if(entity.cover_url.length==0){
        entity.cover_url = kolEntity.avatar_url;
    }
//    [((UIImageView *)view) yy_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",entity.cover_url]] placeholder:PlaceHolderImage options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation completion:NULL];
    [((UIImageView *)view) setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",entity.cover_url]] placeholder:PlaceHolderImage options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation completion:NULL];
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    if(index>= [kolEntity.kol_shows count]) {
        index = index-[kolEntity.kol_shows count];
    }
    RBKOLShowEntity*entity = kolEntity.kol_shows[index];
    entity.link = [entity.link stringByReplacingOccurrencesOfString:@"http://weibo.com" withString:@"http://m.weibo.com"];
    TOWebViewController*toview = [[TOWebViewController alloc] init];
    toview.url = [NSURL URLWithString:entity.link];
    toview.title = entity.desc;
    toview.showPageTitles = NO;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - LoadDesView Delegate
- (void)loadKeywordView {
    keywordView = [[UIView alloc]initWithFrame:CGRectMake(0, showView.bottom, ScreenWidth, 75.0*3.0)];
    [scrollviewn addSubview:keywordView];
    //
    UIView*gapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, CellBottom)];
    gapView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [keywordView addSubview:gapView];
    //
    UILabel*tLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, gapView.bottom+18.0, ScreenWidth, 18.0)];
    tLabel.font = font_cu_cu_15;
    tLabel.textAlignment = NSTextAlignmentCenter;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [keywordView addSubview:tLabel];
    //
    UILabel*tipLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth-20.0)/2.0, tLabel.bottom+10.0, 20.0, 3.0)];
    tipLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [keywordView addSubview:tipLabel];
    //
    tLabel.text = NSLocalizedString(@"R7004",@"KOL关键字");
    //
    DBSphereView *sphereView = [[DBSphereView alloc] initWithFrame:CGRectMake(40.0, tipLabel.bottom+10.0, scrollviewn.width-80.0, scrollviewn.width-80.0)];
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    for (NSInteger i = 0; i < [kolEntity.kol_keywords count]; i ++) {
        RBKOLKeywordsEntity*entity = kolEntity.kol_keywords[i];
        UILabel*tagsLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 22)];
        tagsLabel.text = entity.name;
        tagsLabel.font = font_30;
        tagsLabel.textAlignment = NSTextAlignmentCenter;
        tagsLabel.textColor = [Utils getUIColorRandom];
        [array addObject:tagsLabel];
        CGSize tagsLabelSize = [Utils getUIFontSizeFitW:tagsLabel];
        tagsLabel.width = tagsLabelSize.width;
        [sphereView addSubview:tagsLabel];
    }
    if ([kolEntity.kol_keywords count]<10) {
        for (NSInteger i = 0; i < [kolEntity.kol_keywords count]; i ++) {
            RBKOLKeywordsEntity*entity = kolEntity.kol_keywords[i];
            UILabel*tagsLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 22)];
            tagsLabel.text = entity.name;
            tagsLabel.font = font_30;
            tagsLabel.textAlignment = NSTextAlignmentCenter;
            tagsLabel.textColor = [Utils getUIColorRandom];
            [array addObject:tagsLabel];
            CGSize tagsLabelSize = [Utils getUIFontSizeFitW:tagsLabel];
            tagsLabel.width = tagsLabelSize.width;
            [sphereView addSubview:tagsLabel];
        }
    }
    [sphereView setCloudTags:array];
    [keywordView addSubview:sphereView];
    //
    keywordView.height = sphereView.bottom+20.0;
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, keywordView.bottom+CellBottom)];
}

#pragma mark- LoadFooterView Delegate
- (void)loadFooterView {
    footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, 45.0);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerBtn setTitle:NSLocalizedString(@"R7005",@"给TA发特邀活动") forState:UIControlStateNormal];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    [footerView addSubview:footerBtn];
}

#pragma mark - UIButton Delegate
- (void)footerBtnAction:(UIButton *)sender {
    if([self isVisitorLogin]==YES){
        return;
    };
    //
    RBUserContactKolViewController*toview = [[RBUserContactKolViewController alloc] init];
    toview.kolName = kolEntity.name;
    toview.kolId = kolEntity.iid;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"big_v/detail"]) {
        [self.hudView dismiss];
        kolEntity = [JsonService getRBKolDetailEntity:jsonObject];
        //
        is_follow = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"is_follow"]];
        if(is_follow.intValue==1) {
            self.navRightLabel.textColor = [Utils getUIColorWithHexString:SysColorRed];
            self.navRightTitle = Icon_btn_care_h;
        } else {
            self.navRightLabel.textColor = [UIColor whiteColor];
            self.navRightTitle = Icon_btn_care_l;
        }
        //
        if (kolEntity.avatar_url.length==0) {
            userIV.image = nil;
            bgIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"pic_kol_avatar_%d.jpg",arc4random()%4]];
        } else {
            userIV.image = nil;
//            [bgIV yy_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",kolEntity.avatar_url]] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
            [bgIV setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",kolEntity.avatar_url]] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
        }
        nameLabel.text = kolEntity.name;
        CGSize nameLabelSize = [Utils getUIFontSizeFitW:nameLabel];
        nameLabel.width = nameLabelSize.width;
        NSString*socialStr = @"";
        for (int i=0; i<[kolEntity.social_accounts count]; i++) {
            RBKOLSocialEntity*social = kolEntity.social_accounts[i];
            if (i==0) {
                socialStr = social.provider_name;
            } else {
                socialStr = [NSString stringWithFormat:@"%@/%@",socialStr,social.provider_name];
            }
        }
        jobLabel.text = [NSString stringWithFormat:@" · %@",socialStr];
        jobLabel.left = nameLabel.right;
        jobLabel.width = ScreenWidth - jobLabel.left;
        for (int i=0; i< [kolEntity.tags count];i++) {
            RBTagEntity*entity = kolEntity.tags[i];
            if(i==0) {
                categoryLabel.text = [NSString stringWithFormat:@"%@",entity.label];
            } else {
                categoryLabel.text = [NSString stringWithFormat:@"%@/%@",categoryLabel.text,entity.label];
            }
        }
        [self loadSocialView];
        if ([kolEntity.social_accounts count]==0) {
            kolTableView.height = 0;
        }
        [self loadDesView];
        [self loadShowView];
        if ([kolEntity.kol_shows count]==0) {
            showView.height = 56.5;
        }
        if ([kolEntity.kol_keywords count]==0) {
            RBKOLKeywordsEntity*entity = [[RBKOLKeywordsEntity alloc]init];
            entity.name = @"Robin8";
            RBKOLKeywordsEntity*entity1 = entity;
            RBKOLKeywordsEntity*entity2 = entity;
            RBKOLKeywordsEntity*entity3 = entity;
            [kolEntity.kol_keywords addObject:entity];
            [kolEntity.kol_keywords addObject:entity1];
            [kolEntity.kol_keywords addObject:entity2];
            [kolEntity.kol_keywords addObject:entity3];
        }
        [self loadKeywordView];
        if ([kolEntity.kol_keywords count]==0) {
            keywordView.height = 56.5;
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
