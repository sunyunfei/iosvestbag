//
//  RBKolDetailViewController.h
//  RB
//
//  Created by AngusNi on 7/11/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBKolSocialTableView.h"
#import "RBUserContactKolViewController.h"
#import "TOWebViewController.h"
#import "RBEngineDetailViewController.h"
@interface RBKolDetailViewController : RBBaseViewController
<RBBaseVCDelegate,RBKolSocialTableViewDelegate,UIScrollViewDelegate,iCarouselDelegate,iCarouselDataSource>
@property(nonatomic, strong) NSString *kolId;

@end
