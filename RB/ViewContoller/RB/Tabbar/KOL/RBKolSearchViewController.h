//
//  RBKolSearchViewController.h
//  RB
//
//  Created by AngusNi on 16/7/27.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBArticleSearchView.h"
#import "RBArticleSearchHistoryView.h"
#import "RBKolListView.h"
#import "RBKolDetailViewController.h"
#import "RBEngineViewController.h"
@interface RBKolSearchViewController : RBBaseViewController
<RBArticleSearchViewDelegate,RBArticleSearchHistoryViewDelegate,RBBaseVCDelegate,RBKolListViewDelegate>
@end
