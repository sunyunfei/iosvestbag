//
//  RBKolSocialTableView.h
//  RB
//
//  Created by AngusNi on 16/7/26.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBKolSocialTableViewCell.h"

@class RBKolSocialTableView;
@protocol RBKolSocialTableViewDelegate <NSObject>
@optional
- (void)RBKolSocialTableViewLoadRefreshViewData:(RBKolSocialTableView *)view;
- (void)RBKolSocialTableViewSelected:(RBKolSocialTableView *)view andIndex:(int)index;

@end


@interface RBKolSocialTableView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBKolSocialTableViewDelegate> delegate;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
- (void)RBKolSocialTableViewLoadRefreshViewFirstData;
- (void)RBKolSocialTableViewSetRefreshViewFinish;

@end
