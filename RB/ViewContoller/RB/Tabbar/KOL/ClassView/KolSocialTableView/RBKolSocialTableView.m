//
//  RBKolSocialTableView.m
//  RB
//
//  Created by AngusNi on 16/7/26.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolSocialTableView.h"

@interface RBKolSocialTableView () {
}
@end

@implementation RBKolSocialTableView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        //
        self.tableviewn = [[UITableView alloc]
                           initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)
                           style:UITableViewStylePlain];
        self.tableviewn.backgroundView = nil;
        self.tableviewn.backgroundColor = [UIColor clearColor];
        self.tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableviewn.delegate = self;
        self.tableviewn.dataSource = self;
        [self addSubview:self.tableviewn];
        [self setRefreshHeaderView];
    }
    return self;
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)RBKolSocialTableViewLoadRefreshViewFirstData {
    self.pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    self.pageSize = 10;
    if ([self.delegate
         respondsToSelector:@selector(RBKolSocialTableViewLoadRefreshViewData:)]) {
        [self.delegate RBKolSocialTableViewLoadRefreshViewData:self];
    }
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    self.tableviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    self.tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.pageIndex = self.pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)RBKolSocialTableViewSetRefreshViewFinish {
    [self.tableviewn.mj_header endRefreshing];
    [self.tableviewn.mj_footer endRefreshing];
    if (self.tableviewn.mj_footer == nil) {
        [self setRefreshFooterView];
    }
    [UIView performWithoutAnimation:^{
        [self.tableviewn reloadData];
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self.datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"Cell%d%d",(int)[indexPath section],(int)[indexPath row]];
    //    static NSString *cellIdentifier = @"CellIdentifier";
    RBKolSocialTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBKolSocialTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:cellIdentifier];
    }
    [cell loadRBKolSocialCellWith:self.datalist andIndex:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate
         respondsToSelector:@selector(RBKolSocialTableViewSelected:andIndex:)]) {
        [self.delegate RBKolSocialTableViewSelected:self andIndex:(int)indexPath.row];
    }
}

@end
