//
//  RBKolClassView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface RBKolClassView : UIView
- (void)loadKolClassView:(NSArray*)array;
@end
