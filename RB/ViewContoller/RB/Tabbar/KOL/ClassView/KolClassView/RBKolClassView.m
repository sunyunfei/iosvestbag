//
//  RBKolClassView.m
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBKolClassView.h"
#import "Service.h"

@interface RBKolClassView () {
}
@end

@implementation RBKolClassView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)loadKolClassView:(NSArray*)array {
    [self removeAllSubviews];
    float w=0;
    int tempCount = (int)[array count];
    if(ScreenWidth>320){
        if(tempCount>4) {
            tempCount = 4;
        }
    } else {
        if(tempCount>3) {
            tempCount = 3;
        }
    }
    for(int i=0; i<tempCount; i++) {
        UILabel*classLabel = [[UILabel alloc] initWithFrame:CGRectMake(w, 0, 0, 13.0)];
        classLabel.font = font_cu_(10);
        classLabel.text = array[i];
        classLabel.textAlignment = NSTextAlignmentCenter;
        classLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
        [self addSubview:classLabel];
        //
        CGSize classLabelSize = [Utils getUIFontSizeFitW:classLabel];
        classLabel.width = classLabelSize.width+10.0;
        w = classLabel.right+10.0;
        classLabel.layer.cornerRadius = classLabel.height/2.0;
        classLabel.layer.borderColor = [Utils getUIColorWithHexString:SysColorYellow].CGColor;
        classLabel.layer.borderWidth = 0.5;
        classLabel.layer.masksToBounds = YES;
    }
}

@end
