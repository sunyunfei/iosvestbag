//
//  RBKolEditView.h
//  RB
//
//  Created by AngusNi on 16/8/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBKolEditView : UIButton
@property(nonatomic, strong) UILabel*tLabel;
@property(nonatomic, strong) UILabel*cLabel;
@property(nonatomic, strong) NSString*shows;
@property(nonatomic, strong) NSString*interest;
@property(nonatomic,strong) UILabel * arrowLabel;
@end
