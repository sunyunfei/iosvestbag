//
//  RBKolSocialButton.h
//  RB
//
//  Created by AngusNi on 16/8/2.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^Shake_ViewDeleteShakeBlock)();
@interface RBKolSocialButton : UIButton
@property(nonatomic, strong) UIImageView * iconIV;
@property(nonatomic, strong) UILabel*tLabel;
@property(nonatomic, strong) NSString*is_ON;
@property(nonatomic,strong) UIButton * deleteButton;
@property(nonatomic,copy) Shake_ViewDeleteShakeBlock deleteShakeBlock;
@end
