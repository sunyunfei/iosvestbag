//
//  RBKolListView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBKolCollectionCell.h"
#import "RBKolHeadCollectionCell.h"

@class RBKolListView;
@protocol RBKolListViewDelegate <NSObject>
@optional
- (void)RBKolListViewLoadRefreshViewData:(RBKolListView *)view;
- (void)RBKolListViewSelected:(RBKolListView *)view andIndex:(int)index;
- (void)RBKolListViewMore:(RBKolListView *)view;

@end


@interface RBKolListView : UIView
<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout,RBKolHeadCollectionCellDelegate>

@property(nonatomic, weak) id<RBKolListViewDelegate> delegate;
@property(nonatomic, strong) UIView*defaultView;
@property(nonatomic, strong) UICollectionView *collectionviewn;

@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;

- (void)RBKolListViewLoadRefreshViewFirstData;
- (void)RBKolListViewSetRefreshViewFinish;

@end
