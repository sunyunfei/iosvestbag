//
//  RBKolView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBKolCollectionCell.h"
#import "RBKolHeadCollectionCell.h"

@class RBKolView;
@protocol RBKolViewDelegate <NSObject>
@optional
- (void)RBKolViewLoadRefreshViewData:(RBKolView *)view;
- (void)RBKolViewSelected:(RBKolView *)view andIndex:(int)index;
- (void)RBKolViewKolSelected:(RBKolView *)view andIndex:(int)index;
- (void)RBKolViewHeadSelected:(RBKolView *)view andIndex:(int)index;
@end


@interface RBKolView : UIView
<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout,RBKolHeadCollectionCellDelegate>

@property(nonatomic, weak) id<RBKolViewDelegate> delegate;
@property(nonatomic, strong) UIView*defaultView;
@property(nonatomic, strong) UICollectionView *collectionviewn;
@property(nonatomic, strong) NSArray*nameArray;
@property(nonatomic, strong) NSArray*labelArray;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) NSMutableArray *newlist;
@property(nonatomic, strong) NSMutableArray *headlist;
@property(nonatomic, strong) id jsonObject;
@property(nonatomic, strong) id jsonObjectNew;

@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;

- (void)RBKolViewLoadRefreshViewFirstData;
- (void)RBKolViewSetRefreshViewFinish;

@end
