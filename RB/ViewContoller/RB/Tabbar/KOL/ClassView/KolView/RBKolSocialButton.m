//
//  RBKolSocialButton.m
//  RB
//
//  Created by AngusNi on 16/8/2.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolSocialButton.h"
#import "Service.h"

@implementation RBKolSocialButton

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = YES;
        
        //
        self.iconIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 45.0, 45.0)];
        [self addSubview:self.iconIV];
        //
        self.deleteButton = [[UIButton alloc]initWithFrame:CGRectMake(-5,-5, 20, 20)];
        //self.deleteButton.center = self.iconIV.origin;
        self.deleteButton.layer.cornerRadius = 20/2;
//        self.deleteButton.backgroundColor = [UIColor blackColor];
//        [self.deleteButton setTitle:@"❌" forState:0];
//        self.deleteButton.titleLabel.font = [UIFont systemFontOfSize:10];
//        [self.deleteButton setTitleColor:[UIColor whiteColor] forState:0];
        [self.deleteButton setImage:[UIImage imageNamed:@"socialDelete"] forState:UIControlStateNormal];
        [self.deleteButton addTarget:self action:@selector(clickDeleteButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.deleteButton];
        self.deleteButton.hidden = YES;

        
        self.tLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.iconIV.bottom, 45.0, 15.0)];
        self.tLabel.font = font_11;
        self.tLabel.textAlignment = NSTextAlignmentCenter;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self addSubview:self.tLabel];
    }
    return self;
}
-(void)clickDeleteButton:(id)sender{
    if (_deleteShakeBlock) {
        _deleteShakeBlock();
    }
}

@end
