//
//  RBKolView.m
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBKolView.h"
@interface RBKolView () {
    RBKolHeadCollectionCell*collectionviewnHeadView;
}
@end

@implementation RBKolView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        //
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        //cell之间的水平间距
        layout.minimumColumnSpacing = 2.0;
        //cell之间的垂直间距
        layout.minimumInteritemSpacing = layout.minimumColumnSpacing;
        //headerView高度
        layout.headerHeight = (ScreenWidth/750.0)*360.0+1.4*ScreenWidth/5.0+7.5+45.0*2.0+10.0;
        //footerView高度
        layout.footerHeight = 0;
        //2列显示
        layout.columnCount = 2;
        self.collectionviewn = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:layout];
        self.collectionviewn.dataSource = self;
        self.collectionviewn.delegate = self;
        self.collectionviewn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:1];
        [self.collectionviewn registerClass:[RBKolCollectionCell class]
            forCellWithReuseIdentifier:@"RBKolCollectionCell"];
        [self.collectionviewn registerClass:[RBKolHeadCollectionCell class]
            forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader
                   withReuseIdentifier:@"RBKolHeadCollectionCell"];
        [self addSubview:self.collectionviewn];
        [self setRefreshHeaderView];
        //
        self.defaultView = [[UIView alloc]initWithFrame:CGRectMake(0, (self.height-120.0)/2.0, ScreenWidth, 120.0)];
        self.defaultView.tag = 9999;
        self.defaultView.hidden = YES;
        [self addSubview:self.defaultView];
        //
        UIImageView*defaultIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-100.0)/2.0, 0, 100.0, 100.0)];
        defaultIV.image = [UIImage imageNamed:@"icon_task_default.png"];
        [self.defaultView addSubview:defaultIV];
        //
        UILabel*defaultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, defaultIV.bottom, ScreenWidth, 20.0)];
        defaultLabel.font = font_cu_17;
        defaultLabel.textAlignment = NSTextAlignmentCenter;
        defaultLabel.text = NSLocalizedString(@"R1028", @"当前页面无数据,试试别的页面吧");
        defaultLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [self.defaultView addSubview:defaultLabel];
        //
        self.nameArray = @[@"internet",@"beauty",@"babies",@"entertainment",@"travel",@"education",@"fashion",@"games",@"realestate",@"finance",@"digital",@"appliances",@"health",@"books",@"sports",@"airline",@"furniture",@"auto",@"hotel",@"ce",@"camera",@"mobile",@"food",@"fitness",@"music",@"overall"];
        self.labelArray = @[@"互联网",@"美妆",@"母婴",@"娱乐",@"旅游",@"教育",@"时尚",@"游戏",@"房地产",@"财经",@"数码",@"家电",@"健康",@"图书",@"体育",@"航空",@"家居",@"汽车",@"酒店",@"消费电子",@"摄影",@"手机",@"美食",@"健身",@"音乐",@"综合"];
    }
    return self;
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)RBKolViewLoadRefreshViewFirstData {
    self.pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    self.pageSize = 20;
    if ([self.delegate
         respondsToSelector:@selector(RBKolViewLoadRefreshViewData:)]) {
        [self.delegate RBKolViewLoadRefreshViewData:self];
    }
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    self.collectionviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    self.collectionviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.pageIndex = self.pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)RBKolViewSetRefreshViewFinish {
    [self.collectionviewn.mj_header endRefreshing];
    [self.collectionviewn.mj_footer endRefreshing];
    if ([self.datalist count] == (self.pageIndex+1)*self.pageSize) {
        if (self.collectionviewn.mj_footer == nil) {
            [self setRefreshFooterView];
        }
    } else {
        [self.collectionviewn.mj_footer removeFromSuperview];
        self.collectionviewn.mj_footer = nil;
    }
    if ([self.datalist count]==0) {
        self.jsonObject = [Utils getFileReadWithFileName:@"RBKol" andFilePath:@"RB"];
        self.datalist = [NSMutableArray new];
        self.datalist = [JsonService getRBKolListEntity:self.jsonObject andBackArray:self.datalist];
        self.headlist = [NSMutableArray new];
        self.headlist = [JsonService getRBKolBannerListEntity:self.jsonObject andBackArray:self.headlist];
    } else {
        [Utils getFileWrite:self.jsonObject andFileName:@"RBKol" andFilePath:@"RB"];
    }
    if ([self.newlist count]==0) {
        self.jsonObjectNew = [Utils getFileReadWithFileName:@"RBKolNew" andFilePath:@"RB"];
        self.newlist = [NSMutableArray new];
        self.newlist = [JsonService getRBKolListEntity:self.jsonObjectNew andBackArray:self.newlist];
    } else {
        [Utils getFileWrite:self.jsonObjectNew andFileName:@"RBKolNew" andFilePath:@"RB"];
    }
    [UIView performWithoutAnimation:^{
        [self.collectionviewn reloadData];
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.datalist count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RBKolCollectionCell *cell = (RBKolCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"RBKolCollectionCell" forIndexPath:indexPath];
    [cell loadKolCollectionCellWith:self.datalist andIndex:indexPath];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((ScreenWidth-4.0)/2.0, ((ScreenWidth-4.0)/2.0)*450.0/375.0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate
         respondsToSelector:@selector(RBKolViewSelected:andIndex:)]) {
        [self.delegate RBKolViewSelected:self andIndex:(int)indexPath.row];
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
        collectionviewnHeadView = [collectionView
                              dequeueReusableSupplementaryViewOfKind:kind
                              withReuseIdentifier:@"RBKolHeadCollectionCell"
                              forIndexPath:indexPath];
    }
    [collectionviewnHeadView loadCycleScrollViewWith:self.headlist and:self.newlist];
    collectionviewnHeadView.delegate = self;
    return collectionviewnHeadView;
}

#pragma mark - UICollectionView Delegate
- (void)RBKolHeadCollectionCellSelected:(RBKolHeadCollectionCell *)view andIndex:(int)index {
    if ([self.delegate
         respondsToSelector:@selector(RBKolViewHeadSelected:andIndex:)]) {
        [self.delegate RBKolViewHeadSelected:self andIndex:index];
    }
}

- (void)RBKolHeadCollectionCellKolSelected:(RBKolHeadCollectionCell *)view andIndex:(int)index {
    if ([self.delegate
         respondsToSelector:@selector(RBKolViewKolSelected:andIndex:)]) {
        [self.delegate RBKolViewKolSelected:self andIndex:index];
    }
}

@end
