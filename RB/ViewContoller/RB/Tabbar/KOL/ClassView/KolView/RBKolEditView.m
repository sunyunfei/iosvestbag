//
//  RBKolEditView.m
//  RB
//
//  Created by AngusNi on 16/8/1.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBKolEditView.h"
#import "Service.h"

@implementation RBKolEditView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = YES;
        //
        self.tLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 0, frame.size.width, frame.size.height)];
        self.tLabel.font = font_15;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self addSubview:self.tLabel];
        //
        self.cLabel = [[UILabel alloc]initWithFrame:CGRectMake(70.0, 0, frame.size.width-CellLeft-13.0-70.0-5.0, frame.size.height)];
        self.cLabel.font = font_15;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.cLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:self.cLabel];
        //
        self.arrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width-CellLeft-13.0, (frame.size.height-13.0)/2.0, 13.0, 13.0)];
        self.arrowLabel.font = font_icon_(13.0);
        self.arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.arrowLabel.text = Icon_btn_right;
        [self addSubview:self.arrowLabel];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, frame.size.height-0.5, frame.size.width, 0.5)];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self addSubview:lineLabel];
    }
    return self;
}

@end
