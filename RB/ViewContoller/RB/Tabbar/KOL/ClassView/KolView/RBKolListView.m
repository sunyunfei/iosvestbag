//
//  RBKolListView.m
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBKolListView.h"
@interface RBKolListView () {
    RBKolHeadCollectionCell*collectionviewnHeadView;
}
@end

@implementation RBKolListView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        //
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        layout.minimumColumnSpacing = 2.0;
        layout.minimumInteritemSpacing = layout.minimumColumnSpacing;
        layout.headerHeight = 0;
        layout.footerHeight = 0;
        layout.columnCount = 2;
        self.collectionviewn = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:layout];
        self.collectionviewn.dataSource = self;
        self.collectionviewn.delegate = self;
        self.collectionviewn.alwaysBounceVertical = YES;
        self.collectionviewn.backgroundColor = [UIColor clearColor];
        [self.collectionviewn registerClass:[RBKolCollectionCell class]
            forCellWithReuseIdentifier:@"RBKolCollectionCell"];
        [self addSubview:self.collectionviewn];
        [self setRefreshHeaderView];
        //
        self.defaultView = [[UIView alloc]initWithFrame:CGRectMake(0, (self.height-180.0)/2.0, ScreenWidth, 180.0)];
        self.defaultView.tag = 9999;
        self.defaultView.hidden = YES;
        [self addSubview:self.defaultView];
        //
        UIImageView*defaultIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-100.0)/2.0, 0, 100.0, 100.0)];
        defaultIV.image = [UIImage imageNamed:@"icon_task_default.png"];
        [self.defaultView addSubview:defaultIV];
        //
        UILabel*defaultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, defaultIV.bottom, ScreenWidth, 20.0)];
        defaultLabel.font = font_cu_17;
        defaultLabel.textAlignment = NSTextAlignmentCenter;
        defaultLabel.text = NSLocalizedString(@"R1028", @"当前页面无数据,试试别的页面吧");
        defaultLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [self.defaultView addSubview:defaultLabel];
        //
        UIButton*defaultBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        defaultBtn.frame = CGRectMake((ScreenWidth-180.0)/2.0, defaultLabel.bottom+20.0, 180.0, 40.0);
        defaultBtn.titleLabel.font = font_cu_17;
        [defaultBtn setTitle:NSLocalizedString(@"R7006",@"点击查看更多数据") forState:UIControlStateNormal];
        [defaultBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
        defaultBtn.layer.borderColor = [[Utils getUIColorWithHexString:SysColorBlue]CGColor];
        defaultBtn.layer.borderWidth = 1.0;
        defaultBtn.layer.masksToBounds = YES;
        defaultBtn.layer.cornerRadius = 5.0;
        [defaultBtn addTarget:self
                     action:@selector(defaultBtnAction:)
           forControlEvents:UIControlEventTouchUpInside];
        [self.defaultView addSubview:defaultBtn];
    }
    return self;
}
#pragma mark - UIButton Delegate
- (void)defaultBtnAction:(UIButton*)sender {
    if ([self.delegate
         respondsToSelector:@selector(RBKolListViewMore:)]) {
        [self.delegate RBKolListViewMore:self];
    }
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)RBKolListViewLoadRefreshViewFirstData {
    self.pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    self.pageSize = 20;
    if ([self.delegate
         respondsToSelector:@selector(RBKolListViewLoadRefreshViewData:)]) {
        [self.delegate RBKolListViewLoadRefreshViewData:self];
    }
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    self.collectionviewn.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    self.collectionviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.pageIndex = self.pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)RBKolListViewSetRefreshViewFinish {
    [self.collectionviewn.mj_header endRefreshing];
    [self.collectionviewn.mj_footer endRefreshing];
    if ([self.datalist count] == (self.pageIndex+1)*self.pageSize) {
        if (self.collectionviewn.mj_footer == nil) {
            [self setRefreshFooterView];
        }
    } else {
        [self.collectionviewn.mj_footer removeFromSuperview];
        self.collectionviewn.mj_footer = nil;
    }
    [UIView performWithoutAnimation:^{
        [self.collectionviewn reloadData];
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.datalist count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RBKolCollectionCell *cell = (RBKolCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"RBKolCollectionCell" forIndexPath:indexPath];
    [cell loadKolCollectionCellWith:self.datalist andIndex:indexPath];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((ScreenWidth-4.0)/2.0, ((ScreenWidth-4.0)/2.0)*450.0/375.0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate
         respondsToSelector:@selector(RBKolListViewSelected:andIndex:)]) {
        [self.delegate RBKolListViewSelected:self andIndex:(int)indexPath.row];
    }
}

@end
