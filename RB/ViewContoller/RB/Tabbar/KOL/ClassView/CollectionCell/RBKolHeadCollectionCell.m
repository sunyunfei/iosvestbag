//
//  RBKolHeadCollectionCell.m
//

#import "RBKolHeadCollectionCell.h"
#import "Service.h"

@implementation RBKolHeadCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = [Utils getUIColorWithHexString:@"ffffff" andAlpha:0.8];
    }
    return self;
}

-(void)loadCycleScrollViewWith:(NSMutableArray*)headlist and:(NSMutableArray*)newlist {
    [self removeAllSubviews];
    [self.cycleScrollView removeFromSuperview];
    self.cycleScrollView = nil;
    self.newlist = newlist;
    NSMutableArray*imglist = [NSMutableArray new];
    for(RBKOLBannerEntity *entity in headlist) {
        [imglist addObject:entity.cover_url];
    }
    self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, ScreenWidth, (ScreenWidth/750.0)*360.0) shouldInfiniteLoop:YES imageNamesGroup:imglist];
//    if (titlelist!=nil) {
//        self.cycleScrollView.titlesGroup = titlelist;
//    }
    self.cycleScrollView.delegate = self;
    self.cycleScrollView.autoScrollTimeInterval = 5.0;
    [self.cycleScrollView setPageControlStyle:SDCycleScrollViewPageContolStyleClassic];
    [self.cycleScrollView setCurrentPageDotColor:[Utils getUIColorWithHexString:SysColorYellow]];
    [self.cycleScrollView setPageDotColor:[Utils getUIColorWithHexString:SysColorWhite andAlpha:0.6]];
    [self.cycleScrollView setPageControlAliment:SDCycleScrollViewPageContolAlimentCenter];
//    self.cycleScrollView.backgroundColor = [Utils getUIColorWithHexString:@"ffffff" andAlpha:0.8];
    [self addSubview:self.cycleScrollView];
    //
//    UISegmentedControl*sc = [[UISegmentedControl alloc]initWithItems:@[@"",@""]];
//    [sc setDividerImage:[UIImage imageNamed:@"icon_kol_left.png"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//    [sc setDividerImage:[UIImage imageNamed:@"icon_kol_right.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
//    sc.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//    sc.tintColor = [Utils getUIColorWithHexString:SysColorYellow];
//    sc.frame = CGRectMake((ScreenWidth-130.0)/2.0, self.cycleScrollView.bottom+5.0, 130, 30);
//    [self addSubview:sc];
    //
    UILabel*newGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.cycleScrollView.bottom, ScreenWidth, CellBottom)];
    newGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
//    [self addSubview:newGapLabel];
    //
    UILabel*newLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.cycleScrollView.bottom, ScreenWidth, 45.0)];
    newLabel.font = font_cu_15;
    newLabel.text = @"最新 · NEW";
    newLabel.textAlignment = NSTextAlignmentCenter;
    newLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    newLabel.backgroundColor = [Utils getUIColorWithHexString:@"ffffff" andAlpha:1];
    [self addSubview:newLabel];
    //
//    UILabel*newTipLabel = [[UILabel alloc] initWithFrame:CGRectMake((ScreenWidth-20.0)/2.0, newLabel.bottom+6.0, 20.0, 4.0)];
//    newTipLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
//    [self addSubview:newTipLabel];
    //
    LTInfiniteScrollView*scrollviewn = [[LTInfiniteScrollView alloc]initWithFrame:CGRectMake(0, newLabel.bottom, ScreenWidth, 1.4*ScreenWidth/5.0)];
    scrollviewn.delegate = self;
    scrollviewn.dataSource = self;
    scrollviewn.scrollEnabled = YES;
    scrollviewn.backgroundColor = [Utils getUIColorWithHexString:@"ffffff" andAlpha:1];
    [self addSubview:scrollviewn];
    [scrollviewn reloadDataWithInitialIndex:2];
    //
    UILabel*hotGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, scrollviewn.bottom+10.0, ScreenWidth, CellBottom)];
    hotGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self addSubview:hotGapLabel];
    //
    UILabel*hotLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, hotGapLabel.bottom, ScreenWidth, 45.0)];
    hotLabel.font = font_cu_15;
    hotLabel.text = @"热门 · HOT";
    hotLabel.textAlignment = NSTextAlignmentCenter;
    hotLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    hotLabel.backgroundColor = [Utils getUIColorWithHexString:@"ffffff" andAlpha:1];
    [self addSubview:hotLabel];
    //
//    AutoSlideScrollView*slideView = [[AutoSlideScrollView alloc] initWithFrame:CGRectMake(0, self.cycleScrollView.bottom+17.0, ScreenWidth, (42.0+27.0)*2.0+15.0)animationDuration:-1];
//    [self addSubview:slideView];
//    slideView.totalPagesCount = ^NSInteger(void) {
//        return 3;
//    };
//    slideView.fetchContentViewAtIndex = ^UIView *(NSInteger page) {
//        UIView*contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, (42.0+27.0)*2.0)];
//        NSArray*classArray = @[@"互联网",@"美妆",@"母婴",@"娱乐",@"旅游",@"教育",@"时尚",@"游戏",@"房地产",@"财经",@"数码",@"家电",@"健康",@"图书",@"体育",@"航空",@"家居",@"汽车",@"酒店",@"摄影",@"美食",@"健身",@"音乐",@"综合"];
//        for(int i=(int)page*8; i<((int)page+1)*8; i++) {
//            UIButton*classBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//            if (i<8) {
//                classBtn.frame = CGRectMake(25.0+(i%4)*(42.0+(ScreenWidth-4.0*42.0-2*25.0)/3.0), (42.0+27.0)*(i/4), 42.0, 42.0);
//            } else if (i>=8&&i<16) {
//                classBtn.frame = CGRectMake(25.0+((i-8)%4)*(42.0+(ScreenWidth-4.0*42.0-2*25.0)/3.0), (42.0+27.0)*((i-8)/4), 42.0, 42.0);
//            } else {
//                classBtn.frame = CGRectMake(25.0+((i-16)%4)*(42.0+(ScreenWidth-4.0*42.0-2*25.0)/3.0), (42.0+27.0)*((i-16)/4), 42.0, 42.0);
//            }
//            [classBtn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",classArray[i]]] forState:UIControlStateNormal];
//            [classBtn addTarget:self action:@selector(classBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//            classBtn.tag = 10000+i;
//            [contentView addSubview:classBtn];
//            //
//            UILabel*classLabel = [[UILabel alloc] initWithFrame:CGRectMake(classBtn.left, classBtn.bottom, classBtn.width, 20.0)];
//            classLabel.font = font_13;
//            classLabel.text = classArray[i];
//            classLabel.textAlignment = NSTextAlignmentCenter;
//            classLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
//            [contentView addSubview:classLabel];
//        }
//        UILabel*numLabel = [[UILabel alloc] initWithFrame:CGRectMake((contentView.width-20.0)/2.0, contentView.height-7.0, 20.0, 20.0)];
//        numLabel.font = font_11;
//        numLabel.text = [NSString stringWithFormat:@"%d/3",(int)page+1];
//        numLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
//        [contentView addSubview:numLabel];
//        return contentView;
//    };
//    slideView.pageControl.hidden = YES;
//    //
//    UILabel*gapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, slideView.bottom, ScreenWidth, CellBottom)];
//    gapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
//    [self addSubview:gapLabel];
//    //
//    UILabel*hotLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, gapLabel.bottom+18.0, ScreenWidth, 15.0)];
//    hotLabel.font = font_cu_cu_13;
//    hotLabel.text = @"热门KOL推荐";
//    hotLabel.textAlignment = NSTextAlignmentCenter;
//    hotLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
//    [self addSubview:hotLabel];
//    //
//    UILabel*tipLabel = [[UILabel alloc] initWithFrame:CGRectMake((ScreenWidth-20.0)/2.0, hotLabel.bottom+6.0, 20.0, 4.0)];
//    tipLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
//    [self addSubview:tipLabel];
}

#pragma mark - UIButton Delegate
- (void)classBtnAction:(UIButton*)sender {
    if ([self.delegate
         respondsToSelector:@selector(RBKolHeadCollectionCellSelected:andIndex:)]) {
        [self.delegate RBKolHeadCollectionCellSelected:self andIndex:(int)sender.tag];
    }
}

#pragma mark - SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    if ([self.delegate
         respondsToSelector:@selector(RBKolHeadCollectionCellSelected:andIndex:)]) {
        [self.delegate RBKolHeadCollectionCellSelected:self andIndex:(int)index];
    }
}

# pragma mark - LTInfiniteScrollView dataSource
- (NSInteger)numberOfViews {
    return [self.newlist count];
}

- (NSInteger)numberOfVisibleViews {
    return 5;
}

# pragma mark - LTInfiniteScrollView delegate
- (UIView *)viewAtIndex:(NSInteger)index reusingView:(UIView *)view {
    RBKOLEntity*kolEntity = self.newlist[index];
    if (view) {
        //[((UIImageView *)view) yy_setImageWithURL:[NSURL URLWithString:kolEntity.avatar_url] placeholder:PlaceHolderUserImage];
        [((UIImageView *)view) setImageWithURL:[NSURL URLWithString:kolEntity.avatar_url] placeholder:PlaceHolderUserImage];
        return view;
    }
    UIImageView *aView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth/5.0, ScreenWidth/5.0)];
    aView.contentMode = UIViewContentModeScaleAspectFill;
   // [aView yy_setImageWithURL:[NSURL URLWithString:kolEntity.avatar_url] placeholder:PlaceHolderUserImage];
    [aView setImageWithURL:[NSURL URLWithString:kolEntity.avatar_url] placeholder:PlaceHolderUserImage];
    aView.layer.cornerRadius = aView.size.width/2.0f;
    aView.layer.masksToBounds = YES;
    aView.clipsToBounds = YES;
    aView.userInteractionEnabled = YES;
    //
    UIButton*userBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [userBtn addTarget:self
                action:@selector(userBtnAction:)
      forControlEvents:UIControlEventTouchUpInside];
    userBtn.frame = aView.frame;
    [aView addSubview:userBtn];
    return aView;
}

- (void)updateView:(UIView *)view withProgress:(CGFloat)progress scrollDirection:(ScrollDirection)direction {
    // you can appy animations duration scrolling here
    CATransform3D transform = CATransform3DIdentity;
    // scale
    CGFloat size = ScreenWidth/5.0;
    CGPoint center = view.center;
    view.center = center;
    size = size * (1.4 - 0.3 * (fabs(progress)));
    view.frame = CGRectMake(0, 0, size, size);
    view.layer.cornerRadius = size / 2;
    view.center = center;
    // translate
    CGFloat translate = ScreenWidth/5.0/3 * progress;
    if (progress > 1) {
        translate = ScreenWidth/5.0/3;
    } else if (progress < -1) {
        translate = -ScreenWidth/5.0/3;
    }
    transform = CATransform3DTranslate(transform, translate, 0, 0);
    // rotate
    if (fabs(progress) < 1) {
        CGFloat angle = 0;
        if(progress > 0) {
            angle = - M_PI * (1 - fabs(progress));
        } else {
            angle =  M_PI * (1 - fabs(progress));
        }
        transform.m34 = 1.0 / -600;
        if (fabs(progress) <= 0.5) {
            angle =  M_PI * progress;
        }
        transform = CATransform3DRotate(transform, angle , 0.0f, 1.0f, 0.0f);
    }
    view.layer.transform = transform;
    RBKOLEntity*kolEntity = self.newlist[view.tag];
   // [((UIImageView *)view) yy_setImageWithURL:[NSURL URLWithString:kolEntity.avatar_url] placeholder:PlaceHolderUserImage];
    [((UIImageView *)view) setImageWithURL:[NSURL URLWithString:kolEntity.avatar_url] placeholder:PlaceHolderUserImage];
}

- (void)scrollView:(LTInfiniteScrollView *)scrollView didScrollToIndex:(NSInteger)index {
//    NSLog(@"scroll to: %d", (int)index);
}

- (void)userBtnAction:(UIButton*)sender {
//    NSLog(@"userBtnAction: %d", (int)sender.superview.tag);
    if ([self.delegate
         respondsToSelector:@selector(RBKolHeadCollectionCellKolSelected:andIndex:)]) {
        [self.delegate RBKolHeadCollectionCellKolSelected:self andIndex:(int)sender.superview.tag];
    }
}


@end
