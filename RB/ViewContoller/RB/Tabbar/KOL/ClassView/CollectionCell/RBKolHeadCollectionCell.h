//
//  RBKolHeadCollectionCell.h
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
#import "LTInfiniteScrollView.h"

@class RBKolHeadCollectionCell;
@protocol RBKolHeadCollectionCellDelegate <NSObject>
@optional
- (void)RBKolHeadCollectionCellSelected:(RBKolHeadCollectionCell *)view andIndex:(int)index;
- (void)RBKolHeadCollectionCellKolSelected:(RBKolHeadCollectionCell *)view andIndex:(int)index;
@end

@interface RBKolHeadCollectionCell : UICollectionReusableView
<SDCycleScrollViewDelegate,LTInfiniteScrollViewDelegate,LTInfiniteScrollViewDataSource>
@property(nonatomic, weak) id<RBKolHeadCollectionCellDelegate> delegate;
@property (nonatomic,strong) SDCycleScrollView *cycleScrollView;
@property (nonatomic,strong) NSMutableArray *newlist;
-(void)loadCycleScrollViewWith:(NSMutableArray*)headlist and:(NSMutableArray*)newlist;
@end
