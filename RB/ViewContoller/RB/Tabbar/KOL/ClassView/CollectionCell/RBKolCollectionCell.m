//
//  RBKolCollectionCell.m
//

#import "RBKolCollectionCell.h"
#import "Service.h"
#import "YYKit.h"
@implementation RBKolCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
	if (self = [super initWithFrame:frame]) {
        float h = frame.size.height;
        if (frame.size.height>((ScreenWidth-4.0)/2.0)*450.0/375.0) {
            h = ((ScreenWidth-4.0)/2.0)*450.0/375.0;
        }
        //大图片
        self.cellIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, h)];
        self.cellIV.backgroundColor = [UIColor clearColor];
        self.cellIV.contentMode = UIViewContentModeScaleAspectFill;
        self.cellIV.clipsToBounds = YES;
        [self.contentView addSubview:self.cellIV];
        //
        self.cellBgIV = [[UIImageView alloc]initWithFrame:CGRectMake((self.cellIV.width-60.0)/2.0, (self.cellIV.height-60.0)/2.0, 60, 60)];
        self.cellBgIV.backgroundColor = [UIColor clearColor];
        self.cellBgIV.contentMode = UIViewContentModeScaleAspectFill;
        self.cellBgIV.clipsToBounds = YES;
        [self.contentView addSubview:self.cellBgIV];
        //
        UIImageView*cellBgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, h-(frame.size.width/374.0*165.0), frame.size.width, frame.size.width/374.0*165.0)];
        cellBgIV.image = [UIImage imageNamed:@"pic_kol_cell_bg.png"];
        [self.contentView addSubview:cellBgIV];
        //
        self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(5.0, self.cellIV.bottom-40.0, frame.size.width, 18.0)];
        self.nameLabel.font = font_cu_15;
        self.nameLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        [self.contentView addSubview:self.nameLabel];
        //
        self.tLabel = [[UILabel alloc]initWithFrame:CGRectMake(5.0, self.cellIV.bottom-40.0, frame.size.width, 18.0)];
        self.tLabel.font = font_cu_11;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
        [self.contentView addSubview:self.tLabel];
        //
        self.classView = [[RBKolClassView alloc]initWithFrame:CGRectMake(5.0, self.cellIV.bottom-20.0, frame.size.width, 20.0)];
        [self.contentView addSubview:self.classView];
	}
	return self;
}

- (void)loadKolCollectionCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    if([datalist count]!=0) {
        RBKOLEntity*kolEntity = datalist[indexPath.row];
        if (kolEntity.avatar_url.length==0) {
            self.cellBgIV.image = nil;
            self.cellIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"pic_kol_avatar_%d.jpg",arc4random()%4]];
        } else {
            self.cellBgIV.image = nil;
//            [self.cellIV yy_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",kolEntity.avatar_url]] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
            [self.cellIV setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",kolEntity.avatar_url]] options:YYWebImageOptionProgressiveBlur | YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation];
        }
        self.nameLabel.text = kolEntity.name;
        if(self.nameLabel.text.length>11) {
            self.nameLabel.text = [self.nameLabel.text substringToIndex:11];
        }
        NSString*socialStr = @"";
        for (int i=0; i<[kolEntity.social_accounts count]; i++) {
            RBKOLSocialEntity*social = kolEntity.social_accounts[i];
            if (i==0) {
                socialStr = social.provider_name;
            } else {
                socialStr = [NSString stringWithFormat:@"%@/%@",socialStr,social.provider_name];
            }
        }
        self.tLabel.text = [NSString stringWithFormat:@" · %@",socialStr];
        NSMutableArray*tagArray = [NSMutableArray new];
        for (RBTagEntity*tag in kolEntity.tags) {
            [tagArray addObject:tag.label];
        }
        [self.classView loadKolClassView:tagArray];
        //
        CGSize nameLabelSize = [Utils getUIFontSizeFitW:self.nameLabel];
        self.nameLabel.width = nameLabelSize.width;
        self.tLabel.left = self.nameLabel.right;
        self.tLabel.width = self.width - self.tLabel.left;
    }
}

@end
