//
//  RBKolCollectionCell.h
//

#import <UIKit/UIKit.h>
#import "RBKolClassView.h"

@interface RBKolCollectionCell : UICollectionViewCell
@property (nonatomic,strong) UIImageView *cellBgIV;
@property (nonatomic,strong) UIImageView *cellIV;
@property (nonatomic,strong) UILabel*nameLabel;
@property (nonatomic,strong) UILabel*tLabel;
@property (nonatomic,strong) RBKolClassView*classView;
- (void)loadKolCollectionCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
@end
