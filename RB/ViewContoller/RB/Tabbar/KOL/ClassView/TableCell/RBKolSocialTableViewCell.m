//
//  RBKolSocialTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBKolSocialTableViewCell.h"
#import "Service.h"

@implementation RBKolSocialTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        self.bgView = [[UIView alloc] initWithFrame:CGRectZero];
        self.bgView.userInteractionEnabled = YES;
        self.bgView.backgroundColor = [UIColor whiteColor];
        self.bgView.tag = 1000;
        [self.contentView addSubview:self.bgView];
        //
        self.iconIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.iconIV.tag = 1001;
        [self.contentView addSubview:self.iconIV];
        //
        self.tLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.tLabel.font = font_cu_15;
        self.tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.tLabel.textAlignment = NSTextAlignmentCenter;
        self.tLabel.tag = 1002;
        [self.contentView addSubview:self.tLabel];
        //
        self.cLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.cLabel.font = font_11;
        self.cLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.cLabel.textAlignment = NSTextAlignmentCenter;
        self.cLabel.tag = 1003;
        [self.contentView addSubview:self.cLabel];
        //
        self.tLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
        self.tLabel1.font = font_cu_15;
        self.tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.tLabel1.textAlignment = NSTextAlignmentCenter;
        self.tLabel1.tag = 1004;
        [self.contentView addSubview:self.tLabel1];
        //
        self.cLabel1 = [[UILabel alloc] initWithFrame:CGRectZero];
        self.cLabel1.font = font_11;
        self.cLabel1.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.cLabel1.textAlignment = NSTextAlignmentCenter;
        self.cLabel1.tag = 1005;
        [self.contentView addSubview:self.cLabel1];
        //
        self.tLabel2 = [[UILabel alloc] initWithFrame:CGRectZero];
        self.tLabel2.font = font_cu_15;
        self.tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.tLabel2.textAlignment = NSTextAlignmentCenter;
        self.tLabel2.tag = 1006;
        [self.contentView addSubview:self.tLabel2];
        //
        self.cLabel2 = [[UILabel alloc] initWithFrame:CGRectZero];
        self.cLabel2.font = font_11;
        self.cLabel2.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.cLabel2.textAlignment = NSTextAlignmentCenter;
        self.cLabel2.tag = 1007;
        [self.contentView addSubview:self.cLabel2];
        //
        self.arrowLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.arrowLabel.font = font_icon_(13.0);
        self.arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.arrowLabel.text = Icon_btn_right;
        self.arrowLabel.tag = 1008;
        [self.contentView addSubview:self.arrowLabel];
        //
        self.lineLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        self.lineLabel.tag = 1010;
        [self.contentView addSubview:self.lineLabel];
    }
    return self;
}

- (instancetype)loadRBKolSocialCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    RBKOLSocialEntity*entity = datalist[indexPath.row];
    self.iconIV.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_ON.png",entity.provider_name]];
    self.arrowLabel.hidden = YES;
    if (entity.uid.length!=0||[entity.provider isEqualToString:@"zhihu"]||[entity.provider isEqualToString:@"meipai"]||[entity.provider isEqualToString:@"miaopai"]) {
        self.arrowLabel.hidden = NO;
    }
    if([entity.provider isEqualToString:@"weibo"]){
        self.cLabel2.text = NSLocalizedString(@"R7007",@"微博数");
    } else if([entity.provider isEqualToString:@"meipai"]||[entity.provider isEqualToString:@"miaopai"]){
        self.cLabel2.text = NSLocalizedString(@"R7009",@"视频数");
    } else if([entity.provider isEqualToString:@"zhihu"]){
        self.cLabel2.text = NSLocalizedString(@"R7011",@"问答数");
    } else {
        self.cLabel2.text = NSLocalizedString(@"R7008",@"文章数");
    }
    self.tLabel.text = entity.username;
    self.cLabel.text = NSLocalizedString(@"R7012",@"昵称");
    self.tLabel1.text = [Utils getNSStringWan:entity.followers_count];
    self.cLabel1.text = NSLocalizedString(@"R7013",@"粉丝数");
    self.tLabel2.text = entity.statuses_count;
    
    self.iconIV.frame = CGRectMake(CellLeft, (75.0-40.0)/2.0, 40.0, 40.0);
    self.tLabel.frame = CGRectMake(self.iconIV.right, (75.0-36.0)/2.0, (ScreenWidth-2*CellLeft-40.0)/3.0-5.0, 20.0);
    self.cLabel.frame = CGRectMake(self.tLabel.left, self.tLabel.bottom, self.tLabel.width, 16.0);
    self.tLabel1.frame = CGRectMake(self.tLabel.right, self.tLabel.top, self.tLabel.width, self.tLabel.height);
    self.cLabel1.frame = CGRectMake(self.tLabel1.left, self.cLabel.top, self.cLabel.width, self.cLabel.height);
    self.tLabel2.frame = CGRectMake(self.tLabel1.right, self.tLabel.top, self.tLabel.width, self.tLabel.height);
    self.cLabel2.frame = CGRectMake(self.tLabel2.left, self.cLabel.top, self.cLabel.width, self.cLabel.height);
    self.arrowLabel.frame = CGRectMake(ScreenWidth-CellLeft-13.0, (75.0-13.0)/2.0, 13.0, 13.0);
    self.lineLabel.frame = CGRectMake(0, 75.0-0.5, ScreenWidth, 0.5);
    self.bgView.frame = CGRectMake(0, 0, self.lineLabel.width, self.lineLabel.bottom);
    self.frame = self.bgView.frame;
    return self;
}

@end
