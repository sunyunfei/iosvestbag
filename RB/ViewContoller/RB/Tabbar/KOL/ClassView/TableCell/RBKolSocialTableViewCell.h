//
//  RBKolSocialTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface RBKolSocialTableViewCell : UITableViewCell

@property(nonatomic, strong) UIView *bgView;
//
@property(nonatomic, strong) UIImageView *iconIV;
//
@property(nonatomic, strong) UILabel *tLabel;
//
@property(nonatomic, strong) UILabel *cLabel;
//
@property(nonatomic, strong) UILabel *tLabel1;
//
@property(nonatomic, strong) UILabel *cLabel1;
//
@property(nonatomic, strong) UILabel *tLabel2;
//
@property(nonatomic, strong) UILabel *cLabel2;
//
@property(nonatomic, strong) UILabel*arrowLabel;
//
@property(nonatomic, strong) UILabel*lineLabel;

- (instancetype)loadRBKolSocialCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;

@end
