//
//  RBAdvertiserPayMoneyController.m
//  RB
//
//  Created by RB8 on 2018/6/5.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBAdvertiserPayMoneyController.h"

@interface RBAdvertiserPayMoneyController (){
    InsetsTextField*inputTF;
    InsetsTextField*inputTF2;
}
@end

@implementation RBAdvertiserPayMoneyController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R6046", @"充值");
    //
    [self loadEditView];
    [self loadFooterView];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"advertiser-recharge"];
    // 监听支付宝支付状态
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationAlipayStatus:)
                                                 name:NotificationAlipayStatus object:nil];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"advertiser-recharge"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)footerBtnAction:(UIButton *)sender {
    if(inputTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6047", @"充值金额")]];
    }
    [self.hudView showOverlay];
    // RB-发布活动-品牌主账户充值
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityAddMyRechargeWithCredits:inputTF.text andCode:inputTF2.text];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R6034", @"立即充值") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIScrollView*editScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self.view addSubview:editScrollView];
    //
    UILabel *tView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, editScrollView.width, 50.0)];
    tView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:tView];
    //
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, tView.top, ScreenWidth, tView.height)];
    tLabel.text = NSLocalizedString(@"R6047", @"充值金额");
    tLabel.font = font_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel];
    CGSize tLabelSize = [Utils getUIFontSizeFitW:tLabel];
    tLabel.width = tLabelSize.width;
    //
    inputTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(tLabel.right+20.0, tView.top, ScreenWidth, tView.height)];
    inputTF.font = font_15;
    inputTF.keyboardType =  UIKeyboardTypeNumberPad;
    inputTF.clearButtonMode = UITextFieldViewModeNever;
    inputTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:inputTF];
    inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"R6048", @"最低充值金额100元") attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    //
    TextFiledKeyBoard *textFieldKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textFieldKB.delegateT = self;
    [inputTF setInputAccessoryView:textFieldKB];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, tView.bottom-0.5, ScreenWidth, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:lineLabel];
    //
    UILabel *tView2 = [[UILabel alloc] initWithFrame:CGRectMake(0, lineLabel.bottom, editScrollView.width, 50.0)];
    tView2.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:tView2];
    //
    UILabel*tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, tView2.top, ScreenWidth, tView.height)];
    tLabel2.text = NSLocalizedString(@"R6082", @"邀请码");
    tLabel2.font = font_15;
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel2];
    CGSize tLabel2Size = [Utils getUIFontSizeFitW:tLabel2];
    tLabel2.width = tLabel2Size.width;
    //
    inputTF2 = [[InsetsTextField alloc]initWithFrame:CGRectMake(tLabel.right+20.0, tView2.top, ScreenWidth, tView.height)];
    inputTF2.font = font_15;
    inputTF2.keyboardType =  UIKeyboardTypeDefault;
    inputTF2.clearButtonMode = UITextFieldViewModeNever;
    inputTF2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:inputTF2];
    inputTF2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"R6083", @"可不填写") attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    [inputTF2 setInputAccessoryView:textFieldKB];
    //
    UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, tView2.bottom-0.5, ScreenWidth, 0.5)];
    lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:lineLabel2];
    //
    UILabel *tView1 = [[UILabel alloc] initWithFrame:CGRectMake(0, lineLabel2.bottom, editScrollView.width, 50.0)];
    tView1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:tView1];
    //
    UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, tView1.top, ScreenWidth, tView.height)];
    tLabel1.text = NSLocalizedString(@"R6049", @"支付方式");
    tLabel1.font = font_15;
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel1];
    CGSize tLabel1Size = [Utils getUIFontSizeFitW:tLabel1];
    tLabel1.width = tLabel1Size.width;
    //
    UIImageView*cIV = [[UIImageView alloc]initWithFrame:CGRectMake(tLabel1.right+20.0, tView1.top+9.0, (32.0/60.0)*169.0, 32.0)];
    cIV.image = [UIImage imageNamed:@"icon_advertiser_alipay.png"];
    [editScrollView addSubview:cIV];
    //
    UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, tView1.bottom-0.5, ScreenWidth, 0.5)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:lineLabel1];
    //
    UILabel*tLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(0, lineLabel1.bottom, ScreenWidth, tView.height)];
    tLabel3.textAlignment = NSTextAlignmentCenter;
    tLabel3.text = NSLocalizedString(@"R6050", @"*充值后可以使用广告账户余额进行活动发布的订单支付");
    tLabel3.font = font_11;
    tLabel3.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [editScrollView addSubview:tLabel3];
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}


#pragma mark - NSNotification Delegate
- (void)RBNotificationAlipayStatus:(NSNotification *)notification {
    NSDictionary*temp = notification.userInfo;
    if ([[NSString stringWithFormat:@"%@",temp[@"STATUS"]] isEqualToString:NSLocalizedString(@"R6028", @"订单支付成功")]) {
        [self.hudView showSuccessWithStatus:temp[@"STATUS"]];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           RBAdvertiserRechargeSuccessedViewController*toview = [[RBAdvertiserRechargeSuccessedViewController alloc] init];
                           toview.hidesBottomBarWhenPushed = YES;
                           [self.navigationController pushViewController:toview animated:YES];
                       });
    } else {
        [self.hudView showErrorWithStatus:temp[@"STATUS"]];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           RBAdvertiserRechargeErrorViewController*toview = [[RBAdvertiserRechargeErrorViewController alloc] init];
                           toview.hidesBottomBarWhenPushed = YES;
                           [self.navigationController pushViewController:toview animated:YES];
                       });
    }
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"recharge"]) {
        [self.hudView dismiss];
        // 支付宝接口
        NSString*orderStr = [jsonObject objectForKey:@"alipay_url"];
        NSLog(@"orderStr:%@",orderStr);
        NSString*scheme = [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"robin8" : @"robin8Test";
        [[AlipaySDK defaultService] payOrder:orderStr fromScheme:scheme callback:^(NSDictionary *resultDic) {
            //            NSLog(@"resultDic = %@",resultDic);
            //            NSLog(@"result = %@",[resultDic valueForKey:@"result"]);
            //            NSLog(@"memo = %@",[resultDic valueForKey:@"memo"]);
            //            NSLog(@"resultStatus = %@",[resultDic valueForKey:@"resultStatus"]);
            //            int tempTag = [NSString stringWithFormat:@"%@",[resultDic valueForKey:@"resultStatus"]].intValue;
            //            NSString*tempStr = @"";
            //            if(tempTag == 9000) {
            //                tempStr = NSLocalizedString(@"R6028", @"订单支付成功");
            //            } else if(tempTag == 8000) {
            //                tempStr = NSLocalizedString(@"R6051", @"正在处理中");
            //            } else if(tempTag == 4000) {
            //                tempStr = NSLocalizedString(@"R6039", @"订单支付失败");
            //            } else if(tempTag == 6001) {
            //                tempStr = NSLocalizedString(@"R6052", @"用户中途取消");
            //            } else if(tempTag == 6002) {
            //                tempStr = NSLocalizedString(@"R6053", @"网络连接出错");
            //            }
            //            NSLog(@"tempStr = %@",tempStr);
            //            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationAlipayStatus
            //                                                                object:self
            //                                                              userInfo:@{@"STATUS":tempStr}];
            //
        }];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
