//
//  RBAdvertiserPayDetailViewController.h
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBAdvertiserPaySuccessedViewController.h"
#import "RBAdvertiserRechargeViewController.h"
#import "RBAdvertiserPayErrorViewController.h"

@interface RBAdvertiserPayDetailViewController : RBBaseViewController
<RBBaseVCDelegate>
@property(nonatomic, strong) RBAdvertiserCampaignPayEntity*adCampaignPayEntity;
@end
