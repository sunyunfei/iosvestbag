//
//  RBAdvertiserPayErrorViewController.m
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserPayErrorViewController.h"

@interface RBAdvertiserPayErrorViewController () {
}
@end

@implementation RBAdvertiserPayErrorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R6039", @"订单支付失败");
    //
    [self loadEditView];
    // 监听支付宝支付状态
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationAlipayStatus:)
                                                 name:NotificationAlipayStatus object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"advertiser-add-pay-error"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"advertiser-add-pay-error"];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationAlipayStatus:(NSNotification *)notification {
    
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    RBAdvertiserMyViewController*toview = [[RBAdvertiserMyViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIScrollView*editScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:editScrollView];
    //
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake((ScreenWidth-80.0)/2.0, (editScrollView.height-80.0-40.0-50.0-45.0-10.0)/2.0-40.0, 80.0, 80.0)];
    tLabel.font = font_icon_(80.0);
    tLabel.text = Icon_btn_select_e;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorRed];
    [editScrollView addSubview:tLabel];
    //
    UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, tLabel.bottom+25.0, ScreenWidth, 20.0)];
    tLabel1.text = NSLocalizedString(@"R6039", @"订单支付失败");
    tLabel1.font = font_15;
    tLabel1.textAlignment = NSTextAlignmentCenter;
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel1];
    //
    UILabel*tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(0, tLabel1.bottom+5.0, ScreenWidth, 20.0)];
    tLabel2.text = NSLocalizedString(@"R6040", @"详细失败原因请联系Robin8客服");
    tLabel2.font = font_13;
    tLabel2.textAlignment = NSTextAlignmentCenter;
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [editScrollView addSubview:tLabel2];
    CGSize tLabel2Size = [Utils getUIFontSizeFitW:tLabel2];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake((ScreenWidth-tLabel2Size.width)/2.0, tLabel2.bottom+25.0, tLabel2Size.width, 45.0);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R6041", @"重新支付") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [editScrollView addSubview:footerBtn];
    //
    [editScrollView setContentSize:CGSizeMake(editScrollView.width, footerBtn.bottom)];
}

@end

