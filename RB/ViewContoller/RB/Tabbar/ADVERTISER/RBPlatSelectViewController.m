//
//  RBPlatSelectViewController.m
//  RB
//
//  Created by RB8 on 2017/9/25.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBPlatSelectViewController.h"
#import "RBPlatView.h"
@interface RBPlatSelectViewController ()<RBPlatViewDelegate,RBBaseVCDelegate>
@property(nonatomic,strong)RBPlatView * platView;
@end

@implementation RBPlatSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navTitle = NSLocalizedString(@"R8043", @"推广平台选择");
    self.delegate = self;
    _platView = [[[NSBundle mainBundle]loadNibNamed:@"RBPlatView" owner:nil options:nil]lastObject];
    _platView.frame = CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight - NavHeight);
    _platView.delegate = self;
    if ([_mark isEqualToString:@"wechat"]) {
        _platView.wechatSelected = YES;
        [_platView.friendsButton setImage:[UIImage imageNamed:@"RBPlat"] forState:UIControlStateNormal];
    }else if ([_mark isEqualToString:@"weibo"]){
        _platView.weiboSelected = YES;
        [_platView.weiboButton setImage:[UIImage imageNamed:@"RBPlat"] forState:UIControlStateNormal];
    }else if ([_mark isEqualToString:@"wechat,weibo"]){
        _platView.weiboSelected = YES;
        _platView.wechatSelected = YES;
        [_platView.friendsButton setImage:[UIImage imageNamed:@"RBPlat"] forState:UIControlStateNormal];
        [_platView.weiboButton setImage:[UIImage imageNamed:@"RBPlat"] forState:UIControlStateNormal];
    }
    [self.view addSubview:_platView];
    
}
-(void)upDelegateAcion{
    if (_platView.weiboSelected == NO && _platView.wechatSelected == NO) {
        [self.hudView showErrorWithStatus:@"请选择活动平台"];
    }else{
        self.platBlock(_platView.weiboSelected, _platView.wechatSelected);
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
