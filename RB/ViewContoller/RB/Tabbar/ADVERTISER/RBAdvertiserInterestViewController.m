//
//  RBAdvertiserInterestViewController.m
//  RB
//
//  Created by AngusNi on 16/8/2.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBAdvertiserInterestViewController.h"

@interface RBAdvertiserInterestViewController () {
    UIScrollView*scrollviewn;
//    NSMutableArray*selectedArray;
    NSArray*nameArray;
    NSMutableArray*selectedNameArray;
}
@end

@implementation RBAdvertiserInterestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R8026",@"KOL分类");
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height-NavHeight-45.0)];
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.userInteractionEnabled = YES;
    scrollviewn.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self.view addSubview:scrollviewn];
    //
    nameArray = @[@"internet",@"beauty",@"babies",@"entertainment",@"travel",@"education",@"fashion",@"games",@"realestate",@"finance",@"digital",@"appliances",@"health",@"books",@"sports",@"airline",@"furniture",@"auto",@"hotel",@"ce",@"camera",@"mobile",@"food",@"fitness",@"music",@"overall"];
    NSArray*tLabelArray = @[@"互联网",@"美妆",@"母婴",@"娱乐",@"旅游",@"教育",@"时尚",@"游戏",@"房地产",@"财经",@"数码",@"家电",@"健康",@"图书",@"体育",@"航空",@"家居",@"汽车",@"酒店",@"消费电子",@"摄影",@"手机",@"美食",@"健身",@"音乐",@"综合"];
    float tempWidth = (ScreenWidth-5*CellLeft)/4.0;
    selectedNameArray = [NSMutableArray new];
    for (int i=0; i<[tLabelArray count]; i++) {
        UIButton*tLabelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        tLabelBtn.frame = CGRectMake(CellLeft+(tempWidth+CellLeft)*(i%4), CellLeft+(40.0+CellLeft)*(i/4), tempWidth, 40.0);
        tLabelBtn.titleLabel.font = font_15;
        [tLabelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
        tLabelBtn.tag = 2000+i;
        tLabelBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        [tLabelBtn addTarget:self
                      action:@selector(tLabelBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        [tLabelBtn setTitle:tLabelArray[i] forState:UIControlStateNormal];
        [scrollviewn addSubview:tLabelBtn];
        if([[self.editView.cLabel.text componentsSeparatedByString:tLabelArray[i]]count]>1||[self.editView.cLabel.text isEqualToString:NSLocalizedString(@"R8028", @"全部")]){
            [selectedNameArray addObject:tLabelArray[i]];
            [tLabelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
            tLabelBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
        }
    }
    self.navTitle = [NSString stringWithFormat:@"%@(%d)",NSLocalizedString(@"R8026",@"KOL分类"),(int)[selectedNameArray count]];
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, CellLeft+([tLabelArray count]/4)*(40.0+CellLeft))];
    //
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton*)sender {
    if ([selectedNameArray count]<2) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R5083", @"最少选择2个")];
        return;
    }
    NSString*srcdic = @"";
    for (int i=0;i<[selectedNameArray count];i++) {
        if (i==0) {
            srcdic=[NSString stringWithFormat:@"%@",selectedNameArray[i]];
        } else {
            srcdic=[NSString stringWithFormat:@"%@,%@",srcdic,selectedNameArray[i]];
        }
    }
    self.editView.cLabel.text = srcdic;
    [self RBNavLeftBtnAction];
}

- (void)tLabelBtnAction:(UIButton *)sender {
    if([selectedNameArray containsObject:sender.titleLabel.text]){
        [selectedNameArray removeObject:sender.titleLabel.text];
        [sender setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
        sender.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    } else {
        [selectedNameArray addObject:sender.titleLabel.text];
        [sender setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
        sender.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    }
    self.navTitle = [NSString stringWithFormat:@"%@(%d)",NSLocalizedString(@"R8026",@"KOL分类"),(int)[selectedNameArray count]];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

@end
