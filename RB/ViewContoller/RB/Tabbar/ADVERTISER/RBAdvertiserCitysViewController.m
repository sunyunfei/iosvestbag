//
//  RBAdvertiserCitysViewController.m
//  RB
//
//  Created by AngusNi on 16/2/17.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBAdvertiserCitysViewController.h"

@interface RBAdvertiserCitysViewController (){
    UITableView*tableviewn;
    NSMutableDictionary*citysDic;
    NSMutableArray*keysArray;
    NSMutableDictionary*rowCitysDic;
    NSMutableArray*rowKeysArray;
    NSMutableArray*selectedNameArray;
    UIButton*rightBtn;
}
@end

@implementation RBAdvertiserCitysViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R8027", @"KOL城市");
    NSMutableParagraphStyle *paragraphStyle  =
    [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize labelSize  =
    [NSLocalizedString(@"R8029",@"全选") boundingRectWithSize:CGSizeMake(1000, 20)
                                                      options:NSStringDrawingUsesLineFragmentOrigin |
     NSStringDrawingUsesFontLeading
                                                   attributes:@{
                                                                NSFontAttributeName : font_cu_15,
                                                                NSParagraphStyleAttributeName : paragraphStyle
                                                                } context:nil]
    .size;
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.navView.width-labelSize.width-CellLeft, 20.0, labelSize.width, 44.0);
    rightBtn.titleLabel.font = font_15;
    [rightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
    [rightBtn setTitle:NSLocalizedString(@"R8029",@"全选") forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:rightBtn];

    
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight, self.view.width, self.view.height-NavHeight-45.0)
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundColor = [UIColor clearColor];
    tableviewn.sectionIndexTrackingBackgroundColor = [UIColor clearColor];
    tableviewn.sectionIndexBackgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
    //
    [self loadFooterView];
    //
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBUserInfoCitys];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnAction:(UIButton*)sender {
    if([sender.titleLabel.text isEqualToString:NSLocalizedString(@"R8029",@"全选")]){
        [rightBtn setTitle:NSLocalizedString(@"R8030",@"清空") forState:UIControlStateNormal];
        selectedNameArray = [NSMutableArray new];
        for (NSString *key in keysArray) {
            NSMutableArray*citys = [citysDic objectForKey:key];
            for (RBCityEntity*cityEntity in citys) {
                if(![selectedNameArray containsObject:cityEntity.name]){
                    [selectedNameArray addObject:cityEntity.name];
                }
            }
        }
    } else {
        selectedNameArray = [NSMutableArray new];
        [rightBtn setTitle:NSLocalizedString(@"R8029",@"全选") forState:UIControlStateNormal];
    }
    self.navTitle = [NSString stringWithFormat:@"%@(%d)",NSLocalizedString(@"R8027", @"KOL城市"),(int)[selectedNameArray count]];
    [tableviewn reloadData];
}

- (void)footerBtnAction:(UIButton*)sender {
    if ([selectedNameArray count]==0) {
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R6084", @"最少选择1个")];
        return;
    }
    NSString*srcdic = @"";
    for (int i=0;i<[selectedNameArray count];i++) {
        if (i==0) {
            srcdic=[NSString stringWithFormat:@"%@",selectedNameArray[i]];
        } else {
            srcdic=[NSString stringWithFormat:@"%@,%@",srcdic,selectedNameArray[i]];
        }
    }
    self.editView.cLabel.text = srcdic;
    [self RBNavLeftBtnAction];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark - initData Delegate
- (void)initCityData {
    citysDic = rowCitysDic;
    keysArray = rowKeysArray;
    [tableviewn reloadData];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [keysArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *key = keysArray[section];
    NSMutableArray *citySection = [citysDic objectForKey:key];
    return [citySection count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableviewn.width, 25.0)];
    UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, tableviewn.width - CellLeft*2.0, headView.height)];
    contentLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    contentLabel.font = font_15;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    [headView addSubview:contentLabel];
    contentLabel.text = [keysArray objectAtIndex:section];
    return headView;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return keysArray;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    UITableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSString *key = keysArray[indexPath.section];
    NSMutableArray*citys = [citysDic objectForKey:key];
    RBCityEntity*cityEntity = citys[indexPath.row];
    cell.textLabel.font = font_13;
    cell.textLabel.text = cityEntity.name;
    if([selectedNameArray containsObject:cityEntity.name]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [keysArray objectAtIndex:indexPath.section];
    RBCityEntity *cityEntity = [[citysDic objectForKey:key] objectAtIndex:indexPath.row];
    if([selectedNameArray containsObject:cityEntity.name]) {
        [selectedNameArray removeObject:cityEntity.name];
    } else {
        [selectedNameArray addObject:cityEntity.name];
    }
    [tableviewn reloadData];
    //
    self.navTitle = [NSString stringWithFormat:@"%@(%d)",NSLocalizedString(@"R8027", @"KOL城市"),(int)[selectedNameArray count]];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"cities"]) {
        [self.hudView dismiss];
        NSMutableArray*datalist = [NSMutableArray new];
        datalist = [JsonService getRBCityListEntity:jsonObject andBackArray:datalist];
        rowCitysDic = [NSMutableDictionary new];
        rowKeysArray = [NSMutableArray arrayWithArray:@[@"HOT",@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z"]];
        selectedNameArray = [NSMutableArray new];
        for (NSString*keystr in rowKeysArray) {
            NSMutableArray*citysarray=[NSMutableArray new];
            for (RBCityEntity*cityEntity in datalist) {
                if ([[[NSString stringWithFormat:@"%@",cityEntity.name_en]substringToIndex:1].uppercaseString isEqualToString:keystr]) {
                    [citysarray addObject:cityEntity];
                }
                if([[self.editView.cLabel.text componentsSeparatedByString:cityEntity.name]count]>1||[self.editView.cLabel.text isEqualToString:NSLocalizedString(@"R8028", @"全部")]){
                    if(![selectedNameArray containsObject:cityEntity.name]){
                        [selectedNameArray addObject:cityEntity.name];
                    }
                }
            }
            [rowCitysDic setObject:citysarray forKey:keystr];
        }
        RBCityEntity*cityDemo = [[RBCityEntity alloc]init];
        cityDemo.name = @"上海市";
        RBCityEntity*cityDemo1 = [[RBCityEntity alloc]init];
        cityDemo1.name = @"北京市";
        RBCityEntity*cityDemo2 = [[RBCityEntity alloc]init];
        cityDemo2.name = @"广州市";
        RBCityEntity*cityDemo3 = [[RBCityEntity alloc]init];
        cityDemo3.name = @"杭州市";
        RBCityEntity*cityDemo4 = [[RBCityEntity alloc]init];
        cityDemo4.name = @"苏州市";
        [rowCitysDic setObject:@[cityDemo,cityDemo1,cityDemo2,cityDemo3,cityDemo4] forKey:@"HOT"];
        self.navTitle = [NSString stringWithFormat:@"%@(%d)",NSLocalizedString(@"R8027", @"KOL城市"),(int)[selectedNameArray count]];
        [rightBtn setTitle:NSLocalizedString(@"R8030",@"清空") forState:UIControlStateNormal];
        [self initCityData];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
