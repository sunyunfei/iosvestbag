//
//  RBAdvertiserMyViewController.m
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserMyViewController.h"
#import "RBBrandMessageController.h"
@interface RBAdvertiserMyViewController (){
    NSString*brand_amount;
    NSString*name;
    NSString*avatar_url;
    NSString*company_name;
    NSString*brand_credit;
    NSString*dateStr;
    
    UILabel*incomeLabel;
    UIImageView*headImageView;
    UILabel * nameLabel;
    UILabel * companyLabel;
    UILabel * brandLabel;
    UILabel * scoreLabel;
    
    NSMutableDictionary * dict;
    
    RBPromoteEntity * entity;

}
@end

@implementation RBAdvertiserMyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R5019", @"品牌主");
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    dict = [[NSMutableDictionary alloc]init];
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"advertiser-my"];
    // RB-发布活动-品牌主账户
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityAddAccount];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"advertiser-my"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    if (company_name.length <= 0 || name.length <= 0) {
        RBNewAlert * alert = [[RBNewAlert alloc]init];
        alert.delegate = self;
        [alert showWithArray:@[@"请先完善信息",@"取消",@"去完善"] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
        UIButton * btn = [alert.bgView viewWithTag:1000];
        btn.backgroundColor = [Utils getUIColorWithHexString:SysColorbackgray];
        [btn setTitleColor:[Utils getUIColorWithHexString: @"777777"] forState:UIControlStateNormal];
        UIButton * btn1 = [alert.bgView viewWithTag:1001];
        btn1.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
        [btn1 setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
        return;
    }
    RBAdvertiserAddViewController *toview = [[RBAdvertiserAddViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)incomeQABtnAction:(UIButton *)sender {
}

- (void)tBtnAction:(UIButton *)sender {
    if (company_name.length <= 0 || name.length <= 0) {
        RBNewAlert * alert = [[RBNewAlert alloc]init];
        alert.delegate = self;
        [alert showWithArray:@[@"请先完善信息",@"取消",@"去完善"] IsCountDown:nil AndImageStr:nil AndBigTitle:nil];
        UIButton * btn = [alert.bgView viewWithTag:1000];
        btn.backgroundColor = [Utils getUIColorWithHexString:SysColorbackgray];
        [btn setTitleColor:[Utils getUIColorWithHexString: @"777777"] forState:UIControlStateNormal];
        UIButton * btn1 = [alert.bgView viewWithTag:1001];
        btn1.backgroundColor = [Utils getUIColorWithHexString:SysBackColorBlue];
        [btn1 setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
        return;
    }
    //
    [self.hudView show];
    Handler * handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBRecharge];
}
- (void)RBNewAlert:(RBNewAlert *)alertView clickedButtonAtIndex:(int)buttonIndex{
    if (buttonIndex == 1) {
        RBBrandMessageController*toview = [[RBBrandMessageController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
}
- (void)tBtn1Action:(UIButton *)sender {
    RBAdvertiserMyListViewController*toview = [[RBAdvertiserMyListViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)tBtn2Action:(UIButton *)sender {
    RBAdvertiserMoneyListViewController*toview = [[RBAdvertiserMoneyListViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)tBtn3Action:(UIButton *)sender {
    RBUserSettingQRCodeViewController *toview = [[RBUserSettingQRCodeViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [footerBtn setTitle:NSLocalizedString(@"R6045", @"发布悬赏活动") forState:UIControlStateNormal];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerBtn setBackgroundColor:[Utils getUIColorWithHexString:SysColorBlue]];
    [footerView addSubview:footerBtn];
    if ([LocalService getRBIsIncheck] == YES) {
        footerView.hidden = YES;
        footerBtn.hidden = YES;
    }
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIScrollView*editScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    [editScrollView setShowsHorizontalScrollIndicator:NO];
    [editScrollView setShowsVerticalScrollIndicator:NO];
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self.view addSubview:editScrollView];
    //
    UIImageView*incomeBgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 140.0)];
    incomeBgIV.image = [UIImage imageNamed:@"RBBrandMessage"];
    incomeBgIV.userInteractionEnabled = YES;
    [editScrollView addSubview:incomeBgIV];
    //
//    UIImageView*incomeTextBgIV = [[UIImageView alloc]initWithFrame:CGRectMake((incomeBgIV.width-45.0*1.7)/2.0, 30.0, 45.0*1.7, 28.0)];
//    incomeTextBgIV.image = [UIImage imageNamed:@"icon_income_text_bg.png"];
//    incomeTextBgIV.userInteractionEnabled = YES;
//    [incomeBgIV addSubview:incomeTextBgIV];
    headImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CellLeft, 30, 55, 55)];
    headImageView.image = PlaceHolderUserImage;
    headImageView.layer.cornerRadius = 55/2.0;
    headImageView.layer.masksToBounds = YES;
    [incomeBgIV addSubview:headImageView];
    //
    nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(headImageView.right + 15, 35, 100, 14)];
    nameLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightBold];
    nameLabel.textColor = [Utils getUIColorWithHexString:@"ffe0a3"];
    nameLabel.text = @"";
    [incomeBgIV addSubview:nameLabel];
    //
    companyLabel = [[UILabel alloc]initWithFrame:CGRectMake(nameLabel.left, nameLabel.bottom + 12, 150, 14)];
    companyLabel.textColor = [Utils getUIColorWithHexString:@"ffe1a5"];
    companyLabel.font = [UIFont systemFontOfSize:14];
    companyLabel.text = @"";
    [incomeBgIV addSubview:companyLabel];
    //
    UIButton * editButton = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - CellLeft - 55, 32, 55*ScaleWidth, 18*ScaleWidth) title:@"修改资料" hlTitle:nil titleColor:[Utils getUIColorWithHexString:@"ebd7b4"] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    editButton.titleLabel.font = [UIFont systemFontOfSize:9];
    editButton.layer.borderWidth = 0.5;
    editButton.layer.borderColor = [Utils getUIColorWithHexString:@"ebd7b4"].CGColor;
    editButton.layer.cornerRadius = 9;
    editButton.layer.masksToBounds = YES;
    [editButton addTarget:self action:@selector(modifyData:) forControlEvents:UIControlEventTouchUpInside];
    [incomeBgIV addSubview:editButton];
    //
    UIImageView * backImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 117, ScreenWidth, 98)];
    backImage.image = [UIImage imageNamed:@"pic-score-card"];
    backImage.userInteractionEnabled = YES;
    [editScrollView addSubview:backImage];
    //
    brandLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 25, (backImage.width - 2*CellLeft) / 2 - 0.5, 20) text:@"" font:font_cu_cu_(20) textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:@"734b2b"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [backImage addSubview:brandLabel];
    //
    UILabel * brandAccount = [[UILabel alloc]initWithFrame:CGRectMake(0, brandLabel.bottom + 10, ScreenWidth/2 - 0.5, 13) text:@"品牌主账户" font:[UIFont systemFontOfSize:13] textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:SysColorBlack] backgroundColor:[UIColor clearColor] numberOfLines:1];
    brandAccount.centerX = brandLabel.centerX;
    [backImage addSubview:brandAccount];
    //
    scoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(backImage.width/2 + 0.5, brandLabel.top, (backImage.width - 2 * CellLeft)/2, brandLabel.height) text:@"" font:brandLabel.font textAlignment:NSTextAlignmentCenter textColor:brandLabel.textColor backgroundColor:[UIColor clearColor] numberOfLines:1];
    
    [backImage addSubview:scoreLabel];
    //
    UILabel * scoreBrandLabel = [[UILabel alloc]initWithFrame:CGRectMake(scoreLabel.left, brandAccount.top, brandAccount.width, brandAccount.height) text:@"品牌主积分" font:brandAccount.font textAlignment:NSTextAlignmentCenter textColor:brandAccount.textColor backgroundColor:[UIColor clearColor] numberOfLines:1];
    [scoreBrandLabel sizeToFit];
    scoreBrandLabel.frame = CGRectMake(backImage.width/2 + (backImage.width/2 - CellLeft - scoreBrandLabel.frame.size.width)/2 ,brandAccount.top, scoreBrandLabel.frame.size.width, 13);
    scoreBrandLabel.centerX = scoreLabel.centerX;
    [backImage addSubview:scoreBrandLabel];
    //
    UIButton * scoreButton = [[UIButton alloc]initWithFrame:CGRectMake(scoreBrandLabel.right + 7, scoreBrandLabel.top+1, scoreBrandLabel.frame.size.height - 2, scoreBrandLabel.frame.size.height-2) title:nil hlTitle:nil titleColor:nil hlTitleColor:nil backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"RBBrandRule"] hlImage:nil];
//    [scoreButton addTarget:self action:@selector(scoreDspAction:) forControlEvents:UIControlEventTouchUpInside];
    [backImage addSubview:scoreButton];
    //
    UIButton * scoreDspBtn = [[UIButton alloc]initWithFrame:CGRectMake(scoreBrandLabel.left, 0, scoreButton.right - scoreBrandLabel.left, scoreBrandLabel.bottom) title:nil hlTitle:nil titleColor:nil hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    [scoreDspBtn addTarget:self action:@selector(scoreDspAction:) forControlEvents:UIControlEventTouchUpInside];
    [backImage addSubview:scoreDspBtn];
    //
    UILabel * seprateLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth - 0.5)/2, 39, 0.5, 20) text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:@"bbbbbb"] numberOfLines:1];
    [backImage addSubview:seprateLabel];
    //
    UILabel*gapLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, backImage.bottom, ScreenWidth, 15)];
    gapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [editScrollView addSubview:gapLabel];
    //
    UIButton*tBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    tBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    tBtn.frame = CGRectMake(0, gapLabel.bottom, editScrollView.width, 50.0);
    [tBtn addTarget:self
                  action:@selector(tBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:tBtn];
    //
    UIImageView*tIV = [[UIImageView alloc]initWithFrame:CGRectMake(CellLeft, 16, 18.0, 18.0)];
    tIV.image = [UIImage imageNamed:@"icon_ark_money.png"];
    [tBtn addSubview:tIV];
    //
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft*2.0+18.0, 0, ScreenWidth, tBtn.height)];
    tLabel.text = NSLocalizedString(@"R6034", @"立即充值");
    tLabel.font = font_cu_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [tBtn addSubview:tLabel];
    //
    UILabel*arrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-CellLeft-13.0, (tBtn.height-13.0)/2.0, 13.0, 13.0)];
    arrowLabel.font = font_icon_(13.0);
    arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    arrowLabel.text = Icon_btn_right;
    [tBtn addSubview:arrowLabel];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, tBtn.height-0.5, ScreenWidth, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [tBtn addSubview:lineLabel];
    //
    UILabel*gapLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, tBtn.bottom, ScreenWidth, CellBottom)];
    gapLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [editScrollView addSubview:gapLabel1];
    //
    UIButton*tBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    tBtn1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    tBtn1.frame = CGRectMake(0, gapLabel1.bottom, tBtn.width, tBtn.height);
    [tBtn1 addTarget:self
             action:@selector(tBtn1Action:)
   forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:tBtn1];
    //
    UIImageView*tIV1 = [[UIImageView alloc]initWithFrame:tIV.frame];
    tIV1.image = [UIImage imageNamed:@"icon_ark_list.png"];
    [tBtn1 addSubview:tIV1];
    //
    UILabel*tLabel1 = [[UILabel alloc] initWithFrame:tLabel.frame];
    tLabel1.text = NSLocalizedString(@"R6043", @"我发布的活动");
    tLabel1.font = font_cu_15;
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [tBtn1 addSubview:tLabel1];
    //
    UILabel*arrowLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-CellLeft-13.0, (tBtn.height-13.0)/2.0, 13.0, 13.0)];
    arrowLabel1.font = font_icon_(13.0);
    arrowLabel1.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    arrowLabel1.text = Icon_btn_right;
    [tBtn1 addSubview:arrowLabel1];
    //
    UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, tBtn.height-0.5, ScreenWidth, 0.5)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [tBtn1 addSubview:lineLabel1];
    //
    UIButton*tBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    tBtn2.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    tBtn2.frame = CGRectMake(0, tBtn1.bottom, tBtn.width, tBtn.height);
    [tBtn2 addTarget:self
              action:@selector(tBtn2Action:)
    forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:tBtn2];
    //
    UIImageView*tIV2 = [[UIImageView alloc]initWithFrame:tIV.frame];
    tIV2.image = [UIImage imageNamed:@"icon_ark_account.png"];
    [tBtn2 addSubview:tIV2];
    //
    UILabel*tLabel2 = [[UILabel alloc] initWithFrame:tLabel.frame];
    tLabel2.text = NSLocalizedString(@"R6202", @"我的品牌主账单");
    tLabel2.font = font_cu_15;
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [tBtn2 addSubview:tLabel2];
    //
    UILabel*arrowLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-CellLeft-13.0, (tBtn.height-13.0)/2.0, 13.0, 13.0)];
    arrowLabel2.font = font_icon_(13.0);
    arrowLabel2.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    arrowLabel2.text = Icon_btn_right;
    [tBtn2 addSubview:arrowLabel2];
    //
    UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, tBtn.height-0.5, ScreenWidth, 0.5)];
    lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [tBtn2 addSubview:lineLabel2];
    //
    UIButton*tBtn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    tBtn3.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    tBtn3.frame = CGRectMake(0, tBtn2.bottom, tBtn.width, tBtn.height);
    [tBtn3 addTarget:self
              action:@selector(tBtn3Action:)
    forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:tBtn3];
    //
    UIImageView*tIV3 = [[UIImageView alloc]initWithFrame:tIV.frame];
    tIV3.image = [UIImage imageNamed:@"icon_ark_add.png"];
    [tBtn3 addSubview:tIV3];
    //
    UILabel*tLabel3 = [[UILabel alloc] initWithFrame:tLabel.frame];
    tLabel3.text = NSLocalizedString(@"R5213", @"扫一扫登录官网");
    tLabel3.font = font_cu_15;
    tLabel3.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [tBtn3 addSubview:tLabel3];
    //
    UILabel*arrowLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-CellLeft-13.0, (tBtn.height-13.0)/2.0, 13.0, 13.0)];
    arrowLabel3.font = font_icon_(13.0);
    arrowLabel3.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    arrowLabel3.text = Icon_btn_right;
    [tBtn3 addSubview:arrowLabel3];
    //
    UILabel*lineLabel3 = [[UILabel alloc]initWithFrame:CGRectMake(0, tBtn.height-0.5, ScreenWidth, 0.5)];
    lineLabel3.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [tBtn3 addSubview:lineLabel3];
    //
    [editScrollView setContentSize:CGSizeMake(editScrollView.width, tBtn3.bottom)];
}
- (void)scoreDspAction:(id)sender{
    RBNewAlert * alert = [[RBNewAlert alloc]init];
    alert.delegate = self;
    NSString * moneyStr = [NSString stringWithFormat:@"%.1f",brand_credit.floatValue/10];
    NSString * dspStr = [NSString stringWithFormat:@"您当前积分%@，使用\n积分可抵扣%@元",brand_credit,moneyStr];
    NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:dspStr];
    NSRange range1 = [dspStr rangeOfString:brand_credit];
    NSRange range2 = [dspStr rangeOfString:moneyStr];
    [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorBlue] range:range1];
    [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorBlue] range:range2];
    [attStr addAttribute:NSFontAttributeName value:font_cu_(15) range:NSMakeRange(0, dspStr.length)];
    attStr.lineSpacing = 10;
    //
    NSString * timeStr = [NSString stringWithFormat:@"积分有效期至:%@",dateStr];
    if (dateStr.length == 0) {
        timeStr = @"";
    }
    NSMutableArray * array = [[NSMutableArray alloc]init];
    [array addObject:attStr];
    [array addObject:timeStr];
    [alert showWithArray:array andBigTitle:@"积分规则"];
}
- (void)modifyData:(id)sender{
    RBBrandMessageController * messageVC = [[RBBrandMessageController alloc]init];
    messageVC.hidesBottomBarWhenPushed = YES;
    messageVC.dict = dict;
    [self.navigationController pushViewController:messageVC animated:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"kol_brand"]) {
        brand_amount = [JsonService checkJson:[jsonObject objectForKey:@"brand_amount"]];
        name = [JsonService checkJson:[jsonObject objectForKey:@"name"]];
        avatar_url = [JsonService checkJson:[jsonObject objectForKey:@"avatar_url"]];
        company_name = [JsonService checkJson:[jsonObject objectForKey:@"campany_name"]];
        brand_credit = [JsonService checkJson:[jsonObject objectForKey:@"brand_credit"]];
        dateStr = [JsonService checkJson:[jsonObject objectForKey:@"brand_credit_expired_at"]];
        [dict setValue:name forKey:@"name"];
        [dict setValue:avatar_url forKey:@"avatar_url"];
        [dict setValue:company_name forKey:@"company_name"];
        [dict setValue:[JsonService checkJson:[jsonObject objectForKey:@"url"]] forKey:@"url"];
        [dict setValue:[JsonService checkJson:[jsonObject objectForKey:@"description"]] forKey:@"description"];
        
//        incomeLabel.text = [Utils getNSStringQian:[Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%@",brand_amount]]];
        brandLabel.text = [Utils getNSStringQian:[Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%@",brand_amount]]];
        scoreLabel.text = [NSString stringWithFormat:@"%@",brand_credit];
        [headImageView sd_setImageWithURL:[NSURL URLWithString:avatar_url] placeholderImage:PlaceHolderUserImage];
        nameLabel.text = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"name"]];
        [nameLabel sizeToFit];
        nameLabel.frame = CGRectMake(headImageView.right + 15, 35, nameLabel.frame.size.width, nameLabel.frame.size.height);
        companyLabel.text = [NSString stringWithFormat:@"%@",company_name];
        [companyLabel sizeToFit];
        companyLabel.frame = CGRectMake(nameLabel.left, nameLabel.bottom + 12, companyLabel.frame.size.width, companyLabel.frame.size.height);
        
    }
    if ([sender isEqualToString:@"promotions"]) {
        [self.hudView dismiss];
        if ([[jsonObject objectForKey:@"promotion"] isEqual:[NSNull null]]) {
            RBAdvertiserPayMoneyController * toview = [[RBAdvertiserPayMoneyController alloc]init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }else{
            entity = [JsonService getRBPromoteEntity:[jsonObject objectForKey:@"promotion"]];
            RBAdvertiserRechargeViewController*toview = [[RBAdvertiserRechargeViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            toview.entity = entity;
            [self.navigationController pushViewController:toview animated:YES];
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
