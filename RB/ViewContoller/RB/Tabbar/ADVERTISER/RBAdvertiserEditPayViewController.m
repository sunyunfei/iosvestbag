//
//  RBAdvertiserEditPayViewController.m
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserEditPayViewController.h"

@interface RBAdvertiserEditPayViewController () {
    UILabel*tLabel2;
    UISwitch*labelSwitch;
    UISwitch*scoreSwitch;
}
@end

@implementation RBAdvertiserEditPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    //
    [self loadEditView];
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
    //
    [TalkingData trackPageBegin:@"advertiser-add-pay"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageEnd:@"advertiser-add-pay"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    for (UIViewController *temp in self.navigationController.viewControllers){
        if ([temp isKindOfClass:[RBAdvertiserEditViewController class]]) {
            RBAdvertiserEditViewController*toview = (RBAdvertiserEditViewController*)temp;
            toview.adCampaignEntity = self.adCampaignEntity;
            [self.navigationController popToViewController:toview animated:YES];
            return;
        }
        if ([temp isKindOfClass:[RBAdvertiserEditAddViewController class]]) {
            RBAdvertiserEditAddViewController*toview = (RBAdvertiserEditAddViewController*)temp;
            toview.adCampaignEntity = self.adCampaignEntity;
            [self.navigationController popToViewController:toview animated:YES];
            return;
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    BOOL isScoreOn = [scoreSwitch isOn];
    NSString * isUseCredit = @"0";
    if (isScoreOn) {
        isUseCredit = @"1";
    }
    [self.hudView showOverlay];
    // RB-发布活动-使用钱包抵扣
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityAddPayWithintegralID:self.adCampaignEntity.iid andUse_credit:isUseCredit];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R5153",@"立即支付") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

- (void)loadEditView {
    UIScrollView*editScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-45.0)];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:editScrollView];
    //
    UIImageView*bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth*9.0/16.0)];
    [bgIV sd_setImageWithURL:[NSURL URLWithString:self.adCampaignEntity.img_url] placeholderImage:PlaceHolderImage];
    [editScrollView addSubview:bgIV];
    //
    UILabel*bgLabel = [[UILabel alloc]initWithFrame:bgIV.bounds];
    bgLabel.backgroundColor = SysColorCover;
    [bgIV addSubview:bgLabel];
    //
    self.navTitle = NSLocalizedString(@"R6007", @"发布活动");
    self.navLeftLabel.textColor = [UIColor whiteColor];
    self.navTitleLabel.textColor = [UIColor whiteColor];
    self.navLineLabel.hidden = YES;
    self.navView.backgroundColor = [UIColor clearColor];
    //
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, bgIV.bottom+16.0, editScrollView.width, 18)];
    titleLabel.font = font_cu_15;
    titleLabel.text = self.adCampaignEntity.name;
    titleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:titleLabel];
    //
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, titleLabel.bottom+6.0, editScrollView.width, 13)];
    subTitleLabel.font = font_11;
    subTitleLabel.text = @"";
    subTitleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:subTitleLabel];
    [Utils getUILabel:subTitleLabel withSpacing:6.0];
    //
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subTitleLabel.bottom+6.0, editScrollView.width, 13)];
    timeLabel.font = font_11;
    timeLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:timeLabel];
    NSString *sDate = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.start_time]componentsSeparatedByString:@"T"][0];
    sDate = [sDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *sDate1 = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.start_time]componentsSeparatedByString:@"T"][1];
    sDate1 = [[NSString stringWithFormat:@"%@",sDate1]componentsSeparatedByString:@"+"][0];
    NSString *eDate = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.deadline]componentsSeparatedByString:@"T"][0];
    eDate = [eDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *eDate1 = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.deadline]componentsSeparatedByString:@"T"][1];
    eDate1 = [[NSString stringWithFormat:@"%@",eDate1]componentsSeparatedByString:@"+"][0];
    timeLabel.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",sDate,sDate1,eDate,eDate1];
    CGSize timeLabelSize = [Utils getUIFontSizeFitW:timeLabel];
    timeLabel.width = timeLabelSize.width+8.0;
    timeLabel.left = (ScreenWidth-timeLabel.width-8.0)/2.0;
    //
    UITextView*contentTV = [[UITextView alloc]initWithFrame:CGRectMake(CellLeft, timeLabel.bottom+10.0, editScrollView.width-CellLeft*2.0, 70.0)];
    contentTV.font = font_13;
    contentTV.text = self.adCampaignEntity.idescription;
    contentTV.text = [contentTV.text stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    contentTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    contentTV.editable = NO;
    contentTV.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:contentTV];
    CGSize contentTVSize = [Utils getUITextViewSizeFitH:contentTV withLineSpacing:6.0];
    if (contentTVSize.height+6.0*2.0 < contentTV.height) {
        contentTV.height = contentTVSize.height+6.0*2.0;
    }
    //
    UILabel*tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, contentTV.bottom+15.0, editScrollView.width, 18.0)];
    tagLabel.font = font_cu_15;
    tagLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tagLabel.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:tagLabel];
    NSString*per_action_budget = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:self.adCampaignEntity.per_action_budget]];
    NSString*per_budget_type = [NSString stringWithFormat:@"%@",self.adCampaignEntity.per_budget_type];
    if ([per_budget_type isEqualToString:@"click"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2014",@"点击"),per_action_budget];
    } else if ([per_budget_type isEqualToString:@"cpa"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2015",@"效果"),per_action_budget];
    } else if ([per_budget_type isEqualToString:@"cpt"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2301",@"任务"),per_action_budget];
    } else if ([per_budget_type isEqualToString:@"simple_cpi"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2200",@"下载"),per_action_budget];
    } else {
        tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2017",@"转发"),per_action_budget];
    }
    //
    UILabel *lineGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, tagLabel.bottom+15.0, editScrollView.width, CellBottom)];
    lineGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [editScrollView addSubview:lineGapLabel];
    //
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineGapLabel.bottom, ScreenWidth, 50.0)];
    tLabel.text = NSLocalizedString(@"R6015", @"活动总预算");
    tLabel.font = font_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel];
    //
    UILabel*cLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, tLabel.top, ScreenWidth-CellLeft, 50.0)];
    cLabel.font = font_cu_15;
    cLabel.textAlignment = NSTextAlignmentRight;
    cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:cLabel];
    NSString*budget = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:self.adCampaignEntity.budget]];
    cLabel.text = [NSString stringWithFormat:@"￥%@",budget];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel.bottom-0.5, ScreenWidth, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:lineLabel];
    //
    UILabel * tLabel3 = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, lineLabel.bottom, ScreenWidth, tLabel.height) text:@"积分抵扣" font:font_(15) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorBlack] backgroundColor:[UIColor clearColor] numberOfLines:1];
    NSString * floatStr;
    if (self.adCampaignEntity.budget.floatValue - self.adCampaignEntity.kol_credit.floatValue/10.0 <= 0) {
        floatStr = [NSString stringWithFormat:@"￥%.1f",self.adCampaignEntity.budget.floatValue];
    }else{
        floatStr = [NSString stringWithFormat:@"￥%.1f",self.adCampaignEntity.kol_credit.floatValue/10.0];
    }
    NSString * scoreStr = [NSString stringWithFormat:@"积分抵扣 %@",floatStr];
    NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:scoreStr];
    NSRange range = [scoreStr rangeOfString:floatStr];
    [attStr addAttribute:NSForegroundColorAttributeName value:[Utils getUIColorWithHexString:SysColorYellow] range:range];
    [attStr addAttribute:NSFontAttributeName value:font_cu_(15) range:range];
    tLabel3.attributedText = attStr;
    [editScrollView addSubview:tLabel3];
    //
    scoreSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(ScreenWidth-50.0-CellLeft, tLabel3.top + (tLabel3.height - 30.0)/2.0, 50, 30)];
    scoreSwitch.onTintColor = [Utils getUIColorWithHexString:SysColorBlue];
    scoreSwitch.tintColor = [Utils getUIColorWithHexString:SysColorBlue];
    scoreSwitch.on = YES;
    [scoreSwitch addTarget:self action:@selector(scoreSwitchAction:) forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:scoreSwitch];
    //
    UILabel * seperateLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel3.bottom - 0.5, ScreenWidth, 0.5)];
    seperateLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:seperateLabel];
    //
    tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(0, seperateLabel.bottom, ScreenWidth-CellLeft, 50.0)];
    tLabel2.font = font_cu_15;
    tLabel2.textAlignment = NSTextAlignmentRight;
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel2];
    //
    
    
    //
    if(self.adCampaignEntity.budget.floatValue-self.adCampaignEntity.kol_credit.floatValue/10.0<=0){
        tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),@"0"];
    } else {
        NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue-self.adCampaignEntity.kol_credit.floatValue/10.0]];
        tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
    }
    //
    UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel2.bottom-0.5, ScreenWidth, 0.5)];
    lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:lineLabel2];
    //
    UILabel *lineGapLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, lineLabel2.bottom, editScrollView.width, editScrollView.height-lineLabel2.bottom)];
    lineGapLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [editScrollView addSubview:lineGapLabel1];
    //
    [editScrollView setContentSize:CGSizeMake(editScrollView.width, lineLabel2.bottom)];
}

#pragma mark - RBSwitch Delegate
//- (void)switchAction:(UISwitch *)sender {
//    BOOL isButtonOn = [sender isOn];
//    BOOL isScoreOn = [scoreSwitch isOn];
//    if (isButtonOn && isScoreOn) {
//        if(self.adCampaignEntity.budget.floatValue - self.adCampaignEntity.kol_amount.floatValue - self.adCampaignEntity.kol_credit.floatValue<=0){
//            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),@"0"];
//        } else {
//            NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue-self.adCampaignEntity.kol_amount.floatValue - self.adCampaignEntity.kol_credit.floatValue]];
//            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
//        }
//    }else if (isButtonOn && !isScoreOn){
//        if(self.adCampaignEntity.budget.floatValue - self.adCampaignEntity.kol_amount.floatValue<=0){
//            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),@"0"];
//        } else {
//            NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue-self.adCampaignEntity.kol_amount.floatValue]];
//            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
//        }
//    }else if (!isButtonOn && isScoreOn){
//        if(self.adCampaignEntity.budget.floatValue - self.adCampaignEntity.kol_credit.floatValue<=0){
//            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),@"0"];
//        } else {
//            NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue - self.adCampaignEntity.kol_credit.floatValue]];
//            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
//        }
//    }else if (!isButtonOn && !isScoreOn){
//        if(self.adCampaignEntity.budget.floatValue - self.adCampaignEntity.kol_credit.floatValue - self.adCampaignEntity.kol_amount.floatValue<=0){
//            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),@"0"];
//        } else {
//            NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue]];
//            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
//        }
//    }
////    if (isButtonOn) {
////        if(self.adCampaignEntity.budget.floatValue-self.adCampaignEntity.kol_amount.floatValue<=0){
////            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),@"0"];
////        } else {
////            NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue-self.adCampaignEntity.kol_amount.floatValue]];
////            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
////        }
////    }else {
////        NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue]];
////        tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
////    }
//}
- (void)scoreSwitchAction:(UISwitch *)sender{
    BOOL isScoreOn = [sender isOn];
    if (isScoreOn) {
        if(self.adCampaignEntity.budget.floatValue  - self.adCampaignEntity.kol_credit.floatValue/10 <= 0){
            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),@"0"];
        } else {
            NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue - self.adCampaignEntity.kol_credit.floatValue/10]];
            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
        }
    }else{
        NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue]];
        tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
    }
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"pay_by_credit"]) {
        RBAdvertiserCampaignPayEntity*entity = [JsonService getRBAdvertiserCampaignPayEntity:jsonObject];
        NSString*status = [NSString stringWithFormat:@"%@",entity.status];
        if ([status isEqualToString:@"unpay"]) {
            RBAdvertiserPayDetailViewController*toview = [[RBAdvertiserPayDetailViewController alloc] init];
            toview.adCampaignPayEntity = entity;
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        } else if ([status isEqualToString:@"unexecute"]) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R6028", @"订单支付成功")];
            dispatch_after(
                           dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(), ^{
                               RBAdvertiserPaySuccessedViewController*toview = [[RBAdvertiserPaySuccessedViewController alloc] init];
                               toview.hidesBottomBarWhenPushed = YES;
                               [self.navigationController pushViewController:toview animated:YES];
                           });
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
@end

