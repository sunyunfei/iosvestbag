//
//  RBAdvertiserMyListPayViewController.m
//  RB
//
//  Created by AngusNi on 6/14/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserMyListPayViewController.h"

@interface RBAdvertiserMyListPayViewController () {
    UILabel*tLabel2;
    UILabel*tagLabel;
    UILabel*tLabel4;
    UISwitch*labelSwitch;
}
@end

@implementation RBAdvertiserMyListPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    //
    [self loadFooterView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
    //
    [TalkingData trackPageBegin:@"advertiser-add-pay"];
    [self.hudView show];
    // RB-发布活动-活动详情
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityAddDetailWithId:self.adCampaignEntity.iid];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageEnd:@"advertiser-add-pay"];
}

#pragma mark - UIButton Delegate
- (void)navLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    NSString*status = [NSString stringWithFormat:@"%@",self.adCampaignEntity.status];
    if ([status isEqualToString:@"rejected"]) {
        RBAdvertiserEditAddViewController*toview = [[RBAdvertiserEditAddViewController alloc] init];
        toview.adCampaignEntity = self.adCampaignEntity;
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
    if ([status isEqualToString:@"unpay"]) {
        NSString*isvoucher = @"0";
//        if (self.adCampaignEntity.used_voucher.intValue==1) {
//            isvoucher = @"1";
//        } else {
            if ([labelSwitch isOn]==YES) {
                isvoucher = @"1";
            }
     //   }
        // RB-发布活动-使用积分抵扣
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
//        [handler getRBActivityAddPayByVoucherWithiid:self.adCampaignEntity.iid andUsed_voucher:isvoucher andUseCredit:isvoucher];
        [handler getRBActivityAddPayWithintegralID:self.adCampaignEntity.iid andUse_credit:isvoucher];
    }
}

- (void)editBtnAction:(UIButton *)sender {
    RBAdvertiserEditAddViewController*toview = [[RBAdvertiserEditAddViewController alloc] init];
    toview.adCampaignEntity = self.adCampaignEntity;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)rightBtnAction:(UIButton *)sender {
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    actionSheet.tag = 7000;
    [actionSheet showWithArray:@[NSLocalizedString(@"R6057", @"撤销此活动?"),NSLocalizedString(@"R1011", @"取消")]];
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if (actionSheet.tag == 7000) {
        if (buttonIndex == 0) {
            [self.hudView show];
            // RB-发布活动-撤销
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBActivityAddRevokeWithId:self.adCampaignEntity.iid];
        }
    }
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
    //
    NSString*status = [NSString stringWithFormat:@"%@",self.adCampaignEntity.status];
    footerBtn.enabled = NO;
    if ([status isEqualToString:@"unpay"]) {
        footerBtn.enabled = YES;
        [footerBtn setTitle:NSLocalizedString(@"R5153",@"立即支付") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    } else if ([status isEqualToString:@"unexecute"]) {
        [footerBtn setTitle:NSLocalizedString(@"R2040", @"审核中") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
    } else if ([status isEqualToString:@"rejected"]) {
        footerBtn.enabled = YES;
        [footerBtn setTitle:NSLocalizedString(@"R6076", @"审核未通过，立即修改") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorRed];
    } else if ([status isEqualToString:@"agreed"]) {
        [footerBtn setTitle:NSLocalizedString(@"R6075", @"等待活动开始") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
    } else if ([status isEqualToString:@"executing"]) {
        [footerBtn setTitle:NSLocalizedString(@"R2004",@"进行中") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
    } else if ([status isEqualToString:@"executing"]) {
        [footerBtn setTitle:NSLocalizedString(@"R6074", @"活动已完成，正在结算") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
    } else if ([status isEqualToString:@"settled"]) {
        [footerBtn setTitle:NSLocalizedString(@"R6004", @"已结算") forState:UIControlStateNormal];
        footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
    }
}

- (void)loadEditView {
    UIScrollView*editScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-45.0)];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:editScrollView];
    //
    UIImageView*bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth*9.0/16.0)];
    [bgIV sd_setImageWithURL:[NSURL URLWithString:self.adCampaignEntity.img_url] placeholderImage:PlaceHolderImage];
    [editScrollView addSubview:bgIV];
    //
    UILabel*bgLabel = [[UILabel alloc]initWithFrame:bgIV.bounds];
    bgLabel.backgroundColor = SysColorCover;
    [bgIV addSubview:bgLabel];
    //
    UIView*scrollViewNavView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 64)];
    [editScrollView addSubview:scrollViewNavView];
    //
    UILabel *navCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.0, 20.0, self.view.width-100.0, 44)];
    navCenterLabel.text = NSLocalizedString(@"R6007", @"发布活动");
    navCenterLabel.font = font_cu_17;
    navCenterLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    navCenterLabel.textAlignment = NSTextAlignmentCenter;
    navCenterLabel.userInteractionEnabled = YES;
    [scrollViewNavView addSubview:navCenterLabel];
    //
    UILabel*navLeftLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 20.0+(44.0-20.0)/2.0, 20.0, 20.0)];
    navLeftLabel.font = font_icon_(20.0);
    navLeftLabel.text = Icon_bar_back;
    navLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    [scrollViewNavView addSubview:navLeftLabel];
    //
    UIButton*navLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navLeftBtn.frame = CGRectMake(0, 20.0, 44.0, 44.0);
    [navLeftBtn addTarget:self
                   action:@selector(navLeftBtnAction)
         forControlEvents:UIControlEventTouchUpInside];
    [scrollViewNavView addSubview:navLeftBtn];
    //
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize labelSize = [NSLocalizedString(@"R6058", @"撤销") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_15,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
    UIButton*rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(scrollViewNavView.width-labelSize.width-CellLeft, 20.0, labelSize.width, 44.0);
    rightBtn.titleLabel.font = font_15;
    [rightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [rightBtn setTitle:NSLocalizedString(@"R6058", @"撤销") forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [scrollViewNavView addSubview:rightBtn];
    //
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, bgIV.bottom+16.0, editScrollView.width, 18)];
    titleLabel.font = font_cu_15;
    titleLabel.text = self.adCampaignEntity.name;
    titleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:titleLabel];
    //
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, titleLabel.bottom+6.0, editScrollView.width, 13)];
    timeLabel.font = font_11;
    timeLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:timeLabel];
    NSString *sDate = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.start_time]componentsSeparatedByString:@"T"][0];
    sDate = [sDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *sDate1 = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.start_time]componentsSeparatedByString:@"T"][1];
    sDate1 = [[NSString stringWithFormat:@"%@",sDate1]componentsSeparatedByString:@"+"][0];
    NSString *eDate = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.deadline]componentsSeparatedByString:@"T"][0];
    eDate = [eDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *eDate1 = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.deadline]componentsSeparatedByString:@"T"][1];
    eDate1 = [[NSString stringWithFormat:@"%@",eDate1]componentsSeparatedByString:@"+"][0];
    timeLabel.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",sDate,sDate1,eDate,eDate1];
    CGSize timeLabelSize = [Utils getUIFontSizeFitW:timeLabel];
    timeLabel.width = timeLabelSize.width+8.0;
    timeLabel.left = (ScreenWidth-timeLabel.width-8.0)/2.0;
    //
    UITextView*contentTV = [[UITextView alloc]initWithFrame:CGRectMake(CellLeft, timeLabel.bottom+10.0, editScrollView.width-CellLeft*2.0, 70.0)];
    contentTV.font = font_13;
    contentTV.text = self.adCampaignEntity.idescription;
    contentTV.text = [contentTV.text stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    contentTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    contentTV.editable = NO;
    contentTV.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:contentTV];
    CGSize contentTVSize = [Utils getUITextViewSizeFitH:contentTV withLineSpacing:6.0];
    if (contentTVSize.height+6.0*2.0 < contentTV.height) {
        contentTV.height = contentTVSize.height+6.0*2.0;
    }
    //
    tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, contentTV.bottom+15.0, editScrollView.width, 18.0)];
    tagLabel.font = font_cu_15;
    tagLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tagLabel.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:tagLabel];
    //
    UILabel *lineGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, tagLabel.bottom+15.0, editScrollView.width, CellBottom)];
    lineGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [editScrollView addSubview:lineGapLabel];
    //
    UIButton*editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    editBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    editBtn.frame = CGRectMake(0, lineGapLabel.bottom, editScrollView.width, 50.0);
    [editBtn addTarget:self
             action:@selector(editBtnAction:)
   forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:editBtn];
    //
    UILabel*tLabel0 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth, editBtn.height)];
    tLabel0.text = NSLocalizedString(@"R6059", @"修改活动信息");
    tLabel0.font = font_15;
    tLabel0.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editBtn addSubview:tLabel0];
    //
    UILabel*arrowLabel0 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-CellLeft-13.0, (editBtn.height-13.0)/2.0, 13.0, 13.0)];
    arrowLabel0.font = font_icon_(13.0);
    arrowLabel0.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    arrowLabel0.text = Icon_btn_right;
    [editBtn addSubview:arrowLabel0];
    //
    UILabel*lineLabel0 = [[UILabel alloc]initWithFrame:CGRectMake(0, editBtn.height-0.5, ScreenWidth, 0.5)];
    lineLabel0.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editBtn addSubview:lineLabel0];
    //
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, editBtn.bottom, ScreenWidth, 50.0)];
    tLabel.text = NSLocalizedString(@"R6015", @"活动总预算");
    tLabel.font = font_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel];
    //
    UILabel*cLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, tLabel.top, ScreenWidth-CellLeft, 50.0)];
    cLabel.font = font_cu_15;
    cLabel.textAlignment = NSTextAlignmentRight;
    cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:cLabel];
    NSString*budget = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:self.adCampaignEntity.budget]];
    cLabel.text = [NSString stringWithFormat:@"￥%@",budget];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel.bottom-0.5, ScreenWidth, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:lineLabel];
    //
    NSString*status = [NSString stringWithFormat:@"%@",self.adCampaignEntity.status];
    if ([status isEqualToString:@"rejected"]) {
        UILabel *lineGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, lineLabel.bottom, editScrollView.width, CellBottom)];
        lineGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        [editScrollView addSubview:lineGapLabel];
        //
        UILabel*tLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(0, lineGapLabel.bottom, lineGapLabel.width, 50.0)];
        tLabel3.font = font_cu_15;
        tLabel3.text = @"审核未通过原因";
        tLabel3.textColor = [Utils getUIColorWithHexString:SysColorRed];
        [editScrollView addSubview:tLabel3];
        //
        UILabel*lineLabel3 = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel3.height-0.5, ScreenWidth, 0.5)];
        lineLabel3.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [tLabel3 addSubview:lineLabel3];
        //
        tLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, tLabel3.bottom+10.0, ScreenWidth-2*CellLeft, 50.0)];
        tLabel4.numberOfLines = 0;
        tLabel4.font = font_15;
        tLabel4.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [editScrollView addSubview:tLabel4];
        NSString*reason = @"";
        for (int i=0; i<[self.adCampaignEntity.invalid_reasons count]; i++) {
            if (i==0) {
                reason = [NSString stringWithFormat:@"%@",self.adCampaignEntity.invalid_reasons[i]];
            } else {
                reason = [NSString stringWithFormat:@"%@\n%@",reason,self.adCampaignEntity.invalid_reasons[i]];
            }
        }
        tLabel4.text = [NSString stringWithFormat:@"%@",reason];
        CGSize tLabel4Size = [Utils getUIFontSizeFitH:tLabel4];
        tLabel4.height = tLabel4Size.height;
        //
        UILabel *lineGapLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, tLabel4.bottom+10.0, editScrollView.width, editScrollView.height-tLabel4.bottom-10.0)];
        lineGapLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        [editScrollView addSubview:lineGapLabel1];
        [editScrollView setContentSize:CGSizeMake(editScrollView.width, tLabel4.bottom+10.0)];
    } else if([status isEqualToString:@"unpay"]){
        UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineLabel.bottom, ScreenWidth, tLabel.height)];
        tLabel1.text = @"积分抵扣";
        tLabel1.font = font_15;
        tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [editScrollView addSubview:tLabel1];
        CGSize tLabel1Size = [Utils getUIFontSizeFitW:tLabel1];
        tLabel1.width = tLabel1Size.width;
        //
        UILabel*cLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(tLabel1.right+5.0, tLabel1.top, ScreenWidth, tLabel1.height)];
        cLabel1.font = font_cu_15;
        cLabel1.textColor = [Utils getUIColorWithHexString:SysColorYellow];
        [editScrollView addSubview:cLabel1];
//        if(self.adCampaignEntity.used_voucher.intValue==1){
//            NSString*voucher_amount = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:self.adCampaignEntity.voucher_amount]];
//            cLabel1.text = [NSString stringWithFormat:@"￥%@",voucher_amount];
//        } else {
         //   if(self.adCampaignEntity.kol_credit.floatValue/10>self.adCampaignEntity.budget.floatValue){
//                cLabel1.text = [NSString stringWithFormat:@"￥%@(%@:￥%@)",budget,NSLocalizedString(@"R6077", @"余额"),kol_amount];
        //    } else {
         //   }
            labelSwitch =
            [[UISwitch alloc] initWithFrame:CGRectMake(ScreenWidth-50.0-CellLeft, tLabel1.top+(tLabel1.height-30.0)/2.0, 50, 30)];
            labelSwitch.onTintColor = [Utils getUIColorWithHexString:SysColorBlue];
            labelSwitch.tintColor = [Utils getUIColorWithHexString:SysColorBlue];
            labelSwitch.on = YES;
            [labelSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventTouchUpInside];
            [editScrollView addSubview:labelSwitch];
     //   }
        //
        UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel1.bottom-0.5, ScreenWidth, 0.5)];
        lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [editScrollView addSubview:lineLabel1];
        //
        tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(0, lineLabel1.bottom, ScreenWidth-CellLeft, 50.0)];
        tLabel2.font = font_cu_15;
        tLabel2.textAlignment = NSTextAlignmentRight;
        tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [editScrollView addSubview:tLabel2];
        //
//        if(self.adCampaignEntity.used_voucher.intValue==1){
//        NSString*need_pay_amount = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%@",self.adCampaignEntity.need_pay_amount]];
//        tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),need_pay_amount];
//        } else {
            if(self.adCampaignEntity.budget.floatValue-self.adCampaignEntity.kol_credit.floatValue/10<=0){
                tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),@"0"];
                cLabel1.text = [NSString stringWithFormat:@" ￥%@",self.adCampaignEntity.budget];
            } else {
                NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue-self.adCampaignEntity.kol_credit.floatValue/10]];
                tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
                cLabel1.text = [NSString stringWithFormat:@" ￥%.1f",self.adCampaignEntity.kol_credit.floatValue/10];
            }
  //      }
        //
        UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel2.bottom-0.5, ScreenWidth, 0.5)];
        lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [editScrollView addSubview:lineLabel2];
        //
        UILabel *lineGapLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, lineLabel2.bottom, editScrollView.width, editScrollView.height-lineLabel2.bottom)];
        lineGapLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        [editScrollView addSubview:lineGapLabel1];
        [editScrollView setContentSize:CGSizeMake(editScrollView.width, lineLabel2.bottom)];
    }else{
        UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineLabel.bottom, ScreenWidth, tLabel.height)];
        tLabel1.text = @"积分抵扣";
        tLabel1.font = font_15;
        tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [editScrollView addSubview:tLabel1];
        CGSize tLabel1Size = [Utils getUIFontSizeFitW:tLabel1];
        tLabel1.width = tLabel1Size.width;
        //
        UILabel*cLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(tLabel1.right+5.0, tLabel1.top, ScreenWidth - CellLeft - tLabel1.right-5.0, tLabel1.height)];
        cLabel1.font = font_cu_15;
        cLabel1.textColor = [Utils getUIColorWithHexString:SysColorYellow];
        cLabel1.textAlignment = NSTextAlignmentRight;
        [editScrollView addSubview:cLabel1];
        //
        if (self.adCampaignEntity.used_credits.length == 0) {
            cLabel1.text = [NSString stringWithFormat:@"￥%@",@"0"];
        }else{
            NSString * useCredit = [NSString stringWithFormat:@"%f",fabsf([self.adCampaignEntity.used_credits floatValue]/10)];
            cLabel1.text = [NSString stringWithFormat:@" ￥%@",useCredit];
        }
        //
        UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel1.bottom-0.5, ScreenWidth, 0.5)];
        lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [editScrollView addSubview:lineLabel1];
        //
        tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(0, lineLabel1.bottom, ScreenWidth-CellLeft, 50.0)];
        tLabel2.font = font_cu_15;
        tLabel2.textAlignment = NSTextAlignmentRight;
        tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [editScrollView addSubview:tLabel2];
        //
        //        if(self.adCampaignEntity.used_voucher.intValue==1){
        //        NSString*need_pay_amount = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%@",self.adCampaignEntity.need_pay_amount]];
        //        tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),need_pay_amount];
        //        } else {
//        if(self.adCampaignEntity.budget.floatValue-self.adCampaignEntity.kol_credit.floatValue/10<=0){
//            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),@"0"];
//        } else {
        NSString * total;
        if (self.adCampaignEntity.used_credits.length == 0) {
            total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue]];
        }else{
            total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue - fabsf([self.adCampaignEntity.used_credits floatValue]/10)]];
        }
        
            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
 //       }
        //      }
        //
        UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel2.bottom-0.5, ScreenWidth, 0.5)];
        lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [editScrollView addSubview:lineLabel2];
        //
        UILabel *lineGapLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, lineLabel2.bottom, editScrollView.width, editScrollView.height-lineLabel2.bottom)];
        lineGapLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        [editScrollView addSubview:lineGapLabel1];
        [editScrollView setContentSize:CGSizeMake(editScrollView.width, lineLabel2.bottom)];
    }
}

#pragma mark - RBSwitch Delegate
- (void)switchAction:(UISwitch *)sender {
    BOOL isButtonOn = [sender isOn];
    if (isButtonOn) {
        if(self.adCampaignEntity.budget.floatValue-self.adCampaignEntity.kol_credit.floatValue/10.0<=0){
            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),@"0"];
        } else {
            NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue-self.adCampaignEntity.kol_credit.floatValue/10.0]];
            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
        }
    }else {
        NSString*total = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%f",self.adCampaignEntity.budget.floatValue]];
        tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6026", @"合计"),total];
    }
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"pay_by_credit"]) {
        RBAdvertiserCampaignPayEntity*entity = [JsonService getRBAdvertiserCampaignPayEntity:jsonObject];
        NSString*status = [NSString stringWithFormat:@"%@",entity.status];
        if ([status isEqualToString:@"unpay"]) {
            RBAdvertiserPayDetailViewController*toview = [[RBAdvertiserPayDetailViewController alloc] init];
            toview.adCampaignPayEntity = entity;
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        } else if ([status isEqualToString:@"unexecute"]) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R6028", @"订单支付成功")];
            dispatch_after(
                           dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(), ^{
                               RBAdvertiserPaySuccessedViewController*toview = [[RBAdvertiserPaySuccessedViewController alloc] init];
                               toview.hidesBottomBarWhenPushed = YES;
                               [self.navigationController pushViewController:toview animated:YES];
                           });
        }
    }
    if ([sender isEqualToString:@"pay_by_voucher"]) {
        RBAdvertiserCampaignPayEntity*entity = [JsonService getRBAdvertiserCampaignPayEntity:jsonObject];
        NSString*status = [NSString stringWithFormat:@"%@",entity.status];
        if ([status isEqualToString:@"unpay"]) {
            RBAdvertiserPayDetailViewController*toview = [[RBAdvertiserPayDetailViewController alloc] init];
            toview.adCampaignPayEntity = entity;
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        } else if ([status isEqualToString:@"unexecute"]) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R6028", @"订单支付成功")];
            dispatch_after(
                           dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(), ^{
                               RBAdvertiserPaySuccessedViewController*toview = [[RBAdvertiserPaySuccessedViewController alloc] init];
                               toview.hidesBottomBarWhenPushed = YES;
                               [self.navigationController pushViewController:toview animated:YES];
                           });
        }
    }
    
    if ([sender isEqualToString:@"kol_campaigns/detail"]) {
        [self.hudView dismiss];
        self.adCampaignEntity = [JsonService getRBAdvertiserCampaignEntity:jsonObject];
        [self loadEditView];
        NSString*per_action_budget = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:self.adCampaignEntity.per_action_budget]];
        NSString*per_budget_type = [NSString stringWithFormat:@"%@",self.adCampaignEntity.per_budget_type];
        if ([per_budget_type isEqualToString:@"click"]) {
            tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2014",@"点击"),per_action_budget];
        } else if ([per_budget_type isEqualToString:@"cpa"]) {
            tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2015",@"效果"),per_action_budget];
        } else if ([per_budget_type isEqualToString:@"cpt"]) {
            tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2301",@"任务"),per_action_budget];
        } else if ([per_budget_type isEqualToString:@"simple_cpi"]) {
            tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2200",@"下载"),per_action_budget];
        } else if ([per_budget_type isEqualToString:@"recruit"]) {
            tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2016",@"招募"),per_action_budget];
        } else if ([per_budget_type isEqualToString:@"invite"]) {
            tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2107",@"特邀"),per_action_budget];
        } else {
            tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2017",@"转发"),per_action_budget];
        }
    }
    
    if ([sender isEqualToString:@"kol_campaigns/revoke"]) {
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R6060", @"活动撤销成功")];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [self.navigationController popViewControllerAnimated:YES];
                       });
    }
    
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
@end

