//
//  RBAdvertiserEditViewController.m
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserEditViewController.h"
#import "RBPlatSelectViewController.h"
@interface RBAdvertiserEditViewController () {
    UIImageView*imageIV;
    RBInputView*inputView;
    RBInputView*inputView1;
    RBInputView*inputView2;
    RBKolEditView*inputViewplat;
    RBInputRightView*inputView3;
    RBInputRightView*inputView4;
    RBInputRightView*inputView5;
    RBInputRightView*inputView6;
    RBInputRightView*inputView7;
    RBInputRightView*inputView8;
    RBInputRightView*inputView9;
    RBKolEditView*inputView10;
    RBKolEditView*inputView11;
    //
    UIScrollView*editScrollView;
    UIButton*footerLeftBtn;
    UIButton*footerRightBtn;
    //
    UIScrollView*pageScrollView;
    RBAdvertiserSwitchView*switchView;
    //
    NSString * platType;
}
@end

@implementation RBAdvertiserEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R6007", @"发布活动");
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    switchView = [[RBAdvertiserSwitchView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 40.0)];
    switchView.delegate = self;
    [switchView showWithData:@[NSLocalizedString(@"R6103",@"智能发布"),NSLocalizedString(@"R6107",@"文章分析")] andSelected:0];
    [self.view addSubview:switchView];
    //
    pageScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, switchView.bottom, ScreenWidth, ScreenHeight-switchView.bottom)];
    pageScrollView.showsHorizontalScrollIndicator = NO;
    pageScrollView.showsVerticalScrollIndicator = NO;
    pageScrollView.userInteractionEnabled = YES;
    pageScrollView.bounces = YES;
    pageScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [pageScrollView setContentSize:CGSizeMake(ScreenWidth*2.0, pageScrollView.height)];
    pageScrollView.delegate = self;
    [self.view addSubview:pageScrollView];
    // 1
    [self loadEditView];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(inputResult)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [self inputResult];
    // 2
    [self loadNlpView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"advertiser-add"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"advertiser-add"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

#pragma mark- UIScrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView==pageScrollView){
        CGPoint offset = scrollView.contentOffset;
        int currentPage = offset.x / (self.view.bounds.size.width);
        [switchView showWithData:@[NSLocalizedString(@"R6103",@"智能发布"),NSLocalizedString(@"R6107",@"文章分析")] andSelected:currentPage];
        [self RBAdvertiserSwitchViewSelected:switchView andIndex:currentPage];
    }
}

#pragma mark- RBAdvertiserSwitchView Delegate
- (void)RBAdvertiserSwitchViewSelected:(RBAdvertiserSwitchView *)view andIndex:(int)index {
    [pageScrollView scrollRectToVisible:CGRectMake(pageScrollView.width*index, 0, pageScrollView.width, pageScrollView.height) animated:YES];
}

#pragma mark - loadNlpView Delegate
- (void)loadNlpView {
    RBAdvertiserNlpView*nlpView = [[RBAdvertiserNlpView alloc]initWithFrame:CGRectMake(pageScrollView.width, 0, ScreenWidth, pageScrollView.height)];
    [pageScrollView addSubview:nlpView];
    [nlpView loadContentView:self.nlpEntity];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)demoBtnAction:(UIButton*)sender {
    RBAlertView *alertView = [[RBAlertView alloc]init];
    alertView.delegate = self;
    [alertView showWithArray:@[@"Robin8优先推送最精准匹配的KOL\n6小时后活动未消耗完会进行补推\n推送给第二梯队的匹配KOL\n以此类推......直到活动结束",NSLocalizedString(@"R2500", @"知道了")]];
}

- (void)inputResult {
    BOOL isOK = YES;
    if(inputView.inputTF.text.length==0){
        isOK = NO;
    }
    if(inputView1.inputTV.text.length==0){
        isOK = NO;
    }
    if(self.adCampaignEntity.img_url==nil&&[imageIV.image isEqual: [UIImage imageNamed:@"pic_advertiser_bg.jpg"]]) {
        isOK = NO;
    }
    if(inputView2.inputTF.text.length==0){
        isOK = NO;
    }
    if(inputView3.inputTF.text.length==0){
        isOK = NO;
    }
    if(inputView4.inputTF.text.length==0){
        isOK = NO;
    }
    if(inputView6.inputTF.text.length==0){
        isOK = NO;
    }
    if(inputView7.inputTF.text.length==0){
        isOK = NO;
    }
    if(platType.length <= 0||platType == nil){
        isOK = NO;
    }
    if (isOK == NO) {
//        footerLeftBtn.enabled = NO;
        footerLeftBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
//        footerRightBtn.enabled = NO;
        footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorSubGray];
    } else {
//        footerLeftBtn.enabled = YES;
        footerLeftBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
//        footerRightBtn.enabled = YES;
        footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    }
}

- (BOOL)inputCheck {
    if(inputView.inputTF.text.length==0){
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6001", @"活动标题")]];
        return NO;
    }
    if(inputView1.inputTV.text.length==0){
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6008", @"活动简介")]];
        return NO;
    }
    if([imageIV.image isEqual: [UIImage imageNamed:@"pic_advertiser_bg.jpg"]]){
        [self.hudView showErrorWithStatus:NSLocalizedString(@"R6069", @"请上传封面图片")];
        return NO;
    }
    if(inputView2.inputTF.text.length==0){
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@(%@)",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6010", @"活动链接"),NSLocalizedString(@"R6089", @"下载类活动需填写APP下载地址")]];
        return NO;
    }
    if(inputView3.inputTF.text.length==0){
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6011", @"活动开始时间")]];
        return NO;
    }
    if(inputView4.inputTF.text.length==0){
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6012", @"活动结束时间")]];
        return NO;
    }
    if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6013", @"按KOL点击计费")]){
        if(inputView7.inputTF.text.length==0) {
            [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6014", @"单个点击费用")]];
            return NO;
        }
    } else if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6086", @"按KOL下载计费")]) {
        if(inputView7.inputTF.text.length==0) {
            [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6087", @"单个下载费用")]];
            return NO;
        }
    } else if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6090", @"按KOL任务计费")]) {
        if(inputView7.inputTF.text.length==0) {
            [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6091", @"单个任务费用")]];
            return NO;
        }
    } else {
        if(inputView7.inputTF.text.length==0) {
            [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6023", @"单个转发费用")]];
            return NO;
        }
    }
    if(inputView6.inputTF.text.length==0){
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6015", @"活动总预算")]];
        return NO;
    }
    if (platType.length <= 0 || platType == nil) {
        [self.hudView showErrorWithStatus:@"请选择活动平台"];
        return NO;
    }
    return YES;
}

- (void)footerLeftBtnAction:(UIButton *)sender {
    if([self inputCheck]==NO){
        return;
    }
    //
    RBAdvertiserCampaignEntity*entity = [[RBAdvertiserCampaignEntity alloc]init];
    entity.name = inputView.inputTF.text;
    entity.idescription = inputView1.inputTV.text;
    entity.url = inputView2.inputTF.text;
    entity.image = imageIV.image;
    entity.start_time = inputView3.inputTF.text;
    entity.deadline = inputView4.inputTF.text;
    if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6013", @"按KOL点击计费")]) {
        entity.per_budget_type = @"click";
    } else if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6086", @"按KOL下载计费")]) {
        entity.per_budget_type = @"simple_cpi";
    } else if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6090", @"按KOL任务计费")]) {
        entity.per_budget_type = @"cpt";
    } else {
        entity.per_budget_type = @"post";
    }
    entity.per_action_budget = inputView7.inputTF.text;
    //
    RBAdvertiserEditPreviewViewController*toview = [[RBAdvertiserEditPreviewViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    toview.adCampaignEntity = entity;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)footerRightBtnAction:(UIButton *)sender {
    if([self inputCheck]==NO){
        return;
    }
    NSString*type = @"";
    if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6013", @"按KOL点击计费")]) {
        type = @"click";
    }else if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6090", @"按KOL任务计费")]) {
        type = @"cpt";
    }else {
        type = @"post";
    }
//    else if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6086", @"按KOL下载计费")]) {
//        type = @"simple_cpi";
//    }
//    else if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6090", @"按KOL任务计费")]) {
//        type = @"cpt";
//    }

    NSString*age = [inputView8.inputTF.text stringByReplacingOccurrencesOfString:@"-" withString:@","];
    NSString*gender = [inputView9.inputTF.text stringByReplacingOccurrencesOfString:NSLocalizedString(@"R5068",@"男") withString:@"1"];
    gender = [gender stringByReplacingOccurrencesOfString:NSLocalizedString(@"R5069",@"女") withString:@"2"];
    //
    [self.hudView showOverlay];
    if(self.adCampaignEntity==nil||self.adCampaignEntity.iid.length==0) {
        // RB-发布活动
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBActivityAddWithName:inputView.inputTF.text andDescription:inputView1.inputTV.text andUrl:inputView2.inputTF.text andImg:imageIV.image andStart_time:inputView3.inputTF.text andDeadline:inputView4.inputTF.text andPer_budget_type:type andBudget:inputView6.inputTF.text andPer_action_budget:inputView7.inputTF.text andAge:age andGender:gender andRegion:inputView11.cLabel.text andTags:inputView10.cLabel.text andSub_type:platType];
    } else {
        // RB-修改活动
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBActivityAddEditWithId:self.adCampaignEntity.iid andName:inputView.inputTF.text andDescription:inputView1.inputTV.text andUrl:inputView2.inputTF.text andImg:imageIV.image andStart_time:inputView3.inputTF.text andDeadline:inputView4.inputTF.text andPer_budget_type:type andBudget:inputView6.inputTF.text andPer_action_budget:inputView7.inputTF.text andAge:age andGender:gender andRegion:inputView11.cLabel.text andTags:inputView10.cLabel.text andSubtype:platType];
        
        
    }
}

#pragma mark- loadFooterView Delegate
- (void)loadEditView {
    editScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, pageScrollView.height)];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [pageScrollView addSubview:editScrollView];
    //
    inputView = [[RBInputView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 50.0)];
    [inputView loadInputViewWithMaxNum:@"60" andText:NSLocalizedString(@"R6001", @"活动标题") andLineNum:1];
    inputView.userInteractionEnabled = YES;
    [editScrollView addSubview:inputView];
    inputView.delegate = self;
    //
    inputView1 = [[RBInputView alloc]initWithFrame:CGRectMake(0, inputView.bottom, ScreenWidth, inputView.height*2.0)];
    [inputView1 loadInputViewWithMaxNum:@"500" andText:NSLocalizedString(@"R6008", @"活动简介") andLineNum:2];
    inputView1.userInteractionEnabled = YES;
    [editScrollView addSubview:inputView1];
    inputView1.delegate = self;
    //
    UIButton*imageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    imageBtn.frame = CGRectMake(0, inputView1.bottom, editScrollView.width, 240.0*9.0/16.0+CellLeft*2.0);
    [imageBtn addTarget:self action:@selector(imageBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    imageBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:imageBtn];
    //
    imageIV = [[UIImageView alloc]initWithFrame:CGRectMake(CellLeft, CellLeft, 240.0, 240.0*9.0/16.0)];
    imageIV.image = [UIImage imageNamed:@"pic_advertiser_bg.jpg"];
    [imageBtn addSubview:imageIV];
    //
    UILabel*imageLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, imageBtn.height-0.5, ScreenWidth, 0.5)];
    imageLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [imageBtn addSubview:imageLineLabel];
    //
    UILabel*imageLabel = [[UILabel alloc]initWithFrame:CGRectMake(imageIV.right, imageIV.top, imageBtn.width-imageIV.right, imageIV.height)];
    imageLabel.font = font_13;
    imageLabel.text = NSLocalizedString(@"R6016", @"点击上传封面图片");
    imageLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [imageBtn addSubview:imageLabel];
    CGSize imageLabelSize = [Utils getUIFontSizeFitW:imageLabel];
    imageLabel.frame = CGRectMake(ScreenWidth-imageLabelSize.width-CellLeft, imageIV.top, imageLabelSize.width, imageIV.height);
    imageIV.frame = CGRectMake(CellLeft, CellLeft, ScreenWidth-CellLeft*3.0-imageLabelSize.width, (ScreenWidth-CellLeft*3.0-imageLabelSize.width)*9.0/16.0);
    imageBtn.frame = CGRectMake(0, inputView1.bottom, editScrollView.width, imageIV.height+CellLeft*2.0);
    imageLineLabel.frame = CGRectMake(0, imageBtn.height-0.5, ScreenWidth, 0.5);
    //
    inputView2 = [[RBInputView alloc]initWithFrame:CGRectMake(0, imageBtn.bottom, ScreenWidth, inputView.height)];
    [inputView2 loadInputViewWithMaxNum:nil andText:NSLocalizedString(@"R6010", @"活动链接") andLineNum:1];
    inputView2.userInteractionEnabled = YES;
    inputView2.inputTF.delegate = self;
    [editScrollView addSubview:inputView2];
    inputView2.delegate = self;
    //
    UILabel * platFormLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, inputView2.bottom, ScreenWidth-CellLeft, 30) text:@"活动平台" font:font_cu_cu_13 textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorGray] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [editScrollView addSubview:platFormLabel];
    //
    inputViewplat = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, platFormLabel.bottom, ScreenWidth, inputView.height)];
    inputViewplat.tLabel.text = NSLocalizedString(@"R8043",@"推广平台选择");
    inputViewplat.cLabel.text = @"";
    inputViewplat.backgroundColor = [UIColor whiteColor];
    [inputViewplat addTarget:self
                     action:@selector(platformChoice:)
           forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:inputViewplat];
    //
    UIButton*demoBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [demoBtn2 setTitle:NSLocalizedString(@"R6101", @"活动时间") forState:UIControlStateNormal];
    [demoBtn2 setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
    demoBtn2.titleLabel.font = font_cu_cu_13;
    [editScrollView addSubview:demoBtn2];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize labelSize2 = [NSLocalizedString(@"R6101", @"活动时间") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_cu_cu_13,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
    demoBtn2.frame = CGRectMake(CellLeft, inputViewplat.bottom, labelSize2.width, 30.0);
    //
    inputView3 = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, demoBtn2.bottom, ScreenWidth, inputView.height)];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSTimeInterval interval = 60*60*3;
    NSDate* toDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:[NSDate date]];
    NSTimeInterval interval1 = 60*60*27;
    NSDate* toDate1 = [[NSDate alloc] initWithTimeInterval:interval1 sinceDate:[NSDate date]];
    [inputView3 loadInputRightViewWithText:[formatter stringFromDate:toDate] andTitle:NSLocalizedString(@"R6011", @"活动开始时间") andIsDate:YES andIsSelect:NO andValues:nil];
    inputView3.userInteractionEnabled = YES;
    inputView3.tag = 1000;
    [editScrollView addSubview:inputView3];
    inputView3.inputTF.delegate = self;
    inputView3.delegate = self;
    //
    inputView4 = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, inputView3.bottom, ScreenWidth, inputView.height)];
    [inputView4 loadInputRightViewWithText:[formatter stringFromDate:toDate1] andTitle:NSLocalizedString(@"R6012", @"活动结束时间") andIsDate:YES andIsSelect:NO andValues:nil];
    inputView4.userInteractionEnabled = YES;
    inputView4.tag = 2000;
    [editScrollView addSubview:inputView4];
    inputView4.inputTF.delegate = self;
    inputView4.delegate = self;
    //
    UIButton*demoBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [demoBtn1 setTitle:NSLocalizedString(@"R6100", @"活动类型") forState:UIControlStateNormal];
    [demoBtn1 setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
    demoBtn1.titleLabel.font = font_cu_cu_13;
    [editScrollView addSubview:demoBtn1];
    CGSize labelSize1 = [NSLocalizedString(@"R6100", @"活动类型") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_cu_cu_13,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
    demoBtn1.frame = CGRectMake(CellLeft, inputView4.bottom, labelSize1.width, 30.0);
    //
    inputView5 = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, demoBtn1.bottom, ScreenWidth, inputView.height)];
    [inputView5 loadInputRightViewWithText:NSLocalizedString(@"R6013", @"按KOL点击计费") andTitle:NSLocalizedString(@"R6019", @"选择活动类型") andIsDate:NO andIsSelect:YES andValues:@[NSLocalizedString(@"R6013", @"按KOL点击计费"),NSLocalizedString(@"R6020", @"按KOL转发计费"),NSLocalizedString(@"R6090", @"按KOL任务计费")]];
    inputView5.delegate = self;
    inputView5.tag = 3000;
    inputView5.userInteractionEnabled = YES;
    inputView5.inputTF.delegate = self;
    [editScrollView addSubview:inputView5];
    //
    inputView6 = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, inputView5.bottom, ScreenWidth, inputView.height)];
    [inputView6 loadInputRightViewWithText:nil andTitle:NSLocalizedString(@"R6015", @"活动总预算") andIsDate:NO andIsSelect:NO andValues:nil];
    inputView6.userInteractionEnabled = YES;
    inputView6.tag = 4000;
    [editScrollView addSubview:inputView6];
    inputView6.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6021", @"最低100元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    inputView6.inputTF.keyboardType = UIKeyboardTypeNumberPad;
    inputView6.inputTF.delegate = self;
    inputView6.delegate = self;
    //
    inputView7 = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, inputView6.bottom, ScreenWidth, inputView.height)];
    [inputView7 loadInputRightViewWithText:nil andTitle:NSLocalizedString(@"R6014", @"单个点击费用") andIsDate:NO andIsSelect:NO andValues:nil];
    inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6022", @"最低0.2元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    inputView7.userInteractionEnabled = YES;
    inputView7.tag = 5000;
    [editScrollView addSubview:inputView7];
    inputView7.inputTF.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    inputView7.inputTF.delegate = self;
    inputView7.delegate = self;
    //
    UIButton*demoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [demoBtn setTitle:[NSString stringWithFormat:@"%@?",NSLocalizedString(@"R8023", @"精准营销")]forState:UIControlStateNormal];
    [demoBtn setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
    demoBtn.titleLabel.font = font_cu_cu_13;
    [demoBtn addTarget:self action:@selector(demoBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:demoBtn];
    CGSize labelSize = [[NSString stringWithFormat:@"%@?",NSLocalizedString(@"R8023", @"精准营销")] boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_cu_cu_13,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
    demoBtn.frame = CGRectMake(CellLeft, inputView7.bottom, labelSize.width, 30.0);
    //
    inputView8 = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, demoBtn.bottom, ScreenWidth, inputView.height)];
    [inputView8 loadInputRightViewWithText:NSLocalizedString(@"R8028", @"全部") andTitle:NSLocalizedString(@"R8024", @"KOL年龄") andIsDate:NO andIsSelect:YES andValues:@[NSLocalizedString(@"R8028", @"全部"),@"0-20",@"20-40",@"40-60",@"60-100"]];
    inputView8.delegate = self;
    inputView8.tag = 6000;
    inputView8.userInteractionEnabled = YES;
    inputView8.inputTF.delegate = self;
    [editScrollView addSubview:inputView8];
    //
    inputView9 = [[RBInputRightView alloc]initWithFrame:CGRectMake(0, inputView8.bottom, ScreenWidth, inputView.height)];
    [inputView9 loadInputRightViewWithText:NSLocalizedString(@"R8028", @"全部") andTitle:NSLocalizedString(@"R8025", @"KOL性别") andIsDate:NO andIsSelect:YES andValues:@[NSLocalizedString(@"R8028", @"全部"),NSLocalizedString(@"R5068",  @"男"),NSLocalizedString(@"R5069",  @"女")]];
    inputView9.delegate = self;
    inputView9.tag = 7000;
    inputView9.userInteractionEnabled = YES;
    inputView9.inputTF.delegate = self;
    [editScrollView addSubview:inputView9];
    //
    inputView10 = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, inputView9.bottom, ScreenWidth, inputView.height)];
    inputView10.backgroundColor = [UIColor whiteColor];
    inputView10.tLabel.text = NSLocalizedString(@"R8026", @"KOL分类");
    inputView10.cLabel.text = NSLocalizedString(@"R8028", @"全部");
    [inputView10 addTarget:self
                     action:@selector(categoryBtnAction:)
           forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:inputView10];
    //
    inputView11 = [[RBKolEditView alloc]initWithFrame:CGRectMake(0, inputView10.bottom, ScreenWidth, inputView.height)];
    inputView11.backgroundColor = [UIColor whiteColor];
    inputView11.tLabel.text = NSLocalizedString(@"R8027", @"KOL城市");
    inputView11.cLabel.text = NSLocalizedString(@"R8028", @"全部");
    [inputView11 addTarget:self
                    action:@selector(cityBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:inputView11];
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, inputView11.bottom+CellBottom, ScreenWidth, 45.0)];
    [editScrollView addSubview:footerView];
    [editScrollView setContentSize:CGSizeMake(editScrollView.width, footerView.bottom)];
    //
    footerLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerLeftBtn.frame = CGRectMake(0, 0, footerView.width/2.0, footerView.height);
    footerLeftBtn.titleLabel.font = font_cu_15;
    footerLeftBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    [footerLeftBtn addTarget:self
                      action:@selector(footerLeftBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
    [footerLeftBtn setTitle:NSLocalizedString(@"R2097", @"预览") forState:UIControlStateNormal];
    [footerLeftBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerLeftBtn];
    //
    footerRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerRightBtn.frame = CGRectMake(footerView.width/2.0, 0, footerView.width/2.0, footerView.height);
    footerRightBtn.titleLabel.font = font_cu_15;
    footerRightBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    [footerRightBtn addTarget:self
                       action:@selector(footerRightBtnAction:)
             forControlEvents:UIControlEventTouchUpInside];
    [footerRightBtn setTitle:NSLocalizedString(@"R2080", @"提交") forState:UIControlStateNormal];
    [footerRightBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerRightBtn];
    //
    UILabel*footerLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(footerView.width/2.0-0.5f, 10.0, 1, 45.0-20.0)];
    footerLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [footerView addSubview:footerLineLabel];
    // 修改信息
    if(self.adCampaignEntity!=nil) {
        if(self.adCampaignEntity.iid.length!=0) {
        [self.hudView show];
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBActivityAddDetailWithId:self.adCampaignEntity.iid];
        } else {
            [self getResult];
        }
    }
}
-(void)platformChoice:(id)sender{
    RBPlatSelectViewController * selectVC = [[RBPlatSelectViewController alloc]init];
    selectVC.hidesBottomBarWhenPushed = YES;
    selectVC.mark = platType;
    selectVC.platBlock = ^(BOOL weiboSelected, BOOL wechatSelected) {
        
        if (wechatSelected == YES && weiboSelected == NO) {
            inputViewplat.cLabel.text = @"微信朋友圈";
            platType = @"wechat";
            [inputView5 loadInputRightViewWithText:NSLocalizedString(@"R6013", @"按KOL点击计费") andTitle:NSLocalizedString(@"R6019", @"选择活动类型") andIsDate:NO andIsSelect:YES andValues:@[NSLocalizedString(@"R6013", @"按KOL点击计费"),NSLocalizedString(@"R6020", @"按KOL转发计费"),NSLocalizedString(@"R6090", @"按KOL任务计费")]];
            [inputView7 loadInputRightViewWithText:@"0.5" andTitle:NSLocalizedString(@"R6014", @"单个点击费用") andIsDate:NO andIsSelect:NO andValues:nil];
            inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6022", @"最低0.2元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
        }else if (wechatSelected == YES && weiboSelected == YES){
            inputViewplat.cLabel.text = @"微博，微信朋友圈";
            platType = @"wechat,weibo";
            [inputView5 loadInputRightViewWithText:NSLocalizedString(@"R6020", @"按KOL转发计费") andTitle:NSLocalizedString(@"R6019", @"选择活动类型") andIsDate:NO andIsSelect:YES andValues:@[NSLocalizedString(@"R6020", @"按KOL转发计费"),NSLocalizedString(@"R6090", @"按KOL任务计费")]];
            [inputView7 loadInputRightViewWithText:@"3" andTitle:NSLocalizedString(@"R6023", @"单个转发费用") andIsDate:NO andIsSelect:NO andValues:nil];
            inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6024", @"最低2.5元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
        }else if (wechatSelected == NO && weiboSelected == YES){
            inputViewplat.cLabel.text = @"微博";
            platType = @"weibo";
            [inputView5 loadInputRightViewWithText:NSLocalizedString(@"R6020", @"按KOL转发计费") andTitle:NSLocalizedString(@"R6019", @"选择活动类型") andIsDate:NO andIsSelect:YES andValues:@[NSLocalizedString(@"R6020", @"按KOL转发计费"),NSLocalizedString(@"R6090", @"按KOL任务计费")]];
            [inputView7 loadInputRightViewWithText:@"3" andTitle:NSLocalizedString(@"R6023", @"单个转发费用") andIsDate:NO andIsSelect:NO andValues:nil];
            inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6024", @"最低2.5元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
        }else{
            inputViewplat.cLabel.text = @"";
            platType = @"";
            [inputView5 loadInputRightViewWithText:NSLocalizedString(@"R6020", @"按KOL转发计费") andTitle:NSLocalizedString(@"R6019", @"选择活动类型") andIsDate:NO andIsSelect:YES andValues:@[NSLocalizedString(@"R6020", @"按KOL转发计费"),NSLocalizedString(@"R6090", @"按KOL任务计费")]];
            [inputView7 loadInputRightViewWithText:@"3" andTitle:NSLocalizedString(@"R6023", @"单个转发费用") andIsDate:NO andIsSelect:NO andValues:nil];
            inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6024", @"最低2.5元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
        }
        [self inputResult];
    };
    [self.navigationController pushViewController:selectVC animated:YES];
}
#pragma mark - RBKolEditView Delegate
- (void)categoryBtnAction:(RBKolEditView*)sender {
    [self.view endEditing:YES];
    [self inputResult];
    RBAdvertiserInterestViewController *toview = [[RBAdvertiserInterestViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    toview.editView = sender;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)cityBtnAction:(RBKolEditView*)sender {
    [self.view endEditing:YES];
    [self inputResult];
    RBAdvertiserCitysViewController *toview = [[RBAdvertiserCitysViewController alloc] init];
    toview.hidesBottomBarWhenPushed = YES;
    toview.editView = sender;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    [self inputResult];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    float keyboardTop = ScreenHeight-320;
    if (editScrollView.bottom > keyboardTop) {
        [UIView animateWithDuration:0.2 animations:^{
            editScrollView.top = keyboardTop-editScrollView.height;
        } completion:^(BOOL finished) {
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.2 animations:^{
        editScrollView.top = 0;
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - RBInputRightView Delegate
- (void)RBInputViewInputFinished:(RBInputView *)view {
    [self inputResult];
}

- (void)RBInputRightViewInputFinished:(RBInputRightView *)view {
    [self inputResult];
}

- (void)RBInputRightViewPicker:(RBInputRightView *)view andValue:(NSString *)str {
    if(view.tag==3000){
        if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6013", @"按KOL点击计费")]){
            [inputView7 loadInputRightViewWithText:@"0.5" andTitle:NSLocalizedString(@"R6014", @"单个点击费用") andIsDate:NO andIsSelect:NO andValues:nil];
            inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6022", @"最低0.2元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
        } else if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6086", @"按KOL下载计费")]){
            [inputView7 loadInputRightViewWithText:@"3" andTitle:NSLocalizedString(@"R6087", @"单个下载费用") andIsDate:NO andIsSelect:NO andValues:nil];
            inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6088", @"最低3元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
        } else if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6090", @"按KOL任务计费")]){
            [inputView7 loadInputRightViewWithText:@"3" andTitle:NSLocalizedString(@"R6091", @"单个任务费用") andIsDate:NO andIsSelect:NO andValues:nil];
            inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6092", @"最低3元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
        } else {
            [inputView7 loadInputRightViewWithText:@"3" andTitle:NSLocalizedString(@"R6023", @"单个转发费用") andIsDate:NO andIsSelect:NO andValues:nil];
            inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6024", @"最低2.5元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
        }
    }
    [self inputResult];
}

- (void)imageBtnAction:(UIButton*)sender {
    [self.view endEditing:YES];
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    [actionSheet showWithArray:@[NSLocalizedString(@"R2051", @"打开相册"),NSLocalizedString(@"R1011", @"取消")]];
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if (buttonIndex==0) {
        ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
        if (author == ALAuthorizationStatusDenied) {
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.alertBody = @"照片不可用,请设置打开";
            [[UIApplication sharedApplication]
             presentLocalNotificationNow:notification];
            return;
        }
        RBPhotoAsstesGroupViewController*toview = [[RBPhotoAsstesGroupViewController alloc]init];
        toview.selectNumber = 1;
        toview.delegateC = self;
        toview.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:toview animated:YES];
    }
}

#pragma mark - RBPhotoAsstesGroupViewController Delegate
- (void)RBPhotoAsstesGroupViewController:(RBPhotoAsstesGroupViewController *)vc selectedList:(NSMutableArray *)list {
    ALAsset *asset = list[0];
    ALAssetRepresentation *rep = [asset defaultRepresentation];
    UIImage *originalImg = [UIImage imageWithCGImage:rep.fullScreenImage];
    if (originalImg !=  nil) {
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           VPImageCropperViewController *imgEditorVC  =
                           [[VPImageCropperViewController alloc]
                            initWithImage:originalImg
                            cropFrame:CGRectMake(0, (ScreenHeight-self.view.width*9.0/16.0)/2.0, self.view.width, self.view.width*9.0/16.0)
                            limitScaleRatio:2.0];
                           imgEditorVC.delegate = self;
                           [self presentViewController:imgEditorVC
                                              animated:YES
                                            completion:NULL];
                       });
    }
}

#pragma mark - VPImageCropperViewController Delegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController
         didFinished:(UIImage *)editedImage {
    imageIV.image = editedImage;
    [self inputResult];
    [cropperViewController
     dismissViewControllerAnimated:YES
     completion:NULL];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES
                                              completion:NULL];
}

- (void)getResult {
    inputView.inputTF.text = self.adCampaignEntity.name;
    inputView1.inputTV.text = self.adCampaignEntity.idescription;
    inputView1.inputTV.placeholder = @"";
    inputView2.inputTF.text = self.adCampaignEntity.url;
    [imageIV sd_setImageWithURL:[NSURL URLWithString:self.adCampaignEntity.img_url] placeholderImage:[UIImage imageNamed:@"pic_advertiser_bg.jpg"]];
    NSString *sDate = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.start_time]componentsSeparatedByString:@"T"][0];
    sDate = [sDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *sDate1 = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.start_time]componentsSeparatedByString:@"T"][1];
    sDate1 = [[NSString stringWithFormat:@"%@",sDate1]componentsSeparatedByString:@"+"][0];
    NSString *eDate = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.deadline]componentsSeparatedByString:@"T"][0];
    eDate = [eDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *eDate1 = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.deadline]componentsSeparatedByString:@"T"][1];
    eDate1 = [[NSString stringWithFormat:@"%@",eDate1]componentsSeparatedByString:@"+"][0];
    inputView3.inputTF.text = [NSString stringWithFormat:@"%@ %@",sDate,sDate1];
    inputView4.inputTF.text = [NSString stringWithFormat:@"%@ %@",eDate,eDate1];
    if([self.adCampaignEntity.per_budget_type isEqualToString:@"post"]){
        inputView5.inputTF.text = NSLocalizedString(@"R6020", @"按KOL转发计费");
        inputView7.inputLabel.text = NSLocalizedString(@"R6023", @"单个转发费用");
    }else if([self.adCampaignEntity.per_budget_type isEqualToString:@"cpt"]){
        inputView5.inputTF.text = NSLocalizedString(@"R6090", @"按KOL任务计费");
        inputView7.inputLabel.text = NSLocalizedString(@"R6091", @"单个任务费用");
    }else {
        inputView5.inputTF.text = NSLocalizedString(@"R6013", @"按KOL点击计费");
        inputView7.inputLabel.text = NSLocalizedString(@"R6014", @"单个点击费用");
    }
//    else if([self.adCampaignEntity.per_budget_type isEqualToString:@"simple_cpi"]){
//        inputView5.inputTF.text = NSLocalizedString(@"R6086", @"按KOL下载计费");
//        inputView7.inputLabel.text = NSLocalizedString(@"R6087", @"单个下载费用");
//    }

    if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6013", @"按KOL点击计费")]){
        [inputView7 loadInputRightViewWithText:@"0.5" andTitle:NSLocalizedString(@"R6014", @"单个点击费用") andIsDate:NO andIsSelect:NO andValues:nil];
        inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6022", @"最低0.2元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    } else if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6090", @"按KOL任务计费")]){
        [inputView7 loadInputRightViewWithText:@"3" andTitle:NSLocalizedString(@"R6091", @"单个任务费用") andIsDate:NO andIsSelect:NO andValues:nil];
        inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6092", @"最低3元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    }else {
        [inputView7 loadInputRightViewWithText:@"3" andTitle:NSLocalizedString(@"R6023", @"单个转发费用") andIsDate:NO andIsSelect:NO andValues:nil];
        inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6024", @"最低2.5元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    }
//    else if([inputView5.inputTF.text isEqualToString:NSLocalizedString(@"R6086", @"按KOL下载计费")]){
//        [inputView7 loadInputRightViewWithText:@"3" andTitle:NSLocalizedString(@"R6087", @"单个下载费用") andIsDate:NO andIsSelect:NO andValues:nil];
//        inputView7.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",NSLocalizedString(@"R6088", @"最低2元")] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
//
    inputView6.inputTF.text = self.adCampaignEntity.budget;
    if(self.adCampaignEntity.iid.length!=0&&self.adCampaignEntity.budget_editable.intValue==0){
        inputView6.inputTF.enabled = NO;
        inputView6.inputLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        inputView6.inputTF.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    } else {
        inputView6.inputLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        inputView6.inputTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        inputView6.inputTF.enabled = YES;
    }
    inputView7.inputTF.text = self.adCampaignEntity.per_action_budget;
    inputView8.inputTF.text = [self.adCampaignEntity.age stringByReplacingOccurrencesOfString:@"," withString:@"-"];
    NSString*gender = [self.adCampaignEntity.gender stringByReplacingOccurrencesOfString:@"1" withString:NSLocalizedString(@"R5068",@"男")];
    gender = [gender stringByReplacingOccurrencesOfString:@"2" withString:NSLocalizedString(@"R5069",@"女")];
    inputView9.inputTF.text = gender;
    inputView10.cLabel.text = self.adCampaignEntity.tag_labels;
    inputView11.cLabel.text = self.adCampaignEntity.region;
    //
    [self inputResult];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"kol_campaigns/add"]||[sender isEqualToString:@"kol_campaigns/update"]) {
        [self.hudView dismiss];
        RBAdvertiserCampaignEntity*entity = [JsonService getRBAdvertiserCampaignEntity:jsonObject];
        NSString*iid = [NSString stringWithFormat:@"%@",entity.iid];
        NSString*status = [NSString stringWithFormat:@"%@",entity.status];
        if (iid.length!=0&&[status isEqualToString:@"unpay"]) {
            RBAdvertiserEditPayViewController*toview = [[RBAdvertiserEditPayViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            toview.adCampaignEntity = entity;
            [self.navigationController pushViewController:toview animated:YES];
        } else {
            for (UIViewController *temp in self.navigationController.viewControllers){
                if ([temp isKindOfClass:[RBAdvertiserMyListViewController class]]) {
                    RBAdvertiserMyListViewController*toview = (RBAdvertiserMyListViewController*)temp;
                    [self.navigationController popToViewController:toview animated:YES];
                    return;
                }
            }
        }
    }
    if ([sender isEqualToString:@"kol_campaigns/detail"]) {
        [self.hudView dismiss];
        self.adCampaignEntity = [JsonService getRBAdvertiserCampaignEntity:jsonObject];
        [self getResult];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

