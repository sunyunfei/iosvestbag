//
//  RBAdvertiserEditViewController.h
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInputView.h"
#import "RBKolEditView.h"
#import "RBInputRightView.h"
#import "RBPictureView.h"
#import "RBAdvertiserNlpView.h"
#import "RBAdvertiserSwitchView.h"
//
#import "RBAdvertiserCitysViewController.h"
#import "RBAdvertiserInterestViewController.h"
#import "RBPhotoAsstesGroupViewController.h"
#import "RBAdvertiserEditPayViewController.h"
#import "RBAdvertiserEditPreviewViewController.h"
@interface RBAdvertiserEditViewController : RBBaseViewController
<RBBaseVCDelegate,RBInputViewDelegate,RBInputRightViewDelegate,RBPhotoAsstesGroupViewControllerDelegate,RBActionSheetDelegate,VPImageCropperDelegate,UITextFieldDelegate,UIScrollViewDelegate,RBAdvertiserSwitchViewDelegate>
@property(nonatomic, strong) RBAdvertiserCampaignEntity*adCampaignEntity;
@property(nonatomic, strong) RBNlpEntity *nlpEntity;
@end
