//
//  RBAdvertiserMoneyListViewController.h
//  RB
//
//  Created by AngusNi on 6/13/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBUserIncomeListTableViewCell.h"

@interface RBAdvertiserMoneyListViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate>

@end