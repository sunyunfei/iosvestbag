//
//  RBAdvertiserCitysViewController.h
//  RB
//
//  Created by AngusNi on 16/2/17.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBArticleSearchView.h"
#import "RBKolEditView.h"

@interface RBAdvertiserCitysViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,CLLocationManagerDelegate,RBArticleSearchViewDelegate>
@property(nonatomic, strong) RBKolEditView*editView;
@end
