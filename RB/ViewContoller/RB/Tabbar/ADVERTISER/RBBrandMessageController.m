//
//  RBBrandMessageController.m
//  RB
//
//  Created by RB8 on 2018/5/22.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBBrandMessageController.h"
#import "RBAdverView.h"
@interface RBBrandMessageController ()
{
    UIButton * headButton;
    RBAdverView * brandNameView;
    RBAdverView * companyView;
    RBAdverView * netView;
    UITextView * dspTextView;
    UILabel * placeHolderLabel;
    UIImage * uploadImg;
}
@end

@implementation RBBrandMessageController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitle = @"品牌主信息";
    self.delegate = self;
    // Do any additional setup after loading the view.
    [self loadEditView];
    [self loadFootView];
}
- (void)loadEditView{
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    UIScrollView * editScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight - NavHeight - 41)];
    editScroll.showsVerticalScrollIndicator = NO;
    editScroll.showsHorizontalScrollIndicator = NO;
    editScroll.bounces = NO;
    editScroll.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self.view addSubview:editScroll];
    //
    UIView * backView = [[UIView alloc]initWithFrame:CGRectMake(0, 10, ScreenWidth, 0)];
    backView.backgroundColor = [UIColor whiteColor];
    [editScroll addSubview:backView];
    //
    UILabel * logoLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 0, 50, 66) text:@"品牌Logo" font:font_(14) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:@"000000"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [logoLabel sizeToFit];
    logoLabel.size = CGSizeMake(logoLabel.frame.size.width, 66);
    [backView addSubview:logoLabel];
    //
    headButton = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - CellLeft - 40, 13, 40, 40) title:nil hlTitle:nil titleColor:nil hlTitleColor:nil backgroundColor:nil image:PlaceHolderImage hlImage:nil];
    [headButton addTarget:self action:@selector(uploadImage:) forControlEvents:UIControlEventTouchUpInside];
    headButton.layer.cornerRadius = 20.0;
    headButton.layer.masksToBounds = YES;
    NSString * headUrl = [NSString stringWithFormat:@"%@",[JsonService checkJson:[_dict objectForKey:@"avatar_url"]]];
    if (headUrl.length > 0) {
        [headButton sd_setImageWithURL:[NSURL URLWithString:headUrl] forState:UIControlStateNormal];
    }
    [backView addSubview:headButton];
    //
    UILabel * lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, logoLabel.bottom - 0.5, ScreenWidth - 2*CellLeft, 0.5) text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorLine] numberOfLines:1];
    [backView addSubview:lineLabel];
    //
    brandNameView = [[RBAdverView alloc]initWithFrame:CGRectMake(0, lineLabel.bottom, ScreenWidth - 2 * CellLeft, 55)];
    brandNameView.title = @"品牌名称*";
    brandNameView.placeHolder = @"填写品牌名称";
    NSString * brandName = [NSString stringWithFormat:@"%@",[JsonService checkJson:[_dict objectForKey:@"name"]]];
    if (brandName.length > 0) {
        brandNameView.textField.text = brandName;
    }
    [backView addSubview:brandNameView];
    //
    UILabel * lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, brandNameView.bottom - 0.5, ScreenWidth, 0.5) text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorLine] numberOfLines:1];
    [backView addSubview:lineLabel1];
    //
    companyView = [[RBAdverView alloc]initWithFrame:CGRectMake(0, lineLabel1.bottom, ScreenWidth - 2*CellLeft, 55)];
    companyView.title = @"公司名称*";
    companyView.placeHolder = @"填写完整抬头或\"个人\"";
    NSString * companyName = [JsonService checkJson:[_dict objectForKey:@"company_name"]];
    if (companyName.length > 0) {
        companyView.textField.text = companyName;
    }
    [backView addSubview:companyView];
    //
    UILabel * lineLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, companyView.bottom - 0.5, ScreenWidth - 2*CellLeft, 0.5) text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorLine] numberOfLines:1];
    [backView addSubview:lineLabel2];
    //
    netView = [[RBAdverView alloc]initWithFrame:CGRectMake(0, lineLabel2.bottom, ScreenWidth - 2*CellLeft, 55)];
    netView.title = @"官方网站";
    netView.placeHolder = @"您公司的官网";
    NSString * companyUrl = [JsonService checkJson:[_dict objectForKey:@"url"]];
    if (companyUrl.length > 0) {
        netView.textField.text = companyUrl;
    }
    [backView addSubview:netView];
    //
    UILabel * seperateLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, netView.bottom, ScreenWidth, 10) text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorBkg] numberOfLines:1];
    [backView addSubview:seperateLabel];
    //
    UILabel * brandDspLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, seperateLabel.bottom, ScreenWidth, 55) text:@"品牌介绍" font:font_(14) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:@"000000"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [backView addSubview:brandDspLabel];
    //
    UILabel * lineLabel3 = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, brandDspLabel.bottom - 0.5, ScreenWidth - 2 * CellLeft, 0.5)];
    lineLabel3.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [backView addSubview:lineLabel3];
    //
    dspTextView = [[UITextView alloc]initWithFrame:CGRectMake(CellLeft, lineLabel3.bottom, ScreenWidth - 2 * CellLeft, 118)];
    dspTextView.delegate = self;
    dspTextView.textAlignment = NSTextAlignmentLeft;
    dspTextView.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    TextFiledKeyBoard *textFieldKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textFieldKB.delegateT = self;
    [dspTextView setInputAccessoryView:textFieldKB];
    NSString * dspText = [JsonService checkJson:[_dict objectForKey:@"description"]];
    if (dspText.length > 0) {
        dspTextView.text = dspText;
    }
    [backView addSubview:dspTextView];
    //
    backView.height = dspTextView.bottom;
    //
    placeHolderLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth-CellLeft, 50) text:@"请填写您品牌的介绍" font:font_(14) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:@"bbbbbb"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    if (dspText.length > 0) {
        placeHolderLabel.hidden = YES;
    }
    [dspTextView addSubview:placeHolderLabel];
    
}
- (void)loadFootView{
    UIButton * footBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, ScreenHeight - 40, ScreenWidth, 40) title:@"保存" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorWhite] hlTitleColor:nil backgroundColor:[Utils getUIColorWithHexString:SysBackColorBlue] image:nil hlImage:nil];
    [footBtn addTarget:self action:@selector(saveAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:footBtn];
}
#pragma mark 上传头像
- (void)uploadImage:(id)sender{
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    actionSheet.tag = 1000;
    [actionSheet showWithArray:@[NSLocalizedString(@"R2051", @"打开相册"),NSLocalizedString(@"R1011",@"取消")]];
}
- (void)saveAction:(id)sender{
    if (companyView.textField.text.length <= 0){
        [self.hudView showErrorWithStatus:@"请填写公司名称"];
        return;
    }else if (brandNameView.textField.text.length <= 0){
        [self.hudView showErrorWithStatus:@"请填写品牌名称"];
        return;
    }
    
    [self.hudView show];
    Handler * handler = [Handler shareHandler];
    handler.delegate = self;
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    [dic setValue:brandNameView.textField.text forKey:@"name"];
    [dic setValue:companyView.textField.text forKey:@"campany_name"];
    [dic setValue:netView.textField.text forKey:@"url"];
    [dic setValue:dspTextView.text forKey:@"description"];
    uploadImg = headButton.imageView.image;
    [handler getRBActivityModifybrandwith:dic andAvatar:uploadImg];
}
- (void)RBNavLeftBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark RBActionSheetViewDelegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex{
    if (buttonIndex == 0) {
        RBPictureUpload*picUpload = [RBPictureUpload sharedRBPictureUpload];
        picUpload.scaleFrame = CGRectMake(CellLeft, (ScreenHeight-ScreenWidth+2*CellLeft)/2.0, ScreenWidth - 2*CellLeft, ScreenWidth-2*CellLeft);
        picUpload.fromVC = self.navigationController;
        picUpload.delegate = self;
        [picUpload RBPictureUploadClickedButtonAtIndex:1];
        return;
    }else{
        
    }
}
#pragma mark RBPicureUploadDelegate
- (void)RBPictureUpload:(RBPictureUpload *)pictureUpload selectImage:(UIImage *)img{
    img = [Utils getUIImageScalingFromSourceImage:img targetSize:headButton.size];
    [headButton setBackgroundImage:img forState:UIControlStateNormal];
    [headButton setImage:img forState:UIControlStateNormal];
    uploadImg = img;
}
#pragma mark textfieldBoardDelegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender{
    [self.view endEditing:YES];
}
#pragma textViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    placeHolderLabel.hidden = YES;
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        placeHolderLabel.hidden = NO;
    }else{
        placeHolderLabel.hidden = YES;
    }
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        placeHolderLabel.hidden = NO;
    }else{
        placeHolderLabel.hidden = YES;
    }
    return YES;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender{
    if ([sender isEqualToString:@"kol_brand/update_profile"]) {
        [self.hudView dismiss];
        if ([[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"error"]] isEqualToString:@"0"]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender{
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}
- (void)handlerError:(NSError *)error Tag:(NSString *)sender{
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
//-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
//    return YES;
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
