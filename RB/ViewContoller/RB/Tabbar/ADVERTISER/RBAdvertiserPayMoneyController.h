//
//  RBAdvertiserPayMoneyController.h
//  RB
//
//  Created by RB8 on 2018/6/5.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBAdvertiserRechargeSuccessedViewController.h"
#import "RBAdvertiserRechargeErrorViewController.h"
@interface RBAdvertiserPayMoneyController : RBBaseViewController<RBBaseVCDelegate,TextFiledKeyBoardDelegate>

@end
