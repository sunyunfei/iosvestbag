//
//  RBAdvertiserMyListDetailViewController.h
//  RB
//
//  Created by AngusNi on 6/13/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBAdvertiserMyListDetailKolsViewController.h"
#import "RBCampaignDetailViewController.h"

@interface RBAdvertiserMyListDetailViewController : RBBaseViewController
<RBBaseVCDelegate>
@property(nonatomic, strong) RBAdvertiserCampaignEntity*adCampaignEntity;
@end
