//
//  RBAdvertiserEditPreviewViewController.h
//  RB
//
//  Created by AngusNi on 16/1/28.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
@interface RBAdvertiserEditPreviewViewController : RBBaseViewController
<RBBaseVCDelegate,UIScrollViewDelegate,RBActionSheetDelegate,RBAlertViewDelegate>
@property(nonatomic, strong) RBAdvertiserCampaignEntity*adCampaignEntity;
@end
