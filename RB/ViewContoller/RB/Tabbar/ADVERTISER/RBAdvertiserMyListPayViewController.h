//
//  RBAdvertiserMyListPayViewController.h
//  RB
//
//  Created by AngusNi on 6/14/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBAdvertiserPayDetailViewController.h"

@interface RBAdvertiserMyListPayViewController : RBBaseViewController
<RBBaseVCDelegate,RBActionSheetDelegate>
@property(nonatomic, strong) RBAdvertiserCampaignEntity*adCampaignEntity;

@end
