//
//  RBAdvertiserEditAddViewController.h
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBInputView.h"
#import "RBKolEditView.h"
#import "RBInputRightView.h"
#import "RBPictureView.h"
//
#import "RBAdvertiserCitysViewController.h"
#import "RBAdvertiserInterestViewController.h"
#import "RBPhotoAsstesGroupViewController.h"
#import "RBAdvertiserEditPayViewController.h"
#import "RBAdvertiserEditPreviewViewController.h"
@interface RBAdvertiserEditAddViewController : RBBaseViewController
<RBBaseVCDelegate,RBInputViewDelegate,RBInputRightViewDelegate,RBPhotoAsstesGroupViewControllerDelegate,RBActionSheetDelegate,VPImageCropperDelegate,UITextFieldDelegate>
@property(nonatomic, strong) RBAdvertiserCampaignEntity*adCampaignEntity;
@property(nonatomic, strong) NSString*againStr;

@end
