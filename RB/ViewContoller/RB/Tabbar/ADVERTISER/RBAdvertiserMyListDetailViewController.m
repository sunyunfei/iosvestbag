//
//  RBAdvertiserMyListDetailViewController.m
//  RB
//
//  Created by AngusNi on 6/13/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserMyListDetailViewController.h"

@interface RBAdvertiserMyListDetailViewController () {
    UILabel*tagLabel;
    UIView*moneyView;
    SHLineGraphView*lineGraphView;
    SHPlot*linePlot;
    SHPlot*linePlot1;
    NSDate*tempDate;
    UILabel*lineGraphViewLabel;
    NSTimer*updateTimer;
}
@end

@implementation RBAdvertiserMyListDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = self.adCampaignEntity.name;
    [self.hudView show];
    // RB-发布活动-活动详情
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityAddDetailWithId:self.adCampaignEntity.iid];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
    //
    [TalkingData trackPageBegin:@"advertiser-my-detail"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [TalkingData trackPageEnd:@"advertiser-my-detail"];
}

#pragma mark - UIButton Delegate
- (void)navLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)editBtn0Action:(UIButton *)sender {
    RBCampaignDetailViewController*toview = [[RBCampaignDetailViewController alloc] init];
    toview.campaignId = self.adCampaignEntity.iid;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

- (void)editBtn1Action:(UIButton *)sender {
    RBAdvertiserMyListDetailKolsViewController*toview = [[RBAdvertiserMyListDetailKolsViewController alloc] init];
    toview.adCampaignEntity = self.adCampaignEntity;
    toview.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:toview animated:YES];
}

#pragma mark - UIScreenEdgePanGestureRecognizer Delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (void)edgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender {
    [self navLeftBtnAction];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIScrollView*editScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self.view addSubview:editScrollView];
    //
    UIScreenEdgePanGestureRecognizer *edgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(edgePanGesture:)];
    edgePanGestureRecognizer.delegate = self;
    edgePanGestureRecognizer.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:edgePanGestureRecognizer];
    //
    UILabel*bgLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth*2.0)];
    bgLabel.backgroundColor = [UIColor whiteColor];
    [editScrollView addSubview:bgLabel];
    //
    UIImageView*bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenWidth*9.0/16.0)];
    [bgIV sd_setImageWithURL:[NSURL URLWithString:self.adCampaignEntity.img_url] placeholderImage:PlaceHolderImage];
    [editScrollView addSubview:bgIV];
    //
    UILabel*bgIVLabel = [[UILabel alloc]initWithFrame:bgIV.bounds];
    bgIVLabel.backgroundColor = SysColorCover;
    [bgIV addSubview:bgIVLabel];
    //
    UIView*scrollViewNavView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 64)];
    [editScrollView addSubview:scrollViewNavView];
    //
    UILabel *navCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.0, 20.0, self.view.width-100.0, 44)];
    navCenterLabel.text = NSLocalizedString(@"R6007", @"发布活动");
    navCenterLabel.font = font_cu_17;
    navCenterLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    navCenterLabel.textAlignment = NSTextAlignmentCenter;
    navCenterLabel.userInteractionEnabled = YES;
    [scrollViewNavView addSubview:navCenterLabel];
    //
    UILabel*navLeftLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 20.0+(44.0-20.0)/2.0, 20.0, 20.0)];
    navLeftLabel.font = font_icon_(20.0);
    navLeftLabel.text = Icon_bar_back;
    navLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    [scrollViewNavView addSubview:navLeftLabel];
    //
    UIButton*navLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navLeftBtn.frame = CGRectMake(0, 20.0, 44.0, 44.0);
    [navLeftBtn addTarget:self
                   action:@selector(navLeftBtnAction)
         forControlEvents:UIControlEventTouchUpInside];
    [scrollViewNavView addSubview:navLeftBtn];
    //
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, bgIV.bottom+16.0, editScrollView.width, 18)];
    titleLabel.font = font_cu_15;
    titleLabel.text = self.adCampaignEntity.name;
    titleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:titleLabel];
    //
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, titleLabel.bottom+6.0, editScrollView.width, 13)];
    timeLabel.font = font_11;
    timeLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:timeLabel];
    NSString *sDate = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.start_time]componentsSeparatedByString:@"T"][0];
    sDate = [sDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *sDate1 = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.start_time]componentsSeparatedByString:@"T"][1];
    sDate1 = [[NSString stringWithFormat:@"%@",sDate1]componentsSeparatedByString:@"+"][0];
    NSString *eDate = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.deadline]componentsSeparatedByString:@"T"][0];
    eDate = [eDate stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    NSString *eDate1 = [[NSString stringWithFormat:@"%@",self.adCampaignEntity.deadline]componentsSeparatedByString:@"T"][1];
    eDate1 = [[NSString stringWithFormat:@"%@",eDate1]componentsSeparatedByString:@"+"][0];
    timeLabel.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",sDate,sDate1,eDate,eDate1];
    CGSize timeLabelSize = [Utils getUIFontSizeFitW:timeLabel];
    timeLabel.width = timeLabelSize.width+8.0;
    timeLabel.left = (ScreenWidth-timeLabel.width-8.0)/2.0;
    //
    UITextView*contentTV = [[UITextView alloc]initWithFrame:CGRectMake(CellLeft, timeLabel.bottom+10.0, editScrollView.width-CellLeft*2.0, 70.0)];
    contentTV.font = font_13;
    contentTV.text = self.adCampaignEntity.idescription;
    contentTV.text = [contentTV.text stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    contentTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    contentTV.editable = NO;
    contentTV.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:contentTV];
    CGSize contentTVSize = [Utils getUITextViewSizeFitH:contentTV withLineSpacing:6.0];
    if (contentTVSize.height+6.0*2.0 < contentTV.height) {
        contentTV.height = contentTVSize.height+6.0*2.0;
    }
    //
    tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, contentTV.bottom+15.0, editScrollView.width, 18.0)];
    tagLabel.font = font_cu_15;
    tagLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tagLabel.textAlignment = NSTextAlignmentCenter;
    [editScrollView addSubview:tagLabel];
    //
    UILabel *lineGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, tagLabel.bottom+15.0, editScrollView.width, CellBottom)];
    lineGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [editScrollView addSubview:lineGapLabel];
    //
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineGapLabel.bottom, ScreenWidth, 50.0)];
    tLabel.text = NSLocalizedString(@"R6015", @"活动总预算");
    tLabel.font = font_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel];
    //
    UILabel*cLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, lineGapLabel.bottom, ScreenWidth-CellLeft, 50.0)];
    cLabel.font = font_cu_15;
    cLabel.textAlignment = NSTextAlignmentRight;
    cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:cLabel];
    NSString*budget = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:self.adCampaignEntity.budget]];
    cLabel.text = [NSString stringWithFormat:@"￥%@",budget];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel.bottom-0.5, ScreenWidth, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:lineLabel];
    //
    UIButton*editBtn0 = [UIButton buttonWithType:UIButtonTypeCustom];
    editBtn0.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    editBtn0.frame = CGRectMake(0, lineLabel.bottom, editScrollView.width, 50.0);
    [editBtn0 addTarget:self
                 action:@selector(editBtn0Action:)
       forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:editBtn0];
    //
    UILabel*tLabel0 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth, editBtn0.height)];
    tLabel0.text = NSLocalizedString(@"R6061", @"查看活动页面");
    tLabel0.font = font_15;
    tLabel0.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editBtn0 addSubview:tLabel0];
    //
    UILabel*arrowLabel0 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-CellLeft-13.0, (editBtn0.height-13.0)/2.0, 13.0, 13.0)];
    arrowLabel0.font = font_icon_(13.0);
    arrowLabel0.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    arrowLabel0.text = Icon_btn_right;
    [editBtn0 addSubview:arrowLabel0];
    //
    UILabel*lineLabel0 = [[UILabel alloc]initWithFrame:CGRectMake(0, editBtn0.height-0.5, ScreenWidth, 0.5)];
    lineLabel0.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editBtn0 addSubview:lineLabel0];
    //
    UIButton*editBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    editBtn1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    editBtn1.frame = CGRectMake(0, editBtn0.bottom, editScrollView.width, 50.0);
    [editBtn1 addTarget:self
                 action:@selector(editBtn1Action:)
       forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:editBtn1];
    //
    UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth, editBtn1.height)];
    tLabel1.text = NSLocalizedString(@"R6062", @"查看参与人员页面");
    tLabel1.font = font_15;
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editBtn1 addSubview:tLabel1];
    //
    UILabel*arrowLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-CellLeft-13.0, (editBtn1.height-13.0)/2.0, 13.0, 13.0)];
    arrowLabel1.font = font_icon_(13.0);
    arrowLabel1.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    arrowLabel1.text = Icon_btn_right;
    [editBtn1 addSubview:arrowLabel1];
    //
    UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, editBtn1.height-0.5, ScreenWidth, 0.5)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editBtn1 addSubview:lineLabel1];
    //
    UILabel *lineGapLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, editBtn1.bottom, editScrollView.width, CellBottom)];
    lineGapLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [editScrollView addSubview:lineGapLabel1];
    //
    moneyView = [[UIView alloc]initWithFrame:CGRectMake(0, lineGapLabel1.bottom, editScrollView.width, 62.0)];
    moneyView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:moneyView];
    //
    UILabel *lineGapLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, moneyView.bottom, editScrollView.width, CellBottom)];
    lineGapLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [editScrollView addSubview:lineGapLabel2];
    //
    UIView*lineView = [[UIView alloc]initWithFrame:CGRectMake(0, lineGapLabel2.bottom, editScrollView.width, 50.0)];
    lineView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:lineView];
    //
    UILabel*cLabel21 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth, lineView.height)];
    cLabel21.font = font_cu_cu_13;
    cLabel21.textColor = [Utils getUIColorWithHexString:@"62e6fb"];
    cLabel21.text = [NSString stringWithFormat:@"■%@",NSLocalizedString(@"R6005", @"总点击数")];
    [lineView addSubview:cLabel21];
    CGSize cLabel21Size = [Utils getUIFontSizeFitW:cLabel21];
    cLabel21.width = cLabel21Size.width;
    //
    UILabel*cLabel22 = [[UILabel alloc] initWithFrame:CGRectMake(cLabel21.right+10.0, 0, ScreenWidth, lineView.height)];
    cLabel22.font = font_cu_cu_13;
    cLabel22.textColor = [Utils getUIColorWithHexString:@"f98b33"];
    cLabel22.text = [NSString stringWithFormat:@"■%@",NSLocalizedString(@"R6006", @"计费点击")];
    [lineView addSubview:cLabel22];
    CGSize cLabel22Size = [Utils getUIFontSizeFitW:cLabel22];
    cLabel22.width = cLabel22Size.width;
    //
    UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, lineView.height-0.5, ScreenWidth, 0.5)];
    lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    //    [lineView addSubview:lineLabel2];
    //
    lineGraphView = [[SHLineGraphView alloc] initWithFrame:CGRectMake(0, lineView.bottom-10.0, ScreenWidth, 280)];
    lineGraphView.backgroundColor = [UIColor whiteColor];
    lineGraphView.themeAttributes = @{
                                      kXAxisLabelColorKey:[Utils getUIColorWithHexString:SysColorSubGray],
                                      kXAxisLabelFontKey:font_11,
                                      kYAxisLabelColorKey:[Utils getUIColorWithHexString:SysColorSubGray],
                                      kYAxisLabelFontKey:font_11,
                                      kYAxisLabelSideMarginsKey:@15,
                                      kPlotBackgroundLineColorKye:[Utils getUIColorWithHexString:SysColorSubGray]
                                      };
    lineGraphView.yAxisSuffix = @"";
    [editScrollView addSubview:lineGraphView];
    linePlot = [[SHPlot alloc]init];
    linePlot.plotThemeAttributes = @{
                                     kPlotFillColorKey:[Utils getUIColorWithHexString:@"91eefc"],
                                     kPlotStrokeWidthKey:@2,
                                     kPlotStrokeColorKey:[Utils getUIColorWithHexString:@"62e6fb"],
                                     kPlotPointFillColorKey:[Utils getUIColorWithHexString:@"62e6fb"],
                                     kPlotPointValueFontKey:font_11
                                     };
    linePlot1 = [[SHPlot alloc]init];
    linePlot1.plotThemeAttributes = @{
                                      kPlotFillColorKey:[Utils getUIColorWithHexString:@"fbae71"],
                                      kPlotStrokeWidthKey:@2,
                                      kPlotStrokeColorKey:[Utils getUIColorWithHexString:@"f98b33"],
                                      kPlotPointFillColorKey:[Utils getUIColorWithHexString:@"f98b33"],
                                      kPlotPointValueFontKey:font_11
                                      };
    //
    lineGraphViewLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, lineGraphView.bottom, ScreenWidth, 36.0)];
    lineGraphViewLabel.textAlignment = NSTextAlignmentCenter;
    lineGraphViewLabel.numberOfLines = 0;
    lineGraphViewLabel.text = @"";
    lineGraphViewLabel.font = font_11;
    lineGraphViewLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
    [editScrollView addSubview:lineGraphViewLabel];
    //
    [editScrollView setContentSize:CGSizeMake(editScrollView.width, lineGraphViewLabel.bottom)];
}

#pragma mark - Timer Delegate
- (void)updateTimer {
    tempDate = [NSDate dateWithTimeInterval:-60 sinceDate:tempDate];
    [self dateStatus];
}

-(void)dateStatus {
    NSTimeInterval late1 = [[NSDate date] timeIntervalSince1970]*1;
    NSTimeInterval late2 = [tempDate timeIntervalSince1970]*1;
    NSTimeInterval cha = late2-late1;
    if((int)cha<=0) {
        lineGraphViewLabel.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"R6070", @"为保证用户参与数据的准确性，Robin8将在活动结束的4天后为您进行结算")];
        [updateTimer invalidate];
        updateTimer = nil;
        return;
    }
    NSString *timeString=@"";
    NSString *day=@"";
    NSString *house=@"";
    NSString *min=@"";
    NSString *sen=@"";
    //秒
    sen = [NSString stringWithFormat:@"%d", (int)cha%60];
    sen=[NSString stringWithFormat:@"%@", sen];
    //分
    min = [NSString stringWithFormat:@"%d", (int)cha/60%60];
    min=[NSString stringWithFormat:@"%@", min];
    //小时
    house = [NSString stringWithFormat:@"%d", (int)cha/3600];
    house = [NSString stringWithFormat:@"%@", house];
    //天
    if(house.intValue>=24){
        day = [NSString stringWithFormat:@"%d", house.intValue/24];
        house = [NSString stringWithFormat:@"%d", house.intValue-24*day.intValue];
        timeString = [NSString stringWithFormat:NSLocalizedString(@"R6073",@"结算倒计时 %@天%@小时%@分"),day,house,min];
    } else {
        timeString = [NSString stringWithFormat:NSLocalizedString(@"R6078",@"结算倒计时 %@小时%@分"),house,min];
    }
    lineGraphViewLabel.text = [NSString stringWithFormat:@"%@\n%@",NSLocalizedString(@"R6070", @"为保证用户参与数据的准确性，Robin8将在活动结束的4个工作日后为您进行结算"),timeString];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"kol_campaigns/detail"]) {
        [self.hudView dismiss];
        self.navView.hidden = YES;
        self.adCampaignEntity = [JsonService getRBAdvertiserCampaignEntity:jsonObject];
        //
        [self loadEditView];
        if([self.adCampaignEntity.status isEqualToString:@"executed"]){
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            tempDate = [formatter dateFromString:self.adCampaignEntity.cal_settle_time];
            [self dateStatus];
            updateTimer = [NSTimer scheduledTimerWithTimeInterval:60
                                                         target  :self
                                                         selector:@selector(updateTimer)
                                                         userInfo:nil
                                                         repeats :YES];
        }
        //
        NSString*per_action_budget = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:self.adCampaignEntity.per_action_budget]];
        NSString*per_budget_type = [NSString stringWithFormat:@"%@",self.adCampaignEntity.per_budget_type];
        if ([per_budget_type isEqualToString:@"click"]||[per_budget_type isEqualToString:@"cpa"]) {
            if ([per_budget_type isEqualToString:@"click"]) {
                tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2014",@"点击"),per_action_budget];
            }
            if ([per_budget_type isEqualToString:@"cpa"]) {
                tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2015",@"效果"),per_action_budget];
            }
            //
            NSArray*moneyArray = @[NSLocalizedString(@"R6063", @"已花费"),NSLocalizedString(@"R6064", @"参与人数"),NSLocalizedString(@"R6005", @"总点击数"),NSLocalizedString(@"R6006", @"计费点击")];
            NSArray*moneyArray1 = @[[NSString stringWithFormat:@"￥%@",[Utils getNSStringTwoFloat:self.adCampaignEntity.take_budget]],self.adCampaignEntity.share_times,self.adCampaignEntity.total_click,self.adCampaignEntity.avail_click];
            for (int i=0; i<moneyArray.count; i++) {
                UILabel*cLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*ScreenWidth/4.0, 14.0, ScreenWidth/4.0, 14.0)];
                cLabel.textAlignment = NSTextAlignmentCenter;
                cLabel.text = moneyArray[i];
                cLabel.font = font_11;
                cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
                [moneyView addSubview:cLabel];
                //
                UILabel*cLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(cLabel.left, cLabel.bottom+5.0, cLabel.width, 16.0)];
                cLabel1.textAlignment = NSTextAlignmentCenter;
                cLabel1.text = moneyArray1[i];
                cLabel1.font = font_cu_cu_(13.0);
                cLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
                [moneyView addSubview:cLabel1];
            }
        } else if ([per_budget_type isEqualToString:@"simple_cpi"]||[per_budget_type isEqualToString:@"cpt"]||[per_budget_type isEqualToString:@"recruit"]||[per_budget_type isEqualToString:@"invite"]) {
            if ([per_budget_type isEqualToString:@"cpt"]) {
                tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2301",@"任务"),per_action_budget];
            }
            if ([per_budget_type isEqualToString:@"simple_cpi"]) {
                tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2200",@"下载"),per_action_budget];
            }
            if ([per_budget_type isEqualToString:@"recruit"]) {
                tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2016",@"招募"),per_action_budget];
            }
            if ([per_budget_type isEqualToString:@"invite"]) {
                tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2107",@"特邀"),per_action_budget];
            }
            //
            NSArray*moneyArray = @[NSLocalizedString(@"R6063", @"已花费"),NSLocalizedString(@"R6064", @"参与人数")];
            NSArray*moneyArray1 = @[[NSString stringWithFormat:@"￥%@",[Utils getNSStringTwoFloat:self.adCampaignEntity.take_budget]],self.adCampaignEntity.share_times];
            for (int i=0; i<moneyArray.count; i++) {
                UILabel*cLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*ScreenWidth/2.0, 14.0, ScreenWidth/2.0, 14.0)];
                cLabel.textAlignment = NSTextAlignmentCenter;
                cLabel.text = moneyArray[i];
                cLabel.font = font_11;
                cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
                [moneyView addSubview:cLabel];
                //
                UILabel*cLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(cLabel.left, cLabel.bottom+5.0, cLabel.width, 16.0)];
                cLabel1.textAlignment = NSTextAlignmentCenter;
                cLabel1.text = moneyArray1[i];
                cLabel1.font = font_cu_cu_(13.0);
                cLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
                [moneyView addSubview:cLabel1];
            }
        } else {
            tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2017",@"转发"),per_action_budget];
            //
            NSArray*moneyArray = @[NSLocalizedString(@"R6063", @"已花费"),NSLocalizedString(@"R6064", @"参与人数"),NSLocalizedString(@"R6005", @"总点击数")];
            NSArray*moneyArray1 = @[[NSString stringWithFormat:@"￥%@",[Utils getNSStringTwoFloat:self.adCampaignEntity.take_budget]],self.adCampaignEntity.share_times,self.adCampaignEntity.total_click];
            for (int i=0; i<moneyArray.count; i++) {
                UILabel*cLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*ScreenWidth/3.0, 14.0, ScreenWidth/3.0, 14.0)];
                cLabel.textAlignment = NSTextAlignmentCenter;
                cLabel.text = moneyArray[i];
                cLabel.font = font_11;
                cLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
                [moneyView addSubview:cLabel];
                //
                UILabel*cLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(cLabel.left, cLabel.bottom+5.0, cLabel.width, 16.0)];
                cLabel1.textAlignment = NSTextAlignmentCenter;
                cLabel1.text = moneyArray1[i];
                cLabel1.font = font_cu_cu_(13.0);
                cLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
                [moneyView addSubview:cLabel1];
            }
        }
        if(![self.adCampaignEntity.stats_data isEqual:[NSNull null]]&&[self.adCampaignEntity.stats_data count]!=0){
            NSArray*dateArray = self.adCampaignEntity.stats_data[0];
            NSMutableArray*array = [NSMutableArray new];
            for (int i=0; i<[dateArray count]; i++) {
                NSDictionary*dic = @{[NSString stringWithFormat:@"%d",i]:[NSString stringWithFormat:@"%@",dateArray[i]]};
                [array addObject:dic];
            }
            lineGraphView.xAxisValues = array;
            //
            NSArray*clicksArray = self.adCampaignEntity.stats_data[1];
            int max = 9;
            NSMutableArray*array1 = [NSMutableArray new];
            NSMutableArray*array11 = [NSMutableArray new];
            for (int i=0; i<[clicksArray count]; i++) {
                NSNumber*num = [NSNumber numberWithFloat:[NSString stringWithFormat:@"%@",clicksArray[i]].floatValue];
                NSDictionary*dic = @{[NSString stringWithFormat:@"%d",i]:num};
                if ([NSString stringWithFormat:@"%@",clicksArray[i]].intValue>max) {
                    max = [NSString stringWithFormat:@"%@",clicksArray[i]].intValue;
                }
                [array1 addObject:dic];
                [array11 addObject:[NSString stringWithFormat:@"%@",clicksArray[i]]];
            }
            linePlot.plottingValues = array1;
            linePlot.plottingPointsLabels = array11;
            [lineGraphView addPlot:linePlot];
            //
            if([self.adCampaignEntity.stats_data count]>2){
                NSArray*clicksArray1 = self.adCampaignEntity.stats_data[2];
                NSMutableArray*array2 = [NSMutableArray new];
                NSMutableArray*array21 = [NSMutableArray new];
                for (int i=0; i<[clicksArray1 count]; i++) {
                    NSNumber*num = [NSNumber numberWithFloat:[NSString stringWithFormat:@"%@",clicksArray1[i]].floatValue];
                    NSDictionary*dic = @{[NSString stringWithFormat:@"%d",i]:num};
                    [array2 addObject:dic];
                    [array21 addObject:[NSString stringWithFormat:@"%@",clicksArray1[i]]];
                }
                linePlot1.plottingValues = array2;
                linePlot1.plottingPointsLabels = array21;
                [lineGraphView addPlot:linePlot1];
            }
            //
            lineGraphView.yAxisRange = [NSNumber numberWithFloat:max*1.2];
            [lineGraphView setupTheView];
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
@end

