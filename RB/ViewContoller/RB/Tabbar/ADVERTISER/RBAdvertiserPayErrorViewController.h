//
//  RBAdvertiserPayErrorViewController.h
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBAdvertiserMyViewController.h"
@interface RBAdvertiserPayErrorViewController : RBBaseViewController
<RBBaseVCDelegate>

@end
