//
//  RBAdvertiserRechargeSuccessedViewController.h
//  RB
//
//  Created by AngusNi on 6/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBAdvertiserMyViewController.h"
@interface RBAdvertiserRechargeSuccessedViewController : RBBaseViewController
<RBBaseVCDelegate>

@end
