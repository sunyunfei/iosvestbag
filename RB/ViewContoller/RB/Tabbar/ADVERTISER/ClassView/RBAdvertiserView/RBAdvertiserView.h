//
//  RBAdvertiserView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBAdvertiserTableViewCell.h"

@class RBAdvertiserView;
@protocol RBAdvertiserViewDelegate <NSObject>
@optional
- (void)RBAdvertiserViewLoadRefreshViewData:(RBAdvertiserView *)view;
- (void)RBAdvertiserViewSelected:(RBAdvertiserView *)view andIndex:(int)index;
- (void)RBAdvertiserViewCancelBtnSelected:(RBAdvertiserView *)view andIndex:(int)index;
- (void)RBAdvertiserViewAgainBtnSelected:(RBAdvertiserView *)view andIndex:(int)index;
@end


@interface RBAdvertiserView : UIView
<UITableViewDataSource,UITableViewDelegate,HandlerDelegate>
@property(nonatomic, weak) id<RBAdvertiserViewDelegate> delegate;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) UIView *defaultView;
@property(nonatomic, strong) NSMutableArray *datalist;
@property(nonatomic, strong) NSString *type;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int pageSize;
- (void)RBAdvertiserViewLoadRefreshViewFirstData;
- (void)RBAdvertiserViewSetRefreshViewFinish;
@end
