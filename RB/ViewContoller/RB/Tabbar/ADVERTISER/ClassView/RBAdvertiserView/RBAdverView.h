//
//  RBAdverView.h
//  RB
//
//  Created by RB8 on 2018/5/22.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFiledKeyBoard.h"
@interface RBAdverView : UIView<UITextFieldDelegate,TextFiledKeyBoardDelegate>
@property(nonatomic,strong)UILabel * titleLabel;
@property(nonatomic,strong)UITextField * textField;
@property(nonatomic,strong)NSString * title;
@property(nonatomic,strong)NSString * placeHolder;
@end
