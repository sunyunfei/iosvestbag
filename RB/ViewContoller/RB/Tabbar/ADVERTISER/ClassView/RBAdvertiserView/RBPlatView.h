//
//  RBPlatView.h
//  RB
//
//  Created by RB8 on 2017/9/25.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RBPlatViewDelegate <NSObject>
-(void)upDelegateAcion;
@end
@interface RBPlatView : UIView
@property (weak, nonatomic) IBOutlet UIButton *friendsButton;
- (IBAction)friendsAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *weiboButton;
- (IBAction)weiboAction:(id)sender;
- (IBAction)upAction:(id)sender;
@property(nonatomic,weak)id<RBPlatViewDelegate>delegate;
@property(nonatomic,assign)BOOL wechatSelected;
@property(nonatomic,assign)BOOL weiboSelected;
@end
