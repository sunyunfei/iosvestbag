//
//  RBPlatView.m
//  RB
//
//  Created by RB8 on 2017/9/25.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBPlatView.h"

@implementation RBPlatView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib{
    [super awakeFromNib];
    _wechatSelected = NO;
    _weiboSelected = NO;
    _weiboButton.layer.borderWidth = 1.0;
    _weiboButton.layer.cornerRadius = 2;
    _weiboButton.clipsToBounds = YES;
    _weiboButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
    _friendsButton.layer.borderWidth = 1.0;
    _friendsButton.layer.cornerRadius = 2;
    _friendsButton.clipsToBounds = YES;
    _friendsButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
}
- (IBAction)friendsAction:(id)sender {
    _wechatSelected = !_wechatSelected;
    if (_wechatSelected == YES) {
        [_friendsButton setImage:[UIImage imageNamed:@"RBPlat"] forState:UIControlStateNormal];
    }else if (_wechatSelected == NO){
        [_friendsButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
}
- (IBAction)weiboAction:(id)sender {
    _weiboSelected = !_weiboSelected;
    if (_weiboSelected == YES) {
        [_weiboButton setImage:[UIImage imageNamed:@"RBPlat"] forState:UIControlStateNormal];
    }else if (_weiboSelected == NO){
        [_weiboButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
}

- (IBAction)upAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(upDelegateAcion)]) {
        [self.delegate upDelegateAcion];
    }
}
@end
