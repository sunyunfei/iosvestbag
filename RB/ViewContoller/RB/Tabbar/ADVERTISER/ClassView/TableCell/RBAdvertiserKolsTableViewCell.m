//
//  RBAdvertiserKolsTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBAdvertiserKolsTableViewCell.h"
#import "Service.h"

@implementation RBAdvertiserKolsTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UIImageView*tIV = [[UIImageView alloc]initWithFrame:CGRectZero];
        tIV.tag = 1001;
        [self.contentView addSubview:tIV];
        //
        TTTAttributedLabel*tLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1003;
        [self.contentView addSubview:tLabel];
        //
        TTTAttributedLabel*tLabel1 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        tLabel1.font = font_11;
        tLabel1.textAlignment = NSTextAlignmentCenter;
        tLabel1.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        tLabel1.tag = 1004;
        [self.contentView addSubview:tLabel1];
        //
        TTTAttributedLabel*tLabel2 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        tLabel2.font = font_cu_13;
        tLabel2.textAlignment = NSTextAlignmentCenter;
        tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel2.tag = 1005;
        [self.contentView addSubview:tLabel2];
        //
        TTTAttributedLabel*tLabel3 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        tLabel3.font = font_11;
        tLabel3.textAlignment = NSTextAlignmentCenter;
        tLabel3.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        tLabel3.tag = 1006;
        [self.contentView addSubview:tLabel3];
        //
        TTTAttributedLabel*tLabel4 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        tLabel4.font = font_cu_13;
        tLabel4.textAlignment = NSTextAlignmentCenter;
        tLabel4.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel4.tag = 1007;
        [self.contentView addSubview:tLabel4];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1010;
        [self.contentView addSubview:lineLabel];
    }
    return self;
}

- (instancetype)loadRBAdvertiserKolsCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UIImageView*tIV=(UIImageView*)[self.contentView viewWithTag:1001];
    TTTAttributedLabel*tLabel=(TTTAttributedLabel*)[self.contentView viewWithTag:1003];
    TTTAttributedLabel*tLabel1=(TTTAttributedLabel*)[self.contentView viewWithTag:1004];
    TTTAttributedLabel*tLabel2=(TTTAttributedLabel*)[self.contentView viewWithTag:1005];
    TTTAttributedLabel*tLabel3=(TTTAttributedLabel*)[self.contentView viewWithTag:1006];
    TTTAttributedLabel*tLabel4=(TTTAttributedLabel*)[self.contentView viewWithTag:1007];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    //
    RBUserEntity*entity = datalist[indexPath.row];
    [tIV sd_setImageWithURL:[NSURL URLWithString:entity.avatar_url] placeholderImage:PlaceHolderUserImage];
    tLabel.text = entity.kol_name;
    tLabel1.text = NSLocalizedString(@"R6005", @"总点击数");
    tLabel2.text = entity.total_click;
    tLabel3.text = NSLocalizedString(@"R6006", @"计费点击");
    tLabel4.text = entity.avail_click;
    //
    tIV.frame = CGRectMake(CellLeft, CellLeft, 50, 50);
    tIV.layer.cornerRadius = tIV.width/2.0;
    tIV.layer.masksToBounds = YES;
    tLabel.frame = CGRectMake(tIV.right+CellLeft, tIV.top, 190.0, tIV.height);
    tLabel1.frame = CGRectMake(ScreenWidth-80.0*2.0-CellLeft, 0, 80.0, 14.0);
    CGSize tLabel1Size = [Utils getUIFontSizeFitW:tLabel1];
    tLabel1.frame = CGRectMake(ScreenWidth-(tLabel1Size.width+5.0)*2.0-CellLeft, CellLeft+10.0, tLabel1Size.width+5.0, 14.0);
    tLabel2.frame = CGRectMake(tLabel1.left, tLabel1.bottom, tLabel1.width, 17.0);
    tLabel3.frame = CGRectMake(tLabel1.right, tLabel1.top, tLabel1.width, tLabel1.height);
    tLabel4.frame = CGRectMake(tLabel3.left, tLabel3.bottom, tLabel3.width, tLabel2.height);
    lineLabel.frame = CGRectMake(0, tIV.bottom+tIV.top, ScreenWidth, 0.5);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    return self;
}

@end
