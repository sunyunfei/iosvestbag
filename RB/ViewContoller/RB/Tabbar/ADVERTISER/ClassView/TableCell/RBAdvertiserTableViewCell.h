//
//  RBAdvertiserTableViewCell.h
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBButton.h"
@interface RBAdvertiserTableViewCell : UITableViewCell
@property(nonatomic, strong)  RBButton *cancelBtn;
@property(nonatomic, strong)  RBButton *againBtn;

- (instancetype)loadRBAdvertiserCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath;
@end
