//
//  RBAdvertiserTableViewCell.m
//
//  Created by AngusNi on 15/7/9.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBAdvertiserTableViewCell.h"
#import "Service.h"

@implementation RBAdvertiserTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        //
        UIView*bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.userInteractionEnabled = YES;
        bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        bgView.tag = 1000;
        [self.contentView addSubview:bgView];
        //
        UIImageView*tIV = [[UIImageView alloc]initWithFrame:CGRectZero];
        tIV.tag = 1001;
        [self.contentView addSubview:tIV];
        //
        TTTAttributedLabel*tLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        tLabel.font = font_15;
        tLabel.numberOfLines = 2;
        tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel.tag = 1003;
        [self.contentView addSubview:tLabel];
        //
        TTTAttributedLabel*tLabel1 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        tLabel1.font = font_11;
        tLabel1.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        tLabel1.tag = 1004;
        [self.contentView addSubview:tLabel1];
        //
        TTTAttributedLabel*tLabel2 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        tLabel2.font = font_13;
        tLabel2.textAlignment = NSTextAlignmentRight;
        tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel2.tag = 1005;
        [self.contentView addSubview:tLabel2];
        //
        TTTAttributedLabel*tLabel3 = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        tLabel3.font = font_cu_13;
        tLabel3.textAlignment = NSTextAlignmentCenter;
        tLabel3.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        tLabel3.tag = 1006;
        [self.contentView addSubview:tLabel3];
        //
        self.cancelBtn = [RBButton buttonWithType:UIButtonTypeCustom];
        [self.cancelBtn setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        [self.cancelBtn setTitle:NSLocalizedString(@"R6079", @"取消活动") forState:UIControlStateNormal];
        self.cancelBtn.titleLabel.font = font_cu_13;
        self.cancelBtn.tag = 1007;
        [self.contentView addSubview:self.cancelBtn];
        //
        self.againBtn = [RBButton buttonWithType:UIButtonTypeCustom];
        [self.againBtn setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
        [self.againBtn setTitle:NSLocalizedString(@"R6200", @"再次发起") forState:UIControlStateNormal];
        self.againBtn.titleLabel.font = font_cu_13;
        self.againBtn.tag = 1008;
        [self.contentView addSubview:self.againBtn];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel.tag = 1010;
        [self.contentView addSubview:lineLabel];
        //
        UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        lineLabel1.tag = 1011;
        [self.contentView addSubview:lineLabel1];
        //
        UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        lineLabel2.tag = 1012;
        [self.contentView addSubview:lineLabel2];
        //
        UILabel*lineLabel3 = [[UILabel alloc]initWithFrame:CGRectZero];
        lineLabel3.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        lineLabel3.tag = 1013;
        [self.contentView addSubview:lineLabel3];
    }
    return self;
}

- (instancetype)loadRBAdvertiserCellWith:(NSMutableArray*)datalist andIndex:(NSIndexPath*)indexPath {
    UIView*bgView=(UIView*)[self.contentView viewWithTag:1000];
    UIImageView*tIV=(UIImageView*)[self.contentView viewWithTag:1001];
    TTTAttributedLabel*tLabel=(TTTAttributedLabel*)[self.contentView viewWithTag:1003];
    TTTAttributedLabel*tLabel1=(TTTAttributedLabel*)[self.contentView viewWithTag:1004];
    TTTAttributedLabel*tLabel2=(TTTAttributedLabel*)[self.contentView viewWithTag:1005];
    TTTAttributedLabel*tLabel3=(TTTAttributedLabel*)[self.contentView viewWithTag:1006];
    self.cancelBtn=(RBButton*)[self.contentView viewWithTag:1007];
    self.againBtn=(RBButton*)[self.contentView viewWithTag:1008];
    UILabel*lineLabel=(UILabel*)[self.contentView viewWithTag:1010];
    UILabel*lineLabel1=(UILabel*)[self.contentView viewWithTag:1011];
    UILabel*lineLabel2=(UILabel*)[self.contentView viewWithTag:1012];
    UILabel*lineLabel3=(UILabel*)[self.contentView viewWithTag:1013];
    //
    if ([datalist count]>0) {
        RBAdvertiserCampaignEntity *entity = datalist[indexPath.row];
        [tIV sd_setImageWithURL:[NSURL URLWithString:entity.img_url] placeholderImage:PlaceHolderImage];
        tLabel.text = entity.name;
        NSString *sDate = [[NSString stringWithFormat:@"%@",entity.start_time]componentsSeparatedByString:@"T"][0];
        NSString *sDate2 = [sDate componentsSeparatedByString:@"-"][1];
        NSString *sDate3 = [sDate componentsSeparatedByString:@"-"][2];
        sDate = [NSString stringWithFormat:@"%@.%@",sDate2,sDate3];
        NSString *sDate1 = [[NSString stringWithFormat:@"%@",entity.start_time]componentsSeparatedByString:@"T"][1];
        sDate1 = [[NSString stringWithFormat:@"%@",sDate1]componentsSeparatedByString:@"+"][0];
        NSString *eDate = [[NSString stringWithFormat:@"%@",entity.deadline]componentsSeparatedByString:@"T"][0];
        NSString *eDate2 = [eDate componentsSeparatedByString:@"-"][1];
        NSString *eDate3 = [eDate componentsSeparatedByString:@"-"][2];
        eDate = [NSString stringWithFormat:@"%@.%@",eDate2,eDate3];
        NSString *eDate1 = [[NSString stringWithFormat:@"%@",entity.deadline]componentsSeparatedByString:@"T"][1];
        eDate1 = [[NSString stringWithFormat:@"%@",eDate1]componentsSeparatedByString:@"+"][0];
        tLabel1.text = [NSString stringWithFormat:@"%@ %@ - %@ %@",sDate,sDate1,eDate,eDate1];
        tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6002", @"活动预算"),[Utils getNSStringTwoFloat:entity.budget]];
        NSString*status = [NSString stringWithFormat:@"%@",entity.status];
        lineLabel3.hidden = YES;
        self.cancelBtn.hidden = YES;
        self.againBtn.hidden = YES;
        if ([status isEqualToString:@"unpay"]) {
            tLabel2.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6002", @"活动预算"),[Utils getNSStringTwoFloat:entity.need_pay_amount]];
            tLabel3.text = NSLocalizedString(@"R6003", @"立即付款");
            tLabel3.textColor = [Utils getUIColorWithHexString:SysColorBlack];
            lineLabel3.hidden = NO;
            self.againBtn.hidden = YES;
            self.cancelBtn.hidden = NO;
        } else if ([status isEqualToString:@"unexecute"]) {
            tLabel3.text = NSLocalizedString(@"R2040", @"审核中");
            tLabel3.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            self.cancelBtn.hidden = NO;
            self.againBtn.hidden = YES;
        } else if ([status isEqualToString:@"rejected"]) {
            tLabel3.text = NSLocalizedString(@"R2041", @"审核拒绝");
            tLabel3.textColor = [Utils getUIColorWithHexString:SysColorRed];
            self.cancelBtn.hidden = NO;
            self.againBtn.hidden = YES;
        } else if ([status isEqualToString:@"agreed"]) {
            tLabel3.text = NSLocalizedString(@"R6075", @"等待活动开始");
            tLabel3.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            self.cancelBtn.hidden = YES;
            self.againBtn.hidden = YES;
        } else if ([status isEqualToString:@"executing"]) {
            tLabel3.text = NSLocalizedString(@"R2004",@"进行中");
            tLabel3.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            self.cancelBtn.hidden = YES;
            self.againBtn.hidden = NO;
        } else if ([status isEqualToString:@"executed"]) {
            tLabel3.text = NSLocalizedString(@"R6074", @"活动已完成，正在结算");
            tLabel3.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            self.cancelBtn.hidden = YES;
            self.againBtn.hidden = NO;
        } else if ([status isEqualToString:@"settled"]) {
            tLabel3.text = NSLocalizedString(@"R6004", @"已结算");
            tLabel3.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
            self.cancelBtn.hidden = YES;
            self.againBtn.hidden = NO;
        }
    }
    //
    lineLabel2.frame = CGRectMake(0, 0, ScreenWidth, CellBottom);
    tIV.frame = CGRectMake(CellLeft, lineLabel2.bottom+CellLeft, (73.0/9.0)*16.0, 73.0);
    tIV.layer.borderWidth = 0.5;
    tIV.layer.borderColor = [[Utils getUIColorWithHexString:SysColorSubGray]CGColor];
    tIV.contentMode = UIViewContentModeScaleAspectFill;
    tIV.layer.masksToBounds = YES;
    tLabel.frame = CGRectMake(tIV.right+CellBottom, tIV.top, ScreenWidth-CellLeft-(tIV.right+CellBottom), 1000.0);
    CGSize tLabelSize = [Utils getUIFontSizeFitH:tLabel];
    tLabel.height = tLabelSize.height;
    if (tLabelSize.height > 37) {
        tLabel.height = 38;
    }
    tLabel1.frame = CGRectMake(tLabel.left, tLabel.bottom+(tIV.height-30.0-tLabel.height)/2.0, tLabel.width, 14.0);
    tLabel2.frame = CGRectMake(tLabel.left, tLabel1.bottom+(tIV.height-30.0-tLabel.height)/2.0, tLabel.width, 16.0);
    lineLabel1.frame = CGRectMake(0, tIV.top+tIV.bottom-0.5, ScreenWidth, 0.5);
    tLabel3.frame = CGRectMake(0, lineLabel1.bottom, ScreenWidth, 37.0);
    if ([datalist count]>0) {
        RBAdvertiserCampaignEntity *entity = datalist[indexPath.row];
        NSString*status = [NSString stringWithFormat:@"%@",entity.status];
        if ([status isEqualToString:@"unpay"]||[status isEqualToString:@"unexecute"]||[status isEqualToString:@"rejected"]) {
            tLabel3.frame = CGRectMake(0, lineLabel1.bottom, ScreenWidth/2.0, 37.0);
            self.cancelBtn.frame = CGRectMake(ScreenWidth/2.0, lineLabel1.bottom, ScreenWidth/2.0, 37.0);
            lineLabel3.frame = CGRectMake(ScreenWidth/2.0, lineLabel1.bottom+3.5f, 0.5f, 30.0);
        }
        if ([status isEqualToString:@"executing"]||[status isEqualToString:@"executed"]||[status isEqualToString:@"settled"]) {
            tLabel3.frame = CGRectMake(0, lineLabel1.bottom, ScreenWidth/2.0, 37.0);
            self.againBtn.frame = CGRectMake(ScreenWidth/2.0, lineLabel1.bottom, ScreenWidth/2.0, 37.0);
            lineLabel3.frame = CGRectMake(ScreenWidth/2.0, lineLabel1.bottom+3.5f, 0.5f, 30.0);
        }
    }
    lineLabel.frame = CGRectMake(0, tLabel3.bottom-0.5, ScreenWidth, 0.5);
    bgView.frame = CGRectMake(0, 0, ScreenWidth, lineLabel.bottom);
    self.frame = CGRectMake(0, 0, ScreenWidth, bgView.bottom);
    UIView *cellBgView = [[UIView alloc] initWithFrame:self.frame];
    cellBgView.backgroundColor = [UIColor clearColor];
    self.backgroundView = cellBgView;
    return self;
}

@end
