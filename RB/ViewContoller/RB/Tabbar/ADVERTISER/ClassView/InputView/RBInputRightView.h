//
//  RBInputRightView.h
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InsetsTextField.h"
#import "TextFiledKeyBoard.h"
#import "DatePickerView.h"
#import "PickerViewKeyBoard.h"

@class RBInputRightView;
@protocol RBInputRightViewDelegate <NSObject>
-(void)RBInputRightViewPicker:(RBInputRightView*)view andValue:(NSString*)str;
-(void)RBInputRightViewInputFinished:(RBInputRightView*)view;

@optional
@end

@interface RBInputRightView : UIView
<TextFiledKeyBoardDelegate,DatePickerViewDelegate,PickerViewKeyBoardDelegate,UITextFieldDelegate>
@property(nonatomic, weak)   id<RBInputRightViewDelegate> delegate;
@property(nonatomic, strong) UILabel*inputLabel;
@property(nonatomic, strong) InsetsTextField*inputTF;
- (void)loadInputRightViewWithText:(NSString*)text andTitle:(NSString*)title andIsDate:(BOOL)isDate andIsSelect:(BOOL)isSelect andValues:(NSArray*)array;
@end