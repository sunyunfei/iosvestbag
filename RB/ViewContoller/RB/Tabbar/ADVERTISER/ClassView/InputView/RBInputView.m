//
//  RBInputView.m
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBInputView.h"
#import "Service.h"

@implementation RBInputView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.delegate = nil;
}

- (void)loadInputViewWithMaxNum:(NSString*)maxNum andText:(NSString*)text andLineNum:(int)lineNum {
    [self removeAllSubviews];
    self.maxNum = maxNum;
    if (lineNum>1 && self.inputTV==nil) {
        self.inputTV = [[PlaceholderTextView alloc]initWithFrame:CGRectMake(CellLeft, 0, self.width-CellLeft-60.0, 50.0*lineNum)];
        self.inputTV.font = font_15;
        self.inputTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.inputTV.keyboardType = UIKeyboardTypeDefault;
        self.inputTV.placeholderColor = [Utils getUIColorWithHexString:SysColorSubGray];
        self.inputTV.placeholderFont = font_15;
        self.inputTV.delegate = self;
        self.inputTV.userInteractionEnabled = YES;
        [self.inputTV addObserver:self forKeyPath:@"text" options:0 context:nil];
        [self addSubview:self.inputTV];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.inputTV.bottom-0.5, ScreenWidth, 0.5)];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self addSubview:lineLabel];
        //
        TextViewKeyBoard*textViewKB = [[TextViewKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
        textViewKB.delegateP = self;
        [self.inputTV setInputAccessoryView:textViewKB];
        //
        if (self.maxNum!=nil && self.inputNumLabel==nil) {
            self.inputNumLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.inputTV.height-50.0, self.width-CellLeft, 50.0)];
            self.inputNumLabel.font = font_13;
            self.inputNumLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
            self.inputNumLabel.textAlignment = NSTextAlignmentRight;
            [self addSubview:self.inputNumLabel];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNotification:) name:UITextViewTextDidChangeNotification object:nil];
        }
        //
        self.inputNumLabel.text = [NSString stringWithFormat:@"%d/%@",(int)self.inputTV.text.length,maxNum];
        self.inputTV.placeholder = [NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),text];
    }
    
    if (lineNum==1 && self.inputTF==nil) {
        self.inputTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(CellLeft, 0, self.width-CellLeft-60.0, self.height)];
        self.inputTF.font = font_15;
        self.inputTF.keyboardType =  UIKeyboardTypeDefault;
        self.inputTF.clearButtonMode = UITextFieldViewModeNever;
        self.inputTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self.inputTF addObserver:self forKeyPath:@"text" options:0 context:nil];
        [self addSubview:self.inputTF];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.inputTF.bottom-0.5, ScreenWidth, 0.5)];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self addSubview:lineLabel];
        //
        TextFiledKeyBoard *textFieldKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
        textFieldKB.delegateT = self;
        [self.inputTF setInputAccessoryView:textFieldKB];
        //
        if (self.maxNum!=nil && self.inputNumLabel==nil) {
            self.inputNumLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.width-CellLeft, self.height)];
            self.inputNumLabel.font = font_13;
            self.inputNumLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
            self.inputNumLabel.textAlignment = NSTextAlignmentRight;
            [self addSubview:self.inputNumLabel];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNotification:) name:UITextFieldTextDidChangeNotification object:nil];
        } else {
            self.inputTF.frame = CGRectMake(CellLeft, 0, self.width-2*CellLeft, self.height);
        }
        //
        self.inputNumLabel.text = [NSString stringWithFormat:@"%d/%@",(int)self.inputTF.text.length,maxNum];
        self.inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),text] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    }
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    if ([self.delegate
         respondsToSelector:@selector(RBInputViewInputFinished:)]) {
        [self.delegate RBInputViewInputFinished:self];
    }
    [self.inputTF resignFirstResponder];
    [self.inputTV resignFirstResponder];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}

- (void)TextViewKeyBoardFinish:(TextViewKeyBoard *)sender {
    [self endEdit];
}

#pragma mark - kvo Delegate
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"text"]) {
        if (self.inputTF) {
            if (self.maxNum) {
                self.inputNumLabel.text = [NSString stringWithFormat:@"%d/%@",(int)self.inputTF.text.length,self.maxNum];
                if (self.inputTF.text.length>self.maxNum.intValue) {
                    self.inputTF.text = [self.inputTF.text substringToIndex:self.maxNum.intValue];
                }
            }
        }
        if (self.inputTV) {
            if (self.maxNum) {
                self.inputNumLabel.text = [NSString stringWithFormat:@"%d/%@",(int)self.inputTV.text.length,self.maxNum];
                if (self.inputTV.text.length>self.maxNum.intValue) {
                    self.inputTV.text = [self.inputTV.text substringToIndex:self.maxNum.intValue];
                }
            }
        }
    }
}

#pragma mark - NSNotification Delegate
-(void)didChangeNotification:(NSNotification*)notification {
    if (self.inputTF) {
        if (self.maxNum) {
            self.inputNumLabel.text = [NSString stringWithFormat:@"%d/%@",(int)self.inputTF.text.length,self.maxNum];
            if (self.inputTF.text.length>self.maxNum.intValue) {
                self.inputTF.text = [self.inputTF.text substringToIndex:self.maxNum.intValue];
            }
        }
    }
    if (self.inputTV) {
        if (self.maxNum) {
            self.inputNumLabel.text = [NSString stringWithFormat:@"%d/%@",(int)self.inputTV.text.length,self.maxNum];
            if (self.inputTV.text.length>self.maxNum.intValue) {
                self.inputTV.text = [self.inputTV.text substringToIndex:self.maxNum.intValue];
            }
        }
    }
}

@end


