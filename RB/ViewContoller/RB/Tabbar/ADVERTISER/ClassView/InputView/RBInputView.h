//
//  RBInputView.h
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InsetsTextField.h"
#import "TextFiledKeyBoard.h"
#import "TextViewKeyBoard.h"
#import "PlaceholderTextView.h"

@class RBInputView;
@protocol RBInputViewDelegate <NSObject>
@optional
-(void)RBInputViewInputFinished:(RBInputView*)view;
@end

@interface RBInputView : UIView
<TextFiledKeyBoardDelegate,TextViewKeyBoardDelegate,UITextViewDelegate,UITextFieldDelegate>
@property(nonatomic, weak)   id<RBInputViewDelegate> delegate;
@property(nonatomic, strong) UILabel*inputNumLabel;
@property(nonatomic, strong) InsetsTextField*inputTF;
@property(nonatomic, strong) PlaceholderTextView*inputTV;
@property(nonatomic, strong) NSString*maxNum;

- (void)loadInputViewWithMaxNum:(NSString*)maxNum andText:(NSString*)text andLineNum:(int)lineNum;

@end