//
//  RBInputRightView.m
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBInputRightView.h"
#import "Service.h"

@implementation RBInputRightView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        self.userInteractionEnabled = YES;
        //
        self.inputLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth, frame.size.height)];
        self.inputLabel.font = font_15;
        self.inputLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self addSubview:self.inputLabel];
        //
        self.inputTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth-CellLeft-13.0, frame.size.height)];
        self.inputTF.font = font_15;
        self.inputTF.textAlignment = NSTextAlignmentRight;
        self.inputTF.keyboardType =  UIKeyboardTypeDefault;
        self.inputTF.clearButtonMode = UITextFieldViewModeNever;
        self.inputTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        [self addSubview:self.inputTF];
        //
        UILabel*arrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-CellLeft-13.0, (frame.size.height-13.0)/2.0, 13.0, 13.0)];
        arrowLabel.font = font_icon_(13.0);
        arrowLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        arrowLabel.text = Icon_btn_right;
        arrowLabel.tag = 1007;
        [self addSubview:arrowLabel];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.inputTF.bottom-0.5, ScreenWidth, 0.5)];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self addSubview:lineLabel];

    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.delegate = nil;
}

- (void)loadInputRightViewWithText:(NSString*)text andTitle:(NSString*)title andIsDate:(BOOL)isDate andIsSelect:(BOOL)isSelect andValues:(NSArray*)array {
    if (isDate==YES) {
        DatePickerView *textFieldPV = [[DatePickerView alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
        textFieldPV.delegate = self;
        textFieldPV.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        [self.inputTF setInputView:textFieldPV];
    } else if (isSelect==YES) {
        PickerViewKeyBoard *pickerViewKB = [[PickerViewKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
        pickerViewKB.delegate = self;
        pickerViewKB.values = array;
        [self.inputTF setInputView:pickerViewKB];
    } else {
        TextFiledKeyBoard *textFieldKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
        textFieldKB.delegateT = self;
        [self.inputTF setInputAccessoryView:textFieldKB];
    }
    self.inputLabel.text = title;
    self.inputTF.text = text;
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.inputTF resignFirstResponder];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
    if ([self.delegate
         respondsToSelector:@selector(RBInputRightViewInputFinished:)]) {
        [self.delegate RBInputRightViewInputFinished:self];
    }
}

- (void)DatePickerViewFinish:(DatePickerView *)sender {
    [self endEdit];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.inputTF.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:sender.datePicker.date]];
    if ([self.delegate
         respondsToSelector:@selector(RBInputRightViewInputFinished:)]) {
        [self.delegate RBInputRightViewInputFinished:self];
    }
}

- (void)PickerViewKeyBoardFinish:(PickerViewKeyBoard *)sender {
    [self endEdit];
    self.inputTF.text = [NSString stringWithFormat:@"%@",sender.value];
    if ([self.delegate
         respondsToSelector:@selector(RBInputRightViewPicker:andValue:)]) {
        [self.delegate RBInputRightViewPicker:self andValue:self.inputTF.text];
    }
}

@end


