//
//  RBAdvertiserSwitchView.m
//  RB
//
//  Created by AngusNi on 3/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserSwitchView.h"
@implementation RBAdvertiserSwitchView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        self.frame = frame;
        //
        self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.leftBtn.frame = CGRectMake(0, 0, self.width/2.0, self.height);
        self.leftBtn.backgroundColor = [UIColor clearColor];
        [self.leftBtn addTarget:self
                    action:@selector(leftBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
        self.leftBtn.exclusiveTouch = YES;
        [self addSubview:self.leftBtn];
        //
        self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.rightBtn.frame = CGRectMake(self.width/2.0, 0, self.width/2.0, self.height);
        self.rightBtn.backgroundColor = [UIColor clearColor];
        [self.rightBtn addTarget:self
                    action:@selector(rightBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];
        self.rightBtn.exclusiveTouch = YES;
        [self addSubview:self.rightBtn];
        //
        self.leftLabel = [[UILabel alloc]initWithFrame:self.leftBtn.frame];
        self.leftLabel.font = font_cu_15;
        self.leftLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.leftLabel];
        //
        self.lineLabel = [[UILabel alloc]initWithFrame:CGRectMake((self.width/2.0-25.0)/2.0, 32.0, 25.0, 3.0)];
        self.lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
        [self addSubview:self.lineLabel];
        //
        self.rightLabel = [[UILabel alloc]initWithFrame:self.rightBtn.frame];
        self.rightLabel.font = self.leftLabel.font;
        self.rightLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.rightLabel];
    }
    return self;
}

#pragma mark - RBSelectView Delegate
- (void)showWithData:(NSArray *)data andSelected:(int)tag {
    if(self.array==nil){
        self.array = data;
        self.leftLabel.text = data[0];
        self.rightLabel.text = data[1];
    }
    if (tag==0) {
        self.leftLabel.font = font_cu_cu_15;
        self.rightLabel.font = font_15;
        self.leftLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.rightLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        self.lineLabel.frame = CGRectMake((self.width/2.0-25.0)/2.0, 32.0, 25.0, 3.0);
    } else {
        self.leftLabel.font = font_15;
        self.rightLabel.font = font_cu_cu_15;
        self.leftLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
        self.rightLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        self.lineLabel.frame = CGRectMake(self.width/2.0+(self.width/2.0-25.0)/2.0, 32.0, 25.0, 3.0);
    }
}

#pragma mark - UIButton Delegate
- (void)leftBtnAction:(UIButton *)sender {
    [self showWithData:self.array andSelected:0];
    if ([self.delegate
         respondsToSelector:@selector(RBAdvertiserSwitchViewSelected:andIndex:)]) {
        [self.delegate RBAdvertiserSwitchViewSelected:self andIndex:0];
    }
}

- (void)rightBtnAction:(UIButton *)sender {
    [self showWithData:self.array andSelected:1];
    if ([self.delegate
         respondsToSelector:@selector(RBAdvertiserSwitchViewSelected:andIndex:)]) {
        [self.delegate RBAdvertiserSwitchViewSelected:self andIndex:1];
    }
}

@end
