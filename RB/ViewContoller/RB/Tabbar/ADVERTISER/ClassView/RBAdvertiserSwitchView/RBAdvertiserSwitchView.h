//
//  RBAdvertiserSwitchView.h
//  RB
//
//  Created by AngusNi on 3/15/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"

@class RBAdvertiserSwitchView;
@protocol RBAdvertiserSwitchViewDelegate <NSObject>
@optional
- (void)RBAdvertiserSwitchViewSelected:(RBAdvertiserSwitchView *)view andIndex:(int)index;
@end


@interface RBAdvertiserSwitchView : UIView
@property(nonatomic, weak) id<RBAdvertiserSwitchViewDelegate> delegate;
@property(nonatomic, strong) NSArray*array;
@property(nonatomic, strong) UILabel*leftLabel;
@property(nonatomic, strong) UILabel*lineLabel;
@property(nonatomic, strong) UILabel*rightLabel;
@property(nonatomic, strong) UIButton *leftBtn;
@property(nonatomic, strong) UIButton *rightBtn;
@property(nonatomic, assign) int selected;

-(void)showWithData:(NSArray*)data andSelected:(int)tag;
@end
