//
//  RBAdvertiserNlpView.m
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserNlpView.h"

@interface RBAdvertiserNlpView () {
    NSArray*colorArray;
}
@end

@implementation RBAdvertiserNlpView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        self.userInteractionEnabled = YES;
        self.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
        colorArray = @[@"f5484f",@"4e5460",@"4dc0bd",@"fcb464",@"95a0b1",SysColorBlue,SysColorBrown];
    }
    return self;
}

- (void)loadContentView:(RBNlpEntity*)nlpEntity {
    self.nlpEntity = nlpEntity;
    UIScrollView*editScrollView = [[UIScrollView alloc]initWithFrame:self.bounds];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self addSubview:editScrollView];
    // 1
    UIView*textView = [[UIView alloc]initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth-2*CellLeft, 120.0)];
    textView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:textView];
    //
    UITextView*contentTV = [[UITextView alloc]initWithFrame:CGRectMake(CellBottom, CellLeft, textView.width-2*CellBottom, 120.0-2*CellLeft)];
    contentTV.font = font_13;
    contentTV.text = self.nlpEntity.text;
    contentTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    contentTV.editable = NO;
    contentTV.textAlignment = NSTextAlignmentCenter;
    [textView addSubview:contentTV];
    // 2
    UIView*textView1 = [[UIView alloc]initWithFrame:CGRectMake(textView.left, textView.bottom+CellBottom, textView.width, ScreenWidth/2.0+25.0)];
    textView1.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:textView1];
    //
    UILabel*tipLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellBottom, CellBottom, 3.0, 15.0)];
    tipLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [textView1 addSubview:tipLabel1];
    //
    UILabel*tipTextLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(tipLabel1.right+5.0, tipLabel1.top, 200, tipLabel1.height)];
    tipTextLabel1.font = font_cu_cu_13;
    tipTextLabel1.text = NSLocalizedString(@"R6108",@"文章类别");
    tipTextLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.8];
    [textView1 addSubview:tipTextLabel1];
    //
    MCPieChartView*pieView = [[MCPieChartView alloc] initWithFrame:CGRectMake(0, tipLabel1.bottom, ScreenWidth/2.0, ScreenWidth/2.0)];
    pieView.dataSource = self;
    pieView.delegate = self;
    pieView.ringWidth = 50;
    pieView.tag = 10001;
    [textView1 addSubview:pieView];
    //
    int pieCount = (int)[self.nlpEntity.categories count];
    if(pieCount>5){
        pieCount = 5;
    }
    float gap = ((ScreenWidth/2.0)-pieCount*20.0)/(pieCount+1);
    for (int i=0; i<pieCount; i++) {
        RBTagEntity*entity = self.nlpEntity.categories[i];
        //
        UILabel*pieTLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2.0+10.0, tipLabel1.bottom+gap+2.5+i*(gap+20.0), 15.0, 15.0)];
        pieTLabel.backgroundColor = [Utils getUIColorWithHexString:colorArray[i]];
        [textView1 addSubview:pieTLabel];
        //
        UILabel*pieLabel = [[UILabel alloc]initWithFrame:CGRectMake(pieTLabel.right+2.0, tipLabel1.bottom+gap+i*(gap+20.0), ScreenWidth/2.0-27.0, 20.0)];
        pieLabel.font = font_cu_15;
        pieLabel.text = [NSString stringWithFormat:@"%@ %.2f%%",entity.label,entity.probability.floatValue*100];
        pieLabel.textColor = [Utils getUIColorWithHexString:colorArray[i]];
        [textView1 addSubview:pieLabel];
    }
    // 3
    UIView*textView2 = [[UIView alloc]initWithFrame:CGRectMake(textView.left, textView1.bottom+CellBottom, textView.width, ScreenWidth)];
    textView2.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:textView2];
    //
    UILabel*tipLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CellBottom, CellBottom, 3.0, 15.0)];
    tipLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [textView2 addSubview:tipLabel2];
    //
    UILabel*tipTextLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(tipLabel2.right+5.0, tipLabel2.top, 200, tipLabel2.height)];
    tipTextLabel2.font = font_cu_cu_13;
    tipTextLabel2.text = NSLocalizedString(@"R6109",@"关键词云");
    tipTextLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.8];
    [textView2 addSubview:tipTextLabel2];
    //
    DBSphereView *sphereView = [[DBSphereView alloc] initWithFrame:CGRectMake(30.0, tipLabel2.bottom+20.0, ScreenWidth-80.0, ScreenWidth-80.0)];
    sphereView.gesture.enabled = NO;
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    int keywords_count = (int)[self.nlpEntity.keywords count];
    if(keywords_count<5){
        keywords_count = (int)[self.nlpEntity.keywords count]+(int)[self.nlpEntity.categories count];
    }
    for (int i = 0; i < keywords_count; i ++) {
        RBTagEntity*entity;
        if ([self.nlpEntity.keywords count]<5) {
            if (i<[self.nlpEntity.keywords count]) {
                entity = self.nlpEntity.keywords[i];
            } else {
                entity = self.nlpEntity.categories[i-[self.nlpEntity.keywords count]];
            }
        } else {
            entity = self.nlpEntity.keywords[i];
        }
        UILabel*tagsLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 22)];
        tagsLabel.text = entity.label;
        tagsLabel.font = font_30;
        tagsLabel.textAlignment = NSTextAlignmentCenter;
        tagsLabel.textColor = [Utils getUIColorRandom];
        [array addObject:tagsLabel];
        CGSize tagsLabelSize = [Utils getUIFontSizeFitW:tagsLabel];
        tagsLabel.width = tagsLabelSize.width;
        [sphereView addSubview:tagsLabel];
    }
    [sphereView setCloudTags:array];
    [textView2 addSubview:sphereView];
    // 4
    UIView*textView3 = [[UIView alloc]initWithFrame:CGRectMake(textView.left, textView2.bottom+CellBottom, textView.width, ScreenWidth/2.0+25.0)];
    textView3.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:textView3];
    //
    UILabel*tipLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(CellBottom, CellBottom, 3.0, 15.0)];
    tipLabel3.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [textView3 addSubview:tipLabel3];
    //
    UILabel*tipTextLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(tipLabel2.right+5.0, tipLabel2.top, 200, tipLabel2.height)];
    tipTextLabel3.font = font_cu_cu_13;
    tipTextLabel3.text = NSLocalizedString(@"R6110",@"情感分析");
    tipTextLabel3.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.8];
    [textView3 addSubview:tipTextLabel3];
    //
    MCPieChartView*pieView3 = [[MCPieChartView alloc] initWithFrame:CGRectMake(0, tipLabel1.bottom, ScreenWidth/2.0, ScreenWidth/2.0)];
    pieView3.dataSource = self;
    pieView3.delegate = self;
    pieView3.ringWidth = 50;
    pieView3.tag = 10003;
    [textView3 addSubview:pieView3];
    //
    int pieCount3 = (int)[self.nlpEntity.sentiment count];
    float gap3 = ((ScreenWidth/2.0)-pieCount3*20.0)/(pieCount3+1);
    for (int i=0; i<pieCount3; i++) {
        RBTagEntity*entity = self.nlpEntity.sentiment[i];
        //
        UILabel*pieTLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2.0+10.0, tipLabel3.bottom+10+gap3+2.5+i*(gap3), 15.0, 15.0)];
        pieTLabel.backgroundColor = [Utils getUIColorWithHexString:colorArray[i]];
        [textView3 addSubview:pieTLabel];
        //
        UILabel*pieLabel = [[UILabel alloc]initWithFrame:CGRectMake(pieTLabel.right+2.0, tipLabel3.bottom+10+gap3+i*(gap3), ScreenWidth/2.0-27.0, 20.0)];
        pieLabel.font = font_cu_15;
        pieLabel.text = [NSString stringWithFormat:@"%@ %.2f%%",entity.label,entity.probability.floatValue*100];
        pieLabel.text = [pieLabel.text stringByReplacingOccurrencesOfString:@"positive" withString:NSLocalizedString(@"R6111",@"积极")];
        pieLabel.text = [pieLabel.text stringByReplacingOccurrencesOfString:@"negative" withString:NSLocalizedString(@"R6112",@"消极")];
        pieLabel.textColor = [Utils getUIColorWithHexString:colorArray[i]];
        [textView3 addSubview:pieLabel];
    }
    // 5
    UIView*textView4 = [[UIView alloc]initWithFrame:CGRectMake(textView.left, textView3.bottom+CellBottom, textView.width, ScreenWidth/2.0+25.0)];
    textView4.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:textView4];
    //
    UILabel*tipLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(CellBottom, CellBottom, 3.0, 15.0)];
    tipLabel4.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [textView4 addSubview:tipLabel4];
    //
    UILabel*tipTextLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(tipLabel4.right+5.0, tipLabel4.top, 200, tipLabel3.height)];
    tipTextLabel4.font = font_cu_cu_13;
    tipTextLabel4.text = NSLocalizedString(@"R6113",@"产品服务");
    tipTextLabel4.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.8];
    [textView4 addSubview:tipTextLabel4];
    //
    MCBarChartView*barView4 = [[MCBarChartView alloc] initWithFrame:CGRectMake(0, tipLabel4.bottom, textView4.width, ScreenWidth/2.0)];
    barView4.delegate = self;
    barView4.dataSource = self;
    barView4.tag = 1000;
    [textView4 addSubview:barView4];
    // 6
    UIView*textView5 = [[UIView alloc]initWithFrame:CGRectMake(textView.left, textView4.bottom+CellBottom, textView.width, ScreenWidth/2.0+25.0)];
    textView5.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [editScrollView addSubview:textView5];
    //
    UILabel*tipLabel5 = [[UILabel alloc] initWithFrame:CGRectMake(CellBottom, CellBottom, 3.0, 15.0)];
    tipLabel5.backgroundColor = [Utils getUIColorWithHexString:SysColorYellow];
    [textView5 addSubview:tipLabel5];
    //
    UILabel*tipTextLabel5 = [[UILabel alloc] initWithFrame:CGRectMake(tipLabel4.right+5.0, tipLabel4.top, 200, tipLabel3.height)];
    tipTextLabel5.font = font_cu_cu_13;
    tipTextLabel5.text = NSLocalizedString(@"R6114",@"人物品牌");
    tipTextLabel5.textColor = [Utils getUIColorWithHexString:SysColorBlack andAlpha:0.8];
    [textView5 addSubview:tipTextLabel5];
    //
    MCBarChartView*barView5 = [[MCBarChartView alloc] initWithFrame:CGRectMake(0, tipLabel5.bottom, textView4.width, ScreenWidth/2.0)];
    barView5.delegate = self;
    barView5.dataSource = self;
    barView5.tag = 2000;
    [textView5 addSubview:barView5];
    //
    [editScrollView setContentSize:CGSizeMake(editScrollView.width, textView5.bottom+CellLeft)];
}

#pragma mark - MCBarChartView Delegate
- (NSInteger)numberOfSectionsInBarChartView:(MCBarChartView *)barChartView {
    if(barChartView.tag==1000) {
        return [self.nlpEntity.products count];
    }
    return [self.nlpEntity.persons_brands count];
}

- (NSInteger)barChartView:(MCBarChartView *)barChartView numberOfBarsInSection:(NSInteger)section {
    return 1;
}

- (id)barChartView:(MCBarChartView *)barChartView valueOfBarInSection:(NSInteger)section index:(NSInteger)index {
    if(barChartView.tag==1000) {
        RBTagEntity*entity = self.nlpEntity.products[section];
        return entity.freq;
    }
    RBTagEntity*entity = self.nlpEntity.persons_brands[section];
    return entity.freq;
}

- (NSString *)barChartView:(MCBarChartView *)barChartView titleOfBarInSection:(NSInteger)section {
    if(barChartView.tag==1000) {
        RBTagEntity*entity = self.nlpEntity.products[section];
        return entity.label;
    }
    RBTagEntity*entity = self.nlpEntity.persons_brands[section];
    return entity.label;
}

- (CGFloat)paddingForBarInBarChartView:(MCBarChartView *)barChartView {
    return 25;
}

- (CGFloat)barWidthInBarChartView:(MCBarChartView *)barChartView {
    return 25;
}

- (CGFloat)paddingForSectionInBarChartView:(MCBarChartView *)barChartView {
    return 25;
}

- (UIColor *)barChartView:(MCBarChartView *)barChartView colorOfBarInSection:(NSInteger)section index:(NSInteger)index {
    if(section>=[colorArray count]){
        return [Utils getUIColorRandom];
    }
    return [Utils getUIColorWithHexString:colorArray[section]];
}

#pragma mark - MCPieChartView Delegate
- (NSInteger)numberOfPieInPieChartView:(MCPieChartView *)pieChartView {
    if (pieChartView.tag==1001) {
        if([self.nlpEntity.categories count]>5) {
            return 5;
        }
        return [self.nlpEntity.categories count];
    } else {
        return [self.nlpEntity.sentiment count];
    }
}

- (id)pieChartView:(MCPieChartView *)pieChartView valueOfPieAtIndex:(NSInteger)index {
    if (pieChartView.tag==1001) {
        RBTagEntity*entity = self.nlpEntity.categories[index];
        return entity.probability;
    } else {
        RBTagEntity*entity = self.nlpEntity.sentiment[index];
        return entity.probability;
    }
}

- (UIColor *)pieChartView:(MCPieChartView *)pieChartView colorOfPieAtIndex:(NSInteger)index {
    return [Utils getUIColorWithHexString:colorArray[index]];
}

@end
