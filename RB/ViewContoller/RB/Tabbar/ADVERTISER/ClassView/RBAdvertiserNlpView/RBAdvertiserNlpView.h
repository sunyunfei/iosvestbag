//
//  RBAdvertiserNlpView.h
//  RB
//
//  Created by AngusNi on 3/16/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBAdvertiserTableViewCell.h"

@class RBAdvertiserNlpView;
@protocol RBAdvertiserNlpViewDelegate <NSObject>
@optional
- (void)RBAdvertiserNlpViewLoadRefreshViewData:(RBAdvertiserNlpView *)view;
@end


@interface RBAdvertiserNlpView : UIView
<MCPieChartViewDelegate,MCPieChartViewDataSource,MCBarChartViewDelegate,MCBarChartViewDataSource,HandlerDelegate>
@property(nonatomic, weak) id<RBAdvertiserNlpViewDelegate> delegate;
@property(nonatomic, strong) UITableView *tableviewn;
@property(nonatomic, strong) RBNlpEntity *nlpEntity;
- (void)loadContentView:(RBNlpEntity*)nlpEntity;
@end
