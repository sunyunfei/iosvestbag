//
//  RBAdvertiserAddViewController.h
//  RB-TEST
//
//  Created by AngusNi on 10/10/2016.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBAdvertiserEditViewController.h"

@interface RBAdvertiserAddViewController : RBBaseViewController
<RBBaseVCDelegate,UITextFieldDelegate,TextFiledKeyBoardDelegate>
@end
