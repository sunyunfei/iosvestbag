//
//  RBAdvertiserMyListViewController.m
//  RB
//
//  Created by AngusNi on 6/13/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserMyListViewController.h"

@interface RBAdvertiserMyListViewController (){
    UIScrollView*scrollviewn;
    NSArray*statusArray;
    RBRankingSwitchView*switchView;
    RBAdvertiserCampaignEntity*cancelEntity;
    RBAdvertiserCampaignEntity*againEntity;

}
@end

@implementation RBAdvertiserMyListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R6043", @"我发布的活动");
    //
    switchView = [[RBRankingSwitchView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, 38.0)];
    switchView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [switchView RBRankingSwitchViewShow:@[NSLocalizedString(@"R6056", @"待付款"),NSLocalizedString(@"R2040", @"审核中"),NSLocalizedString(@"R2004",@"进行中"),NSLocalizedString(@"R2006",@"已完成")] andSelected:0];
    switchView.delegate = self;
    [self.view addSubview:switchView];
    //
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, switchView.bottom, ScreenWidth, ScreenHeight-switchView.bottom)];
    scrollviewn.bounces = YES;
    scrollviewn.pagingEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.delegate = self;
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width*4.0, scrollviewn.height)];
    [self.view addSubview:scrollviewn];
    //
    statusArray = @[@"unpay",@"checking",@"running",@"completed"];
    for (int i=0; i<4; i++) {
        RBAdvertiserView*advertiserView = [[RBAdvertiserView alloc]initWithFrame:CGRectMake(scrollviewn.width*i, 0, scrollviewn.width, scrollviewn.height)];
        advertiserView.delegate = self;
        advertiserView.tag = 10000+i;
        [scrollviewn addSubview:advertiserView];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"advertiser-my-list"];
    //
    RBAdvertiserView*advertiserView = (RBAdvertiserView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [advertiserView RBAdvertiserViewLoadRefreshViewFirstData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"advertiser-my-list"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- UIScrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView==scrollviewn){
        CGPoint offset = scrollView.contentOffset;
        int currentPage = offset.x / (self.view.bounds.size.width);
        [switchView RBRankingSwitchViewShow:@[NSLocalizedString(@"R6056", @"待付款"),NSLocalizedString(@"R2040", @"审核中"),NSLocalizedString(@"R2004",@"进行中"),NSLocalizedString(@"R2006",@"已完成")] andSelected:currentPage];
        [self RBRankingSwitchViewSelected:switchView andIndex:currentPage];
    }
}

#pragma mark- RBAdvertiserSwitchView Delegate
- (void)RBRankingSwitchViewSelected:(RBRankingSwitchView *)view andIndex:(int)index {
    RBAdvertiserView*advertiserView = (RBAdvertiserView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView show];
    [advertiserView RBAdvertiserViewLoadRefreshViewFirstData];
    [scrollviewn scrollRectToVisible:CGRectMake(scrollviewn.width*index, 0, scrollviewn.width, scrollviewn.height) animated:YES];
}

#pragma mark- RBAdvertiserView Delegate
-(void)RBAdvertiserViewLoadRefreshViewData:(RBAdvertiserView *)view {
    self.defaultView.hidden = YES;
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityAddListWithCampaign_type:statusArray[switchView.selectedTag] andPage:[NSString stringWithFormat:@"%d",view.pageIndex+1]];
}

-(void)RBAdvertiserViewCancelBtnSelected:(RBAdvertiserView *)view andIndex:(int)index {
    cancelEntity = view.datalist[index];
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    actionSheet.tag = 7000;
    [actionSheet showWithArray:@[NSLocalizedString(@"R6079", @"撤销活动"),NSLocalizedString(@"R1011", @"取消")]];
}

-(void)RBAdvertiserViewAgainBtnSelected:(RBAdvertiserView *)view andIndex:(int)index {
    againEntity = view.datalist[index];
    RBActionSheet *actionSheet = [[RBActionSheet alloc]init];
    actionSheet.delegate = self;
    actionSheet.tag = 8000;
    [actionSheet showWithArray:@[NSLocalizedString(@"R6201", @"再次发起此活动?"),NSLocalizedString(@"R1011", @"取消")]];
}

-(void)RBAdvertiserViewSelected:(RBAdvertiserView *)view andIndex:(int)index {
    RBAdvertiserCampaignEntity*entity = view.datalist[index];
    if ([entity.status isEqualToString:@"unpay"]||[entity.status isEqualToString:@"unexecute"]||[entity.status isEqualToString:@"rejected"]) {
        RBAdvertiserMyListPayViewController*toview = [[RBAdvertiserMyListPayViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        toview.adCampaignEntity = entity;
        [self.navigationController pushViewController:toview animated:YES];
    } else if ([entity.status isEqualToString:@"agreed"]||[entity.status isEqualToString:@"executing"]||[entity.status isEqualToString:@"executed"]||[entity.status isEqualToString:@"settled"]) {
        RBAdvertiserMyListDetailViewController*toview = [[RBAdvertiserMyListDetailViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        toview.adCampaignEntity = entity;
        [self.navigationController pushViewController:toview animated:YES];
    } else {
    }
}

#pragma mark - RBActionSheet Delegate
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex {
    if (actionSheet.tag == 7000) {
        if (buttonIndex == 0) {
            [self.hudView show];
            // RB-发布活动-撤销
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBActivityAddRevokeWithId:cancelEntity.iid];
        }
    }
    if (actionSheet.tag == 8000) {
        if (buttonIndex == 0) {
            RBAdvertiserEditAddViewController*toview = [[RBAdvertiserEditAddViewController alloc] init];
            toview.againStr = @"YES";
            toview.adCampaignEntity = againEntity;
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }
    }
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"kol_campaigns"]) {
        RBAdvertiserView*advertiserView = (RBAdvertiserView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
        if (advertiserView.pageIndex == 0) {
            advertiserView.datalist = [NSMutableArray new];
        }
        advertiserView.datalist = [JsonService getRBAdvertiserCampaignEntityList:jsonObject andBackArray:advertiserView.datalist];
        advertiserView.defaultView.hidden = YES;
        if ([advertiserView.datalist count] == 0) {
            advertiserView.defaultView.hidden = NO;
        }
        [self.hudView dismiss];
        [advertiserView RBAdvertiserViewSetRefreshViewFinish];
    }
    
    if ([sender isEqualToString:@"kol_campaigns/revoke"]) {
        [self.hudView showSuccessWithStatus:NSLocalizedString(@"R6060", @"活动撤销成功")];
        RBAdvertiserView*advertiserView = (RBAdvertiserView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
        [advertiserView RBAdvertiserViewLoadRefreshViewFirstData];
    }
    
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    RBAdvertiserView*advertiserView = (RBAdvertiserView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView dismiss];
    [advertiserView RBAdvertiserViewSetRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    RBAdvertiserView*advertiserView = (RBAdvertiserView*)[scrollviewn viewWithTag:10000+switchView.selectedTag];
    [self.hudView dismiss];
    [advertiserView RBAdvertiserViewSetRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

