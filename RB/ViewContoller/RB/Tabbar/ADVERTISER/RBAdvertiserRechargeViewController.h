//
//  RBAdvertiserRechargeViewController.h
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBAdvertiserRechargeSuccessedViewController.h"
#import "RBAdvertiserRechargeErrorViewController.h"
@interface RBAdvertiserRechargeViewController : RBBaseViewController
<RBBaseVCDelegate,TextFiledKeyBoardDelegate,UITextFieldDelegate>
@property(nonatomic,strong) RBPromoteEntity * entity;

@end
