//
//  RBAdvertiserEditPayViewController.h
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBAdvertiserEditViewController.h"
#import "RBAdvertiserEditAddViewController.h"
#import "RBAdvertiserPayDetailViewController.h"
@interface RBAdvertiserEditPayViewController : RBBaseViewController
<RBBaseVCDelegate>
@property(nonatomic, strong) RBAdvertiserCampaignEntity*adCampaignEntity;

@end
