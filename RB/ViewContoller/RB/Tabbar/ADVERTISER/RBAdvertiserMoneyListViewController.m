//
//  RBAdvertiserMoneyListViewController.m
//  RB
//
//  Created by AngusNi on 6/13/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserMoneyListViewController.h"

@interface RBAdvertiserMoneyListViewController (){
    int pageIndex;
    int pageNumber;
    int pageSize;
    UITableView*tableviewn;
    NSMutableArray*datalist;
    NSMutableArray*dataArray;
    UIButton * moneyButton;
    UIButton * scoreButton;
    UILabel * lineLabel;
    
    BOOL isCredit;
}
@end

@implementation RBAdvertiserMoneyListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R6202", @"我的品牌主账单");
    //
    isCredit = NO;
    [self loadTopView];
    //
    tableviewn = [[UITableView alloc]
                  initWithFrame:CGRectMake(0, NavHeight + 52, self.view.width, self.view.height-NavHeight - 52)
                  style:UITableViewStylePlain];
    tableviewn.dataSource = self;
    tableviewn.delegate = self;
    tableviewn.backgroundView = nil;
    tableviewn.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableviewn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableviewn];
    [self setRefreshHeaderView];
    //
    [self loadRefreshViewFirstData];
}
- (void)loadTopView{
    moneyButton = [[UIButton alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth/2, 52) title:@"资金流水" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorBlack] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    moneyButton.titleLabel.font = font_cu_(14);
    [moneyButton addTarget:self action:@selector(moneyBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:moneyButton];
    //
    
    scoreButton = [[UIButton alloc]initWithFrame:CGRectMake(moneyButton.right, NavHeight, ScreenWidth/2, 52) title:@"积分流水" hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorBlack] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
    scoreButton.titleLabel.font = font_(14);
    [scoreButton addTarget:self action:@selector(scoreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:scoreButton];
    //
    lineLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth/2 - 70*ScaleWidth)/2, scoreButton.bottom - 2, 70 * ScaleWidth, 2) text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysBackColorBlue] numberOfLines:1];
    [self.view addSubview:lineLabel];
    //
    UILabel * seperateLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, lineLabel.bottom, ScreenWidth, 0.5) text:nil font:nil textAlignment:NSTextAlignmentCenter textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorLine] numberOfLines:1];
    [self.view addSubview:seperateLabel];
    //
    
}
- (void)moneyBtnAction:(id)sender{
    scoreButton.enabled = YES;
    moneyButton.enabled = NO;
    [datalist removeAllObjects];
    isCredit = NO;
    pageIndex = 0;
    moneyButton.titleLabel.font = font_cu_(14);
    scoreButton.titleLabel.font = font_(14);
    [UIView animateWithDuration:0.5 animations:^{
        lineLabel.frame = CGRectMake((ScreenWidth/2 - 70*ScaleWidth)/2, scoreButton.bottom - 2, 70 * ScaleWidth, 2);
    }];
    [self loadRefreshViewFirstData];
}
- (void)scoreBtnAction:(id)sender{
    scoreButton.enabled = NO;
    moneyButton.enabled = YES;
    datalist = [NSMutableArray new];
    isCredit = YES;
    pageIndex = 0;
    moneyButton.titleLabel.font = font_(14);
    scoreButton.titleLabel.font = font_cu_(14);
    [UIView animateWithDuration:0.5 animations:^{
        lineLabel.frame = CGRectMake(ScreenWidth/2 + (ScreenWidth/2 - 70*ScaleWidth)/2, scoreButton.bottom - 2, 70 * ScaleWidth, 2);
    }];
    [self loadRefreshViewFirstData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"advertiser-money-list"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"advertiser-money-list"];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - LoadTableHeadFootView Delegate
- (void)loadRefreshViewFirstData {
    [self.hudView show];
    pageIndex = 0;
    [self loadRefreshViewData];
}

- (void)loadRefreshViewData {
    pageSize = 10;
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    if (isCredit == NO) {
        [handler getRBActivityAddMyAccountWith:[NSString stringWithFormat:@"%d",pageIndex+1]];
    }else{
        [handler getRBCreditListwithPage:[NSString stringWithFormat:@"%d",pageIndex+1]];
    }
}

- (void)setRefreshHeaderView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageIndex = 0;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshFooterView {
    __weak typeof(self) weakSelf = self;
    tableviewn.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageIndex = pageIndex + 1;
        [weakSelf loadRefreshViewData];
    }];
}

- (void)setRefreshViewFinish {
    [tableviewn.mj_header endRefreshing];
    [tableviewn.mj_footer endRefreshing];
    if ([datalist count] == (pageIndex+1)*pageSize) {
        if (tableviewn.mj_footer == nil){
            [self setRefreshFooterView];
        }
    } else {
        [tableviewn.mj_footer removeFromSuperview];
        tableviewn.mj_footer = nil;
    }
    [UIView performWithoutAnimation:^{
        [tableviewn reloadData];
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [datalist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"cellIdentifier%d%d",(int)[indexPath section],(int)[indexPath row]];
    RBUserIncomeListTableViewCell *cell  =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RBUserIncomeListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:cellIdentifier];
    }
    if (isCredit == NO) {
        [cell loadRBUserIncomeListCellWith:datalist andIndex:indexPath];
    }else{
        [cell loadRBUserCreditListCellWith:datalist andIndex:indexPath];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell  =
    [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"kol_brand/bill"]) {
        [self.hudView dismiss];
        if (pageIndex==0) {
            datalist = [NSMutableArray new];
        }
        datalist = [JsonService getRBIncomeListEntity:jsonObject andBackArray:datalist];
        self.defaultView.hidden = YES;
        if ([datalist count] == 0) {
            self.defaultView.hidden = NO;
        }
        [self setRefreshViewFinish];
    }
    if ([sender isEqualToString:@"credits"]) {
        [self.hudView dismiss];
        if (pageIndex == 0) {
            datalist = [NSMutableArray new];
        }
        datalist = [JsonService getRBCreditListEntity:jsonObject andBackArray:datalist];
        self.defaultView.hidden = YES;
        if ([datalist count] == 0) {
            self.defaultView.hidden = NO;
        }
        [self setRefreshViewFinish];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    if (jsonObject !=  nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self setRefreshViewFinish];
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end
