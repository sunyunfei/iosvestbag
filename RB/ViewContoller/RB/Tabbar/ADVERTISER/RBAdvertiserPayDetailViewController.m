//
//  RBAdvertiserPayDetailViewController.m
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserPayDetailViewController.h"

@interface RBAdvertiserPayDetailViewController () {
    UIButton*selectBtn1;
    UIButton*selectBtn2;
    BOOL selectedAlipay;
    RBPromoteEntity * entity;
}
@end

@implementation RBAdvertiserPayDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R6029", @"活动订单支付");
    // 监听支付宝支付状态
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationAlipayStatus:)
                                                 name:NotificationAlipayStatus object:nil];
    //
    [self loadEditView];
    [self loadFooterView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"advertiser-add-pay-detail"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"advertiser-add-pay-detail"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationAlipayStatus:(NSNotification *)notification {
    NSDictionary*temp = notification.userInfo;
    if ([[NSString stringWithFormat:@"%@",temp[@"STATUS"]] isEqualToString:NSLocalizedString(@"R6028", @"订单支付成功")]) {
        [self.hudView showSuccessWithStatus:temp[@"STATUS"]];
        // RB-发布活动-使用支付宝
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBActivityAddPayByAlipayWithiid:self.adCampaignPayEntity.iid];
        //
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           RBAdvertiserPaySuccessedViewController*toview = [[RBAdvertiserPaySuccessedViewController alloc] init];
                           toview.hidesBottomBarWhenPushed = YES;
                           [self.navigationController pushViewController:toview animated:YES];
                       });
    } else {
        [self.hudView showErrorWithStatus:temp[@"STATUS"]];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           RBAdvertiserPayErrorViewController*toview = [[RBAdvertiserPayErrorViewController alloc] init];
                           toview.hidesBottomBarWhenPushed = YES;
                           [self.navigationController pushViewController:toview animated:YES];
                       });
    }
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    if(selectedAlipay == YES) {
        // 支付宝接口
        NSString*orderStr = self.adCampaignPayEntity.alipay_url;
        NSLog(@"orderStr:%@",orderStr);
        NSString*scheme = [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"robin8" : @"robin8Test";
        [[AlipaySDK defaultService] payOrder:orderStr fromScheme:scheme callback:^(NSDictionary *resultDic) {
//            NSLog(@"resultDic = %@",resultDic);
//            NSLog(@"result = %@",[resultDic valueForKey:@"result"]);
//            NSLog(@"memo = %@",[resultDic valueForKey:@"memo"]);
//            NSLog(@"resultStatus = %@",[resultDic valueForKey:@"resultStatus"]);
//            int tempTag = [NSString stringWithFormat:@"%@",[resultDic valueForKey:@"resultStatus"]].intValue;
//            NSString*tempStr = @"";
//            if(tempTag == 9000) {
//                tempStr = NSLocalizedString(@"R6028", @"订单支付成功");
//            } else if(tempTag == 8000) {
//                tempStr = NSLocalizedString(@"R6051", @"正在处理中");
//            } else if(tempTag == 4000) {
//                tempStr = NSLocalizedString(@"R6039", @"订单支付失败");
//            } else if(tempTag == 6001) {
//                tempStr = NSLocalizedString(@"R6052", @"用户中途取消");
//            } else if(tempTag == 6002) {
//                tempStr = NSLocalizedString(@"R6053", @"网络连接出错");
//            }
//            NSLog(@"tempStr = %@",tempStr);
//            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationAlipayStatus
//                                                                object:self
//                                                              userInfo:@{@"STATUS":tempStr}];
//            
        }];
    } else {
        NSString*brand_amount = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%@",self.adCampaignPayEntity.brand_amount]];
        NSString*need_pay_amount = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%@",self.adCampaignPayEntity.need_pay_amount]];
        if (brand_amount.floatValue<need_pay_amount.floatValue) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R6030", @"广告账户余额不足,请先充值")];
        } else {
            [self.hudView showOverlay];
            // RB-发布活动-使用广告账户余额抵扣
            Handler*handler = [Handler shareHandler];
            handler.delegate = self;
            [handler getRBActivityAddPayByAdvertiserWithiid:self.adCampaignPayEntity.iid];
        }
    }
}

- (void)rechargeBtnAction:(UIButton *)sender {
    
    [self.hudView show];
    Handler * handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBRecharge];
}

- (void)selectBtn1Action:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:Icon_btn_select_h]) {
        [sender setTitle:Icon_btn_select_l forState:UIControlStateNormal];
        [sender
         setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray]
         forState:UIControlStateNormal];
        //
        [selectBtn2 setTitle:Icon_btn_select_h forState:UIControlStateNormal];
        [selectBtn2
         setTitleColor:[Utils getUIColorWithHexString:SysColorBlue]
         forState:UIControlStateNormal];
        //
        selectedAlipay = YES;
    } else {
        [sender setTitle:Icon_btn_select_h forState:UIControlStateNormal];
        [sender
         setTitleColor:[Utils getUIColorWithHexString:SysColorBlue]
         forState:UIControlStateNormal];
        //
        [selectBtn2 setTitle:Icon_btn_select_l forState:UIControlStateNormal];
        [selectBtn2
         setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray]
         forState:UIControlStateNormal];
        //
        selectedAlipay = NO;
    }
}

- (void)selectBtn2Action:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:Icon_btn_select_h]) {
        [sender setTitle:Icon_btn_select_l forState:UIControlStateNormal];
        [sender
         setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray]
         forState:UIControlStateNormal];
        //
        [selectBtn1 setTitle:Icon_btn_select_h forState:UIControlStateNormal];
        [selectBtn1
         setTitleColor:[Utils getUIColorWithHexString:SysColorBlue]
         forState:UIControlStateNormal];
        //
        selectedAlipay = NO;
    } else {
        [sender setTitle:Icon_btn_select_h forState:UIControlStateNormal];
        [sender
         setTitleColor:[Utils getUIColorWithHexString:SysColorBlue]
         forState:UIControlStateNormal];
        //
        [selectBtn1 setTitle:Icon_btn_select_l forState:UIControlStateNormal];
        [selectBtn1
         setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray]
         forState:UIControlStateNormal];
        //
        selectedAlipay = YES;
    }
}
#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    NSString*need_pay_amount = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%@",self.adCampaignPayEntity.need_pay_amount]];
    [footerBtn setTitle:[NSString stringWithFormat:@"%@ ￥%@",NSLocalizedString(@"R6031", @"确认支付"),need_pay_amount] forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

- (void)loadEditView {
    UIScrollView*editScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [self.view addSubview:editScrollView];
    //
    UILabel *lineGapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, editScrollView.width, CellBottom)];
    lineGapLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [editScrollView addSubview:lineGapLabel];
    //
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineGapLabel.bottom, ScreenWidth, 30.0)];
    tLabel.text = NSLocalizedString(@"R6032", @"请选择支付方式");
    tLabel.font = font_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [editScrollView addSubview:tLabel];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel.bottom-0.5, ScreenWidth, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:lineLabel];
    //
    UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, tLabel.bottom, ScreenWidth, 50.0)];
    tLabel1.text = NSLocalizedString(@"R6033", @"广告账户余额支付");
    tLabel1.font = font_15;
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel1];
    CGSize tLabel1Size = [Utils getUIFontSizeFitW:tLabel1];
    tLabel1.width = tLabel1Size.width;
    //
    UILabel*cLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(tLabel1.right+5.0, tLabel1.top, ScreenWidth, tLabel1.height)];
    NSString*brand_amount = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%@",self.adCampaignPayEntity.brand_amount]];
    cLabel1.text = [NSString stringWithFormat:@"￥%@",brand_amount];
    cLabel1.font = font_cu_15;
    cLabel1.textColor = [Utils getUIColorWithHexString:SysColorYellow];
    [editScrollView addSubview:cLabel1];
    CGSize cLabel1Size = [Utils getUIFontSizeFitW:cLabel1];
    cLabel1.width = cLabel1Size.width;
    //
    UIButton*rechargeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rechargeBtn setTitle:NSLocalizedString(@"R6034", @"立即充值") forState:UIControlStateNormal];
    [rechargeBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    rechargeBtn.titleLabel.font = font_11;
    [rechargeBtn addTarget:self action:@selector(rechargeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [editScrollView addSubview:rechargeBtn];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    CGSize labelSize = [NSLocalizedString(@"R6034", @"立即充值") boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font_11,NSParagraphStyleAttributeName:paragraphStyle} context:nil].size;
    rechargeBtn.frame = CGRectMake(cLabel1.right+5.0, cLabel1.top, labelSize.width, cLabel1.height);
    //
    selectBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    selectBtn1.frame = CGRectMake(editScrollView.width-25.0-CellLeft, tLabel1.top+(tLabel1.height-25.0)/2.0, 25.0, 25.0);
    [selectBtn1 addTarget:self
                    action:@selector(selectBtn1Action:)
          forControlEvents:UIControlEventTouchUpInside];
    selectBtn1.titleLabel.font = font_icon_(selectBtn1.width);
    [selectBtn1 setTitle:Icon_btn_select_h forState:UIControlStateNormal];
    [selectBtn1 setTitleColor:[Utils getUIColorWithHexString:SysColorBlue] forState:UIControlStateNormal];
    [editScrollView addSubview:selectBtn1];
    selectedAlipay = NO;
    //
    UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel1.bottom-0.5, ScreenWidth, 0.5)];
    lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:lineLabel1];
    //
    UILabel*tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineLabel1.bottom, ScreenWidth, tLabel1.height)];
    tLabel2.text = NSLocalizedString(@"R6035", @"支付宝支付");
    tLabel2.font = font_15;
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel2];
    //
    selectBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    selectBtn2.frame = CGRectMake(editScrollView.width-25.0-CellLeft, tLabel2.top+(tLabel1.height-25.0)/2.0, 25.0, 25.0);
    [selectBtn2 addTarget:self
                  action:@selector(selectBtn2Action:)
        forControlEvents:UIControlEventTouchUpInside];
    selectBtn2.titleLabel.font = font_icon_(selectBtn2.width);
    [selectBtn2 setTitle:Icon_btn_select_l forState:UIControlStateNormal];
    [selectBtn2 setTitleColor:[Utils getUIColorWithHexString:SysColorSubGray] forState:UIControlStateNormal];
    [editScrollView addSubview:selectBtn2];
    //
    UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel2.bottom-0.5, ScreenWidth, 0.5)];
    lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:lineLabel2];
    //
//    UILabel * tLabel4 = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, lineLabel2.bottom, ScreenWidth, tLabel1.height) text:@"使用积分" font:font_(15) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorBlack] backgroundColor:[UIColor clearColor] numberOfLines:1];
//    [editScrollView addSubview:tLabel4];
//    //
//    selectBtn3 = [[UIButton alloc]initWithFrame:CGRectMake(editScrollView.width-25.0-CellLeft, tLabel4.top+(tLabel1.height-25.0)/2.0, 25.0, 25.0) title:Icon_btn_select_l hlTitle:nil titleColor:[Utils getUIColorWithHexString:SysColorSubGray] hlTitleColor:nil backgroundColor:[UIColor clearColor] image:nil hlImage:nil];
//    [selectBtn3 addTarget:self action:@selector(selectBtn3Action:) forControlEvents:UIControlEventTouchUpInside];
//    [editScrollView addSubview:selectBtn3];
//    //
//    UILabel * lineLabel4 = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel4.bottom - 0.5, ScreenWidth, 0.5) text:nil font:nil textAlignment:NSTextAlignmentLeft textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorLine] numberOfLines:1];
//    [editScrollView addSubview:lineLabel4];
    //
    
    //
    UILabel*tLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(0, lineLabel2.bottom, ScreenWidth-CellLeft, 50.0)];
    tLabel3.textAlignment = NSTextAlignmentRight;
    NSString*need_pay_amount = [Utils getNSStringTwoFloat:[NSString stringWithFormat:@"%@",self.adCampaignPayEntity.need_pay_amount]];
    tLabel3.text = [NSString stringWithFormat:@"%@:￥%@",NSLocalizedString(@"R6036", @"支付金额"),need_pay_amount];
    tLabel3.font = font_15;
    tLabel3.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:tLabel3];
    //
    UILabel*lineLabel3 = [[UILabel alloc]initWithFrame:CGRectMake(0, tLabel3.bottom-0.5, ScreenWidth, 0.5)];
    lineLabel3.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:lineLabel3];
    //
    UILabel *lineGapLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(0, lineLabel3.bottom, editScrollView.width, editScrollView.height-lineLabel3.bottom)];
    lineGapLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [editScrollView addSubview:lineGapLabel1];
    //
    [editScrollView setContentSize:CGSizeMake(editScrollView.width, lineLabel3.bottom)];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"kol_campaigns/pay"]) {
        RBAdvertiserCampaignPayEntity*entity = [JsonService getRBAdvertiserCampaignPayEntity:jsonObject];
        NSString*status = [NSString stringWithFormat:@"%@",entity.status];
        if ([status isEqualToString:@"unexecute"]) {
            [self.hudView showErrorWithStatus:NSLocalizedString(@"R6028", @"订单支付成功")];
            dispatch_after(
                           dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(), ^{
                               RBAdvertiserPaySuccessedViewController*toview = [[RBAdvertiserPaySuccessedViewController alloc] init];
                               toview.hidesBottomBarWhenPushed = YES;
                               [self.navigationController pushViewController:toview animated:YES];
                           });
        }
    }
    if ([sender isEqualToString:@"promotions"]) {
        NSLog(@"%@",jsonObject);
        [self.hudView dismiss];
        if ([[jsonObject objectForKey:@"promotion"] isEqual:[NSNull null]]) {
            RBAdvertiserPayMoneyController * toview = [[RBAdvertiserPayMoneyController alloc]init];
            toview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:toview animated:YES];
        }else{
            entity = [JsonService getRBPromoteEntity:[jsonObject objectForKey:@"promotion"]];
            RBAdvertiserRechargeViewController*toview = [[RBAdvertiserRechargeViewController alloc] init];
            toview.hidesBottomBarWhenPushed = YES;
            toview.entity = entity;
            [self.navigationController pushViewController:toview animated:YES];
        }
    }
    
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}
@end

