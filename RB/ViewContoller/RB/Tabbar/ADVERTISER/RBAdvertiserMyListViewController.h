//
//  RBAdvertiserMyListViewController.h
//  RB
//
//  Created by AngusNi on 6/13/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
//
#import "RBAdvertiserView.h"
#import "RBRankingSwitchView.h"
//
#import "RBAdvertiserMyListDetailViewController.h"
#import "RBAdvertiserMyListPayViewController.h"
#import "RBAdvertiserEditAddViewController.h"

@interface RBAdvertiserMyListViewController : RBBaseViewController
<RBBaseVCDelegate,RBAdvertiserViewDelegate,RBRankingSwitchViewDelegate,UIScrollViewDelegate,RBActionSheetDelegate>
@end
