//
//  RBPlatSelectViewController.h
//  RB
//
//  Created by RB8 on 2017/9/25.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBPlatSelectViewController : RBBaseViewController
@property(nonatomic,copy)void(^platBlock)(BOOL weiboSelected,BOOL wechatSelected);
@property(nonatomic,copy)NSString * mark;
@end
