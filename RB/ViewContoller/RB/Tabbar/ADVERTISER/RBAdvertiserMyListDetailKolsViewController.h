//
//  RBAdvertiserMyListDetailKolsViewController.h
//  RB
//
//  Created by AngusNi on 6/14/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBAdvertiserKolsTableViewCell.h"
#import "RBKolDetailViewController.h"

@interface RBAdvertiserMyListDetailKolsViewController : RBBaseViewController
<RBBaseVCDelegate,UITableViewDataSource,UITableViewDelegate>
@property(nonatomic, strong) RBAdvertiserCampaignEntity*adCampaignEntity;

@end
