//
//  RBAdvertiserRechargeViewController.m
//  RB
//
//  Created by AngusNi on 6/12/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserRechargeViewController.h"

@interface RBAdvertiserRechargeViewController () {
    InsetsTextField*inputTF;
    InsetsTextField*inputTF2;
    InsetsTextField*inputTF3;
    UILabel * campaignNameLabel;
    UILabel * campaignDspLabel;
    UILabel * dateLabel;
    UILabel * markLabel;
}
@end


@implementation RBAdvertiserRechargeViewController
- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R6046", @"充值");
    // 监听支付宝支付状态
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(RBNotificationAlipayStatus:)
//                                                 name:NotificationAlipayStatus object:nil];
    //
    [self loadEditView];
    [self loadFooterView];
 //   [self requestData];
}
- (void)requestData{
    Handler * handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBRecharge];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"advertiser-recharge"];
    // 监听支付宝支付状态
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationAlipayStatus:)
                                                 name:NotificationAlipayStatus object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"advertiser-recharge"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)footerBtnAction:(UIButton *)sender {
    if(inputTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6047", @"充值金额")]];
    }
    [self.hudView showOverlay];
    // RB-发布活动-品牌主账户充值
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityAddMyRechargeWithCredits:inputTF.text andCode:inputTF2.text];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R6034", @"立即充值") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

#pragma mark- loadEditView Delegate
- (void)loadEditView {
    UIScrollView*editScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0)];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self.view addSubview:editScrollView];
    //
    UIImageView * backImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 90)];
    backImage.image = [UIImage imageNamed:@"payBanner"];
    [editScrollView addSubview:backImage];
    //
    campaignNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 16, ScreenWidth - CellLeft, 17) text:@"" font:font_cu_cu_(15) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorWhite] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [backImage addSubview:campaignNameLabel];
    //
    campaignDspLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, campaignNameLabel.bottom + 13, ScreenWidth - CellLeft, 30) text:@"单次充值超过10000" font:font_(12) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorWhite] backgroundColor:[UIColor clearColor] numberOfLines:2];
   
    [backImage addSubview:campaignDspLabel];
    //
    UIView * backView = [[UIView alloc]initWithFrame:CGRectMake(0, backImage.bottom + 9, ScreenWidth, 0)];
    backView.backgroundColor = [UIColor whiteColor];
    [editScrollView addSubview:backView];
    //
    UILabel*tLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth, 50.0)];
    tLabel.text = NSLocalizedString(@"R6047", @"充值金额");
    tLabel.font = font_15;
    tLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [backView addSubview:tLabel];
    CGSize tLabelSize = [Utils getUIFontSizeFitW:tLabel];
    tLabel.width = tLabelSize.width;
    //
    inputTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(tLabel.right+20.0, tLabel.top, ScreenWidth - tLabel.right - 20 - CellLeft, tLabel.height)];
    inputTF.font = font_15;
    inputTF.tag = 998;
    inputTF.keyboardType =  UIKeyboardTypeNumberPad;
    inputTF.clearButtonMode = UITextFieldViewModeNever;
    inputTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    inputTF.textAlignment = NSTextAlignmentRight;
    inputTF.delegate = self;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textFieldTextDidChangeOneCI:)
                                                name:UITextFieldTextDidChangeNotification
                                              object:inputTF];
    [backView addSubview:inputTF];
    inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"R6048", @"最低充值金额500元") attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    //
    TextFiledKeyBoard *textFieldKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textFieldKB.delegateT = self;
    [inputTF setInputAccessoryView:textFieldKB];
    //
    UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, tLabel.bottom-0.5, ScreenWidth - 2 * CellLeft, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [backView addSubview:lineLabel];
    //
    UILabel*tLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, lineLabel.bottom, ScreenWidth, tLabel.height)];
    tLabel2.text = NSLocalizedString(@"R6203", @"活动赠送积分");
    tLabel2.font = font_15;
    tLabel2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [backView addSubview:tLabel2];
    CGSize tLabel2Size = [Utils getUIFontSizeFitW:tLabel2];
    tLabel2.width = tLabel2Size.width;
    //
    inputTF2 = [[InsetsTextField alloc]initWithFrame:CGRectMake(tLabel2.right+20.0, lineLabel.bottom, ScreenWidth - tLabel2.right - 20 - CellLeft, tLabel2.height)];
    inputTF2.font = font_15;
    inputTF2.keyboardType =  UIKeyboardTypeDefault;
    inputTF2.clearButtonMode = UITextFieldViewModeNever;
    inputTF2.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    inputTF2.enabled = NO;
    [backView addSubview:inputTF2];
    inputTF2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"0" attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    [inputTF2 setInputAccessoryView:textFieldKB];
    inputTF2.textAlignment = NSTextAlignmentRight;
    //
    UILabel*lineLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, tLabel2.bottom-0.5, ScreenWidth - 2 * CellLeft, 0.5)];
    lineLabel2.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [backView addSubview:lineLabel2];
    //
    markLabel = [[UILabel alloc]initWithFrame:CGRectZero text:nil font:font_(8) textAlignment:NSTextAlignmentRight textColor:[Utils getUIColorWithHexString:@"777777"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [backView addSubview:markLabel];
    //
    dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, lineLabel2.bottom + 11, ScreenWidth - 2 * CellLeft, 10) text:@"" font:font_(10) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:@"777777"] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [backView addSubview:dateLabel];
    //
    UILabel * grayLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, dateLabel.bottom + 11, ScreenWidth, 10) text:nil font:nil textAlignment:NSTextAlignmentRight textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorBkg] numberOfLines:1];
    [backView addSubview:grayLabel];
    //
    UILabel * inviteLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, grayLabel.bottom, ScreenWidth, 50) text:NSLocalizedString(@"R1116", @"邀请码") font:font_(15) textAlignment:NSTextAlignmentLeft textColor:[Utils getUIColorWithHexString:SysColorBlack] backgroundColor:[UIColor clearColor] numberOfLines:1];
    CGSize inviteLabelsize = [Utils getUIFontSizeFitW:inviteLabel];
    inviteLabel.width = inviteLabelsize.width;
    [backView addSubview:inviteLabel];
    //
    inputTF3 = [[InsetsTextField alloc]initWithFrame:CGRectMake(inviteLabel.right+20.0, inviteLabel.top, ScreenWidth - inviteLabel.right - 20 - CellLeft, inviteLabel.height)];
    inputTF3.font = font_15;
    inputTF3.keyboardType =  UIKeyboardTypeDefault;
    inputTF3.clearButtonMode = UITextFieldViewModeNever;
    inputTF3.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [backView addSubview:inputTF3];
    inputTF3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"R6083", @"可不填写") attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    [inputTF3 setInputAccessoryView:textFieldKB];
    inputTF3.textAlignment = NSTextAlignmentRight;
    //
    UILabel * seperateLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, inviteLabel.bottom - 0.5, ScreenWidth - 2 * CellLeft, 0.5) text:nil font:nil textAlignment:NSTextAlignmentRight textColor:nil backgroundColor:[Utils getUIColorWithHexString:SysColorLine] numberOfLines:1];
    [backView addSubview:seperateLabel];
    //
    UILabel*tLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, seperateLabel.bottom, ScreenWidth - 2*CellLeft, inviteLabel.height)];
    tLabel1.text = NSLocalizedString(@"R6049", @"支付方式");
    tLabel1.font = font_15;
    tLabel1.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [backView addSubview:tLabel1];
    CGSize tLabel1Size = [Utils getUIFontSizeFitW:tLabel1];
    tLabel1.width = tLabel1Size.width;
    //
    UIImageView*cIV = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - CellLeft - (32.0/60.0)*169.0, tLabel1.top+9.0, (32.0/60.0)*169.0, 32.0)];
    cIV.image = [UIImage imageNamed:@"icon_advertiser_alipay.png"];
    [backView addSubview:cIV];
    //
    backView.height = tLabel1.bottom;
    //
    campaignNameLabel.text = _entity.title;
    //        inputTF2.placeholder = [NSString stringWithFormat:@"充值%@元可获赠积分",entity.min_credit];
    inputTF2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"充值%@元可获赠积分",_entity.min_credit] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    campaignDspLabel.text = _entity.campaignDes;
    NSString * timeStr = [NSString stringWithFormat:@"注意: 此次赠送积分有效使用时间至%@",_entity.expired_at];
    dateLabel.text = timeStr;
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self endEdit];
    return YES;
}

#pragma mark - TextFiledKeyBoard Delegate
- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender {
    [self endEdit];
}


#pragma mark - NSNotification Delegate
- (void)RBNotificationAlipayStatus:(NSNotification *)notification {
    NSDictionary*temp = notification.userInfo;
    if ([[NSString stringWithFormat:@"%@",temp[@"STATUS"]] isEqualToString:NSLocalizedString(@"R6028", @"订单支付成功")]) {
        [self.hudView showSuccessWithStatus:temp[@"STATUS"]];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           RBAdvertiserRechargeSuccessedViewController*toview = [[RBAdvertiserRechargeSuccessedViewController alloc] init];
                           toview.hidesBottomBarWhenPushed = YES;
                           [self.navigationController pushViewController:toview animated:YES];
                       });
    } else {
        [self.hudView showErrorWithStatus:temp[@"STATUS"]];
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           RBAdvertiserRechargeErrorViewController*toview = [[RBAdvertiserRechargeErrorViewController alloc] init];
                           toview.hidesBottomBarWhenPushed = YES;
                           [self.navigationController pushViewController:toview animated:YES];
                       });
    }
}
#pragma mark - textFieldDelegate

-(void)textFieldTextDidChangeOneCI:(NSNotification *)notification
{
    UITextField *textfield=[notification object];
    if (textfield.text.intValue >= _entity.min_credit.intValue) {
//        markLabel.hidden = YES;
        
        int amount = textfield.text.intValue;
        float rate = _entity.rate.floatValue;
        NSString * integral = [NSString stringWithFormat:@"%f",amount * rate];
        NSArray * arr = [integral componentsSeparatedByString:@"."];
        inputTF2.text = arr[0];
    }else{
//        inputTF2.placeholder = [NSString stringWithFormat:@"充值%@元可获赠积分",entity.min_credit];
        inputTF2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"充值%@元可获赠积分",_entity.min_credit] attributes:@{NSFontAttributeName:font_15 ,NSForegroundColorAttributeName:[Utils getUIColorWithHexString:SysColorSubGray]}];
    }
    NSLog(@"ssssss %@",textfield.text);
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"recharge"]) {
        [self.hudView dismiss];
        // 支付宝接口
        NSString*orderStr = [jsonObject objectForKey:@"alipay_url"];
        NSLog(@"orderStr:%@",orderStr);
        NSString*scheme = [AppBundleIdentifier isEqualToString:@"com.robin8.rb"] ? @"robin8" : @"robin8Test";
        [[AlipaySDK defaultService] payOrder:orderStr fromScheme:scheme callback:^(NSDictionary *resultDic) {
//            NSLog(@"resultDic = %@",resultDic);
//            NSLog(@"result = %@",[resultDic valueForKey:@"result"]);
//            NSLog(@"memo = %@",[resultDic valueForKey:@"memo"]);
//            NSLog(@"resultStatus = %@",[resultDic valueForKey:@"resultStatus"]);
//            int tempTag = [NSString stringWithFormat:@"%@",[resultDic valueForKey:@"resultStatus"]].intValue;
//            NSString*tempStr = @"";
//            if(tempTag == 9000) {
//                tempStr = NSLocalizedString(@"R6028", @"订单支付成功");
//            } else if(tempTag == 8000) {
//                tempStr = NSLocalizedString(@"R6051", @"正在处理中");
//            } else if(tempTag == 4000) {
//                tempStr = NSLocalizedString(@"R6039", @"订单支付失败");
//            } else if(tempTag == 6001) {
//                tempStr = NSLocalizedString(@"R6052", @"用户中途取消");
//            } else if(tempTag == 6002) {
//                tempStr = NSLocalizedString(@"R6053", @"网络连接出错");
//            }
//            NSLog(@"tempStr = %@",tempStr);
//            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationAlipayStatus
//                                                                object:self
//                                                              userInfo:@{@"STATUS":tempStr}];
//            
        }];
    }
    if ([sender isEqualToString:@"promotions"]) {
        NSLog(@"%@",jsonObject);
     //   entity = [JsonService getRBPromoteEntity:[jsonObject objectForKey:@"promotion"]];
       
        
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
        
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

