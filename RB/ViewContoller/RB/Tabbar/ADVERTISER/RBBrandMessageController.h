//
//  RBBrandMessageController.h
//  RB
//
//  Created by RB8 on 2018/5/22.
//  Copyright © 2018年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "TextFiledKeyBoard.h"
@interface RBBrandMessageController : RBBaseViewController<HandlerDelegate,RBBaseVCDelegate,UITextViewDelegate,RBActionSheetDelegate,RBPictureUploadDelegate,TextFiledKeyBoardDelegate>
@property(nonatomic,strong)NSDictionary * dict;
@end
