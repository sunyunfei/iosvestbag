//
//  RBAdvertiserAddViewController.m
//  RB-TEST
//
//  Created by AngusNi on 10/10/2016.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBAdvertiserAddViewController.h"

@interface RBAdvertiserAddViewController () {
    UIScrollView*editScrollView;
    InsetsTextField*editTF;
    UILabel*resultLabel;
    NSMutableArray*resultArray;
    int resultTag;
}
@end

@implementation RBAdvertiserAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    self.navTitle = NSLocalizedString(@"R6007", @"发布活动");
    self.view.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    //
    [self loadEditView];
    [self loadFooterView];
    //
    [self RBNotificationBecomeActive];
    //
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityAddResult];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    [TalkingData trackPageBegin:@"advertiser-add"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //
    [TalkingData trackPageEnd:@"advertiser-add"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)RBNotificationBecomeActive {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    if ([[pasteboard.string componentsSeparatedByString:@"http"]count]>1) {
        editTF.text = pasteboard.string;
    }
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)resultBtnAction:(UIButton *)sender {
    resultTag = (int)sender.tag-1000;
    for (int i=0; i<[resultArray count]; i++) {
        UIButton*resultBtn = (UIButton*)[editScrollView viewWithTag:1000+i];
        if (i==resultTag) {
            [resultBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
            resultBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
        } else {
            [resultBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
            resultBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
        }
    }
}

- (void)footerBtnAction:(UIButton *)sender {
    if(editTF.text.length==0) {
        [self.hudView showErrorWithStatus:[NSString stringWithFormat:@"%@%@(%@)",NSLocalizedString(@"R2092", @"请填写"),NSLocalizedString(@"R6010", @"活动链接"),NSLocalizedString(@"R6089", @"下载类活动需填写APP下载地址")]];
        return;
    }
    //
    RBTagEntity*tagEntity = resultArray[resultTag];
    [self.hudView show];
    Handler*handler = [Handler shareHandler];
    handler.delegate = self;
    [handler getRBActivityAddAnalysisWithUrl:editTF.text andExpect:tagEntity.name];
}

#pragma mark- loadFooterView Delegate
- (void)loadFooterView {
    UILabel*aboutLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, ScreenHeight-45.0-60.0, ScreenWidth-2*CellLeft, 60.0)];
    aboutLabel.font = font_13;
    aboutLabel.textAlignment = NSTextAlignmentCenter;
    aboutLabel.numberOfLines = 0;
    aboutLabel.text = NSLocalizedString(@"R6102",@"Robin8国内首家利用NLP技术进行精准营销\n只需复制文章链接即可智能发布");
    aboutLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [self.view addSubview:aboutLabel];
    //
    UIView*footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    UIButton*footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    [footerBtn addTarget:self
                  action:@selector(footerBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitle:NSLocalizedString(@"R6103",@"智能发布") forState:UIControlStateNormal];
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
}

- (void)loadEditView {
    editScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight-45.0-60.0)];
    editScrollView.showsHorizontalScrollIndicator = NO;
    editScrollView.showsVerticalScrollIndicator = NO;
    editScrollView.userInteractionEnabled = YES;
    editScrollView.backgroundColor = [Utils getUIColorWithHexString:SysColorBkg];
    [self.view addSubview:editScrollView];
    //
    UILabel*editLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, 0, ScreenWidth-2*CellLeft, 35.0)];
    editLabel.font = font_13;
    editLabel.text = NSLocalizedString(@"R6104",@"填写文章链接");
    editLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [editScrollView addSubview:editLabel];
    //
    UILabel*editLineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, editLabel.bottom-0.5, ScreenWidth, 0.5f)];
    editLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:editLineLabel];
    //
    editTF = [[InsetsTextField alloc]initWithFrame:CGRectMake(0, editLabel.bottom, ScreenWidth, 50.0)];
    editTF.font = font_cu_15;
    editTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    editTF.placeholder = NSLocalizedString(@"R6105",@"复制链接，可自动填写(推荐微信文章)");
    editTF.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    editTF.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    editTF.keyboardType = UIKeyboardTypeURL;
    editTF.delegate = self;
    [editScrollView addSubview:editTF];
    //
    TextFiledKeyBoard *textFieldKB = [[TextFiledKeyBoard alloc]initWithFrame:CGRectMake(0, ScreenHeight-260, ScreenWidth, 260)];
    textFieldKB.delegateT = self;
    [editTF setInputAccessoryView:textFieldKB];
    //
    UILabel*editLineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, editTF.bottom, ScreenWidth, 0.5f)];
    editLineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [editScrollView addSubview:editLineLabel1];
    //
    resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(CellLeft, editLineLabel1.bottom, ScreenWidth-2*CellLeft, 35.0)];
    resultLabel.font = font_13;
    resultLabel.text = NSLocalizedString(@"R6106",@"选择期望效果");
    resultLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    if ([LocalService getRBIsIncheck] == YES) {
        resultLabel.hidden = YES;
    }
    [editScrollView addSubview:resultLabel];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

- (void)TextFiledKeyBoardFinish:(TextFiledKeyBoard *)sender{
    [self.view endEditing:YES];
}

#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"analysis"]) {
        [self.hudView dismiss];
        RBAdvertiserCampaignEntity*campaignEntity = [JsonService getRBAdvertiserResultCampaignEntity:jsonObject];
        RBNlpEntity*nlpEntity = [JsonService getRBAdvertiserResultNlpEntity:jsonObject];
        //
        RBAdvertiserEditViewController*toview = [[RBAdvertiserEditViewController alloc] init];
        toview.hidesBottomBarWhenPushed = YES;
        toview.adCampaignEntity = campaignEntity;
        toview.nlpEntity = nlpEntity;
        [self.navigationController pushViewController:toview animated:YES];
    }
    
    if ([sender isEqualToString:@"expect_effect_list"]) {
        [self.hudView dismiss];
        resultArray = [NSMutableArray new];
        resultArray = [JsonService getRBAdvertiserResultEntity:jsonObject andBackArray:resultArray];
        //
        float tempw = (ScreenWidth-3.0*CellLeft)/2.0;
        float temph = 50.0;
        for (int i=0; i<[resultArray count]; i++) {
            RBTagEntity*tagEntity = resultArray[i];
            UIButton*resultBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            resultBtn.frame = CGRectMake(CellLeft+(i%2)*(CellLeft+tempw), resultLabel.bottom+(i/2)*(CellLeft+temph), tempw, temph);
            resultBtn.tag = 1000+i;
            [resultBtn addTarget:self
                          action:@selector(resultBtnAction:)
                forControlEvents:UIControlEventTouchUpInside];
            resultBtn.titleLabel.font = font_cu_15;
            [resultBtn setTitle:tagEntity.label forState:UIControlStateNormal];
            if (i==0) {
                resultTag = 0;
                [resultBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
                resultBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
            } else {
                [resultBtn setTitleColor:[Utils getUIColorWithHexString:SysColorBlack] forState:UIControlStateNormal];
                resultBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
            }
            if ([LocalService getRBIsIncheck] == YES) {
                resultBtn.hidden = YES;
            }
            [editScrollView addSubview:resultBtn];
            if (i==[resultArray count]-1) {
                [editScrollView setContentSize:CGSizeMake(ScreenWidth, resultBtn.bottom)];
            }
            
        }
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    if (jsonObject != nil) {
        [self.hudView showErrorWithStatus:[jsonObject objectForKey:@"detail"]];
    }
}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self.hudView showErrorWithStatus:error.localizedDescription];
}

@end

