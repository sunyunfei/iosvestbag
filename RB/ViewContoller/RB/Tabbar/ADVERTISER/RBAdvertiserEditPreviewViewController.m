//
//  RBAdvertiserEditPreviewViewController.m
//  RB
//
//  Created by AngusNi on 16/1/28.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBAdvertiserEditPreviewViewController.h"

@interface RBAdvertiserEditPreviewViewController (){
    UIScrollView*subScrollView;
    UIImageView*bgIV;
    UIScrollView*scrollviewn;
    UIWebView*webviewn;
    UIView*webViewNV;
    UIView*footerView;
    UIButton*footerBtn;
    UIButton*footerRightBtn;
    UILabel*footerLineLabel;
    NSDate*tempDate;
    UILabel *tagTimeLabel;
    TTTAttributedLabel *tagTextLabel;
    UILabel *tagLabel;
}
@end

@implementation RBAdvertiserEditPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    [self loadScrollView];
    [self loadWebView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleLightContent];
    //
    [TalkingData trackPageBegin:@"advertiser-add-preview"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    //
    [TalkingData trackPageEnd:@"advertiser-add-preview"];
}

#pragma mark - UIScreenEdgePanGestureRecognizer Delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (void)edgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender {
    [self navLeftBtnAction:nil];
}

#pragma mark - ContentView Delegate
-(void)loadScrollView {
    scrollviewn = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth , ScreenHeight-45.0)];
    scrollviewn.backgroundColor = [UIColor whiteColor];
    scrollviewn.bounces = YES;
    scrollviewn.pagingEnabled = YES;
    scrollviewn.userInteractionEnabled = YES;
    scrollviewn.showsHorizontalScrollIndicator = NO;
    scrollviewn.showsVerticalScrollIndicator = NO;
    scrollviewn.delegate = self;
    [self.view addSubview:scrollviewn];
    //
    UIScreenEdgePanGestureRecognizer *edgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(edgePanGesture:)];
    edgePanGestureRecognizer.delegate = self;
    edgePanGestureRecognizer.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:edgePanGestureRecognizer];
    //
    subScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth , ScreenHeight-45.0)];
    subScrollView.backgroundColor = [UIColor whiteColor];
    subScrollView.bounces = YES;
    subScrollView.pagingEnabled = NO;
    subScrollView.userInteractionEnabled = YES;
    subScrollView.showsHorizontalScrollIndicator = NO;
    subScrollView.showsVerticalScrollIndicator = NO;
    [scrollviewn addSubview:subScrollView];
    //
    bgIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth , ScreenWidth*9.0/16.0)];
    bgIV.image = self.adCampaignEntity.image;
    [subScrollView addSubview:bgIV];
    //
    UILabel*bgLabel = [[UILabel alloc]initWithFrame:bgIV.bounds];
    bgLabel.backgroundColor = SysColorCover;
    [bgIV addSubview:bgLabel];
    //
    UIView*scrollViewNavView = [[UIView alloc]initWithFrame:CGRectMake(0,0, self.view.width, 64)];
    [subScrollView addSubview:scrollViewNavView];
    //
    UILabel *navCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.0, 20.0, self.view.width-100.0, 44)];
    navCenterLabel.font = font_cu_17;
    navCenterLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    navCenterLabel.textAlignment = NSTextAlignmentCenter;
    navCenterLabel.userInteractionEnabled = YES;
    [scrollViewNavView addSubview:navCenterLabel];
    //
    UILabel*navLeftLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 20.0+(44.0-20.0)/2.0, 20.0, 20.0)];
    navLeftLabel.font = font_icon_(20.0);
    navLeftLabel.text = Icon_bar_back;
    navLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    [scrollViewNavView addSubview:navLeftLabel];
    //
    UIButton*navLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navLeftBtn.frame = CGRectMake(0, 20.0, 44.0, 44.0);
    [navLeftBtn addTarget:self
                   action:@selector(navLeftBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];
    [scrollViewNavView addSubview:navLeftBtn];
    //
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, bgIV.bottom+16.0, scrollviewn.width, 18)];
    titleLabel.font = font_cu_15;
    titleLabel.text = [NSString stringWithFormat:@"%@",self.adCampaignEntity.name];
    titleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:titleLabel];
    //
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, titleLabel.bottom+6.0, scrollviewn.width, 13)];
    timeLabel.font = font_11;
    timeLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:timeLabel];
    //
    timeLabel.text = [NSString stringWithFormat:@"%@ - %@",self.adCampaignEntity.start_time,self.adCampaignEntity.deadline];
    CGSize timeLabelSize = [Utils getUIFontSizeFitW:timeLabel];
    timeLabel.width = timeLabelSize.width+8.0;
    timeLabel.left = (ScreenWidth-timeLabel.width-8.0)/2.0;
    //
    UITextView*contentTV = [[UITextView alloc]initWithFrame:CGRectMake(20, timeLabel.bottom+10.0, scrollviewn.width-20.0*2, subScrollView.height-35.0-64.0-40.0-18.0-14.0-40.0-timeLabel.bottom-10.0)];
    contentTV.font = font_13;
    contentTV.text =  self.adCampaignEntity.idescription;
    contentTV.text = [contentTV.text stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    contentTV.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    contentTV.editable = NO;
    contentTV.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:contentTV];
    CGSize contentTVSize = [Utils getUITextViewSizeFitH:contentTV withLineSpacing:6.0];
    if (contentTVSize.height+6.0*2.0 < contentTV.height) {
        contentTV.height = contentTVSize.height+6.0*2.0;
    }
    //
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subScrollView.height-35.0-64.0-40.0-18.0-14.0-40.0, scrollviewn.width, 0.5)];
    lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
    [subScrollView addSubview:lineLabel];
    // 活动类型
    tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, subScrollView.height-35.0-64.0-40.0-18.0-14.0, scrollviewn.width, 18.0)];
    tagLabel.font = font_cu_15;
    tagLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    tagLabel.textAlignment = NSTextAlignmentCenter;
    [subScrollView addSubview:tagLabel];
    // 底部框
    UIView*tagView = [[UIImageView alloc] initWithFrame:CGRectMake(30.0, subScrollView.height-35.0-64.0-40.0, ScreenWidth-2*30.0, 64.0)];
    tagView.backgroundColor = [Utils getUIColorWithHexString:SysColorSubBlack];
    [subScrollView addSubview:tagView];
    //
    tagTextLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, (tagView.height-18.0-15.0-5.0)/2.0,tagView.width, 18.0)];
    tagTextLabel.font = font_cu_15;
    tagTextLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
    tagTextLabel.textAlignment = NSTextAlignmentCenter;
    [tagView addSubview:tagTextLabel];
    //
    tagTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, tagTextLabel.bottom+5.0, tagView.width, 15)];
    tagTimeLabel.font = font_11;
    tagTimeLabel.textColor = [Utils getUIColorWithHexString:SysColorYellow];
    tagTimeLabel.textAlignment = NSTextAlignmentCenter;
    [tagView addSubview:tagTimeLabel];
    // 活动详情
    UIButton*detailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    detailBtn.frame = CGRectMake(0, subScrollView.height-26.0, ScreenWidth, 26.0);
    [detailBtn addTarget:self
                  action:@selector(detailBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [subScrollView addSubview:detailBtn];
    //
    UILabel *btnLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 13.0)];
    btnLabel.font = font_11;
    btnLabel.text = NSLocalizedString(@"R2026", @"推广内容详情");
    btnLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    btnLabel.textAlignment=NSTextAlignmentCenter;
    [detailBtn addSubview:btnLabel];
    //
    UILabel*downLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth-13.0)/2.0, btnLabel.bottom, 13.0, 13.0)];
    downLabel.font = font_icon_(13.0);
    downLabel.text = Icon_btn_down;
    downLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
    [detailBtn addSubview:downLabel];
    //
    [self updateText];
    [self updateFooterView];
}

-(void)updateText {
    NSString*per_budget_type = [NSString stringWithFormat:@"%@",self.adCampaignEntity.per_budget_type];
    NSString*per_action_budget = [NSString stringWithFormat:@"%@",[Utils getNSStringTwoFloat:self.adCampaignEntity.per_action_budget]];
    if ([per_budget_type isEqualToString:@"click"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2014",@"点击"),per_action_budget];
        tagTextLabel.text = NSLocalizedString(@"R2027", @"分享后好友点击此文章立即获得报酬");
    } else if ([per_budget_type isEqualToString:@"cpt"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2301",@"任务"),per_action_budget];
        tagTextLabel.text = NSLocalizedString(@"R2302", @"分享后完成指定任务立即获得报酬");
    } else if ([per_budget_type isEqualToString:@"cpa"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2015",@"效果"),per_action_budget];
        tagTextLabel.text = NSLocalizedString(@"R2028", @"分享后好友完成指定活动立即获得报酬");
    } else if ([per_budget_type isEqualToString:@"simple_cpi"]) {
        tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2200",@"下载"),per_action_budget];
        tagTextLabel.text = NSLocalizedString(@"R2201", @"分享后下载指定APP立即获得报酬");
    } else {
        tagLabel.text = [NSString stringWithFormat:@"%@丨￥%@",NSLocalizedString(@"R2017",@"转发"),per_action_budget];
        tagTextLabel.text = NSLocalizedString(@"R2029", @"分享此文章立即获得报酬");
    }
    tagTextLabel.frame = CGRectMake(0, 0, ScreenWidth-2*30.0, 64.0);
}

- (void)loadWebView {
    webViewNV = [[UIView alloc]initWithFrame:CGRectMake(0, scrollviewn.height, self.view.width, 64)];
    webViewNV.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    [scrollviewn addSubview:webViewNV];
    //
    UILabel *navCenterLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20.0, self.view.width, 44)];
    navCenterLabel.text = NSLocalizedString(@"R2026", @"推广内容详情");
    navCenterLabel.font = font_cu_17;
    navCenterLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    navCenterLabel.textAlignment = NSTextAlignmentCenter;
    navCenterLabel.userInteractionEnabled = YES;
    [webViewNV addSubview:navCenterLabel];
    //
    UILabel*navLeftLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, 20.0+(44.0-20.0)/2.0, 20.0, 20.0)];
    navLeftLabel.font = font_icon_(20.0);
    navLeftLabel.text = Icon_bar_back;
    navLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [webViewNV addSubview:navLeftLabel];
    //
    UIButton*navLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navLeftBtn.frame = CGRectMake(0, 20.0, 44.0, 44.0);
    [navLeftBtn addTarget:self
                      action:@selector(webLeftBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
    [webViewNV addSubview:navLeftBtn];
    //
    UILabel*navLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, webViewNV.height-0.5, webViewNV.width, 0.5)];
    navLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBarLine];
    [webViewNV addSubview:navLineLabel];
    //
    webviewn = [[UIWebView alloc] initWithFrame:CGRectMake(10, webViewNV.bottom, ScreenWidth-20.0, ScreenHeight-45.0-64.0)];
    webviewn.scalesPageToFit = YES;
    if ([[self.adCampaignEntity.url componentsSeparatedByString:@"http"]count]>1) {
        [webviewn loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.adCampaignEntity.url]]];
    } else {
        [webviewn loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",self.adCampaignEntity.url]]]];
    }
    [scrollviewn addSubview:webviewn];
    //
    [scrollviewn setContentSize:CGSizeMake(scrollviewn.width, webviewn.bottom)];
}

#pragma mark - FooterView Delegate
- (void)updateFooterView {
    [footerView removeFromSuperview];
    footerView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight-45.0, ScreenWidth, 45.0)];
    [self.view addSubview:footerView];
    //
    footerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    footerBtn.frame = CGRectMake(0, 0, footerView.width, footerView.height);
    footerBtn.titleLabel.font = font_cu_15;
    [footerBtn setTitleColor:[Utils getUIColorWithHexString:SysColorWhite] forState:UIControlStateNormal];
    [footerView addSubview:footerBtn];
    [footerBtn setTitle:NSLocalizedString(@"R2033", @"立即分享") forState:UIControlStateNormal];
    footerBtn.backgroundColor = [Utils getUIColorWithHexString:SysColorBlue];
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self changeStatusBar];
}

-(void)changeStatusBar {
    if (scrollviewn.contentOffset.y == scrollviewn.height) {
        [[UIApplication sharedApplication]
         setStatusBarStyle:UIStatusBarStyleDefault];
    } else {
        [[UIApplication sharedApplication]
         setStatusBarStyle:UIStatusBarStyleLightContent];
    }
}

#pragma mark - UIButton Delegate
- (void)navLeftBtnAction:(UIButton *)sender {
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)webLeftBtnAction:(UIButton *)sender {
    [self changeStatusBar];
    [scrollviewn scrollRectToVisible:CGRectMake(0, 0, scrollviewn.width, scrollviewn.height) animated:YES];
}

- (void)detailBtnAction:(UIButton *)sender {
    [self changeStatusBar];
    [scrollviewn scrollRectToVisible:CGRectMake(0, scrollviewn.height, scrollviewn.width, scrollviewn.height) animated:YES];
}
@end
