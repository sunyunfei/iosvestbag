//
//  RBAppDelegate.h
//  RBAppDelegate
//
//  Created by AngusNi on 14-3-5.
//  Copyright (c) 2014年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
//
#import "RBTabCampaignViewController.h"
#import "RBTabKolViewController.h"
#import "RBTabUserViewController.h"
#import "RBLoginViewController.h"
#import "RBTabCpsViewController.h"
#import "RBInfluenceLoadingViewController.h"
#import "RBbindSocialController.h"
#import "TBTabBarController.h"

//
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 100000
@interface RBAppDelegate : UIResponder
<UIApplicationDelegate, UIAlertViewDelegate, WXApiDelegate, GeTuiSdkDelegate, RCIMUserInfoDataSource, HandlerDelegate , RBAlertViewDelegate>
#else
@interface RBAppDelegate : UIResponder
<UIApplicationDelegate, UIAlertViewDelegate, WXApiDelegate, GeTuiSdkDelegate, RCIMUserInfoDataSource, HandlerDelegate , RBAlertViewDelegate,UNUserNotificationCenterDelegate>
#endif
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) NSMutableArray *localData;
- (void)loadViewController:(int)tag;
- (void)getRongIMConnect;

@end
