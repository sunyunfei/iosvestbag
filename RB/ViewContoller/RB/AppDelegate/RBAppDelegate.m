//
//  RBAppDelegate.m
//  RBAppDelegate
//
//  Created by AngusNi on 14-3-5.
//  Copyright (c) 2014年 AngusNi. All rights reserved.
//

#import "RBAppDelegate.h"
@implementation RBAppDelegate
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    // TalkingData Adtracking
    if([AppBundleIdentifier isEqualToString:@"com.robin8.luobin"]) {
        [TalkingDataAppCpa onReceiveDeepLink:url];
    }
    
    if ([url.host isEqualToString:@"safepay"]) {
        // 跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"applicationresultDic = %@",resultDic);
            NSLog(@"result = %@",[resultDic valueForKey:@"result"]);
            NSLog(@"memo = %@",[resultDic valueForKey:@"memo"]);
            NSLog(@"resultStatus = %@",[resultDic valueForKey:@"resultStatus"]);
            int tempTag = [NSString stringWithFormat:@"%@",[resultDic valueForKey:@"resultStatus"]].intValue;
            NSString*tempStr = @"";
            if(tempTag == 9000) {
                tempStr = NSLocalizedString(@"R6028", @"订单支付成功");
            } else if(tempTag == 8000) {
                tempStr = NSLocalizedString(@"R6051", @"正在处理中");
            } else if(tempTag == 4000) {
                tempStr = NSLocalizedString(@"R6039", @"订单支付失败");
            } else if(tempTag == 6001) {
                tempStr = NSLocalizedString(@"R6052", @"用户中途取消");
            } else if(tempTag == 6002) {
                tempStr = NSLocalizedString(@"R6053", @"网络连接出错");
            }
            NSLog(@"tempStr = %@",tempStr);
            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationAlipayStatus
                                                                object:self
                                                              userInfo:@{@"STATUS":tempStr}];
        }];
    }
    return YES;
}

// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {

    // TalkingData Adtracking
    if([AppBundleIdentifier isEqualToString:@"com.robin8.luobin"]) {
        [TalkingDataAppCpa onReceiveDeepLink:url];
    }
    
    if ([url.host isEqualToString:@"safepay"]) {
        // 跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"applicationresultDic = %@",resultDic);
            NSLog(@"result = %@",[resultDic valueForKey:@"result"]);
            NSLog(@"memo = %@",[resultDic valueForKey:@"memo"]);
            NSLog(@"resultStatus = %@",[resultDic valueForKey:@"resultStatus"]);
            int tempTag = [NSString stringWithFormat:@"%@",[resultDic valueForKey:@"resultStatus"]].intValue;
            NSString*tempStr = @"";
            if(tempTag == 9000) {
                tempStr = NSLocalizedString(@"R6028", @"订单支付成功");
            } else if(tempTag == 8000) {
                tempStr = NSLocalizedString(@"R6051", @"正在处理中");
            } else if(tempTag == 4000) {
                tempStr = NSLocalizedString(@"R6039", @"订单支付失败");
            } else if(tempTag == 6001) {
                tempStr = NSLocalizedString(@"R6052", @"用户中途取消");
            } else if(tempTag == 6002) {
                tempStr = NSLocalizedString(@"R6053", @"网络连接出错");
            }
            NSLog(@"tempStr = %@",tempStr);
            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationAlipayStatus
                                                                object:self
                                                              userInfo:@{@"STATUS":tempStr}];
        }];
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray * __nullable restorableObjects))restorationHandler
{
    // TalkingData Adtracking
    if([AppBundleIdentifier isEqualToString:@"com.robin8.luobin"]) {
        [TalkingDataAppCpa onReceiveDeepLink:userActivity.webpageURL];
    }
    return YES;
}

- (void)registerRemoteNotification {
    /*
     警告：Xcode8 需要手动开启"TARGETS -> Capabilities -> Push Notifications"
     */
    
    /*
     警告：该方法需要开发者自定义，以下代码根据 APP 支持的 iOS 系统不同，代码可以对应修改。
     以下为演示代码，注意根据实际需要修改，注意测试支持的 iOS 系统都能获取到 DeviceToken
     */
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0 // Xcode 8编译会调用
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionCarPlay) completionHandler:^(BOOL granted, NSError *_Nullable error) {
            if (!error) {
                NSLog(@"request authorization succeeded!");
            }
        }];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
#else // Xcode 7编译会调用
        UIUserNotificationType types = (UIUserNotificationTypeAlert | UIUserNotificationTypeSound | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
#endif
    } else if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIUserNotificationType types = (UIUserNotificationTypeAlert | UIUserNotificationTypeSound | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        UIRemoteNotificationType apn_type = (UIRemoteNotificationType)(UIRemoteNotificationTypeAlert |
                                                                       UIRemoteNotificationTypeSound |
                                                                       UIRemoteNotificationTypeBadge);
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:apn_type];
    }
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // 当程序载入后执行
    
//    // 注册APNS
//    if ([Utils getCurrentDeviceSystemVersion].floatValue >= 10) {
//        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//        center.delegate = self;
//        [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error) {
//            NSLog(@"通知未打开,请设置打开");
//        } ];
//    } else {
//        UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
//        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
//        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
//        [[UIApplication sharedApplication] registerForRemoteNotifications];
//        if ([[UIApplication sharedApplication] currentUserNotificationSettings].types == UIUserNotificationTypeNone) { NSLog(@"通知未打开,请设置打开");}
//    }
//    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];


    // 后台运行
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];

    
    // SDWebImage
//    [[SDImageCache sharedImageCache] setMaxMemoryCost:1024 * 1024 * 1];
//    [[SDImageCache sharedImageCache] setMaxCacheAge:3600 * 24 * 7];
//    [[SDImageCache sharedImageCache] setShouldDecompressImages:NO];
//    [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
//    [[SDImageCache sharedImageCache] setShouldCacheImagesInMemory:NO];

    
    // CoreData
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"RBModel.sqlite"];
    
    
    // TalkingData SDK
    if([AppBundleIdentifier isEqualToString:@"com.robin8.luobin"] && TalkingDataAppKey.length!=0) {
        [TalkingData setExceptionReportEnabled:YES]; //收集用户错误日志
        [TalkingData sessionStarted:TalkingDataAppKey
                      withChannelId:@"IOS"];
    }
    
    // TalkingData Adtracking AppCpa SDK
    if([AppBundleIdentifier isEqualToString:@"com.robin8.luobin"] && TalkingDataCpaAppKey.length!=0) {
        [TalkingDataAppCpa init:TalkingDataCpaAppKey withChannelId:@"AppStore"];
    }
    
    // GeTuiSdk
    [GeTuiSdk startSdkWithAppId:GtSDKAppId appKey:GtSDKAppKey appSecret:GtSDKAppSecret delegate:self];
    [self registerRemoteNotification];
    [self receiveNotificationByLaunchingOptions:launchOptions];
    
    // RCIM
    [[RCIMClient sharedRCIMClient] initWithAppKey:RCIMAppKey];
    [[RCIM sharedRCIM] initWithAppKey:RCIMAppKey];
    [self getRongIMConnect];
    [[RCIM sharedRCIM] setUserInfoDataSource:self];
 
    // ShareSDK
    [ShareSDK registerApp:ShareSDKAppKey
          activePlatforms:@[
                            @(SSDKPlatformTypeSinaWeibo),
                            @(SSDKPlatformTypeWechat),@(SSDKPlatformTypeQQ)]
                 onImport:^(SSDKPlatformType platformType){
                     switch (platformType) {
                         case SSDKPlatformTypeWechat:
                             [ShareSDKConnector connectWeChat:[WXApi class]];
                             break;
                         case SSDKPlatformTypeSinaWeibo:
                             [ShareSDKConnector connectWeibo:[WeiboSDK class]];
                             break;
                         case SSDKPlatformTypeQQ:
                             [ShareSDKConnector connectQQ:[QQApiInterface class]
                                        tencentOAuthClass:[TencentOAuth class]];
                             break;
                         default:
                             break;
                     }
                 } onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo) {
                     switch (platformType) {
                         case SSDKPlatformTypeSinaWeibo:
                             [appInfo SSDKSetupSinaWeiboByAppKey:SinaWeiboAppKey
                                                       appSecret:SinaWeiboAppSecret
                                                     redirectUri:SinaWeiboAppUrl
                                                        authType:SSDKAuthTypeBoth];
                             break;
                         case SSDKPlatformTypeWechat:
                             [appInfo SSDKSetupWeChatByAppId:WeChatAppId
                                                   appSecret:WeChatAppSecret];
                             break;
                         case SSDKPlatformTypeQQ:
                             [appInfo SSDKSetupQQByAppId:QQAppId
                                                  appKey:QQAppSecret
                                                authType:SSDKAuthTypeSSO];
                             break;
                         default:
                             break;
                     }
                 }];
    
    
    // IDFA
    if ([LocalService getRBLocalDataUserIDFA]==nil||[LocalService getRBLocalDataUserIDFA].length==0) {
        [LocalService setRBLocalDataUserIDFAWith:[[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString]];
    }
    
    
    //    // Google广告统计
    //    [ACTConversionReporter reportWithConversionID:@"937000043" label:@"m46RCK3p02QQ6_jlvgM" value:@"1.00" isRepeatable:NO];
    
    
    // 延迟0.5秒显示
    //    [NSThread sleepForTimeInterval:0.5];
    
    // RB-页面加载
    // RB-根据IP获取城市名
//    Handler*handler = [Handler shareHandler];
//    [handler getRBInfluenceCityName];
    if ([LocalService getRBLocalDataUserPrivateToken] != nil) {
            [self loadViewController:1];

    } else {
        // RB-游客登录
        Handler*handler = [Handler shareHandler];
        handler.delegate = self;
        [handler getRBUserLoginWithPhone:@"13000000000" andCode:@"123456" andInvteCode:nil];
    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // 当应用程序将要入非活动状态时执行,在此期间,应用程序不接收消息或事件,比如来电话了
    NSLog(@"applicationWillResignActive");
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSLog(@"applicationDidBecomeActive");
    // 当应用程序进入活动状态时执行,这个刚好跟上面那个方法相反
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
    [self getRongIMConnect];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSLog(@"applicationDidEnterBackground");
    // 当程序被推送到后台时调用。所以要设置后台继续运行,则在这个函数里面设置即可
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    NSLog(@"applicationWillEnterForeground");
    // 当程序从后台将要重新回到前台时调用,这个刚好跟上面的那个方法相反
}

- (void)applicationWillTerminate:(UIApplication *)application {
    NSLog(@"applicationWillTerminate");
    // 当程序将要退出时调用,通常是用来保存数据和一些退出前的清理工作。这个需要要设置UIApplicationExitsOnSuspend的键值
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    // 当程序可用内存不足时调用,通常是用来进行内存清理工作
    NSLog(@"applicationDidReceiveMemoryWarning");
    // 清空缓存
    [Service clearImageCache];
}

#ifdef __IPHONE_10_0
// App在前台时候回调
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    
    NSLog(@"willPresentNotification:%@", notification.request.content.userInfo);
    completionHandler(UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert);
}

// App在后台时候点击推送调用
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
//    NSLog(@"didReceiveNotificationResponse:%@", response.notification.request.content.userInfo);
//}
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler{
    NSLog(@"didReceiveNotificationResponse:%@", response.notification.request.content.userInfo);
}
#endif


#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    // 注册本地通知成功
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler {
    // 收到本地通知
    // 在没有启动本App时,收到本地消息,下拉消息会有快捷回复的按钮,点击按钮后调用的方法,根据identifier来判断点击的哪个按钮
    NSLog(@"%@----%@", identifier, notification);
    completionHandler();
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler {
    // 收到推送通知
    // 在没有启动本App时,收到推送消息,下拉消息会有快捷回复的按钮,点击按钮后调用的方法,根据identifier来判断点击的哪个按钮
    NSLog(@"%@----%@", identifier, userInfo);
    completionHandler();
}
#endif

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    // 收到本地通知
    NSString *bodyStr = [NSString stringWithFormat:@"%@", notification.alertBody];
    if ([bodyStr isEqualToString:@"照片不可用,请设置打开"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:bodyStr delegate:self cancelButtonTitle:NSLocalizedString(@"R1020", @"确定") otherButtonTitles:nil];
        alert.tag = 1000;
        if ([bodyStr isEqualToString:@"麦克风不可用,请设置打开"] || [bodyStr isEqualToString:@"蓝牙不可用,请设置打开"] || [bodyStr isEqualToString:@"相机不可用,请设置打开"] || [bodyStr isEqualToString:@"照片不可用,请设置打开"] || [bodyStr isEqualToString:@"位置不可用,请设置打开"] || [bodyStr isEqualToString:@"蓝牙共享不可用,请设置打开"] || [bodyStr isEqualToString:@"通讯录不可用,请设置打开"]) {
            alert.tag = 2000;
        }
        [alert show];
    }
    
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 2000) {
        // IOS8以上系统可直接跳转到本应用的设置界面
        if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
        return;
    }
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // 注册推送通知成功
    NSString *token = [[[[deviceToken description]
                         stringByReplacingOccurrencesOfString:@"<" withString:@""]
                        stringByReplacingOccurrencesOfString:@">" withString:@""]
                       stringByReplacingOccurrencesOfString:@" " withString:@""];
    [LocalService setRBLocalDataUserDeviceTokenWith:token];
    // GeTuiSdk
    NSLog(@"token:%@,clientId:%@",token,[GeTuiSdk clientId]);
    [GeTuiSdk registerDeviceToken:token];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    // 注册推送通知失败
    NSLog(@"注册推送通知失败");
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // App运行在后台/App运行在前台
    NSLog(@"[Receive RemoteNotification]IOS6:%@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {
    // 处理APNs代码，通过userInfo可以取到推送的信息（包括内容，角标，自定义参数等）。如果需要弹窗等其他操作，则需要自行编码。
    NSLog(@"\n>>>[Receive RemoteNotification - Background Fetch]:%@\n\n",userInfo);
    
    //静默推送收到消息后也需要将APNs信息传给个推统计
    [GeTuiSdk handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    NSLog(@"Background Fetch 恢复SDK 运行");
    [GeTuiSdk resume];
    completionHandler(UIBackgroundFetchResultNewData);
}

#pragma mark - GeTuiSdkDelegate
// 自定义：APP被“推送”启动时处理推送消息处理（APP 未启动--》启动）
- (void)receiveNotificationByLaunchingOptions:(NSDictionary *)launchOptions {
    if (!launchOptions) {
        return;
    }
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (userInfo) {
        NSLog(@"\n>>>[Launching RemoteNotification]:%@", userInfo);
    }
}

// SDK启动成功返回clientId
- (void)GeTuiSdkDidRegisterClient:(NSString *)clientId {
    NSLog(@"GexinSdk,clientId:%@", clientId);
    [LocalService setRBLocalDataUserClientIdWith:clientId];
}

// SDK遇到错误回调
- (void)GeTuiSdkDidOccurError:(NSError *)error {
    NSLog(@"\n>>>[GexinSdk error]:%@\n\n", [error localizedDescription]);
}

/** SDK收到透传消息回调 */
- (void)GeTuiSdkDidReceivePayloadData:(NSData *)payloadData andTaskId:(NSString *)taskId andMsgId:(NSString *)msgId andOffLine:(BOOL)offLine fromGtAppId:(NSString *)appId {
    //收到个推消息
    // 脚标清0
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
    NSLog(@"\n>>>[GexinSdk DidReceivePayload]offLine:%D", offLine);

    NSString *payloadMsg = nil;
    if (payloadData) {
        payloadMsg = [[NSString alloc] initWithBytes:payloadData.bytes length:payloadData.length encoding:NSUTF8StringEncoding];
        NSLog(@"payloadMsg:%@",payloadMsg);
        RBPushEntity *entity = [JsonService getRBPushEntity:[payloadMsg JSONValue]];
        if ([LocalService getRBLocalDataUserPrivateToken]!=nil) {
            RBUserEntity*userEntity=[JsonService getRBUserEntity];
            if (userEntity.mobile_number.length!=0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NotificationRefreshHomeView
                                                                    object:self
                                                                  userInfo:nil];
                if (offLine==1) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationShowMessageView
                                                                        object:self
                                                                      userInfo:nil];
                }else{
                    UILocalNotification * notification = [[UILocalNotification alloc]init];
                    notification.alertTitle = [NSString stringWithFormat:@"%@",entity.title];
                    notification.alertBody = [NSString stringWithFormat:@"%@",entity.name];
                    [[UIApplication sharedApplication]presentLocalNotificationNow:notification];
                // 新收入
                if ([[NSString stringWithFormat:@"%@",entity.action] isEqualToString:@"income"]) {
                    
                }
                // 公告
                if ([[NSString stringWithFormat:@"%@",entity.action] isEqualToString:@"announcement"]) {
                
                }
                // 新活动
                if ([[NSString stringWithFormat:@"%@",entity.action] isEqualToString:@"campaign"]) {
                    
                }
                
                }
            }
        }
    }
}

// SDK收到sendMessage消息回调
- (void)GeTuiSdkDidSendMessage:(NSString *)messageId result:(int)result {
    // [4-EXT]:发送上行消息结果反馈
    NSLog(@"\n>>>[GexinSdk DidSendMessage]:%@\n\n", [NSString stringWithFormat:@"sendmessage=%@,result=%d", messageId, result]);
}

// SDK运行状态通知
- (void)GeTuiSDkDidNotifySdkState:(SdkStatus)aStatus {
    // [EXT]:通知SDK运行状态
    NSLog(@"\n>>>[GexinSdk SdkState]:%u\n\n", aStatus);
}

// SDK设置推送模式回调
- (void)GeTuiSdkDidSetPushMode:(BOOL)isModeOff error:(NSError *)error {
    if (error) {
        NSLog(@"\n>>>[GexinSdk SetModeOff Error]:%@\n\n", [error localizedDescription]);
        return;
    }
    NSLog(@"\n>>>[GexinSdk SetModeOff]:%@\n\n", isModeOff ? @"开启" : @"关闭");
}

#pragma mark - RongIM Delegate
- (void)getRongIMConnect {
    if ([LocalService getRBLocalDataUserPrivateToken]!=nil) {
        if ([LocalService getRBLocalDataUserRongToken]!=nil) {
            if(!([[RCIM sharedRCIM] getConnectionStatus]==ConnectionStatus_Connected)){
                [[RCIM sharedRCIM] connectWithToken:[LocalService getRBLocalDataUserRongToken] success:^(NSString*userId){
                    NSLog(@"RCIM,success:%@",userId);
                } error:^(RCConnectErrorCode status) {
                    NSLog(@"RCIM,error:%d",(int)status);
                    if ([LocalService getRBLocalDataUserLoginPhone]!=nil) {
                        // 获取融云token
                        Handler*handler = [Handler shareHandler];
                        [handler getNetWorkRongCloudToken];
                    }
                } tokenIncorrect:^{
                    NSLog(@"RCIM,tokenIncorrect");
                }];
            }
        } else {
            if ([LocalService getRBLocalDataUserLoginPhone]!=nil) {
                // 获取融云token
                Handler*handler = [Handler shareHandler];
                [handler getNetWorkRongCloudToken];
            }
        }
    }
}

#pragma mark - RCIMUserInfoDataSource
- (void)getUserInfoWithUserId:(NSString *)userId
                   completion:(void (^)(RCUserInfo *userInfo))completion{
    RCUserInfo*userInfo = [[RCUserInfo alloc]init];
    userInfo.userId = [NSString stringWithFormat:@"%@",[LocalService getRBLocalDataUserLoginPhone]];
    userInfo.name = [NSString stringWithFormat:@"%@",[LocalService getRBLocalDataUserLoginName]];
    userInfo.portraitUri = [NSString stringWithFormat:@"%@",[LocalService getRBLocalDataUserLoginAvatar]];
    completion(userInfo);
}

#pragma mark - UIStatusBar Delegate
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    CGPoint location = [[[event allTouches] anyObject] locationInView:[self window]];
    CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
    if (CGRectContainsPoint(statusBarFrame, location)) {
        [self statusBarTouchedAction];
    }
}

- (void)statusBarTouchedAction {
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationStatusBarAction
                                                        object:nil];
}

#pragma mark - loadViewController Delegate
- (void)loadViewController:(int)tag {
    // UIWindow
    if(self.window==nil) {
        self.window.backgroundColor = [UIColor whiteColor];
        self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    }
    if (tag==2) {
        // loginPhonePage
        [self.window setRootViewController:[[UINavigationController alloc] initWithRootViewController:[[RBLoginPhoneViewController alloc] init]]];
    }
    if (tag==0) {
        // loginPage
        [self.window setRootViewController:[[UINavigationController alloc] initWithRootViewController:[[RBLoginViewController alloc] init]]];
    }
    if (tag == 4) {
        [self.window setRootViewController:[[RBInfluenceLoadingViewController alloc]init]];
    }
    if (tag==3) {
        TBTabBarController * tabBarControllern = [[TBTabBarController alloc]init];
        tabBarControllern.selectedIndex = 2;
        [self.window setRootViewController:tabBarControllern];
    }
    if (tag == 22) {
        TBTabBarController * tabBarControllern = [[TBTabBarController alloc]init];
        tabBarControllern.selectedIndex = 0;
        [self.window setRootViewController:tabBarControllern];
    }
    if(tag == 33){
        TBTabBarController * tabBarControllern = [[TBTabBarController alloc]init];
        tabBarControllern.selectedIndex = 1;
        [self.window setRootViewController:tabBarControllern];
    }
    if (tag==1) {
        TBTabBarController * tabBarControllern = [[TBTabBarController alloc]init];
        tabBarControllern.selectedIndex = 0;
        [self.window setRootViewController:tabBarControllern];
        
    }
    [self.window makeKeyAndVisible];
}


#pragma mark - Handler Delegate
- (void)handlerSuccess:(id)jsonObject Tag:(NSString *)sender {
    if ([sender isEqualToString:@"kols/sign_in"]) {
        [GeTuiSdk setPushModeForOff:NO];
        [JsonService setRBUserEntityWith:jsonObject];
        RBUserEntity*userEntity = [JsonService getRBUserEntity:jsonObject];
        [LocalService setRBLocalDataUserLoginIdWith:userEntity.iid];
        [LocalService setRBLocalDataUserLoginNameWith:userEntity.name];
        [LocalService setRBLocalDataUserLoginPhoneWith:userEntity.mobile_number];
        [LocalService setRBLocalDataUserLoginAvatarWith:userEntity.avatar_url];
        [LocalService setRBLocalDataUserPrivateTokenWith:userEntity.issue_token];
        [LocalService setRBLocalDataUserLoginKolIdWith:userEntity.kol_uuid];
        //
        [self loadViewController:1];

        [self getRongIMConnect];
    }
}

- (void)handlerErrorJson:(id)jsonObject Tag:(NSString *)sender {
    [self loadViewController:1];

}

- (void)handlerError:(NSError *)error Tag:(NSString *)sender {
    [self loadViewController:1];
    [LocalService setRBDevelopIPURL:@"http://api.robin8.net/"];
}

@end
