//
//  RBBaseViewController.m
//  RB
//
//  Created by AngusNi on 16/1/28.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"
#import "RBUserMessageViewController.h"
#import "RBUserSettingFeedBackViewController.h"
#import "RBAppDelegate.h"
#import "RBLoginViewController.h"

@interface RBBaseViewController () {
}
@end

@implementation RBBaseViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Utils getIOS7UINavigationController:self];
    self.view.backgroundColor = [UIColor whiteColor];
    // 背景图片
    self.bgIV = [[UIImageView alloc]initWithFrame:self.view.bounds];
    self.bgIV.image = [UIImage imageNamed:@"pic_bg.jpg"];
    [self.view addSubview:self.bgIV];
    self.bgIV.hidden = YES;
    // 监听是否刷新首页数据
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationRefreshHomeView:)
                                                 name:NotificationRefreshHomeView
                                               object:nil];
    // 监听是否刷新夺宝列表数据
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationRefreshIndianaView:)
                                                 name:NotificationRefreshIndianaView
                                               object:nil];
    // 监听是否显示消息页面
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationShowMessageView:)
                                                 name:NotificationShowMessageView
                                               object:nil];
    // 监听是否显示登录页面
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationShowLoginView:)
                                                 name:NotificationShowLoginView
                                               object:nil];
    // 监听是否刷新收藏页面
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationRefreshArticleView:)
                                                 name:NotificationRefreshArticleView
                                               object:nil];
    // 监听是否刷新用户数据
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationRefreshUserInfo:)
                                                 name:NotificationRefreshUserInfo
                                               object:nil];
    // 监听是否点击StatusBar
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationStatusBarAction:)
                                                 name:NotificationStatusBarAction
                                               object:nil];
    // 监听是否进入非活动状态
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationResignActive:)
                                                 name:UIApplicationWillResignActiveNotification object:nil];
    // 监听是否进入活动状态
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    // 监听是否Dismiss
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RBNotificationDismissView:)
                                                 name:NotificationDismissView object:nil];
    //返回活动页
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RBNotificationBackToHome:) name:NotificationBackToHome object:nil];
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RBNotificationCampaignAddCover:) name:NotificationCampainAddCoverView object:nil];
    // 绑定第三方账号数据
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(RBNotificationRefreshApply:) name:NotificationApplyInfo object:nil];
    // SHAKE
    if([LocalService getRBLocalDataUserShake]==nil || [LocalService getRBLocalDataUserShake].length==0){
        [[UIApplication sharedApplication] setApplicationSupportsShakeToEdit:YES];
    } else {
        [[UIApplication sharedApplication] setApplicationSupportsShakeToEdit:NO];
    }
    //
    [self.tabBarController.tabBar setBackgroundImage:[UIImage imageNamed:@"nav_bg.png"]];
    self.tabBarController.tabBar.barStyle = UIBarStyleBlack;
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}
    
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [self.hudView dismiss];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [SVProgressHUD dismiss];
    [self.hudView dismiss];
    [Service clearImageCache];
}

#pragma mark- NavigationView Delegate
- (void)loadNavigationView {
    if (self.navView!=nil) {
        return;
    }
    self.navView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, NavHeight)];
    self.navView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite andAlpha:0.5];
    self.navView.userInteractionEnabled = YES;
    [self.view addSubview:self.navView];
    //
    self.navTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.0, StatusHeight, self.view.width-100.0, 44)];
    self.navTitleLabel.font = font_cu_cu_17;
    self.navTitleLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    self.navTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navView addSubview:self.navTitleLabel];
    //
    self.navLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.navView.bottom-0.5, self.view.width, 0.5)];
    self.navLineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorBarLine];
    [self.navView addSubview:self.navLineLabel];
    //
    self.navLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.navLeftBtn.frame = CGRectMake(0, StatusHeight, 44.0, 44.0);
    [self.navLeftBtn addTarget:self
                   action:@selector(navLeftBtnAction)
         forControlEvents:UIControlEventTouchUpInside];
    self.navLeftBtn.titleLabel.font = font_17;
    [self.navLeftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.navView addSubview:self.navLeftBtn];
    //
    self.navLeftLabel = [[UILabel alloc]initWithFrame:CGRectMake(CellLeft, (44.0-20.0)/2.0, 20.0, 20.0)];
    self.navLeftLabel.font = font_icon_(20.0);
    self.navLeftLabel.text = Icon_bar_back;
    self.navLeftLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [self.navLeftBtn addSubview:self.navLeftLabel];
    //
    UIButton*navRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navRightBtn.frame = CGRectMake(self.navView.width-44.0, StatusHeight, 44.0, 44.0);
    navRightBtn.tag = 10000;
    [navRightBtn addTarget:self
                   action:@selector(navRightBtnAction)
         forControlEvents:UIControlEventTouchUpInside];
    navRightBtn.titleLabel.font = font_17;
    [navRightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.navView addSubview:navRightBtn];
    //
    self.navRightLabel = [[UILabel alloc]initWithFrame:CGRectMake(navRightBtn.width-20.0-CellLeft, (44.0-20.0)/2.0, 20.0, 20.0)];
    self.navRightLabel.font = font_icon_(20.0);
    self.navRightLabel.text = @"";
    self.navRightLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
    [navRightBtn addSubview:self.navRightLabel];
    //
    self.edgePGR = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(edgePanGesture:)];
    self.edgePGR.delegate = self;
    self.edgePGR.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:self.edgePGR];
}

#pragma mark- UIGestureRecognizer Delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (void)edgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender {
    [self navLeftBtnAction];
}

#pragma mark - UIButton Delegate
- (void)navLeftBtnAction {
    if ([self.delegate respondsToSelector:@selector(RBNavLeftBtnAction)]) {
        [self.delegate RBNavLeftBtnAction];
    }
}

- (void)navRightBtnAction {
    if ([self.delegate respondsToSelector:@selector(RBNavRightBtnAction)]) {
        [self.delegate RBNavRightBtnAction];
    }
}

#pragma mark - NavTitle Delegate
- (void)setNavTitle:(NSString *)navTitle {
    _navTitle = navTitle;
    if ((navTitle !=  nil) && (navTitle.length !=  0)) {
        [self loadNavigationView];
        self.navTitleLabel.text = navTitle;
    }
}

- (void)setNavLeftTitle:(NSString *)navLeftTitle {
    _navLeftTitle = navLeftTitle;
    if ((navLeftTitle !=  nil) && (navLeftTitle.length !=  0)) {
        [self loadNavigationView];
        self.navLeftLabel.text = navLeftTitle;
        if([navLeftTitle isEqualToString:Icon_bar_more_ark]){
            self.navLeftLabel.text = @"";
            [self.navLeftLabel removeAllSubviews];
            //
            UIImageView*leftIV = [[UIImageView alloc]initWithFrame:self.navLeftLabel.bounds];
            leftIV.image = [UIImage imageNamed:@"icon_ark_more.png"];
            [self.navLeftLabel addSubview:leftIV];
        }
        if([navLeftTitle isEqualToString:Icon_bar_add_ark]){
            self.navLeftLabel.text = @"";
            [self.navLeftLabel removeAllSubviews];
            //
            UIImageView*leftIV = [[UIImageView alloc]initWithFrame:self.navLeftLabel.bounds];
            leftIV.image = [UIImage imageNamed:@"icon_ark_create.png"];
            [self.navLeftLabel addSubview:leftIV];
        }
        if([navLeftTitle isEqualToString:Icon_bar_write_ark]){
            self.navLeftLabel.text = @"";
            [self.navLeftLabel removeAllSubviews];
            //
            UIImageView*leftIV = [[UIImageView alloc]initWithFrame:self.navLeftLabel.bounds];
            leftIV.image = [UIImage imageNamed:@"icon_ark_write.png"];
            [self.navLeftLabel addSubview:leftIV];
        }
        if ([navLeftTitle isEqualToString:NSLocalizedString(@"R7059", @"跳过")]) {
            self.navLeftLabel.text = @"";
            self.navLeftBtn.frame = CGRectMake(CellLeft, StatusHeight, 44.0, 44.0);
            [self.navLeftBtn setTitle:NSLocalizedString(@"R7059", @"跳过") forState:UIControlStateNormal];
        }
        if ([navLeftTitle isEqualToString:@"空"]) {
            self.navLeftLabel.text = @"";
            [self.navLeftLabel removeAllSubviews];
        }
    }
}

- (void)setNavRightTitle:(NSString *)navRightTitle {
    _navRightTitle = navRightTitle;
    if ((navRightTitle !=  nil) && (navRightTitle.length !=  0)) {
        [self loadNavigationView];
        self.navRightLabel.text = navRightTitle;
        if([navRightTitle isEqualToString:Icon_bar_filter_ark]){
            self.navRightLabel.text = @"";
            [self.navRightLabel removeAllSubviews];
            //
            UIImageView*rightIV = [[UIImageView alloc]initWithFrame:self.navRightLabel.bounds];
            rightIV.image = [UIImage imageNamed:@"icon_ark_filter.png"];
            [self.navRightLabel addSubview:rightIV];
        }
        if([navRightTitle isEqualToString:Icon_bar_search_ark]){
            self.navRightLabel.text = @"";
            [self.navRightLabel removeAllSubviews];
            //
            UIImageView*rightIV = [[UIImageView alloc]initWithFrame:self.navRightLabel.bounds];
            rightIV.image = [UIImage imageNamed:@"icon_ark_search.png"];
            [self.navRightLabel addSubview:rightIV];
        }
        if([navRightTitle isEqualToString:Icon_bar_more_ark]) {
            self.navRightLabel.text = @"";
            [self.navRightLabel removeAllSubviews];
            //
            UIImageView*rightIV = [[UIImageView alloc]initWithFrame:self.navRightLabel.bounds];
            rightIV.image = [UIImage imageNamed:@"icon_ark_more.png"];
            [self.navRightLabel addSubview:rightIV];
        }
        if([navRightTitle isEqualToString:Icon_bar_cps_ark]) {
            self.navRightLabel.text = @"";
            [self.navRightLabel removeAllSubviews];
            //
            UIImageView*rightIV = [[UIImageView alloc]initWithFrame:self.navRightLabel.bounds];
            rightIV.image = [UIImage imageNamed:@"icon_ark_cps.png"];
            [self.navRightLabel addSubview:rightIV];
        }
        if ([navRightTitle isEqualToString:@"share"]) {
            self.navRightLabel.text = @"";
            [self.navRightLabel removeAllSubviews];
            //
            UIImageView*rightIV = [[UIImageView alloc]initWithFrame:self.navRightLabel.bounds];
            rightIV.image = [UIImage imageNamed:@"RBshareImage"];
            [self.navRightLabel addSubview:rightIV];
        }
        if([navRightTitle isEqualToString:NSLocalizedString(@"R7057", @"编辑")]){
            self.navRightLabel.text = @"";
            UIButton * button = (UIButton*)[self.navView viewWithTag:10000];
            button.frame = CGRectMake(self.navView.width-44.0-CellLeft, StatusHeight, 44.0, 44.0);
            [button setTitle:NSLocalizedString(@"R7057", @"编辑") forState:UIControlStateNormal];
        }
        if ([navRightTitle isEqualToString:NSLocalizedString(@"R7058", @"取消")]) {
            self.navRightLabel.text = @"";
            UIButton * button = (UIButton*)[self.navView viewWithTag:10000];
            button.frame = CGRectMake(self.navView.width-44.0-CellLeft, StatusHeight, 44.0, 44.0);
            [button setTitle:NSLocalizedString(@"R7058", @"取消") forState:UIControlStateNormal];
        }
        if ([navRightTitle isEqualToString:@"空"]) {
            self.navRightLabel.text = @"";
            [self.navRightLabel removeAllSubviews];
        }
    }
}

#pragma mark - UIGestureRecognizer Delegate
- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    // 检测到摇动
}

- (void)motionCancelled:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    // 摇动取消
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if([LocalService getRBLocalDataUserShake]==nil||[LocalService getRBLocalDataUserShake].length==0){
        // 摇动结束
        if ([LocalService getRBLocalDataUserPrivateToken]!=nil) {
            if (event.subtype == UIEventSubtypeMotionShake) {
                if ([self.delegate respondsToSelector:@selector(RBNotificationShowFeedbackView)]) {
                    [self.delegate RBNotificationShowFeedbackView];
                } else {
                    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);
                    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
                    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    //
                    RBUserSettingFeedBackViewController *toview = [[RBUserSettingFeedBackViewController alloc] init];
                    toview.feedbackImage = image;
                    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:toview] animated:YES completion:NULL];
                }
            }
        }
    }
}

#pragma mark - NSNotification Delegate
- (void)RBNotificationResignActive:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [self.hudView dismiss];
//    // 防止离线数据大于100条
//    while ([[ArticleEntityManager selectCoreDataArticleEntity_All] count] > 100) {
//        RBArticleEntity*articleEntity = [[ArticleEntityManager selectCoreDataArticleEntity_All] lastObject];
//        [ArticleEntityManager deleteCoreDataArticleEntity_By:articleEntity.article_id];
//    }
    if ([self.delegate respondsToSelector:@selector(RBNotificationResignActive)]) {
        [self.delegate RBNotificationResignActive];
    }
}

- (void)RBNotificationBecomeActive:(NSNotification *)notification {
    if ([self.delegate respondsToSelector:@selector(RBNotificationBecomeActive)]) {
        [self.delegate RBNotificationBecomeActive];
    }
}

- (void)RBNotificationRefreshArticleView:(NSNotification *)notification {
    if ([self.delegate respondsToSelector:@selector(RBNotificationRefreshArticleView)]) {
        [self.delegate RBNotificationRefreshArticleView];
    }
}

- (void)RBNotificationRefreshUserInfo:(NSNotification *)notification {
    if ([self.delegate respondsToSelector:@selector(RBNotificationRefreshUserInfo)]) {
        [self.delegate RBNotificationRefreshUserInfo];
    }
}

- (void)RBNotificationRefreshHomeView:(NSNotification *)notification {
    if ([self.delegate respondsToSelector:@selector(RBNotificationRefreshHomeView)]) {
        [self.delegate RBNotificationRefreshHomeView];
    }
}

- (void)RBNotificationRefreshIndianaView:(NSNotification *)notification {
    if ([self.delegate respondsToSelector:@selector(RBNotificationRefreshIndianaView)]) {
        [self.delegate RBNotificationRefreshIndianaView];
    }
}
-(void)RBNotificationRefreshApply:(NSNotification*)notification{
    if ([self.delegate respondsToSelector:@selector(RBNotificationRefreshApply)]) {
        [self.delegate RBNotificationRefreshApply];
    }
}
- (void)RBNotificationShowMessageView:(NSNotification *)notification {
    if ([self.delegate respondsToSelector:@selector(RBNotificationShowMessageView)]) {
        [self.delegate RBNotificationShowMessageView];
    }
}

- (void)RBNotificationStatusBarAction:(NSNotification *)notification {
    if ([self.delegate respondsToSelector:@selector(RBNotificationStatusBarAction)]) {
        [self.delegate RBNotificationStatusBarAction];
    }
}

- (void)RBNotificationDismissView:(NSNotification *)notification {
    if ([self.delegate respondsToSelector:@selector(RBNotificationDismissView)]) {
        [self.delegate RBNotificationDismissView];
    }
}
- (void)RBNotificationBackToHome:(NSNotification *)notification{
    if ([self.delegate respondsToSelector:@selector(RBNotificationBackToHome)]) {
        [self.delegate RBNotificationBackToHome];
    }
}
- (void)RBNotificationCampaignAddCover:(NSNotification *)notification{
    if ([self.delegate respondsToSelector:@selector(RBNotificationCampaignAddCover)]) {
        [self.delegate RBNotificationCampaignAddCover];
    }
}
#pragma mark - RBAlertView Delegate
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex {
    if (alertView.tag == 9999) {
        if (buttonIndex==0) {
            RBUserMessageViewController *toview = [[RBUserMessageViewController alloc] init];
            toview.status = @"unread";
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:toview] animated:YES completion:NULL];
        }
    }
}

- (void)RBNotificationShowLoginView:(NSNotification *)notification {
    if ([self.delegate respondsToSelector:@selector(RBNotificationShowLoginView)]) {
        [self.delegate RBNotificationShowLoginView];
    } else {
        RBAppDelegate *appDelegate = (RBAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate loadViewController:0];
    }
}

- (BOOL)isCurrentVCVisible:(UIViewController *)viewController {
    return (viewController.isViewLoaded && viewController.view.window);
}

#pragma mark - RBHUDView Method
-(RBHUDView *)hudView {
    if (!_hudView) {
        _hudView = [[RBHUDView alloc] initWithFrame:CGRectZero];
        _hudView.senderView = self.view;
    }
    return _hudView;
}

- (UIView*)defaultView {
    if (!_defaultView) {
        _defaultView = [[UIView alloc]initWithFrame:CGRectMake(0, (self.view.height-120.0)/2.0, ScreenWidth, 120.0)];
        _defaultView.tag = 9999;
        _defaultView.hidden = YES;
        [self.view addSubview:_defaultView];
        //
        UIImageView*defaultIV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-100.0)/2.0, 0, 100.0, 100.0)];
        defaultIV.image = [UIImage imageNamed:@"icon_task_default.png"];
        [_defaultView addSubview:defaultIV];
        //
        UILabel*defaultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, defaultIV.bottom, ScreenWidth, 20.0)];
        defaultLabel.font = font_17;
        defaultLabel.textAlignment = NSTextAlignmentCenter;
        defaultLabel.text = NSLocalizedString(@"R1028", @"当前页面无数据,试试别的页面吧");
        defaultLabel.textColor = [Utils getUIColorWithHexString:SysColorSubGray];
        [_defaultView addSubview:defaultLabel];
    }
    return _defaultView;
}

#pragma mark - VisitorLogin Method
- (BOOL)isVisitorLogin {
    if ([JsonService isRBUserVisitor] && [LocalService getRBLocalEmail].length <= 0) {
        RBLoginViewController*toview = [[RBLoginViewController alloc]init];
//        toview.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:toview animated:YES];
        toview.isPush = YES;
        [self presentVC:toview];
        return YES;
    }
    return NO;
}

#pragma mark - Dismiss Method
- (void)dismissVC {
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationDismissView
                                                            object:nil];
    }];
    
}

#pragma mark - Present Method
- (void)presentVC:(RBBaseViewController *)toVC {
    self.transitionAnimator = [RBTransitionAnimator new];
    UINavigationController*toNC = [[UINavigationController alloc] initWithRootViewController:toVC];
//    toNC.transitioningDelegate = self;
//    toNC.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:toNC animated:YES completion:NULL];
}

#pragma mark - Transitioning Delegate (Modal)
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    self.transitionAnimator.animationType = AnimationTypePresent;
    return self.transitionAnimator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.transitionAnimator.animationType = AnimationTypeDismiss;
    return self.transitionAnimator;
}

@end
