//
//  RBSwitch.h
//  RB
//
//  Created by AngusNi on 3/23/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBSwitch : UISwitch
@property(nonatomic, strong) NSString *typeStr;

@end
