//
//  PKLineView.h
//  RB
//
//  Created by RB8 on 2017/8/1.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JGProgressView.h"
#import "Service.h"
@interface PKLineView : UIView
@property(nonatomic,strong)JGProgressView * leftProgressView;
@property(nonatomic,strong)JGProgressView * rightProgressView;
@property(nonatomic,strong)UILabel * MiddleLabel;
@property(nonatomic,strong)UILabel * leftScoreLabel;
@property(nonatomic,strong)UILabel * rightScoreLabel;
-(void)getdata:(NSString*)middleText;
@end
