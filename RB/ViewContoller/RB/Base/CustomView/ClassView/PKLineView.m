//
//  PKLineView.m
//  RB
//
//  Created by RB8 on 2017/8/1.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "PKLineView.h"

@implementation PKLineView
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        _leftScoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 0,100, 20)];
        _leftScoreLabel.textColor = [Utils getUIColorWithHexString:@"181818"];
        _leftScoreLabel.text = @"13452";
        _leftScoreLabel.font = font_cu_17;
        _leftScoreLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_leftScoreLabel];
        //
        _MiddleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        _MiddleLabel.textColor = [Utils getUIColorWithHexString:@"181818"];
        _MiddleLabel.font = font_cu_15;
        _MiddleLabel.textAlignment = NSTextAlignmentCenter;
//        [_MiddleLabel sizeToFit];
//        _MiddleLabel.frame = CGRectMake((ScreenWidth-_MiddleLabel.frame.size.width)/2, 0, _MiddleLabel.frame.size.width, _MiddleLabel.frame.size.height);
        [self addSubview:_MiddleLabel];
        //
        _rightScoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 115, _leftScoreLabel.top, 100, 20)];
        _rightScoreLabel.textColor = [Utils getUIColorWithHexString:@"181818"];
        _rightScoreLabel.text = @"13452";
        _rightScoreLabel.font = font_cu_17;
        _rightScoreLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:_rightScoreLabel];
        //
        _leftProgressView = [[JGProgressView alloc]initWithFrame:CGRectMake(15, _leftScoreLabel.bottom+10, (ScreenWidth - 30-6)/2, 10)];
        _leftProgressView.direction = @"right";
        _leftProgressView.Ccolor = [Utils getUIColorWithHexString:@"e4e4e4"];
        _leftProgressView.Tcolor = [Utils getUIColorWithHexString:@"ffc701"];
        _leftProgressView.circle = @"YES";
       // _leftProgressView.progress = 0.5;
        [self addSubview:_leftProgressView];
        //
        _rightProgressView = [[JGProgressView alloc]initWithFrame:CGRectMake(_leftProgressView.right+6, _leftProgressView.top, _leftProgressView.width, 10)];
        _rightProgressView.direction = @"left";
//        _rightProgressView.Ccolor = [Utils getUIColorWithHexString:@"e4e4e4"];
//        _rightProgressView.Tcolor = [Utils getUIColorWithHexString:@"b4b4b4"];
        _rightProgressView.Ccolor = [Utils getUIColorWithHexString:@"e4e4e4"];
        _rightProgressView.Tcolor = [Utils getUIColorWithHexString:@"ffc701"];
        _rightProgressView.circle = @"YES";
       // _rightProgressView.progress = 0.8;
        [self addSubview:_rightProgressView];
        //
        UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 16, 16)];
        view.center = CGPointMake(self.center.x, _leftProgressView.center.y);
        view.layer.cornerRadius = 8.0;
        view.layer.borderColor = [Utils getUIColorWithHexString:@"cacaca"].CGColor;
        view.layer.borderWidth = 1.0;
        view.backgroundColor = [Utils getUIColorWithHexString:@"ffffff"];
        [self addSubview:view];
    }
    return self;
}
-(void)getdata:(NSString*)middleText{
    _MiddleLabel.text = middleText;
    [_MiddleLabel sizeToFit];
    _MiddleLabel.frame = CGRectMake((ScreenWidth-_MiddleLabel.frame.size.width)/2, 0, _MiddleLabel.frame.size.width, _MiddleLabel.frame.size.height);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
