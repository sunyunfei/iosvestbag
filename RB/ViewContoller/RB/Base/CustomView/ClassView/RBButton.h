//
//  RBButton.h
//  RB
//
//  Created by AngusNi on 16/1/30.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBButton : UIButton
@property(nonatomic, strong) UILabel *btnLabel;
@property(nonatomic, strong) NSString *typeStr;
@property(nonatomic, strong) NSString *contentStr;
@property(nonatomic, strong) UIView *btnView;
@property(nonatomic, strong) UITextView *btnTV;

@end
