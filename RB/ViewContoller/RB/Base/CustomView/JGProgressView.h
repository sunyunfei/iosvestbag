//
//  JGProgressView.h
//  JGProgressView
//
//  Created by 郭军 on 2017/3/16.
//  Copyright © 2017年 ZJNY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JGProgressView : UIView

@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, copy) NSString * direction;
@property (nonatomic, strong) UIColor * Tcolor;
@property(nonatomic,strong)UIColor * Ccolor;
@property(nonatomic,strong)UIColor * borderColor;
//圆角还是平角
@property(nonatomic,copy)NSString * circle;

-(void)setCViewColor:(UIColor*)cColor AndTViewColor:(UIColor*)tColor AndBorderColor:(UIColor*)borderColor;
@end
