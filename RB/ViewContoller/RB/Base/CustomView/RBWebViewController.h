//
//  TOWebViewController.h
//  RB
//
//  Created by AngusNi on 15/7/24.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBBaseViewController.h"

@interface RBWebViewController
    : RBBaseViewController <RBBaseVCDelegate, UIWebViewDelegate>
@property(nonatomic, strong) UIWebView *webView;
@property(nonatomic, strong) NSURL *url;
@property(nonatomic, strong) NSString *titleStr;
@property(nonatomic, strong) NSString *shareTitle;
@property(nonatomic, strong) NSString *shareImg;
@property(nonatomic, strong) NSString *shareUrl;

@end
