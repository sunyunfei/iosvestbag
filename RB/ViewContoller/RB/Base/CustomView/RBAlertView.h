//
//  RBAlertView.h
//  RB
//
//  Created by AngusNi on 16/3/10.
//  Copyright © 2016年 AngusNi. All rights reserved.
//
#import <UIKit/UIKit.h>

@class RBAlertView;
@protocol RBAlertViewDelegate <NSObject>
@optional
- (void)RBAlertView:(RBAlertView *)alertView clickedButtonAtIndex:(int)buttonIndex;
@end

@interface RBAlertView : UIView
@property (nonatomic, weak)  id<RBAlertViewDelegate> delegate;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) UIView *bgView;

- (void)showWithArray:(NSArray *)array;
@end
