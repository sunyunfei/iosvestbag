//
//  RBPictureView.h
//  RB
//
//  Created by AngusNi on 16/3/10.
//  Copyright © 2016年 AngusNi. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Service.h"

@class RBPictureView;
@protocol RBPictureViewDelegate <NSObject>
@optional
- (void)RBPictureView:(RBPictureView *)actionSheet clickedButtonAtIndex:(int)buttonIndex;
@end

@interface RBPictureView : UIView

@property (nonatomic, weak)  id<RBPictureViewDelegate> delegate;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UIImageView*bgIV;
- (void)showWithPic:(NSString*)str;
@end
