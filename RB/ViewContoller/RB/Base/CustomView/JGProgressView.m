//
//  JGProgressView.m
//  JGProgressView
//
//  Created by 郭军 on 2017/3/16.
//  Copyright © 2017年 ZJNY. All rights reserved.
//

#import "JGProgressView.h"
#import "Utils.h"
#define KProgressBorderWidth 2.0f
#define KProgressPadding 1.0f
#define KProgressColor [UIColor colorWithRed:0/255.0 green:191/255.0 blue:255/255.0 alpha:1]

@interface JGProgressView ()

@property (nonatomic, weak) UIView *tView;
@property(nonatomic,strong)UIView * cView;

@end

@implementation JGProgressView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        //边框
        UIView *borderView = [[UIView alloc] initWithFrame:self.bounds];
        borderView.layer.cornerRadius = self.bounds.size.height * 0.5;
        borderView.layer.masksToBounds = YES;
       // borderView.backgroundColor = [UIColor whiteColor];
//        borderView.layer.borderColor = [Utils getUIColorWithHexString:@"ffc701"].CGColor;
//        borderView.layer.borderWidth = KProgressBorderWidth;
        [self addSubview:borderView];
        self.cView = borderView;
        //进度
        UIView *tView = [[UIView alloc] init];
        tView.backgroundColor = [Utils getUIColorWithHexString:@"ff3030"]
        ;
        tView.layer.cornerRadius = (self.bounds.size.height - (KProgressBorderWidth) * 2) * 0.5;
        tView.layer.masksToBounds = YES;
        tView.frame = CGRectMake(0, 0, 0,self.bounds.size.height);
        [borderView addSubview:tView];
        self.tView = tView;
    }
    
    return self;
}
-(void)setTcolor:(UIColor *)Tcolor{
    _Tcolor = Tcolor;
    _tView.backgroundColor = _Tcolor;
}
-(void)setCcolor:(UIColor *)Ccolor{
    _Ccolor = Ccolor;
    _cView.backgroundColor = _Ccolor;
}
-(void)setBorderColor:(UIColor *)borderColor{
    _borderColor = borderColor;
    _cView.layer.borderColor = _borderColor.CGColor;
}
- (void)setProgress:(CGFloat)progress
{
    _progress = progress;
    CGFloat margin = KProgressBorderWidth;
    CGFloat maxWidth = self.bounds.size.width - margin * 2;
    CGFloat heigth;
    if (_borderColor!=nil) {
        _cView.layer.borderWidth = KProgressBorderWidth;
        _cView.layer.borderColor = _borderColor.CGColor;
        maxWidth = self.bounds.size.width - margin*2;
        heigth = self.bounds.size.height - margin * 2;
    }else{
        maxWidth = self.bounds.size.width;
        heigth = self.bounds.size.height;
    }
    if ([_circle isEqualToString:@"circle"]) {
        _tView.layer.cornerRadius = (self.bounds.size.height - (KProgressBorderWidth) * 2) * 0.5;
    }else{
        _tView.layer.cornerRadius = 0;
    }
    if ([self.direction isEqualToString:@"right"]) {
        if (_borderColor != nil) {
            _tView.frame = CGRectMake(self.bounds.size.width-KProgressBorderWidth, margin,0, self.bounds.size.height);
            [UIView animateWithDuration:0.8 animations:^{
                _tView.frame = CGRectMake(maxWidth*(1.0-progress)+KProgressBorderWidth, margin, maxWidth * progress, heigth);
            }];
        }else{
            _tView.frame = CGRectMake(self.bounds.size.width, 0, 0,_cView.frame.size.height);
            [UIView animateWithDuration:0.5 animations:^{
                _tView.frame = CGRectMake(maxWidth*(1.0-progress), 0, maxWidth * progress, heigth);
            }];
        }
    }else{
        if (_borderColor != nil) {
            [UIView animateWithDuration:0.8 animations:^{
                _tView.frame = CGRectMake(margin, margin, maxWidth * progress, heigth);
            }];
        }else{
            [UIView animateWithDuration:0.8 animations:^{
                _tView.frame = CGRectMake(0, 0, maxWidth * progress, heigth);
            }];
        }
   
    }
}
-(void)setCViewColor:(UIColor*)cColor AndTViewColor:(UIColor*)tColor AndBorderColor:(UIColor*)borderColor{
    _cView.backgroundColor = cColor;
    _tView.backgroundColor = tColor;
    if (borderColor != nil) {
        _cView.layer.borderColor = borderColor.CGColor;
    }
}

@end
