//
//  RBShareView.m
//  RB
//
//  Created by AngusNi on 15/6/2.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBShareView.h"
#import "Service.h"

@interface RBShareView () {
    NSString *titleString;
    NSString *contentString;
    NSString *urlString;
    NSString *imgUrlString;
}
@end

@implementation RBShareView

DEFINE_SINGLETON_FOR_CLASS(RBShareView);

- (instancetype)init {
    if (self = [super init]) {
        self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
        id<UIApplicationDelegate> delegate  =
        [[UIApplication sharedApplication] delegate];
        if ([delegate respondsToSelector:@selector(window)]) {
            self.window = [delegate performSelector:@selector(window)];
        } else {
            self.window = [[UIApplication sharedApplication] keyWindow];
        }
        //
        self.frame = [[UIScreen mainScreen] bounds];
        self.backgroundColor = SysColorCoverDeep;
        //
        UIButton *bgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        bgBtn.frame = self.bounds;
        bgBtn.backgroundColor = [UIColor clearColor];
        [bgBtn addTarget:self
                  action:@selector(cancelBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:bgBtn];
        //
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight-140.0, ScreenWidth, 140.0)];
        bottomView.backgroundColor  = [UIColor whiteColor];
        [self addSubview:bottomView];
        //
        UILabel *bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 16, ScreenWidth, 20.0)];
        bottomLabel.backgroundColor = [UIColor clearColor];
        bottomLabel.font = font_13;
        bottomLabel.textColor = [Utils getUIColorWithHexString:SysColorBlack];
        bottomLabel.textAlignment = NSTextAlignmentCenter;
        bottomLabel.text = NSLocalizedString(@"R1022", @"分享");
        [bottomView addSubview:bottomLabel];
        //
        NSArray *array = @[NSLocalizedString(@"R1023", @"微信"), NSLocalizedString(@"R1024", @"朋友圈"), NSLocalizedString(@"R1025", @"微博"), NSLocalizedString(@"R1026", @"QQ"), NSLocalizedString(@"R1027", @"QQ空间")];
        NSArray *imgArray = @[
                              @"icon_wechat.png",
                              @"icon_wechat_friends.png",
                              @"icon_weibo.png",
                              @"icon_qq.png",
                              @"icon_qzone.png"
                              ];
        for (int i = 0; i < [array count]; i++) {
            @autoreleasepool {
                UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                shareBtn.frame = CGRectZero;
                shareBtn.backgroundColor = [UIColor clearColor];
                shareBtn.tag = 1000 + i;
                [shareBtn addTarget:self
                             action:@selector(shareBtnAction:)
                   forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:shareBtn];
                //
                UIImageView *shareIV = [[UIImageView alloc]initWithFrame:CGRectZero];
                shareIV.image = [UIImage imageNamed:imgArray[i]];
                [self addSubview:shareIV];
                //
                UILabel *shareLabel = [[UILabel alloc] initWithFrame:CGRectZero];
                shareLabel.backgroundColor = [UIColor clearColor];
                shareLabel.font = font_13;
                shareLabel.textColor = [Utils getUIColorWithHexString:SysColorGray];
                shareLabel.textAlignment = NSTextAlignmentCenter;
                shareLabel.text = [array objectAtIndex:i];
                [self addSubview:shareLabel];
                //
                shareIV.frame = CGRectMake(((self.width -40.0 * 5.0) /
                                            6.0) +
                                           i * (40.0 +
                                                (self.width -40.0 * 5.0) /
                                                6.0),
                                           (self.height -
                                            140.0+60.0),
                                           40, 40);
                shareLabel.frame  = CGRectMake(shareIV.left-25.0,
                                               shareIV.bottom+8.0,
                                               shareIV.width+50, 20);
                shareBtn.frame  =
                CGRectMake(shareIV.left, shareIV.top,
                           shareIV.width, shareIV.height + 20);
            }
        }
    }
    return self;
}

- (void)showViewWithTitle:(NSString *)titleStr
                  Content:(NSString *)contentStr
                      URL:(NSString *)urlStr
                   ImgURL:(NSString *)imgUrlStr {
    titleString = titleStr;
    contentString = contentStr;
    urlString = urlStr;
    imgUrlString = imgUrlStr;
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
    }
}

#pragma mark - UIButton Delegate
- (void)cancelBtnAction:(UIButton *)sender {
    [self removeFromSuperview];
}

- (void)shareBtnAction:(UIButton *)sender {
    [self cancelBtnAction:nil];
    if (urlString.length == 0) {
        return;
    }
    SSDKPlatformType type;
    if (sender.tag == 1000) {
        type = SSDKPlatformSubTypeWechatSession;
    } else if (sender.tag == 1001) {
        type = SSDKPlatformSubTypeWechatTimeline;
    } else if (sender.tag == 1002) {
        type = SSDKPlatformTypeSinaWeibo;
    } else if (sender.tag == 1003) {
        type = SSDKPlatformSubTypeQQFriend;
    } else {
        type = SSDKPlatformSubTypeQZone;
    }
    [self shareSDKWithTitle:titleString Content:contentString URL:urlString ImgURL:imgUrlString SSDKPlatformType:type];
}

#pragma mark - ShareSDK Delegate
- (void)shareSDKWithTitle:(NSString *)titleStr
                  Content:(NSString *)contentStr
                      URL:(NSString *)urlStr
                   ImgURL:(NSString *)imgUrlStr
         SSDKPlatformType:(SSDKPlatformType)type {
    [SVProgressHUD show];
    NSLog(@"title:%@,content:%@,img:%@,url:%@",titleStr,contentStr,imgUrlStr,urlStr);
    if (type==SSDKPlatformTypeSinaWeibo) {
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:[NSString stringWithFormat:@"%@ %@",titleStr,urlStr]
                                         images:imgUrlStr
                                            url:[NSURL URLWithString:urlStr]
                                          title:[NSString stringWithFormat:@"%@ %@",titleStr,urlStr]
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            if (state == SSDKResponseStateSuccess) {
//                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
                [SVProgressHUD dismiss];
                if ([self.delegate respondsToSelector:@selector(shareSuccess)]) {
                    [self.delegate shareSuccess];
                }
            }
            if (state == SSDKResponseStateFail) {
                NSLog(@"%@",error.description);
                if (error.code == 204) {
                    [SVProgressHUD showErrorWithStatus:@"相同的内容不能重复分享"];
                } else {
                    if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                        [SVProgressHUD showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                    } else {
                        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
                    }
                    [SVProgressHUD dismiss];
                }
            }
            if (state == SSDKResponseStateCancel) {
                [SVProgressHUD dismiss];
                //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
            }
        }];
    } else {
        [[SDWebImageDownloader sharedDownloader]downloadImageWithURL:[NSURL URLWithString:imgUrlStr] options:SDWebImageDownloaderHighPriority progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            if(finished==NO) {
                NSLog(@"图片下载失败:%@,%@",image,data);
            } else {
                image = [Utils narrowWithImage:image];
            }
            NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
            [shareParams SSDKSetupShareParamsByText:contentStr
                                             images:image
                                                url:[NSURL URLWithString:urlStr]
                                              title:titleStr
                                               type:SSDKContentTypeWebPage];
            [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
                if (state == SSDKResponseStateSuccess) {
//                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"R1041", @"分享成功")];
                    if ([self.delegate respondsToSelector:@selector(shareSuccess)]) {
                        [self.delegate shareSuccess];
                    }
                }
                if (state == SSDKResponseStateFail) {
                    if([NSString stringWithFormat:@"%@",[error.userInfo objectForKey:@"error_message"]].length!=0) {
                        [SVProgressHUD showErrorWithStatus:[error.userInfo objectForKey:@"error_message"]];
                    } else {
                        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"R1039", @"分享失败")];
                    }
                }
                if (state == SSDKResponseStateCancel) {
                    [SVProgressHUD dismiss];
                    //            [self.hudView showErrorWithStatus:NSLocalizedString(@"R1019", @"操作取消")];
                }
            }];
        }];
    }
}

@end
