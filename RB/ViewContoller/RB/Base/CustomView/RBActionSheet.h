//
//  RBActionSheet.h
//  RB
//
//  Created by AngusNi on 15/6/2.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
@class RBActionSheet;
@protocol RBActionSheetDelegate <NSObject>
@optional
- (void)RBActionSheet:(RBActionSheet *)actionSheet clickedButtonAtIndex:(int)buttonIndex;
@end

@interface RBActionSheet : UIView
@property (nonatomic, weak)  id<RBActionSheetDelegate> delegate;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) UIView *bgView;

- (void)showWithArray:(NSArray *)array;
@end
