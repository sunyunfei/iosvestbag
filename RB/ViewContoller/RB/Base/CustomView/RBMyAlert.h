//
//  RBMyAlert.h
//  RB
//
//  Created by RB8 on 2017/8/15.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RBMyAlert;
@protocol RBMyAlertDelegate <NSObject>
- (void)RBMyAlert:(RBMyAlert *)alertView clickedButtonAtIndex:(int)buttonIndex;

@end
@interface RBMyAlert : UIView
@property(nonatomic,strong)UIWindow * window;
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView * backView;
@property (nonatomic,weak) id<RBMyAlertDelegate>delegate;
-(void)showWithArray:(NSArray*)array;

@end
