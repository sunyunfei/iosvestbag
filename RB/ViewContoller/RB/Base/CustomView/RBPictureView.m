//
//  RBPictureView.m
//  RB
//
//  Created by AngusNi on 16/3/10.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBPictureView.h"

@interface RBPictureView () {
}
@end

@implementation RBPictureView

- (instancetype)init {
    if (self = [super init]) {
        self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
        id<UIApplicationDelegate> delegate = [[UIApplication sharedApplication] delegate];
        if ([delegate respondsToSelector:@selector(window)]) {
            self.window = [delegate performSelector:@selector(window)];
        } else {
            self.window = [[UIApplication sharedApplication] keyWindow];
        }
        //
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.backgroundColor = SysColorCoverDeep;
        self.bgIV = [[UIImageView alloc]initWithFrame:self.frame];
        self.bgIV.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.bgIV];
//        //
//        UILabel *strLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, (ScreenHeight-44.0)/2.0, ScreenWidth, 44.0)];
//        strLabel.text = [NSString stringWithFormat:@"%@ (%@)",NSLocalizedString(@"R2052", @"截图参考"),NSLocalizedString(@"R8042", @"点击关闭")];
//        strLabel.font = font_cu_17;
//        strLabel.textColor = [Utils getUIColorWithHexString:SysColorWhite];
//        strLabel.textAlignment = NSTextAlignmentCenter;
//        strLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorRed];
//        [self.bgIV addSubview:strLabel];
        //
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}


- (void)showWithPic:(NSString*)str {
    if (str!=nil&&str.length!=0) {
        [self.bgIV sd_setImageWithURL:[NSURL URLWithString:str]];
    } else {
        self.bgIV.image = [UIImage imageNamed:@"pic_task_screenshot.png"];
    }
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = self.bgIV.transform;
        self.bgIV.transform  =
        CGAffineTransformScale(self.bgIV.transform, 0.5, 0.5);
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.bgIV.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)tapGesture {
    [self removeFromSuperview];
    if ([self.delegate respondsToSelector:@selector(RBPictureView:clickedButtonAtIndex:)]) {
        [self.delegate RBPictureView:self clickedButtonAtIndex:0];
    }
}

@end
