//
//  RBShareView.h
//  RB
//
//  Created by AngusNi on 15/6/2.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constants.h"
@protocol RBShareViewDelegate <NSObject>
- (void)shareSuccess;
@end
@interface RBShareView : UIView
DEFINE_SINGLETON_FOR_HEADER(RBShareView);

- (void)showViewWithTitle:(NSString *)titleStr
                  Content:(NSString *)contentStr
                      URL:(NSString *)urlStr
                   ImgURL:(NSString *)imgUrlStr;
@property(nonatomic, strong) UIWindow*window;
@property(nonatomic, weak)id<RBShareViewDelegate>delegate;
@end
