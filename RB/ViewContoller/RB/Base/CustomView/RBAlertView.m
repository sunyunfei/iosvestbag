//
//  RBAlertView.m
//  RB
//
//  Created by AngusNi on 16/3/10.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import "RBAlertView.h"
#import "Service.h"

@interface RBAlertView () {
}
@end

@implementation RBAlertView

- (instancetype)init {
    if (self = [super init]) {
        self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
        id<UIApplicationDelegate> delegate  =
        [[UIApplication sharedApplication] delegate];
        if ([delegate respondsToSelector:@selector(window)]) {
            self.window = [delegate performSelector:@selector(window)];
        } else {
            self.window = [[UIApplication sharedApplication] keyWindow];
        }
        //
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.backgroundColor = SysColorCoverDeep;
        //
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}


- (void)showWithArray:(NSArray *)array {
    [self removeAllSubviews];
    self.array = array;
    //
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(50.0,0, ScreenWidth-50.0*2.0,0)];
    self.bgView.layer.cornerRadius = 5.0;
    self.bgView.layer.masksToBounds = YES;
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bgView];
    //
    NSString*titleStr = array[0];
    UILabel*titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 0, self.bgView.width-30.0, 0)];
    titleLabel.font = font_15;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:titleLabel];
    titleLabel.text = titleStr;
    [Utils getUILabel:titleLabel withLineSpacing:10.0];
    CGSize titleLabelSize = [Utils getUIFontSizeFitH:titleLabel];
    titleLabel.height = titleLabelSize.height+70.0;
    self.bgView.height = titleLabel.bottom + 50.0;
    self.bgView.top = (ScreenHeight-(titleLabel.bottom + 50.0))/2.0;
    //
    if ([array count]==2) {
        UIButton*tempBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [tempBtn setBackgroundColor:[UIColor whiteColor]];
        tempBtn.frame = CGRectMake( 0, titleLabel.bottom, self.bgView.width, 50);
        tempBtn.tag = 1000;
        [tempBtn addTarget:self
                    action:@selector(tempBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];
        [tempBtn setTitle:array[1] forState:UIControlStateNormal];
        [tempBtn setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
        tempBtn.titleLabel.font = font_17;
        [self.bgView addSubview:tempBtn];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, titleLabel.bottom, self.bgView.width, 0.5)];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.bgView addSubview:lineLabel];
    }
    //
    if ([array count]==3) {
        UIButton*tempBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [tempBtn setBackgroundColor:[UIColor whiteColor]];
        tempBtn.frame = CGRectMake( 0, titleLabel.bottom, self.bgView.width/2.0, 50);
        tempBtn.tag = 1000;
        [tempBtn addTarget:self
                    action:@selector(tempBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];
        [tempBtn setTitle:array[1] forState:UIControlStateNormal];
        [tempBtn setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
        tempBtn.titleLabel.font = font_17;
        [self.bgView addSubview:tempBtn];
        //
        UIButton*tempBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [tempBtn1 setBackgroundColor:[UIColor whiteColor]];
        tempBtn1.frame = CGRectMake( self.bgView.width/2.0, titleLabel.bottom, self.bgView.width/2.0, 50);
        tempBtn1.tag = 1001;
        [tempBtn1 addTarget:self
                    action:@selector(tempBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];
        [tempBtn1 setTitle:array[2] forState:UIControlStateNormal];
        [tempBtn1 setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
        tempBtn1.titleLabel.font = font_17;
        [self.bgView addSubview:tempBtn1];
        //
        UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, titleLabel.bottom, self.bgView.width, 0.5)];
        lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.bgView addSubview:lineLabel];
        //
        UILabel*lineLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(lineLabel.width/2.0, lineLabel.bottom, 0.5, 50.0)];
        lineLabel1.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
        [self.bgView addSubview:lineLabel1];
    }
    //
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = self.bgView.transform;
        self.bgView.transform  =
        CGAffineTransformScale(self.bgView.transform, 0.5, 0.5);
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.bgView.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)tapGesture {
    [self removeFromSuperview];
}

#pragma mark - UIButton Delegate
- (void)tempBtnAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(RBAlertView:clickedButtonAtIndex:)]) {
        [self.delegate RBAlertView:self clickedButtonAtIndex:(int)sender.tag-1000];
    }
    [self removeFromSuperview];
}

@end
