//
//  TOWebViewController.m
//  RB
//
//  Created by AngusNi on 15/7/24.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBWebViewController.h"

@interface RBWebViewController () {
}
@end

@implementation RBWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    self.delegate = self;
    if (self.titleStr==nil||self.titleStr.length==0) {
        self.titleStr = @"ROBIN8";
    }
    self.navTitle = self.titleStr;
    if(self.shareTitle!=nil) {
        self.navRightTitle = Icon_btn_share;
    }
    //
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64.0, self.view.width, self.view.height-64.0)];
    self.webView.delegate = self;
    self.webView.scalesPageToFit = YES;
    self.webView.contentMode = UIViewContentModeRedraw;
    self.webView.opaque = YES;
    [self.view addSubview:self.webView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //
    if (self.url) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    }
}

#pragma mark - UIButton Delegate
- (void)RBNavLeftBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)RBNavRightBtnAction {
    RBShareView *shareView = [RBShareView sharedRBShareView];
    [shareView showViewWithTitle:self.shareTitle Content:self.shareTitle URL:self.shareUrl ImgURL:self.shareImg];
}

#pragma mark - UIWebView Delegate
- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.hudView show];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.hudView dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.hudView dismiss];
}

@end
