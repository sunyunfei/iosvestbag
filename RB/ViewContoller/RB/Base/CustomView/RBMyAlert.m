//
//  RBMyAlert.m
//  RB
//
//  Created by RB8 on 2017/8/15.
//  Copyright © 2017年 AngusNi. All rights reserved.
//

#import "RBMyAlert.h"
#import "Service.h"
@implementation RBMyAlert
-(instancetype)init{
    if (self = [super init]) {
        self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
        id<UIApplicationDelegate> delegate  =
        [[UIApplication sharedApplication] delegate];
        if ([delegate respondsToSelector:@selector(window)]) {
            self.window = [delegate performSelector:@selector(window)];
        } else {
            self.window = [[UIApplication sharedApplication] keyWindow];
        }
        //
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.backgroundColor = SysColorCoverDeep;
        //
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}
-(void)showWithArray:(NSArray*)array{
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(30*ScaleWidth, 207*ScaleHeight, ScreenWidth - 2*30*ScaleWidth, 0)];
    self.bgView.backgroundColor = [Utils getUIColorWithHexString:SysColorWhite];
    self.bgView.layer.cornerRadius = 5;
    self.bgView.clipsToBounds = YES;
    [self addSubview:self.bgView];
    //
    UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 42, self.bgView.width, 17) text:array[0] font:font_17 textAlignment:NSTextAlignmentCenter textColor:[Utils getUIColorWithHexString:ccColor2e2e2e] backgroundColor:[UIColor clearColor] numberOfLines:1];
    [self.bgView addSubview:titleLabel];
    
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        //
        CGAffineTransform oldTransform = self.bgView.transform;
        self.bgView.transform  =
        CGAffineTransformScale(self.bgView.transform, 0.5, 0.5);
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.bgView.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }

}
-(void)PressBtn:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(RBMyAlert:clickedButtonAtIndex:)]) {
        [self.delegate RBMyAlert:self clickedButtonAtIndex:(int)sender.tag -100];
    }
    [self removeFromSuperview];
}
#pragma mark - UITapGestureRecognizer Delegate
- (void)tapGesture {
    [self removeFromSuperview];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
