//
//  RBPictureUpload.m
//  RB
//
//  Created by AngusNi on 5/17/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBPictureUpload.h"
#import "Service.h"

@implementation RBPictureUpload
DEFINE_SINGLETON_FOR_CLASS(RBPictureUpload);

- (instancetype)init {
    if (self = [super init]) {
    }
    return self;
}

- (void)RBPictureUploadClickedButtonAtIndex:(int)buttonIndex {
    if (buttonIndex == 0) {
        AVAuthorizationStatus authStatus  =
        [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (authStatus == ALAuthorizationStatusDenied) {
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.alertBody = @"相机不可用,请设置打开";
            [[UIApplication sharedApplication]
             presentLocalNotificationNow:notification];
            return;
        }
        [self getMediaFromSource:UIImagePickerControllerSourceTypeCamera];
    }
    if (buttonIndex == 1) {
        ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
        if (author == ALAuthorizationStatusDenied) {
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.alertBody = @"照片不可用,请设置打开";
            [[UIApplication sharedApplication]
             presentLocalNotificationNow:notification];
            return;
        }
        [self getMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *originalImage  =
    [info objectForKey:UIImagePickerControllerOriginalImage];
    if (originalImage !=  nil) {
        originalImage = [originalImage fixOrientation];
    }
    [self.fromVC dismissViewControllerAnimated:
     YES completion:^{
         if (originalImage !=  nil) {
             if(self.scaleFrame.size.width==0) {
                 if ([self.delegate respondsToSelector:@selector(RBPictureUpload:selectImage:)]) {
                     [self.delegate RBPictureUpload:self selectImage:originalImage];
                 }
             } else {
                 VPImageCropperViewController *imgEditorVC  =
                 [[VPImageCropperViewController alloc]
                  initWithImage:originalImage
                  cropFrame:self.scaleFrame
                  limitScaleRatio:3.0];
                 imgEditorVC.delegate = self;
                 [self.fromVC presentViewController:imgEditorVC
                                           animated:YES
                                         completion:NULL];
             }
         }
     }];
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated {
    if ([navigationController isKindOfClass:[UIImagePickerController class]] &&
        (((UIImagePickerController *)navigationController).sourceType  =
         UIImagePickerControllerSourceTypePhotoLibrary)) {
        }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self.fromVC dismissViewControllerAnimated:NO completion:NULL];
}

- (void)getMediaFromSource:(UIImagePickerControllerSourceType)sourceType {
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        UIImagePickerController *mpicker = [[UIImagePickerController alloc] init];
        mpicker.delegate = self;
        mpicker.sourceType = sourceType;
        [self.fromVC presentViewController:mpicker
                                  animated:YES
                                completion:NULL];
    }
}

- (void)imageCropper:(VPImageCropperViewController *)cropperViewController
         didFinished:(UIImage *)editedImage {
//    if (editedImage != nil) {
//        editedImage = [Utils getUIImageByScalingAndCroppingForSize:CGSizeMake(self.scaleFrame.size.width, self.scaleFrame.size.height) sourceImage:editedImage];
//        NSData *imageData = UIImageJPEGRepresentation(editedImage, 0.5);
//        editedImage = [UIImage imageWithData:imageData];
//    }
    [cropperViewController
     dismissViewControllerAnimated:YES
     completion:^{
         if (editedImage != nil) {
             if ([self.delegate respondsToSelector:@selector(RBPictureUpload:selectImage:)]) {
                 [self.delegate RBPictureUpload:self selectImage:editedImage];
             }
         }
     }];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES
                                              completion:NULL];
}


@end
