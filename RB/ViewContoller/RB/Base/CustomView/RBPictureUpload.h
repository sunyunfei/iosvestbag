//
//  RBPictureUpload.h
//  RB
//
//  Created by AngusNi on 5/17/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"


@class RBPictureUpload;
@protocol RBPictureUploadDelegate <NSObject>
@optional
- (void)RBPictureUpload:(RBPictureUpload *)pictureUpload selectImage:(UIImage*)img;
@end

@interface RBPictureUpload : NSObject
<VPImageCropperDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
DEFINE_SINGLETON_FOR_HEADER(RBPictureUpload);
@property (nonatomic, assign) CGRect scaleFrame;
@property (nonatomic, strong) UINavigationController *fromVC;
@property (nonatomic, weak)  id<RBPictureUploadDelegate> delegate;

- (void)RBPictureUploadClickedButtonAtIndex:(int)buttonIndex;
@end
