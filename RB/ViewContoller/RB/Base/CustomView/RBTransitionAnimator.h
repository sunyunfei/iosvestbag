//
//  RBTransitionAnimator.h
//  RB
//
//  Created by AngusNi on 3/28/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBTransitionAnimator : NSObject <UIViewControllerAnimatedTransitioning>
typedef NS_ENUM (NSInteger, AnimationType) {
    AnimationTypeDismiss = 0,
    AnimationTypePresent = 1,
};

@property(nonatomic, assign) AnimationType animationType;

@end
