//
//  RBTransitionAnimator.m
//  RB
//
//  Created by AngusNi on 3/28/16.
//  Copyright © 2016 AngusNi. All rights reserved.
//

#import "RBTransitionAnimator.h"
@implementation RBTransitionAnimator

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 0;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView * toView = toViewController.view;
    UIView * fromView = fromViewController.view;
    if (self.animationType == AnimationTypeDismiss) {
        UIView * toViewSnap = [toView snapshotViewAfterScreenUpdates:YES];
        [transitionContext.containerView addSubview:toViewSnap];
        UIView * fromViewSnap = [fromView snapshotViewAfterScreenUpdates:YES];
        [transitionContext.containerView addSubview:fromViewSnap];
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            [toViewSnap setFrame:CGRectMake(-[UIScreen mainScreen].bounds.size.width,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
            [fromViewSnap setFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3 animations:^{
                [toViewSnap setFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
                [fromViewSnap setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
            } completion:^(BOOL finished) {
                [fromViewSnap removeFromSuperview];
                [toViewSnap removeFromSuperview];
                [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
            }];
        }];
    } else {
        UIView * toViewSnap = [toView snapshotViewAfterScreenUpdates:YES];
        [transitionContext.containerView addSubview:toViewSnap];
        UIView * fromViewSnap = [fromView snapshotViewAfterScreenUpdates:YES];
        [transitionContext.containerView addSubview:fromViewSnap];
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            [toViewSnap setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
            [fromViewSnap setFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3 animations:^{
                [toViewSnap setFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
                [fromViewSnap setFrame:CGRectMake(-[UIScreen mainScreen].bounds.size.width,0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
            } completion:^(BOOL finished) {
                [fromViewSnap removeFromSuperview];
                [toViewSnap removeFromSuperview];
                [[transitionContext containerView] addSubview:toView];
                [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
            }];
        }];
    }
}
@end