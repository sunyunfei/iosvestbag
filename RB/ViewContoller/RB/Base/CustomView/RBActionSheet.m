//
//  RBActionSheet.m
//  RB
//
//  Created by AngusNi on 15/6/2.
//  Copyright (c) 2015年 AngusNi. All rights reserved.
//

#import "RBActionSheet.h"
#import "Service.h"

@interface RBActionSheet () {
}
@end

@implementation RBActionSheet

- (instancetype)init {
    if (self = [super init]) {
        self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
        id<UIApplicationDelegate> delegate  = 
        [[UIApplication sharedApplication] delegate];
        if ([delegate respondsToSelector:@selector(window)]) {
            self.window = [delegate performSelector:@selector(window)];
        } else {
            self.window = [[UIApplication sharedApplication] keyWindow];
        }
        //
        self.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.backgroundColor = SysColorCoverDeep;
        //
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}


- (void)showWithArray:(NSArray *)array {
    [self removeAllSubviews];
    self.array = array;
    float top = ScreenHeight - ([self.array count]-1)*55.0-50.0;
    //
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(14.0,top, ScreenWidth-14.0*2.0,([self.array count]-1)*55.0+50.0)];
    [self addSubview:self.bgView];
    //
    for(int i=0;i<[self.array count];i++){
        UIButton*tempBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [tempBtn setBackgroundColor:[UIColor whiteColor]];
        tempBtn.tag = 1000+i;
        if (i == [self.array count]-1) {
            tempBtn.frame = CGRectMake( 0, i*55.0, self.bgView.width, 50);
        } else {
            tempBtn.frame = CGRectMake( 0, i*55.0, self.bgView.width, 55.0);
        }
        [tempBtn addTarget:self
                        action:@selector(tempBtnAction:)
              forControlEvents:UIControlEventTouchUpInside];
        [tempBtn setTitle:self.array[i] forState:UIControlStateNormal];
        [tempBtn setTitleColor:[Utils getUIColorWithHexString:SysColorGray] forState:UIControlStateNormal];
        tempBtn.titleLabel.font = font_15;
        [self.bgView addSubview:tempBtn];
        //
        if (i != [self.array count]-1) {
            UILabel*lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, tempBtn.height-0.5, tempBtn.width, 0.5)];
            lineLabel.backgroundColor = [Utils getUIColorWithHexString:SysColorLine];
            [tempBtn addSubview:lineLabel];
        }
    }
    //
    if (![self.window.subviews containsObject:self]) {
        [self.window addSubview:self];
        CGAffineTransform oldTransform = self.bgView.transform;
        self.bgView.transform  =
        CGAffineTransformScale(self.bgView.transform, 0.5, 0.5);
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.bgView.transform = oldTransform;
        } completion:^(BOOL finished) {
        }];
    }
}

#pragma mark - UITapGestureRecognizer Delegate
- (void)tapGesture {
    [self removeFromSuperview];
}

#pragma mark - UIButton Delegate
- (void)tempBtnAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(RBActionSheet:clickedButtonAtIndex:)]) {
        [self.delegate RBActionSheet:self clickedButtonAtIndex:(int)sender.tag-1000];
    }
    [self removeFromSuperview];
}

@end
