//
//  RBBaseViewController.h
//  RB
//
//  Created by AngusNi on 16/1/28.
//  Copyright © 2016年 AngusNi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "Handler.h"
#import "RBTransitionAnimator.h"
#import "RBHUDView.h"
#import "RBAlertView.h"

@protocol RBBaseVCDelegate <NSObject>
@optional
- (void)RBNotificationShowLoginView;
- (void)RBNotificationRefreshHomeView;
- (void)RBNotificationRefreshIndianaView;
- (void)RBNotificationShowMessageView;
- (void)RBNotificationShowFeedbackView;
- (void)RBNotificationRefreshArticleView;
- (void)RBNotificationResignActive;
- (void)RBNotificationBecomeActive;
- (void)RBNotificationRefreshUserInfo;
- (void)RBNotificationStatusBarAction;
- (void)RBNotificationDismissView;
- (void)RBNotificationRefreshApply;

- (void)RBNavLeftBtnAction;
- (void)RBNavRightBtnAction;
- (void)RBNotificationBackToHome;
- (void)RBNotificationCampaignAddCover;
@end

@interface RBBaseViewController : UIViewController
<UIGestureRecognizerDelegate,HandlerDelegate,RBAlertViewDelegate,UIViewControllerTransitioningDelegate>
@property(nonatomic, weak) id<RBBaseVCDelegate> delegate;
@property(nonatomic, strong) RBHUDView *hudView;
@property(nonatomic, strong) RBTransitionAnimator *transitionAnimator;
@property(nonatomic, strong) UIView*defaultView;

- (BOOL)isCurrentVCVisible:(UIViewController *)viewController;
- (void)dismissVC;
- (void)presentVC:(UIViewController *)toVC;
- (BOOL)isVisitorLogin;
- (void)loadNavigationView;

@property(nonatomic, strong) UIImageView*bgIV;
@property(nonatomic, strong) UIScreenEdgePanGestureRecognizer *edgePGR;
@property(nonatomic, strong) UIView*navView;
@property(nonatomic, strong) NSString*navTitle;
@property(nonatomic, strong) NSString*navRightTitle;
@property(nonatomic, strong) NSString*navLeftTitle;

@property(nonatomic, strong) UIButton*navLeftBtn;
@property(nonatomic, strong) UILabel*navLineLabel;
@property(nonatomic, strong) UILabel*navTitleLabel;
@property(nonatomic, strong) UILabel*navLeftLabel;
@property(nonatomic, strong) UILabel*navRightLabel;
@end
