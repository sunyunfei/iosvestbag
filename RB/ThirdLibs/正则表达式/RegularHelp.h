//
//  RegularHelp.h
//

#import <Foundation/Foundation.h>

@interface RegularHelp : NSObject
//银行卡
+ (BOOL)validateNumber:(NSString *) textString;
//数字匹配
+ (BOOL) validateUserAge:(NSString *)str;
//检验邮箱
+ (BOOL) validateUserEmail:(NSString *)str;
//检验手机号
+ (BOOL) validateUserPhone:(NSString *)str;
//验证是否为正数
+ (BOOL) validatePositiveNumber:(NSString *)str;
//
+ (BOOL) validateMoney:(NSString *)str;
//正则匹配用户密码6-18位数字和字母组合
+ (BOOL)checkPassword:(NSString*)password;
//匹配网址
+ (BOOL)isURL:(NSString*)url;

@end
